package defpackage;

import com.google.android.material.textfield.TextInputEditText;
import n.Unit;
import n.n.b.Functions0;
import n.n.c.j;

/* renamed from: g  reason: default package */
/* compiled from: com.android.tools.r8.jetbrains.kotlin-style lambda group */
public final class g extends j implements Functions0<Boolean, n.g> {
    public final /* synthetic */ int c;
    public final /* synthetic */ Object d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public g(int i2, Object obj) {
        super(1);
        this.c = i2;
        this.d = obj;
    }

    public final Object a(Object obj) {
        int i2 = this.c;
        if (i2 == 0) {
            ((TextInputEditText) this.d).setEnabled(((Boolean) obj).booleanValue());
            return Unit.a;
        } else if (i2 == 1) {
            ((TextInputEditText) this.d).setEnabled(((Boolean) obj).booleanValue());
            return Unit.a;
        } else {
            throw null;
        }
    }
}
