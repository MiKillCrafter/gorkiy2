package k.a;

import m.a.Provider;

public final class DoubleCheck<T> implements Provider<T> {
    public static final Object c = new Object();
    public volatile Provider<T> a;
    public volatile Object b = c;

    public DoubleCheck(Provider<T> provider) {
        this.a = provider;
    }

    public static Object a(Object obj, Object obj2) {
        if (!(obj != c) || obj == obj2) {
            return obj2;
        }
        throw new IllegalStateException("Scoped provider was invoked recursively returning different results: " + obj + " & " + obj2 + ". This is likely due to a circular dependency.");
    }

    public T get() {
        T t2 = this.b;
        if (t2 == c) {
            synchronized (this) {
                t2 = this.b;
                if (t2 == c) {
                    t2 = this.a.get();
                    a(this.b, t2);
                    this.b = t2;
                    this.a = null;
                }
            }
        }
        return t2;
    }

    public static <P extends Provider<T>, T> Provider<T> a(P p2) {
        if (p2 == null) {
            throw null;
        } else if (p2 instanceof DoubleCheck) {
            return p2;
        } else {
            return new DoubleCheck(p2);
        }
    }
}
