package k.a;

import java.util.Map;
import k.a.AbstractMapFactory;
import m.a.Provider;

public final class MapProviderFactory<K, V> extends AbstractMapFactory<K, V, Provider<V>> {

    public static final class b<K, V> extends AbstractMapFactory.a<K, V, Provider<V>> {
        public /* synthetic */ b(int i2, a aVar) {
            super(i2);
        }

        public MapProviderFactory<K, V> a() {
            return new MapProviderFactory<>(super.a, null);
        }
    }

    public /* synthetic */ MapProviderFactory(Map map, a aVar) {
        super(map);
    }

    public static <K, V> b<K, V> a(int i2) {
        return new b<>(i2, null);
    }

    public Object get() {
        return super.a;
    }
}
