package q.b.a;

import com.crashlytics.android.core.SessionProtobufHelper;
import j.a.a.a.outline;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.InvalidObjectException;
import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import n.i.Collections;
import org.threeten.bp.DateTimeException;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;
import q.b.a.v.ChronoField;
import q.b.a.v.Temporal;
import q.b.a.v.TemporalField;
import q.b.a.v.TemporalQueries;
import q.b.a.v.TemporalQuery;
import q.b.a.v.ValueRange;
import q.b.a.v.e;
import q.b.a.v.f;
import q.b.a.w.ZoneRules;

public final class ZoneOffset extends o implements e, f, Comparable<p>, Serializable {

    /* renamed from: e  reason: collision with root package name */
    public static final ConcurrentMap<Integer, p> f3096e = new ConcurrentHashMap(16, 0.75f, 4);

    /* renamed from: f  reason: collision with root package name */
    public static final ConcurrentMap<String, p> f3097f = new ConcurrentHashMap(16, 0.75f, 4);
    public static final ZoneOffset g = a(0);
    public static final ZoneOffset h = a(-64800);

    /* renamed from: i  reason: collision with root package name */
    public static final ZoneOffset f3098i = a(64800);
    public final int c;
    public final transient String d;

    public ZoneOffset(int i2) {
        String str;
        this.c = i2;
        if (i2 == 0) {
            str = "Z";
        } else {
            int abs = Math.abs(i2);
            StringBuilder sb = new StringBuilder();
            int i3 = abs / 3600;
            int i4 = (abs / 60) % 60;
            sb.append(i2 < 0 ? "-" : "+");
            sb.append(i3 < 10 ? SessionProtobufHelper.SIGNAL_DEFAULT : "");
            sb.append(i3);
            String str2 = ":0";
            sb.append(i4 < 10 ? str2 : ":");
            sb.append(i4);
            int i5 = abs % 60;
            if (i5 != 0) {
                sb.append(i5 >= 10 ? ":" : str2);
                sb.append(i5);
            }
            str = sb.toString();
        }
        this.d = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.ZoneOffset.a(java.lang.CharSequence, int, boolean):int
     arg types: [java.lang.String, int, int]
     candidates:
      q.b.a.ZoneOffset.a(int, int, int):q.b.a.ZoneOffset
      q.b.a.ZoneOffset.a(java.lang.CharSequence, int, boolean):int */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x008e A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00a7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static q.b.a.ZoneOffset a(java.lang.String r7) {
        /*
            java.lang.String r0 = "offsetId"
            n.i.Collections.a(r7, r0)
            java.util.concurrent.ConcurrentMap<java.lang.String, q.b.a.p> r0 = q.b.a.ZoneOffset.f3097f
            java.lang.Object r0 = r0.get(r7)
            q.b.a.ZoneOffset r0 = (q.b.a.ZoneOffset) r0
            if (r0 == 0) goto L_0x0010
            return r0
        L_0x0010:
            int r0 = r7.length()
            r1 = 2
            r2 = 1
            r3 = 0
            if (r0 == r1) goto L_0x0062
            r1 = 3
            if (r0 == r1) goto L_0x007e
            r4 = 5
            if (r0 == r4) goto L_0x0059
            r5 = 6
            r6 = 4
            if (r0 == r5) goto L_0x0050
            r5 = 7
            if (r0 == r5) goto L_0x0043
            r1 = 9
            if (r0 != r1) goto L_0x0037
            int r0 = a(r7, r2, r3)
            int r1 = a(r7, r6, r2)
            int r2 = a(r7, r5, r2)
            goto L_0x0084
        L_0x0037:
            org.threeten.bp.DateTimeException r0 = new org.threeten.bp.DateTimeException
            java.lang.String r1 = "Invalid ID for ZoneOffset, invalid format: "
            java.lang.String r7 = j.a.a.a.outline.a(r1, r7)
            r0.<init>(r7)
            throw r0
        L_0x0043:
            int r0 = a(r7, r2, r3)
            int r1 = a(r7, r1, r3)
            int r2 = a(r7, r4, r3)
            goto L_0x0084
        L_0x0050:
            int r0 = a(r7, r2, r3)
            int r1 = a(r7, r6, r2)
            goto L_0x0083
        L_0x0059:
            int r0 = a(r7, r2, r3)
            int r1 = a(r7, r1, r3)
            goto L_0x0083
        L_0x0062:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            char r1 = r7.charAt(r3)
            r0.append(r1)
            java.lang.String r1 = "0"
            r0.append(r1)
            char r7 = r7.charAt(r2)
            r0.append(r7)
            java.lang.String r7 = r0.toString()
        L_0x007e:
            int r0 = a(r7, r2, r3)
            r1 = 0
        L_0x0083:
            r2 = 0
        L_0x0084:
            char r3 = r7.charAt(r3)
            r4 = 43
            r5 = 45
            if (r3 == r4) goto L_0x009d
            if (r3 != r5) goto L_0x0091
            goto L_0x009d
        L_0x0091:
            org.threeten.bp.DateTimeException r0 = new org.threeten.bp.DateTimeException
            java.lang.String r1 = "Invalid ID for ZoneOffset, plus/minus not found when expected: "
            java.lang.String r7 = j.a.a.a.outline.a(r1, r7)
            r0.<init>(r7)
            throw r0
        L_0x009d:
            if (r3 != r5) goto L_0x00a7
            int r7 = -r0
            int r0 = -r1
            int r1 = -r2
            q.b.a.ZoneOffset r7 = a(r7, r0, r1)
            return r7
        L_0x00a7:
            q.b.a.ZoneOffset r7 = a(r0, r1, r2)
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: q.b.a.ZoneOffset.a(java.lang.String):q.b.a.ZoneOffset");
    }

    private Object readResolve() {
        throw new InvalidObjectException("Deserialization via serialization delegate");
    }

    private Object writeReplace() {
        return new Ser((byte) 8, this);
    }

    public int b(TemporalField temporalField) {
        if (temporalField == ChronoField.OFFSET_SECONDS) {
            return this.c;
        }
        if (!(temporalField instanceof ChronoField)) {
            return a(temporalField).a(d(temporalField), temporalField);
        }
        throw new UnsupportedTemporalTypeException(outline.a("Unsupported field: ", temporalField));
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [q.b.a.v.TemporalAccessor, q.b.a.ZoneOffset] */
    public boolean c(TemporalField temporalField) {
        if (temporalField instanceof ChronoField) {
            if (temporalField == ChronoField.OFFSET_SECONDS) {
                return true;
            }
            return false;
        } else if (temporalField == null || !temporalField.a(this)) {
            return false;
        } else {
            return true;
        }
    }

    public int compareTo(Object obj) {
        return ((ZoneOffset) obj).c - this.c;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [q.b.a.v.TemporalAccessor, q.b.a.ZoneOffset] */
    public long d(TemporalField temporalField) {
        if (temporalField == ChronoField.OFFSET_SECONDS) {
            return (long) this.c;
        }
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.b(this);
        }
        throw new DateTimeException(outline.a("Unsupported field: ", temporalField));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ZoneOffset) || this.c != ((ZoneOffset) obj).c) {
            return false;
        }
        return true;
    }

    public String f() {
        return this.d;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.ZoneOffset, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public ZoneRules g() {
        Collections.a((Object) this, "offset");
        return new ZoneRules.a(this);
    }

    public int hashCode() {
        return this.c;
    }

    public String toString() {
        return this.d;
    }

    public void b(DataOutput dataOutput) {
        int i2 = this.c;
        int i3 = i2 % 900 == 0 ? i2 / 900 : 127;
        dataOutput.writeByte(i3);
        if (i3 == 127) {
            dataOutput.writeInt(i2);
        }
    }

    public static int a(CharSequence charSequence, int i2, boolean z) {
        if (!z || charSequence.charAt(i2 - 1) == ':') {
            char charAt = charSequence.charAt(i2);
            char charAt2 = charSequence.charAt(i2 + 1);
            if (charAt < '0' || charAt > '9' || charAt2 < '0' || charAt2 > '9') {
                throw new DateTimeException("Invalid ID for ZoneOffset, non numeric characters found: " + ((Object) charSequence));
            }
            return (charAt2 - '0') + ((charAt - '0') * 10);
        }
        throw new DateTimeException("Invalid ID for ZoneOffset, colon not found when expected: " + ((Object) charSequence));
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [R, q.b.a.v.TemporalAccessor, q.b.a.ZoneOffset] */
    public <R> R a(TemporalQuery temporalQuery) {
        if (temporalQuery == TemporalQueries.f3150e || temporalQuery == TemporalQueries.d) {
            return this;
        }
        if (temporalQuery == TemporalQueries.f3151f || temporalQuery == TemporalQueries.g || temporalQuery == TemporalQueries.c || temporalQuery == TemporalQueries.b || temporalQuery == TemporalQueries.a) {
            return null;
        }
        return temporalQuery.a(this);
    }

    public static ZoneOffset a(int i2, int i3, int i4) {
        if (i2 < -18 || i2 > 18) {
            throw new DateTimeException(outline.b("Zone offset hours not in valid range: value ", i2, " is not in the range -18 to 18"));
        }
        if (i2 > 0) {
            if (i3 < 0 || i4 < 0) {
                throw new DateTimeException("Zone offset minutes and seconds must be positive because hours is positive");
            }
        } else if (i2 < 0) {
            if (i3 > 0 || i4 > 0) {
                throw new DateTimeException("Zone offset minutes and seconds must be negative because hours is negative");
            }
        } else if ((i3 > 0 && i4 < 0) || (i3 < 0 && i4 > 0)) {
            throw new DateTimeException("Zone offset minutes and seconds must have the same sign");
        }
        if (Math.abs(i3) > 59) {
            StringBuilder a = outline.a("Zone offset minutes not in valid range: abs(value) ");
            a.append(Math.abs(i3));
            a.append(" is not in the range 0 to 59");
            throw new DateTimeException(a.toString());
        } else if (Math.abs(i4) > 59) {
            StringBuilder a2 = outline.a("Zone offset seconds not in valid range: abs(value) ");
            a2.append(Math.abs(i4));
            a2.append(" is not in the range 0 to 59");
            throw new DateTimeException(a2.toString());
        } else if (Math.abs(i2) != 18 || (Math.abs(i3) <= 0 && Math.abs(i4) <= 0)) {
            return a((i3 * 60) + (i2 * 3600) + i4);
        } else {
            throw new DateTimeException("Zone offset not in valid range: -18:00 to +18:00");
        }
    }

    public static ZoneOffset a(int i2) {
        if (Math.abs(i2) > 64800) {
            throw new DateTimeException("Zone offset not in valid range: -18:00 to +18:00");
        } else if (i2 % 900 != 0) {
            return new ZoneOffset(i2);
        } else {
            Integer valueOf = Integer.valueOf(i2);
            ZoneOffset zoneOffset = f3096e.get(valueOf);
            if (zoneOffset != null) {
                return zoneOffset;
            }
            f3096e.putIfAbsent(valueOf, new ZoneOffset(i2));
            ZoneOffset zoneOffset2 = f3096e.get(valueOf);
            f3097f.putIfAbsent(zoneOffset2.d, zoneOffset2);
            return zoneOffset2;
        }
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [q.b.a.v.TemporalAccessor, q.b.a.ZoneOffset] */
    public ValueRange a(TemporalField temporalField) {
        if (temporalField == ChronoField.OFFSET_SECONDS) {
            return temporalField.g();
        }
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.c(this);
        }
        throw new UnsupportedTemporalTypeException(outline.a("Unsupported field: ", temporalField));
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public Temporal a(Temporal temporal) {
        return temporal.a((TemporalField) ChronoField.OFFSET_SECONDS, (long) this.c);
    }

    public void a(DataOutput dataOutput) {
        dataOutput.writeByte(8);
        b(dataOutput);
    }

    public static ZoneOffset a(DataInput dataInput) {
        byte readByte = dataInput.readByte();
        return readByte == Byte.MAX_VALUE ? a(dataInput.readInt()) : a(readByte * 900);
    }
}
