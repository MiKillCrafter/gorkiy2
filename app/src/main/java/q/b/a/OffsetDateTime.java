package q.b.a;

import androidx.recyclerview.widget.RecyclerView;
import j.a.a.a.outline;
import java.io.DataInput;
import java.io.InvalidObjectException;
import java.io.Serializable;
import n.i.Collections;
import org.threeten.bp.DateTimeException;
import q.b.a.s.ChronoLocalDateTime;
import q.b.a.s.IsoChronology;
import q.b.a.u.b;
import q.b.a.v.ChronoField;
import q.b.a.v.ChronoUnit;
import q.b.a.v.Temporal;
import q.b.a.v.TemporalAdjuster;
import q.b.a.v.TemporalField;
import q.b.a.v.TemporalQueries;
import q.b.a.v.TemporalQuery;
import q.b.a.v.TemporalUnit;
import q.b.a.v.ValueRange;
import q.b.a.v.d;
import q.b.a.v.f;

public final class OffsetDateTime extends b implements d, f, Comparable<i>, Serializable {
    public final LocalDateTime b;
    public final ZoneOffset c;

    static {
        LocalDateTime localDateTime = LocalDateTime.d;
        ZoneOffset zoneOffset = ZoneOffset.f3098i;
        if (localDateTime != null) {
            new OffsetDateTime(localDateTime, zoneOffset);
            LocalDateTime localDateTime2 = LocalDateTime.f3090e;
            ZoneOffset zoneOffset2 = ZoneOffset.h;
            if (localDateTime2 != null) {
                new OffsetDateTime(localDateTime2, zoneOffset2);
                return;
            }
            throw null;
        }
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.LocalDateTime, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.ZoneOffset, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public OffsetDateTime(LocalDateTime localDateTime, ZoneOffset zoneOffset) {
        Collections.a((Object) localDateTime, "dateTime");
        this.b = localDateTime;
        Collections.a((Object) zoneOffset, "offset");
        this.c = zoneOffset;
    }

    private Object readResolve() {
        throw new InvalidObjectException("Deserialization via serialization delegate");
    }

    private Object writeReplace() {
        return new Ser((byte) 69, this);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.u.DefaultInterfaceTemporalAccessor, q.b.a.OffsetDateTime] */
    public <R> R a(TemporalQuery temporalQuery) {
        if (temporalQuery == TemporalQueries.b) {
            return IsoChronology.d;
        }
        if (temporalQuery == TemporalQueries.c) {
            return ChronoUnit.NANOS;
        }
        if (temporalQuery == TemporalQueries.f3150e || temporalQuery == TemporalQueries.d) {
            return this.c;
        }
        if (temporalQuery == TemporalQueries.f3151f) {
            return this.b.b;
        }
        if (temporalQuery == TemporalQueries.g) {
            return this.b.c;
        }
        if (temporalQuery == TemporalQueries.a) {
            return null;
        }
        return OffsetDateTime.super.a(temporalQuery);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.v.TemporalAccessor, q.b.a.OffsetDateTime] */
    public boolean c(TemporalField temporalField) {
        return (temporalField instanceof ChronoField) || (temporalField != null && temporalField.a(this));
    }

    public int compareTo(Object obj) {
        OffsetDateTime offsetDateTime = (OffsetDateTime) obj;
        if (this.c.equals(offsetDateTime.c)) {
            return this.b.compareTo((ChronoLocalDateTime<?>) offsetDateTime.b);
        }
        int a = Collections.a(j(), offsetDateTime.j());
        if (a != 0) {
            return a;
        }
        LocalDateTime localDateTime = this.b;
        int i2 = localDateTime.c.f3094e;
        LocalDateTime localDateTime2 = offsetDateTime.b;
        int i3 = i2 - localDateTime2.c.f3094e;
        return i3 == 0 ? localDateTime.compareTo((ChronoLocalDateTime<?>) localDateTime2) : i3;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [q.b.a.v.TemporalAccessor, q.b.a.OffsetDateTime] */
    public long d(TemporalField temporalField) {
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.b(this);
        }
        int ordinal = ((ChronoField) temporalField).ordinal();
        if (ordinal == 28) {
            return j();
        }
        if (ordinal != 29) {
            return this.b.d(temporalField);
        }
        return (long) this.c.c;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof OffsetDateTime)) {
            return false;
        }
        OffsetDateTime offsetDateTime = (OffsetDateTime) obj;
        if (!this.b.equals(offsetDateTime.b) || !this.c.equals(offsetDateTime.c)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return this.b.hashCode() ^ this.c.c;
    }

    public int i() {
        return this.b.c.f3094e;
    }

    public long j() {
        return this.b.a(this.c);
    }

    public String toString() {
        return this.b.toString() + this.c.d;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [q.b.a.u.DefaultInterfaceTemporalAccessor, q.b.a.OffsetDateTime] */
    public int b(TemporalField temporalField) {
        if (!(temporalField instanceof ChronoField)) {
            return OffsetDateTime.super.b(temporalField);
        }
        int ordinal = ((ChronoField) temporalField).ordinal();
        if (ordinal == 28) {
            throw new DateTimeException(outline.a("Field too large for an int: ", temporalField));
        } else if (ordinal != 29) {
            return this.b.b(temporalField);
        } else {
            return this.c.c;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.LocalDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.LocalDateTime
     arg types: [long, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.LocalDateTime.b(q.b.a.LocalDate, q.b.a.LocalTime):q.b.a.LocalDateTime
      q.b.a.LocalDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDateTime
      q.b.a.LocalDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDateTime.b(long, q.b.a.v.m):q.b.a.s.ChronoLocalDateTime<D>
      q.b.a.s.ChronoLocalDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.LocalDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.LocalDateTime */
    public OffsetDateTime b(long j2, TemporalUnit temporalUnit) {
        if (temporalUnit instanceof ChronoUnit) {
            return a(this.b.b(j2, temporalUnit), this.c);
        }
        return (OffsetDateTime) temporalUnit.a(this, j2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.Instant, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.ZoneId, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public static OffsetDateTime a(Instant instant, ZoneId zoneId) {
        Collections.a((Object) instant, "instant");
        Collections.a((Object) zoneId, "zone");
        ZoneOffset a = zoneId.g().a(instant);
        return new OffsetDateTime(LocalDateTime.a(instant.b, instant.c, a), a);
    }

    public final OffsetDateTime a(LocalDateTime localDateTime, ZoneOffset zoneOffset) {
        if (this.b != localDateTime || !this.c.equals(zoneOffset)) {
            return new OffsetDateTime(localDateTime, zoneOffset);
        }
        return this;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.v.TemporalAccessor, q.b.a.OffsetDateTime] */
    public ValueRange a(TemporalField temporalField) {
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.c(this);
        }
        if (temporalField == ChronoField.INSTANT_SECONDS || temporalField == ChronoField.OFFSET_SECONDS) {
            return temporalField.g();
        }
        return this.b.a(temporalField);
    }

    /* JADX WARN: Type inference failed for: r2v2, types: [q.b.a.v.Temporal, q.b.a.OffsetDateTime] */
    /* JADX WARN: Type inference failed for: r2v4, types: [q.b.a.v.Temporal, q.b.a.OffsetDateTime] */
    /* JADX WARN: Type inference failed for: r2v5, types: [q.b.a.v.Temporal, q.b.a.OffsetDateTime] */
    /* JADX WARN: Type inference failed for: r2v7, types: [q.b.a.v.Temporal, q.b.a.OffsetDateTime] */
    /* JADX WARN: Type inference failed for: r0v9, types: [q.b.a.ZoneId, q.b.a.ZoneOffset] */
    /* JADX WARN: Type inference failed for: r2v9, types: [q.b.a.v.Temporal, q.b.a.OffsetDateTime] */
    public Temporal a(TemporalAdjuster temporalAdjuster) {
        if ((temporalAdjuster instanceof LocalDate) || (temporalAdjuster instanceof LocalTime) || (temporalAdjuster instanceof LocalDateTime)) {
            return a(this.b.a(temporalAdjuster), this.c);
        }
        if (temporalAdjuster instanceof Instant) {
            return a((Instant) temporalAdjuster, (ZoneId) this.c);
        }
        if (temporalAdjuster instanceof ZoneOffset) {
            return a(this.b, (ZoneOffset) temporalAdjuster);
        }
        if (temporalAdjuster instanceof OffsetDateTime) {
            return (OffsetDateTime) temporalAdjuster;
        }
        return (OffsetDateTime) temporalAdjuster.a(this);
    }

    /* JADX WARN: Type inference failed for: r4v2, types: [q.b.a.v.Temporal, q.b.a.OffsetDateTime] */
    /* JADX WARN: Type inference failed for: r0v2, types: [java.lang.Enum, q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r5v1, types: [q.b.a.ZoneId, q.b.a.ZoneOffset] */
    /* JADX WARN: Type inference failed for: r4v5, types: [q.b.a.v.Temporal, q.b.a.OffsetDateTime] */
    /* JADX WARN: Type inference failed for: r4v7, types: [q.b.a.v.Temporal, q.b.a.OffsetDateTime] */
    /* JADX WARN: Type inference failed for: r4v9, types: [q.b.a.v.Temporal, q.b.a.OffsetDateTime] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.LocalDateTime.a(q.b.a.v.TemporalField, long):q.b.a.LocalDateTime
     arg types: [q.b.a.v.TemporalField, long]
     candidates:
      q.b.a.LocalDateTime.a(java.lang.CharSequence, q.b.a.t.DateTimeFormatter):q.b.a.LocalDateTime
      q.b.a.LocalDateTime.a(long, q.b.a.v.TemporalUnit):q.b.a.LocalDateTime
      q.b.a.LocalDateTime.a(q.b.a.LocalDate, q.b.a.LocalTime):q.b.a.LocalDateTime
      q.b.a.LocalDateTime.a(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDateTime
      q.b.a.LocalDateTime.a(q.b.a.v.TemporalField, long):q.b.a.s.ChronoLocalDateTime
      q.b.a.LocalDateTime.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.LocalDateTime.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDateTime.a(long, q.b.a.v.m):q.b.a.s.ChronoLocalDateTime<D>
      q.b.a.s.ChronoLocalDateTime.a(q.b.a.v.j, long):q.b.a.s.ChronoLocalDateTime<D>
      q.b.a.s.ChronoLocalDateTime.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDateTime.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.LocalDateTime.a(q.b.a.v.TemporalField, long):q.b.a.LocalDateTime */
    public Temporal a(TemporalField temporalField, long j2) {
        if (!(temporalField instanceof ChronoField)) {
            return (OffsetDateTime) temporalField.a(this, j2);
        }
        ? r0 = (ChronoField) temporalField;
        int ordinal = r0.ordinal();
        if (ordinal == 28) {
            return a(Instant.b(j2, (long) i()), (ZoneId) this.c);
        }
        if (ordinal != 29) {
            return a(this.b.a(temporalField, j2), this.c);
        }
        return a(this.b, ZoneOffset.a(r0.range.a(j2, (TemporalField) r0)));
    }

    /* JADX WARN: Type inference failed for: r4v2, types: [q.b.a.v.Temporal, q.b.a.OffsetDateTime] */
    /* JADX WARN: Type inference failed for: r4v5, types: [q.b.a.v.Temporal, q.b.a.OffsetDateTime] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.OffsetDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.OffsetDateTime
     arg types: [int, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.OffsetDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.OffsetDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.OffsetDateTime */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.OffsetDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.OffsetDateTime
     arg types: [?, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.OffsetDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.OffsetDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.OffsetDateTime */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.OffsetDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.OffsetDateTime
     arg types: [long, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.OffsetDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.OffsetDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.OffsetDateTime */
    public Temporal a(long j2, TemporalUnit temporalUnit) {
        return j2 == Long.MIN_VALUE ? b((long) RecyclerView.FOREVER_NS, temporalUnit).b(1L, temporalUnit) : b(-j2, temporalUnit);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v1, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v2, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public Temporal a(Temporal temporal) {
        return temporal.a((TemporalField) ChronoField.EPOCH_DAY, this.b.b.l()).a((TemporalField) ChronoField.NANO_OF_DAY, this.b.c.i()).a((TemporalField) ChronoField.OFFSET_SECONDS, (long) this.c.c);
    }

    public static OffsetDateTime a(DataInput dataInput) {
        return new OffsetDateTime(LocalDateTime.a(dataInput), ZoneOffset.a(dataInput));
    }
}
