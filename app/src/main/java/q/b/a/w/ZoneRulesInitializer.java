package q.b.a.w;

import java.util.Iterator;
import java.util.ServiceConfigurationError;
import java.util.ServiceLoader;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public abstract class ZoneRulesInitializer {
    public static final AtomicBoolean a = new AtomicBoolean(false);
    public static final AtomicReference<g> b = new AtomicReference<>();

    public static class a extends ZoneRulesInitializer {
        public void a() {
            Class<ZoneRulesProvider> cls = ZoneRulesProvider.class;
            Iterator it = ServiceLoader.load(cls, cls.getClassLoader()).iterator();
            while (it.hasNext()) {
                try {
                    ZoneRulesProvider.a((ZoneRulesProvider) it.next());
                } catch (ServiceConfigurationError e2) {
                    if (!(e2.getCause() instanceof SecurityException)) {
                        throw e2;
                    }
                }
            }
        }
    }

    public abstract void a();
}
