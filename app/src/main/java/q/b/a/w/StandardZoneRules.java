package q.b.a.w;

import j.a.a.a.outline;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import q.b.a.DayOfWeek;
import q.b.a.Instant;
import q.b.a.LocalDate;
import q.b.a.LocalDateTime;
import q.b.a.Month;
import q.b.a.ZoneOffset;
import q.b.a.e;
import q.b.a.p;
import q.b.a.s.IsoChronology;
import q.b.a.v.TemporalAdjuster;
import q.b.a.v.TemporalAdjusters0;
import q.b.a.w.ZoneOffsetTransitionRule;
import q.b.a.w.ZoneRules;

public final class StandardZoneRules extends ZoneRules implements Serializable {
    public final long[] b;
    public final ZoneOffset[] c;
    public final long[] d;

    /* renamed from: e  reason: collision with root package name */
    public final LocalDateTime[] f3160e;

    /* renamed from: f  reason: collision with root package name */
    public final ZoneOffset[] f3161f;
    public final ZoneOffsetTransitionRule[] g;
    public final ConcurrentMap<Integer, d[]> h = new ConcurrentHashMap();

    public StandardZoneRules(long[] jArr, ZoneOffset[] zoneOffsetArr, long[] jArr2, ZoneOffset[] zoneOffsetArr2, ZoneOffsetTransitionRule[] zoneOffsetTransitionRuleArr) {
        this.b = jArr;
        this.c = zoneOffsetArr;
        this.d = jArr2;
        this.f3161f = zoneOffsetArr2;
        this.g = zoneOffsetTransitionRuleArr;
        ArrayList arrayList = new ArrayList();
        int i2 = 0;
        while (i2 < jArr2.length) {
            ZoneOffset zoneOffset = zoneOffsetArr2[i2];
            int i3 = i2 + 1;
            ZoneOffset zoneOffset2 = zoneOffsetArr2[i3];
            LocalDateTime a = LocalDateTime.a(jArr2[i2], 0, zoneOffset);
            if (zoneOffset2.c > zoneOffset.c) {
                arrayList.add(a);
                arrayList.add(a.c((long) (zoneOffset2.c - zoneOffset.c)));
            } else {
                arrayList.add(a.c((long) (zoneOffset2.c - zoneOffset.c)));
                arrayList.add(a);
            }
            i2 = i3;
        }
        this.f3160e = (LocalDateTime[]) arrayList.toArray(new LocalDateTime[arrayList.size()]);
    }

    private Object writeReplace() {
        return new Ser((byte) 1, this);
    }

    public boolean a() {
        return this.d.length == 0;
    }

    public List<p> b(e eVar) {
        Object c2 = c(eVar);
        if (!(c2 instanceof ZoneOffsetTransition)) {
            return Collections.singletonList((ZoneOffset) c2);
        }
        ZoneOffsetTransition zoneOffsetTransition = (ZoneOffsetTransition) c2;
        if (zoneOffsetTransition.h()) {
            return Collections.emptyList();
        }
        return Arrays.asList(zoneOffsetTransition.c, zoneOffsetTransition.d);
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x008a  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0097 A[ADDED_TO_REGION, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object c(q.b.a.LocalDateTime r10) {
        /*
            r9 = this;
            q.b.a.w.ZoneOffsetTransitionRule[] r0 = r9.g
            int r0 = r0.length
            r1 = 0
            r2 = 1
            if (r0 <= 0) goto L_0x009a
            q.b.a.LocalDateTime[] r0 = r9.f3160e
            int r3 = r0.length
            int r3 = r3 - r2
            r0 = r0[r3]
            r3 = 0
            if (r10 == 0) goto L_0x0099
            boolean r4 = r0 instanceof q.b.a.LocalDateTime
            if (r4 == 0) goto L_0x001b
            int r0 = r10.a(r0)
            if (r0 <= 0) goto L_0x003e
            goto L_0x0040
        L_0x001b:
            q.b.a.LocalDate r4 = r10.b
            long r4 = r4.l()
            q.b.a.LocalDate r6 = r0.b
            long r6 = r6.l()
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 > 0) goto L_0x0040
            if (r8 != 0) goto L_0x003e
            q.b.a.LocalTime r4 = r10.c
            long r4 = r4.i()
            q.b.a.LocalTime r0 = r0.c
            long r6 = r0.i()
            int r0 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r0 <= 0) goto L_0x003e
            goto L_0x0040
        L_0x003e:
            r0 = 0
            goto L_0x0041
        L_0x0040:
            r0 = 1
        L_0x0041:
            if (r0 == 0) goto L_0x009a
            q.b.a.LocalDate r0 = r10.b
            int r0 = r0.b
            q.b.a.w.ZoneOffsetTransition[] r0 = r9.a(r0)
            int r2 = r0.length
        L_0x004c:
            if (r1 >= r2) goto L_0x0098
            r3 = r0[r1]
            q.b.a.LocalDateTime r4 = r3.b
            boolean r5 = r3.h()
            if (r5 == 0) goto L_0x006f
            boolean r4 = r10.b(r4)
            if (r4 == 0) goto L_0x0061
            q.b.a.ZoneOffset r4 = r3.c
            goto L_0x0086
        L_0x0061:
            q.b.a.LocalDateTime r4 = r3.f()
            boolean r4 = r10.b(r4)
            if (r4 == 0) goto L_0x006c
            goto L_0x0085
        L_0x006c:
            q.b.a.ZoneOffset r4 = r3.d
            goto L_0x0086
        L_0x006f:
            boolean r4 = r10.b(r4)
            if (r4 != 0) goto L_0x0078
            q.b.a.ZoneOffset r4 = r3.d
            goto L_0x0086
        L_0x0078:
            q.b.a.LocalDateTime r4 = r3.f()
            boolean r4 = r10.b(r4)
            if (r4 == 0) goto L_0x0085
            q.b.a.ZoneOffset r4 = r3.c
            goto L_0x0086
        L_0x0085:
            r4 = r3
        L_0x0086:
            boolean r5 = r4 instanceof q.b.a.w.ZoneOffsetTransition
            if (r5 != 0) goto L_0x0097
            q.b.a.ZoneOffset r3 = r3.c
            boolean r3 = r4.equals(r3)
            if (r3 == 0) goto L_0x0093
            goto L_0x0097
        L_0x0093:
            int r1 = r1 + 1
            r3 = r4
            goto L_0x004c
        L_0x0097:
            return r4
        L_0x0098:
            return r3
        L_0x0099:
            throw r3
        L_0x009a:
            q.b.a.LocalDateTime[] r0 = r9.f3160e
            int r10 = java.util.Arrays.binarySearch(r0, r10)
            r0 = -1
            if (r10 != r0) goto L_0x00a8
            q.b.a.ZoneOffset[] r10 = r9.f3161f
            r10 = r10[r1]
            return r10
        L_0x00a8:
            if (r10 >= 0) goto L_0x00ae
            int r10 = -r10
            int r10 = r10 + -2
            goto L_0x00c1
        L_0x00ae:
            q.b.a.LocalDateTime[] r0 = r9.f3160e
            int r1 = r0.length
            int r1 = r1 - r2
            if (r10 >= r1) goto L_0x00c1
            r1 = r0[r10]
            int r3 = r10 + 1
            r0 = r0[r3]
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x00c1
            r10 = r3
        L_0x00c1:
            r0 = r10 & 1
            if (r0 != 0) goto L_0x00e8
            q.b.a.LocalDateTime[] r0 = r9.f3160e
            r1 = r0[r10]
            int r3 = r10 + 1
            r0 = r0[r3]
            q.b.a.ZoneOffset[] r3 = r9.f3161f
            int r10 = r10 / 2
            r4 = r3[r10]
            int r10 = r10 + r2
            r10 = r3[r10]
            int r2 = r10.c
            int r3 = r4.c
            if (r2 <= r3) goto L_0x00e2
            q.b.a.w.ZoneOffsetTransition r0 = new q.b.a.w.ZoneOffsetTransition
            r0.<init>(r1, r4, r10)
            return r0
        L_0x00e2:
            q.b.a.w.ZoneOffsetTransition r1 = new q.b.a.w.ZoneOffsetTransition
            r1.<init>(r0, r4, r10)
            return r1
        L_0x00e8:
            q.b.a.ZoneOffset[] r0 = r9.f3161f
            int r10 = r10 / 2
            int r10 = r10 + r2
            r10 = r0[r10]
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: q.b.a.w.StandardZoneRules.c(q.b.a.LocalDateTime):java.lang.Object");
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof StandardZoneRules) {
            StandardZoneRules standardZoneRules = (StandardZoneRules) obj;
            if (!Arrays.equals(this.b, standardZoneRules.b) || !Arrays.equals(this.c, standardZoneRules.c) || !Arrays.equals(this.d, standardZoneRules.d) || !Arrays.equals(this.f3161f, standardZoneRules.f3161f) || !Arrays.equals(this.g, standardZoneRules.g)) {
                return false;
            }
            return true;
        }
        if ((obj instanceof ZoneRules.a) && a()) {
            ZoneOffset a = a(Instant.d);
            Instant instant = Instant.d;
            if (a.equals(((ZoneRules.a) obj).b)) {
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        return (((Arrays.hashCode(this.b) ^ Arrays.hashCode(this.c)) ^ Arrays.hashCode(this.d)) ^ Arrays.hashCode(this.f3161f)) ^ Arrays.hashCode(this.g);
    }

    public String toString() {
        StringBuilder a = outline.a("StandardZoneRules[currentStandardOffset=");
        ZoneOffset[] zoneOffsetArr = this.c;
        a.append(zoneOffsetArr[zoneOffsetArr.length - 1]);
        a.append("]");
        return a.toString();
    }

    public ZoneOffsetTransition a(LocalDateTime localDateTime) {
        Object c2 = c(localDateTime);
        if (c2 instanceof ZoneOffsetTransition) {
            return (ZoneOffsetTransition) c2;
        }
        return null;
    }

    public boolean a(LocalDateTime localDateTime, ZoneOffset zoneOffset) {
        return b((e) localDateTime).contains(zoneOffset);
    }

    public final ZoneOffsetTransition[] a(int i2) {
        LocalDate localDate;
        Integer valueOf = Integer.valueOf(i2);
        ZoneOffsetTransition[] zoneOffsetTransitionArr = this.h.get(valueOf);
        if (zoneOffsetTransitionArr != null) {
            return zoneOffsetTransitionArr;
        }
        ZoneOffsetTransitionRule[] zoneOffsetTransitionRuleArr = this.g;
        ZoneOffsetTransition[] zoneOffsetTransitionArr2 = new ZoneOffsetTransition[zoneOffsetTransitionRuleArr.length];
        for (int i3 = 0; i3 < zoneOffsetTransitionRuleArr.length; i3++) {
            ZoneOffsetTransitionRule zoneOffsetTransitionRule = zoneOffsetTransitionRuleArr[i3];
            byte b2 = zoneOffsetTransitionRule.c;
            if (b2 < 0) {
                Month month = zoneOffsetTransitionRule.b;
                localDate = LocalDate.b(i2, month, month.b(IsoChronology.d.a((long) i2)) + 1 + zoneOffsetTransitionRule.c);
                DayOfWeek dayOfWeek = zoneOffsetTransitionRule.d;
                if (dayOfWeek != null) {
                    localDate = localDate.a((TemporalAdjuster) new TemporalAdjusters0(1, dayOfWeek, null));
                }
            } else {
                localDate = LocalDate.b(i2, zoneOffsetTransitionRule.b, b2);
                DayOfWeek dayOfWeek2 = zoneOffsetTransitionRule.d;
                if (dayOfWeek2 != null) {
                    localDate = localDate.a(n.i.Collections.a(dayOfWeek2));
                }
            }
            LocalDateTime b3 = LocalDateTime.b(localDate.a((long) zoneOffsetTransitionRule.f3163f), zoneOffsetTransitionRule.f3162e);
            ZoneOffsetTransitionRule.a aVar = zoneOffsetTransitionRule.g;
            ZoneOffset zoneOffset = zoneOffsetTransitionRule.h;
            ZoneOffset zoneOffset2 = zoneOffsetTransitionRule.f3164i;
            int ordinal = aVar.ordinal();
            if (ordinal == 0) {
                b3 = b3.c((long) (zoneOffset2.c - ZoneOffset.g.c));
            } else if (ordinal == 2) {
                b3 = b3.c((long) (zoneOffset2.c - zoneOffset.c));
            }
            zoneOffsetTransitionArr2[i3] = new ZoneOffsetTransition(b3, zoneOffsetTransitionRule.f3164i, zoneOffsetTransitionRule.f3165j);
        }
        if (i2 < 2100) {
            this.h.putIfAbsent(valueOf, zoneOffsetTransitionArr2);
        }
        return zoneOffsetTransitionArr2;
    }

    public boolean b(Instant instant) {
        int binarySearch = Arrays.binarySearch(this.b, instant.b);
        if (binarySearch < 0) {
            binarySearch = (-binarySearch) - 2;
        }
        return !this.c[binarySearch + 1].equals(a(instant));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.b(long, long):long
     arg types: [long, int]
     candidates:
      n.i.Collections.b(int, int):int
      n.i.Collections.b(long, int):long
      n.i.Collections.b(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.b(long, long):long */
    public ZoneOffset a(Instant instant) {
        long j2 = instant.b;
        if (this.g.length > 0) {
            long[] jArr = this.d;
            if (j2 > jArr[jArr.length - 1]) {
                ZoneOffset[] zoneOffsetArr = this.f3161f;
                ZoneOffsetTransition[] a = a(LocalDate.e(n.i.Collections.b(((long) zoneOffsetArr[zoneOffsetArr.length - 1].c) + j2, 86400L)).b);
                ZoneOffsetTransition zoneOffsetTransition = null;
                for (int i2 = 0; i2 < a.length; i2++) {
                    zoneOffsetTransition = a[i2];
                    if (j2 < zoneOffsetTransition.b.a(zoneOffsetTransition.c)) {
                        return zoneOffsetTransition.c;
                    }
                }
                return zoneOffsetTransition.d;
            }
        }
        int binarySearch = Arrays.binarySearch(this.d, j2);
        if (binarySearch < 0) {
            binarySearch = (-binarySearch) - 2;
        }
        return this.f3161f[binarySearch + 1];
    }
}
