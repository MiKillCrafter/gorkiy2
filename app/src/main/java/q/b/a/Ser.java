package q.b.a;

import java.io.DataInput;
import java.io.Externalizable;
import java.io.InvalidClassException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.StreamCorruptedException;
import n.i.Collections;

public final class Ser implements Externalizable {
    public byte b;
    public Object c;

    public Ser() {
    }

    public static Object a(DataInput dataInput) {
        return a(dataInput.readByte(), dataInput);
    }

    private Object readResolve() {
        return this.c;
    }

    public void readExternal(ObjectInput objectInput) {
        byte readByte = objectInput.readByte();
        this.b = readByte;
        this.c = a(readByte, objectInput);
    }

    public void writeExternal(ObjectOutput objectOutput) {
        byte b2 = this.b;
        Object obj = this.c;
        objectOutput.writeByte(b2);
        if (b2 != 64) {
            switch (b2) {
                case 1:
                    Duration duration = (Duration) obj;
                    objectOutput.writeLong(duration.b);
                    objectOutput.writeInt(duration.c);
                    return;
                case 2:
                    Instant instant = (Instant) obj;
                    objectOutput.writeLong(instant.b);
                    objectOutput.writeInt(instant.c);
                    return;
                case 3:
                    LocalDate localDate = (LocalDate) obj;
                    objectOutput.writeInt(localDate.b);
                    objectOutput.writeByte(localDate.c);
                    objectOutput.writeByte(localDate.d);
                    return;
                case 4:
                    ((LocalDateTime) obj).a(objectOutput);
                    return;
                case 5:
                    ((LocalTime) obj).a(objectOutput);
                    return;
                case 6:
                    ZonedDateTime zonedDateTime = (ZonedDateTime) obj;
                    zonedDateTime.b.a(objectOutput);
                    zonedDateTime.c.b(objectOutput);
                    zonedDateTime.d.a(objectOutput);
                    return;
                case 7:
                    objectOutput.writeUTF(((ZoneRegion) obj).c);
                    return;
                case 8:
                    ((ZoneOffset) obj).b(objectOutput);
                    return;
                default:
                    switch (b2) {
                        case 66:
                            OffsetTime offsetTime = (OffsetTime) obj;
                            offsetTime.b.a(objectOutput);
                            offsetTime.c.b(objectOutput);
                            return;
                        case 67:
                            objectOutput.writeInt(((Year) obj).b);
                            return;
                        case 68:
                            YearMonth yearMonth = (YearMonth) obj;
                            objectOutput.writeInt(yearMonth.b);
                            objectOutput.writeByte(yearMonth.c);
                            return;
                        case 69:
                            OffsetDateTime offsetDateTime = (OffsetDateTime) obj;
                            offsetDateTime.b.a(objectOutput);
                            offsetDateTime.c.b(objectOutput);
                            return;
                        default:
                            throw new InvalidClassException("Unknown serialized type");
                    }
            }
        } else {
            MonthDay monthDay = (MonthDay) obj;
            objectOutput.writeByte(monthDay.b);
            objectOutput.writeByte(monthDay.c);
        }
    }

    public Ser(byte b2, Object obj) {
        this.b = b2;
        this.c = obj;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.LocalDateTime, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.ZoneOffset, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.ZoneId, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public static Object a(byte b2, DataInput dataInput) {
        if (b2 == 64) {
            return MonthDay.a(dataInput);
        }
        switch (b2) {
            case 1:
                return Duration.a(dataInput);
            case 2:
                return Instant.a(dataInput);
            case 3:
                return LocalDate.a(dataInput);
            case 4:
                return LocalDateTime.a(dataInput);
            case 5:
                return LocalTime.a(dataInput);
            case 6:
                LocalDateTime a = LocalDateTime.a(dataInput);
                ZoneOffset a2 = ZoneOffset.a(dataInput);
                ZoneId zoneId = (ZoneId) a(dataInput);
                Collections.a((Object) a, "localDateTime");
                Collections.a((Object) a2, "offset");
                Collections.a((Object) zoneId, "zone");
                if (!(zoneId instanceof ZoneOffset) || a2.equals(zoneId)) {
                    return new ZonedDateTime(a, a2, zoneId);
                }
                throw new IllegalArgumentException("ZoneId must match ZoneOffset");
            case 7:
                return ZoneRegion.a(dataInput);
            case 8:
                return ZoneOffset.a(dataInput);
            default:
                switch (b2) {
                    case 66:
                        return OffsetTime.a(dataInput);
                    case 67:
                        return Year.a(dataInput);
                    case 68:
                        return YearMonth.a(dataInput);
                    case 69:
                        return OffsetDateTime.a(dataInput);
                    default:
                        throw new StreamCorruptedException("Unknown serialized type");
                }
        }
    }
}
