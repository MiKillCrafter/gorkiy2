package q.b.a;

import java.io.DataInput;
import java.io.InvalidObjectException;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.regex.Pattern;
import n.i.Collections;
import q.b.a.v.ChronoUnit;
import q.b.a.v.Temporal;
import q.b.a.v.i;

public final class Duration implements i, Comparable<b>, Serializable {
    public static final Duration d = new Duration(0, 0);
    public final long b;
    public final int c;

    static {
        BigInteger.valueOf(1000000000);
        Pattern.compile("([-+]?)P(?:([-+]?[0-9]+)D)?(T(?:([-+]?[0-9]+)H)?(?:([-+]?[0-9]+)M)?(?:([-+]?[0-9]+)(?:[.,]([0-9]{0,9}))?S)?)?", 2);
    }

    public Duration(long j2, int i2) {
        this.b = j2;
        this.c = i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.b(long, long):long
     arg types: [long, int]
     candidates:
      n.i.Collections.b(int, int):int
      n.i.Collections.b(long, int):long
      n.i.Collections.b(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.b(long, long):long */
    public static Duration a(long j2, long j3) {
        return a(Collections.d(j2, Collections.b(j3, 1000000000L)), Collections.a(j3, 1000000000));
    }

    public static Duration b(long j2) {
        return a(j2, 0);
    }

    private Object readResolve() {
        throw new InvalidObjectException("Deserialization via serialization delegate");
    }

    private Object writeReplace() {
        return new Ser((byte) 1, this);
    }

    public int compareTo(Object obj) {
        Duration duration = (Duration) obj;
        int a = Collections.a(this.b, duration.b);
        return a != 0 ? a : this.c - duration.c;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Duration)) {
            return false;
        }
        Duration duration = (Duration) obj;
        if (this.b == duration.b && this.c == duration.c) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        long j2 = this.b;
        return (this.c * 51) + ((int) (j2 ^ (j2 >>> 32)));
    }

    public String toString() {
        if (this == d) {
            return "PT0S";
        }
        long j2 = this.b;
        long j3 = j2 / 3600;
        int i2 = (int) ((j2 % 3600) / 60);
        int i3 = (int) (j2 % 60);
        StringBuilder sb = new StringBuilder(24);
        sb.append("PT");
        if (j3 != 0) {
            sb.append(j3);
            sb.append('H');
        }
        if (i2 != 0) {
            sb.append(i2);
            sb.append('M');
        }
        if (i3 == 0 && this.c == 0 && sb.length() > 2) {
            return sb.toString();
        }
        if (i3 >= 0 || this.c <= 0) {
            sb.append(i3);
        } else if (i3 == -1) {
            sb.append("-0");
        } else {
            sb.append(i3 + 1);
        }
        if (this.c > 0) {
            int length = sb.length();
            if (i3 < 0) {
                sb.append(2000000000 - this.c);
            } else {
                sb.append(this.c + 1000000000);
            }
            while (sb.charAt(sb.length() - 1) == '0') {
                sb.setLength(sb.length() - 1);
            }
            sb.setCharAt(length, '.');
        }
        sb.append('S');
        return sb.toString();
    }

    public static Duration a(long j2) {
        long j3 = j2 / 1000000000;
        int i2 = (int) (j2 % 1000000000);
        if (i2 < 0) {
            i2 += 1000000000;
            j3--;
        }
        return a(j3, i2);
    }

    public static Duration a(long j2, int i2) {
        if ((((long) i2) | j2) == 0) {
            return d;
        }
        return new Duration(j2, i2);
    }

    /* JADX WARN: Type inference failed for: r2v1, types: [q.b.a.v.ChronoUnit, q.b.a.v.TemporalUnit] */
    /* JADX WARN: Type inference failed for: r2v2, types: [q.b.a.v.ChronoUnit, q.b.a.v.TemporalUnit] */
    public Temporal a(Temporal temporal) {
        long j2 = this.b;
        if (j2 != 0) {
            temporal = temporal.b(j2, ChronoUnit.SECONDS);
        }
        int i2 = this.c;
        return i2 != 0 ? temporal.b((long) i2, ChronoUnit.NANOS) : temporal;
    }

    public static Duration a(DataInput dataInput) {
        return a(dataInput.readLong(), (long) dataInput.readInt());
    }
}
