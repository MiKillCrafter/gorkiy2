package q.b.a;

import androidx.recyclerview.widget.RecyclerView;
import java.io.DataInput;
import java.io.InvalidObjectException;
import java.io.Serializable;
import n.i.Collections;
import q.b.a.u.c;
import q.b.a.v.ChronoField;
import q.b.a.v.ChronoUnit;
import q.b.a.v.Temporal;
import q.b.a.v.TemporalAdjuster;
import q.b.a.v.TemporalField;
import q.b.a.v.TemporalQueries;
import q.b.a.v.TemporalQuery;
import q.b.a.v.TemporalUnit;
import q.b.a.v.ValueRange;
import q.b.a.v.d;
import q.b.a.v.f;

public final class OffsetTime extends c implements d, f, Comparable<j>, Serializable {
    public final LocalTime b;
    public final ZoneOffset c;

    static {
        LocalTime localTime = LocalTime.f3092f;
        ZoneOffset zoneOffset = ZoneOffset.f3098i;
        if (localTime != null) {
            new OffsetTime(localTime, zoneOffset);
            LocalTime localTime2 = LocalTime.g;
            ZoneOffset zoneOffset2 = ZoneOffset.h;
            if (localTime2 != null) {
                new OffsetTime(localTime2, zoneOffset2);
                return;
            }
            throw null;
        }
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.LocalTime, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.ZoneOffset, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public OffsetTime(LocalTime localTime, ZoneOffset zoneOffset) {
        Collections.a((Object) localTime, "time");
        this.b = localTime;
        Collections.a((Object) zoneOffset, "offset");
        this.c = zoneOffset;
    }

    private Object readResolve() {
        throw new InvalidObjectException("Deserialization via serialization delegate");
    }

    private Object writeReplace() {
        return new Ser((byte) 66, this);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.OffsetTime, q.b.a.u.DefaultInterfaceTemporalAccessor] */
    public <R> R a(TemporalQuery temporalQuery) {
        if (temporalQuery == TemporalQueries.c) {
            return ChronoUnit.NANOS;
        }
        if (temporalQuery == TemporalQueries.f3150e || temporalQuery == TemporalQueries.d) {
            return this.c;
        }
        if (temporalQuery == TemporalQueries.g) {
            return this.b;
        }
        if (temporalQuery == TemporalQueries.b || temporalQuery == TemporalQueries.f3151f || temporalQuery == TemporalQueries.a) {
            return null;
        }
        return OffsetTime.super.a(temporalQuery);
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [q.b.a.v.TemporalAccessor, q.b.a.OffsetTime] */
    public boolean c(TemporalField temporalField) {
        if (temporalField instanceof ChronoField) {
            if (temporalField.h() || temporalField == ChronoField.OFFSET_SECONDS) {
                return true;
            }
            return false;
        } else if (temporalField == null || !temporalField.a(this)) {
            return false;
        } else {
            return true;
        }
    }

    public int compareTo(Object obj) {
        OffsetTime offsetTime = (OffsetTime) obj;
        if (this.c.equals(offsetTime.c)) {
            return this.b.compareTo(offsetTime.b);
        }
        int a = Collections.a(this.b.i() - (((long) this.c.c) * 1000000000), offsetTime.b.i() - (((long) offsetTime.c.c) * 1000000000));
        return a == 0 ? this.b.compareTo(offsetTime.b) : a;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [q.b.a.v.TemporalAccessor, q.b.a.OffsetTime] */
    public long d(TemporalField temporalField) {
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.b(this);
        }
        if (temporalField == ChronoField.OFFSET_SECONDS) {
            return (long) this.c.c;
        }
        return this.b.d(temporalField);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof OffsetTime)) {
            return false;
        }
        OffsetTime offsetTime = (OffsetTime) obj;
        if (!this.b.equals(offsetTime.b) || !this.c.equals(offsetTime.c)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return this.b.hashCode() ^ this.c.c;
    }

    public String toString() {
        return this.b.toString() + this.c.d;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.OffsetTime, q.b.a.u.DefaultInterfaceTemporalAccessor] */
    public int b(TemporalField temporalField) {
        return OffsetTime.super.b(temporalField);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.LocalTime.b(long, q.b.a.v.TemporalUnit):q.b.a.LocalTime
     arg types: [long, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.LocalTime.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.LocalTime.b(long, q.b.a.v.TemporalUnit):q.b.a.LocalTime */
    public OffsetTime b(long j2, TemporalUnit temporalUnit) {
        if (temporalUnit instanceof ChronoUnit) {
            return a(this.b.b(j2, temporalUnit), this.c);
        }
        return (OffsetTime) temporalUnit.a(this, j2);
    }

    public final OffsetTime a(LocalTime localTime, ZoneOffset zoneOffset) {
        if (this.b != localTime || !this.c.equals(zoneOffset)) {
            return new OffsetTime(localTime, zoneOffset);
        }
        return this;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.v.TemporalAccessor, q.b.a.OffsetTime] */
    public ValueRange a(TemporalField temporalField) {
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.c(this);
        }
        if (temporalField == ChronoField.OFFSET_SECONDS) {
            return temporalField.g();
        }
        return this.b.a(temporalField);
    }

    /* JADX WARN: Type inference failed for: r2v2, types: [q.b.a.OffsetTime, q.b.a.v.Temporal] */
    /* JADX WARN: Type inference failed for: r2v3, types: [q.b.a.OffsetTime, q.b.a.v.Temporal] */
    /* JADX WARN: Type inference failed for: r2v5, types: [q.b.a.OffsetTime, q.b.a.v.Temporal] */
    /* JADX WARN: Type inference failed for: r2v7, types: [q.b.a.OffsetTime, q.b.a.v.Temporal] */
    public Temporal a(TemporalAdjuster temporalAdjuster) {
        if (temporalAdjuster instanceof LocalTime) {
            return a((LocalTime) temporalAdjuster, this.c);
        }
        if (temporalAdjuster instanceof ZoneOffset) {
            return a(this.b, (ZoneOffset) temporalAdjuster);
        }
        if (temporalAdjuster instanceof OffsetTime) {
            return (OffsetTime) temporalAdjuster;
        }
        return (OffsetTime) temporalAdjuster.a(this);
    }

    /* JADX WARN: Type inference failed for: r3v2, types: [q.b.a.OffsetTime, q.b.a.v.Temporal] */
    /* JADX WARN: Type inference failed for: r3v4, types: [q.b.a.OffsetTime, q.b.a.v.Temporal] */
    /* JADX WARN: Type inference failed for: r3v5, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r3v8, types: [q.b.a.OffsetTime, q.b.a.v.Temporal] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.LocalTime.a(q.b.a.v.TemporalField, long):q.b.a.LocalTime
     arg types: [q.b.a.v.TemporalField, long]
     candidates:
      q.b.a.LocalTime.a(int, int):q.b.a.LocalTime
      q.b.a.LocalTime.a(long, int):q.b.a.LocalTime
      q.b.a.LocalTime.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.LocalTime.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.LocalTime.a(q.b.a.v.TemporalField, long):q.b.a.LocalTime */
    public Temporal a(TemporalField temporalField, long j2) {
        if (!(temporalField instanceof ChronoField)) {
            return (OffsetTime) temporalField.a(this, j2);
        }
        if (temporalField != ChronoField.OFFSET_SECONDS) {
            return a(this.b.a(temporalField, j2), this.c);
        }
        ? r3 = (ChronoField) temporalField;
        return a(this.b, ZoneOffset.a(r3.range.a(j2, (TemporalField) r3)));
    }

    /* JADX WARN: Type inference failed for: r4v2, types: [q.b.a.OffsetTime, q.b.a.v.Temporal] */
    /* JADX WARN: Type inference failed for: r4v5, types: [q.b.a.OffsetTime, q.b.a.v.Temporal] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.OffsetTime.b(long, q.b.a.v.TemporalUnit):q.b.a.OffsetTime
     arg types: [int, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.OffsetTime.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.OffsetTime.b(long, q.b.a.v.TemporalUnit):q.b.a.OffsetTime */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.OffsetTime.b(long, q.b.a.v.TemporalUnit):q.b.a.OffsetTime
     arg types: [?, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.OffsetTime.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.OffsetTime.b(long, q.b.a.v.TemporalUnit):q.b.a.OffsetTime */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.OffsetTime.b(long, q.b.a.v.TemporalUnit):q.b.a.OffsetTime
     arg types: [long, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.OffsetTime.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.OffsetTime.b(long, q.b.a.v.TemporalUnit):q.b.a.OffsetTime */
    public Temporal a(long j2, TemporalUnit temporalUnit) {
        return j2 == Long.MIN_VALUE ? b((long) RecyclerView.FOREVER_NS, temporalUnit).b(1L, temporalUnit) : b(-j2, temporalUnit);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v1, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public Temporal a(Temporal temporal) {
        return temporal.a((TemporalField) ChronoField.NANO_OF_DAY, this.b.i()).a((TemporalField) ChronoField.OFFSET_SECONDS, (long) this.c.c);
    }

    public static OffsetTime a(DataInput dataInput) {
        return new OffsetTime(LocalTime.a(dataInput), ZoneOffset.a(dataInput));
    }
}
