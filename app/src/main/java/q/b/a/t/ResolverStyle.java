package q.b.a.t;

public enum ResolverStyle {
    STRICT,
    SMART,
    LENIENT
}
