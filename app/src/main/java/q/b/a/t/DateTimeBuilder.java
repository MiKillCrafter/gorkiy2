package q.b.a.t;

import com.crashlytics.android.answers.RetryManager;
import j.a.a.a.outline;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import n.i.Collections;
import org.threeten.bp.DateTimeException;
import q.b.a.DayOfWeek;
import q.b.a.Instant;
import q.b.a.LocalDate;
import q.b.a.LocalTime;
import q.b.a.Month;
import q.b.a.Period;
import q.b.a.Year;
import q.b.a.ZoneId;
import q.b.a.ZoneOffset;
import q.b.a.s.ChronoLocalDate;
import q.b.a.s.ChronoZonedDateTime;
import q.b.a.s.Chronology;
import q.b.a.s.IsoChronology;
import q.b.a.u.DefaultInterfaceTemporalAccessor;
import q.b.a.v.ChronoField;
import q.b.a.v.TemporalAccessor;
import q.b.a.v.TemporalField;
import q.b.a.v.TemporalQueries;
import q.b.a.v.TemporalQuery;
import q.b.a.v.a;
import q.b.a.v.j;

public final class DateTimeBuilder extends DefaultInterfaceTemporalAccessor implements TemporalAccessor, Cloneable {
    public final Map<j, Long> b = new HashMap();
    public Chronology c;
    public ZoneId d;

    /* renamed from: e  reason: collision with root package name */
    public ChronoLocalDate f3124e;

    /* renamed from: f  reason: collision with root package name */
    public LocalTime f3125f;
    public boolean g;
    public Period h;

    /* JADX WARN: Type inference failed for: r3v4, types: [q.b.a.v.TemporalAccessor, q.b.a.s.ChronoLocalDate] */
    public <R> R a(TemporalQuery<R> temporalQuery) {
        if (temporalQuery == TemporalQueries.a) {
            return this.d;
        }
        if (temporalQuery == TemporalQueries.b) {
            return this.c;
        }
        if (temporalQuery == TemporalQueries.f3151f) {
            ? r3 = this.f3124e;
            if (r3 != 0) {
                return LocalDate.a((TemporalAccessor) r3);
            }
            return null;
        } else if (temporalQuery == TemporalQueries.g) {
            return this.f3125f;
        } else {
            if (temporalQuery == TemporalQueries.d || temporalQuery == TemporalQueries.f3150e) {
                return temporalQuery.a(this);
            }
            if (temporalQuery == TemporalQueries.c) {
                return null;
            }
            return temporalQuery.a(this);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.v.TemporalField, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public DateTimeBuilder b(TemporalField temporalField, long j2) {
        Collections.a((Object) temporalField, "field");
        Long l2 = this.b.get(temporalField);
        if (l2 == null || l2.longValue() == j2) {
            this.b.put(temporalField, Long.valueOf(j2));
            return this;
        }
        throw new DateTimeException("Conflict found: " + temporalField + " " + l2 + " differs from " + temporalField + " " + j2 + ": " + this);
    }

    public boolean c(TemporalField temporalField) {
        ChronoLocalDate chronoLocalDate;
        LocalTime localTime;
        if (temporalField == null) {
            return false;
        }
        if (this.b.containsKey(temporalField) || (((chronoLocalDate = this.f3124e) != null && chronoLocalDate.c(temporalField)) || ((localTime = this.f3125f) != null && localTime.c(temporalField)))) {
            return true;
        }
        return false;
    }

    /* JADX WARN: Type inference failed for: r0v12, types: [q.b.a.v.TemporalAccessor, q.b.a.s.ChronoLocalDate] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.v.TemporalField, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public long d(TemporalField temporalField) {
        Collections.a((Object) temporalField, "field");
        Long l2 = this.b.get(temporalField);
        if (l2 != null) {
            return l2.longValue();
        }
        ChronoLocalDate chronoLocalDate = this.f3124e;
        if (chronoLocalDate != null && chronoLocalDate.c(temporalField)) {
            return this.f3124e.d(temporalField);
        }
        LocalTime localTime = this.f3125f;
        if (localTime != null && localTime.c(temporalField)) {
            return this.f3125f.d(temporalField);
        }
        throw new DateTimeException(outline.a("Field not found: ", temporalField));
    }

    /* JADX WARN: Type inference failed for: r0v7, types: [q.b.a.ZoneId, q.b.a.ZoneOffset] */
    public final void i() {
        if (this.b.containsKey(ChronoField.INSTANT_SECONDS)) {
            ZoneId zoneId = this.d;
            if (zoneId != null) {
                a(zoneId);
                return;
            }
            Long l2 = this.b.get(ChronoField.OFFSET_SECONDS);
            if (l2 != null) {
                a((ZoneId) ZoneOffset.a(l2.intValue()));
            }
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("DateTimeBuilder[");
        if (this.b.size() > 0) {
            sb.append("fields=");
            sb.append(this.b);
        }
        sb.append(", ");
        sb.append(this.c);
        sb.append(", ");
        sb.append(this.d);
        sb.append(", ");
        sb.append(this.f3124e);
        sb.append(", ");
        sb.append(this.f3125f);
        sb.append(']');
        return sb.toString();
    }

    /* JADX WARN: Type inference failed for: r12v14, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r12v18, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r12v24, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r12v31, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r12v41, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r12v46, types: [java.lang.Object, q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r12v47, types: [java.lang.Object, q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v50, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v51, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v52, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v57, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v58, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v59, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v60, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v65, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v66, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v67, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v72, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v73, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v74, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r2v1, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r2v2, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r2v3, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r2v7, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v92, types: [java.lang.Object, q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v93, types: [java.lang.Object, q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v98, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v100, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v106, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v108, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public final void b(ResolverStyle resolverStyle) {
        long j2 = 0;
        if (this.b.containsKey(ChronoField.CLOCK_HOUR_OF_DAY)) {
            long longValue = this.b.remove(ChronoField.CLOCK_HOUR_OF_DAY).longValue();
            if (!(resolverStyle == ResolverStyle.LENIENT || (resolverStyle == ResolverStyle.SMART && longValue == 0))) {
                ? r0 = ChronoField.CLOCK_HOUR_OF_DAY;
                r0.range.b(longValue, r0);
            }
            ? r02 = ChronoField.HOUR_OF_DAY;
            if (longValue == 24) {
                longValue = 0;
            }
            b(r02, longValue);
        }
        if (this.b.containsKey(ChronoField.CLOCK_HOUR_OF_AMPM)) {
            long longValue2 = this.b.remove(ChronoField.CLOCK_HOUR_OF_AMPM).longValue();
            if (!(resolverStyle == ResolverStyle.LENIENT || (resolverStyle == ResolverStyle.SMART && longValue2 == 0))) {
                ? r03 = ChronoField.CLOCK_HOUR_OF_AMPM;
                r03.range.b(longValue2, r03);
            }
            ? r04 = ChronoField.HOUR_OF_AMPM;
            if (longValue2 != 12) {
                j2 = longValue2;
            }
            b(r04, j2);
        }
        if (resolverStyle != ResolverStyle.LENIENT) {
            if (this.b.containsKey(ChronoField.AMPM_OF_DAY)) {
                ? r05 = ChronoField.AMPM_OF_DAY;
                r05.range.b(this.b.get(r05).longValue(), r05);
            }
            if (this.b.containsKey(ChronoField.HOUR_OF_AMPM)) {
                ? r06 = ChronoField.HOUR_OF_AMPM;
                r06.range.b(this.b.get(r06).longValue(), r06);
            }
        }
        if (this.b.containsKey(ChronoField.AMPM_OF_DAY) && this.b.containsKey(ChronoField.HOUR_OF_AMPM)) {
            b(ChronoField.HOUR_OF_DAY, (this.b.remove(ChronoField.AMPM_OF_DAY).longValue() * 12) + this.b.remove(ChronoField.HOUR_OF_AMPM).longValue());
        }
        if (this.b.containsKey(ChronoField.NANO_OF_DAY)) {
            long longValue3 = this.b.remove(ChronoField.NANO_OF_DAY).longValue();
            if (resolverStyle != ResolverStyle.LENIENT) {
                ? r2 = ChronoField.NANO_OF_DAY;
                r2.range.b(longValue3, r2);
            }
            b(ChronoField.SECOND_OF_DAY, longValue3 / 1000000000);
            b(ChronoField.NANO_OF_SECOND, longValue3 % 1000000000);
        }
        if (this.b.containsKey(ChronoField.MICRO_OF_DAY)) {
            long longValue4 = this.b.remove(ChronoField.MICRO_OF_DAY).longValue();
            if (resolverStyle != ResolverStyle.LENIENT) {
                ? r07 = ChronoField.MICRO_OF_DAY;
                r07.range.b(longValue4, r07);
            }
            b(ChronoField.SECOND_OF_DAY, longValue4 / RetryManager.NANOSECONDS_IN_MS);
            b(ChronoField.MICRO_OF_SECOND, longValue4 % RetryManager.NANOSECONDS_IN_MS);
        }
        if (this.b.containsKey(ChronoField.MILLI_OF_DAY)) {
            long longValue5 = this.b.remove(ChronoField.MILLI_OF_DAY).longValue();
            if (resolverStyle != ResolverStyle.LENIENT) {
                ? r08 = ChronoField.MILLI_OF_DAY;
                r08.range.b(longValue5, r08);
            }
            b(ChronoField.SECOND_OF_DAY, longValue5 / 1000);
            b(ChronoField.MILLI_OF_SECOND, longValue5 % 1000);
        }
        if (this.b.containsKey(ChronoField.SECOND_OF_DAY)) {
            long longValue6 = this.b.remove(ChronoField.SECOND_OF_DAY).longValue();
            if (resolverStyle != ResolverStyle.LENIENT) {
                ? r09 = ChronoField.SECOND_OF_DAY;
                r09.range.b(longValue6, r09);
            }
            b(ChronoField.HOUR_OF_DAY, longValue6 / 3600);
            b(ChronoField.MINUTE_OF_HOUR, (longValue6 / 60) % 60);
            b(ChronoField.SECOND_OF_MINUTE, longValue6 % 60);
        }
        if (this.b.containsKey(ChronoField.MINUTE_OF_DAY)) {
            long longValue7 = this.b.remove(ChronoField.MINUTE_OF_DAY).longValue();
            if (resolverStyle != ResolverStyle.LENIENT) {
                ? r010 = ChronoField.MINUTE_OF_DAY;
                r010.range.b(longValue7, r010);
            }
            b(ChronoField.HOUR_OF_DAY, longValue7 / 60);
            b(ChronoField.MINUTE_OF_HOUR, longValue7 % 60);
        }
        if (resolverStyle != ResolverStyle.LENIENT) {
            if (this.b.containsKey(ChronoField.MILLI_OF_SECOND)) {
                ? r12 = ChronoField.MILLI_OF_SECOND;
                r12.range.b(this.b.get(r12).longValue(), r12);
            }
            if (this.b.containsKey(ChronoField.MICRO_OF_SECOND)) {
                ? r122 = ChronoField.MICRO_OF_SECOND;
                r122.range.b(this.b.get(r122).longValue(), r122);
            }
        }
        if (this.b.containsKey(ChronoField.MILLI_OF_SECOND) && this.b.containsKey(ChronoField.MICRO_OF_SECOND)) {
            b(ChronoField.MICRO_OF_SECOND, (this.b.get(ChronoField.MICRO_OF_SECOND).longValue() % 1000) + (this.b.remove(ChronoField.MILLI_OF_SECOND).longValue() * 1000));
        }
        if (this.b.containsKey(ChronoField.MICRO_OF_SECOND) && this.b.containsKey(ChronoField.NANO_OF_SECOND)) {
            b(ChronoField.MICRO_OF_SECOND, this.b.get(ChronoField.NANO_OF_SECOND).longValue() / 1000);
            this.b.remove(ChronoField.MICRO_OF_SECOND);
        }
        if (this.b.containsKey(ChronoField.MILLI_OF_SECOND) && this.b.containsKey(ChronoField.NANO_OF_SECOND)) {
            b(ChronoField.MILLI_OF_SECOND, this.b.get(ChronoField.NANO_OF_SECOND).longValue() / RetryManager.NANOSECONDS_IN_MS);
            this.b.remove(ChronoField.MILLI_OF_SECOND);
        }
        if (this.b.containsKey(ChronoField.MICRO_OF_SECOND)) {
            b(ChronoField.NANO_OF_SECOND, this.b.remove(ChronoField.MICRO_OF_SECOND).longValue() * 1000);
        } else if (this.b.containsKey(ChronoField.MILLI_OF_SECOND)) {
            b(ChronoField.NANO_OF_SECOND, this.b.remove(ChronoField.MILLI_OF_SECOND).longValue() * RetryManager.NANOSECONDS_IN_MS);
        }
    }

    public final void a(LocalDate localDate) {
        if (localDate != null) {
            this.f3124e = localDate;
            for (TemporalField next : this.b.keySet()) {
                if ((next instanceof ChronoField) && next.f()) {
                    try {
                        long d2 = localDate.d(next);
                        Long l2 = this.b.get(next);
                        if (d2 != l2.longValue()) {
                            throw new DateTimeException("Conflict found: Field " + next + " " + d2 + " differs from " + next + " " + l2 + " derived from " + localDate);
                        }
                    } catch (DateTimeException unused) {
                        continue;
                    }
                }
            }
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r14v6, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r13v18, types: [q.b.a.ZoneId, q.b.a.ZoneOffset] */
    /* JADX WARN: Type inference failed for: r14v9, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r13v44, types: [q.b.a.v.TemporalAccessor, q.b.a.s.ChronoLocalDate] */
    /* JADX WARN: Type inference failed for: r13v45, types: [q.b.a.v.TemporalAccessor, q.b.a.LocalTime] */
    /* JADX WARN: Type inference failed for: r13v46, types: [q.b.a.v.TemporalAccessor, q.b.a.s.ChronoLocalDateTime] */
    /* JADX WARN: Type inference failed for: r4v13, types: [q.b.a.s.ChronoLocalDateTime] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.b(long, long):long
     arg types: [long, int]
     candidates:
      n.i.Collections.b(int, int):int
      n.i.Collections.b(long, int):long
      n.i.Collections.b(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.b(long, long):long */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public q.b.a.t.a a(q.b.a.t.i r13, java.util.Set<q.b.a.v.j> r14) {
        /*
            r12 = this;
            if (r14 == 0) goto L_0x000b
            java.util.Map<q.b.a.v.j, java.lang.Long> r0 = r12.b
            java.util.Set r0 = r0.keySet()
            r0.retainAll(r14)
        L_0x000b:
            r12.i()
            r12.a(r13)
            r12.b(r13)
            r14 = 0
            r0 = 0
        L_0x0016:
            r1 = 100
            if (r0 >= r1) goto L_0x00b8
            java.util.Map<q.b.a.v.j, java.lang.Long> r2 = r12.b
            java.util.Set r2 = r2.entrySet()
            java.util.Iterator r2 = r2.iterator()
        L_0x0024:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x00b8
            java.lang.Object r3 = r2.next()
            java.util.Map$Entry r3 = (java.util.Map.Entry) r3
            java.lang.Object r3 = r3.getKey()
            q.b.a.v.TemporalField r3 = (q.b.a.v.TemporalField) r3
            java.util.Map<q.b.a.v.j, java.lang.Long> r4 = r12.b
            q.b.a.v.TemporalAccessor r4 = r3.a(r4, r12, r13)
            if (r4 == 0) goto L_0x00ac
            boolean r1 = r4 instanceof q.b.a.s.ChronoZonedDateTime
            if (r1 == 0) goto L_0x0073
            q.b.a.s.ChronoZonedDateTime r4 = (q.b.a.s.ChronoZonedDateTime) r4
            q.b.a.ZoneId r1 = r12.d
            if (r1 != 0) goto L_0x004f
            q.b.a.ZoneId r1 = r4.j()
            r12.d = r1
            goto L_0x0059
        L_0x004f:
            q.b.a.ZoneId r2 = r4.j()
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x005e
        L_0x0059:
            q.b.a.s.ChronoLocalDateTime r4 = r4.p()
            goto L_0x0073
        L_0x005e:
            org.threeten.bp.DateTimeException r13 = new org.threeten.bp.DateTimeException
            java.lang.String r14 = "ChronoZonedDateTime must use the effective parsed zone: "
            java.lang.StringBuilder r14 = j.a.a.a.outline.a(r14)
            q.b.a.ZoneId r0 = r12.d
            r14.append(r0)
            java.lang.String r14 = r14.toString()
            r13.<init>(r14)
            throw r13
        L_0x0073:
            boolean r1 = r4 instanceof q.b.a.s.ChronoLocalDate
            if (r1 == 0) goto L_0x007d
            q.b.a.s.ChronoLocalDate r4 = (q.b.a.s.ChronoLocalDate) r4
            r12.a(r3, r4)
            goto L_0x00b4
        L_0x007d:
            boolean r1 = r4 instanceof q.b.a.LocalTime
            if (r1 == 0) goto L_0x0087
            q.b.a.LocalTime r4 = (q.b.a.LocalTime) r4
            r12.a(r3, r4)
            goto L_0x00b4
        L_0x0087:
            boolean r1 = r4 instanceof q.b.a.s.ChronoLocalDateTime
            if (r1 == 0) goto L_0x009c
            q.b.a.s.ChronoLocalDateTime r4 = (q.b.a.s.ChronoLocalDateTime) r4
            q.b.a.s.ChronoLocalDate r1 = r4.j()
            r12.a(r3, r1)
            q.b.a.LocalTime r1 = r4.l()
            r12.a(r3, r1)
            goto L_0x00b4
        L_0x009c:
            org.threeten.bp.DateTimeException r13 = new org.threeten.bp.DateTimeException
            java.lang.String r14 = "Unknown type: "
            java.lang.StringBuilder r14 = j.a.a.a.outline.a(r14)
            java.lang.String r14 = j.a.a.a.outline.a(r4, r14)
            r13.<init>(r14)
            throw r13
        L_0x00ac:
            java.util.Map<q.b.a.v.j, java.lang.Long> r4 = r12.b
            boolean r3 = r4.containsKey(r3)
            if (r3 != 0) goto L_0x0024
        L_0x00b4:
            int r0 = r0 + 1
            goto L_0x0016
        L_0x00b8:
            if (r0 == r1) goto L_0x0379
            r1 = 1
            if (r0 <= 0) goto L_0x00bf
            r0 = 1
            goto L_0x00c0
        L_0x00bf:
            r0 = 0
        L_0x00c0:
            if (r0 == 0) goto L_0x00cb
            r12.i()
            r12.a(r13)
            r12.b(r13)
        L_0x00cb:
            java.util.Map<q.b.a.v.j, java.lang.Long> r0 = r12.b
            q.b.a.v.ChronoField r2 = q.b.a.v.ChronoField.HOUR_OF_DAY
            java.lang.Object r0 = r0.get(r2)
            java.lang.Long r0 = (java.lang.Long) r0
            java.util.Map<q.b.a.v.j, java.lang.Long> r2 = r12.b
            q.b.a.v.ChronoField r3 = q.b.a.v.ChronoField.MINUTE_OF_HOUR
            java.lang.Object r2 = r2.get(r3)
            java.lang.Long r2 = (java.lang.Long) r2
            java.util.Map<q.b.a.v.j, java.lang.Long> r3 = r12.b
            q.b.a.v.ChronoField r4 = q.b.a.v.ChronoField.SECOND_OF_MINUTE
            java.lang.Object r3 = r3.get(r4)
            java.lang.Long r3 = (java.lang.Long) r3
            java.util.Map<q.b.a.v.j, java.lang.Long> r4 = r12.b
            q.b.a.v.ChronoField r5 = q.b.a.v.ChronoField.NANO_OF_SECOND
            java.lang.Object r4 = r4.get(r5)
            java.lang.Long r4 = (java.lang.Long) r4
            r5 = 0
            if (r0 != 0) goto L_0x00f9
            goto L_0x0256
        L_0x00f9:
            if (r2 != 0) goto L_0x0101
            if (r3 != 0) goto L_0x0256
            if (r4 == 0) goto L_0x0101
            goto L_0x0256
        L_0x0101:
            if (r2 == 0) goto L_0x0109
            if (r3 != 0) goto L_0x0109
            if (r4 == 0) goto L_0x0109
            goto L_0x0256
        L_0x0109:
            q.b.a.t.ResolverStyle r7 = q.b.a.t.ResolverStyle.LENIENT
            r8 = 24
            if (r13 == r7) goto L_0x0197
            q.b.a.t.ResolverStyle r7 = q.b.a.t.ResolverStyle.SMART
            if (r13 != r7) goto L_0x0143
            long r10 = r0.longValue()
            int r13 = (r10 > r8 ? 1 : (r10 == r8 ? 0 : -1))
            if (r13 != 0) goto L_0x0143
            if (r2 == 0) goto L_0x0125
            long r7 = r2.longValue()
            int r13 = (r7 > r5 ? 1 : (r7 == r5 ? 0 : -1))
            if (r13 != 0) goto L_0x0143
        L_0x0125:
            if (r3 == 0) goto L_0x012f
            long r7 = r3.longValue()
            int r13 = (r7 > r5 ? 1 : (r7 == r5 ? 0 : -1))
            if (r13 != 0) goto L_0x0143
        L_0x012f:
            if (r4 == 0) goto L_0x0139
            long r7 = r4.longValue()
            int r13 = (r7 > r5 ? 1 : (r7 == r5 ? 0 : -1))
            if (r13 != 0) goto L_0x0143
        L_0x0139:
            java.lang.Long r0 = java.lang.Long.valueOf(r5)
            q.b.a.Period r13 = q.b.a.Period.a(r1)
            r12.h = r13
        L_0x0143:
            q.b.a.v.ChronoField r13 = q.b.a.v.ChronoField.HOUR_OF_DAY
            long r0 = r0.longValue()
            int r13 = r13.a(r0)
            if (r2 == 0) goto L_0x018b
            q.b.a.v.ChronoField r0 = q.b.a.v.ChronoField.MINUTE_OF_HOUR
            long r1 = r2.longValue()
            int r0 = r0.a(r1)
            if (r3 == 0) goto L_0x0181
            q.b.a.v.ChronoField r1 = q.b.a.v.ChronoField.SECOND_OF_MINUTE
            long r2 = r3.longValue()
            int r1 = r1.a(r2)
            if (r4 == 0) goto L_0x0179
            q.b.a.v.ChronoField r2 = q.b.a.v.ChronoField.NANO_OF_SECOND
            long r3 = r4.longValue()
            int r2 = r2.a(r3)
            q.b.a.LocalTime r13 = q.b.a.LocalTime.b(r13, r0, r1, r2)
            r12.f3125f = r13
            goto L_0x023a
        L_0x0179:
            q.b.a.LocalTime r13 = q.b.a.LocalTime.a(r13, r0, r1)
            r12.f3125f = r13
            goto L_0x023a
        L_0x0181:
            if (r4 != 0) goto L_0x023a
            q.b.a.LocalTime r13 = q.b.a.LocalTime.a(r13, r0)
            r12.f3125f = r13
            goto L_0x023a
        L_0x018b:
            if (r3 != 0) goto L_0x023a
            if (r4 != 0) goto L_0x023a
            q.b.a.LocalTime r13 = q.b.a.LocalTime.a(r13, r14)
            r12.f3125f = r13
            goto L_0x023a
        L_0x0197:
            long r0 = r0.longValue()
            if (r2 == 0) goto L_0x021e
            if (r3 == 0) goto L_0x01f1
            if (r4 != 0) goto L_0x01a5
            java.lang.Long r4 = java.lang.Long.valueOf(r5)
        L_0x01a5:
            r7 = 3600000000000(0x34630b8a000, double:1.7786363250285E-311)
            long r0 = n.i.Collections.e(r0, r7)
            long r7 = r2.longValue()
            r9 = 60000000000(0xdf8475800, double:2.96439387505E-313)
            long r7 = n.i.Collections.e(r7, r9)
            long r0 = n.i.Collections.d(r0, r7)
            long r2 = r3.longValue()
            r7 = 1000000000(0x3b9aca00, double:4.94065646E-315)
            long r2 = n.i.Collections.e(r2, r7)
            long r0 = n.i.Collections.d(r0, r2)
            long r2 = r4.longValue()
            long r0 = n.i.Collections.d(r0, r2)
            r2 = 86400000000000(0x4e94914f0000, double:4.26872718006837E-310)
            long r7 = n.i.Collections.b(r0, r2)
            int r13 = (int) r7
            long r0 = n.i.Collections.c(r0, r2)
            q.b.a.LocalTime r0 = q.b.a.LocalTime.e(r0)
            r12.f3125f = r0
            q.b.a.Period r13 = q.b.a.Period.a(r13)
            r12.h = r13
            goto L_0x023a
        L_0x01f1:
            r3 = 3600(0xe10, double:1.7786E-320)
            long r0 = n.i.Collections.e(r0, r3)
            long r2 = r2.longValue()
            r7 = 60
            long r2 = n.i.Collections.e(r2, r7)
            long r0 = n.i.Collections.d(r0, r2)
            r2 = 86400(0x15180, double:4.26873E-319)
            long r7 = n.i.Collections.b(r0, r2)
            int r13 = (int) r7
            long r0 = n.i.Collections.c(r0, r2)
            q.b.a.LocalTime r0 = q.b.a.LocalTime.f(r0)
            r12.f3125f = r0
            q.b.a.Period r13 = q.b.a.Period.a(r13)
            r12.h = r13
            goto L_0x023a
        L_0x021e:
            long r2 = n.i.Collections.b(r0, r8)
            int r13 = n.i.Collections.a(r2)
            r2 = 24
            int r0 = n.i.Collections.a(r0, r2)
            long r0 = (long) r0
            int r1 = (int) r0
            q.b.a.LocalTime r0 = q.b.a.LocalTime.a(r1, r14)
            r12.f3125f = r0
            q.b.a.Period r13 = q.b.a.Period.a(r13)
            r12.h = r13
        L_0x023a:
            java.util.Map<q.b.a.v.j, java.lang.Long> r13 = r12.b
            q.b.a.v.ChronoField r0 = q.b.a.v.ChronoField.HOUR_OF_DAY
            r13.remove(r0)
            java.util.Map<q.b.a.v.j, java.lang.Long> r13 = r12.b
            q.b.a.v.ChronoField r0 = q.b.a.v.ChronoField.MINUTE_OF_HOUR
            r13.remove(r0)
            java.util.Map<q.b.a.v.j, java.lang.Long> r13 = r12.b
            q.b.a.v.ChronoField r0 = q.b.a.v.ChronoField.SECOND_OF_MINUTE
            r13.remove(r0)
            java.util.Map<q.b.a.v.j, java.lang.Long> r13 = r12.b
            q.b.a.v.ChronoField r0 = q.b.a.v.ChronoField.NANO_OF_SECOND
            r13.remove(r0)
        L_0x0256:
            java.util.Map<q.b.a.v.j, java.lang.Long> r13 = r12.b
            int r13 = r13.size()
            if (r13 <= 0) goto L_0x027d
            q.b.a.s.ChronoLocalDate r13 = r12.f3124e
            if (r13 == 0) goto L_0x026e
            q.b.a.LocalTime r0 = r12.f3125f
            if (r0 == 0) goto L_0x026e
            q.b.a.s.ChronoLocalDateTime r13 = r13.a(r0)
            r12.a(r13)
            goto L_0x027d
        L_0x026e:
            q.b.a.s.ChronoLocalDate r13 = r12.f3124e
            if (r13 == 0) goto L_0x0276
            r12.a(r13)
            goto L_0x027d
        L_0x0276:
            q.b.a.LocalTime r13 = r12.f3125f
            if (r13 == 0) goto L_0x027d
            r12.a(r13)
        L_0x027d:
            q.b.a.Period r13 = r12.h
            if (r13 == 0) goto L_0x02a1
            if (r13 == 0) goto L_0x029f
            q.b.a.Period r0 = q.b.a.Period.f3095e
            if (r13 != r0) goto L_0x0288
            r14 = 1
        L_0x0288:
            if (r14 != 0) goto L_0x02a1
            q.b.a.s.ChronoLocalDate r13 = r12.f3124e
            if (r13 == 0) goto L_0x02a1
            q.b.a.LocalTime r14 = r12.f3125f
            if (r14 == 0) goto L_0x02a1
            q.b.a.Period r14 = r12.h
            q.b.a.s.ChronoLocalDate r13 = r13.a(r14)
            r12.f3124e = r13
            q.b.a.Period r13 = q.b.a.Period.f3095e
            r12.h = r13
            goto L_0x02a1
        L_0x029f:
            r13 = 0
            throw r13
        L_0x02a1:
            q.b.a.LocalTime r13 = r12.f3125f
            if (r13 != 0) goto L_0x031b
            java.util.Map<q.b.a.v.j, java.lang.Long> r13 = r12.b
            q.b.a.v.ChronoField r14 = q.b.a.v.ChronoField.INSTANT_SECONDS
            boolean r13 = r13.containsKey(r14)
            if (r13 != 0) goto L_0x02c3
            java.util.Map<q.b.a.v.j, java.lang.Long> r13 = r12.b
            q.b.a.v.ChronoField r14 = q.b.a.v.ChronoField.SECOND_OF_DAY
            boolean r13 = r13.containsKey(r14)
            if (r13 != 0) goto L_0x02c3
            java.util.Map<q.b.a.v.j, java.lang.Long> r13 = r12.b
            q.b.a.v.ChronoField r14 = q.b.a.v.ChronoField.SECOND_OF_MINUTE
            boolean r13 = r13.containsKey(r14)
            if (r13 == 0) goto L_0x031b
        L_0x02c3:
            java.util.Map<q.b.a.v.j, java.lang.Long> r13 = r12.b
            q.b.a.v.ChronoField r14 = q.b.a.v.ChronoField.NANO_OF_SECOND
            boolean r13 = r13.containsKey(r14)
            if (r13 == 0) goto L_0x02fa
            java.util.Map<q.b.a.v.j, java.lang.Long> r13 = r12.b
            q.b.a.v.ChronoField r14 = q.b.a.v.ChronoField.NANO_OF_SECOND
            java.lang.Object r13 = r13.get(r14)
            java.lang.Long r13 = (java.lang.Long) r13
            long r13 = r13.longValue()
            java.util.Map<q.b.a.v.j, java.lang.Long> r0 = r12.b
            q.b.a.v.ChronoField r1 = q.b.a.v.ChronoField.MICRO_OF_SECOND
            r2 = 1000(0x3e8, double:4.94E-321)
            long r2 = r13 / r2
            java.lang.Long r2 = java.lang.Long.valueOf(r2)
            r0.put(r1, r2)
            java.util.Map<q.b.a.v.j, java.lang.Long> r0 = r12.b
            q.b.a.v.ChronoField r1 = q.b.a.v.ChronoField.MILLI_OF_SECOND
            r2 = 1000000(0xf4240, double:4.940656E-318)
            long r13 = r13 / r2
            java.lang.Long r13 = java.lang.Long.valueOf(r13)
            r0.put(r1, r13)
            goto L_0x031b
        L_0x02fa:
            java.util.Map<q.b.a.v.j, java.lang.Long> r13 = r12.b
            q.b.a.v.ChronoField r14 = q.b.a.v.ChronoField.NANO_OF_SECOND
            java.lang.Long r0 = java.lang.Long.valueOf(r5)
            r13.put(r14, r0)
            java.util.Map<q.b.a.v.j, java.lang.Long> r13 = r12.b
            q.b.a.v.ChronoField r14 = q.b.a.v.ChronoField.MICRO_OF_SECOND
            java.lang.Long r0 = java.lang.Long.valueOf(r5)
            r13.put(r14, r0)
            java.util.Map<q.b.a.v.j, java.lang.Long> r13 = r12.b
            q.b.a.v.ChronoField r14 = q.b.a.v.ChronoField.MILLI_OF_SECOND
            java.lang.Long r0 = java.lang.Long.valueOf(r5)
            r13.put(r14, r0)
        L_0x031b:
            q.b.a.s.ChronoLocalDate r13 = r12.f3124e
            if (r13 == 0) goto L_0x0378
            q.b.a.LocalTime r13 = r12.f3125f
            if (r13 == 0) goto L_0x0378
            java.util.Map<q.b.a.v.j, java.lang.Long> r13 = r12.b
            q.b.a.v.ChronoField r14 = q.b.a.v.ChronoField.OFFSET_SECONDS
            java.lang.Object r13 = r13.get(r14)
            java.lang.Long r13 = (java.lang.Long) r13
            if (r13 == 0) goto L_0x0355
            int r13 = r13.intValue()
            q.b.a.ZoneOffset r13 = q.b.a.ZoneOffset.a(r13)
            q.b.a.s.ChronoLocalDate r14 = r12.f3124e
            q.b.a.LocalTime r0 = r12.f3125f
            q.b.a.s.ChronoLocalDateTime r14 = r14.a(r0)
            q.b.a.s.ChronoZonedDateTime r13 = r14.a(r13)
            q.b.a.v.ChronoField r14 = q.b.a.v.ChronoField.INSTANT_SECONDS
            long r13 = r13.d(r14)
            java.util.Map<q.b.a.v.j, java.lang.Long> r0 = r12.b
            q.b.a.v.ChronoField r1 = q.b.a.v.ChronoField.INSTANT_SECONDS
            java.lang.Long r13 = java.lang.Long.valueOf(r13)
            r0.put(r1, r13)
            goto L_0x0378
        L_0x0355:
            q.b.a.ZoneId r13 = r12.d
            if (r13 == 0) goto L_0x0378
            q.b.a.s.ChronoLocalDate r13 = r12.f3124e
            q.b.a.LocalTime r14 = r12.f3125f
            q.b.a.s.ChronoLocalDateTime r13 = r13.a(r14)
            q.b.a.ZoneId r14 = r12.d
            q.b.a.s.ChronoZonedDateTime r13 = r13.a(r14)
            q.b.a.v.ChronoField r14 = q.b.a.v.ChronoField.INSTANT_SECONDS
            long r13 = r13.d(r14)
            java.util.Map<q.b.a.v.j, java.lang.Long> r0 = r12.b
            q.b.a.v.ChronoField r1 = q.b.a.v.ChronoField.INSTANT_SECONDS
            java.lang.Long r13 = java.lang.Long.valueOf(r13)
            r0.put(r1, r13)
        L_0x0378:
            return r12
        L_0x0379:
            org.threeten.bp.DateTimeException r13 = new org.threeten.bp.DateTimeException
            java.lang.String r14 = "Badly written field"
            r13.<init>(r14)
            throw r13
        */
        throw new UnsupportedOperationException("Method not decompiled: q.b.a.t.DateTimeBuilder.a(q.b.a.t.ResolverStyle, java.util.Set):q.b.a.t.DateTimeBuilder");
    }

    public final void a(TemporalField temporalField, ChronoLocalDate chronoLocalDate) {
        if (this.c.equals(chronoLocalDate.i())) {
            long l2 = chronoLocalDate.l();
            Long put = this.b.put(ChronoField.EPOCH_DAY, Long.valueOf(l2));
            if (put != null && put.longValue() != l2) {
                StringBuilder a = outline.a("Conflict found: ");
                a.append(LocalDate.e(put.longValue()));
                a.append(" differs from ");
                a.append(LocalDate.e(l2));
                a.append(" while resolving  ");
                a.append(temporalField);
                throw new DateTimeException(a.toString());
            }
            return;
        }
        StringBuilder a2 = outline.a("ChronoLocalDate must use the effective parsed chronology: ");
        a2.append(this.c);
        throw new DateTimeException(a2.toString());
    }

    public final void a(TemporalField temporalField, LocalTime localTime) {
        long i2 = localTime.i();
        Long put = this.b.put(ChronoField.NANO_OF_DAY, Long.valueOf(i2));
        if (put != null && put.longValue() != i2) {
            StringBuilder a = outline.a("Conflict found: ");
            a.append(LocalTime.e(put.longValue()));
            a.append(" differs from ");
            a.append(localTime);
            a.append(" while resolving  ");
            a.append(temporalField);
            throw new DateTimeException(a.toString());
        }
    }

    /* JADX WARN: Type inference failed for: r13v9, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r13v22, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r13v42, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r13v59, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r13v76, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v47, types: [java.lang.Object, q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r6v51, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r4v22, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.s.Chronology.a(java.util.Map<q.b.a.v.j, java.lang.Long>, q.b.a.v.a, long):void
     arg types: [java.util.Map<q.b.a.v.j, java.lang.Long>, q.b.a.v.ChronoField, long]
     candidates:
      q.b.a.s.IsoChronology.a(int, int, int):q.b.a.s.ChronoLocalDate
      q.b.a.s.Chronology.a(int, int, int):q.b.a.s.ChronoLocalDate
      q.b.a.s.Chronology.a(java.util.Map<q.b.a.v.j, java.lang.Long>, q.b.a.v.a, long):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.b(long, long):long
     arg types: [long, int]
     candidates:
      n.i.Collections.b(int, int):int
      n.i.Collections.b(long, int):long
      n.i.Collections.b(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.b(long, long):long */
    public final void a(ResolverStyle resolverStyle) {
        LocalDate localDate;
        LocalDate a;
        LocalDate a2;
        if (this.c instanceof IsoChronology) {
            IsoChronology isoChronology = IsoChronology.d;
            Map<j, Long> map = this.b;
            if (map.containsKey(ChronoField.EPOCH_DAY)) {
                localDate = LocalDate.e(map.remove(ChronoField.EPOCH_DAY).longValue());
            } else {
                Long remove = map.remove(ChronoField.PROLEPTIC_MONTH);
                if (remove != null) {
                    if (resolverStyle != ResolverStyle.LENIENT) {
                        ? r4 = ChronoField.PROLEPTIC_MONTH;
                        r4.range.b(remove.longValue(), r4);
                    }
                    isoChronology.a(map, (a) ChronoField.MONTH_OF_YEAR, (long) (Collections.a(remove.longValue(), 12) + 1));
                    isoChronology.a(map, (a) ChronoField.YEAR, Collections.b(remove.longValue(), 12L));
                }
                Long remove2 = map.remove(ChronoField.YEAR_OF_ERA);
                if (remove2 != null) {
                    if (resolverStyle != ResolverStyle.LENIENT) {
                        ? r6 = ChronoField.YEAR_OF_ERA;
                        r6.range.b(remove2.longValue(), r6);
                    }
                    Long remove3 = map.remove(ChronoField.ERA);
                    if (remove3 == null) {
                        Long l2 = map.get(ChronoField.YEAR);
                        if (resolverStyle != ResolverStyle.STRICT) {
                            isoChronology.a(map, (a) ChronoField.YEAR, (l2 == null || l2.longValue() > 0) ? remove2.longValue() : Collections.f(1, remove2.longValue()));
                        } else if (l2 != null) {
                            isoChronology.a(map, (a) ChronoField.YEAR, l2.longValue() > 0 ? remove2.longValue() : Collections.f(1, remove2.longValue()));
                        } else {
                            map.put(ChronoField.YEAR_OF_ERA, remove2);
                        }
                    } else if (remove3.longValue() == 1) {
                        isoChronology.a(map, (a) ChronoField.YEAR, remove2.longValue());
                    } else if (remove3.longValue() == 0) {
                        isoChronology.a(map, (a) ChronoField.YEAR, Collections.f(1, remove2.longValue()));
                    } else {
                        throw new DateTimeException("Invalid value for era: " + remove3);
                    }
                } else if (map.containsKey(ChronoField.ERA)) {
                    ? r0 = ChronoField.ERA;
                    r0.range.b(map.get(r0).longValue(), r0);
                }
                if (map.containsKey(ChronoField.YEAR)) {
                    if (map.containsKey(ChronoField.MONTH_OF_YEAR)) {
                        if (map.containsKey(ChronoField.DAY_OF_MONTH)) {
                            ChronoField chronoField = ChronoField.YEAR;
                            int a3 = chronoField.a(map.remove(chronoField).longValue());
                            int a4 = Collections.a(map.remove(ChronoField.MONTH_OF_YEAR).longValue());
                            int a5 = Collections.a(map.remove(ChronoField.DAY_OF_MONTH).longValue());
                            if (resolverStyle == ResolverStyle.LENIENT) {
                                localDate = LocalDate.a(a3, 1, 1).b((long) Collections.e(a4, 1)).a((long) Collections.e(a5, 1));
                            } else if (resolverStyle == ResolverStyle.SMART) {
                                ? r13 = ChronoField.DAY_OF_MONTH;
                                r13.range.b((long) a5, r13);
                                if (a4 == 4 || a4 == 6 || a4 == 9 || a4 == 11) {
                                    a5 = Math.min(a5, 30);
                                } else if (a4 == 2) {
                                    a5 = Math.min(a5, Month.FEBRUARY.b(Year.b((long) a3)));
                                }
                                localDate = LocalDate.a(a3, a4, a5);
                            } else {
                                localDate = LocalDate.a(a3, a4, a5);
                            }
                        } else if (map.containsKey(ChronoField.ALIGNED_WEEK_OF_MONTH)) {
                            if (map.containsKey(ChronoField.ALIGNED_DAY_OF_WEEK_IN_MONTH)) {
                                ChronoField chronoField2 = ChronoField.YEAR;
                                int a6 = chronoField2.a(map.remove(chronoField2).longValue());
                                if (resolverStyle == ResolverStyle.LENIENT) {
                                    localDate = LocalDate.a(a6, 1, 1).b(Collections.f(map.remove(ChronoField.MONTH_OF_YEAR).longValue(), 1)).c(Collections.f(map.remove(ChronoField.ALIGNED_WEEK_OF_MONTH).longValue(), 1)).a(Collections.f(map.remove(ChronoField.ALIGNED_DAY_OF_WEEK_IN_MONTH).longValue(), 1));
                                } else {
                                    ChronoField chronoField3 = ChronoField.MONTH_OF_YEAR;
                                    int a7 = chronoField3.a(map.remove(chronoField3).longValue());
                                    ChronoField chronoField4 = ChronoField.ALIGNED_WEEK_OF_MONTH;
                                    int a8 = chronoField4.a(map.remove(chronoField4).longValue());
                                    ChronoField chronoField5 = ChronoField.ALIGNED_DAY_OF_WEEK_IN_MONTH;
                                    a2 = LocalDate.a(a6, a7, 1).a((long) ((chronoField5.a(map.remove(chronoField5).longValue()) - 1) + ((a8 - 1) * 7)));
                                    if (resolverStyle == ResolverStyle.STRICT && a2.b((TemporalField) ChronoField.MONTH_OF_YEAR) != a7) {
                                        throw new DateTimeException("Strict mode rejected date parsed to a different month");
                                    }
                                }
                            } else if (map.containsKey(ChronoField.DAY_OF_WEEK)) {
                                ChronoField chronoField6 = ChronoField.YEAR;
                                int a9 = chronoField6.a(map.remove(chronoField6).longValue());
                                if (resolverStyle == ResolverStyle.LENIENT) {
                                    localDate = LocalDate.a(a9, 1, 1).b(Collections.f(map.remove(ChronoField.MONTH_OF_YEAR).longValue(), 1)).c(Collections.f(map.remove(ChronoField.ALIGNED_WEEK_OF_MONTH).longValue(), 1)).a(Collections.f(map.remove(ChronoField.DAY_OF_WEEK).longValue(), 1));
                                } else {
                                    ChronoField chronoField7 = ChronoField.MONTH_OF_YEAR;
                                    int a10 = chronoField7.a(map.remove(chronoField7).longValue());
                                    ChronoField chronoField8 = ChronoField.ALIGNED_WEEK_OF_MONTH;
                                    int a11 = chronoField8.a(map.remove(chronoField8).longValue());
                                    ChronoField chronoField9 = ChronoField.DAY_OF_WEEK;
                                    a2 = LocalDate.a(a9, a10, 1).c((long) (a11 - 1)).a(Collections.a(DayOfWeek.a(chronoField9.a(map.remove(chronoField9).longValue()))));
                                    if (resolverStyle == ResolverStyle.STRICT && a2.b((TemporalField) ChronoField.MONTH_OF_YEAR) != a10) {
                                        throw new DateTimeException("Strict mode rejected date parsed to a different month");
                                    }
                                }
                            }
                            localDate = a2;
                        }
                    }
                    if (map.containsKey(ChronoField.DAY_OF_YEAR)) {
                        ChronoField chronoField10 = ChronoField.YEAR;
                        int a12 = chronoField10.a(map.remove(chronoField10).longValue());
                        if (resolverStyle == ResolverStyle.LENIENT) {
                            localDate = LocalDate.a(a12, 1).a(Collections.f(map.remove(ChronoField.DAY_OF_YEAR).longValue(), 1));
                        } else {
                            ChronoField chronoField11 = ChronoField.DAY_OF_YEAR;
                            localDate = LocalDate.a(a12, chronoField11.a(map.remove(chronoField11).longValue()));
                        }
                    } else if (map.containsKey(ChronoField.ALIGNED_WEEK_OF_YEAR)) {
                        if (map.containsKey(ChronoField.ALIGNED_DAY_OF_WEEK_IN_YEAR)) {
                            ChronoField chronoField12 = ChronoField.YEAR;
                            int a13 = chronoField12.a(map.remove(chronoField12).longValue());
                            if (resolverStyle == ResolverStyle.LENIENT) {
                                localDate = LocalDate.a(a13, 1, 1).c(Collections.f(map.remove(ChronoField.ALIGNED_WEEK_OF_YEAR).longValue(), 1)).a(Collections.f(map.remove(ChronoField.ALIGNED_DAY_OF_WEEK_IN_YEAR).longValue(), 1));
                            } else {
                                ChronoField chronoField13 = ChronoField.ALIGNED_WEEK_OF_YEAR;
                                int a14 = chronoField13.a(map.remove(chronoField13).longValue());
                                ChronoField chronoField14 = ChronoField.ALIGNED_DAY_OF_WEEK_IN_YEAR;
                                a = LocalDate.a(a13, 1, 1).a((long) ((chronoField14.a(map.remove(chronoField14).longValue()) - 1) + ((a14 - 1) * 7)));
                                if (resolverStyle == ResolverStyle.STRICT && a.b((TemporalField) ChronoField.YEAR) != a13) {
                                    throw new DateTimeException("Strict mode rejected date parsed to a different year");
                                }
                            }
                        } else if (map.containsKey(ChronoField.DAY_OF_WEEK)) {
                            ChronoField chronoField15 = ChronoField.YEAR;
                            int a15 = chronoField15.a(map.remove(chronoField15).longValue());
                            if (resolverStyle == ResolverStyle.LENIENT) {
                                localDate = LocalDate.a(a15, 1, 1).c(Collections.f(map.remove(ChronoField.ALIGNED_WEEK_OF_YEAR).longValue(), 1)).a(Collections.f(map.remove(ChronoField.DAY_OF_WEEK).longValue(), 1));
                            } else {
                                ChronoField chronoField16 = ChronoField.ALIGNED_WEEK_OF_YEAR;
                                int a16 = chronoField16.a(map.remove(chronoField16).longValue());
                                ChronoField chronoField17 = ChronoField.DAY_OF_WEEK;
                                a = LocalDate.a(a15, 1, 1).c((long) (a16 - 1)).a(Collections.a(DayOfWeek.a(chronoField17.a(map.remove(chronoField17).longValue()))));
                                if (resolverStyle == ResolverStyle.STRICT && a.b((TemporalField) ChronoField.YEAR) != a15) {
                                    throw new DateTimeException("Strict mode rejected date parsed to a different month");
                                }
                            }
                        }
                        localDate = a;
                    }
                }
                localDate = null;
            }
            a(localDate);
        } else if (this.b.containsKey(ChronoField.EPOCH_DAY)) {
            a(LocalDate.e(this.b.remove(ChronoField.EPOCH_DAY).longValue()));
        }
    }

    /* JADX WARN: Type inference failed for: r0v6, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v7, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r1v3, types: [q.b.a.s.ChronoLocalDate] */
    /* JADX WARN: Type inference failed for: r0v8, types: [q.b.a.s.ChronoLocalDate] */
    public final void a(ZoneId zoneId) {
        ChronoZonedDateTime<?> a = this.c.a(Instant.a(this.b.remove(ChronoField.INSTANT_SECONDS).longValue(), 0), zoneId);
        if (this.f3124e == null) {
            this.f3124e = a.o();
        } else {
            a((TemporalField) ChronoField.INSTANT_SECONDS, (ChronoLocalDate) a.o());
        }
        b(ChronoField.SECOND_OF_DAY, (long) a.q().j());
    }

    public final void a(TemporalAccessor temporalAccessor) {
        Iterator<Map.Entry<j, Long>> it = this.b.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry next = it.next();
            TemporalField temporalField = (TemporalField) next.getKey();
            long longValue = ((Long) next.getValue()).longValue();
            if (temporalAccessor.c(temporalField)) {
                try {
                    long d2 = temporalAccessor.d(temporalField);
                    if (d2 == longValue) {
                        it.remove();
                    } else {
                        throw new DateTimeException("Cross check failed: " + temporalField + " " + d2 + " vs " + temporalField + " " + longValue);
                    }
                } catch (RuntimeException unused) {
                    continue;
                }
            }
        }
    }
}
