package q.b.a.t;

import j.a.a.a.outline;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import n.i.Collections;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;
import q.b.a.Period;
import q.b.a.ZoneId;
import q.b.a.s.Chronology;
import q.b.a.t.d;
import q.b.a.u.DefaultInterfaceTemporalAccessor;
import q.b.a.v.TemporalField;
import q.b.a.v.TemporalQueries;
import q.b.a.v.TemporalQuery;
import q.b.a.v.j;

public final class DateTimeParseContext {
    public Locale a;
    public DecimalStyle b;
    public Chronology c;
    public ZoneId d;

    /* renamed from: e  reason: collision with root package name */
    public boolean f3144e = true;

    /* renamed from: f  reason: collision with root package name */
    public boolean f3145f = true;
    public final ArrayList<d.a> g;

    public final class a extends DefaultInterfaceTemporalAccessor {
        public Chronology b = null;
        public ZoneId c = null;
        public final Map<j, Long> d = new HashMap();

        /* renamed from: e  reason: collision with root package name */
        public boolean f3146e;

        /* renamed from: f  reason: collision with root package name */
        public Period f3147f = Period.f3095e;
        public List<Object[]> g;

        public a() {
        }

        public <R> R a(TemporalQuery<R> temporalQuery) {
            if (temporalQuery == TemporalQueries.b) {
                return this.b;
            }
            if (temporalQuery == TemporalQueries.a || temporalQuery == TemporalQueries.d) {
                return this.c;
            }
            return super.a(temporalQuery);
        }

        public int b(TemporalField temporalField) {
            if (this.d.containsKey(temporalField)) {
                return Collections.a(this.d.get(temporalField).longValue());
            }
            throw new UnsupportedTemporalTypeException(outline.a("Unsupported field: ", temporalField));
        }

        public boolean c(TemporalField temporalField) {
            return this.d.containsKey(temporalField);
        }

        public long d(TemporalField temporalField) {
            if (this.d.containsKey(temporalField)) {
                return this.d.get(temporalField).longValue();
            }
            throw new UnsupportedTemporalTypeException(outline.a("Unsupported field: ", temporalField));
        }

        public String toString() {
            return this.d.toString() + "," + this.b + "," + this.c;
        }
    }

    public DateTimeParseContext(DateTimeFormatter dateTimeFormatter) {
        ArrayList<d.a> arrayList = new ArrayList<>();
        this.g = arrayList;
        this.a = dateTimeFormatter.b;
        this.b = dateTimeFormatter.c;
        this.c = dateTimeFormatter.f3132f;
        this.d = dateTimeFormatter.g;
        arrayList.add(new a());
    }

    public boolean a(char c2, char c3) {
        if (this.f3144e) {
            return c2 == c3;
        }
        if (c2 == c3 || Character.toUpperCase(c2) == Character.toUpperCase(c3) || Character.toLowerCase(c2) == Character.toLowerCase(c3)) {
            return true;
        }
        return false;
    }

    public String toString() {
        return a().toString();
    }

    public boolean a(CharSequence charSequence, int i2, CharSequence charSequence2, int i3, int i4) {
        if (i2 + i4 > charSequence.length() || i3 + i4 > charSequence2.length()) {
            return false;
        }
        if (this.f3144e) {
            for (int i5 = 0; i5 < i4; i5++) {
                if (charSequence.charAt(i2 + i5) != charSequence2.charAt(i3 + i5)) {
                    return false;
                }
            }
            return true;
        }
        for (int i6 = 0; i6 < i4; i6++) {
            char charAt = charSequence.charAt(i2 + i6);
            char charAt2 = charSequence2.charAt(i3 + i6);
            if (charAt != charAt2 && Character.toUpperCase(charAt) != Character.toUpperCase(charAt2) && Character.toLowerCase(charAt) != Character.toLowerCase(charAt2)) {
                return false;
            }
        }
        return true;
    }

    public void a(boolean z) {
        if (z) {
            ArrayList<d.a> arrayList = this.g;
            arrayList.remove(arrayList.size() - 2);
            return;
        }
        ArrayList<d.a> arrayList2 = this.g;
        arrayList2.remove(arrayList2.size() - 1);
    }

    public final a a() {
        ArrayList<d.a> arrayList = this.g;
        return arrayList.get(arrayList.size() - 1);
    }

    public Long a(TemporalField temporalField) {
        return a().d.get(temporalField);
    }

    public DateTimeParseContext(DateTimeParseContext dateTimeParseContext) {
        ArrayList<d.a> arrayList = new ArrayList<>();
        this.g = arrayList;
        this.a = dateTimeParseContext.a;
        this.b = dateTimeParseContext.b;
        this.c = dateTimeParseContext.c;
        this.d = dateTimeParseContext.d;
        this.f3144e = dateTimeParseContext.f3144e;
        this.f3145f = dateTimeParseContext.f3145f;
        arrayList.add(new a());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.v.TemporalField, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public int a(TemporalField temporalField, long j2, int i2, int i3) {
        Collections.a((Object) temporalField, "field");
        Long put = a().d.put(temporalField, Long.valueOf(j2));
        return (put == null || put.longValue() == j2) ? i3 : ~i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.ZoneId, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public void a(ZoneId zoneId) {
        Collections.a((Object) zoneId, "zone");
        a().c = zoneId;
    }
}
