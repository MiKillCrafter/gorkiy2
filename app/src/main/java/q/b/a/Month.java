package q.b.a;

import j.a.a.a.outline;
import org.threeten.bp.DateTimeException;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;
import q.b.a.s.Chronology;
import q.b.a.s.IsoChronology;
import q.b.a.v.ChronoField;
import q.b.a.v.ChronoUnit;
import q.b.a.v.Temporal;
import q.b.a.v.TemporalAccessor;
import q.b.a.v.TemporalField;
import q.b.a.v.TemporalQueries;
import q.b.a.v.TemporalQuery;
import q.b.a.v.ValueRange;
import q.b.a.v.e;
import q.b.a.v.f;

public enum Month implements e, f {
    JANUARY,
    FEBRUARY,
    MARCH,
    APRIL,
    MAY,
    JUNE,
    JULY,
    AUGUST,
    SEPTEMBER,
    OCTOBER,
    NOVEMBER,
    DECEMBER;
    
    public static final Month[] ENUMS = values();
    public static final TemporalQuery<g> FROM = new a();

    public class a implements TemporalQuery<g> {
        /* JADX WARN: Type inference failed for: r0v4, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
        public Object a(TemporalAccessor temporalAccessor) {
            if (temporalAccessor instanceof Month) {
                return (Month) temporalAccessor;
            }
            try {
                if (!IsoChronology.d.equals(Chronology.c(temporalAccessor))) {
                    temporalAccessor = LocalDate.a(temporalAccessor);
                }
                return Month.a(temporalAccessor.b(ChronoField.MONTH_OF_YEAR));
            } catch (DateTimeException e2) {
                throw new DateTimeException(outline.a(temporalAccessor, outline.a("Unable to obtain Month from TemporalAccessor: ", temporalAccessor, ", type ")), e2);
            }
        }
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.Month, q.b.a.v.TemporalAccessor] */
    public <R> R a(TemporalQuery temporalQuery) {
        if (temporalQuery == TemporalQueries.b) {
            return IsoChronology.d;
        }
        if (temporalQuery == TemporalQueries.c) {
            return ChronoUnit.MONTHS;
        }
        if (temporalQuery == TemporalQueries.f3151f || temporalQuery == TemporalQueries.g || temporalQuery == TemporalQueries.d || temporalQuery == TemporalQueries.a || temporalQuery == TemporalQueries.f3150e) {
            return null;
        }
        return temporalQuery.a(this);
    }

    public int b(TemporalField temporalField) {
        if (temporalField == ChronoField.MONTH_OF_YEAR) {
            return getValue();
        }
        return a(temporalField).a(d(temporalField), temporalField);
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [q.b.a.Month, q.b.a.v.TemporalAccessor] */
    public boolean c(TemporalField temporalField) {
        if (temporalField instanceof ChronoField) {
            if (temporalField == ChronoField.MONTH_OF_YEAR) {
                return true;
            }
            return false;
        } else if (temporalField == null || !temporalField.a(this)) {
            return false;
        } else {
            return true;
        }
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [q.b.a.Month, q.b.a.v.TemporalAccessor] */
    public long d(TemporalField temporalField) {
        if (temporalField == ChronoField.MONTH_OF_YEAR) {
            return (long) getValue();
        }
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.b(this);
        }
        throw new UnsupportedTemporalTypeException(outline.a("Unsupported field: ", temporalField));
    }

    public int f() {
        int ordinal = ordinal();
        if (ordinal != 1) {
            return (ordinal == 3 || ordinal == 5 || ordinal == 8 || ordinal == 10) ? 30 : 31;
        }
        return 29;
    }

    public int getValue() {
        return ordinal() + 1;
    }

    public int b(boolean z) {
        int ordinal = ordinal();
        return ordinal != 1 ? (ordinal == 3 || ordinal == 5 || ordinal == 8 || ordinal == 10) ? 30 : 31 : z ? 29 : 28;
    }

    public static Month a(int i2) {
        if (i2 >= 1 && i2 <= 12) {
            return ENUMS[i2 - 1];
        }
        throw new DateTimeException(outline.b("Invalid value for MonthOfYear: ", i2));
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [q.b.a.Month, q.b.a.v.TemporalAccessor] */
    public ValueRange a(TemporalField temporalField) {
        if (temporalField == ChronoField.MONTH_OF_YEAR) {
            return temporalField.g();
        }
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.c(this);
        }
        throw new UnsupportedTemporalTypeException(outline.a("Unsupported field: ", temporalField));
    }

    public int a(boolean z) {
        switch (ordinal()) {
            case 0:
                return 1;
            case 1:
                return 32;
            case 2:
                return z + true;
            case 3:
                return (z ? 1 : 0) + true;
            case 4:
                return z + true;
            case 5:
                return z + true;
            case 6:
                return z + true;
            case 7:
                return z + true;
            case 8:
                return z + true;
            case 9:
                return z + true;
            case 10:
                return z + true;
            default:
                return z + true;
        }
    }

    /* JADX WARN: Type inference failed for: r0v3, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public Temporal a(Temporal temporal) {
        if (Chronology.c(temporal).equals(IsoChronology.d)) {
            return temporal.a((TemporalField) ChronoField.MONTH_OF_YEAR, (long) getValue());
        }
        throw new DateTimeException("Adjustment only supported on ISO date-time");
    }
}
