package q.b.a.s;

import j.a.a.a.outline;
import java.io.DataInput;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Calendar;
import org.threeten.bp.DateTimeException;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;
import q.b.a.LocalDate;
import q.b.a.f;
import q.b.a.v.ChronoField;
import q.b.a.v.Temporal;
import q.b.a.v.TemporalAdjuster;
import q.b.a.v.TemporalAmount;
import q.b.a.v.TemporalField;
import q.b.a.v.TemporalUnit;
import q.b.a.v.ValueRange;
import q.b.a.v.d;
import q.b.a.v.m;

public final class JapaneseDate extends ChronoDateImpl<p> implements Serializable {

    /* renamed from: e  reason: collision with root package name */
    public static final LocalDate f3118e = LocalDate.a(1873, 1, 1);
    public final LocalDate b;
    public transient JapaneseEra c;
    public transient int d;

    public JapaneseDate(LocalDate localDate) {
        if (!localDate.b(f3118e)) {
            JapaneseEra a = JapaneseEra.a(localDate);
            this.c = a;
            this.d = localDate.b - (a.c.b - 1);
            this.b = localDate;
            return;
        }
        throw new DateTimeException("Minimum supported date is January 1st Meiji 6");
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        JapaneseEra a = JapaneseEra.a(this.b);
        this.c = a;
        this.d = this.b.b - (a.c.b - 1);
    }

    private Object writeReplace() {
        return new Ser((byte) 1, this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.s.ChronoDateImpl.b(long, q.b.a.v.m):q.b.a.s.ChronoDateImpl<D>
     arg types: [long, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.s.JapaneseDate.b(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
      q.b.a.s.JapaneseDate.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoDateImpl.b(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
      q.b.a.s.ChronoDateImpl.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoDateImpl.b(long, q.b.a.v.m):q.b.a.s.ChronoDateImpl<D> */
    public ChronoDateImpl b(long j2, TemporalUnit temporalUnit) {
        return (JapaneseDate) super.b(j2, (m) temporalUnit);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.s.ChronoLocalDate, q.b.a.s.JapaneseDate] */
    public boolean c(TemporalField temporalField) {
        if (temporalField == ChronoField.ALIGNED_DAY_OF_WEEK_IN_MONTH || temporalField == ChronoField.ALIGNED_DAY_OF_WEEK_IN_YEAR || temporalField == ChronoField.ALIGNED_WEEK_OF_MONTH || temporalField == ChronoField.ALIGNED_WEEK_OF_YEAR) {
            return false;
        }
        return JapaneseDate.super.c(temporalField);
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [q.b.a.v.TemporalAccessor, q.b.a.s.JapaneseDate] */
    public long d(TemporalField temporalField) {
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.b(this);
        }
        int ordinal = ((ChronoField) temporalField).ordinal();
        if (!(ordinal == 16 || ordinal == 17)) {
            if (ordinal == 19) {
                return o();
            }
            if (ordinal == 25) {
                return (long) this.d;
            }
            if (ordinal == 27) {
                return (long) this.c.b;
            }
            if (!(ordinal == 21 || ordinal == 22)) {
                return this.b.d(temporalField);
            }
        }
        throw new UnsupportedTemporalTypeException(outline.a("Unsupported field: ", temporalField));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof JapaneseDate) {
            return this.b.equals(((JapaneseDate) obj).b);
        }
        return false;
    }

    public int hashCode() {
        if (JapaneseChronology.f3116e != null) {
            return -688086063 ^ this.b.hashCode();
        }
        throw null;
    }

    public Chronology i() {
        return JapaneseChronology.f3116e;
    }

    public Era j() {
        return this.c;
    }

    public long l() {
        return this.b.l();
    }

    public final long o() {
        int p2;
        if (this.d == 1) {
            p2 = (this.b.p() - this.c.c.p()) + 1;
        } else {
            p2 = this.b.p();
        }
        return (long) p2;
    }

    /* JADX WARN: Type inference failed for: r1v2, types: [q.b.a.s.ChronoLocalDate, q.b.a.s.JapaneseDate] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.s.ChronoDateImpl.b(long, q.b.a.v.m):q.b.a.s.ChronoDateImpl<D>
     arg types: [long, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.s.JapaneseDate.b(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
      q.b.a.s.JapaneseDate.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoDateImpl.b(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
      q.b.a.s.ChronoDateImpl.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoDateImpl.b(long, q.b.a.v.m):q.b.a.s.ChronoDateImpl<D> */
    /* renamed from: b  reason: collision with other method in class */
    public ChronoLocalDate m22b(long j2, TemporalUnit temporalUnit) {
        return (JapaneseDate) super.b(j2, (m) temporalUnit);
    }

    public final ChronoLocalDateTime<p> a(f fVar) {
        return new ChronoLocalDateTimeImpl(this, fVar);
    }

    /* JADX WARN: Type inference failed for: r1v2, types: [q.b.a.v.Temporal, q.b.a.s.JapaneseDate] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.s.ChronoDateImpl.b(long, q.b.a.v.m):q.b.a.s.ChronoDateImpl<D>
     arg types: [long, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.s.JapaneseDate.b(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
      q.b.a.s.JapaneseDate.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoDateImpl.b(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
      q.b.a.s.ChronoDateImpl.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoDateImpl.b(long, q.b.a.v.m):q.b.a.s.ChronoDateImpl<D> */
    /* renamed from: b  reason: collision with other method in class */
    public Temporal m23b(long j2, TemporalUnit temporalUnit) {
        return (JapaneseDate) super.b(j2, (m) temporalUnit);
    }

    public ChronoDateImpl c(long j2) {
        return a(this.b.d(j2));
    }

    /* JADX WARN: Type inference failed for: r2v3, types: [q.b.a.s.ChronoLocalDate, q.b.a.s.JapaneseDate] */
    public ChronoLocalDate a(TemporalAdjuster temporalAdjuster) {
        return (JapaneseDate) JapaneseChronology.f3116e.a((d) temporalAdjuster.a(this));
    }

    public ChronoDateImpl b(long j2) {
        return a(this.b.b(j2));
    }

    /* JADX WARN: Type inference failed for: r2v3, types: [q.b.a.v.Temporal, q.b.a.s.JapaneseDate] */
    /* renamed from: a  reason: collision with other method in class */
    public Temporal m21a(TemporalAdjuster temporalAdjuster) {
        return (JapaneseDate) JapaneseChronology.f3116e.a((d) temporalAdjuster.a(this));
    }

    /* JADX WARN: Type inference failed for: r2v3, types: [q.b.a.s.ChronoLocalDate, q.b.a.s.JapaneseDate] */
    public ChronoLocalDate a(TemporalAmount temporalAmount) {
        return (JapaneseDate) i().a((d) temporalAmount.a(this));
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [q.b.a.v.TemporalAccessor, q.b.a.s.JapaneseDate] */
    public ValueRange a(TemporalField temporalField) {
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.c(this);
        }
        if (c(temporalField)) {
            ChronoField chronoField = (ChronoField) temporalField;
            int ordinal = chronoField.ordinal();
            if (ordinal == 19) {
                return a(6);
            }
            if (ordinal != 25) {
                return JapaneseChronology.f3116e.a(chronoField);
            }
            return a(1);
        }
        throw new UnsupportedTemporalTypeException(outline.a("Unsupported field: ", temporalField));
    }

    public final ValueRange a(int i2) {
        Calendar instance = Calendar.getInstance(JapaneseChronology.d);
        instance.set(0, this.c.b + 2);
        int i3 = this.d;
        LocalDate localDate = this.b;
        instance.set(i3, localDate.c - 1, localDate.d);
        return ValueRange.a((long) instance.getActualMinimum(i2), (long) instance.getActualMaximum(i2));
    }

    /* JADX WARN: Type inference failed for: r0v2, types: [java.lang.Enum, q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.LocalDate.a(q.b.a.v.TemporalField, long):q.b.a.LocalDate
     arg types: [q.b.a.v.TemporalField, long]
     candidates:
      q.b.a.LocalDate.a(int, int):q.b.a.LocalDate
      q.b.a.LocalDate.a(java.lang.CharSequence, q.b.a.t.DateTimeFormatter):q.b.a.LocalDate
      q.b.a.LocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.LocalDate
      q.b.a.LocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
      q.b.a.LocalDate.a(q.b.a.v.TemporalField, long):q.b.a.s.ChronoLocalDate
      q.b.a.LocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.LocalDate.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
      q.b.a.s.ChronoLocalDate.a(q.b.a.v.TemporalField, long):q.b.a.s.ChronoLocalDate
      q.b.a.s.ChronoLocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDate.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.v.Temporal.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.v.Temporal.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.LocalDate.a(q.b.a.v.TemporalField, long):q.b.a.LocalDate */
    public JapaneseDate a(TemporalField temporalField, long j2) {
        if (!(temporalField instanceof ChronoField)) {
            return (JapaneseDate) temporalField.a(this, j2);
        }
        ? r0 = (ChronoField) temporalField;
        if (d(r0) == j2) {
            return this;
        }
        int ordinal = r0.ordinal();
        if (ordinal == 19 || ordinal == 25 || ordinal == 27) {
            int a = JapaneseChronology.f3116e.a((ChronoField) r0).a(j2, (TemporalField) r0);
            int ordinal2 = r0.ordinal();
            if (ordinal2 == 19) {
                return a(this.b.a(((long) a) - o()));
            }
            if (ordinal2 == 25) {
                return a(this.c, a);
            }
            if (ordinal2 == 27) {
                return a(JapaneseEra.a(a), this.d);
            }
        }
        return a(this.b.a(temporalField, j2));
    }

    /* JADX WARN: Type inference failed for: r1v2, types: [q.b.a.s.ChronoLocalDate, q.b.a.s.JapaneseDate] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.s.ChronoLocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
     arg types: [long, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.s.JapaneseDate.a(q.b.a.v.TemporalField, long):q.b.a.s.ChronoLocalDate
      q.b.a.s.JapaneseDate.a(q.b.a.s.JapaneseEra, int):q.b.a.s.JapaneseDate
      q.b.a.s.JapaneseDate.a(q.b.a.v.TemporalField, long):q.b.a.s.JapaneseDate
      q.b.a.s.JapaneseDate.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.JapaneseDate.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate */
    public ChronoLocalDate a(long j2, TemporalUnit temporalUnit) {
        return (JapaneseDate) JapaneseDate.super.a(j2, temporalUnit);
    }

    /* JADX WARN: Type inference failed for: r1v2, types: [q.b.a.v.Temporal, q.b.a.s.JapaneseDate] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.s.ChronoLocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
     arg types: [long, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.s.JapaneseDate.a(q.b.a.v.TemporalField, long):q.b.a.s.ChronoLocalDate
      q.b.a.s.JapaneseDate.a(q.b.a.s.JapaneseEra, int):q.b.a.s.JapaneseDate
      q.b.a.s.JapaneseDate.a(q.b.a.v.TemporalField, long):q.b.a.s.JapaneseDate
      q.b.a.s.JapaneseDate.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.JapaneseDate.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate */
    /* renamed from: a  reason: collision with other method in class */
    public Temporal m20a(long j2, TemporalUnit temporalUnit) {
        return (JapaneseDate) JapaneseDate.super.a(j2, temporalUnit);
    }

    /* JADX WARN: Type inference failed for: r7v2, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public final JapaneseDate a(JapaneseEra japaneseEra, int i2) {
        if (JapaneseChronology.f3116e == null) {
            throw null;
        } else if (japaneseEra instanceof JapaneseEra) {
            ValueRange.a(1, (long) ((japaneseEra.i().b - japaneseEra.c.b) + 1)).b((long) i2, ChronoField.YEAR_OF_ERA);
            return a(this.b.a((japaneseEra.c.b + i2) - 1));
        } else {
            throw new ClassCastException("Era must be JapaneseEra");
        }
    }

    public ChronoDateImpl a(long j2) {
        return a(this.b.a(j2));
    }

    public final JapaneseDate a(LocalDate localDate) {
        return localDate.equals(this.b) ? this : new JapaneseDate(localDate);
    }

    /* JADX WARN: Type inference failed for: r2v1, types: [q.b.a.s.ChronoLocalDate, q.b.a.s.JapaneseDate] */
    public static ChronoLocalDate a(DataInput dataInput) {
        int readInt = dataInput.readInt();
        byte readByte = dataInput.readByte();
        byte readByte2 = dataInput.readByte();
        if (JapaneseChronology.f3116e != null) {
            return new JapaneseDate(LocalDate.a(readInt, readByte, readByte2));
        }
        throw null;
    }
}
