package q.b.a.s;

import java.io.Serializable;
import q.b.a.LocalDate;
import q.b.a.c;
import q.b.a.o;
import q.b.a.v.ChronoField;
import q.b.a.v.ValueRange;
import q.b.a.v.e;

public final class MinguoChronology extends Chronology implements Serializable {
    public static final MinguoChronology d = new MinguoChronology();

    private Object readResolve() {
        return d;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.s.ChronoLocalDate, q.b.a.s.MinguoDate] */
    public ChronoLocalDate a(int i2, int i3, int i4) {
        return new MinguoDate(LocalDate.a(i2 + 1911, i3, i4));
    }

    public ChronoLocalDateTime<s> b(e eVar) {
        return super.b(eVar);
    }

    public String f() {
        return "roc";
    }

    public String g() {
        return "Minguo";
    }

    /* JADX WARN: Type inference failed for: r2v2, types: [q.b.a.s.ChronoLocalDate] */
    /* JADX WARN: Type inference failed for: r2v3, types: [q.b.a.s.ChronoLocalDate, q.b.a.s.MinguoDate] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public q.b.a.s.ChronoLocalDate a(q.b.a.v.TemporalAccessor r2) {
        /*
            r1 = this;
            boolean r0 = r2 instanceof q.b.a.s.MinguoDate
            if (r0 == 0) goto L_0x0007
            q.b.a.s.MinguoDate r2 = (q.b.a.s.MinguoDate) r2
            goto L_0x0011
        L_0x0007:
            q.b.a.s.MinguoDate r0 = new q.b.a.s.MinguoDate
            q.b.a.LocalDate r2 = q.b.a.LocalDate.a(r2)
            r0.<init>(r2)
            r2 = r0
        L_0x0011:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: q.b.a.s.MinguoChronology.a(q.b.a.v.TemporalAccessor):q.b.a.s.ChronoLocalDate");
    }

    /* JADX WARN: Type inference failed for: r1v1, types: [q.b.a.s.Era, q.b.a.s.MinguoEra] */
    public Era a(int i2) {
        return MinguoEra.a(i2);
    }

    public ValueRange a(ChronoField chronoField) {
        switch (chronoField.ordinal()) {
            case 24:
                ValueRange valueRange = ChronoField.PROLEPTIC_MONTH.range;
                return ValueRange.a(valueRange.b - 22932, valueRange.f3152e - 22932);
            case 25:
                ValueRange valueRange2 = ChronoField.YEAR.range;
                return ValueRange.a(1, valueRange2.f3152e - 1911, (-valueRange2.b) + 1 + 1911);
            case 26:
                ValueRange valueRange3 = ChronoField.YEAR.range;
                return ValueRange.a(valueRange3.b - 1911, valueRange3.f3152e - 1911);
            default:
                return chronoField.range;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.s.ChronoZonedDateTimeImpl.a(q.b.a.s.h, q.b.a.c, q.b.a.o):q.b.a.s.ChronoZonedDateTimeImpl<R>
     arg types: [q.b.a.s.MinguoChronology, q.b.a.c, q.b.a.o]
     candidates:
      q.b.a.s.ChronoZonedDateTimeImpl.a(q.b.a.s.ChronoLocalDateTimeImpl, q.b.a.o, q.b.a.p):q.b.a.s.ChronoZonedDateTime<R>
      q.b.a.s.ChronoZonedDateTimeImpl.a(q.b.a.s.h, q.b.a.c, q.b.a.o):q.b.a.s.ChronoZonedDateTimeImpl<R> */
    public ChronoZonedDateTime<s> a(c cVar, o oVar) {
        return ChronoZonedDateTimeImpl.a((h) super, cVar, oVar);
    }
}
