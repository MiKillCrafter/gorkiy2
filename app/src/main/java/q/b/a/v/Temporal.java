package q.b.a.v;

public interface Temporal extends TemporalAccessor {
    Temporal a(long j2, TemporalUnit temporalUnit);

    Temporal a(TemporalAdjuster temporalAdjuster);

    Temporal a(TemporalField temporalField, long j2);

    Temporal b(long j2, TemporalUnit temporalUnit);
}
