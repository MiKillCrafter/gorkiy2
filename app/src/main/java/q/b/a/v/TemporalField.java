package q.b.a.v;

import java.util.Map;
import q.b.a.t.i;

public interface TemporalField {
    <R extends d> R a(R r2, long j2);

    e a(Map<j, Long> map, e eVar, i iVar);

    boolean a(TemporalAccessor temporalAccessor);

    long b(TemporalAccessor temporalAccessor);

    ValueRange c(TemporalAccessor temporalAccessor);

    boolean f();

    ValueRange g();

    boolean h();
}
