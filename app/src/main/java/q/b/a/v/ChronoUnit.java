package q.b.a.v;

import androidx.recyclerview.widget.RecyclerView;
import com.crashlytics.android.answers.RetryManager;
import q.b.a.Duration;
import q.b.a.b;

public enum ChronoUnit implements m {
    NANOS("Nanos", Duration.a(1)),
    MICROS("Micros", Duration.a(1000)),
    MILLIS("Millis", Duration.a((long) RetryManager.NANOSECONDS_IN_MS)),
    SECONDS("Seconds", Duration.b(1)),
    MINUTES("Minutes", Duration.b(60)),
    HOURS("Hours", Duration.b(3600)),
    HALF_DAYS("HalfDays", Duration.b(43200)),
    DAYS("Days", Duration.b(86400)),
    WEEKS("Weeks", Duration.b(604800)),
    MONTHS("Months", Duration.b(2629746)),
    YEARS("Years", Duration.b(31556952)),
    DECADES("Decades", Duration.b(315569520)),
    CENTURIES("Centuries", Duration.b(3155695200L)),
    MILLENNIA("Millennia", Duration.b(31556952000L)),
    ERAS("Eras", Duration.b(31556952000000000L)),
    FOREVER("Forever", Duration.a((long) RecyclerView.FOREVER_NS, 999999999L));
    
    public final Duration duration;
    public final String name;

    /* access modifiers changed from: public */
    ChronoUnit(String str, b bVar) {
        this.name = str;
        this.duration = bVar;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.ChronoUnit, q.b.a.v.TemporalUnit] */
    public <R extends d> R a(R r2, long j2) {
        return r2.b(j2, this);
    }

    public boolean f() {
        return compareTo(DAYS) >= 0 && this != FOREVER;
    }

    public String toString() {
        return this.name;
    }
}
