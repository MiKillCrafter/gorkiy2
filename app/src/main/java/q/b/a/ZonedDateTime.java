package q.b.a;

import androidx.recyclerview.widget.RecyclerView;
import j.a.a.a.outline;
import java.io.InvalidObjectException;
import java.io.Serializable;
import java.util.List;
import n.i.Collections;
import org.threeten.bp.DateTimeException;
import q.b.a.s.ChronoLocalDate;
import q.b.a.s.ChronoLocalDateTime;
import q.b.a.s.ChronoZonedDateTime;
import q.b.a.v.ChronoField;
import q.b.a.v.ChronoUnit;
import q.b.a.v.TemporalAdjuster;
import q.b.a.v.TemporalField;
import q.b.a.v.TemporalQueries;
import q.b.a.v.TemporalQuery;
import q.b.a.v.TemporalUnit;
import q.b.a.v.ValueRange;
import q.b.a.v.d;
import q.b.a.w.ZoneOffsetTransition;
import q.b.a.w.ZoneRules;

public final class ZonedDateTime extends ChronoZonedDateTime<d> implements d, Serializable {
    public final LocalDateTime b;
    public final ZoneOffset c;
    public final ZoneId d;

    public ZonedDateTime(LocalDateTime localDateTime, ZoneOffset zoneOffset, ZoneId zoneId) {
        this.b = localDateTime;
        this.c = zoneOffset;
        this.d = zoneId;
    }

    private Object readResolve() {
        throw new InvalidObjectException("Deserialization via serialization delegate");
    }

    private Object writeReplace() {
        return new Ser((byte) 6, this);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.ZonedDateTime, q.b.a.v.TemporalAccessor] */
    public boolean c(TemporalField temporalField) {
        return (temporalField instanceof ChronoField) || (temporalField != null && temporalField.a(this));
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [q.b.a.s.ChronoZonedDateTime, q.b.a.ZonedDateTime, q.b.a.v.TemporalAccessor] */
    public long d(TemporalField temporalField) {
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.b(this);
        }
        int ordinal = ((ChronoField) temporalField).ordinal();
        if (ordinal == 28) {
            return l();
        }
        if (ordinal != 29) {
            return this.b.d(temporalField);
        }
        return (long) this.c.c;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ZonedDateTime)) {
            return false;
        }
        ZonedDateTime zonedDateTime = (ZonedDateTime) obj;
        if (!this.b.equals(zonedDateTime.b) || !this.c.equals(zonedDateTime.c) || !this.d.equals(zonedDateTime.d)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (this.b.hashCode() ^ this.c.c) ^ Integer.rotateLeft(this.d.hashCode(), 3);
    }

    public ZoneOffset i() {
        return this.c;
    }

    public ZoneId j() {
        return this.d;
    }

    public ChronoLocalDate o() {
        return this.b.b;
    }

    public ChronoLocalDateTime p() {
        return this.b;
    }

    public LocalTime q() {
        return this.b.c;
    }

    public String toString() {
        String str = this.b.toString() + this.c.d;
        if (this.c == this.d) {
            return str;
        }
        return str + '[' + this.d.toString() + ']';
    }

    public int b(TemporalField temporalField) {
        if (!(temporalField instanceof ChronoField)) {
            return super.b(temporalField);
        }
        int ordinal = ((ChronoField) temporalField).ordinal();
        if (ordinal == 28) {
            throw new DateTimeException(outline.a("Field too large for an int: ", temporalField));
        } else if (ordinal != 29) {
            return this.b.b(temporalField);
        } else {
            return this.c.c;
        }
    }

    public static ZonedDateTime a(LocalDateTime localDateTime, ZoneId zoneId) {
        return a(localDateTime, zoneId, (ZoneOffset) null);
    }

    public <R> R a(TemporalQuery<R> temporalQuery) {
        if (temporalQuery == TemporalQueries.f3151f) {
            return this.b.b;
        }
        return super.a(temporalQuery);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.LocalDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.LocalDateTime
     arg types: [long, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.LocalDateTime.b(q.b.a.LocalDate, q.b.a.LocalTime):q.b.a.LocalDateTime
      q.b.a.LocalDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDateTime
      q.b.a.LocalDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDateTime.b(long, q.b.a.v.m):q.b.a.s.ChronoLocalDateTime<D>
      q.b.a.s.ChronoLocalDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.LocalDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.LocalDateTime */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.LocalDateTime, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.ZoneOffset, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.ZoneId, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public ZonedDateTime b(long j2, TemporalUnit temporalUnit) {
        if (!(temporalUnit instanceof ChronoUnit)) {
            return (ZonedDateTime) temporalUnit.a(this, j2);
        }
        if (temporalUnit.f()) {
            return a(this.b.b(j2, temporalUnit));
        }
        LocalDateTime b2 = this.b.b(j2, temporalUnit);
        ZoneOffset zoneOffset = this.c;
        ZoneId zoneId = this.d;
        Collections.a((Object) b2, "localDateTime");
        Collections.a((Object) zoneOffset, "offset");
        Collections.a((Object) zoneId, "zone");
        return a(b2.a(zoneOffset), b2.c.f3094e, zoneId);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.LocalDateTime, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.ZoneId, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.p, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public static ZonedDateTime a(LocalDateTime localDateTime, ZoneId zoneId, ZoneOffset zoneOffset) {
        Collections.a((Object) localDateTime, "localDateTime");
        Collections.a((Object) zoneId, "zone");
        if (zoneId instanceof ZoneOffset) {
            return new ZonedDateTime(localDateTime, (ZoneOffset) zoneId, zoneId);
        }
        ZoneRules g = zoneId.g();
        List<p> b2 = g.b((e) localDateTime);
        if (b2.size() == 1) {
            zoneOffset = (ZoneOffset) b2.get(0);
        } else if (b2.size() == 0) {
            ZoneOffsetTransition a = g.a(localDateTime);
            localDateTime = localDateTime.c(Duration.b((long) (a.d.c - a.c.c)).b);
            zoneOffset = a.d;
        } else if (zoneOffset == null || !b2.contains(zoneOffset)) {
            p pVar = b2.get(0);
            Collections.a((Object) pVar, "offset");
            zoneOffset = (ZoneOffset) pVar;
        }
        return new ZonedDateTime(localDateTime, zoneOffset, zoneId);
    }

    public static ZonedDateTime a(long j2, int i2, ZoneId zoneId) {
        ZoneOffset a = zoneId.g().a(Instant.b(j2, (long) i2));
        return new ZonedDateTime(LocalDateTime.a(j2, i2, a), a, zoneId);
    }

    public final ZonedDateTime a(LocalDateTime localDateTime) {
        return a(localDateTime, this.d, this.c);
    }

    public final ZonedDateTime a(ZoneOffset zoneOffset) {
        return (zoneOffset.equals(this.c) || !this.d.g().a(this.b, zoneOffset)) ? this : new ZonedDateTime(this.b, zoneOffset, this.d);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.ZonedDateTime, q.b.a.v.TemporalAccessor] */
    public ValueRange a(TemporalField temporalField) {
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.c(this);
        }
        if (temporalField == ChronoField.INSTANT_SECONDS || temporalField == ChronoField.OFFSET_SECONDS) {
            return temporalField.g();
        }
        return this.b.a(temporalField);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.ZoneId, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public ChronoZonedDateTime a(ZoneId zoneId) {
        Collections.a((Object) zoneId, "zone");
        return this.d.equals(zoneId) ? super : a(this.b, zoneId, this.c);
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [q.b.a.ZonedDateTime, q.b.a.v.Temporal] */
    public ZonedDateTime a(TemporalAdjuster temporalAdjuster) {
        if (temporalAdjuster instanceof LocalDate) {
            return a(LocalDateTime.b((LocalDate) temporalAdjuster, this.b.c), this.d, this.c);
        }
        if (temporalAdjuster instanceof LocalTime) {
            return a(LocalDateTime.b(this.b.b, (LocalTime) temporalAdjuster), this.d, this.c);
        }
        if (temporalAdjuster instanceof LocalDateTime) {
            return a((LocalDateTime) temporalAdjuster);
        }
        if (temporalAdjuster instanceof Instant) {
            Instant instant = (Instant) temporalAdjuster;
            return a(instant.b, instant.c, this.d);
        } else if (temporalAdjuster instanceof ZoneOffset) {
            return a((ZoneOffset) temporalAdjuster);
        } else {
            return (ZonedDateTime) temporalAdjuster.a(this);
        }
    }

    /* JADX WARN: Type inference failed for: r0v2, types: [java.lang.Enum, q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.LocalDateTime.a(q.b.a.v.TemporalField, long):q.b.a.LocalDateTime
     arg types: [q.b.a.v.TemporalField, long]
     candidates:
      q.b.a.LocalDateTime.a(java.lang.CharSequence, q.b.a.t.DateTimeFormatter):q.b.a.LocalDateTime
      q.b.a.LocalDateTime.a(long, q.b.a.v.TemporalUnit):q.b.a.LocalDateTime
      q.b.a.LocalDateTime.a(q.b.a.LocalDate, q.b.a.LocalTime):q.b.a.LocalDateTime
      q.b.a.LocalDateTime.a(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDateTime
      q.b.a.LocalDateTime.a(q.b.a.v.TemporalField, long):q.b.a.s.ChronoLocalDateTime
      q.b.a.LocalDateTime.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.LocalDateTime.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDateTime.a(long, q.b.a.v.m):q.b.a.s.ChronoLocalDateTime<D>
      q.b.a.s.ChronoLocalDateTime.a(q.b.a.v.j, long):q.b.a.s.ChronoLocalDateTime<D>
      q.b.a.s.ChronoLocalDateTime.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoLocalDateTime.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
      q.b.a.LocalDateTime.a(q.b.a.v.TemporalField, long):q.b.a.LocalDateTime */
    public ZonedDateTime a(TemporalField temporalField, long j2) {
        if (!(temporalField instanceof ChronoField)) {
            return (ZonedDateTime) temporalField.a(this, j2);
        }
        ? r0 = (ChronoField) temporalField;
        int ordinal = r0.ordinal();
        if (ordinal == 28) {
            return a(j2, this.b.c.f3094e, this.d);
        }
        if (ordinal != 29) {
            return a(this.b.a(temporalField, j2));
        }
        return a(ZoneOffset.a(r0.range.a(j2, (TemporalField) r0)));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.ZonedDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.ZonedDateTime
     arg types: [int, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.ZonedDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoZonedDateTime
      q.b.a.ZonedDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoZonedDateTime.b(long, q.b.a.v.m):q.b.a.s.ChronoZonedDateTime<D>
      q.b.a.s.ChronoZonedDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.ZonedDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.ZonedDateTime */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.ZonedDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.ZonedDateTime
     arg types: [?, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.ZonedDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoZonedDateTime
      q.b.a.ZonedDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoZonedDateTime.b(long, q.b.a.v.m):q.b.a.s.ChronoZonedDateTime<D>
      q.b.a.s.ChronoZonedDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.ZonedDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.ZonedDateTime */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.ZonedDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.ZonedDateTime
     arg types: [long, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.ZonedDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoZonedDateTime
      q.b.a.ZonedDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.s.ChronoZonedDateTime.b(long, q.b.a.v.m):q.b.a.s.ChronoZonedDateTime<D>
      q.b.a.s.ChronoZonedDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.ZonedDateTime.b(long, q.b.a.v.TemporalUnit):q.b.a.ZonedDateTime */
    public ZonedDateTime a(long j2, TemporalUnit temporalUnit) {
        return j2 == Long.MIN_VALUE ? b((long) RecyclerView.FOREVER_NS, temporalUnit).b(1L, temporalUnit) : b(-j2, temporalUnit);
    }
}
