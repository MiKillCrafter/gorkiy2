package q.a.a.h;

import java.util.Arrays;
import org.bouncycastle.crypto.DataLengthException;
import q.a.a.b;

public class a implements q.a.a.a {
    public byte[] a;
    public byte[] b;
    public byte[] c;
    public int d;

    /* renamed from: e  reason: collision with root package name */
    public q.a.a.a f3085e = null;

    /* renamed from: f  reason: collision with root package name */
    public boolean f3086f;

    public a(q.a.a.a aVar) {
        this.f3085e = aVar;
        int b2 = aVar.b();
        this.d = b2;
        this.a = new byte[b2];
        this.b = new byte[b2];
        this.c = new byte[b2];
    }

    public void a(boolean z, b bVar) {
        boolean z2 = this.f3086f;
        this.f3086f = z;
        if (!(bVar instanceof q.a.a.j.b)) {
            a();
            if (bVar != null) {
                this.f3085e.a(z, bVar);
            } else if (z2 != z) {
                throw new IllegalArgumentException("cannot change encrypting state without providing key.");
            }
        } else if (((q.a.a.j.b) bVar) != null) {
            throw null;
        } else {
            throw null;
        }
    }

    public int b() {
        return this.f3085e.b();
    }

    public int a(byte[] bArr, int i2, byte[] bArr2, int i3) {
        if (!this.f3086f) {
            int i4 = this.d;
            if (i2 + i4 <= bArr.length) {
                System.arraycopy(bArr, i2, this.c, 0, i4);
                int a2 = this.f3085e.a(bArr, i2, bArr2, i3);
                for (int i5 = 0; i5 < this.d; i5++) {
                    int i6 = i3 + i5;
                    bArr2[i6] = (byte) (bArr2[i6] ^ this.b[i5]);
                }
                byte[] bArr3 = this.b;
                this.b = this.c;
                this.c = bArr3;
                return a2;
            }
            throw new DataLengthException("input buffer too short");
        } else if (this.d + i2 <= bArr.length) {
            for (int i7 = 0; i7 < this.d; i7++) {
                byte[] bArr4 = this.b;
                bArr4[i7] = (byte) (bArr4[i7] ^ bArr[i2 + i7]);
            }
            int a3 = this.f3085e.a(this.b, 0, bArr2, i3);
            byte[] bArr5 = this.b;
            System.arraycopy(bArr2, i3, bArr5, 0, bArr5.length);
            return a3;
        } else {
            throw new DataLengthException("input buffer too short");
        }
    }

    public void a() {
        byte[] bArr = this.a;
        System.arraycopy(bArr, 0, this.b, 0, bArr.length);
        Arrays.fill(this.c, (byte) 0);
        this.f3085e.a();
    }
}
