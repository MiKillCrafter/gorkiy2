package q.a.a.g;

import j.a.a.a.outline;
import java.util.Hashtable;
import q.a.a.c;
import q.a.a.d;

public class a {
    public static Hashtable h;
    public c a;
    public int b;
    public int c;
    public q.a.b.a d;

    /* renamed from: e  reason: collision with root package name */
    public q.a.b.a f3083e;

    /* renamed from: f  reason: collision with root package name */
    public byte[] f3084f;
    public byte[] g;

    static {
        Hashtable hashtable = new Hashtable();
        h = hashtable;
        hashtable.put("GOST3411", 32);
        h.put("MD2", 16);
        h.put("MD4", 64);
        h.put("MD5", 64);
        h.put("RIPEMD128", 64);
        h.put("RIPEMD160", 64);
        h.put("SHA-1", 64);
        h.put("SHA-224", 64);
        h.put("SHA-256", 64);
        h.put("SHA-384", 128);
        h.put("SHA-512", 128);
        h.put("Tiger", 64);
        h.put("Whirlpool", 64);
    }

    public a(c cVar) {
        int i2;
        if (cVar instanceof d) {
            i2 = ((d) cVar).e();
        } else {
            Integer num = (Integer) h.get(cVar.c());
            if (num != null) {
                i2 = num.intValue();
            } else {
                StringBuilder a2 = outline.a("unknown digest passed: ");
                a2.append(cVar.c());
                throw new IllegalArgumentException(a2.toString());
            }
        }
        this.a = cVar;
        int d2 = cVar.d();
        this.b = d2;
        this.c = i2;
        this.f3084f = new byte[i2];
        this.g = new byte[(i2 + d2)];
    }

    public int a(byte[] bArr, int i2) {
        this.a.a(this.g, this.c);
        q.a.b.a aVar = this.f3083e;
        if (aVar != null) {
            ((q.a.b.a) this.a).a(aVar);
            c cVar = this.a;
            cVar.a(this.g, this.c, cVar.d());
        } else {
            c cVar2 = this.a;
            byte[] bArr2 = this.g;
            cVar2.a(bArr2, 0, bArr2.length);
        }
        int a2 = this.a.a(bArr, i2);
        int i3 = this.c;
        while (true) {
            byte[] bArr3 = this.g;
            if (i3 >= bArr3.length) {
                break;
            }
            bArr3[i3] = 0;
            i3++;
        }
        q.a.b.a aVar2 = this.d;
        if (aVar2 != null) {
            ((q.a.b.a) this.a).a(aVar2);
        } else {
            c cVar3 = this.a;
            byte[] bArr4 = this.f3084f;
            cVar3.a(bArr4, 0, bArr4.length);
        }
        return a2;
    }
}
