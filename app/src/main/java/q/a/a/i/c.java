package q.a.a.i;

import java.security.SecureRandom;
import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.crypto.OutputLengthException;
import q.a.a.a;
import q.a.a.b;

public class c {
    public byte[] a;
    public int b = 0;
    public boolean c;
    public a d;

    /* renamed from: e  reason: collision with root package name */
    public a f3087e;

    public c(a aVar, a aVar2) {
        this.d = aVar;
        this.f3087e = aVar2;
        this.a = new byte[aVar.b()];
    }

    public int a(int i2) {
        int i3 = i2 + this.b;
        byte[] bArr = this.a;
        int length = i3 % bArr.length;
        if (length != 0) {
            i3 -= length;
        } else if (!this.c) {
            return i3;
        }
        return i3 + bArr.length;
    }

    /* JADX INFO: finally extract failed */
    public int a(byte[] bArr, int i2) {
        int i3;
        int b2 = this.d.b();
        if (this.c) {
            if (this.b != b2) {
                i3 = 0;
            } else if ((b2 * 2) + i2 <= bArr.length) {
                i3 = this.d.a(this.a, 0, bArr, i2);
                this.b = 0;
            } else {
                a();
                throw new OutputLengthException("output buffer too short");
            }
            this.f3087e.a(this.a, this.b);
            int a2 = this.d.a(this.a, 0, bArr, i2 + i3) + i3;
            a();
            return a2;
        } else if (this.b == b2) {
            a aVar = this.d;
            byte[] bArr2 = this.a;
            int a3 = aVar.a(bArr2, 0, bArr2, 0);
            this.b = 0;
            try {
                int a4 = a3 - this.f3087e.a(this.a);
                System.arraycopy(this.a, 0, bArr, i2, a4);
                a();
                return a4;
            } catch (Throwable th) {
                a();
                throw th;
            }
        } else {
            a();
            throw new DataLengthException("last block incomplete in decryption");
        }
    }

    public void a(boolean z, b bVar) {
        this.c = z;
        a();
        if (bVar instanceof q.a.a.j.c) {
            q.a.a.j.c cVar = (q.a.a.j.c) bVar;
            a aVar = this.f3087e;
            if (cVar != null) {
                aVar.a((SecureRandom) null);
                this.d.a(z, null);
                return;
            }
            throw null;
        }
        this.f3087e.a((SecureRandom) null);
        this.d.a(z, bVar);
    }

    public int a(byte[] bArr, int i2, int i3, byte[] bArr2, int i4) {
        if (i3 >= 0) {
            int b2 = this.d.b();
            int i5 = this.b + i3;
            byte[] bArr3 = this.a;
            int length = i5 % bArr3.length;
            int i6 = 0;
            int max = length == 0 ? Math.max(0, i5 - bArr3.length) : i5 - length;
            if (max <= 0 || max + i4 <= bArr2.length) {
                byte[] bArr4 = this.a;
                int length2 = bArr4.length;
                int i7 = this.b;
                int i8 = length2 - i7;
                if (i3 > i8) {
                    System.arraycopy(bArr, i2, bArr4, i7, i8);
                    this.b = 0;
                    i3 -= i8;
                    i2 += i8;
                    i6 = this.d.a(this.a, 0, bArr2, i4) + 0;
                    while (i3 > this.a.length) {
                        i6 += this.d.a(bArr, i2, bArr2, i4 + i6);
                        i3 -= b2;
                        i2 += b2;
                    }
                }
                System.arraycopy(bArr, i2, this.a, this.b, i3);
                this.b += i3;
                return i6;
            }
            throw new OutputLengthException("output buffer too short");
        }
        throw new IllegalArgumentException("Can't have a negative input length!");
    }

    public void a() {
        int i2 = 0;
        while (true) {
            byte[] bArr = this.a;
            if (i2 < bArr.length) {
                bArr[i2] = 0;
                i2++;
            } else {
                this.b = 0;
                this.d.a();
                return;
            }
        }
    }
}
