package l.a.a.a.o.c;

import java.util.Collection;

public interface Dependency<T> {
    void addDependency(Object obj);

    boolean areDependenciesMet();

    Collection<T> getDependencies();
}
