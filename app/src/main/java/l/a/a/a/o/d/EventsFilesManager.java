package l.a.a.a.o.d;

import android.content.Context;
import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TreeSet;
import java.util.concurrent.CopyOnWriteArrayList;
import l.a.a.a.o.b.CommonUtils;
import l.a.a.a.o.b.CurrentTimeProvider;
import l.a.a.a.o.b.SystemCurrentTimeProvider;
import l.a.a.a.o.b.k;
import l.a.a.a.o.d.b;

public abstract class EventsFilesManager<T> {
    public static final int MAX_BYTE_SIZE_PER_FILE = 8000;
    public static final int MAX_FILES_IN_BATCH = 1;
    public static final int MAX_FILES_TO_KEEP = 100;
    public static final String ROLL_OVER_FILE_NAME_SEPARATOR = "_";
    public final Context context;
    public final CurrentTimeProvider currentTimeProvider;
    public final int defaultMaxFilesToKeep;
    public final EventsStorage eventStorage;
    public volatile long lastRollOverTime;
    public final List<d> rollOverListeners = new CopyOnWriteArrayList();
    public final EventTransform<T> transform;

    public class a implements Comparator<b.b> {
        public a(EventsFilesManager eventsFilesManager) {
        }

        public int compare(Object obj, Object obj2) {
            return (int) (((b) obj).b - ((b) obj2).b);
        }
    }

    public static class b {
        public final File a;
        public final long b;

        public b(File file, long j2) {
            this.a = file;
            this.b = j2;
        }
    }

    public EventsFilesManager(Context context2, EventTransform<T> eventTransform, k kVar, c cVar, int i2) {
        this.context = context2.getApplicationContext();
        this.transform = eventTransform;
        this.eventStorage = cVar;
        this.currentTimeProvider = kVar;
        if (((SystemCurrentTimeProvider) kVar) != null) {
            this.lastRollOverTime = System.currentTimeMillis();
            this.defaultMaxFilesToKeep = i2;
            return;
        }
        throw null;
    }

    private void rollFileOverIfNeeded(int i2) {
        if (!this.eventStorage.a(i2, getMaxByteSizePerFile())) {
            CommonUtils.a(this.context, 4, String.format(Locale.US, "session analytics events file is %d bytes, new event is %d bytes, this is over flush limit of %d, rolling it over", Integer.valueOf(this.eventStorage.c()), Integer.valueOf(i2), Integer.valueOf(getMaxByteSizePerFile())));
            rollFileOver();
        }
    }

    private void triggerRollOverOnListeners(String str) {
        Iterator<d> it = this.rollOverListeners.iterator();
        while (it.hasNext()) {
            try {
                it.next().onRollOver(str);
            } catch (Exception unused) {
                CommonUtils.c(this.context, "One of the roll over listeners threw an exception");
            }
        }
    }

    public void deleteAllEventsFiles() {
        EventsStorage eventsStorage = this.eventStorage;
        eventsStorage.a(eventsStorage.a());
        this.eventStorage.b();
    }

    public void deleteOldestInRollOverIfOverMax() {
        List<File> a2 = this.eventStorage.a();
        int maxFilesToKeep = getMaxFilesToKeep();
        if (a2.size() > maxFilesToKeep) {
            int size = a2.size() - maxFilesToKeep;
            CommonUtils.b(this.context, String.format(Locale.US, "Found %d files in  roll over directory, this is greater than %d, deleting %d oldest files", Integer.valueOf(a2.size()), Integer.valueOf(maxFilesToKeep), Integer.valueOf(size)));
            TreeSet treeSet = new TreeSet(new a(this));
            for (File next : a2) {
                treeSet.add(new b(next, parseCreationTimestampFromFileName(next.getName())));
            }
            ArrayList arrayList = new ArrayList();
            Iterator it = treeSet.iterator();
            while (it.hasNext()) {
                arrayList.add(((b) it.next()).a);
                if (arrayList.size() == size) {
                    break;
                }
            }
            this.eventStorage.a(arrayList);
        }
    }

    public void deleteSentFiles(List<File> list) {
        this.eventStorage.a(list);
    }

    public abstract String generateUniqueRollOverFileName();

    public List<File> getBatchOfFilesToSend() {
        return this.eventStorage.a(1);
    }

    public long getLastRollOverTime() {
        return this.lastRollOverTime;
    }

    public int getMaxByteSizePerFile() {
        return MAX_BYTE_SIZE_PER_FILE;
    }

    public int getMaxFilesToKeep() {
        return this.defaultMaxFilesToKeep;
    }

    public long parseCreationTimestampFromFileName(String str) {
        String[] split = str.split(ROLL_OVER_FILE_NAME_SEPARATOR);
        if (split.length != 3) {
            return 0;
        }
        try {
            return Long.valueOf(split[2]).longValue();
        } catch (NumberFormatException unused) {
            return 0;
        }
    }

    public void registerRollOverListener(EventsStorageListener eventsStorageListener) {
        if (eventsStorageListener != null) {
            this.rollOverListeners.add(eventsStorageListener);
        }
    }

    public boolean rollFileOver() {
        boolean z = true;
        String str = null;
        if (!this.eventStorage.d()) {
            String generateUniqueRollOverFileName = generateUniqueRollOverFileName();
            this.eventStorage.a(generateUniqueRollOverFileName);
            CommonUtils.a(this.context, 4, String.format(Locale.US, "generated new file %s", generateUniqueRollOverFileName));
            if (((SystemCurrentTimeProvider) this.currentTimeProvider) != null) {
                this.lastRollOverTime = System.currentTimeMillis();
                str = generateUniqueRollOverFileName;
            } else {
                throw null;
            }
        } else {
            z = false;
        }
        triggerRollOverOnListeners(str);
        return z;
    }

    public void writeEvent(T t2) {
        byte[] bytes = this.transform.toBytes(t2);
        rollFileOverIfNeeded(bytes.length);
        this.eventStorage.a(bytes);
    }
}
