package l.a.a.a.o.g;

import l.a.a.a.Kit;
import l.a.a.a.o.e.HttpMethod;
import l.a.a.a.o.e.HttpRequestFactory;

public class CreateAppSpiCall extends AbstractAppSpiCall {
    public CreateAppSpiCall(Kit kit, String str, String str2, HttpRequestFactory httpRequestFactory) {
        super(kit, str, str2, httpRequestFactory, HttpMethod.POST);
    }
}
