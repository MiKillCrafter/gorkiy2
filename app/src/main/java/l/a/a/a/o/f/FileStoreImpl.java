package l.a.a.a.o.f;

import android.content.Context;
import android.util.Log;
import java.io.File;
import l.a.a.a.Fabric;
import l.a.a.a.Kit;

public class FileStoreImpl implements FileStore {
    public final Context a;

    public FileStoreImpl(Kit kit) {
        if (kit.getContext() != null) {
            this.a = kit.getContext();
            kit.getPath();
            this.a.getPackageName();
            return;
        }
        throw new IllegalStateException("Cannot get directory before context has been set. Call Fabric.with() first");
    }

    public File a() {
        File filesDir = this.a.getFilesDir();
        if (filesDir != null) {
            if (filesDir.exists() || filesDir.mkdirs()) {
                return filesDir;
            }
            if (Fabric.a().a("Fabric", 5)) {
                Log.w("Fabric", "Couldn't create file", null);
            }
        } else if (Fabric.a().a("Fabric", 3)) {
            Log.d("Fabric", "Null File", null);
        }
        return null;
    }
}
