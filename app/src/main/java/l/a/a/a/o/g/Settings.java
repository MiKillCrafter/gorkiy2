package l.a.a.a.o.g;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;
import l.a.a.a.Fabric;
import l.a.a.a.Kit;
import l.a.a.a.o.b.ApiKey;
import l.a.a.a.o.b.CommonUtils;
import l.a.a.a.o.b.DataCollectionArbiter;
import l.a.a.a.o.b.DeliveryMechanism;
import l.a.a.a.o.b.IdManager;
import l.a.a.a.o.b.SystemCurrentTimeProvider;
import l.a.a.a.o.e.HttpRequestFactory;

public class Settings {
    public final AtomicReference<s> a = new AtomicReference<>();
    public final CountDownLatch b = new CountDownLatch(1);
    public SettingsController c;
    public boolean d = false;

    public static class b {
        public static final Settings a = new Settings(null);
    }

    public /* synthetic */ Settings(a aVar) {
    }

    public synchronized Settings a(Kit kit, IdManager idManager, HttpRequestFactory httpRequestFactory, String str, String str2, String str3, DataCollectionArbiter dataCollectionArbiter) {
        Kit kit2 = kit;
        IdManager idManager2 = idManager;
        synchronized (this) {
            if (this.d) {
                return this;
            }
            if (this.c == null) {
                Context context = kit.getContext();
                String str4 = idManager2.f2635f;
                String c2 = new ApiKey().c(context);
                String d2 = idManager.d();
                SystemCurrentTimeProvider systemCurrentTimeProvider = new SystemCurrentTimeProvider();
                DefaultSettingsJsonTransform defaultSettingsJsonTransform = new DefaultSettingsJsonTransform();
                DefaultCachedSettingsIo defaultCachedSettingsIo = new DefaultCachedSettingsIo(kit2);
                String b2 = CommonUtils.b(context);
                String str5 = str3;
                DefaultSettingsSpiCall defaultSettingsSpiCall = new DefaultSettingsSpiCall(kit2, str5, String.format(Locale.US, "https://settings.crashlytics.com/spi/v2/platforms/android/apps/%s/settings", str4), httpRequestFactory);
                String e2 = idManager.e();
                String a2 = idManager2.a(Build.VERSION.INCREMENTAL);
                String str6 = a2;
                this.c = new DefaultSettingsController(kit, new SettingsRequest(c2, e2, str6, idManager2.a(Build.VERSION.RELEASE), idManager.b(), CommonUtils.a(CommonUtils.j(context)), str2, str, DeliveryMechanism.a(d2).id, b2), systemCurrentTimeProvider, defaultSettingsJsonTransform, defaultCachedSettingsIo, defaultSettingsSpiCall, dataCollectionArbiter);
            }
            this.d = true;
            return this;
        }
    }

    public synchronized boolean b() {
        SettingsData b2;
        DefaultSettingsController defaultSettingsController = (DefaultSettingsController) this.c;
        if (defaultSettingsController != null) {
            b2 = defaultSettingsController.b(SettingsCacheBehavior.USE_CACHE);
            this.a.set(b2);
            this.b.countDown();
        } else {
            throw null;
        }
        return b2 != null;
    }

    public synchronized boolean c() {
        SettingsData b2;
        b2 = ((DefaultSettingsController) this.c).b(SettingsCacheBehavior.SKIP_CACHE_LOOKUP);
        this.a.set(b2);
        this.b.countDown();
        if (b2 == null && Fabric.a().a("Fabric", 6)) {
            Log.e("Fabric", "Failed to force reload of settings from Crashlytics.", null);
        }
        return b2 != null;
    }

    public SettingsData a() {
        try {
            this.b.await();
            return this.a.get();
        } catch (InterruptedException unused) {
            if (Fabric.a().a("Fabric", 6)) {
                Log.e("Fabric", "Interrupted while waiting for settings data.", null);
            }
            return null;
        }
    }
}
