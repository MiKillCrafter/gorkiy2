package l.a.a.a.o.g;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.util.Log;
import l.a.a.a.Fabric;
import l.a.a.a.o.b.CommonUtils;

public class IconRequest {
    public final String a;
    public final int b;
    public final int c;
    public final int d;

    public IconRequest(String str, int i2, int i3, int i4) {
        this.a = str;
        this.b = i2;
        this.c = i3;
        this.d = i4;
    }

    public static IconRequest a(Context context, String str) {
        if (str == null) {
            return null;
        }
        try {
            int c2 = CommonUtils.c(context);
            String str2 = "App icon resource ID is " + c2;
            if (Fabric.a().a("Fabric", 3)) {
                Log.d("Fabric", str2, null);
            }
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeResource(context.getResources(), c2, options);
            return new IconRequest(str, c2, options.outWidth, options.outHeight);
        } catch (Exception e2) {
            if (!Fabric.a().a("Fabric", 6)) {
                return null;
            }
            Log.e("Fabric", "Failed to load icon", e2);
            return null;
        }
    }
}
