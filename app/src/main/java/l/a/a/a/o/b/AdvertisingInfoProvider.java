package l.a.a.a.o.b;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;
import l.a.a.a.Fabric;
import l.a.a.a.o.f.PreferenceStore;
import l.a.a.a.o.f.PreferenceStoreImpl;

public class AdvertisingInfoProvider {
    public final Context a;
    public final PreferenceStore b;

    public AdvertisingInfoProvider(Context context) {
        this.a = context.getApplicationContext();
        this.b = new PreferenceStoreImpl(context, "TwitterAdvertisingInfoPreferences");
    }

    public AdvertisingInfo a() {
        AdvertisingInfo advertisingInfo = new AdvertisingInfo(((PreferenceStoreImpl) this.b).a.getString("advertising_id", ""), ((PreferenceStoreImpl) this.b).a.getBoolean("limit_ad_tracking_enabled", false));
        if (a(advertisingInfo)) {
            if (Fabric.a().a("Fabric", 3)) {
                Log.d("Fabric", "Using AdvertisingInfo from Preference Store", null);
            }
            new Thread(new AdvertisingInfoProvider0(this, advertisingInfo)).start();
            return advertisingInfo;
        }
        AdvertisingInfo b2 = b();
        b(b2);
        return b2;
    }

    @SuppressLint({"CommitPrefEdits"})
    public final void b(AdvertisingInfo advertisingInfo) {
        if (a(advertisingInfo)) {
            PreferenceStore preferenceStore = this.b;
            SharedPreferences.Editor putBoolean = ((PreferenceStoreImpl) preferenceStore).a().putString("advertising_id", advertisingInfo.a).putBoolean("limit_ad_tracking_enabled", advertisingInfo.b);
            if (((PreferenceStoreImpl) preferenceStore) != null) {
                putBoolean.apply();
                return;
            }
            throw null;
        }
        PreferenceStore preferenceStore2 = this.b;
        SharedPreferences.Editor remove = ((PreferenceStoreImpl) preferenceStore2).a().remove("advertising_id").remove("limit_ad_tracking_enabled");
        if (((PreferenceStoreImpl) preferenceStore2) != null) {
            remove.apply();
            return;
        }
        throw null;
    }

    public final AdvertisingInfo b() {
        AdvertisingInfo a2 = new AdvertisingInfoReflectionStrategy(this.a).a();
        if (!a(a2)) {
            a2 = new AdvertisingInfoServiceStrategy(this.a).a();
            if (!a(a2)) {
                if (Fabric.a().a("Fabric", 3)) {
                    Log.d("Fabric", "AdvertisingInfo not present", null);
                }
            } else if (Fabric.a().a("Fabric", 3)) {
                Log.d("Fabric", "Using AdvertisingInfo from Service Provider", null);
            }
        } else if (Fabric.a().a("Fabric", 3)) {
            Log.d("Fabric", "Using AdvertisingInfo from Reflection Provider", null);
        }
        return a2;
    }

    public final boolean a(AdvertisingInfo advertisingInfo) {
        return advertisingInfo != null && !TextUtils.isEmpty(advertisingInfo.a);
    }
}
