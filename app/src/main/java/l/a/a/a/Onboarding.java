package l.a.a.a;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import com.crashlytics.android.core.SessionProtobufHelper;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;
import l.a.a.a.o.b.ApiKey;
import l.a.a.a.o.b.CommonUtils;
import l.a.a.a.o.b.DataCollectionArbiter;
import l.a.a.a.o.b.DeliveryMechanism;
import l.a.a.a.o.e.DefaultHttpRequestFactory;
import l.a.a.a.o.e.HttpRequestFactory;
import l.a.a.a.o.g.AppRequestData;
import l.a.a.a.o.g.CreateAppSpiCall;
import l.a.a.a.o.g.IconRequest;
import l.a.a.a.o.g.Settings;
import l.a.a.a.o.g.SettingsData;
import l.a.a.a.o.g.UpdateAppSpiCall;
import l.a.a.a.o.g.d;
import l.a.a.a.o.g.e;
import l.a.a.a.o.g.m;

public class Onboarding extends Kit<Boolean> {
    public final HttpRequestFactory b = new DefaultHttpRequestFactory();
    public PackageManager c;
    public String d;

    /* renamed from: e  reason: collision with root package name */
    public PackageInfo f2623e;

    /* renamed from: f  reason: collision with root package name */
    public String f2624f;
    public String g;
    public String h;

    /* renamed from: i  reason: collision with root package name */
    public String f2625i;

    /* renamed from: j  reason: collision with root package name */
    public String f2626j;

    /* renamed from: k  reason: collision with root package name */
    public final Future<Map<String, m>> f2627k;

    /* renamed from: l  reason: collision with root package name */
    public final Collection<k> f2628l;

    public Onboarding(Future<Map<String, m>> future, Collection<k> collection) {
        this.f2627k = future;
        this.f2628l = collection;
    }

    public final boolean a(String str, e eVar, Collection<m> collection) {
        if ("new".equals(eVar.a)) {
            if (new CreateAppSpiCall(super, getOverridenSpiEndpoint(), eVar.b, this.b).a(a(IconRequest.a(getContext(), str), collection))) {
                return Settings.b.a.c();
            }
            if (Fabric.a().a("Fabric", 6)) {
                Log.e("Fabric", "Failed to create app with Crashlytics service.", null);
            }
            return false;
        } else if ("configured".equals(eVar.a)) {
            return Settings.b.a.c();
        } else {
            if (eVar.f2665e) {
                if (Fabric.a().a("Fabric", 3)) {
                    Log.d("Fabric", "Server says an update is required - forcing a full App update.", null);
                }
                new UpdateAppSpiCall(super, getOverridenSpiEndpoint(), eVar.b, this.b).a(a(IconRequest.a(getContext(), str), collection));
            }
            return true;
        }
    }

    public Object doInBackground() {
        SettingsData settingsData;
        boolean z;
        Map map;
        String b2 = CommonUtils.b(getContext());
        try {
            Settings settings = Settings.b.a;
            settings.a(super, super.idManager, this.b, this.f2624f, this.g, getOverridenSpiEndpoint(), DataCollectionArbiter.a(getContext()));
            settings.b();
            settingsData = Settings.b.a.a();
        } catch (Exception e2) {
            if (Fabric.a().a("Fabric", 6)) {
                Log.e("Fabric", "Error dealing with settings", e2);
            }
            settingsData = null;
        }
        if (settingsData != null) {
            try {
                if (this.f2627k != null) {
                    map = this.f2627k.get();
                } else {
                    map = new HashMap();
                }
                for (Kit next : this.f2628l) {
                    if (!map.containsKey(super.getIdentifier())) {
                        map.put(super.getIdentifier(), new KitInfo(super.getIdentifier(), super.getVersion(), "binary"));
                    }
                }
                z = a(b2, settingsData.a, map.values());
            } catch (Exception e3) {
                if (Fabric.a().a("Fabric", 6)) {
                    Log.e("Fabric", "Error performing auto configuration.", e3);
                }
            }
            return Boolean.valueOf(z);
        }
        z = false;
        return Boolean.valueOf(z);
    }

    public String getIdentifier() {
        return "io.fabric.sdk.android:fabric";
    }

    public String getOverridenSpiEndpoint() {
        return CommonUtils.a(getContext(), "com.crashlytics.ApiEndpoint");
    }

    public String getVersion() {
        return "1.4.8.32";
    }

    public boolean onPreExecute() {
        try {
            this.h = getIdManager().d();
            this.c = getContext().getPackageManager();
            String packageName = getContext().getPackageName();
            this.d = packageName;
            PackageInfo packageInfo = this.c.getPackageInfo(packageName, 0);
            this.f2623e = packageInfo;
            this.f2624f = Integer.toString(packageInfo.versionCode);
            this.g = this.f2623e.versionName == null ? "0.0" : this.f2623e.versionName;
            this.f2625i = this.c.getApplicationLabel(getContext().getApplicationInfo()).toString();
            this.f2626j = Integer.toString(getContext().getApplicationInfo().targetSdkVersion);
            return true;
        } catch (PackageManager.NameNotFoundException e2) {
            if (Fabric.a().a("Fabric", 6)) {
                Log.e("Fabric", "Failed init", e2);
            }
            return false;
        }
    }

    public final d a(m mVar, Collection<m> collection) {
        Context context = getContext();
        return new AppRequestData(new ApiKey().c(context), getIdManager().f2635f, this.g, this.f2624f, CommonUtils.a(CommonUtils.j(context)), this.f2625i, DeliveryMechanism.a(this.h).id, this.f2626j, SessionProtobufHelper.SIGNAL_DEFAULT, mVar, collection);
    }
}
