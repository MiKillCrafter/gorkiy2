package l.a.a.a.o.a;

import android.content.Context;

public abstract class AbstractValueCache<T> {
    public final AbstractValueCache<T> a;

    public AbstractValueCache(AbstractValueCache<T> abstractValueCache) {
        this.a = abstractValueCache;
    }

    public final synchronized T a(Context context, ValueLoader<T> valueLoader) {
        T t2;
        t2 = ((MemoryValueCache) this).b;
        if (t2 == null) {
            t2 = this.a != null ? this.a.a(context, valueLoader) : valueLoader.load(context);
            if (t2 != null) {
                ((MemoryValueCache) this).b = t2;
            } else {
                throw null;
            }
        }
        return t2;
    }
}
