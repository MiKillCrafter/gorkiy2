package l.a.a.a.o.g;

import android.util.Log;
import j.a.a.a.outline;
import l.a.a.a.DefaultLogger;
import l.a.a.a.Fabric;
import l.a.a.a.Kit;
import l.a.a.a.o.b.CommonUtils;
import l.a.a.a.o.b.CurrentTimeProvider;
import l.a.a.a.o.b.DataCollectionArbiter;
import l.a.a.a.o.f.PreferenceStore;
import l.a.a.a.o.f.PreferenceStoreImpl;
import org.json.JSONObject;

public class DefaultSettingsController implements SettingsController {
    public final SettingsRequest a;
    public final DefaultSettingsJsonTransform b;
    public final CurrentTimeProvider c;
    public final DefaultCachedSettingsIo d;

    /* renamed from: e  reason: collision with root package name */
    public final SettingsSpiCall f2666e;

    /* renamed from: f  reason: collision with root package name */
    public final Kit f2667f;
    public final PreferenceStore g;
    public final DataCollectionArbiter h;

    public DefaultSettingsController(Kit kit, SettingsRequest settingsRequest, CurrentTimeProvider currentTimeProvider, DefaultSettingsJsonTransform defaultSettingsJsonTransform, DefaultCachedSettingsIo defaultCachedSettingsIo, SettingsSpiCall settingsSpiCall, DataCollectionArbiter dataCollectionArbiter) {
        this.f2667f = kit;
        this.a = settingsRequest;
        this.c = currentTimeProvider;
        this.b = defaultSettingsJsonTransform;
        this.d = defaultCachedSettingsIo;
        this.f2666e = settingsSpiCall;
        this.h = dataCollectionArbiter;
        this.g = new PreferenceStoreImpl(kit.getContext(), kit.getClass().getName());
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:40:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final l.a.a.a.o.g.SettingsData a(l.a.a.a.o.g.SettingsCacheBehavior r10) {
        /*
            r9 = this;
            java.lang.String r0 = "Fabric"
            r1 = 0
            l.a.a.a.o.g.SettingsCacheBehavior r2 = l.a.a.a.o.g.SettingsCacheBehavior.SKIP_CACHE_LOOKUP     // Catch:{ Exception -> 0x0074 }
            boolean r2 = r2.equals(r10)     // Catch:{ Exception -> 0x0074 }
            if (r2 != 0) goto L_0x0085
            l.a.a.a.o.g.DefaultCachedSettingsIo r2 = r9.d     // Catch:{ Exception -> 0x0074 }
            org.json.JSONObject r2 = r2.a()     // Catch:{ Exception -> 0x0074 }
            r3 = 3
            if (r2 == 0) goto L_0x0064
            l.a.a.a.o.g.DefaultSettingsJsonTransform r4 = r9.b     // Catch:{ Exception -> 0x0074 }
            l.a.a.a.o.b.CurrentTimeProvider r5 = r9.c     // Catch:{ Exception -> 0x0074 }
            l.a.a.a.o.g.SettingsData r4 = r4.a(r5, r2)     // Catch:{ Exception -> 0x0074 }
            java.lang.String r5 = "Loaded cached settings: "
            r9.a(r2, r5)     // Catch:{ Exception -> 0x0074 }
            l.a.a.a.o.b.CurrentTimeProvider r2 = r9.c     // Catch:{ Exception -> 0x0074 }
            l.a.a.a.o.b.SystemCurrentTimeProvider r2 = (l.a.a.a.o.b.SystemCurrentTimeProvider) r2     // Catch:{ Exception -> 0x0074 }
            if (r2 == 0) goto L_0x0063
            long r5 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0074 }
            l.a.a.a.o.g.SettingsCacheBehavior r2 = l.a.a.a.o.g.SettingsCacheBehavior.IGNORE_CACHE_EXPIRATION     // Catch:{ Exception -> 0x0074 }
            boolean r10 = r2.equals(r10)     // Catch:{ Exception -> 0x0074 }
            if (r10 != 0) goto L_0x004f
            long r7 = r4.f2671f     // Catch:{ Exception -> 0x0074 }
            int r10 = (r7 > r5 ? 1 : (r7 == r5 ? 0 : -1))
            if (r10 >= 0) goto L_0x003b
            r10 = 1
            goto L_0x003c
        L_0x003b:
            r10 = 0
        L_0x003c:
            if (r10 != 0) goto L_0x003f
            goto L_0x004f
        L_0x003f:
            l.a.a.a.DefaultLogger r10 = l.a.a.a.Fabric.a()     // Catch:{ Exception -> 0x0074 }
            java.lang.String r2 = "Cached settings have expired."
            boolean r10 = r10.a(r0, r3)     // Catch:{ Exception -> 0x0074 }
            if (r10 == 0) goto L_0x0085
            android.util.Log.d(r0, r2, r1)     // Catch:{ Exception -> 0x0074 }
            goto L_0x0085
        L_0x004f:
            l.a.a.a.DefaultLogger r10 = l.a.a.a.Fabric.a()     // Catch:{ Exception -> 0x0060 }
            java.lang.String r2 = "Returning cached settings."
            boolean r10 = r10.a(r0, r3)     // Catch:{ Exception -> 0x0060 }
            if (r10 == 0) goto L_0x005e
            android.util.Log.d(r0, r2, r1)     // Catch:{ Exception -> 0x0060 }
        L_0x005e:
            r1 = r4
            goto L_0x0085
        L_0x0060:
            r10 = move-exception
            r1 = r4
            goto L_0x0075
        L_0x0063:
            throw r1     // Catch:{ Exception -> 0x0074 }
        L_0x0064:
            l.a.a.a.DefaultLogger r10 = l.a.a.a.Fabric.a()     // Catch:{ Exception -> 0x0074 }
            java.lang.String r2 = "No cached settings data found."
            boolean r10 = r10.a(r0, r3)     // Catch:{ Exception -> 0x0074 }
            if (r10 == 0) goto L_0x0085
            android.util.Log.d(r0, r2, r1)     // Catch:{ Exception -> 0x0074 }
            goto L_0x0085
        L_0x0074:
            r10 = move-exception
        L_0x0075:
            l.a.a.a.DefaultLogger r2 = l.a.a.a.Fabric.a()
            r3 = 6
            boolean r2 = r2.a(r0, r3)
            if (r2 == 0) goto L_0x0085
            java.lang.String r2 = "Failed to get cached settings"
            android.util.Log.e(r0, r2, r10)
        L_0x0085:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: l.a.a.a.o.g.DefaultSettingsController.a(l.a.a.a.o.g.SettingsCacheBehavior):l.a.a.a.o.g.SettingsData");
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0044 A[SYNTHETIC, Splitter:B:16:0x0044] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0085 A[SYNTHETIC, Splitter:B:30:0x0085] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0098  */
    /* JADX WARNING: Removed duplicated region for block: B:37:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:38:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public l.a.a.a.o.g.SettingsData b(l.a.a.a.o.g.SettingsCacheBehavior r8) {
        /*
            r7 = this;
            l.a.a.a.o.b.DataCollectionArbiter r0 = r7.h
            boolean r0 = r0.a()
            java.lang.String r1 = "Fabric"
            r2 = 0
            if (r0 != 0) goto L_0x001c
            l.a.a.a.DefaultLogger r8 = l.a.a.a.Fabric.a()
            r0 = 3
            boolean r8 = r8.a(r1, r0)
            if (r8 == 0) goto L_0x001b
            java.lang.String r8 = "Not fetching settings, because data collection is disabled by Firebase."
            android.util.Log.d(r1, r8, r2)
        L_0x001b:
            return r2
        L_0x001c:
            boolean r0 = l.a.a.a.Fabric.b()     // Catch:{ Exception -> 0x008c }
            java.lang.String r3 = "existing_instance_identifier"
            if (r0 != 0) goto L_0x0041
            l.a.a.a.o.f.PreferenceStore r0 = r7.g     // Catch:{ Exception -> 0x008c }
            l.a.a.a.o.f.PreferenceStoreImpl r0 = (l.a.a.a.o.f.PreferenceStoreImpl) r0     // Catch:{ Exception -> 0x008c }
            android.content.SharedPreferences r0 = r0.a     // Catch:{ Exception -> 0x008c }
            java.lang.String r4 = ""
            java.lang.String r0 = r0.getString(r3, r4)     // Catch:{ Exception -> 0x008c }
            java.lang.String r4 = r7.a()     // Catch:{ Exception -> 0x008c }
            boolean r0 = r0.equals(r4)     // Catch:{ Exception -> 0x008c }
            r0 = r0 ^ 1
            if (r0 != 0) goto L_0x0041
            l.a.a.a.o.g.SettingsData r8 = r7.a(r8)     // Catch:{ Exception -> 0x008c }
            goto L_0x0042
        L_0x0041:
            r8 = r2
        L_0x0042:
            if (r8 != 0) goto L_0x0082
            l.a.a.a.o.g.SettingsSpiCall r0 = r7.f2666e     // Catch:{ Exception -> 0x007e }
            l.a.a.a.o.g.SettingsRequest r4 = r7.a     // Catch:{ Exception -> 0x007e }
            l.a.a.a.o.g.DefaultSettingsSpiCall r0 = (l.a.a.a.o.g.DefaultSettingsSpiCall) r0
            org.json.JSONObject r0 = r0.b(r4)     // Catch:{ Exception -> 0x007e }
            if (r0 == 0) goto L_0x0082
            l.a.a.a.o.g.DefaultSettingsJsonTransform r4 = r7.b     // Catch:{ Exception -> 0x007e }
            l.a.a.a.o.b.CurrentTimeProvider r5 = r7.c     // Catch:{ Exception -> 0x007e }
            l.a.a.a.o.g.SettingsData r8 = r4.a(r5, r0)     // Catch:{ Exception -> 0x007e }
            l.a.a.a.o.g.DefaultCachedSettingsIo r4 = r7.d     // Catch:{ Exception -> 0x007e }
            long r5 = r8.f2671f     // Catch:{ Exception -> 0x007e }
            r4.a(r5, r0)     // Catch:{ Exception -> 0x007e }
            java.lang.String r4 = "Loaded settings: "
            r7.a(r0, r4)     // Catch:{ Exception -> 0x007e }
            java.lang.String r0 = r7.a()     // Catch:{ Exception -> 0x007e }
            l.a.a.a.o.f.PreferenceStore r4 = r7.g     // Catch:{ Exception -> 0x007e }
            l.a.a.a.o.f.PreferenceStoreImpl r4 = (l.a.a.a.o.f.PreferenceStoreImpl) r4     // Catch:{ Exception -> 0x007e }
            android.content.SharedPreferences$Editor r4 = r4.a()     // Catch:{ Exception -> 0x007e }
            r4.putString(r3, r0)     // Catch:{ Exception -> 0x007e }
            l.a.a.a.o.f.PreferenceStore r0 = r7.g     // Catch:{ Exception -> 0x007e }
            l.a.a.a.o.f.PreferenceStoreImpl r0 = (l.a.a.a.o.f.PreferenceStoreImpl) r0     // Catch:{ Exception -> 0x007e }
            if (r0 == 0) goto L_0x007d
            r4.apply()     // Catch:{ Exception -> 0x007e }
            goto L_0x0082
        L_0x007d:
            throw r2     // Catch:{ Exception -> 0x007e }
        L_0x007e:
            r0 = move-exception
            r2 = r8
            r8 = r0
            goto L_0x008d
        L_0x0082:
            r2 = r8
            if (r2 != 0) goto L_0x009d
            l.a.a.a.o.g.SettingsCacheBehavior r8 = l.a.a.a.o.g.SettingsCacheBehavior.IGNORE_CACHE_EXPIRATION     // Catch:{ Exception -> 0x008c }
            l.a.a.a.o.g.SettingsData r2 = r7.a(r8)     // Catch:{ Exception -> 0x008c }
            goto L_0x009d
        L_0x008c:
            r8 = move-exception
        L_0x008d:
            l.a.a.a.DefaultLogger r0 = l.a.a.a.Fabric.a()
            r3 = 6
            boolean r0 = r0.a(r1, r3)
            if (r0 == 0) goto L_0x009d
            java.lang.String r0 = "Unknown error while loading Crashlytics settings. Crashes will be cached until settings can be retrieved."
            android.util.Log.e(r1, r0, r8)
        L_0x009d:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: l.a.a.a.o.g.DefaultSettingsController.b(l.a.a.a.o.g.SettingsCacheBehavior):l.a.a.a.o.g.SettingsData");
    }

    public final void a(JSONObject jSONObject, String str) {
        DefaultLogger a2 = Fabric.a();
        StringBuilder a3 = outline.a(str);
        a3.append(jSONObject.toString());
        String sb = a3.toString();
        if (a2.a("Fabric", 3)) {
            Log.d("Fabric", sb, null);
        }
    }

    public String a() {
        return CommonUtils.a(CommonUtils.j(this.f2667f.getContext()));
    }
}
