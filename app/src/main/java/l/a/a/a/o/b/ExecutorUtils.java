package l.a.a.a.o.b;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicLong;

public final class ExecutorUtils implements ThreadFactory {
    public final /* synthetic */ String b;
    public final /* synthetic */ AtomicLong c;

    public class a extends BackgroundPriorityRunnable {
        public final /* synthetic */ Runnable b;

        public a(ExecutorUtils executorUtils, Runnable runnable) {
            this.b = runnable;
        }

        public void onRun() {
            this.b.run();
        }
    }

    public ExecutorUtils(String str, AtomicLong atomicLong) {
        this.b = str;
        this.c = atomicLong;
    }

    public Thread newThread(Runnable runnable) {
        Thread newThread = Executors.defaultThreadFactory().newThread(new a(this, runnable));
        newThread.setName(this.b + this.c.getAndIncrement());
        return newThread;
    }
}
