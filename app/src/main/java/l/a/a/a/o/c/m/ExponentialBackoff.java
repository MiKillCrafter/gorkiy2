package l.a.a.a.o.c.m;

public class ExponentialBackoff implements Backoff {
    public final long a;
    public final int b;

    public ExponentialBackoff(long j2, int i2) {
        this.a = j2;
        this.b = i2;
    }

    public long getDelayMillis(int i2) {
        return (long) (Math.pow((double) this.b, (double) i2) * ((double) this.a));
    }
}
