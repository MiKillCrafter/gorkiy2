package l.a.a.a.o.a;

import android.content.Context;

public interface ValueLoader<T> {
    T load(Context context);
}
