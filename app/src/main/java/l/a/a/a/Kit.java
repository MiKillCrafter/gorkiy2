package l.a.a.a;

import android.content.Context;
import j.a.a.a.outline;
import java.io.File;
import java.util.Collection;
import l.a.a.a.o.b.IdManager;
import l.a.a.a.o.b.s;
import l.a.a.a.o.c.DependsOn;
import l.a.a.a.o.c.l;

public abstract class Kit<Result> implements Comparable<k> {
    public Context context;
    public final DependsOn dependsOnAnnotation = ((DependsOn) getClass().getAnnotation(DependsOn.class));
    public Fabric fabric;
    public IdManager idManager;
    public InitializationCallback<Result> initializationCallback;
    public InitializationTask<Result> initializationTask = new InitializationTask<>(this);

    public boolean containsAnnotatedDependency(Kit kit) {
        if (hasAnnotatedDependency()) {
            for (Class<?> isAssignableFrom : this.dependsOnAnnotation.value()) {
                if (isAssignableFrom.isAssignableFrom(kit.getClass())) {
                    return true;
                }
            }
        }
        return false;
    }

    public abstract Result doInBackground();

    public Context getContext() {
        return this.context;
    }

    public Collection<l> getDependencies() {
        return this.initializationTask.getDependencies();
    }

    public Fabric getFabric() {
        return this.fabric;
    }

    public IdManager getIdManager() {
        return this.idManager;
    }

    public abstract String getIdentifier();

    public String getPath() {
        StringBuilder a = outline.a(".Fabric");
        a.append(File.separator);
        a.append(getIdentifier());
        return a.toString();
    }

    public abstract String getVersion();

    public boolean hasAnnotatedDependency() {
        return this.dependsOnAnnotation != null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0049, code lost:
        if (r5 == false) goto L_0x0064;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void initialize() {
        /*
            r10 = this;
            l.a.a.a.InitializationTask<Result> r0 = r10.initializationTask
            l.a.a.a.Fabric r1 = r10.fabric
            java.util.concurrent.ExecutorService r1 = r1.c
            r2 = 1
            java.lang.Void[] r3 = new java.lang.Void[r2]
            r4 = 0
            r5 = 0
            r3[r4] = r5
            if (r0 == 0) goto L_0x007a
            l.a.a.a.o.c.PriorityAsyncTask$a r4 = new l.a.a.a.o.c.PriorityAsyncTask$a
            r4.<init>(r1, r0)
            l.a.a.a.o.c.AsyncTask$g r1 = r0.d
            l.a.a.a.o.c.AsyncTask$g r5 = l.a.a.a.o.c.AsyncTask.g.PENDING
            if (r1 == r5) goto L_0x0036
            l.a.a.a.o.c.AsyncTask$g r1 = r0.d
            int r1 = r1.ordinal()
            if (r1 == r2) goto L_0x002e
            r5 = 2
            if (r1 == r5) goto L_0x0026
            goto L_0x0036
        L_0x0026:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "Cannot execute task: the task has already been executed (a task can be executed only once)"
            r0.<init>(r1)
            throw r0
        L_0x002e:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "Cannot execute task: the task is already running."
            r0.<init>(r1)
            throw r0
        L_0x0036:
            l.a.a.a.o.c.AsyncTask$g r1 = l.a.a.a.o.c.AsyncTask.g.RUNNING
            r0.d = r1
            java.lang.String r1 = "onPreExecute"
            l.a.a.a.o.b.TimingMetric r1 = r0.a(r1)
            l.a.a.a.Kit<Result> r5 = r0.f2622p     // Catch:{ UnmetDependencyException -> 0x0071, Exception -> 0x004e }
            boolean r5 = r5.onPreExecute()     // Catch:{ UnmetDependencyException -> 0x0071, Exception -> 0x004e }
            r1.b()
            if (r5 != 0) goto L_0x0067
            goto L_0x0064
        L_0x004c:
            r3 = move-exception
            goto L_0x0073
        L_0x004e:
            r5 = move-exception
            l.a.a.a.DefaultLogger r6 = l.a.a.a.Fabric.a()     // Catch:{ all -> 0x004c }
            java.lang.String r7 = "Fabric"
            java.lang.String r8 = "Failure onPreExecute()"
            r9 = 6
            boolean r6 = r6.a(r7, r9)     // Catch:{ all -> 0x004c }
            if (r6 == 0) goto L_0x0061
            android.util.Log.e(r7, r8, r5)     // Catch:{ all -> 0x004c }
        L_0x0061:
            r1.b()
        L_0x0064:
            r0.a(r2)
        L_0x0067:
            l.a.a.a.o.c.AsyncTask$h<Params, Result> r1 = r0.b
            r1.b = r3
            java.util.concurrent.FutureTask<Result> r0 = r0.c
            r4.execute(r0)
            return
        L_0x0071:
            r3 = move-exception
            throw r3     // Catch:{ all -> 0x004c }
        L_0x0073:
            r1.b()
            r0.a(r2)
            throw r3
        L_0x007a:
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: l.a.a.a.Kit.initialize():void");
    }

    public void injectParameters(Context context2, f fVar, InitializationCallback<Result> initializationCallback2, s sVar) {
        this.fabric = fVar;
        this.context = new FabricContext(context2, getIdentifier(), getPath());
        this.initializationCallback = initializationCallback2;
        this.idManager = sVar;
    }

    public void onCancelled(Result result) {
    }

    public void onPostExecute(Result result) {
    }

    public boolean onPreExecute() {
        return true;
    }

    public int compareTo(Kit kit) {
        if (containsAnnotatedDependency(kit)) {
            return 1;
        }
        if (kit.containsAnnotatedDependency(this)) {
            return -1;
        }
        if (hasAnnotatedDependency() && !kit.hasAnnotatedDependency()) {
            return 1;
        }
        if (hasAnnotatedDependency() || !kit.hasAnnotatedDependency()) {
            return 0;
        }
        return -1;
    }
}
