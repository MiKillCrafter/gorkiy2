package l.a.a.a.o.c.m;

public interface Backoff {
    long getDelayMillis(int i2);
}
