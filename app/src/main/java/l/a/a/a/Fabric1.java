package l.a.a.a;

import java.util.concurrent.CountDownLatch;

/* compiled from: Fabric */
public class Fabric1 implements InitializationCallback {
    public final CountDownLatch b = new CountDownLatch(this.c);
    public final /* synthetic */ int c;
    public final /* synthetic */ Fabric d;

    public Fabric1(Fabric fabric, int i2) {
        this.d = fabric;
        this.c = i2;
    }

    public void a(Object obj) {
        this.b.countDown();
        if (this.b.getCount() == 0) {
            this.d.f2617i.set(true);
            Fabric fabric = this.d;
            fabric.d.a((f) fabric);
        }
    }

    public void a(Exception exc) {
        this.d.d.a(exc);
    }
}
