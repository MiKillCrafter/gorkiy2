package l.a.a.a.o.c;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import l.a.a.a.o.c.b;
import l.a.a.a.o.c.i;
import l.a.a.a.o.c.l;

public class DependencyPriorityBlockingQueue<E extends b & l & i> extends PriorityBlockingQueue<E> {
    public final Queue<E> b = new LinkedList();
    public final ReentrantLock c = new ReentrantLock();

    public E a(int i2, Long l2, TimeUnit timeUnit) {
        E e2;
        while (true) {
            if (i2 == 0) {
                e2 = (Dependency) super.take();
            } else if (i2 == 1) {
                e2 = (Dependency) super.peek();
            } else if (i2 != 2) {
                e2 = i2 != 3 ? null : (Dependency) super.poll(l2.longValue(), timeUnit);
            } else {
                e2 = (Dependency) super.poll();
            }
            if (e2 == null || e2.areDependenciesMet()) {
                return e2;
            }
            try {
                this.c.lock();
                if (i2 == 1) {
                    super.remove(e2);
                }
                this.b.offer(e2);
            } finally {
                this.c.unlock();
            }
        }
        return e2;
    }

    public void c() {
        try {
            this.c.lock();
            Iterator<E> it = this.b.iterator();
            while (it.hasNext()) {
                Dependency dependency = (Dependency) it.next();
                if (dependency.areDependenciesMet()) {
                    super.offer(dependency);
                    it.remove();
                }
            }
        } finally {
            this.c.unlock();
        }
    }

    public void clear() {
        try {
            this.c.lock();
            this.b.clear();
            super.clear();
        } finally {
            this.c.unlock();
        }
    }

    public boolean contains(Object obj) {
        try {
            this.c.lock();
            return super.contains(obj) || this.b.contains(obj);
        } finally {
            this.c.unlock();
        }
    }

    public int drainTo(Collection<? super E> collection) {
        try {
            this.c.lock();
            int drainTo = super.drainTo(collection) + this.b.size();
            while (!this.b.isEmpty()) {
                collection.add(this.b.poll());
            }
            return drainTo;
        } finally {
            this.c.unlock();
        }
    }

    public Object peek() {
        try {
            return a(1, null, null);
        } catch (InterruptedException unused) {
            return null;
        }
    }

    public Object poll(long j2, TimeUnit timeUnit) {
        return a(3, Long.valueOf(j2), timeUnit);
    }

    public boolean remove(Object obj) {
        try {
            this.c.lock();
            return super.remove(obj) || this.b.remove(obj);
        } finally {
            this.c.unlock();
        }
    }

    public boolean removeAll(Collection<?> collection) {
        try {
            this.c.lock();
            return this.b.removeAll(collection) | super.removeAll(collection);
        } finally {
            this.c.unlock();
        }
    }

    public int size() {
        try {
            this.c.lock();
            return this.b.size() + super.size();
        } finally {
            this.c.unlock();
        }
    }

    public Object take() {
        return a(0, null, null);
    }

    public <T> T[] toArray(T[] tArr) {
        try {
            this.c.lock();
            return a(super.toArray(tArr), this.b.toArray(tArr));
        } finally {
            this.c.unlock();
        }
    }

    public Object poll() {
        try {
            return a(2, null, null);
        } catch (InterruptedException unused) {
            return null;
        }
    }

    public Object[] toArray() {
        try {
            this.c.lock();
            return a(super.toArray(), this.b.toArray());
        } finally {
            this.c.unlock();
        }
    }

    public int drainTo(Collection<? super E> collection, int i2) {
        try {
            this.c.lock();
            int drainTo = super.drainTo(collection, i2);
            while (!this.b.isEmpty() && drainTo <= i2) {
                collection.add(this.b.poll());
                drainTo++;
            }
            return drainTo;
        } finally {
            this.c.unlock();
        }
    }

    public <T> T[] a(T[] tArr, T[] tArr2) {
        int length = tArr.length;
        int length2 = tArr2.length;
        T[] tArr3 = (Object[]) Array.newInstance(tArr.getClass().getComponentType(), length + length2);
        System.arraycopy(tArr, 0, tArr3, 0, length);
        System.arraycopy(tArr2, 0, tArr3, length, length2);
        return tArr3;
    }
}
