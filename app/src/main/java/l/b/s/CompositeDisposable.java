package l.b.s;

import io.reactivex.exceptions.CompositeException;
import j.c.a.a.c.n.c;
import java.util.ArrayList;
import l.b.u.a.DisposableContainer;
import l.b.u.b.ObjectHelper;
import l.b.u.h.ExceptionHelper;
import l.b.u.h.OpenHashSet;

public final class CompositeDisposable implements Disposable, DisposableContainer {
    public OpenHashSet<b> b;
    public volatile boolean c;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.s.Disposable[], java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.s.Disposable, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public boolean a(Disposable... disposableArr) {
        ObjectHelper.a((Object) disposableArr, "disposables is null");
        if (!this.c) {
            synchronized (this) {
                if (!this.c) {
                    OpenHashSet<b> openHashSet = this.b;
                    if (openHashSet == null) {
                        openHashSet = new OpenHashSet<>(disposableArr.length + 1, 0.75f);
                        this.b = openHashSet;
                    }
                    for (Disposable disposable : disposableArr) {
                        ObjectHelper.a((Object) disposable, "A Disposable in the disposables array is null");
                        openHashSet.a(disposable);
                    }
                    return true;
                }
            }
        }
        for (Disposable f2 : disposableArr) {
            f2.f();
        }
        return false;
    }

    public boolean b(Disposable disposable) {
        if (!a(disposable)) {
            return false;
        }
        disposable.f();
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.s.Disposable, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public boolean c(Disposable disposable) {
        ObjectHelper.a((Object) disposable, "disposable is null");
        if (!this.c) {
            synchronized (this) {
                if (!this.c) {
                    OpenHashSet<b> openHashSet = this.b;
                    if (openHashSet == null) {
                        openHashSet = new OpenHashSet<>(16, 0.75f);
                        this.b = openHashSet;
                    }
                    openHashSet.a(disposable);
                    return true;
                }
            }
        }
        disposable.f();
        return false;
    }

    public void f() {
        if (!this.c) {
            synchronized (this) {
                if (!this.c) {
                    this.c = true;
                    OpenHashSet<b> openHashSet = this.b;
                    this.b = null;
                    a(openHashSet);
                }
            }
        }
    }

    public boolean g() {
        return this.c;
    }

    public void a(OpenHashSet<b> openHashSet) {
        if (openHashSet != null) {
            ArrayList arrayList = null;
            for (T t2 : openHashSet.f2772e) {
                if (t2 instanceof Disposable) {
                    try {
                        ((Disposable) t2).f();
                    } catch (Throwable th) {
                        c.c(th);
                        if (arrayList == null) {
                            arrayList = new ArrayList();
                        }
                        arrayList.add(th);
                    }
                }
            }
            if (arrayList == null) {
                return;
            }
            if (arrayList.size() == 1) {
                throw ExceptionHelper.a((Throwable) arrayList.get(0));
            }
            throw new CompositeException(arrayList);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.s.Disposable, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x004c, code lost:
        return false;
     */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0049 A[DONT_GENERATE] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(l.b.s.Disposable r8) {
        /*
            r7 = this;
            java.lang.String r0 = "disposables is null"
            l.b.u.b.ObjectHelper.a(r8, r0)
            boolean r0 = r7.c
            r1 = 0
            if (r0 == 0) goto L_0x000b
            return r1
        L_0x000b:
            monitor-enter(r7)
            boolean r0 = r7.c     // Catch:{ all -> 0x004d }
            if (r0 == 0) goto L_0x0012
            monitor-exit(r7)     // Catch:{ all -> 0x004d }
            return r1
        L_0x0012:
            l.b.u.h.OpenHashSet<l.b.s.b> r0 = r7.b     // Catch:{ all -> 0x004d }
            if (r0 == 0) goto L_0x004b
            T[] r2 = r0.f2772e     // Catch:{ all -> 0x004d }
            int r3 = r0.b     // Catch:{ all -> 0x004d }
            int r4 = r8.hashCode()     // Catch:{ all -> 0x004d }
            int r4 = l.b.u.h.OpenHashSet.a(r4)     // Catch:{ all -> 0x004d }
            r4 = r4 & r3
            r5 = r2[r4]     // Catch:{ all -> 0x004d }
            r6 = 1
            if (r5 != 0) goto L_0x002a
        L_0x0028:
            r8 = 0
            goto L_0x0046
        L_0x002a:
            boolean r5 = r5.equals(r8)     // Catch:{ all -> 0x004d }
            if (r5 == 0) goto L_0x0035
            r0.a(r4, r2, r3)     // Catch:{ all -> 0x004d }
        L_0x0033:
            r8 = 1
            goto L_0x0046
        L_0x0035:
            int r4 = r4 + r6
            r4 = r4 & r3
            r5 = r2[r4]     // Catch:{ all -> 0x004d }
            if (r5 != 0) goto L_0x003c
            goto L_0x0028
        L_0x003c:
            boolean r5 = r5.equals(r8)     // Catch:{ all -> 0x004d }
            if (r5 == 0) goto L_0x0035
            r0.a(r4, r2, r3)     // Catch:{ all -> 0x004d }
            goto L_0x0033
        L_0x0046:
            if (r8 != 0) goto L_0x0049
            goto L_0x004b
        L_0x0049:
            monitor-exit(r7)     // Catch:{ all -> 0x004d }
            return r6
        L_0x004b:
            monitor-exit(r7)     // Catch:{ all -> 0x004d }
            return r1
        L_0x004d:
            r8 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x004d }
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: l.b.s.CompositeDisposable.a(l.b.s.Disposable):boolean");
    }

    public void a() {
        if (!this.c) {
            synchronized (this) {
                if (!this.c) {
                    OpenHashSet<b> openHashSet = this.b;
                    this.b = null;
                    a(openHashSet);
                }
            }
        }
    }
}
