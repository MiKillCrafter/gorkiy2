package l.b.x;

import e.c.b.j.b;
import j.c.a.a.c.n.c;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import l.b.Observer;
import l.b.s.Disposable;
import l.b.u.b.ObjectHelper;
import l.b.u.h.AppendOnlyLinkedArrayList;
import l.b.u.h.ExceptionHelper;
import l.b.u.h.NotificationLite;

public final class BehaviorSubject<T> extends Subject<T> {

    /* renamed from: i  reason: collision with root package name */
    public static final Object[] f2775i = new Object[0];

    /* renamed from: j  reason: collision with root package name */
    public static final a[] f2776j = new a[0];

    /* renamed from: k  reason: collision with root package name */
    public static final a[] f2777k = new a[0];
    public final AtomicReference<Object> b = new AtomicReference<>();
    public final AtomicReference<a<T>[]> c = new AtomicReference<>(f2776j);
    public final ReadWriteLock d;

    /* renamed from: e  reason: collision with root package name */
    public final Lock f2778e;

    /* renamed from: f  reason: collision with root package name */
    public final Lock f2779f = this.d.writeLock();
    public final AtomicReference<Throwable> g = new AtomicReference<>();
    public long h;

    public BehaviorSubject() {
        ReentrantReadWriteLock reentrantReadWriteLock = new ReentrantReadWriteLock();
        this.d = reentrantReadWriteLock;
        this.f2778e = reentrantReadWriteLock.readLock();
    }

    public void a(Disposable disposable) {
        if (this.g.get() != null) {
            disposable.f();
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v1, resolved type: l.b.x.BehaviorSubject$a[]} */
    /* JADX WARN: Type inference failed for: r0v0, types: [l.b.s.Disposable, l.b.x.BehaviorSubject$a] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b(l.b.Observer<? super java.util.Map<e.c.b.j.b, java.lang.String>> r6) {
        /*
            r5 = this;
            l.b.x.BehaviorSubject$a r0 = new l.b.x.BehaviorSubject$a
            r0.<init>(r6, r5)
            r6.a(r0)
        L_0x0008:
            java.util.concurrent.atomic.AtomicReference<l.b.x.BehaviorSubject$a<T>[]> r1 = r5.c
            java.lang.Object r1 = r1.get()
            l.b.x.BehaviorSubject$a[] r1 = (l.b.x.BehaviorSubject.a[]) r1
            l.b.x.BehaviorSubject$a[] r2 = l.b.x.BehaviorSubject.f2777k
            r3 = 0
            if (r1 != r2) goto L_0x0016
            goto L_0x0029
        L_0x0016:
            int r2 = r1.length
            int r4 = r2 + 1
            l.b.x.BehaviorSubject$a[] r4 = new l.b.x.BehaviorSubject.a[r4]
            java.lang.System.arraycopy(r1, r3, r4, r3, r2)
            r4[r2] = r0
            java.util.concurrent.atomic.AtomicReference<l.b.x.BehaviorSubject$a<T>[]> r2 = r5.c
            boolean r1 = r2.compareAndSet(r1, r4)
            if (r1 == 0) goto L_0x0008
            r3 = 1
        L_0x0029:
            if (r3 == 0) goto L_0x0037
            boolean r6 = r0.h
            if (r6 == 0) goto L_0x0033
            r5.a(r0)
            goto L_0x004a
        L_0x0033:
            r0.a()
            goto L_0x004a
        L_0x0037:
            java.util.concurrent.atomic.AtomicReference<java.lang.Throwable> r0 = r5.g
            java.lang.Object r0 = r0.get()
            java.lang.Throwable r0 = (java.lang.Throwable) r0
            java.lang.Throwable r1 = l.b.u.h.ExceptionHelper.a
            if (r0 != r1) goto L_0x0047
            r6.a()
            goto L_0x004a
        L_0x0047:
            r6.a(r0)
        L_0x004a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: l.b.x.BehaviorSubject.b(l.b.Observer):void");
    }

    public T c() {
        T t2 = this.b.get();
        if ((t2 == NotificationLite.COMPLETE) || (t2 instanceof NotificationLite.b)) {
            return null;
        }
        return t2;
    }

    public void e(Object obj) {
        this.f2779f.lock();
        this.h++;
        this.b.lazySet(obj);
        this.f2779f.unlock();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [java.lang.Throwable, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public void a(Throwable th) {
        ObjectHelper.a((Object) th, "onError called with null. Null values are generally not allowed in 2.x operators and sources.");
        if (!this.g.compareAndSet(null, th)) {
            c.b(th);
            return;
        }
        Object a2 = NotificationLite.a(th);
        a[] aVarArr = (a[]) this.c.getAndSet(f2777k);
        if (aVarArr != f2777k) {
            e(a2);
        }
        for (a a3 : aVarArr) {
            a3.a(a2, this.h);
        }
    }

    public void a() {
        if (this.g.compareAndSet(null, ExceptionHelper.a)) {
            NotificationLite notificationLite = NotificationLite.COMPLETE;
            a[] aVarArr = (a[]) this.c.getAndSet(f2777k);
            if (aVarArr != f2777k) {
                e(notificationLite);
            }
            for (a a2 : aVarArr) {
                a2.a(notificationLite, this.h);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [java.util.Map<e.c.b.j.b, java.lang.String>, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public void b(Map<b, String> map) {
        ObjectHelper.a((Object) map, "onNext called with null. Null values are generally not allowed in 2.x operators and sources.");
        if (this.g.get() == null) {
            NotificationLite.a(map);
            e(map);
            for (a a2 : (a[]) this.c.get()) {
                a2.a(map, this.h);
            }
        }
    }

    public static final class a<T> implements l.b.s.b, AppendOnlyLinkedArrayList.a<Object> {
        public final Observer<? super T> b;
        public final BehaviorSubject<T> c;
        public boolean d;

        /* renamed from: e  reason: collision with root package name */
        public boolean f2780e;

        /* renamed from: f  reason: collision with root package name */
        public AppendOnlyLinkedArrayList<Object> f2781f;
        public boolean g;
        public volatile boolean h;

        /* renamed from: i  reason: collision with root package name */
        public long f2782i;

        public a(Observer<? super T> observer, BehaviorSubject<T> behaviorSubject) {
            this.b = observer;
            this.c = behaviorSubject;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:19:0x0031, code lost:
            if (r0 == null) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x0037, code lost:
            if (a(r0) == false) goto L_0x003a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x0039, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x003a, code lost:
            b();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
            return;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void a() {
            /*
                r4 = this;
                boolean r0 = r4.h
                if (r0 == 0) goto L_0x0005
                return
            L_0x0005:
                monitor-enter(r4)
                boolean r0 = r4.h     // Catch:{ all -> 0x003e }
                if (r0 == 0) goto L_0x000c
                monitor-exit(r4)     // Catch:{ all -> 0x003e }
                return
            L_0x000c:
                boolean r0 = r4.d     // Catch:{ all -> 0x003e }
                if (r0 == 0) goto L_0x0012
                monitor-exit(r4)     // Catch:{ all -> 0x003e }
                return
            L_0x0012:
                l.b.x.BehaviorSubject<T> r0 = r4.c     // Catch:{ all -> 0x003e }
                java.util.concurrent.locks.Lock r1 = r0.f2778e     // Catch:{ all -> 0x003e }
                r1.lock()     // Catch:{ all -> 0x003e }
                long r2 = r0.h     // Catch:{ all -> 0x003e }
                r4.f2782i = r2     // Catch:{ all -> 0x003e }
                java.util.concurrent.atomic.AtomicReference<java.lang.Object> r0 = r0.b     // Catch:{ all -> 0x003e }
                java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x003e }
                r1.unlock()     // Catch:{ all -> 0x003e }
                r1 = 1
                if (r0 == 0) goto L_0x002b
                r2 = 1
                goto L_0x002c
            L_0x002b:
                r2 = 0
            L_0x002c:
                r4.f2780e = r2     // Catch:{ all -> 0x003e }
                r4.d = r1     // Catch:{ all -> 0x003e }
                monitor-exit(r4)     // Catch:{ all -> 0x003e }
                if (r0 == 0) goto L_0x003d
                boolean r0 = r4.a(r0)
                if (r0 == 0) goto L_0x003a
                return
            L_0x003a:
                r4.b()
            L_0x003d:
                return
            L_0x003e:
                r0 = move-exception
                monitor-exit(r4)     // Catch:{ all -> 0x003e }
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: l.b.x.BehaviorSubject.a.a():void");
        }

        /* JADX WARNING: Code restructure failed: missing block: B:12:0x0013, code lost:
            r2 = r0.b;
            r0 = r0.a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x0017, code lost:
            if (r2 == null) goto L_0x0000;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0019, code lost:
            r3 = 0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x001a, code lost:
            if (r3 >= r0) goto L_0x002b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x001c, code lost:
            r4 = r2[r3];
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x001e, code lost:
            if (r4 != null) goto L_0x0021;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x0025, code lost:
            if (a(r4) == false) goto L_0x0028;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x0028, code lost:
            r3 = r3 + 1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x002b, code lost:
            r2 = r2[r0];
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void b() {
            /*
                r5 = this;
            L_0x0000:
                boolean r0 = r5.h
                if (r0 == 0) goto L_0x0005
                return
            L_0x0005:
                monitor-enter(r5)
                l.b.u.h.AppendOnlyLinkedArrayList<java.lang.Object> r0 = r5.f2781f     // Catch:{ all -> 0x0030 }
                r1 = 0
                if (r0 != 0) goto L_0x000f
                r5.f2780e = r1     // Catch:{ all -> 0x0030 }
                monitor-exit(r5)     // Catch:{ all -> 0x0030 }
                return
            L_0x000f:
                r2 = 0
                r5.f2781f = r2     // Catch:{ all -> 0x0030 }
                monitor-exit(r5)     // Catch:{ all -> 0x0030 }
                java.lang.Object[] r2 = r0.b
                int r0 = r0.a
            L_0x0017:
                if (r2 == 0) goto L_0x0000
                r3 = 0
            L_0x001a:
                if (r3 >= r0) goto L_0x002b
                r4 = r2[r3]
                if (r4 != 0) goto L_0x0021
                goto L_0x002b
            L_0x0021:
                boolean r4 = r5.a(r4)
                if (r4 == 0) goto L_0x0028
                goto L_0x0000
            L_0x0028:
                int r3 = r3 + 1
                goto L_0x001a
            L_0x002b:
                r2 = r2[r0]
                java.lang.Object[] r2 = (java.lang.Object[]) r2
                goto L_0x0017
            L_0x0030:
                r0 = move-exception
                monitor-exit(r5)     // Catch:{ all -> 0x0030 }
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: l.b.x.BehaviorSubject.a.b():void");
        }

        public void f() {
            if (!this.h) {
                this.h = true;
                this.c.a((a<Boolean>) this);
            }
        }

        public boolean g() {
            return this.h;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:25:0x0031, code lost:
            r3.g = true;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void a(java.lang.Object r4, long r5) {
            /*
                r3 = this;
                boolean r0 = r3.h
                if (r0 == 0) goto L_0x0005
                return
            L_0x0005:
                boolean r0 = r3.g
                if (r0 != 0) goto L_0x0037
                monitor-enter(r3)
                boolean r0 = r3.h     // Catch:{ all -> 0x0034 }
                if (r0 == 0) goto L_0x0010
                monitor-exit(r3)     // Catch:{ all -> 0x0034 }
                return
            L_0x0010:
                long r0 = r3.f2782i     // Catch:{ all -> 0x0034 }
                int r2 = (r0 > r5 ? 1 : (r0 == r5 ? 0 : -1))
                if (r2 != 0) goto L_0x0018
                monitor-exit(r3)     // Catch:{ all -> 0x0034 }
                return
            L_0x0018:
                boolean r5 = r3.f2780e     // Catch:{ all -> 0x0034 }
                if (r5 == 0) goto L_0x002d
                l.b.u.h.AppendOnlyLinkedArrayList<java.lang.Object> r5 = r3.f2781f     // Catch:{ all -> 0x0034 }
                if (r5 != 0) goto L_0x0028
                l.b.u.h.AppendOnlyLinkedArrayList r5 = new l.b.u.h.AppendOnlyLinkedArrayList     // Catch:{ all -> 0x0034 }
                r6 = 4
                r5.<init>(r6)     // Catch:{ all -> 0x0034 }
                r3.f2781f = r5     // Catch:{ all -> 0x0034 }
            L_0x0028:
                r5.a(r4)     // Catch:{ all -> 0x0034 }
                monitor-exit(r3)     // Catch:{ all -> 0x0034 }
                return
            L_0x002d:
                r5 = 1
                r3.d = r5     // Catch:{ all -> 0x0034 }
                monitor-exit(r3)     // Catch:{ all -> 0x0034 }
                r3.g = r5
                goto L_0x0037
            L_0x0034:
                r4 = move-exception
                monitor-exit(r3)     // Catch:{ all -> 0x0034 }
                throw r4
            L_0x0037:
                r3.a(r4)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: l.b.x.BehaviorSubject.a.a(java.lang.Object, long):void");
        }

        /* JADX WARNING: Removed duplicated region for block: B:12:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean a(java.lang.Object r5) {
            /*
                r4 = this;
                boolean r0 = r4.h
                r1 = 0
                r2 = 1
                if (r0 != 0) goto L_0x0023
                l.b.Observer<? super T> r0 = r4.b
                l.b.u.h.NotificationLite r3 = l.b.u.h.NotificationLite.COMPLETE
                if (r5 != r3) goto L_0x0011
                r0.a()
            L_0x000f:
                r5 = 1
                goto L_0x0021
            L_0x0011:
                boolean r3 = r5 instanceof l.b.u.h.NotificationLite.b
                if (r3 == 0) goto L_0x001d
                l.b.u.h.NotificationLite$b r5 = (l.b.u.h.NotificationLite.b) r5
                java.lang.Throwable r5 = r5.b
                r0.a(r5)
                goto L_0x000f
            L_0x001d:
                r0.b(r5)
                r5 = 0
            L_0x0021:
                if (r5 == 0) goto L_0x0024
            L_0x0023:
                r1 = 1
            L_0x0024:
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: l.b.x.BehaviorSubject.a.a(java.lang.Object):boolean");
        }
    }

    public void a(a<Boolean> aVar) {
        a<Boolean>[] aVarArr;
        a[] aVarArr2;
        do {
            aVarArr = (a[]) this.c.get();
            int length = aVarArr.length;
            if (length != 0) {
                int i2 = -1;
                int i3 = 0;
                while (true) {
                    if (i3 >= length) {
                        break;
                    } else if (aVarArr[i3] == aVar) {
                        i2 = i3;
                        break;
                    } else {
                        i3++;
                    }
                }
                if (i2 >= 0) {
                    if (length == 1) {
                        aVarArr2 = f2776j;
                    } else {
                        a[] aVarArr3 = new a[(length - 1)];
                        System.arraycopy(aVarArr, 0, aVarArr3, 0, i2);
                        System.arraycopy(aVarArr, i2 + 1, aVarArr3, i2, (length - i2) - 1);
                        aVarArr2 = aVarArr3;
                    }
                } else {
                    return;
                }
            } else {
                return;
            }
        } while (!this.c.compareAndSet(aVarArr, aVarArr2));
    }
}
