package l.b.x;

import l.b.Observable;
import l.b.Observer;

public abstract class Subject<T> extends Observable<T> implements Observer<T> {
}
