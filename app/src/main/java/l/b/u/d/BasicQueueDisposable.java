package l.b.u.d;

import l.b.u.c.QueueDisposable;

public abstract class BasicQueueDisposable<T> implements QueueDisposable<T> {
    public final boolean offer(T t2) {
        throw new UnsupportedOperationException("Should not be called");
    }
}
