package l.b.u.e.d;

import java.util.concurrent.atomic.AtomicReference;
import l.b.CompletableObserver;
import l.b.CompletableSource;
import l.b.SingleObserver;
import l.b.SingleSource;
import l.b.b;
import l.b.c;
import l.b.d;
import l.b.s.Disposable;
import l.b.t.Function;
import l.b.u.a.DisposableHelper;
import l.b.u.b.ObjectHelper;

public final class SingleFlatMapCompletable<T> extends b {
    public final SingleSource<T> a;
    public final Function<? super T, ? extends d> b;

    public static final class a<T> extends AtomicReference<l.b.s.b> implements SingleObserver<T>, c, l.b.s.b {
        public final CompletableObserver b;
        public final Function<? super T, ? extends d> c;

        public a(c cVar, Function<? super T, ? extends d> function) {
            this.b = cVar;
            this.c = function;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: l.b.u.a.DisposableHelper.a(java.util.concurrent.atomic.AtomicReference<l.b.s.b>, l.b.s.b):boolean
         arg types: [l.b.u.e.d.SingleFlatMapCompletable$a, l.b.s.Disposable]
         candidates:
          l.b.u.a.DisposableHelper.a(l.b.s.Disposable, l.b.s.Disposable):boolean
          l.b.u.a.DisposableHelper.a(java.util.concurrent.atomic.AtomicReference<l.b.s.b>, l.b.s.b):boolean */
        public void a(Disposable disposable) {
            DisposableHelper.a((AtomicReference<l.b.s.b>) super, (l.b.s.b) disposable);
        }

        public void f() {
            DisposableHelper.a(super);
        }

        public boolean g() {
            return DisposableHelper.a((Disposable) get());
        }

        /* JADX WARN: Type inference failed for: r1v0, types: [l.b.CompletableObserver, l.b.u.e.d.SingleFlatMapCompletable$a] */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
         arg types: [? extends l.b.d, java.lang.String]
         candidates:
          l.b.u.b.ObjectHelper.a(int, java.lang.String):int
          l.b.u.b.ObjectHelper.a(long, long):int
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
        public void a(T t2) {
            try {
                Object a = this.c.a(t2);
                ObjectHelper.a((Object) a, "The mapper returned a null CompletableSource");
                CompletableSource completableSource = (CompletableSource) a;
                if (!g()) {
                    completableSource.a(this);
                }
            } catch (Throwable th) {
                j.c.a.a.c.n.c.c(th);
                this.b.a(th);
            }
        }

        public void a(Throwable th) {
            this.b.a(th);
        }

        public void a() {
            this.b.a();
        }
    }

    public SingleFlatMapCompletable(SingleSource<T> singleSource, Function<? super T, ? extends d> function) {
        this.a = singleSource;
        this.b = function;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [l.b.s.Disposable, l.b.SingleObserver, l.b.u.e.d.SingleFlatMapCompletable$a] */
    public void b(CompletableObserver completableObserver) {
        ? aVar = new a(completableObserver, this.b);
        completableObserver.a((Disposable) aVar);
        this.a.a(aVar);
    }
}
