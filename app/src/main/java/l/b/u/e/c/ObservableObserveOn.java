package l.b.u.e.c;

import j.c.a.a.c.n.c;
import l.b.ObservableSource;
import l.b.Observer;
import l.b.Scheduler;
import l.b.l;
import l.b.s.Disposable;
import l.b.u.a.DisposableHelper;
import l.b.u.c.QueueDisposable;
import l.b.u.c.SimpleQueue;
import l.b.u.d.BasicIntQueueDisposable;
import l.b.u.f.SpscLinkedArrayQueue;
import l.b.u.g.TrampolineScheduler;

public final class ObservableObserveOn<T> extends AbstractObservableWithUpstream<T, T> {
    public final Scheduler c;
    public final boolean d;

    /* renamed from: e  reason: collision with root package name */
    public final int f2737e;

    public ObservableObserveOn(ObservableSource<T> observableSource, l lVar, boolean z, int i2) {
        super(observableSource);
        this.c = lVar;
        this.d = z;
        this.f2737e = i2;
    }

    public void b(Observer<? super T> observer) {
        Scheduler scheduler = this.c;
        if (scheduler instanceof TrampolineScheduler) {
            super.b.a(observer);
            return;
        }
        super.b.a(new a(observer, scheduler.a(), this.d, this.f2737e));
    }

    public static final class a<T> extends BasicIntQueueDisposable<T> implements Observer<T>, Runnable {
        public final Observer<? super T> b;
        public final Scheduler.b c;
        public final boolean d;

        /* renamed from: e  reason: collision with root package name */
        public final int f2738e;

        /* renamed from: f  reason: collision with root package name */
        public SimpleQueue<T> f2739f;
        public Disposable g;
        public Throwable h;

        /* renamed from: i  reason: collision with root package name */
        public volatile boolean f2740i;

        /* renamed from: j  reason: collision with root package name */
        public volatile boolean f2741j;

        /* renamed from: k  reason: collision with root package name */
        public int f2742k;

        /* renamed from: l  reason: collision with root package name */
        public boolean f2743l;

        public a(Observer<? super T> observer, l.b bVar, boolean z, int i2) {
            this.b = observer;
            this.c = bVar;
            this.d = z;
            this.f2738e = i2;
        }

        /* JADX WARN: Type inference failed for: r3v3, types: [l.b.u.c.QueueDisposable, l.b.u.c.SimpleQueue<T>] */
        public void a(Disposable disposable) {
            if (DisposableHelper.a(this.g, disposable)) {
                this.g = disposable;
                if (disposable instanceof QueueDisposable) {
                    ? r3 = (QueueDisposable) disposable;
                    int a = r3.a(7);
                    if (a == 1) {
                        this.f2742k = a;
                        this.f2739f = r3;
                        this.f2740i = true;
                        this.b.a((Disposable) this);
                        b();
                        return;
                    } else if (a == 2) {
                        this.f2742k = a;
                        this.f2739f = r3;
                        this.b.a((Disposable) this);
                        return;
                    }
                }
                this.f2739f = new SpscLinkedArrayQueue(this.f2738e);
                this.b.a((Disposable) this);
            }
        }

        public void b(T t2) {
            if (!this.f2740i) {
                if (this.f2742k != 2) {
                    this.f2739f.offer(t2);
                }
                b();
            }
        }

        public void clear() {
            this.f2739f.clear();
        }

        public void f() {
            if (!this.f2741j) {
                this.f2741j = true;
                this.g.f();
                this.c.f();
                if (!this.f2743l && getAndIncrement() == 0) {
                    this.f2739f.clear();
                }
            }
        }

        public boolean g() {
            return this.f2741j;
        }

        public boolean isEmpty() {
            return this.f2739f.isEmpty();
        }

        public T poll() {
            return this.f2739f.poll();
        }

        /*  JADX ERROR: StackOverflow in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxOverflowException: 
            	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:52)
            	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:86)
            */
        public void run() {
            /*
                r7 = this;
                boolean r0 = r7.f2743l
                r1 = 1
                if (r0 == 0) goto L_0x004f
                r0 = 1
            L_0x0006:
                boolean r2 = r7.f2741j
                if (r2 == 0) goto L_0x000c
                goto L_0x0097
            L_0x000c:
                boolean r2 = r7.f2740i
                java.lang.Throwable r3 = r7.h
                boolean r4 = r7.d
                if (r4 != 0) goto L_0x0028
                if (r2 == 0) goto L_0x0028
                if (r3 == 0) goto L_0x0028
                r7.f2741j = r1
                l.b.Observer<? super T> r0 = r7.b
                java.lang.Throwable r1 = r7.h
                r0.a(r1)
                l.b.Scheduler$b r0 = r7.c
                r0.f()
                goto L_0x0097
            L_0x0028:
                l.b.Observer<? super T> r3 = r7.b
                r4 = 0
                r3.b(r4)
                if (r2 == 0) goto L_0x0047
                r7.f2741j = r1
                java.lang.Throwable r0 = r7.h
                if (r0 == 0) goto L_0x003c
                l.b.Observer<? super T> r1 = r7.b
                r1.a(r0)
                goto L_0x0041
            L_0x003c:
                l.b.Observer<? super T> r0 = r7.b
                r0.a()
            L_0x0041:
                l.b.Scheduler$b r0 = r7.c
                r0.f()
                goto L_0x0097
            L_0x0047:
                int r0 = -r0
                int r0 = r7.addAndGet(r0)
                if (r0 != 0) goto L_0x0006
                goto L_0x0097
            L_0x004f:
                l.b.u.c.SimpleQueue<T> r0 = r7.f2739f
                l.b.Observer<? super T> r2 = r7.b
                r3 = 1
            L_0x0054:
                boolean r4 = r7.f2740i
                boolean r5 = r0.isEmpty()
                boolean r4 = r7.a(r4, r5, r2)
                if (r4 == 0) goto L_0x0061
                goto L_0x0097
            L_0x0061:
                boolean r4 = r7.f2740i
                java.lang.Object r5 = r0.poll()     // Catch:{ all -> 0x0081 }
                if (r5 != 0) goto L_0x006b
                r6 = 1
                goto L_0x006c
            L_0x006b:
                r6 = 0
            L_0x006c:
                boolean r4 = r7.a(r4, r6, r2)
                if (r4 == 0) goto L_0x0073
                goto L_0x0097
            L_0x0073:
                if (r6 == 0) goto L_0x007d
                int r3 = -r3
                int r3 = r7.addAndGet(r3)
                if (r3 != 0) goto L_0x0054
                goto L_0x0097
            L_0x007d:
                r2.b(r5)
                goto L_0x0061
            L_0x0081:
                r3 = move-exception
                j.c.a.a.c.n.c.c(r3)
                r7.f2741j = r1
                l.b.s.Disposable r1 = r7.g
                r1.f()
                r0.clear()
                r2.a(r3)
                l.b.Scheduler$b r0 = r7.c
                r0.f()
            L_0x0097:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: l.b.u.e.c.ObservableObserveOn.a.run():void");
        }

        public void b() {
            if (getAndIncrement() == 0) {
                this.c.a(this);
            }
        }

        public void a(Throwable th) {
            if (this.f2740i) {
                c.b(th);
                return;
            }
            this.h = th;
            this.f2740i = true;
            b();
        }

        public void a() {
            if (!this.f2740i) {
                this.f2740i = true;
                b();
            }
        }

        public boolean a(boolean z, boolean z2, Observer<? super T> observer) {
            if (this.f2741j) {
                this.f2739f.clear();
                return true;
            } else if (!z) {
                return false;
            } else {
                Throwable th = this.h;
                if (this.d) {
                    if (!z2) {
                        return false;
                    }
                    this.f2741j = true;
                    if (th != null) {
                        observer.a(th);
                    } else {
                        observer.a();
                    }
                    this.c.f();
                    return true;
                } else if (th != null) {
                    this.f2741j = true;
                    this.f2739f.clear();
                    observer.a(th);
                    this.c.f();
                    return true;
                } else if (!z2) {
                    return false;
                } else {
                    this.f2741j = true;
                    observer.a();
                    this.c.f();
                    return true;
                }
            }
        }

        public int a(int i2) {
            if ((i2 & 2) == 0) {
                return 0;
            }
            this.f2743l = true;
            return 2;
        }
    }
}
