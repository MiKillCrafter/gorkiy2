package l.b.u.e.c;

import j.c.a.a.c.n.c;
import java.util.NoSuchElementException;
import l.b.ObservableSource;
import l.b.Observer;
import l.b.Single;
import l.b.SingleObserver;
import l.b.s.Disposable;
import l.b.s.b;
import l.b.u.a.DisposableHelper;

public final class ObservableSingleSingle<T> extends Single<T> {
    public final ObservableSource<? extends T> a;
    public final T b;

    public ObservableSingleSingle(ObservableSource<? extends T> observableSource, T t2) {
        this.a = observableSource;
        this.b = t2;
    }

    public void b(SingleObserver<? super T> singleObserver) {
        this.a.a(new a(singleObserver, this.b));
    }

    public static final class a<T> implements Observer<T>, b {
        public final SingleObserver<? super T> b;
        public final T c;
        public Disposable d;

        /* renamed from: e  reason: collision with root package name */
        public T f2745e;

        /* renamed from: f  reason: collision with root package name */
        public boolean f2746f;

        public a(SingleObserver<? super T> singleObserver, T t2) {
            this.b = singleObserver;
            this.c = t2;
        }

        /* JADX WARN: Type inference failed for: r1v0, types: [l.b.s.Disposable, l.b.u.e.c.ObservableSingleSingle$a] */
        public void a(Disposable disposable) {
            if (DisposableHelper.a(this.d, disposable)) {
                this.d = disposable;
                this.b.a((Disposable) this);
            }
        }

        public void b(T t2) {
            if (!this.f2746f) {
                if (this.f2745e != null) {
                    this.f2746f = true;
                    this.d.f();
                    this.b.a((Throwable) new IllegalArgumentException("Sequence contains more than one element!"));
                    return;
                }
                this.f2745e = t2;
            }
        }

        public void f() {
            this.d.f();
        }

        public boolean g() {
            return this.d.g();
        }

        public void a(Throwable th) {
            if (this.f2746f) {
                c.b(th);
                return;
            }
            this.f2746f = true;
            this.b.a(th);
        }

        public void a() {
            if (!this.f2746f) {
                this.f2746f = true;
                T t2 = this.f2745e;
                this.f2745e = null;
                if (t2 == null) {
                    t2 = this.c;
                }
                if (t2 != null) {
                    this.b.a((Object) t2);
                } else {
                    this.b.a((Throwable) new NoSuchElementException());
                }
            }
        }
    }
}
