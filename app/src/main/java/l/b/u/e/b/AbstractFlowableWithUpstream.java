package l.b.u.e.b;

import l.b.Flowable;
import l.b.u.b.ObjectHelper;

public abstract class AbstractFlowableWithUpstream<T, R> extends Flowable<R> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.Flowable<T>, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public AbstractFlowableWithUpstream(Flowable<T> flowable) {
        ObjectHelper.a((Object) flowable, "source is null");
    }
}
