package l.b.u.e.c;

import j.c.a.a.c.n.c;
import java.util.Iterator;
import l.b.Observable;
import l.b.Observer;
import l.b.s.Disposable;
import l.b.u.a.EmptyDisposable;
import l.b.u.b.ObjectHelper;
import l.b.u.d.BasicQueueDisposable;

public final class ObservableFromIterable<T> extends Observable<T> {
    public final Iterable<? extends T> b;

    public static final class a<T> extends BasicQueueDisposable<T> {
        public final Observer<? super T> b;
        public final Iterator<? extends T> c;
        public volatile boolean d;

        /* renamed from: e  reason: collision with root package name */
        public boolean f2735e;

        /* renamed from: f  reason: collision with root package name */
        public boolean f2736f;
        public boolean g;

        public a(Observer<? super T> observer, Iterator<? extends T> it) {
            this.b = observer;
            this.c = it;
        }

        public int a(int i2) {
            if ((i2 & 1) == 0) {
                return 0;
            }
            this.f2735e = true;
            return 1;
        }

        public void clear() {
            this.f2736f = true;
        }

        public void f() {
            this.d = true;
        }

        public boolean g() {
            return this.d;
        }

        public boolean isEmpty() {
            return this.f2736f;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
         arg types: [T, java.lang.String]
         candidates:
          l.b.u.b.ObjectHelper.a(int, java.lang.String):int
          l.b.u.b.ObjectHelper.a(long, long):int
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
        public T poll() {
            if (this.f2736f) {
                return null;
            }
            if (!this.g) {
                this.g = true;
            } else if (!this.c.hasNext()) {
                this.f2736f = true;
                return null;
            }
            T next = this.c.next();
            ObjectHelper.a((Object) next, "The iterator returned a null value");
            return next;
        }
    }

    public ObservableFromIterable(Iterable<? extends T> iterable) {
        this.b = iterable;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [l.b.u.a.EmptyDisposable, l.b.s.Disposable] */
    /* JADX WARN: Type inference failed for: r1v1, types: [l.b.u.a.EmptyDisposable, l.b.s.Disposable] */
    /* JADX WARN: Type inference failed for: r1v3, types: [l.b.u.e.c.ObservableFromIterable$a, l.b.s.Disposable] */
    /* JADX WARN: Type inference failed for: r0v8, types: [l.b.u.a.EmptyDisposable, l.b.s.Disposable] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [? extends T, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public void b(Observer<? super T> observer) {
        try {
            Iterator<? extends T> it = this.b.iterator();
            try {
                if (!it.hasNext()) {
                    observer.a((Disposable) EmptyDisposable.INSTANCE);
                    observer.a();
                    return;
                }
                ? aVar = new a(observer, it);
                observer.a((Disposable) aVar);
                if (!aVar.f2735e) {
                    while (!aVar.d) {
                        try {
                            Object next = aVar.c.next();
                            ObjectHelper.a((Object) next, "The iterator returned a null value");
                            aVar.b.b(next);
                            if (!aVar.d) {
                                try {
                                    if (!aVar.c.hasNext()) {
                                        if (!aVar.d) {
                                            aVar.b.a();
                                            return;
                                        }
                                        return;
                                    }
                                } catch (Throwable th) {
                                    c.c(th);
                                    aVar.b.a(th);
                                    return;
                                }
                            } else {
                                return;
                            }
                        } catch (Throwable th2) {
                            c.c(th2);
                            aVar.b.a(th2);
                            return;
                        }
                    }
                }
            } catch (Throwable th3) {
                c.c(th3);
                observer.a((Disposable) EmptyDisposable.INSTANCE);
                observer.a(th3);
            }
        } catch (Throwable th4) {
            c.c(th4);
            observer.a((Disposable) EmptyDisposable.INSTANCE);
            observer.a(th4);
        }
    }
}
