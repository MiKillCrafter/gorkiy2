package l.b.u.e.c;

import l.b.Observable;
import l.b.Observer;
import l.b.s.b;
import l.b.t.Action;
import l.b.t.Consumer;
import l.b.t.a;
import l.b.u.d.DisposableLambdaObserver;

public final class ObservableDoOnLifecycle<T> extends AbstractObservableWithUpstream<T, T> {
    public final Consumer<? super b> c;
    public final Action d;

    public ObservableDoOnLifecycle(Observable<T> observable, Consumer<? super b> consumer, a aVar) {
        super(observable);
        this.c = consumer;
        this.d = aVar;
    }

    public void b(Observer<? super T> observer) {
        super.b.a(new DisposableLambdaObserver(observer, this.c, this.d));
    }
}
