package l.b.u.e.c;

import java.util.concurrent.atomic.AtomicReference;
import l.b.ObservableSource;
import l.b.Observer;
import l.b.c;
import l.b.d;
import l.b.s.CompositeDisposable;
import l.b.s.Disposable;
import l.b.s.b;
import l.b.t.Function;
import l.b.u.a.DisposableHelper;
import l.b.u.d.BasicIntQueueDisposable;
import l.b.u.h.AtomicThrowable;
import l.b.u.h.ExceptionHelper;

public final class ObservableFlatMapCompletable<T> extends AbstractObservableWithUpstream<T, T> {
    public final Function<? super T, ? extends d> c;
    public final boolean d;

    public ObservableFlatMapCompletable(ObservableSource<T> observableSource, Function<? super T, ? extends d> function, boolean z) {
        super(observableSource);
        this.c = function;
        this.d = z;
    }

    public void b(Observer<? super T> observer) {
        super.b.a(new a(observer, this.c, this.d));
    }

    public static final class a<T> extends BasicIntQueueDisposable<T> implements Observer<T> {
        public final Observer<? super T> b;
        public final AtomicThrowable c = new AtomicThrowable();
        public final Function<? super T, ? extends d> d;

        /* renamed from: e  reason: collision with root package name */
        public final boolean f2725e;

        /* renamed from: f  reason: collision with root package name */
        public final CompositeDisposable f2726f = new CompositeDisposable();
        public Disposable g;
        public volatile boolean h;

        /* renamed from: l.b.u.e.c.ObservableFlatMapCompletable$a$a  reason: collision with other inner class name */
        public final class C0035a extends AtomicReference<b> implements c, b {
            public C0035a() {
            }

            public void a(Disposable disposable) {
                DisposableHelper.b(super, disposable);
            }

            public void f() {
                DisposableHelper.a(super);
            }

            public boolean g() {
                return DisposableHelper.a((Disposable) get());
            }

            /* JADX WARN: Type inference failed for: r2v0, types: [l.b.s.Disposable, l.b.u.e.c.ObservableFlatMapCompletable$a$a] */
            public void a() {
                a aVar = a.this;
                aVar.f2726f.a((Disposable) this);
                aVar.a();
            }

            /* JADX WARN: Type inference failed for: r2v0, types: [l.b.s.Disposable, l.b.u.e.c.ObservableFlatMapCompletable$a$a] */
            /*  JADX ERROR: StackOverflow in pass: MethodInvokeVisitor
                jadx.core.utils.exceptions.JadxOverflowException: 
                	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:52)
                	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:86)
                */
            public void a(java.lang.Throwable r3) {
                /*
                    r2 = this;
                    l.b.u.e.c.ObservableFlatMapCompletable$a r0 = l.b.u.e.c.ObservableFlatMapCompletable.a.this
                    l.b.s.CompositeDisposable r1 = r0.f2726f
                    r1.a(r2)
                    r0.a(r3)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: l.b.u.e.c.ObservableFlatMapCompletable.a.C0035a.a(java.lang.Throwable):void");
            }
        }

        /*  JADX ERROR: StackOverflow in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxOverflowException: 
            	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:52)
            	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:86)
            */
        public a(l.b.Observer<? super T> r1, l.b.t.Function<? super T, ? extends l.b.d> r2, boolean r3) {
            /*
                r0 = this;
                r0.<init>()
                r0.b = r1
                r0.d = r2
                r0.f2725e = r3
                l.b.u.h.AtomicThrowable r1 = new l.b.u.h.AtomicThrowable
                r1.<init>()
                r0.c = r1
                l.b.s.CompositeDisposable r1 = new l.b.s.CompositeDisposable
                r1.<init>()
                r0.f2726f = r1
                r1 = 1
                r0.lazySet(r1)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: l.b.u.e.c.ObservableFlatMapCompletable.a.<init>(l.b.Observer, l.b.t.Function, boolean):void");
        }

        public int a(int i2) {
            return i2 & 2;
        }

        /* JADX WARN: Type inference failed for: r1v0, types: [l.b.u.e.c.ObservableFlatMapCompletable$a, l.b.s.Disposable] */
        public void a(Disposable disposable) {
            if (DisposableHelper.a(this.g, disposable)) {
                this.g = disposable;
                this.b.a((Disposable) this);
            }
        }

        /* JADX WARN: Type inference failed for: r0v3, types: [l.b.CompletableObserver, l.b.s.Disposable, l.b.u.e.c.ObservableFlatMapCompletable$a$a] */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
         arg types: [? extends l.b.d, java.lang.String]
         candidates:
          l.b.u.b.ObjectHelper.a(int, java.lang.String):int
          l.b.u.b.ObjectHelper.a(long, long):int
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
        /*  JADX ERROR: StackOverflow in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxOverflowException: 
            	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:52)
            	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:86)
            */
        public void b(T r3) {
            /*
                r2 = this;
                l.b.t.Function<? super T, ? extends l.b.d> r0 = r2.d     // Catch:{ all -> 0x0025 }
                java.lang.Object r3 = r0.a(r3)     // Catch:{ all -> 0x0025 }
                java.lang.String r0 = "The mapper returned a null CompletableSource"
                l.b.u.b.ObjectHelper.a(r3, r0)     // Catch:{ all -> 0x0025 }
                l.b.CompletableSource r3 = (l.b.CompletableSource) r3     // Catch:{ all -> 0x0025 }
                r2.getAndIncrement()
                l.b.u.e.c.ObservableFlatMapCompletable$a$a r0 = new l.b.u.e.c.ObservableFlatMapCompletable$a$a
                r0.<init>()
                boolean r1 = r2.h
                if (r1 != 0) goto L_0x0024
                l.b.s.CompositeDisposable r1 = r2.f2726f
                boolean r1 = r1.c(r0)
                if (r1 == 0) goto L_0x0024
                r3.a(r0)
            L_0x0024:
                return
            L_0x0025:
                r3 = move-exception
                j.c.a.a.c.n.c.c(r3)
                l.b.s.Disposable r0 = r2.g
                r0.f()
                r2.a(r3)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: l.b.u.e.c.ObservableFlatMapCompletable.a.b(java.lang.Object):void");
        }

        public void clear() {
        }

        public void f() {
            this.h = true;
            this.g.f();
            this.f2726f.f();
        }

        public boolean g() {
            return this.g.g();
        }

        public boolean isEmpty() {
            return true;
        }

        public T poll() {
            return null;
        }

        /*  JADX ERROR: StackOverflow in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxOverflowException: 
            	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:52)
            	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:86)
            */
        public void a(java.lang.Throwable r3) {
            /*
                r2 = this;
                l.b.u.h.AtomicThrowable r0 = r2.c
                r1 = 0
                if (r0 == 0) goto L_0x0041
                boolean r0 = l.b.u.h.ExceptionHelper.a(r0, r3)
                if (r0 == 0) goto L_0x003d
                boolean r3 = r2.f2725e
                if (r3 == 0) goto L_0x0024
                int r3 = r2.decrementAndGet()
                if (r3 != 0) goto L_0x0040
                l.b.u.h.AtomicThrowable r3 = r2.c
                if (r3 == 0) goto L_0x0023
                java.lang.Throwable r3 = l.b.u.h.ExceptionHelper.a(r3)
                l.b.Observer<? super T> r0 = r2.b
                r0.a(r3)
                goto L_0x0040
            L_0x0023:
                throw r1
            L_0x0024:
                r2.f()
                r3 = 0
                int r3 = r2.getAndSet(r3)
                if (r3 <= 0) goto L_0x0040
                l.b.u.h.AtomicThrowable r3 = r2.c
                if (r3 == 0) goto L_0x003c
                java.lang.Throwable r3 = l.b.u.h.ExceptionHelper.a(r3)
                l.b.Observer<? super T> r0 = r2.b
                r0.a(r3)
                goto L_0x0040
            L_0x003c:
                throw r1
            L_0x003d:
                j.c.a.a.c.n.c.b(r3)
            L_0x0040:
                return
            L_0x0041:
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: l.b.u.e.c.ObservableFlatMapCompletable.a.a(java.lang.Throwable):void");
        }

        public void a() {
            if (decrementAndGet() == 0) {
                AtomicThrowable atomicThrowable = this.c;
                if (atomicThrowable != null) {
                    Throwable a = ExceptionHelper.a(atomicThrowable);
                    if (a != null) {
                        this.b.a(a);
                    } else {
                        this.b.a();
                    }
                } else {
                    throw null;
                }
            }
        }
    }
}
