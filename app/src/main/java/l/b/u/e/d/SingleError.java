package l.b.u.e.d;

import j.c.a.a.c.n.c;
import java.util.concurrent.Callable;
import l.b.Single;
import l.b.SingleObserver;
import l.b.s.Disposable;
import l.b.u.a.EmptyDisposable;
import l.b.u.b.ObjectHelper;

public final class SingleError<T> extends Single<T> {
    public final Callable<? extends Throwable> a;

    public SingleError(Callable<? extends Throwable> callable) {
        this.a = callable;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [l.b.u.a.EmptyDisposable, l.b.s.Disposable] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [? extends java.lang.Throwable, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public void b(SingleObserver<? super T> singleObserver) {
        try {
            Object call = this.a.call();
            ObjectHelper.a((Object) call, "Callable returned null throwable. Null values are generally not allowed in 2.x operators and sources.");
            th = (Throwable) call;
        } catch (Throwable th) {
            th = th;
            c.c(th);
        }
        singleObserver.a((Disposable) EmptyDisposable.INSTANCE);
        singleObserver.a(th);
    }
}
