package l.b.u.e.c;

import java.util.concurrent.atomic.AtomicReference;
import l.b.ObservableSource;
import l.b.Observer;
import l.b.Scheduler;
import l.b.l;
import l.b.s.Disposable;
import l.b.u.a.DisposableHelper;

public final class ObservableSubscribeOn<T> extends AbstractObservableWithUpstream<T, T> {
    public final Scheduler c;

    public static final class a<T> extends AtomicReference<l.b.s.b> implements Observer<T>, l.b.s.b {
        public final Observer<? super T> b;
        public final AtomicReference<l.b.s.b> c = new AtomicReference<>();

        public a(Observer<? super T> observer) {
            this.b = observer;
        }

        public void a(Disposable disposable) {
            DisposableHelper.b(this.c, disposable);
        }

        public void b(T t2) {
            this.b.b(t2);
        }

        public void f() {
            DisposableHelper.a(this.c);
            DisposableHelper.a(super);
        }

        public boolean g() {
            return DisposableHelper.a((Disposable) get());
        }

        public void a(Throwable th) {
            this.b.a(th);
        }

        public void a() {
            this.b.a();
        }
    }

    public final class b implements Runnable {
        public final a<T> b;

        public b(a<T> aVar) {
            this.b = aVar;
        }

        public void run() {
            ObservableSubscribeOn.this.b.a(this.b);
        }
    }

    public ObservableSubscribeOn(ObservableSource<T> observableSource, l lVar) {
        super(observableSource);
        this.c = lVar;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [l.b.s.Disposable, java.util.concurrent.atomic.AtomicReference, l.b.u.e.c.ObservableSubscribeOn$a] */
    public void b(Observer<? super T> observer) {
        ? aVar = new a(observer);
        observer.a((Disposable) aVar);
        DisposableHelper.b(aVar, this.c.a(new b(aVar)));
    }
}
