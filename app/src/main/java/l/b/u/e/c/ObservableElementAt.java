package l.b.u.e.c;

import j.c.a.a.c.n.c;
import java.util.NoSuchElementException;
import l.b.ObservableSource;
import l.b.Observer;
import l.b.s.Disposable;
import l.b.s.b;
import l.b.u.a.DisposableHelper;

public final class ObservableElementAt<T> extends AbstractObservableWithUpstream<T, T> {
    public final long c;
    public final T d;

    /* renamed from: e  reason: collision with root package name */
    public final boolean f2703e;

    public ObservableElementAt(ObservableSource<T> observableSource, long j2, T t2, boolean z) {
        super(observableSource);
        this.c = j2;
        this.d = t2;
        this.f2703e = z;
    }

    public void b(Observer<? super T> observer) {
        super.b.a(new a(observer, this.c, this.d, this.f2703e));
    }

    public static final class a<T> implements Observer<T>, b {
        public final Observer<? super T> b;
        public final long c;
        public final T d;

        /* renamed from: e  reason: collision with root package name */
        public final boolean f2704e;

        /* renamed from: f  reason: collision with root package name */
        public Disposable f2705f;
        public long g;
        public boolean h;

        public a(Observer<? super T> observer, long j2, T t2, boolean z) {
            this.b = observer;
            this.c = j2;
            this.d = t2;
            this.f2704e = z;
        }

        /* JADX WARN: Type inference failed for: r1v0, types: [l.b.u.e.c.ObservableElementAt$a, l.b.s.Disposable] */
        public void a(Disposable disposable) {
            if (DisposableHelper.a(this.f2705f, disposable)) {
                this.f2705f = disposable;
                this.b.a((Disposable) this);
            }
        }

        public void b(T t2) {
            if (!this.h) {
                long j2 = this.g;
                if (j2 == this.c) {
                    this.h = true;
                    this.f2705f.f();
                    this.b.b(t2);
                    this.b.a();
                    return;
                }
                this.g = j2 + 1;
            }
        }

        public void f() {
            this.f2705f.f();
        }

        public boolean g() {
            return this.f2705f.g();
        }

        public void a(Throwable th) {
            if (this.h) {
                c.b(th);
                return;
            }
            this.h = true;
            this.b.a(th);
        }

        public void a() {
            if (!this.h) {
                this.h = true;
                T t2 = this.d;
                if (t2 != null || !this.f2704e) {
                    if (t2 != null) {
                        this.b.b(t2);
                    }
                    this.b.a();
                    return;
                }
                this.b.a(new NoSuchElementException());
            }
        }
    }
}
