package l.b.u.a;

import io.reactivex.exceptions.ProtocolViolationException;
import j.c.a.a.c.n.c;
import java.util.concurrent.atomic.AtomicReference;
import l.b.s.Disposable;
import l.b.s.b;
import l.b.u.b.ObjectHelper;

public enum DisposableHelper implements b {
    DISPOSED;

    public static boolean a(Disposable disposable) {
        return disposable == DISPOSED;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.s.b, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public static boolean b(AtomicReference<b> atomicReference, b bVar) {
        ObjectHelper.a((Object) bVar, "d is null");
        if (atomicReference.compareAndSet(null, bVar)) {
            return true;
        }
        bVar.f();
        if (atomicReference.get() == DISPOSED) {
            return false;
        }
        c.b((Throwable) new ProtocolViolationException("Disposable already set!"));
        return false;
    }

    public void f() {
    }

    public boolean g() {
        return true;
    }

    public static boolean a(AtomicReference<b> atomicReference, b bVar) {
        Disposable disposable;
        do {
            disposable = atomicReference.get();
            if (disposable == DISPOSED) {
                if (bVar == null) {
                    return false;
                }
                bVar.f();
                return false;
            }
        } while (!atomicReference.compareAndSet(disposable, bVar));
        return true;
    }

    public static boolean a(AtomicReference<b> atomicReference) {
        Disposable andSet;
        Disposable disposable = atomicReference.get();
        DisposableHelper disposableHelper = DISPOSED;
        if (disposable == disposableHelper || (andSet = atomicReference.getAndSet(disposableHelper)) == disposableHelper) {
            return false;
        }
        if (andSet == null) {
            return true;
        }
        andSet.f();
        return true;
    }

    public static boolean a(Disposable disposable, Disposable disposable2) {
        if (disposable2 == null) {
            c.b((Throwable) new NullPointerException("next is null"));
            return false;
        } else if (disposable == null) {
            return true;
        } else {
            disposable2.f();
            c.b((Throwable) new ProtocolViolationException("Disposable already set!"));
            return false;
        }
    }
}
