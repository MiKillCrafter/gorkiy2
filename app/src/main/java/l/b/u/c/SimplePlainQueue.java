package l.b.u.c;

public interface SimplePlainQueue<T> extends SimpleQueue<T> {
    T poll();
}
