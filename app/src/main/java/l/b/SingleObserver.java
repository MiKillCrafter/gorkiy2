package l.b;

import l.b.s.Disposable;

public interface SingleObserver<T> {
    void a(Object obj);

    void a(Throwable th);

    void a(Disposable disposable);
}
