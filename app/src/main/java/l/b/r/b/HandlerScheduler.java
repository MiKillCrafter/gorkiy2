package l.b.r.b;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Message;
import j.c.a.a.c.n.c;
import java.util.concurrent.TimeUnit;
import l.b.Scheduler;
import l.b.s.Disposable;
import l.b.u.a.EmptyDisposable;
import l.b.u.b.ObjectHelper;

public final class HandlerScheduler extends Scheduler {
    public final Handler a;
    public final boolean b;

    public static final class a extends Scheduler.b {
        public final Handler b;
        public final boolean c;
        public volatile boolean d;

        public a(Handler handler, boolean z) {
            this.b = handler;
            this.c = z;
        }

        /* JADX WARN: Type inference failed for: r3v7, types: [l.b.u.a.EmptyDisposable, l.b.s.Disposable] */
        /* JADX WARN: Type inference failed for: r3v8, types: [l.b.u.a.EmptyDisposable, l.b.s.Disposable] */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
         arg types: [java.lang.Runnable, java.lang.String]
         candidates:
          l.b.u.b.ObjectHelper.a(int, java.lang.String):int
          l.b.u.b.ObjectHelper.a(long, long):int
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
        @SuppressLint({"NewApi"})
        public Disposable a(Runnable runnable, long j2, TimeUnit timeUnit) {
            if (runnable == null) {
                throw new NullPointerException("run == null");
            } else if (timeUnit == null) {
                throw new NullPointerException("unit == null");
            } else if (this.d) {
                return EmptyDisposable.INSTANCE;
            } else {
                ObjectHelper.a((Object) runnable, "run is null");
                b bVar = new b(this.b, runnable);
                Message obtain = Message.obtain(this.b, bVar);
                obtain.obj = this;
                if (this.c) {
                    obtain.setAsynchronous(true);
                }
                this.b.sendMessageDelayed(obtain, timeUnit.toMillis(j2));
                if (!this.d) {
                    return bVar;
                }
                this.b.removeCallbacks(bVar);
                return EmptyDisposable.INSTANCE;
            }
        }

        public void f() {
            this.d = true;
            this.b.removeCallbacksAndMessages(this);
        }

        public boolean g() {
            return this.d;
        }
    }

    public static final class b implements Runnable, Disposable {
        public final Handler b;
        public final Runnable c;
        public volatile boolean d;

        public b(Handler handler, Runnable runnable) {
            this.b = handler;
            this.c = runnable;
        }

        public void f() {
            this.b.removeCallbacks(this);
            this.d = true;
        }

        public boolean g() {
            return this.d;
        }

        public void run() {
            try {
                this.c.run();
            } catch (Throwable th) {
                c.b(th);
            }
        }
    }

    public HandlerScheduler(Handler handler, boolean z) {
        this.a = handler;
        this.b = z;
    }

    public Scheduler.b a() {
        return new a(this.a, this.b);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [java.lang.Runnable, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    @SuppressLint({"NewApi"})
    public Disposable a(Runnable runnable, long j2, TimeUnit timeUnit) {
        if (runnable == null) {
            throw new NullPointerException("run == null");
        } else if (timeUnit != null) {
            ObjectHelper.a((Object) runnable, "run is null");
            b bVar = new b(this.a, runnable);
            Message obtain = Message.obtain(this.a, bVar);
            if (this.b) {
                obtain.setAsynchronous(true);
            }
            this.a.sendMessageDelayed(obtain, timeUnit.toMillis(j2));
            return bVar;
        } else {
            throw new NullPointerException("unit == null");
        }
    }
}
