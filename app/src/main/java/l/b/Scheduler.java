package l.b;

import java.util.concurrent.TimeUnit;
import l.b.s.Disposable;
import l.b.u.b.ObjectHelper;
import l.b.u.g.NewThreadWorker;

public abstract class Scheduler {

    public static final class a implements Disposable, Runnable {
        public final Runnable b;
        public final b c;
        public Thread d;

        public a(Runnable runnable, b bVar) {
            this.b = runnable;
            this.c = bVar;
        }

        public void f() {
            if (this.d == Thread.currentThread()) {
                b bVar = this.c;
                if (bVar instanceof NewThreadWorker) {
                    NewThreadWorker newThreadWorker = (NewThreadWorker) bVar;
                    if (!newThreadWorker.c) {
                        newThreadWorker.c = true;
                        newThreadWorker.b.shutdown();
                        return;
                    }
                    return;
                }
            }
            this.c.f();
        }

        public boolean g() {
            return this.c.g();
        }

        public void run() {
            this.d = Thread.currentThread();
            try {
                this.b.run();
            } finally {
                f();
                this.d = null;
            }
        }
    }

    public static abstract class b implements Disposable {
        public Disposable a(Runnable runnable) {
            return a(runnable, 0, TimeUnit.NANOSECONDS);
        }

        public abstract Disposable a(Runnable runnable, long j2, TimeUnit timeUnit);

        public long a(TimeUnit timeUnit) {
            return timeUnit.convert(System.currentTimeMillis(), TimeUnit.MILLISECONDS);
        }
    }

    static {
        TimeUnit.MINUTES.toNanos(Long.getLong("rx2.scheduler.drift-tolerance", 15).longValue());
    }

    public abstract b a();

    public Disposable a(Runnable runnable) {
        return a(runnable, 0, TimeUnit.NANOSECONDS);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [java.lang.Runnable, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public Disposable a(Runnable runnable, long j2, TimeUnit timeUnit) {
        b a2 = a();
        ObjectHelper.a((Object) runnable, "run is null");
        a aVar = new a(runnable, a2);
        a2.a(aVar, j2, timeUnit);
        return aVar;
    }
}
