package androidx.lifecycle;

import i.o.FullLifecycleObserver;
import i.o.Lifecycle;
import i.o.LifecycleEventObserver;
import i.o.LifecycleOwner;

public class FullLifecycleObserverAdapter implements LifecycleEventObserver {
    public final FullLifecycleObserver a;
    public final LifecycleEventObserver b;

    public FullLifecycleObserverAdapter(FullLifecycleObserver fullLifecycleObserver, LifecycleEventObserver lifecycleEventObserver) {
        this.a = fullLifecycleObserver;
        this.b = lifecycleEventObserver;
    }

    public void a(LifecycleOwner lifecycleOwner, Lifecycle.a aVar) {
        switch (aVar.ordinal()) {
            case 0:
                this.a.c(lifecycleOwner);
                break;
            case 1:
                this.a.f(lifecycleOwner);
                break;
            case 2:
                this.a.a(lifecycleOwner);
                break;
            case 3:
                this.a.d(lifecycleOwner);
                break;
            case 4:
                this.a.e(lifecycleOwner);
                break;
            case 5:
                this.a.b(lifecycleOwner);
                break;
            case 6:
                throw new IllegalArgumentException("ON_ANY must not been send by anybody");
        }
        LifecycleEventObserver lifecycleEventObserver = this.b;
        if (lifecycleEventObserver != null) {
            lifecycleEventObserver.a(lifecycleOwner, aVar);
        }
    }
}
