package androidx.activity;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import i.a.OnBackPressedDispatcherOwner;
import i.o.Lifecycle;
import i.o.LifecycleEventObserver;
import i.o.LifecycleOwner;
import i.o.LifecycleRegistry;
import i.o.ReportFragment;
import i.o.ViewModelStore;
import i.o.ViewModelStoreOwner;
import i.t.SavedStateRegistry;
import i.t.SavedStateRegistryController;
import i.t.SavedStateRegistryOwner;

public class ComponentActivity extends i.h.d.ComponentActivity implements LifecycleOwner, ViewModelStoreOwner, SavedStateRegistryOwner, OnBackPressedDispatcherOwner {
    public final LifecycleRegistry c = new LifecycleRegistry(this);
    public final SavedStateRegistryController d = new SavedStateRegistryController(this);

    /* renamed from: e  reason: collision with root package name */
    public ViewModelStore f1e;

    /* renamed from: f  reason: collision with root package name */
    public final OnBackPressedDispatcher f2f = new OnBackPressedDispatcher(new a());

    public class a implements Runnable {
        public a() {
        }

        public void run() {
            ComponentActivity.super.onBackPressed();
        }
    }

    public static final class b {
        public ViewModelStore a;
    }

    public ComponentActivity() {
        LifecycleRegistry lifecycleRegistry = this.c;
        if (lifecycleRegistry != null) {
            lifecycleRegistry.a(new LifecycleEventObserver() {
                /* class androidx.activity.ComponentActivity.AnonymousClass2 */

                public void a(LifecycleOwner lifecycleOwner, Lifecycle.a aVar) {
                    if (aVar == Lifecycle.a.ON_STOP) {
                        Window window = ComponentActivity.this.getWindow();
                        View peekDecorView = window != null ? window.peekDecorView() : null;
                        if (peekDecorView != null) {
                            peekDecorView.cancelPendingInputEvents();
                        }
                    }
                }
            });
            this.c.a(new LifecycleEventObserver() {
                /* class androidx.activity.ComponentActivity.AnonymousClass3 */

                public void a(LifecycleOwner lifecycleOwner, Lifecycle.a aVar) {
                    if (aVar == Lifecycle.a.ON_DESTROY && !ComponentActivity.this.isChangingConfigurations()) {
                        ComponentActivity.this.f().a();
                    }
                }
            });
            if (Build.VERSION.SDK_INT <= 23) {
                this.c.a(new ImmLeaksCleaner(this));
                return;
            }
            return;
        }
        throw new IllegalStateException("getLifecycle() returned null in ComponentActivity's constructor. Please make sure you are lazily constructing your Lifecycle in the first call to getLifecycle() rather than relying on field initialization.");
    }

    public final OnBackPressedDispatcher c() {
        return this.f2f;
    }

    public final SavedStateRegistry d() {
        return this.d.b;
    }

    public ViewModelStore f() {
        if (getApplication() != null) {
            if (this.f1e == null) {
                b bVar = (b) getLastNonConfigurationInstance();
                if (bVar != null) {
                    this.f1e = bVar.a;
                }
                if (this.f1e == null) {
                    this.f1e = new ViewModelStore();
                }
            }
            return this.f1e;
        }
        throw new IllegalStateException("Your activity is not yet attached to the Application instance. You can't request ViewModel before onCreate call.");
    }

    public void onBackPressed() {
        this.f2f.a();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.d.a(bundle);
        ReportFragment.b(this);
    }

    public final Object onRetainNonConfigurationInstance() {
        b bVar;
        ViewModelStore viewModelStore = this.f1e;
        if (viewModelStore == null && (bVar = (b) getLastNonConfigurationInstance()) != null) {
            viewModelStore = bVar.a;
        }
        if (viewModelStore == null) {
            return null;
        }
        b bVar2 = new b();
        bVar2.a = viewModelStore;
        return bVar2;
    }

    public void onSaveInstanceState(Bundle bundle) {
        LifecycleRegistry lifecycleRegistry = this.c;
        if (lifecycleRegistry instanceof LifecycleRegistry) {
            lifecycleRegistry.a(Lifecycle.b.CREATED);
        }
        super.onSaveInstanceState(bundle);
        this.d.b(bundle);
    }

    public Lifecycle a() {
        return this.c;
    }
}
