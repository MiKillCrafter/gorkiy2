package androidx.preference;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Handler;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import i.b.k.ResourcesFlusher;
import i.e.SimpleArrayMap;
import i.q.d;
import java.util.ArrayList;
import java.util.List;

public abstract class PreferenceGroup extends Preference {

    /* renamed from: p  reason: collision with root package name */
    public final SimpleArrayMap<String, Long> f266p;

    /* renamed from: q  reason: collision with root package name */
    public List<Preference> f267q;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, int, int, boolean):boolean
     arg types: [android.content.res.TypedArray, int, int, int]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, int, int, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int, boolean, int):int
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, org.xmlpull.v1.XmlPullParser, android.util.AttributeSet, android.content.res.Resources$Theme):android.content.res.ColorStateList
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, android.content.res.Resources$Theme, android.util.AttributeSet, int[]):android.content.res.TypedArray
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, org.xmlpull.v1.XmlPullParser, java.lang.String, int):java.lang.String
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, java.lang.String, boolean):void
      i.b.k.ResourcesFlusher.a(int, android.graphics.Rect, android.graphics.Rect, android.graphics.Rect):boolean
      i.b.k.ResourcesFlusher.a(i.f.a.h.d, i.f.a.h.f, java.util.List<i.f.a.h.f>, boolean):boolean
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, int, int, boolean):boolean */
    public PreferenceGroup(Context context, AttributeSet attributeSet, int i2, int i3) {
        super(context, attributeSet, i2, i3);
        this.f266p = new SimpleArrayMap<>();
        new Handler();
        this.f267q = new ArrayList();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, d.PreferenceGroup, i2, i3);
        int i4 = d.PreferenceGroup_orderingFromXml;
        ResourcesFlusher.a(obtainStyledAttributes, i4, i4, true);
        if (obtainStyledAttributes.hasValue(d.PreferenceGroup_initialExpandedChildrenCount)) {
            int i5 = d.PreferenceGroup_initialExpandedChildrenCount;
            if (obtainStyledAttributes.getInt(i5, obtainStyledAttributes.getInt(i5, Integer.MAX_VALUE)) != Integer.MAX_VALUE && !(!TextUtils.isEmpty(super.f258f))) {
                Log.e("PreferenceGroup", getClass().getSimpleName() + " should have a key defined if it contains an expandable preference");
            }
        }
        obtainStyledAttributes.recycle();
    }

    public PreferenceGroup(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0);
    }
}
