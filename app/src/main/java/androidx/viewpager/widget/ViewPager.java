package androidx.viewpager.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.WindowInsets;
import android.view.accessibility.AccessibilityEvent;
import android.view.animation.Interpolator;
import android.widget.EdgeEffect;
import android.widget.Scroller;
import com.crashlytics.android.answers.AnswersRetryFilesSender;
import com.crashlytics.android.core.CodedOutputStream;
import com.google.android.material.textfield.TextInputEditText;
import e.a.a.a.a.LoginPagerAdapter;
import e.a.a.a.a.LoginPagerAdapter0;
import e.a.a.a.a.LoginPagerAdapter1;
import e.a.a.a.a.LoginPagerAdapter2;
import e.a.a.a.a.LoginPagerAdapter3;
import e.a.a.a.a.LoginPagerAdapter4;
import e.a.a.a.a.LoginPagerAdapter5;
import e.c.d.a.ViewAction;
import i.h.e.ContextCompat;
import i.h.l.AccessibilityDelegateCompat;
import i.h.l.OnApplyWindowInsetsListener;
import i.h.l.ViewCompat;
import i.h.l.WindowInsetsCompat;
import i.h.l.x.AccessibilityNodeInfoCompat;
import i.j.a.AbsSavedState;
import i.z.a.PagerAdapter;
import j.a.a.a.outline;
import j.f.a.MaskedTextChangedListener;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import l.b.Observable;
import l.b.s.CompositeDisposable;
import n.i.Collections;
import n.n.c.Intrinsics;

public class ViewPager extends ViewGroup {
    public static final int[] b0 = {16842931};
    public static final Comparator<f> c0 = new a();
    public static final Interpolator d0 = new b();
    public static final n e0 = new n();
    public int A;
    public int B;
    public int C;
    public float D;
    public float E;
    public float F;
    public float G;
    public int H = -1;
    public VelocityTracker I;
    public int J;
    public int K;
    public int L;
    public int M;
    public EdgeEffect N;
    public EdgeEffect O;
    public boolean P = true;
    public boolean Q;
    public int R;
    public List<j> S;
    public j T;
    public List<i> U;
    public ArrayList<View> V;
    public final Runnable W = new c();
    public int a0 = 0;
    public int b;
    public final ArrayList<f> c = new ArrayList<>();
    public final f d = new f();

    /* renamed from: e  reason: collision with root package name */
    public final Rect f349e = new Rect();

    /* renamed from: f  reason: collision with root package name */
    public PagerAdapter f350f;
    public int g;
    public int h = -1;

    /* renamed from: i  reason: collision with root package name */
    public Parcelable f351i = null;

    /* renamed from: j  reason: collision with root package name */
    public ClassLoader f352j = null;

    /* renamed from: k  reason: collision with root package name */
    public Scroller f353k;

    /* renamed from: l  reason: collision with root package name */
    public boolean f354l;

    /* renamed from: m  reason: collision with root package name */
    public k f355m;

    /* renamed from: n  reason: collision with root package name */
    public int f356n;

    /* renamed from: o  reason: collision with root package name */
    public Drawable f357o;

    /* renamed from: p  reason: collision with root package name */
    public int f358p;

    /* renamed from: q  reason: collision with root package name */
    public int f359q;

    /* renamed from: r  reason: collision with root package name */
    public float f360r = -3.4028235E38f;

    /* renamed from: s  reason: collision with root package name */
    public float f361s = Float.MAX_VALUE;

    /* renamed from: t  reason: collision with root package name */
    public int f362t;
    public boolean u;
    public boolean v;
    public boolean w;
    public int x = 1;
    public boolean y;
    public boolean z;

    public static class a implements Comparator<f> {
        public int compare(Object obj, Object obj2) {
            return ((f) obj).b - ((f) obj2).b;
        }
    }

    public static class b implements Interpolator {
        public float getInterpolation(float f2) {
            float f3 = f2 - 1.0f;
            return (f3 * f3 * f3 * f3 * f3) + 1.0f;
        }
    }

    public class c implements Runnable {
        public c() {
        }

        public void run() {
            ViewPager.this.setScrollState(0);
            ViewPager viewPager = ViewPager.this;
            viewPager.e(viewPager.g);
        }
    }

    public class d implements OnApplyWindowInsetsListener {
        public final Rect a = new Rect();

        public d() {
        }

        public WindowInsetsCompat a(View view, WindowInsetsCompat windowInsetsCompat) {
            WindowInsetsCompat a2 = ViewCompat.a(view, windowInsetsCompat);
            if (a2.e()) {
                return a2;
            }
            Rect rect = this.a;
            rect.left = a2.b();
            rect.top = a2.d();
            rect.right = a2.c();
            rect.bottom = a2.a();
            int childCount = ViewPager.this.getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                View childAt = ViewPager.this.getChildAt(i2);
                WindowInsets windowInsets = (WindowInsets) a2.a;
                WindowInsets dispatchApplyWindowInsets = childAt.dispatchApplyWindowInsets(windowInsets);
                if (!dispatchApplyWindowInsets.equals(windowInsets)) {
                    windowInsets = new WindowInsets(dispatchApplyWindowInsets);
                }
                WindowInsetsCompat a3 = WindowInsetsCompat.a(windowInsets);
                rect.left = Math.min(a3.b(), rect.left);
                rect.top = Math.min(a3.d(), rect.top);
                rect.right = Math.min(a3.c(), rect.right);
                rect.bottom = Math.min(a3.a(), rect.bottom);
            }
            return new WindowInsetsCompat(((WindowInsets) a2.a).replaceSystemWindowInsets(rect.left, rect.top, rect.right, rect.bottom));
        }
    }

    @Inherited
    @Target({ElementType.TYPE})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface e {
    }

    public static class f {
        public Object a;
        public int b;
        public boolean c;
        public float d;

        /* renamed from: e  reason: collision with root package name */
        public float f363e;
    }

    public interface i {
        void a(ViewPager viewPager, PagerAdapter pagerAdapter, PagerAdapter pagerAdapter2);
    }

    public interface j {
        void a(int i2);

        void a(int i2, float f2, int i3);

        void b(int i2);
    }

    public class k extends DataSetObserver {
        public k() {
        }

        public void onChanged() {
            ViewPager.this.a();
        }

        public void onInvalidated() {
            ViewPager.this.a();
        }
    }

    public static class l extends AbsSavedState {
        public static final Parcelable.Creator<l> CREATOR = new a();
        public int d;

        /* renamed from: e  reason: collision with root package name */
        public Parcelable f366e;

        /* renamed from: f  reason: collision with root package name */
        public ClassLoader f367f;

        public static class a implements Parcelable.ClassLoaderCreator<l> {
            public Object createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new l(parcel, classLoader);
            }

            public Object[] newArray(int i2) {
                return new l[i2];
            }

            public Object createFromParcel(Parcel parcel) {
                return new l(parcel, null);
            }
        }

        public l(Parcelable parcelable) {
            super(parcelable);
        }

        public String toString() {
            StringBuilder a2 = outline.a("FragmentPager.SavedState{");
            a2.append(Integer.toHexString(System.identityHashCode(this)));
            a2.append(" position=");
            a2.append(this.d);
            a2.append("}");
            return a2.toString();
        }

        public void writeToParcel(Parcel parcel, int i2) {
            parcel.writeParcelable(super.b, i2);
            parcel.writeInt(this.d);
            parcel.writeParcelable(this.f366e, i2);
        }

        public l(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            classLoader = classLoader == null ? l.class.getClassLoader() : classLoader;
            this.d = parcel.readInt();
            this.f366e = parcel.readParcelable(classLoader);
            this.f367f = classLoader;
        }
    }

    public static class m implements j {
        public void a(int i2) {
        }

        public void a(int i2, float f2, int i3) {
        }
    }

    public static class n implements Comparator<View> {
        public int compare(Object obj, Object obj2) {
            g gVar = (g) ((View) obj).getLayoutParams();
            g gVar2 = (g) ((View) obj2).getLayoutParams();
            boolean z = gVar.a;
            if (z != gVar2.a) {
                return z ? 1 : -1;
            }
            return gVar.f364e - gVar2.f364e;
        }
    }

    public ViewPager(Context context) {
        super(context);
        c();
    }

    private int getClientWidth() {
        return (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight();
    }

    private void setScrollingCacheEnabled(boolean z2) {
        if (this.v != z2) {
            this.v = z2;
        }
    }

    public void a(int i2, boolean z2, boolean z3) {
        a(i2, z2, z3, 0);
    }

    public void addFocusables(ArrayList<View> arrayList, int i2, int i3) {
        f a2;
        int size = arrayList.size();
        int descendantFocusability = getDescendantFocusability();
        if (descendantFocusability != 393216) {
            for (int i4 = 0; i4 < getChildCount(); i4++) {
                View childAt = getChildAt(i4);
                if (childAt.getVisibility() == 0 && (a2 = a(childAt)) != null && a2.b == this.g) {
                    childAt.addFocusables(arrayList, i2, i3);
                }
            }
        }
        if ((descendantFocusability == 262144 && size != arrayList.size()) || !isFocusable()) {
            return;
        }
        if ((i3 & 1) != 1 || !isInTouchMode() || isFocusableInTouchMode()) {
            arrayList.add(this);
        }
    }

    public void addTouchables(ArrayList<View> arrayList) {
        f a2;
        for (int i2 = 0; i2 < getChildCount(); i2++) {
            View childAt = getChildAt(i2);
            if (childAt.getVisibility() == 0 && (a2 = a(childAt)) != null && a2.b == this.g) {
                childAt.addTouchables(arrayList);
            }
        }
    }

    public void addView(View view, int i2, ViewGroup.LayoutParams layoutParams) {
        if (!checkLayoutParams(layoutParams)) {
            layoutParams = new g();
        }
        g gVar = (g) layoutParams;
        boolean z2 = gVar.a | (view.getClass().getAnnotation(e.class) != null);
        gVar.a = z2;
        if (!this.u) {
            super.addView(view, i2, layoutParams);
        } else if (!z2) {
            gVar.d = true;
            addViewInLayout(view, i2, layoutParams);
        } else {
            throw new IllegalStateException("Cannot add pager decor view during layout");
        }
    }

    public final void b(int i2) {
        j jVar = this.T;
        if (jVar != null) {
            jVar.b(i2);
        }
        List<j> list = this.S;
        if (list != null) {
            int size = list.size();
            for (int i3 = 0; i3 < size; i3++) {
                j jVar2 = this.S.get(i3);
                if (jVar2 != null) {
                    jVar2.b(i2);
                }
            }
        }
    }

    public void c() {
        setWillNotDraw(false);
        setDescendantFocusability(262144);
        setFocusable(true);
        Context context = getContext();
        this.f353k = new Scroller(context, d0);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        float f2 = context.getResources().getDisplayMetrics().density;
        this.C = viewConfiguration.getScaledPagingTouchSlop();
        this.J = (int) (400.0f * f2);
        this.K = viewConfiguration.getScaledMaximumFlingVelocity();
        this.N = new EdgeEffect(context);
        this.O = new EdgeEffect(context);
        this.L = (int) (25.0f * f2);
        this.M = (int) (2.0f * f2);
        this.A = (int) (f2 * 16.0f);
        ViewCompat.a(this, new h());
        if (getImportantForAccessibility() == 0) {
            setImportantForAccessibility(1);
        }
        ViewCompat.a(this, new d());
    }

    public boolean canScrollHorizontally(int i2) {
        if (this.f350f == null) {
            return false;
        }
        int clientWidth = getClientWidth();
        int scrollX = getScrollX();
        if (i2 < 0) {
            if (scrollX > ((int) (((float) clientWidth) * this.f360r))) {
                return true;
            }
            return false;
        } else if (i2 <= 0 || scrollX >= ((int) (((float) clientWidth) * this.f361s))) {
            return false;
        } else {
            return true;
        }
    }

    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof g) && super.checkLayoutParams(layoutParams);
    }

    public void computeScroll() {
        this.f354l = true;
        if (this.f353k.isFinished() || !this.f353k.computeScrollOffset()) {
            a(true);
            return;
        }
        int scrollX = getScrollX();
        int scrollY = getScrollY();
        int currX = this.f353k.getCurrX();
        int currY = this.f353k.getCurrY();
        if (!(scrollX == currX && scrollY == currY)) {
            scrollTo(currX, currY);
            if (!d(currX)) {
                this.f353k.abortAnimation();
                scrollTo(0, currY);
            }
        }
        ViewCompat.A(this);
    }

    public final boolean d(int i2) {
        if (this.c.size() != 0) {
            f b2 = b();
            int clientWidth = getClientWidth();
            int i3 = this.f356n;
            int i4 = clientWidth + i3;
            float f2 = (float) clientWidth;
            int i5 = b2.b;
            float f3 = ((((float) i2) / f2) - b2.f363e) / (b2.d + (((float) i3) / f2));
            this.Q = false;
            a(i5, f3, (int) (((float) i4) * f3));
            if (this.Q) {
                return true;
            }
            throw new IllegalStateException("onPageScrolled did not call superclass implementation");
        } else if (this.P) {
            return false;
        } else {
            this.Q = false;
            a(0, 0.0f, 0);
            if (this.Q) {
                return false;
            }
            throw new IllegalStateException("onPageScrolled did not call superclass implementation");
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean dispatchKeyEvent(android.view.KeyEvent r6) {
        /*
            r5 = this;
            boolean r0 = super.dispatchKeyEvent(r6)
            r1 = 0
            r2 = 1
            if (r0 != 0) goto L_0x005d
            int r0 = r6.getAction()
            if (r0 != 0) goto L_0x005a
            int r0 = r6.getKeyCode()
            r3 = 21
            r4 = 2
            if (r0 == r3) goto L_0x0048
            r3 = 22
            if (r0 == r3) goto L_0x0036
            r3 = 61
            if (r0 == r3) goto L_0x0020
            goto L_0x005a
        L_0x0020:
            boolean r0 = r6.hasNoModifiers()
            if (r0 == 0) goto L_0x002b
            boolean r6 = r5.a(r4)
            goto L_0x005b
        L_0x002b:
            boolean r6 = r6.hasModifiers(r2)
            if (r6 == 0) goto L_0x005a
            boolean r6 = r5.a(r2)
            goto L_0x005b
        L_0x0036:
            boolean r6 = r6.hasModifiers(r4)
            if (r6 == 0) goto L_0x0041
            boolean r6 = r5.e()
            goto L_0x005b
        L_0x0041:
            r6 = 66
            boolean r6 = r5.a(r6)
            goto L_0x005b
        L_0x0048:
            boolean r6 = r6.hasModifiers(r4)
            if (r6 == 0) goto L_0x0053
            boolean r6 = r5.d()
            goto L_0x005b
        L_0x0053:
            r6 = 17
            boolean r6 = r5.a(r6)
            goto L_0x005b
        L_0x005a:
            r6 = 0
        L_0x005b:
            if (r6 == 0) goto L_0x005e
        L_0x005d:
            r1 = 1
        L_0x005e:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.viewpager.widget.ViewPager.dispatchKeyEvent(android.view.KeyEvent):boolean");
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        f a2;
        if (accessibilityEvent.getEventType() == 4096) {
            return super.dispatchPopulateAccessibilityEvent(accessibilityEvent);
        }
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (childAt.getVisibility() == 0 && (a2 = a(childAt)) != null && a2.b == this.g && childAt.dispatchPopulateAccessibilityEvent(accessibilityEvent)) {
                return true;
            }
        }
        return false;
    }

    public void draw(Canvas canvas) {
        PagerAdapter pagerAdapter;
        super.draw(canvas);
        int overScrollMode = getOverScrollMode();
        boolean z2 = false;
        if (overScrollMode == 0 || (overScrollMode == 1 && (pagerAdapter = this.f350f) != null && pagerAdapter.a() > 1)) {
            if (!this.N.isFinished()) {
                int save = canvas.save();
                int height = (getHeight() - getPaddingTop()) - getPaddingBottom();
                int width = getWidth();
                canvas.rotate(270.0f);
                canvas.translate((float) (getPaddingTop() + (-height)), this.f360r * ((float) width));
                this.N.setSize(height, width);
                z2 = false | this.N.draw(canvas);
                canvas.restoreToCount(save);
            }
            if (!this.O.isFinished()) {
                int save2 = canvas.save();
                int width2 = getWidth();
                int height2 = (getHeight() - getPaddingTop()) - getPaddingBottom();
                canvas.rotate(90.0f);
                canvas.translate((float) (-getPaddingTop()), (-(this.f361s + 1.0f)) * ((float) width2));
                this.O.setSize(height2, width2);
                z2 |= this.O.draw(canvas);
                canvas.restoreToCount(save2);
            }
        } else {
            this.N.finish();
            this.O.finish();
        }
        if (z2) {
            ViewCompat.A(this);
        }
    }

    public void drawableStateChanged() {
        super.drawableStateChanged();
        Drawable drawable = this.f357o;
        if (drawable != null && drawable.isStateful()) {
            drawable.setState(getDrawableState());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:154:0x0274, code lost:
        r7 = r7 - (r8.d + r3);
        r8.f363e = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:155:0x027a, code lost:
        if (r10 != 0) goto L_0x027e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:156:0x027c, code lost:
        r14.f360r = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:157:0x027e, code lost:
        r4 = r4 - 1;
        r9 = r9 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:168:0x02a6, code lost:
        if (r8 != r15) goto L_0x02ae;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:169:0x02a8, code lost:
        r14.f361s = (r7.d + r4) - 1.0f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0056, code lost:
        if (r6 == r7) goto L_0x005d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void e(int r15) {
        /*
            r14 = this;
            int r0 = r14.g
            r1 = 0
            if (r0 == r15) goto L_0x000c
            androidx.viewpager.widget.ViewPager$f r0 = r14.c(r0)
            r14.g = r15
            goto L_0x000d
        L_0x000c:
            r0 = r1
        L_0x000d:
            i.z.a.PagerAdapter r15 = r14.f350f
            if (r15 != 0) goto L_0x0012
            return
        L_0x0012:
            boolean r15 = r14.w
            if (r15 == 0) goto L_0x0017
            return
        L_0x0017:
            android.os.IBinder r15 = r14.getWindowToken()
            if (r15 != 0) goto L_0x001e
            return
        L_0x001e:
            i.z.a.PagerAdapter r15 = r14.f350f
            if (r15 == 0) goto L_0x038f
            int r15 = r14.x
            int r2 = r14.g
            int r2 = r2 - r15
            r3 = 0
            int r2 = java.lang.Math.max(r3, r2)
            i.z.a.PagerAdapter r3 = r14.f350f
            int r3 = r3.a()
            int r4 = r3 + -1
            int r5 = r14.g
            int r5 = r5 + r15
            int r15 = java.lang.Math.min(r4, r5)
            int r4 = r14.b
            if (r3 != r4) goto L_0x033d
            r4 = 0
        L_0x0040:
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r5 = r14.c
            int r5 = r5.size()
            if (r4 >= r5) goto L_0x005c
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r5 = r14.c
            java.lang.Object r5 = r5.get(r4)
            androidx.viewpager.widget.ViewPager$f r5 = (androidx.viewpager.widget.ViewPager.f) r5
            int r6 = r5.b
            int r7 = r14.g
            if (r6 < r7) goto L_0x0059
            if (r6 != r7) goto L_0x005c
            goto L_0x005d
        L_0x0059:
            int r4 = r4 + 1
            goto L_0x0040
        L_0x005c:
            r5 = r1
        L_0x005d:
            if (r5 != 0) goto L_0x0067
            if (r3 <= 0) goto L_0x0067
            int r5 = r14.g
            androidx.viewpager.widget.ViewPager$f r5 = r14.a(r5, r4)
        L_0x0067:
            if (r5 == 0) goto L_0x02bd
            int r6 = r4 + -1
            if (r6 < 0) goto L_0x0076
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r7 = r14.c
            java.lang.Object r7 = r7.get(r6)
            androidx.viewpager.widget.ViewPager$f r7 = (androidx.viewpager.widget.ViewPager.f) r7
            goto L_0x0077
        L_0x0076:
            r7 = r1
        L_0x0077:
            int r8 = r14.getClientWidth()
            r9 = 1073741824(0x40000000, float:2.0)
            if (r8 > 0) goto L_0x0081
            r10 = 0
            goto L_0x008d
        L_0x0081:
            float r10 = r5.d
            float r10 = r9 - r10
            int r11 = r14.getPaddingLeft()
            float r11 = (float) r11
            float r12 = (float) r8
            float r11 = r11 / r12
            float r10 = r10 + r11
        L_0x008d:
            int r11 = r14.g
            int r11 = r11 + -1
            r12 = 0
        L_0x0092:
            if (r11 < 0) goto L_0x00f0
            int r13 = (r12 > r10 ? 1 : (r12 == r10 ? 0 : -1))
            if (r13 < 0) goto L_0x00c0
            if (r11 >= r2) goto L_0x00c0
            if (r7 != 0) goto L_0x009d
            goto L_0x00f0
        L_0x009d:
            int r13 = r7.b
            if (r11 != r13) goto L_0x00ed
            boolean r13 = r7.c
            if (r13 != 0) goto L_0x00ed
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r13 = r14.c
            r13.remove(r6)
            i.z.a.PagerAdapter r13 = r14.f350f
            java.lang.Object r7 = r7.a
            r13.a(r14, r11, r7)
            int r6 = r6 + -1
            int r4 = r4 + -1
            if (r6 < 0) goto L_0x00ec
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r7 = r14.c
            java.lang.Object r7 = r7.get(r6)
            androidx.viewpager.widget.ViewPager$f r7 = (androidx.viewpager.widget.ViewPager.f) r7
            goto L_0x00ed
        L_0x00c0:
            if (r7 == 0) goto L_0x00d6
            int r13 = r7.b
            if (r11 != r13) goto L_0x00d6
            float r7 = r7.d
            float r12 = r12 + r7
            int r6 = r6 + -1
            if (r6 < 0) goto L_0x00ec
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r7 = r14.c
            java.lang.Object r7 = r7.get(r6)
            androidx.viewpager.widget.ViewPager$f r7 = (androidx.viewpager.widget.ViewPager.f) r7
            goto L_0x00ed
        L_0x00d6:
            int r7 = r6 + 1
            androidx.viewpager.widget.ViewPager$f r7 = r14.a(r11, r7)
            float r7 = r7.d
            float r12 = r12 + r7
            int r4 = r4 + 1
            if (r6 < 0) goto L_0x00ec
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r7 = r14.c
            java.lang.Object r7 = r7.get(r6)
            androidx.viewpager.widget.ViewPager$f r7 = (androidx.viewpager.widget.ViewPager.f) r7
            goto L_0x00ed
        L_0x00ec:
            r7 = r1
        L_0x00ed:
            int r11 = r11 + -1
            goto L_0x0092
        L_0x00f0:
            float r2 = r5.d
            int r6 = r4 + 1
            int r7 = (r2 > r9 ? 1 : (r2 == r9 ? 0 : -1))
            if (r7 >= 0) goto L_0x0186
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r7 = r14.c
            int r7 = r7.size()
            if (r6 >= r7) goto L_0x0109
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r7 = r14.c
            java.lang.Object r7 = r7.get(r6)
            androidx.viewpager.widget.ViewPager$f r7 = (androidx.viewpager.widget.ViewPager.f) r7
            goto L_0x010a
        L_0x0109:
            r7 = r1
        L_0x010a:
            if (r8 > 0) goto L_0x010e
            r8 = 0
            goto L_0x0117
        L_0x010e:
            int r10 = r14.getPaddingRight()
            float r10 = (float) r10
            float r8 = (float) r8
            float r10 = r10 / r8
            float r8 = r10 + r9
        L_0x0117:
            int r9 = r14.g
            int r9 = r9 + 1
            r10 = r6
        L_0x011c:
            if (r9 >= r3) goto L_0x0186
            int r11 = (r2 > r8 ? 1 : (r2 == r8 ? 0 : -1))
            if (r11 < 0) goto L_0x014c
            if (r9 <= r15) goto L_0x014c
            if (r7 != 0) goto L_0x0127
            goto L_0x0186
        L_0x0127:
            int r11 = r7.b
            if (r9 != r11) goto L_0x0183
            boolean r11 = r7.c
            if (r11 != 0) goto L_0x0183
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r11 = r14.c
            r11.remove(r10)
            i.z.a.PagerAdapter r11 = r14.f350f
            java.lang.Object r7 = r7.a
            r11.a(r14, r9, r7)
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r7 = r14.c
            int r7 = r7.size()
            if (r10 >= r7) goto L_0x0182
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r7 = r14.c
            java.lang.Object r7 = r7.get(r10)
            androidx.viewpager.widget.ViewPager$f r7 = (androidx.viewpager.widget.ViewPager.f) r7
            goto L_0x0183
        L_0x014c:
            if (r7 == 0) goto L_0x0168
            int r11 = r7.b
            if (r9 != r11) goto L_0x0168
            float r7 = r7.d
            float r2 = r2 + r7
            int r10 = r10 + 1
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r7 = r14.c
            int r7 = r7.size()
            if (r10 >= r7) goto L_0x0182
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r7 = r14.c
            java.lang.Object r7 = r7.get(r10)
            androidx.viewpager.widget.ViewPager$f r7 = (androidx.viewpager.widget.ViewPager.f) r7
            goto L_0x0183
        L_0x0168:
            androidx.viewpager.widget.ViewPager$f r7 = r14.a(r9, r10)
            int r10 = r10 + 1
            float r7 = r7.d
            float r2 = r2 + r7
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r7 = r14.c
            int r7 = r7.size()
            if (r10 >= r7) goto L_0x0182
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r7 = r14.c
            java.lang.Object r7 = r7.get(r10)
            androidx.viewpager.widget.ViewPager$f r7 = (androidx.viewpager.widget.ViewPager.f) r7
            goto L_0x0183
        L_0x0182:
            r7 = r1
        L_0x0183:
            int r9 = r9 + 1
            goto L_0x011c
        L_0x0186:
            i.z.a.PagerAdapter r15 = r14.f350f
            int r15 = r15.a()
            int r2 = r14.getClientWidth()
            if (r2 <= 0) goto L_0x0198
            int r3 = r14.f356n
            float r3 = (float) r3
            float r2 = (float) r2
            float r3 = r3 / r2
            goto L_0x0199
        L_0x0198:
            r3 = 0
        L_0x0199:
            r2 = 1065353216(0x3f800000, float:1.0)
            if (r0 == 0) goto L_0x0232
            int r7 = r0.b
            int r8 = r5.b
            if (r7 >= r8) goto L_0x01ef
            float r8 = r0.f363e
            float r0 = r0.d
            float r8 = r8 + r0
            float r8 = r8 + r3
            r0 = 0
        L_0x01aa:
            int r7 = r7 + 1
            int r9 = r5.b
            if (r7 > r9) goto L_0x0232
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r9 = r14.c
            int r9 = r9.size()
            if (r0 >= r9) goto L_0x0232
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r9 = r14.c
            java.lang.Object r9 = r9.get(r0)
            androidx.viewpager.widget.ViewPager$f r9 = (androidx.viewpager.widget.ViewPager.f) r9
        L_0x01c0:
            int r10 = r9.b
            if (r7 <= r10) goto L_0x01d9
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r10 = r14.c
            int r10 = r10.size()
            int r10 = r10 + -1
            if (r0 >= r10) goto L_0x01d9
            int r0 = r0 + 1
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r9 = r14.c
            java.lang.Object r9 = r9.get(r0)
            androidx.viewpager.widget.ViewPager$f r9 = (androidx.viewpager.widget.ViewPager.f) r9
            goto L_0x01c0
        L_0x01d9:
            int r10 = r9.b
            if (r7 >= r10) goto L_0x01e8
            i.z.a.PagerAdapter r10 = r14.f350f
            if (r10 == 0) goto L_0x01e7
            float r10 = r2 + r3
            float r8 = r8 + r10
            int r7 = r7 + 1
            goto L_0x01d9
        L_0x01e7:
            throw r1
        L_0x01e8:
            r9.f363e = r8
            float r9 = r9.d
            float r9 = r9 + r3
            float r8 = r8 + r9
            goto L_0x01aa
        L_0x01ef:
            if (r7 <= r8) goto L_0x0232
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r8 = r14.c
            int r8 = r8.size()
            int r8 = r8 + -1
            float r0 = r0.f363e
        L_0x01fb:
            int r7 = r7 + -1
            int r9 = r5.b
            if (r7 < r9) goto L_0x0232
            if (r8 < 0) goto L_0x0232
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r9 = r14.c
            java.lang.Object r9 = r9.get(r8)
            androidx.viewpager.widget.ViewPager$f r9 = (androidx.viewpager.widget.ViewPager.f) r9
        L_0x020b:
            int r10 = r9.b
            if (r7 >= r10) goto L_0x021c
            if (r8 <= 0) goto L_0x021c
            int r8 = r8 + -1
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r9 = r14.c
            java.lang.Object r9 = r9.get(r8)
            androidx.viewpager.widget.ViewPager$f r9 = (androidx.viewpager.widget.ViewPager.f) r9
            goto L_0x020b
        L_0x021c:
            int r10 = r9.b
            if (r7 <= r10) goto L_0x022b
            i.z.a.PagerAdapter r10 = r14.f350f
            if (r10 == 0) goto L_0x022a
            float r10 = r2 + r3
            float r0 = r0 - r10
            int r7 = r7 + -1
            goto L_0x021c
        L_0x022a:
            throw r1
        L_0x022b:
            float r10 = r9.d
            float r10 = r10 + r3
            float r0 = r0 - r10
            r9.f363e = r0
            goto L_0x01fb
        L_0x0232:
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r0 = r14.c
            int r0 = r0.size()
            float r7 = r5.f363e
            int r8 = r5.b
            int r9 = r8 + -1
            if (r8 != 0) goto L_0x0242
            r8 = r7
            goto L_0x0245
        L_0x0242:
            r8 = -8388609(0xffffffffff7fffff, float:-3.4028235E38)
        L_0x0245:
            r14.f360r = r8
            int r8 = r5.b
            int r15 = r15 + -1
            if (r8 != r15) goto L_0x0254
            float r8 = r5.f363e
            float r10 = r5.d
            float r8 = r8 + r10
            float r8 = r8 - r2
            goto L_0x0257
        L_0x0254:
            r8 = 2139095039(0x7f7fffff, float:3.4028235E38)
        L_0x0257:
            r14.f361s = r8
            int r4 = r4 + -1
        L_0x025b:
            if (r4 < 0) goto L_0x0283
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r8 = r14.c
            java.lang.Object r8 = r8.get(r4)
            androidx.viewpager.widget.ViewPager$f r8 = (androidx.viewpager.widget.ViewPager.f) r8
        L_0x0265:
            int r10 = r8.b
            if (r9 <= r10) goto L_0x0274
            i.z.a.PagerAdapter r10 = r14.f350f
            int r9 = r9 + -1
            if (r10 == 0) goto L_0x0273
            float r10 = r2 + r3
            float r7 = r7 - r10
            goto L_0x0265
        L_0x0273:
            throw r1
        L_0x0274:
            float r11 = r8.d
            float r11 = r11 + r3
            float r7 = r7 - r11
            r8.f363e = r7
            if (r10 != 0) goto L_0x027e
            r14.f360r = r7
        L_0x027e:
            int r4 = r4 + -1
            int r9 = r9 + -1
            goto L_0x025b
        L_0x0283:
            float r4 = r5.f363e
            float r7 = r5.d
            float r4 = r4 + r7
            float r4 = r4 + r3
            int r5 = r5.b
        L_0x028b:
            int r5 = r5 + 1
            if (r6 >= r0) goto L_0x02b7
            java.util.ArrayList<androidx.viewpager.widget.ViewPager$f> r7 = r14.c
            java.lang.Object r7 = r7.get(r6)
            androidx.viewpager.widget.ViewPager$f r7 = (androidx.viewpager.widget.ViewPager.f) r7
        L_0x0297:
            int r8 = r7.b
            if (r5 >= r8) goto L_0x02a6
            i.z.a.PagerAdapter r8 = r14.f350f
            int r5 = r5 + 1
            if (r8 == 0) goto L_0x02a5
            float r8 = r2 + r3
            float r4 = r4 + r8
            goto L_0x0297
        L_0x02a5:
            throw r1
        L_0x02a6:
            if (r8 != r15) goto L_0x02ae
            float r8 = r7.d
            float r8 = r8 + r4
            float r8 = r8 - r2
            r14.f361s = r8
        L_0x02ae:
            r7.f363e = r4
            float r7 = r7.d
            float r7 = r7 + r3
            float r4 = r4 + r7
            int r6 = r6 + 1
            goto L_0x028b
        L_0x02b7:
            i.z.a.PagerAdapter r15 = r14.f350f
            if (r15 == 0) goto L_0x02bc
            goto L_0x02bd
        L_0x02bc:
            throw r1
        L_0x02bd:
            i.z.a.PagerAdapter r15 = r14.f350f
            if (r15 == 0) goto L_0x033c
            int r15 = r14.getChildCount()
            r0 = 0
        L_0x02c6:
            if (r0 >= r15) goto L_0x02f0
            android.view.View r2 = r14.getChildAt(r0)
            android.view.ViewGroup$LayoutParams r3 = r2.getLayoutParams()
            androidx.viewpager.widget.ViewPager$g r3 = (androidx.viewpager.widget.ViewPager.g) r3
            r3.f365f = r0
            boolean r4 = r3.a
            if (r4 != 0) goto L_0x02ed
            float r4 = r3.c
            r5 = 0
            int r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
            if (r4 != 0) goto L_0x02ed
            androidx.viewpager.widget.ViewPager$f r2 = r14.a(r2)
            if (r2 == 0) goto L_0x02ed
            float r4 = r2.d
            r3.c = r4
            int r2 = r2.b
            r3.f364e = r2
        L_0x02ed:
            int r0 = r0 + 1
            goto L_0x02c6
        L_0x02f0:
            boolean r15 = r14.hasFocus()
            if (r15 == 0) goto L_0x033b
            android.view.View r15 = r14.findFocus()
            if (r15 == 0) goto L_0x0311
        L_0x02fc:
            android.view.ViewParent r0 = r15.getParent()
            if (r0 == r14) goto L_0x030d
            if (r0 == 0) goto L_0x0311
            boolean r15 = r0 instanceof android.view.View
            if (r15 != 0) goto L_0x0309
            goto L_0x0311
        L_0x0309:
            r15 = r0
            android.view.View r15 = (android.view.View) r15
            goto L_0x02fc
        L_0x030d:
            androidx.viewpager.widget.ViewPager$f r1 = r14.a(r15)
        L_0x0311:
            if (r1 == 0) goto L_0x0319
            int r15 = r1.b
            int r0 = r14.g
            if (r15 == r0) goto L_0x033b
        L_0x0319:
            r15 = 0
        L_0x031a:
            int r0 = r14.getChildCount()
            if (r15 >= r0) goto L_0x033b
            android.view.View r0 = r14.getChildAt(r15)
            androidx.viewpager.widget.ViewPager$f r1 = r14.a(r0)
            if (r1 == 0) goto L_0x0338
            int r1 = r1.b
            int r2 = r14.g
            if (r1 != r2) goto L_0x0338
            r1 = 2
            boolean r0 = r0.requestFocus(r1)
            if (r0 == 0) goto L_0x0338
            goto L_0x033b
        L_0x0338:
            int r15 = r15 + 1
            goto L_0x031a
        L_0x033b:
            return
        L_0x033c:
            throw r1
        L_0x033d:
            android.content.res.Resources r15 = r14.getResources()     // Catch:{ NotFoundException -> 0x034a }
            int r0 = r14.getId()     // Catch:{ NotFoundException -> 0x034a }
            java.lang.String r15 = r15.getResourceName(r0)     // Catch:{ NotFoundException -> 0x034a }
            goto L_0x0352
        L_0x034a:
            int r15 = r14.getId()
            java.lang.String r15 = java.lang.Integer.toHexString(r15)
        L_0x0352:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "The application's PagerAdapter changed the adapter's contents without calling PagerAdapter#notifyDataSetChanged! Expected adapter item count: "
            java.lang.StringBuilder r1 = j.a.a.a.outline.a(r1)
            int r2 = r14.b
            r1.append(r2)
            java.lang.String r2 = ", found: "
            r1.append(r2)
            r1.append(r3)
            java.lang.String r2 = " Pager id: "
            r1.append(r2)
            r1.append(r15)
            java.lang.String r15 = " Pager class: "
            r1.append(r15)
            java.lang.Class<androidx.viewpager.widget.ViewPager> r15 = androidx.viewpager.widget.ViewPager.class
            r1.append(r15)
            java.lang.String r15 = " Problematic adapter: "
            r1.append(r15)
            i.z.a.PagerAdapter r15 = r14.f350f
            java.lang.Class r15 = r15.getClass()
            r1.append(r15)
            java.lang.String r15 = r1.toString()
            r0.<init>(r15)
            throw r0
        L_0x038f:
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.viewpager.widget.ViewPager.e(int):void");
    }

    public final boolean f() {
        this.H = -1;
        this.y = false;
        this.z = false;
        VelocityTracker velocityTracker = this.I;
        if (velocityTracker != null) {
            velocityTracker.recycle();
            this.I = null;
        }
        this.N.onRelease();
        this.O.onRelease();
        if (this.N.isFinished() || this.O.isFinished()) {
            return true;
        }
        return false;
    }

    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new g();
    }

    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return new g();
    }

    public PagerAdapter getAdapter() {
        return this.f350f;
    }

    public int getChildDrawingOrder(int i2, int i3) {
        return ((g) this.V.get(i3).getLayoutParams()).f365f;
    }

    public int getCurrentItem() {
        return this.g;
    }

    public int getOffscreenPageLimit() {
        return this.x;
    }

    public int getPageMargin() {
        return this.f356n;
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.P = true;
    }

    public void onDetachedFromWindow() {
        removeCallbacks(this.W);
        Scroller scroller = this.f353k;
        if (scroller != null && !scroller.isFinished()) {
            this.f353k.abortAnimation();
        }
        super.onDetachedFromWindow();
    }

    public void onDraw(Canvas canvas) {
        float f2;
        float f3;
        super.onDraw(canvas);
        if (this.f356n > 0 && this.f357o != null && this.c.size() > 0 && this.f350f != null) {
            int scrollX = getScrollX();
            int width = getWidth();
            float f4 = (float) width;
            float f5 = ((float) this.f356n) / f4;
            int i2 = 0;
            f fVar = this.c.get(0);
            float f6 = fVar.f363e;
            int size = this.c.size();
            int i3 = fVar.b;
            int i4 = this.c.get(size - 1).b;
            while (i3 < i4) {
                while (i3 > fVar.b && i2 < size) {
                    i2++;
                    fVar = this.c.get(i2);
                }
                if (i3 == fVar.b) {
                    float f7 = fVar.f363e;
                    float f8 = fVar.d;
                    f2 = (f7 + f8) * f4;
                    f6 = f7 + f8 + f5;
                } else if (this.f350f != null) {
                    f2 = (f6 + 1.0f) * f4;
                    f6 = 1.0f + f5 + f6;
                } else {
                    throw null;
                }
                if (((float) this.f356n) + f2 > ((float) scrollX)) {
                    f3 = f5;
                    this.f357o.setBounds(Math.round(f2), this.f358p, Math.round(((float) this.f356n) + f2), this.f359q);
                    this.f357o.draw(canvas);
                } else {
                    f3 = f5;
                }
                if (f2 <= ((float) (scrollX + width))) {
                    i3++;
                    f5 = f3;
                } else {
                    return;
                }
            }
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        MotionEvent motionEvent2 = motionEvent;
        int action = motionEvent.getAction() & 255;
        if (action == 3 || action == 1) {
            f();
            return false;
        }
        if (action != 0) {
            if (this.y) {
                return true;
            }
            if (this.z) {
                return false;
            }
        }
        if (action == 0) {
            float x2 = motionEvent.getX();
            this.F = x2;
            this.D = x2;
            float y2 = motionEvent.getY();
            this.G = y2;
            this.E = y2;
            this.H = motionEvent2.getPointerId(0);
            this.z = false;
            this.f354l = true;
            this.f353k.computeScrollOffset();
            if (this.a0 != 2 || Math.abs(this.f353k.getFinalX() - this.f353k.getCurrX()) <= this.M) {
                a(false);
                this.y = false;
            } else {
                this.f353k.abortAnimation();
                this.w = false;
                e(this.g);
                this.y = true;
                b(true);
                setScrollState(1);
            }
        } else if (action == 2) {
            int i2 = this.H;
            if (i2 != -1) {
                int findPointerIndex = motionEvent2.findPointerIndex(i2);
                float x3 = motionEvent2.getX(findPointerIndex);
                float f2 = x3 - this.D;
                float abs = Math.abs(f2);
                float y3 = motionEvent2.getY(findPointerIndex);
                float abs2 = Math.abs(y3 - this.G);
                int i3 = (f2 > 0.0f ? 1 : (f2 == 0.0f ? 0 : -1));
                if (i3 != 0) {
                    float f3 = this.D;
                    if (!((f3 < ((float) this.B) && i3 > 0) || (f3 > ((float) (getWidth() - this.B)) && f2 < 0.0f)) && a(this, false, (int) f2, (int) x3, (int) y3)) {
                        this.D = x3;
                        this.E = y3;
                        this.z = true;
                        return false;
                    }
                }
                if (abs > ((float) this.C) && abs * 0.5f > abs2) {
                    this.y = true;
                    b(true);
                    setScrollState(1);
                    float f4 = this.F;
                    float f5 = (float) this.C;
                    this.D = i3 > 0 ? f4 + f5 : f4 - f5;
                    this.E = y3;
                    setScrollingCacheEnabled(true);
                } else if (abs2 > ((float) this.C)) {
                    this.z = true;
                }
                if (this.y && a(x3)) {
                    ViewCompat.A(this);
                }
            }
        } else if (action == 6) {
            a(motionEvent);
        }
        if (this.I == null) {
            this.I = VelocityTracker.obtain();
        }
        this.I.addMovement(motionEvent2);
        return this.y;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.viewpager.widget.ViewPager.a(int, boolean, int, boolean):void
     arg types: [int, int, int, int]
     candidates:
      androidx.viewpager.widget.ViewPager.a(int, int, int, int):void
      androidx.viewpager.widget.ViewPager.a(int, boolean, boolean, int):void
      androidx.viewpager.widget.ViewPager.a(int, boolean, int, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x008e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onLayout(boolean r19, int r20, int r21, int r22, int r23) {
        /*
            r18 = this;
            r0 = r18
            int r1 = r18.getChildCount()
            int r2 = r22 - r20
            int r3 = r23 - r21
            int r4 = r18.getPaddingLeft()
            int r5 = r18.getPaddingTop()
            int r6 = r18.getPaddingRight()
            int r7 = r18.getPaddingBottom()
            int r8 = r18.getScrollX()
            r10 = 0
            r11 = 0
        L_0x0020:
            r12 = 8
            if (r10 >= r1) goto L_0x00b6
            android.view.View r13 = r0.getChildAt(r10)
            int r14 = r13.getVisibility()
            if (r14 == r12) goto L_0x00b2
            android.view.ViewGroup$LayoutParams r12 = r13.getLayoutParams()
            androidx.viewpager.widget.ViewPager$g r12 = (androidx.viewpager.widget.ViewPager.g) r12
            boolean r14 = r12.a
            if (r14 == 0) goto L_0x00b2
            int r12 = r12.b
            r14 = r12 & 7
            r12 = r12 & 112(0x70, float:1.57E-43)
            r15 = 1
            if (r14 == r15) goto L_0x005c
            r15 = 3
            if (r14 == r15) goto L_0x0056
            r15 = 5
            if (r14 == r15) goto L_0x0049
            r14 = r4
            goto L_0x006d
        L_0x0049:
            int r14 = r2 - r6
            int r15 = r13.getMeasuredWidth()
            int r14 = r14 - r15
            int r15 = r13.getMeasuredWidth()
            int r6 = r6 + r15
            goto L_0x0068
        L_0x0056:
            int r14 = r13.getMeasuredWidth()
            int r14 = r14 + r4
            goto L_0x006d
        L_0x005c:
            int r14 = r13.getMeasuredWidth()
            int r14 = r2 - r14
            int r14 = r14 / 2
            int r14 = java.lang.Math.max(r14, r4)
        L_0x0068:
            r17 = r14
            r14 = r4
            r4 = r17
        L_0x006d:
            r15 = 16
            if (r12 == r15) goto L_0x008e
            r15 = 48
            if (r12 == r15) goto L_0x0088
            r15 = 80
            if (r12 == r15) goto L_0x007b
            r12 = r5
            goto L_0x009f
        L_0x007b:
            int r12 = r3 - r7
            int r15 = r13.getMeasuredHeight()
            int r12 = r12 - r15
            int r15 = r13.getMeasuredHeight()
            int r7 = r7 + r15
            goto L_0x009a
        L_0x0088:
            int r12 = r13.getMeasuredHeight()
            int r12 = r12 + r5
            goto L_0x009f
        L_0x008e:
            int r12 = r13.getMeasuredHeight()
            int r12 = r3 - r12
            int r12 = r12 / 2
            int r12 = java.lang.Math.max(r12, r5)
        L_0x009a:
            r17 = r12
            r12 = r5
            r5 = r17
        L_0x009f:
            int r4 = r4 + r8
            int r15 = r13.getMeasuredWidth()
            int r15 = r15 + r4
            int r16 = r13.getMeasuredHeight()
            int r9 = r16 + r5
            r13.layout(r4, r5, r15, r9)
            int r11 = r11 + 1
            r5 = r12
            r4 = r14
        L_0x00b2:
            int r10 = r10 + 1
            goto L_0x0020
        L_0x00b6:
            int r2 = r2 - r4
            int r2 = r2 - r6
            r6 = 0
        L_0x00b9:
            if (r6 >= r1) goto L_0x0108
            android.view.View r8 = r0.getChildAt(r6)
            int r9 = r8.getVisibility()
            if (r9 == r12) goto L_0x0105
            android.view.ViewGroup$LayoutParams r9 = r8.getLayoutParams()
            androidx.viewpager.widget.ViewPager$g r9 = (androidx.viewpager.widget.ViewPager.g) r9
            boolean r10 = r9.a
            if (r10 != 0) goto L_0x0105
            androidx.viewpager.widget.ViewPager$f r10 = r0.a(r8)
            if (r10 == 0) goto L_0x0105
            float r13 = (float) r2
            float r10 = r10.f363e
            float r10 = r10 * r13
            int r10 = (int) r10
            int r10 = r10 + r4
            boolean r14 = r9.d
            if (r14 == 0) goto L_0x00f8
            r14 = 0
            r9.d = r14
            float r9 = r9.c
            float r13 = r13 * r9
            int r9 = (int) r13
            r13 = 1073741824(0x40000000, float:2.0)
            int r9 = android.view.View.MeasureSpec.makeMeasureSpec(r9, r13)
            int r14 = r3 - r5
            int r14 = r14 - r7
            int r13 = android.view.View.MeasureSpec.makeMeasureSpec(r14, r13)
            r8.measure(r9, r13)
        L_0x00f8:
            int r9 = r8.getMeasuredWidth()
            int r9 = r9 + r10
            int r13 = r8.getMeasuredHeight()
            int r13 = r13 + r5
            r8.layout(r10, r5, r9, r13)
        L_0x0105:
            int r6 = r6 + 1
            goto L_0x00b9
        L_0x0108:
            r0.f358p = r5
            int r3 = r3 - r7
            r0.f359q = r3
            r0.R = r11
            boolean r1 = r0.P
            if (r1 == 0) goto L_0x011a
            int r1 = r0.g
            r2 = 0
            r0.a(r1, r2, r2, r2)
            goto L_0x011b
        L_0x011a:
            r2 = 0
        L_0x011b:
            r0.P = r2
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.viewpager.widget.ViewPager.onLayout(boolean, int, int, int, int):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0093  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00a2  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00a8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r14, int r15) {
        /*
            r13 = this;
            r0 = 0
            int r14 = android.view.ViewGroup.getDefaultSize(r0, r14)
            int r15 = android.view.ViewGroup.getDefaultSize(r0, r15)
            r13.setMeasuredDimension(r14, r15)
            int r14 = r13.getMeasuredWidth()
            int r15 = r14 / 10
            int r1 = r13.A
            int r15 = java.lang.Math.min(r15, r1)
            r13.B = r15
            int r15 = r13.getPaddingLeft()
            int r14 = r14 - r15
            int r15 = r13.getPaddingRight()
            int r14 = r14 - r15
            int r15 = r13.getMeasuredHeight()
            int r1 = r13.getPaddingTop()
            int r15 = r15 - r1
            int r1 = r13.getPaddingBottom()
            int r15 = r15 - r1
            int r1 = r13.getChildCount()
            r2 = 0
        L_0x0037:
            r3 = 8
            r4 = 1
            r5 = 1073741824(0x40000000, float:2.0)
            if (r2 >= r1) goto L_0x00b2
            android.view.View r6 = r13.getChildAt(r2)
            int r7 = r6.getVisibility()
            if (r7 == r3) goto L_0x00af
            android.view.ViewGroup$LayoutParams r3 = r6.getLayoutParams()
            androidx.viewpager.widget.ViewPager$g r3 = (androidx.viewpager.widget.ViewPager.g) r3
            if (r3 == 0) goto L_0x00af
            boolean r7 = r3.a
            if (r7 == 0) goto L_0x00af
            int r7 = r3.b
            r8 = r7 & 7
            r7 = r7 & 112(0x70, float:1.57E-43)
            r9 = 48
            if (r7 == r9) goto L_0x0065
            r9 = 80
            if (r7 != r9) goto L_0x0063
            goto L_0x0065
        L_0x0063:
            r7 = 0
            goto L_0x0066
        L_0x0065:
            r7 = 1
        L_0x0066:
            r9 = 3
            if (r8 == r9) goto L_0x006e
            r9 = 5
            if (r8 != r9) goto L_0x006d
            goto L_0x006e
        L_0x006d:
            r4 = 0
        L_0x006e:
            r8 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r7 == 0) goto L_0x0075
            r8 = 1073741824(0x40000000, float:2.0)
            goto L_0x007a
        L_0x0075:
            if (r4 == 0) goto L_0x007a
            r9 = 1073741824(0x40000000, float:2.0)
            goto L_0x007c
        L_0x007a:
            r9 = -2147483648(0xffffffff80000000, float:-0.0)
        L_0x007c:
            int r10 = r3.width
            r11 = -1
            r12 = -2
            if (r10 == r12) goto L_0x0089
            if (r10 == r11) goto L_0x0085
            goto L_0x0086
        L_0x0085:
            r10 = r14
        L_0x0086:
            r8 = 1073741824(0x40000000, float:2.0)
            goto L_0x008a
        L_0x0089:
            r10 = r14
        L_0x008a:
            int r3 = r3.height
            if (r3 == r12) goto L_0x0093
            if (r3 == r11) goto L_0x0091
            goto L_0x0095
        L_0x0091:
            r3 = r15
            goto L_0x0095
        L_0x0093:
            r3 = r15
            r5 = r9
        L_0x0095:
            int r8 = android.view.View.MeasureSpec.makeMeasureSpec(r10, r8)
            int r3 = android.view.View.MeasureSpec.makeMeasureSpec(r3, r5)
            r6.measure(r8, r3)
            if (r7 == 0) goto L_0x00a8
            int r3 = r6.getMeasuredHeight()
            int r15 = r15 - r3
            goto L_0x00af
        L_0x00a8:
            if (r4 == 0) goto L_0x00af
            int r3 = r6.getMeasuredWidth()
            int r14 = r14 - r3
        L_0x00af:
            int r2 = r2 + 1
            goto L_0x0037
        L_0x00b2:
            android.view.View.MeasureSpec.makeMeasureSpec(r14, r5)
            int r15 = android.view.View.MeasureSpec.makeMeasureSpec(r15, r5)
            r13.f362t = r15
            r13.u = r4
            int r15 = r13.g
            r13.e(r15)
            r13.u = r0
            int r15 = r13.getChildCount()
        L_0x00c8:
            if (r0 >= r15) goto L_0x00f2
            android.view.View r1 = r13.getChildAt(r0)
            int r2 = r1.getVisibility()
            if (r2 == r3) goto L_0x00ef
            android.view.ViewGroup$LayoutParams r2 = r1.getLayoutParams()
            androidx.viewpager.widget.ViewPager$g r2 = (androidx.viewpager.widget.ViewPager.g) r2
            if (r2 == 0) goto L_0x00e0
            boolean r4 = r2.a
            if (r4 != 0) goto L_0x00ef
        L_0x00e0:
            float r4 = (float) r14
            float r2 = r2.c
            float r4 = r4 * r2
            int r2 = (int) r4
            int r2 = android.view.View.MeasureSpec.makeMeasureSpec(r2, r5)
            int r4 = r13.f362t
            r1.measure(r2, r4)
        L_0x00ef:
            int r0 = r0 + 1
            goto L_0x00c8
        L_0x00f2:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.viewpager.widget.ViewPager.onMeasure(int, int):void");
    }

    public boolean onRequestFocusInDescendants(int i2, Rect rect) {
        int i3;
        int i4;
        f a2;
        int childCount = getChildCount();
        int i5 = -1;
        if ((i2 & 2) != 0) {
            i5 = childCount;
            i4 = 0;
            i3 = 1;
        } else {
            i4 = childCount - 1;
            i3 = -1;
        }
        while (i4 != i5) {
            View childAt = getChildAt(i4);
            if (childAt.getVisibility() == 0 && (a2 = a(childAt)) != null && a2.b == this.g && childAt.requestFocus(i2, rect)) {
                return true;
            }
            i4 += i3;
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.viewpager.widget.ViewPager.a(int, boolean, boolean, int):void
     arg types: [int, int, int, int]
     candidates:
      androidx.viewpager.widget.ViewPager.a(int, int, int, int):void
      androidx.viewpager.widget.ViewPager.a(int, boolean, int, boolean):void
      androidx.viewpager.widget.ViewPager.a(int, boolean, boolean, int):void */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof l)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        l lVar = (l) parcelable;
        super.onRestoreInstanceState(lVar.b);
        if (this.f350f != null) {
            a(lVar.d, false, true, 0);
            return;
        }
        this.h = lVar.d;
        this.f351i = lVar.f366e;
        this.f352j = lVar.f367f;
    }

    public Parcelable onSaveInstanceState() {
        l lVar = new l(super.onSaveInstanceState());
        lVar.d = this.g;
        PagerAdapter pagerAdapter = this.f350f;
        if (pagerAdapter != null) {
            if (pagerAdapter != null) {
                lVar.f366e = null;
            } else {
                throw null;
            }
        }
        return lVar;
    }

    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        if (i2 != i4) {
            int i6 = this.f356n;
            a(i2, i4, i6, i6);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.viewpager.widget.ViewPager.a(int, boolean, int, boolean):void
     arg types: [int, int, int, int]
     candidates:
      androidx.viewpager.widget.ViewPager.a(int, int, int, int):void
      androidx.viewpager.widget.ViewPager.a(int, boolean, boolean, int):void
      androidx.viewpager.widget.ViewPager.a(int, boolean, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.viewpager.widget.ViewPager.a(int, boolean, boolean, int):void
     arg types: [int, int, int, int]
     candidates:
      androidx.viewpager.widget.ViewPager.a(int, int, int, int):void
      androidx.viewpager.widget.ViewPager.a(int, boolean, int, boolean):void
      androidx.viewpager.widget.ViewPager.a(int, boolean, boolean, int):void */
    public boolean onTouchEvent(MotionEvent motionEvent) {
        PagerAdapter pagerAdapter;
        boolean z2 = false;
        if ((motionEvent.getAction() == 0 && motionEvent.getEdgeFlags() != 0) || (pagerAdapter = this.f350f) == null || pagerAdapter.a() == 0) {
            return false;
        }
        if (this.I == null) {
            this.I = VelocityTracker.obtain();
        }
        this.I.addMovement(motionEvent);
        int action = motionEvent.getAction() & 255;
        if (action == 0) {
            this.f353k.abortAnimation();
            this.w = false;
            e(this.g);
            float x2 = motionEvent.getX();
            this.F = x2;
            this.D = x2;
            float y2 = motionEvent.getY();
            this.G = y2;
            this.E = y2;
            this.H = motionEvent.getPointerId(0);
        } else if (action != 1) {
            if (action == 2) {
                if (!this.y) {
                    int findPointerIndex = motionEvent.findPointerIndex(this.H);
                    if (findPointerIndex == -1) {
                        z2 = f();
                    } else {
                        float x3 = motionEvent.getX(findPointerIndex);
                        float abs = Math.abs(x3 - this.D);
                        float y3 = motionEvent.getY(findPointerIndex);
                        float abs2 = Math.abs(y3 - this.E);
                        if (abs > ((float) this.C) && abs > abs2) {
                            this.y = true;
                            b(true);
                            float f2 = this.F;
                            this.D = x3 - f2 > 0.0f ? f2 + ((float) this.C) : f2 - ((float) this.C);
                            this.E = y3;
                            setScrollState(1);
                            setScrollingCacheEnabled(true);
                            ViewParent parent = getParent();
                            if (parent != null) {
                                parent.requestDisallowInterceptTouchEvent(true);
                            }
                        }
                    }
                }
                if (this.y) {
                    z2 = false | a(motionEvent.getX(motionEvent.findPointerIndex(this.H)));
                }
            } else if (action != 3) {
                if (action == 5) {
                    int actionIndex = motionEvent.getActionIndex();
                    this.D = motionEvent.getX(actionIndex);
                    this.H = motionEvent.getPointerId(actionIndex);
                } else if (action == 6) {
                    a(motionEvent);
                    this.D = motionEvent.getX(motionEvent.findPointerIndex(this.H));
                }
            } else if (this.y) {
                a(this.g, true, 0, false);
                z2 = f();
            }
        } else if (this.y) {
            VelocityTracker velocityTracker = this.I;
            velocityTracker.computeCurrentVelocity(AnswersRetryFilesSender.BACKOFF_MS, (float) this.K);
            int xVelocity = (int) velocityTracker.getXVelocity(this.H);
            this.w = true;
            int clientWidth = getClientWidth();
            int scrollX = getScrollX();
            f b2 = b();
            float f3 = (float) clientWidth;
            float f4 = ((float) this.f356n) / f3;
            int i2 = b2.b;
            float f5 = ((((float) scrollX) / f3) - b2.f363e) / (b2.d + f4);
            if (Math.abs((int) (motionEvent.getX(motionEvent.findPointerIndex(this.H)) - this.F)) <= this.L || Math.abs(xVelocity) <= this.J) {
                i2 += (int) (f5 + (i2 >= this.g ? 0.4f : 0.6f));
            } else if (xVelocity <= 0) {
                i2++;
            }
            if (this.c.size() > 0) {
                ArrayList<f> arrayList = this.c;
                i2 = Math.max(this.c.get(0).b, Math.min(i2, arrayList.get(arrayList.size() - 1).b));
            }
            a(i2, true, true, xVelocity);
            z2 = f();
        }
        if (z2) {
            ViewCompat.A(this);
        }
        return true;
    }

    public void removeView(View view) {
        if (this.u) {
            removeViewInLayout(view);
        } else {
            super.removeView(view);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.viewpager.widget.ViewPager.a(int, boolean, boolean, int):void
     arg types: [int, int, int, int]
     candidates:
      androidx.viewpager.widget.ViewPager.a(int, int, int, int):void
      androidx.viewpager.widget.ViewPager.a(int, boolean, int, boolean):void
      androidx.viewpager.widget.ViewPager.a(int, boolean, boolean, int):void */
    public void setAdapter(PagerAdapter pagerAdapter) {
        PagerAdapter pagerAdapter2 = this.f350f;
        if (pagerAdapter2 != null) {
            pagerAdapter2.a(null);
            if (this.f350f != null) {
                for (int i2 = 0; i2 < this.c.size(); i2++) {
                    f fVar = this.c.get(i2);
                    this.f350f.a(super, fVar.b, fVar.a);
                }
                if (this.f350f != null) {
                    this.c.clear();
                    int i3 = 0;
                    while (i3 < getChildCount()) {
                        if (!((g) getChildAt(i3).getLayoutParams()).a) {
                            removeViewAt(i3);
                            i3--;
                        }
                        i3++;
                    }
                    this.g = 0;
                    scrollTo(0, 0);
                } else {
                    throw null;
                }
            } else {
                throw null;
            }
        }
        PagerAdapter pagerAdapter3 = this.f350f;
        this.f350f = pagerAdapter;
        this.b = 0;
        if (pagerAdapter != null) {
            if (this.f355m == null) {
                this.f355m = new k();
            }
            this.f350f.a(this.f355m);
            this.w = false;
            boolean z2 = this.P;
            this.P = true;
            this.b = this.f350f.a();
            int i4 = this.h;
            if (i4 >= 0) {
                if (this.f350f != null) {
                    a(i4, false, true, 0);
                    this.h = -1;
                    this.f351i = null;
                    this.f352j = null;
                } else {
                    throw null;
                }
            } else if (!z2) {
                e(this.g);
            } else {
                requestLayout();
            }
        }
        List<i> list = this.U;
        if (list != null && !list.isEmpty()) {
            int size = this.U.size();
            for (int i5 = 0; i5 < size; i5++) {
                this.U.get(i5).a(this, pagerAdapter3, pagerAdapter);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.viewpager.widget.ViewPager.a(int, boolean, boolean):void
     arg types: [int, boolean, int]
     candidates:
      androidx.viewpager.widget.ViewPager.a(int, float, int):void
      androidx.viewpager.widget.ViewPager.a(int, boolean, boolean):void */
    public void setCurrentItem(int i2) {
        this.w = false;
        a(i2, !this.P, false);
    }

    public void setOffscreenPageLimit(int i2) {
        if (i2 < 1) {
            Log.w("ViewPager", "Requested offscreen page limit " + i2 + " too small; defaulting to " + 1);
            i2 = 1;
        }
        if (i2 != this.x) {
            this.x = i2;
            e(this.g);
        }
    }

    @Deprecated
    public void setOnPageChangeListener(j jVar) {
        this.T = jVar;
    }

    public void setPageMargin(int i2) {
        int i3 = this.f356n;
        this.f356n = i2;
        int width = getWidth();
        a(width, width, i2, i3);
        requestLayout();
    }

    public void setPageMarginDrawable(Drawable drawable) {
        this.f357o = drawable;
        if (drawable != null) {
            refreshDrawableState();
        }
        setWillNotDraw(drawable == null);
        invalidate();
    }

    public void setScrollState(int i2) {
        if (this.a0 != i2) {
            this.a0 = i2;
            j jVar = this.T;
            if (jVar != null) {
                jVar.a(i2);
            }
            List<j> list = this.S;
            if (list != null) {
                int size = list.size();
                for (int i3 = 0; i3 < size; i3++) {
                    j jVar2 = this.S.get(i3);
                    if (jVar2 != null) {
                        jVar2.a(i2);
                    }
                }
            }
        }
    }

    public boolean verifyDrawable(Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.f357o;
    }

    public static class g extends ViewGroup.LayoutParams {
        public boolean a;
        public int b;
        public float c = 0.0f;
        public boolean d;

        /* renamed from: e  reason: collision with root package name */
        public int f364e;

        /* renamed from: f  reason: collision with root package name */
        public int f365f;

        public g() {
            super(-1, -1);
        }

        public g(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, ViewPager.b0);
            this.b = obtainStyledAttributes.getInteger(0, 48);
            obtainStyledAttributes.recycle();
        }
    }

    public void a(int i2, boolean z2, boolean z3, int i3) {
        PagerAdapter pagerAdapter = this.f350f;
        boolean z4 = false;
        if (pagerAdapter == null || pagerAdapter.a() <= 0) {
            setScrollingCacheEnabled(false);
        } else if (z3 || this.g != i2 || this.c.size() == 0) {
            if (i2 < 0) {
                i2 = 0;
            } else if (i2 >= this.f350f.a()) {
                i2 = this.f350f.a() - 1;
            }
            int i4 = this.x;
            int i5 = this.g;
            if (i2 > i5 + i4 || i2 < i5 - i4) {
                for (int i6 = 0; i6 < this.c.size(); i6++) {
                    this.c.get(i6).c = true;
                }
            }
            if (this.g != i2) {
                z4 = true;
            }
            if (this.P) {
                this.g = i2;
                if (z4) {
                    b(i2);
                }
                requestLayout();
                return;
            }
            e(i2);
            a(i2, z2, i3, z4);
        } else {
            setScrollingCacheEnabled(false);
        }
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new g(getContext(), attributeSet);
    }

    public void setPageMarginDrawable(int i2) {
        setPageMarginDrawable(ContextCompat.c(getContext(), i2));
    }

    public final void b(boolean z2) {
        ViewParent parent = getParent();
        if (parent != null) {
            parent.requestDisallowInterceptTouchEvent(z2);
        }
    }

    public final f b() {
        int i2;
        int clientWidth = getClientWidth();
        float f2 = 0.0f;
        float scrollX = clientWidth > 0 ? ((float) getScrollX()) / ((float) clientWidth) : 0.0f;
        float f3 = clientWidth > 0 ? ((float) this.f356n) / ((float) clientWidth) : 0.0f;
        f fVar = null;
        float f4 = 0.0f;
        int i3 = -1;
        int i4 = 0;
        boolean z2 = true;
        while (i4 < this.c.size()) {
            f fVar2 = this.c.get(i4);
            if (!z2 && fVar2.b != (i2 = i3 + 1)) {
                fVar2 = this.d;
                fVar2.f363e = f2 + f4 + f3;
                fVar2.b = i2;
                if (this.f350f != null) {
                    fVar2.d = 1.0f;
                    i4--;
                } else {
                    throw null;
                }
            }
            f2 = fVar2.f363e;
            float f5 = fVar2.d + f2 + f3;
            if (!z2 && scrollX < f2) {
                return fVar;
            }
            if (scrollX < f5 || i4 == this.c.size() - 1) {
                return fVar2;
            }
            i3 = fVar2.b;
            f4 = fVar2.d;
            i4++;
            fVar = fVar2;
            z2 = false;
        }
        return fVar;
    }

    public class h extends AccessibilityDelegateCompat {
        public h() {
        }

        public void a(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            super.a.onInitializeAccessibilityNodeInfo(view, accessibilityNodeInfoCompat.a);
            accessibilityNodeInfoCompat.a.setClassName(ViewPager.class.getName());
            PagerAdapter pagerAdapter = ViewPager.this.f350f;
            accessibilityNodeInfoCompat.a.setScrollable(pagerAdapter != null && pagerAdapter.a() > 1);
            if (ViewPager.this.canScrollHorizontally(1)) {
                accessibilityNodeInfoCompat.a.addAction((int) CodedOutputStream.DEFAULT_BUFFER_SIZE);
            }
            if (ViewPager.this.canScrollHorizontally(-1)) {
                accessibilityNodeInfoCompat.a.addAction(8192);
            }
        }

        public void b(View view, AccessibilityEvent accessibilityEvent) {
            PagerAdapter pagerAdapter;
            super.a.onInitializeAccessibilityEvent(view, accessibilityEvent);
            accessibilityEvent.setClassName(ViewPager.class.getName());
            PagerAdapter pagerAdapter2 = ViewPager.this.f350f;
            boolean z = true;
            if (pagerAdapter2 == null || pagerAdapter2.a() <= 1) {
                z = false;
            }
            accessibilityEvent.setScrollable(z);
            if (accessibilityEvent.getEventType() == 4096 && (pagerAdapter = ViewPager.this.f350f) != null) {
                accessibilityEvent.setItemCount(pagerAdapter.a());
                accessibilityEvent.setFromIndex(ViewPager.this.g);
                accessibilityEvent.setToIndex(ViewPager.this.g);
            }
        }

        public boolean a(View view, int i2, Bundle bundle) {
            if (super.a(view, i2, bundle)) {
                return true;
            }
            if (i2 != 4096) {
                if (i2 != 8192 || !ViewPager.this.canScrollHorizontally(-1)) {
                    return false;
                }
                ViewPager viewPager = ViewPager.this;
                viewPager.setCurrentItem(viewPager.g - 1);
                return true;
            } else if (!ViewPager.this.canScrollHorizontally(1)) {
                return false;
            } else {
                ViewPager viewPager2 = ViewPager.this;
                viewPager2.setCurrentItem(viewPager2.g + 1);
                return true;
            }
        }
    }

    public ViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        c();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.viewpager.widget.ViewPager.a(int, boolean, boolean, int):void
     arg types: [int, int, int, int]
     candidates:
      androidx.viewpager.widget.ViewPager.a(int, int, int, int):void
      androidx.viewpager.widget.ViewPager.a(int, boolean, int, boolean):void
      androidx.viewpager.widget.ViewPager.a(int, boolean, boolean, int):void */
    public boolean d() {
        int i2 = this.g;
        if (i2 <= 0) {
            return false;
        }
        this.w = false;
        a(i2 - 1, true, false, 0);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public final void a(int i2, boolean z2, int i3, boolean z3) {
        int i4;
        int i5;
        f c2 = c(i2);
        int max = c2 != null ? (int) (Math.max(this.f360r, Math.min(c2.f363e, this.f361s)) * ((float) getClientWidth())) : 0;
        if (z2) {
            if (getChildCount() == 0) {
                setScrollingCacheEnabled(false);
            } else {
                Scroller scroller = this.f353k;
                if (scroller != null && !scroller.isFinished()) {
                    i4 = this.f354l ? this.f353k.getCurrX() : this.f353k.getStartX();
                    this.f353k.abortAnimation();
                    setScrollingCacheEnabled(false);
                } else {
                    i4 = getScrollX();
                }
                int i6 = i4;
                int scrollY = getScrollY();
                int i7 = max - i6;
                int i8 = 0 - scrollY;
                if (i7 == 0 && i8 == 0) {
                    a(false);
                    e(this.g);
                    setScrollState(0);
                } else {
                    setScrollingCacheEnabled(true);
                    setScrollState(2);
                    int clientWidth = getClientWidth();
                    int i9 = clientWidth / 2;
                    float f2 = (float) clientWidth;
                    float f3 = (float) i9;
                    float sin = (((float) Math.sin((double) ((Math.min(1.0f, (((float) Math.abs(i7)) * 1.0f) / f2) - 0.5f) * 0.47123894f))) * f3) + f3;
                    int abs = Math.abs(i3);
                    if (abs > 0) {
                        i5 = Math.round(Math.abs(sin / ((float) abs)) * 1000.0f) * 4;
                    } else if (this.f350f != null) {
                        i5 = (int) (((((float) Math.abs(i7)) / ((f2 * 1.0f) + ((float) this.f356n))) + 1.0f) * 100.0f);
                    } else {
                        throw null;
                    }
                    int min = Math.min(i5, 600);
                    this.f354l = false;
                    this.f353k.startScroll(i6, scrollY, i7, i8, min);
                    ViewCompat.A(this);
                }
            }
            if (z3) {
                b(i2);
                return;
            }
            return;
        }
        if (z3) {
            b(i2);
        }
        a(false);
        scrollTo(max, 0);
        d(max);
    }

    public f c(int i2) {
        for (int i3 = 0; i3 < this.c.size(); i3++) {
            f fVar = this.c.get(i3);
            if (fVar.b == i2) {
                return fVar;
            }
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, androidx.viewpager.widget.ViewPager, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [com.google.android.material.textfield.TextInputEditText, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
     arg types: [j.e.a.InitialValueObservable, e.c.d.a.ViewAction<java.lang.CharSequence>]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(java.lang.Object, java.lang.String):T
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b */
    public f a(int i2, int i3) {
        int i4 = i2;
        int i5 = i3;
        f fVar = new f();
        fVar.b = i4;
        LoginPagerAdapter1 loginPagerAdapter1 = (LoginPagerAdapter1) this.f350f;
        if (loginPagerAdapter1 != null) {
            LoginPagerAdapter loginPagerAdapter = LoginPagerAdapter.values()[i4];
            View inflate = LayoutInflater.from(loginPagerAdapter1.f583j).inflate(loginPagerAdapter.layoutResId, (ViewGroup) super, false);
            addView(inflate);
            int ordinal = loginPagerAdapter.ordinal();
            if (ordinal == 0) {
                Intrinsics.a((Object) inflate, "layout");
                TextInputEditText textInputEditText = (TextInputEditText) inflate.findViewById(e.a.a.d.frag_auth_login_et_login);
                CompositeDisposable compositeDisposable = loginPagerAdapter1.h;
                if (compositeDisposable != null) {
                    compositeDisposable.f();
                }
                CompositeDisposable compositeDisposable2 = new CompositeDisposable();
                loginPagerAdapter1.h = compositeDisposable2;
                TextInputEditText textInputEditText2 = (TextInputEditText) inflate.findViewById(e.a.a.d.frag_auth_login_et_login);
                compositeDisposable2.a(Collections.a((Observable) outline.a(textInputEditText2, "layout.frag_auth_login_et_login", textInputEditText2, "RxTextView.textChanges(this)"), (ViewAction) loginPagerAdapter1.b), loginPagerAdapter1.b.a(new LoginPagerAdapter4(loginPagerAdapter1, inflate)), loginPagerAdapter1.d.a(new defpackage.g(1, textInputEditText)), loginPagerAdapter1.f581f.a(new LoginPagerAdapter5(textInputEditText, loginPagerAdapter1, inflate)));
            } else if (ordinal == 1) {
                CompositeDisposable compositeDisposable3 = loginPagerAdapter1.f582i;
                if (compositeDisposable3 != null) {
                    compositeDisposable3.f();
                }
                loginPagerAdapter1.f582i = new CompositeDisposable();
                Intrinsics.a((Object) inflate, "layout");
                TextInputEditText textInputEditText3 = (TextInputEditText) inflate.findViewById(e.a.a.d.frag_auth_snils_et_login);
                String string = textInputEditText3.getContext().getString(e.a.a.f.frag_auth_snils_pattern);
                Intrinsics.a((Object) string, "context.getString(R.stri….frag_auth_snils_pattern)");
                Intrinsics.a((Object) textInputEditText3, "this");
                MaskedTextChangedListener maskedTextChangedListener = new MaskedTextChangedListener(string, textInputEditText3, new LoginPagerAdapter0(loginPagerAdapter1, inflate));
                textInputEditText3.addTextChangedListener(maskedTextChangedListener);
                textInputEditText3.setOnFocusChangeListener(maskedTextChangedListener);
                CompositeDisposable compositeDisposable4 = loginPagerAdapter1.f582i;
                if (compositeDisposable4 != null) {
                    compositeDisposable4.a(loginPagerAdapter1.f580e.a(new defpackage.g(0, textInputEditText3)), loginPagerAdapter1.c.a(new LoginPagerAdapter2(loginPagerAdapter1, inflate)), loginPagerAdapter1.g.a(new LoginPagerAdapter3(textInputEditText3, loginPagerAdapter1, inflate)));
                }
            }
            Intrinsics.a((Object) inflate, "layout");
            fVar.a = inflate;
            if (this.f350f != null) {
                fVar.d = 1.0f;
                if (i5 < 0 || i5 >= this.c.size()) {
                    this.c.add(fVar);
                } else {
                    this.c.add(i5, fVar);
                }
                return fVar;
            }
            throw null;
        }
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.viewpager.widget.ViewPager.a(int, boolean, boolean, int):void
     arg types: [int, int, int, int]
     candidates:
      androidx.viewpager.widget.ViewPager.a(int, int, int, int):void
      androidx.viewpager.widget.ViewPager.a(int, boolean, int, boolean):void
      androidx.viewpager.widget.ViewPager.a(int, boolean, boolean, int):void */
    public void a() {
        int a2 = this.f350f.a();
        this.b = a2;
        boolean z2 = this.c.size() < (this.x * 2) + 1 && this.c.size() < a2;
        int i2 = this.g;
        int i3 = 0;
        while (i3 < this.c.size()) {
            PagerAdapter pagerAdapter = this.f350f;
            Object obj = this.c.get(i3).a;
            if (pagerAdapter != null) {
                i3++;
            } else {
                throw null;
            }
        }
        java.util.Collections.sort(this.c, c0);
        if (z2) {
            int childCount = getChildCount();
            for (int i4 = 0; i4 < childCount; i4++) {
                g gVar = (g) getChildAt(i4).getLayoutParams();
                if (!gVar.a) {
                    gVar.c = 0.0f;
                }
            }
            a(i2, false, true, 0);
            requestLayout();
        }
    }

    public f a(View view) {
        int i2 = 0;
        while (i2 < this.c.size()) {
            f fVar = this.c.get(i2);
            PagerAdapter pagerAdapter = this.f350f;
            Object obj = fVar.a;
            if (((LoginPagerAdapter1) pagerAdapter) == null) {
                throw null;
            } else if (view == null) {
                Intrinsics.a("view");
                throw null;
            } else if (obj != null) {
                if (view == obj) {
                    return fVar;
                }
                i2++;
            } else {
                Intrinsics.a("object");
                throw null;
            }
        }
        return null;
    }

    public final void a(int i2, int i3, int i4, int i5) {
        if (i3 <= 0 || this.c.isEmpty()) {
            f c2 = c(this.g);
            int min = (int) ((c2 != null ? Math.min(c2.f363e, this.f361s) : 0.0f) * ((float) ((i2 - getPaddingLeft()) - getPaddingRight())));
            if (min != getScrollX()) {
                a(false);
                scrollTo(min, getScrollY());
            }
        } else if (!this.f353k.isFinished()) {
            this.f353k.setFinalX(getCurrentItem() * getClientWidth());
        } else {
            scrollTo((int) ((((float) getScrollX()) / ((float) (((i3 - getPaddingLeft()) - getPaddingRight()) + i5))) * ((float) (((i2 - getPaddingLeft()) - getPaddingRight()) + i4))), getScrollY());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.viewpager.widget.ViewPager.a(int, boolean, boolean, int):void
     arg types: [int, int, int, int]
     candidates:
      androidx.viewpager.widget.ViewPager.a(int, int, int, int):void
      androidx.viewpager.widget.ViewPager.a(int, boolean, int, boolean):void
      androidx.viewpager.widget.ViewPager.a(int, boolean, boolean, int):void */
    public boolean e() {
        PagerAdapter pagerAdapter = this.f350f;
        if (pagerAdapter == null || this.g >= pagerAdapter.a() - 1) {
            return false;
        }
        this.w = false;
        a(this.g + 1, true, false, 0);
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0064  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(int r13, float r14, int r15) {
        /*
            r12 = this;
            int r0 = r12.R
            r1 = 0
            r2 = 1
            if (r0 <= 0) goto L_0x006b
            int r0 = r12.getScrollX()
            int r3 = r12.getPaddingLeft()
            int r4 = r12.getPaddingRight()
            int r5 = r12.getWidth()
            int r6 = r12.getChildCount()
            r7 = 0
        L_0x001b:
            if (r7 >= r6) goto L_0x006b
            android.view.View r8 = r12.getChildAt(r7)
            android.view.ViewGroup$LayoutParams r9 = r8.getLayoutParams()
            androidx.viewpager.widget.ViewPager$g r9 = (androidx.viewpager.widget.ViewPager.g) r9
            boolean r10 = r9.a
            if (r10 != 0) goto L_0x002c
            goto L_0x0068
        L_0x002c:
            int r9 = r9.b
            r9 = r9 & 7
            if (r9 == r2) goto L_0x004d
            r10 = 3
            if (r9 == r10) goto L_0x0047
            r10 = 5
            if (r9 == r10) goto L_0x003a
            r9 = r3
            goto L_0x005c
        L_0x003a:
            int r9 = r5 - r4
            int r10 = r8.getMeasuredWidth()
            int r9 = r9 - r10
            int r10 = r8.getMeasuredWidth()
            int r4 = r4 + r10
            goto L_0x0059
        L_0x0047:
            int r9 = r8.getWidth()
            int r9 = r9 + r3
            goto L_0x005c
        L_0x004d:
            int r9 = r8.getMeasuredWidth()
            int r9 = r5 - r9
            int r9 = r9 / 2
            int r9 = java.lang.Math.max(r9, r3)
        L_0x0059:
            r11 = r9
            r9 = r3
            r3 = r11
        L_0x005c:
            int r3 = r3 + r0
            int r10 = r8.getLeft()
            int r3 = r3 - r10
            if (r3 == 0) goto L_0x0067
            r8.offsetLeftAndRight(r3)
        L_0x0067:
            r3 = r9
        L_0x0068:
            int r7 = r7 + 1
            goto L_0x001b
        L_0x006b:
            androidx.viewpager.widget.ViewPager$j r0 = r12.T
            if (r0 == 0) goto L_0x0072
            r0.a(r13, r14, r15)
        L_0x0072:
            java.util.List<androidx.viewpager.widget.ViewPager$j> r0 = r12.S
            if (r0 == 0) goto L_0x008c
            int r0 = r0.size()
        L_0x007a:
            if (r1 >= r0) goto L_0x008c
            java.util.List<androidx.viewpager.widget.ViewPager$j> r3 = r12.S
            java.lang.Object r3 = r3.get(r1)
            androidx.viewpager.widget.ViewPager$j r3 = (androidx.viewpager.widget.ViewPager.j) r3
            if (r3 == 0) goto L_0x0089
            r3.a(r13, r14, r15)
        L_0x0089:
            int r1 = r1 + 1
            goto L_0x007a
        L_0x008c:
            r12.Q = r2
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.viewpager.widget.ViewPager.a(int, float, int):void");
    }

    public final void a(boolean z2) {
        boolean z3 = this.a0 == 2;
        if (z3) {
            setScrollingCacheEnabled(false);
            if (!this.f353k.isFinished()) {
                this.f353k.abortAnimation();
                int scrollX = getScrollX();
                int scrollY = getScrollY();
                int currX = this.f353k.getCurrX();
                int currY = this.f353k.getCurrY();
                if (!(scrollX == currX && scrollY == currY)) {
                    scrollTo(currX, currY);
                    if (currX != scrollX) {
                        d(currX);
                    }
                }
            }
        }
        this.w = false;
        for (int i2 = 0; i2 < this.c.size(); i2++) {
            f fVar = this.c.get(i2);
            if (fVar.c) {
                fVar.c = false;
                z3 = true;
            }
        }
        if (!z3) {
            return;
        }
        if (z2) {
            ViewCompat.a(this, this.W);
        } else {
            this.W.run();
        }
    }

    public final boolean a(float f2) {
        boolean z2;
        boolean z3;
        float f3 = this.D - f2;
        this.D = f2;
        float scrollX = ((float) getScrollX()) + f3;
        float clientWidth = (float) getClientWidth();
        float f4 = this.f360r * clientWidth;
        float f5 = this.f361s * clientWidth;
        boolean z4 = false;
        f fVar = this.c.get(0);
        ArrayList<f> arrayList = this.c;
        f fVar2 = arrayList.get(arrayList.size() - 1);
        if (fVar.b != 0) {
            f4 = fVar.f363e * clientWidth;
            z2 = false;
        } else {
            z2 = true;
        }
        if (fVar2.b != this.f350f.a() - 1) {
            f5 = fVar2.f363e * clientWidth;
            z3 = false;
        } else {
            z3 = true;
        }
        if (scrollX < f4) {
            if (z2) {
                this.N.onPull(Math.abs(f4 - scrollX) / clientWidth);
                z4 = true;
            }
            scrollX = f4;
        } else if (scrollX > f5) {
            if (z3) {
                this.O.onPull(Math.abs(scrollX - f5) / clientWidth);
                z4 = true;
            }
            scrollX = f5;
        }
        int i2 = (int) scrollX;
        this.D = (scrollX - ((float) i2)) + this.D;
        scrollTo(i2, getScrollY());
        d(i2);
        return z4;
    }

    public final void a(MotionEvent motionEvent) {
        int actionIndex = motionEvent.getActionIndex();
        if (motionEvent.getPointerId(actionIndex) == this.H) {
            int i2 = actionIndex == 0 ? 1 : 0;
            this.D = motionEvent.getX(i2);
            this.H = motionEvent.getPointerId(i2);
            VelocityTracker velocityTracker = this.I;
            if (velocityTracker != null) {
                velocityTracker.clear();
            }
        }
    }

    public boolean a(View view, boolean z2, int i2, int i3, int i4) {
        int i5;
        View view2 = view;
        if (view2 instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view2;
            int scrollX = view.getScrollX();
            int scrollY = view.getScrollY();
            for (int childCount = super.getChildCount() - 1; childCount >= 0; childCount--) {
                View childAt = super.getChildAt(childCount);
                int i6 = i3 + scrollX;
                if (i6 >= childAt.getLeft() && i6 < childAt.getRight() && (i5 = i4 + scrollY) >= childAt.getTop() && i5 < childAt.getBottom()) {
                    if (a(childAt, true, i2, i6 - childAt.getLeft(), i5 - childAt.getTop())) {
                        return true;
                    }
                }
            }
        }
        if (!z2 || !view.canScrollHorizontally(-i2)) {
            return false;
        }
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:41:0x00ca  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(int r7) {
        /*
            r6 = this;
            android.view.View r0 = r6.findFocus()
            r1 = 1
            r2 = 0
            r3 = 0
            if (r0 != r6) goto L_0x000a
            goto L_0x0063
        L_0x000a:
            if (r0 == 0) goto L_0x0064
            android.view.ViewParent r4 = r0.getParent()
        L_0x0010:
            boolean r5 = r4 instanceof android.view.ViewGroup
            if (r5 == 0) goto L_0x001d
            if (r4 != r6) goto L_0x0018
            r4 = 1
            goto L_0x001e
        L_0x0018:
            android.view.ViewParent r4 = r4.getParent()
            goto L_0x0010
        L_0x001d:
            r4 = 0
        L_0x001e:
            if (r4 != 0) goto L_0x0064
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.Class r5 = r0.getClass()
            java.lang.String r5 = r5.getSimpleName()
            r4.append(r5)
            android.view.ViewParent r0 = r0.getParent()
        L_0x0034:
            boolean r5 = r0 instanceof android.view.ViewGroup
            if (r5 == 0) goto L_0x004d
            java.lang.String r5 = " => "
            r4.append(r5)
            java.lang.Class r5 = r0.getClass()
            java.lang.String r5 = r5.getSimpleName()
            r4.append(r5)
            android.view.ViewParent r0 = r0.getParent()
            goto L_0x0034
        L_0x004d:
            java.lang.String r0 = "arrowScroll tried to find focus based on non-child current focused view "
            java.lang.StringBuilder r0 = j.a.a.a.outline.a(r0)
            java.lang.String r4 = r4.toString()
            r0.append(r4)
            java.lang.String r0 = r0.toString()
            java.lang.String r4 = "ViewPager"
            android.util.Log.e(r4, r0)
        L_0x0063:
            r0 = r3
        L_0x0064:
            android.view.FocusFinder r3 = android.view.FocusFinder.getInstance()
            android.view.View r3 = r3.findNextFocus(r6, r0, r7)
            r4 = 66
            r5 = 17
            if (r3 == 0) goto L_0x00b5
            if (r3 == r0) goto L_0x00b5
            if (r7 != r5) goto L_0x0095
            android.graphics.Rect r1 = r6.f349e
            android.graphics.Rect r1 = r6.a(r1, r3)
            int r1 = r1.left
            android.graphics.Rect r2 = r6.f349e
            android.graphics.Rect r2 = r6.a(r2, r0)
            int r2 = r2.left
            if (r0 == 0) goto L_0x008f
            if (r1 < r2) goto L_0x008f
            boolean r0 = r6.d()
            goto L_0x0093
        L_0x008f:
            boolean r0 = r3.requestFocus()
        L_0x0093:
            r2 = r0
            goto L_0x00c8
        L_0x0095:
            if (r7 != r4) goto L_0x00c8
            android.graphics.Rect r1 = r6.f349e
            android.graphics.Rect r1 = r6.a(r1, r3)
            int r1 = r1.left
            android.graphics.Rect r2 = r6.f349e
            android.graphics.Rect r2 = r6.a(r2, r0)
            int r2 = r2.left
            if (r0 == 0) goto L_0x00b0
            if (r1 > r2) goto L_0x00b0
            boolean r0 = r6.e()
            goto L_0x0093
        L_0x00b0:
            boolean r0 = r3.requestFocus()
            goto L_0x0093
        L_0x00b5:
            if (r7 == r5) goto L_0x00c4
            if (r7 != r1) goto L_0x00ba
            goto L_0x00c4
        L_0x00ba:
            if (r7 == r4) goto L_0x00bf
            r0 = 2
            if (r7 != r0) goto L_0x00c8
        L_0x00bf:
            boolean r2 = r6.e()
            goto L_0x00c8
        L_0x00c4:
            boolean r2 = r6.d()
        L_0x00c8:
            if (r2 == 0) goto L_0x00d1
            int r7 = android.view.SoundEffectConstants.getContantForFocusDirection(r7)
            r6.playSoundEffect(r7)
        L_0x00d1:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.viewpager.widget.ViewPager.a(int):boolean");
    }

    public final Rect a(Rect rect, View view) {
        if (rect == null) {
            rect = new Rect();
        }
        if (view == null) {
            rect.set(0, 0, 0, 0);
            return rect;
        }
        rect.left = view.getLeft();
        rect.right = view.getRight();
        rect.top = view.getTop();
        rect.bottom = view.getBottom();
        ViewParent parent = view.getParent();
        while ((parent instanceof ViewGroup) && parent != this) {
            ViewGroup viewGroup = (ViewGroup) parent;
            rect.left = super.getLeft() + rect.left;
            rect.right = super.getRight() + rect.right;
            rect.top = super.getTop() + rect.top;
            rect.bottom = super.getBottom() + rect.bottom;
            parent = super.getParent();
        }
        return rect;
    }
}
