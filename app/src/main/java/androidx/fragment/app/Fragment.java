package androidx.fragment.app;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ComponentCallbacks;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Looper;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import i.b.k.ResourcesFlusher;
import i.h.d.SharedElementCallback;
import i.l.a.FragmentActivity;
import i.l.a.FragmentFactory;
import i.l.a.FragmentHostCallback;
import i.l.a.FragmentManager;
import i.l.a.FragmentManagerImpl;
import i.l.a.FragmentManagerViewModel;
import i.l.a.FragmentViewLifecycleOwner;
import i.o.Lifecycle;
import i.o.LifecycleEventObserver;
import i.o.LifecycleOwner;
import i.o.LifecycleRegistry;
import i.o.MutableLiveData;
import i.o.ViewModelStore;
import i.o.ViewModelStoreOwner;
import i.o.k;
import i.t.SavedStateRegistry;
import i.t.SavedStateRegistryController;
import i.t.SavedStateRegistryOwner;
import j.a.a.a.outline;
import java.lang.reflect.InvocationTargetException;
import java.util.UUID;

public class Fragment implements ComponentCallbacks, View.OnCreateContextMenuListener, LifecycleOwner, ViewModelStoreOwner, SavedStateRegistryOwner {
    public static final Object W = new Object();
    public boolean A;
    public boolean B;
    public boolean C;
    public boolean D;
    public boolean E = true;
    public boolean F;
    public ViewGroup G;
    public View H;
    public View I;
    public boolean J;
    public boolean K = true;
    public b L;
    public boolean M;
    public boolean N;
    public float O;
    public LayoutInflater P;
    public boolean Q;
    public Lifecycle.b R = Lifecycle.b.RESUMED;
    public LifecycleRegistry S;
    public FragmentViewLifecycleOwner T;
    public MutableLiveData<k> U = new MutableLiveData<>();
    public SavedStateRegistryController V;
    public int b = 0;
    public Bundle c;
    public SparseArray<Parcelable> d;

    /* renamed from: e  reason: collision with root package name */
    public Boolean f220e;

    /* renamed from: f  reason: collision with root package name */
    public String f221f = UUID.randomUUID().toString();
    public Bundle g;
    public Fragment h;

    /* renamed from: i  reason: collision with root package name */
    public String f222i = null;

    /* renamed from: j  reason: collision with root package name */
    public int f223j;

    /* renamed from: k  reason: collision with root package name */
    public Boolean f224k = null;

    /* renamed from: l  reason: collision with root package name */
    public boolean f225l;

    /* renamed from: m  reason: collision with root package name */
    public boolean f226m;

    /* renamed from: n  reason: collision with root package name */
    public boolean f227n;

    /* renamed from: o  reason: collision with root package name */
    public boolean f228o;

    /* renamed from: p  reason: collision with root package name */
    public boolean f229p;

    /* renamed from: q  reason: collision with root package name */
    public boolean f230q;

    /* renamed from: r  reason: collision with root package name */
    public int f231r;

    /* renamed from: s  reason: collision with root package name */
    public FragmentManagerImpl f232s;

    /* renamed from: t  reason: collision with root package name */
    public FragmentHostCallback f233t;
    public FragmentManagerImpl u = new FragmentManagerImpl();
    public Fragment v;
    public int w;
    public int x;
    public String y;
    public boolean z;

    public static class InstantiationException extends RuntimeException {
        public InstantiationException(String str, Exception exc) {
            super(str, exc);
        }
    }

    public class a implements Runnable {
        public a() {
        }

        public void run() {
            Fragment.this.j();
        }
    }

    public static class b {
        public View a;
        public Animator b;
        public int c;
        public int d;

        /* renamed from: e  reason: collision with root package name */
        public int f234e;

        /* renamed from: f  reason: collision with root package name */
        public int f235f;
        public Object g = null;
        public Object h;

        /* renamed from: i  reason: collision with root package name */
        public Object f236i;

        /* renamed from: j  reason: collision with root package name */
        public Object f237j;

        /* renamed from: k  reason: collision with root package name */
        public Object f238k;

        /* renamed from: l  reason: collision with root package name */
        public Object f239l;

        /* renamed from: m  reason: collision with root package name */
        public Boolean f240m;

        /* renamed from: n  reason: collision with root package name */
        public Boolean f241n;

        /* renamed from: o  reason: collision with root package name */
        public SharedElementCallback f242o;

        /* renamed from: p  reason: collision with root package name */
        public SharedElementCallback f243p;

        /* renamed from: q  reason: collision with root package name */
        public boolean f244q;

        /* renamed from: r  reason: collision with root package name */
        public c f245r;

        /* renamed from: s  reason: collision with root package name */
        public boolean f246s;

        public b() {
            Object obj = Fragment.W;
            this.h = obj;
            this.f236i = null;
            this.f237j = obj;
            this.f238k = null;
            this.f239l = obj;
            this.f242o = null;
            this.f243p = null;
        }
    }

    public interface c {
    }

    public Fragment() {
        y();
    }

    public final boolean A() {
        return this.f231r > 0;
    }

    public void B() {
        this.F = true;
    }

    public void C() {
        this.F = true;
    }

    public void D() {
        this.F = true;
    }

    public void E() {
        this.F = true;
    }

    public void F() {
        this.F = true;
    }

    public void G() {
        this.F = true;
    }

    public void H() {
        this.F = true;
    }

    public final FragmentActivity I() {
        FragmentActivity l2 = l();
        if (l2 != null) {
            return l2;
        }
        throw new IllegalStateException(outline.a("Fragment ", this, " not attached to an activity."));
    }

    public final Context J() {
        Context p2 = p();
        if (p2 != null) {
            return p2;
        }
        throw new IllegalStateException(outline.a("Fragment ", this, " not attached to a context."));
    }

    public final FragmentManager K() {
        FragmentManagerImpl fragmentManagerImpl = this.f232s;
        if (fragmentManagerImpl != null) {
            return fragmentManagerImpl;
        }
        throw new IllegalStateException(outline.a("Fragment ", this, " not associated with a fragment manager."));
    }

    public final View L() {
        View view = this.H;
        if (view != null) {
            return view;
        }
        throw new IllegalStateException(outline.a("Fragment ", this, " did not return a View from onCreateView() or this was called before onCreateView()."));
    }

    public void M() {
        FragmentManagerImpl fragmentManagerImpl = this.f232s;
        if (fragmentManagerImpl == null || fragmentManagerImpl.f1305q == null) {
            k().f244q = false;
        } else if (Looper.myLooper() != this.f232s.f1305q.d.getLooper()) {
            this.f232s.f1305q.d.postAtFrontOfQueue(new a());
        } else {
            j();
        }
    }

    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return null;
    }

    public Lifecycle a() {
        return this.S;
    }

    public void a(View view, Bundle bundle) {
    }

    public void b(Bundle bundle) {
        Parcelable parcelable;
        boolean z2 = true;
        this.F = true;
        if (!(bundle == null || (parcelable = bundle.getParcelable("android:support:fragments")) == null)) {
            this.u.a(parcelable);
            this.u.f();
        }
        if (this.u.f1304p < 1) {
            z2 = false;
        }
        if (!z2) {
            this.u.f();
        }
    }

    public LayoutInflater c(Bundle bundle) {
        FragmentHostCallback fragmentHostCallback = this.f233t;
        if (fragmentHostCallback != null) {
            FragmentActivity.a aVar = (FragmentActivity.a) fragmentHostCallback;
            LayoutInflater cloneInContext = FragmentActivity.this.getLayoutInflater().cloneInContext(FragmentActivity.this);
            FragmentManagerImpl fragmentManagerImpl = this.u;
            if (fragmentManagerImpl != null) {
                cloneInContext.setFactory2(fragmentManagerImpl);
                return cloneInContext;
            }
            throw null;
        }
        throw new IllegalStateException("onGetLayoutInflater() cannot be executed until the Fragment is attached to the FragmentManager.");
    }

    public final SavedStateRegistry d() {
        return this.V.b;
    }

    public void d(Bundle bundle) {
    }

    public LayoutInflater e(Bundle bundle) {
        LayoutInflater c2 = c(bundle);
        this.P = c2;
        return c2;
    }

    public final boolean equals(Object obj) {
        return super.equals(obj);
    }

    public ViewModelStore f() {
        FragmentManagerImpl fragmentManagerImpl = this.f232s;
        if (fragmentManagerImpl != null) {
            FragmentManagerViewModel fragmentManagerViewModel = fragmentManagerImpl.F;
            ViewModelStore viewModelStore = fragmentManagerViewModel.d.get(this.f221f);
            if (viewModelStore != null) {
                return viewModelStore;
            }
            ViewModelStore viewModelStore2 = new ViewModelStore();
            fragmentManagerViewModel.d.put(this.f221f, viewModelStore2);
            return viewModelStore2;
        }
        throw new IllegalStateException("Can't access ViewModels from detached fragment");
    }

    public final int hashCode() {
        return super.hashCode();
    }

    public void j() {
        b bVar = this.L;
        Object obj = null;
        if (bVar != null) {
            bVar.f244q = false;
            Object obj2 = bVar.f245r;
            bVar.f245r = null;
            obj = obj2;
        }
        if (obj != null) {
            FragmentManagerImpl.j jVar = (FragmentManagerImpl.j) obj;
            int i2 = jVar.c - 1;
            jVar.c = i2;
            if (i2 == 0) {
                jVar.b.f1275s.p();
            }
        }
    }

    public final b k() {
        if (this.L == null) {
            this.L = new b();
        }
        return this.L;
    }

    public final FragmentActivity l() {
        FragmentHostCallback fragmentHostCallback = this.f233t;
        if (fragmentHostCallback == null) {
            return null;
        }
        return (FragmentActivity) fragmentHostCallback.b;
    }

    public View m() {
        b bVar = this.L;
        if (bVar == null) {
            return null;
        }
        return bVar.a;
    }

    public Animator n() {
        b bVar = this.L;
        if (bVar == null) {
            return null;
        }
        return bVar.b;
    }

    public final FragmentManager o() {
        if (this.f233t != null) {
            return this.u;
        }
        throw new IllegalStateException(outline.a("Fragment ", this, " has not been attached yet."));
    }

    public void onConfigurationChanged(Configuration configuration) {
        this.F = true;
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        I().onCreateContextMenu(contextMenu, view, contextMenuInfo);
    }

    public void onLowMemory() {
        this.F = true;
    }

    public Context p() {
        FragmentHostCallback fragmentHostCallback = this.f233t;
        if (fragmentHostCallback == null) {
            return null;
        }
        return fragmentHostCallback.c;
    }

    public Object q() {
        b bVar = this.L;
        if (bVar == null) {
            return null;
        }
        return bVar.g;
    }

    public Object r() {
        b bVar = this.L;
        if (bVar == null) {
            return null;
        }
        return bVar.f236i;
    }

    public int s() {
        b bVar = this.L;
        if (bVar == null) {
            return 0;
        }
        return bVar.d;
    }

    public int t() {
        b bVar = this.L;
        if (bVar == null) {
            return 0;
        }
        return bVar.f234e;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
     arg types: [androidx.fragment.app.Fragment, java.lang.StringBuilder]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
      i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
      i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
      i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
      i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
      i.b.k.ResourcesFlusher.a(int, int):boolean
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
      i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
      i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void */
    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        ResourcesFlusher.a((Object) this, sb);
        sb.append(" (");
        sb.append(this.f221f);
        sb.append(")");
        if (this.w != 0) {
            sb.append(" id=0x");
            sb.append(Integer.toHexString(this.w));
        }
        if (this.y != null) {
            sb.append(" ");
            sb.append(this.y);
        }
        sb.append('}');
        return sb.toString();
    }

    public int u() {
        b bVar = this.L;
        if (bVar == null) {
            return 0;
        }
        return bVar.f235f;
    }

    public final Resources v() {
        return J().getResources();
    }

    public Object w() {
        b bVar = this.L;
        if (bVar == null) {
            return null;
        }
        return bVar.f238k;
    }

    public int x() {
        b bVar = this.L;
        if (bVar == null) {
            return 0;
        }
        return bVar.c;
    }

    public final void y() {
        this.S = new LifecycleRegistry(this);
        this.V = new SavedStateRegistryController(this);
        this.S.a(new LifecycleEventObserver() {
            /* class androidx.fragment.app.Fragment.AnonymousClass2 */

            public void a(LifecycleOwner lifecycleOwner, Lifecycle.a aVar) {
                View view;
                if (aVar == Lifecycle.a.ON_STOP && (view = Fragment.this.H) != null) {
                    view.cancelPendingInputEvents();
                }
            }
        });
    }

    public boolean z() {
        b bVar = this.L;
        if (bVar == null) {
            return false;
        }
        return bVar.f246s;
    }

    @SuppressLint({"BanParcelableUsage"})
    public static class d implements Parcelable {
        public static final Parcelable.Creator<d> CREATOR = new a();
        public final Bundle b;

        public static class a implements Parcelable.ClassLoaderCreator<d> {
            public Object createFromParcel(Parcel parcel) {
                return new d(parcel, null);
            }

            public Object[] newArray(int i2) {
                return new d[i2];
            }

            public Object createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new d(parcel, classLoader);
            }
        }

        public d(Bundle bundle) {
            this.b = bundle;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i2) {
            parcel.writeBundle(this.b);
        }

        public d(Parcel parcel, ClassLoader classLoader) {
            Bundle readBundle = parcel.readBundle();
            this.b = readBundle;
            if (classLoader != null && readBundle != null) {
                readBundle.setClassLoader(classLoader);
            }
        }
    }

    @Deprecated
    public static Fragment a(Context context, String str, Bundle bundle) {
        try {
            Fragment fragment = (Fragment) FragmentFactory.d(context.getClassLoader(), str).getConstructor(new Class[0]).newInstance(new Object[0]);
            if (bundle != null) {
                bundle.setClassLoader(fragment.getClass().getClassLoader());
                fragment.f(bundle);
            }
            return fragment;
        } catch (InstantiationException e2) {
            throw new InstantiationException(outline.a("Unable to instantiate fragment ", str, ": make sure class name exists, is public, and has an empty constructor that is public"), e2);
        } catch (IllegalAccessException e3) {
            throw new InstantiationException(outline.a("Unable to instantiate fragment ", str, ": make sure class name exists, is public, and has an empty constructor that is public"), e3);
        } catch (NoSuchMethodException e4) {
            throw new InstantiationException(outline.a("Unable to instantiate fragment ", str, ": could not find Fragment constructor"), e4);
        } catch (InvocationTargetException e5) {
            throw new InstantiationException(outline.a("Unable to instantiate fragment ", str, ": calling Fragment constructor caused an exception"), e5);
        }
    }

    public void f(Bundle bundle) {
        boolean z2;
        FragmentManagerImpl fragmentManagerImpl = this.f232s;
        if (fragmentManagerImpl != null) {
            if (fragmentManagerImpl == null) {
                z2 = false;
            } else {
                z2 = fragmentManagerImpl.m();
            }
            if (z2) {
                throw new IllegalStateException("Fragment already added and state has been saved");
            }
        }
        this.g = bundle;
    }

    public void b(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.u.n();
        boolean z2 = true;
        this.f230q = true;
        this.T = new FragmentViewLifecycleOwner();
        View a2 = a(layoutInflater, viewGroup, bundle);
        this.H = a2;
        if (a2 != null) {
            FragmentViewLifecycleOwner fragmentViewLifecycleOwner = this.T;
            if (fragmentViewLifecycleOwner.b == null) {
                fragmentViewLifecycleOwner.b = new LifecycleRegistry(fragmentViewLifecycleOwner);
            }
            this.U.a((k) this.T);
            return;
        }
        if (this.T.b == null) {
            z2 = false;
        }
        if (!z2) {
            this.T = null;
            return;
        }
        throw new IllegalStateException("Called getViewLifecycleOwner() but onCreateView() returned null");
    }

    public final String a(int i2) {
        return v().getString(i2);
    }

    public void a(AttributeSet attributeSet, Bundle bundle) {
        Activity activity;
        this.F = true;
        FragmentHostCallback fragmentHostCallback = this.f233t;
        if (fragmentHostCallback == null) {
            activity = null;
        } else {
            activity = fragmentHostCallback.b;
        }
        if (activity != null) {
            this.F = false;
            this.F = true;
        }
    }

    public void a(Context context) {
        Activity activity;
        this.F = true;
        FragmentHostCallback fragmentHostCallback = this.f233t;
        if (fragmentHostCallback == null) {
            activity = null;
        } else {
            activity = fragmentHostCallback.b;
        }
        if (activity != null) {
            this.F = false;
            this.F = true;
        }
    }

    public void b(int i2) {
        if (this.L != null || i2 != 0) {
            k().d = i2;
        }
    }

    public void a(Bundle bundle) {
        this.F = true;
    }

    public void a(c cVar) {
        k();
        c cVar2 = this.L.f245r;
        if (cVar != cVar2) {
            if (cVar == null || cVar2 == null) {
                b bVar = this.L;
                if (bVar.f244q) {
                    bVar.f245r = cVar;
                }
                if (cVar != null) {
                    ((FragmentManagerImpl.j) cVar).c++;
                    return;
                }
                return;
            }
            throw new IllegalStateException("Trying to set a replacement startPostponedEnterTransition on " + this);
        }
    }

    public void a(View view) {
        k().a = view;
    }

    public void a(Animator animator) {
        k().b = animator;
    }

    public void a(boolean z2) {
        k().f246s = z2;
    }
}
