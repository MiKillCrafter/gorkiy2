package androidx.appcompat.app;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewStub;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import androidx.core.widget.NestedScrollView;
import i.b.j;
import i.b.k.AppCompatDialog;
import java.lang.ref.WeakReference;

public class AlertController {
    public NestedScrollView A;
    public int B = 0;
    public Drawable C;
    public ImageView D;
    public TextView E;
    public TextView F;
    public View G;
    public ListAdapter H;
    public int I = -1;
    public int J;
    public int K;
    public int L;
    public int M;
    public int N;
    public int O;
    public boolean P;
    public int Q = 0;
    public Handler R;
    public final View.OnClickListener S = new a();
    public final Context a;
    public final AppCompatDialog b;
    public final Window c;
    public final int d;

    /* renamed from: e  reason: collision with root package name */
    public CharSequence f4e;

    /* renamed from: f  reason: collision with root package name */
    public CharSequence f5f;
    public ListView g;
    public View h;

    /* renamed from: i  reason: collision with root package name */
    public int f6i;

    /* renamed from: j  reason: collision with root package name */
    public int f7j;

    /* renamed from: k  reason: collision with root package name */
    public int f8k;

    /* renamed from: l  reason: collision with root package name */
    public int f9l;

    /* renamed from: m  reason: collision with root package name */
    public int f10m;

    /* renamed from: n  reason: collision with root package name */
    public boolean f11n = false;

    /* renamed from: o  reason: collision with root package name */
    public Button f12o;

    /* renamed from: p  reason: collision with root package name */
    public CharSequence f13p;

    /* renamed from: q  reason: collision with root package name */
    public Message f14q;

    /* renamed from: r  reason: collision with root package name */
    public Drawable f15r;

    /* renamed from: s  reason: collision with root package name */
    public Button f16s;

    /* renamed from: t  reason: collision with root package name */
    public CharSequence f17t;
    public Message u;
    public Drawable v;
    public Button w;
    public CharSequence x;
    public Message y;
    public Drawable z;

    public static class RecycleListView extends ListView {
        public final int b;
        public final int c;

        public RecycleListView(Context context) {
            this(context, null);
        }

        public RecycleListView(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, j.RecycleListView);
            this.c = obtainStyledAttributes.getDimensionPixelOffset(j.RecycleListView_paddingBottomNoButtons, -1);
            this.b = obtainStyledAttributes.getDimensionPixelOffset(j.RecycleListView_paddingTopNoTitle, -1);
        }
    }

    public class a implements View.OnClickListener {
        public a() {
        }

        public void onClick(View view) {
            Message message;
            Message message2;
            Message message3;
            Message message4;
            AlertController alertController = AlertController.this;
            if (view != alertController.f12o || (message4 = alertController.f14q) == null) {
                AlertController alertController2 = AlertController.this;
                if (view != alertController2.f16s || (message3 = alertController2.u) == null) {
                    AlertController alertController3 = AlertController.this;
                    message = (view != alertController3.w || (message2 = alertController3.y) == null) ? null : Message.obtain(message2);
                } else {
                    message = Message.obtain(message3);
                }
            } else {
                message = Message.obtain(message4);
            }
            if (message != null) {
                message.sendToTarget();
            }
            AlertController alertController4 = AlertController.this;
            alertController4.R.obtainMessage(1, alertController4.b).sendToTarget();
        }
    }

    public static class b {
        public final Context a;
        public final LayoutInflater b;
        public int c = 0;
        public Drawable d;

        /* renamed from: e  reason: collision with root package name */
        public int f18e = 0;

        /* renamed from: f  reason: collision with root package name */
        public CharSequence f19f;
        public View g;
        public CharSequence h;

        /* renamed from: i  reason: collision with root package name */
        public CharSequence f20i;

        /* renamed from: j  reason: collision with root package name */
        public DialogInterface.OnClickListener f21j;

        /* renamed from: k  reason: collision with root package name */
        public CharSequence f22k;

        /* renamed from: l  reason: collision with root package name */
        public DialogInterface.OnClickListener f23l;

        /* renamed from: m  reason: collision with root package name */
        public boolean f24m;

        /* renamed from: n  reason: collision with root package name */
        public DialogInterface.OnCancelListener f25n;

        /* renamed from: o  reason: collision with root package name */
        public DialogInterface.OnDismissListener f26o;

        /* renamed from: p  reason: collision with root package name */
        public DialogInterface.OnKeyListener f27p;

        /* renamed from: q  reason: collision with root package name */
        public ListAdapter f28q;

        /* renamed from: r  reason: collision with root package name */
        public DialogInterface.OnClickListener f29r;

        /* renamed from: s  reason: collision with root package name */
        public boolean f30s = false;

        /* renamed from: t  reason: collision with root package name */
        public boolean f31t;
        public int u = -1;

        public b(Context context) {
            this.a = context;
            this.f24m = true;
            this.b = (LayoutInflater) context.getSystemService("layout_inflater");
        }
    }

    public static final class c extends Handler {
        public WeakReference<DialogInterface> a;

        public c(DialogInterface dialogInterface) {
            this.a = new WeakReference<>(dialogInterface);
        }

        public void handleMessage(Message message) {
            int i2 = message.what;
            if (i2 == -3 || i2 == -2 || i2 == -1) {
                ((DialogInterface.OnClickListener) message.obj).onClick(this.a.get(), message.what);
            } else if (i2 == 1) {
                ((DialogInterface) message.obj).dismiss();
            }
        }
    }

    public static class d extends ArrayAdapter<CharSequence> {
        public d(Context context, int i2, int i3, CharSequence[] charSequenceArr) {
            super(context, i2, i3, charSequenceArr);
        }

        public long getItemId(int i2) {
            return (long) i2;
        }

        public boolean hasStableIds() {
            return true;
        }
    }

    public AlertController(Context context, AppCompatDialog appCompatDialog, Window window) {
        this.a = context;
        this.b = appCompatDialog;
        this.c = window;
        this.R = new c(appCompatDialog);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(null, j.AlertDialog, i.b.a.alertDialogStyle, 0);
        this.J = obtainStyledAttributes.getResourceId(j.AlertDialog_android_layout, 0);
        this.K = obtainStyledAttributes.getResourceId(j.AlertDialog_buttonPanelSideLayout, 0);
        this.L = obtainStyledAttributes.getResourceId(j.AlertDialog_listLayout, 0);
        this.M = obtainStyledAttributes.getResourceId(j.AlertDialog_multiChoiceItemLayout, 0);
        this.N = obtainStyledAttributes.getResourceId(j.AlertDialog_singleChoiceItemLayout, 0);
        this.O = obtainStyledAttributes.getResourceId(j.AlertDialog_listItemLayout, 0);
        this.P = obtainStyledAttributes.getBoolean(j.AlertDialog_showTitle, true);
        this.d = obtainStyledAttributes.getDimensionPixelSize(j.AlertDialog_buttonIconDimen, 0);
        obtainStyledAttributes.recycle();
        appCompatDialog.a(1);
    }

    public static boolean a(View view) {
        if (view.onCheckIsTextEditor()) {
            return true;
        }
        if (!(view instanceof ViewGroup)) {
            return false;
        }
        ViewGroup viewGroup = (ViewGroup) view;
        int childCount = viewGroup.getChildCount();
        while (childCount > 0) {
            childCount--;
            if (a(viewGroup.getChildAt(childCount))) {
                return true;
            }
        }
        return false;
    }

    public void a(int i2, CharSequence charSequence, DialogInterface.OnClickListener onClickListener, Message message, Drawable drawable) {
        if (message == null && onClickListener != null) {
            message = this.R.obtainMessage(i2, onClickListener);
        }
        if (i2 == -3) {
            this.x = charSequence;
            this.y = message;
            this.z = drawable;
        } else if (i2 == -2) {
            this.f17t = charSequence;
            this.u = message;
            this.v = drawable;
        } else if (i2 == -1) {
            this.f13p = charSequence;
            this.f14q = message;
            this.f15r = drawable;
        } else {
            throw new IllegalArgumentException("Button does not exist");
        }
    }

    public void a(int i2) {
        this.C = null;
        this.B = i2;
        ImageView imageView = this.D;
        if (imageView == null) {
            return;
        }
        if (i2 != 0) {
            imageView.setVisibility(0);
            this.D.setImageResource(this.B);
            return;
        }
        imageView.setVisibility(8);
    }

    public final ViewGroup a(View view, View view2) {
        if (view == null) {
            if (view2 instanceof ViewStub) {
                view2 = ((ViewStub) view2).inflate();
            }
            return (ViewGroup) view2;
        }
        if (view2 != null) {
            ViewParent parent = view2.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeView(view2);
            }
        }
        if (view instanceof ViewStub) {
            view = ((ViewStub) view).inflate();
        }
        return (ViewGroup) view;
    }

    public static void a(View view, View view2, View view3) {
        int i2 = 0;
        if (view2 != null) {
            view2.setVisibility(view.canScrollVertically(-1) ? 0 : 4);
        }
        if (view3 != null) {
            if (!view.canScrollVertically(1)) {
                i2 = 4;
            }
            view3.setVisibility(i2);
        }
    }

    public final void a(Button button) {
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) button.getLayoutParams();
        layoutParams.gravity = 1;
        layoutParams.weight = 0.5f;
        button.setLayoutParams(layoutParams);
    }
}
