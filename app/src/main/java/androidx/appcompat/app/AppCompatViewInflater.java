package androidx.appcompat.app;

import android.content.Context;
import android.content.ContextWrapper;
import android.util.AttributeSet;
import android.view.View;
import androidx.appcompat.widget.AppCompatCheckBox;
import i.b.q.AppCompatAutoCompleteTextView;
import i.b.q.AppCompatButton;
import i.b.q.AppCompatRadioButton;
import i.b.q.AppCompatTextView;
import i.e.ArrayMap;
import j.a.a.a.outline;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

public class AppCompatViewInflater {
    public static final Class<?>[] b = {Context.class, AttributeSet.class};
    public static final int[] c = {16843375};
    public static final String[] d = {"android.widget.", "android.view.", "android.webkit."};

    /* renamed from: e  reason: collision with root package name */
    public static final Map<String, Constructor<? extends View>> f32e = new ArrayMap();
    public final Object[] a = new Object[2];

    public static class a implements View.OnClickListener {
        public final View b;
        public final String c;
        public Method d;

        /* renamed from: e  reason: collision with root package name */
        public Context f33e;

        public a(View view, String str) {
            this.b = view;
            this.c = str;
        }

        public void onClick(View view) {
            String str;
            Method method;
            if (this.d == null) {
                for (Context context = this.b.getContext(); context != null; context = context instanceof ContextWrapper ? ((ContextWrapper) context).getBaseContext() : null) {
                    try {
                        if (!context.isRestricted() && (method = context.getClass().getMethod(this.c, View.class)) != null) {
                            this.d = method;
                            this.f33e = context;
                        }
                    } catch (NoSuchMethodException unused) {
                    }
                }
                int id = this.b.getId();
                if (id == -1) {
                    str = "";
                } else {
                    StringBuilder a = outline.a(" with id '");
                    a.append(this.b.getContext().getResources().getResourceEntryName(id));
                    a.append("'");
                    str = a.toString();
                }
                StringBuilder a2 = outline.a("Could not find method ");
                a2.append(this.c);
                a2.append("(View) in a parent or ancestor Context for android:onClick attribute defined on view ");
                a2.append(this.b.getClass());
                a2.append(str);
                throw new IllegalStateException(a2.toString());
            }
            try {
                this.d.invoke(this.f33e, view);
            } catch (IllegalAccessException e2) {
                throw new IllegalStateException("Could not execute non-public method for android:onClick", e2);
            } catch (InvocationTargetException e3) {
                throw new IllegalStateException("Could not execute method for android:onClick", e3);
            }
        }
    }

    public AppCompatAutoCompleteTextView a(Context context, AttributeSet attributeSet) {
        return new AppCompatAutoCompleteTextView(context, attributeSet);
    }

    public AppCompatButton b(Context context, AttributeSet attributeSet) {
        return new AppCompatButton(context, attributeSet);
    }

    public AppCompatCheckBox c(Context context, AttributeSet attributeSet) {
        return new AppCompatCheckBox(context, attributeSet);
    }

    public AppCompatRadioButton d(Context context, AttributeSet attributeSet) {
        return new AppCompatRadioButton(context, attributeSet, i.b.a.radioButtonStyle);
    }

    public AppCompatTextView e(Context context, AttributeSet attributeSet) {
        return new AppCompatTextView(context, attributeSet);
    }

    public final void a(View view, String str) {
        if (view == null) {
            throw new IllegalStateException(getClass().getName() + " asked to inflate view for <" + str + ">, but returned null");
        }
    }

    public final View a(Context context, String str, String str2) {
        String str3;
        Constructor<? extends U> constructor = f32e.get(str);
        if (constructor == null) {
            if (str2 != null) {
                try {
                    str3 = str2 + str;
                } catch (Exception unused) {
                    return null;
                }
            } else {
                str3 = str;
            }
            constructor = Class.forName(str3, false, context.getClassLoader()).asSubclass(View.class).getConstructor(b);
            f32e.put(str, constructor);
        }
        constructor.setAccessible(true);
        return (View) constructor.newInstance(this.a);
    }
}
