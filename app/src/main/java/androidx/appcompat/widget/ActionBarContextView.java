package androidx.appcompat.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import i.b.f;
import i.b.g;
import i.b.j;
import i.b.l.a.AppCompatResources;
import i.b.p.ActionMode;
import i.b.p.i.MenuBuilder;
import i.b.p.i.MenuView;
import i.b.q.AbsActionBarView;
import i.b.q.ActionMenuPresenter;
import i.b.q.ViewUtils;
import i.h.l.ViewCompat;

public class ActionBarContextView extends AbsActionBarView {

    /* renamed from: j  reason: collision with root package name */
    public CharSequence f61j;

    /* renamed from: k  reason: collision with root package name */
    public CharSequence f62k;

    /* renamed from: l  reason: collision with root package name */
    public View f63l;

    /* renamed from: m  reason: collision with root package name */
    public View f64m;

    /* renamed from: n  reason: collision with root package name */
    public LinearLayout f65n;

    /* renamed from: o  reason: collision with root package name */
    public TextView f66o;

    /* renamed from: p  reason: collision with root package name */
    public TextView f67p;

    /* renamed from: q  reason: collision with root package name */
    public int f68q;

    /* renamed from: r  reason: collision with root package name */
    public int f69r;

    /* renamed from: s  reason: collision with root package name */
    public boolean f70s;

    /* renamed from: t  reason: collision with root package name */
    public int f71t;

    public class a implements View.OnClickListener {
        public final /* synthetic */ ActionMode b;

        public a(ActionBarContextView actionBarContextView, ActionMode actionMode) {
            this.b = actionMode;
        }

        public void onClick(View view) {
            this.b.a();
        }
    }

    public ActionBarContextView(Context context) {
        this(context, null);
    }

    public final void a() {
        if (this.f65n == null) {
            LayoutInflater.from(getContext()).inflate(g.abc_action_bar_title_item, this);
            LinearLayout linearLayout = (LinearLayout) getChildAt(getChildCount() - 1);
            this.f65n = linearLayout;
            this.f66o = (TextView) linearLayout.findViewById(f.action_bar_title);
            this.f67p = (TextView) this.f65n.findViewById(f.action_bar_subtitle);
            if (this.f68q != 0) {
                this.f66o.setTextAppearance(getContext(), this.f68q);
            }
            if (this.f69r != 0) {
                this.f67p.setTextAppearance(getContext(), this.f69r);
            }
        }
        this.f66o.setText(this.f61j);
        this.f67p.setText(this.f62k);
        boolean z = !TextUtils.isEmpty(this.f61j);
        boolean z2 = !TextUtils.isEmpty(this.f62k);
        int i2 = 0;
        this.f67p.setVisibility(z2 ? 0 : 8);
        LinearLayout linearLayout2 = this.f65n;
        if (!z && !z2) {
            i2 = 8;
        }
        linearLayout2.setVisibility(i2);
        if (this.f65n.getParent() == null) {
            addView(this.f65n);
        }
    }

    public void b() {
        removeAllViews();
        this.f64m = null;
        super.d = null;
    }

    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new ViewGroup.MarginLayoutParams(-1, -2);
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new ViewGroup.MarginLayoutParams(getContext(), attributeSet);
    }

    public /* bridge */ /* synthetic */ int getAnimatedVisibility() {
        return super.getAnimatedVisibility();
    }

    public /* bridge */ /* synthetic */ int getContentHeight() {
        return super.getContentHeight();
    }

    public CharSequence getSubtitle() {
        return this.f62k;
    }

    public CharSequence getTitle() {
        return this.f61j;
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        ActionMenuPresenter actionMenuPresenter = super.f932e;
        if (actionMenuPresenter != null) {
            actionMenuPresenter.b();
            super.f932e.d();
        }
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        if (accessibilityEvent.getEventType() == 32) {
            accessibilityEvent.setSource(this);
            accessibilityEvent.setClassName(ActionBarContextView.class.getName());
            accessibilityEvent.setPackageName(getContext().getPackageName());
            accessibilityEvent.setContentDescription(this.f61j);
            return;
        }
        super.onInitializeAccessibilityEvent(accessibilityEvent);
    }

    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        boolean a2 = ViewUtils.a(this);
        int paddingRight = a2 ? (i4 - i2) - getPaddingRight() : getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingTop2 = ((i5 - i3) - getPaddingTop()) - getPaddingBottom();
        View view = this.f63l;
        if (!(view == null || view.getVisibility() == 8)) {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.f63l.getLayoutParams();
            int i6 = a2 ? marginLayoutParams.rightMargin : marginLayoutParams.leftMargin;
            int i7 = a2 ? marginLayoutParams.leftMargin : marginLayoutParams.rightMargin;
            int i8 = a2 ? paddingRight - i6 : paddingRight + i6;
            int a3 = i8 + a(this.f63l, i8, paddingTop, paddingTop2, a2);
            paddingRight = a2 ? a3 - i7 : a3 + i7;
        }
        int i9 = paddingRight;
        LinearLayout linearLayout = this.f65n;
        if (!(linearLayout == null || this.f64m != null || linearLayout.getVisibility() == 8)) {
            i9 += a(this.f65n, i9, paddingTop, paddingTop2, a2);
        }
        int i10 = i9;
        View view2 = this.f64m;
        if (view2 != null) {
            a(view2, i10, paddingTop, paddingTop2, a2);
        }
        int paddingLeft = a2 ? getPaddingLeft() : (i4 - i2) - getPaddingRight();
        ActionMenuView actionMenuView = super.d;
        if (actionMenuView != null) {
            a(actionMenuView, paddingLeft, paddingTop, paddingTop2, !a2);
        }
    }

    public void onMeasure(int i2, int i3) {
        int i4 = 1073741824;
        if (View.MeasureSpec.getMode(i2) != 1073741824) {
            throw new IllegalStateException(ActionBarContextView.class.getSimpleName() + " can only be used with android:layout_width=\"match_parent\" (or fill_parent)");
        } else if (View.MeasureSpec.getMode(i3) != 0) {
            int size = View.MeasureSpec.getSize(i2);
            int i5 = super.f933f;
            if (i5 <= 0) {
                i5 = View.MeasureSpec.getSize(i3);
            }
            int paddingBottom = getPaddingBottom() + getPaddingTop();
            int paddingLeft = (size - getPaddingLeft()) - getPaddingRight();
            int i6 = i5 - paddingBottom;
            int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(i6, RecyclerView.UNDEFINED_DURATION);
            View view = this.f63l;
            if (view != null) {
                int a2 = a(view, paddingLeft, makeMeasureSpec, 0);
                ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.f63l.getLayoutParams();
                paddingLeft = a2 - (marginLayoutParams.leftMargin + marginLayoutParams.rightMargin);
            }
            ActionMenuView actionMenuView = super.d;
            if (actionMenuView != null && actionMenuView.getParent() == this) {
                paddingLeft = a(super.d, paddingLeft, makeMeasureSpec, 0);
            }
            LinearLayout linearLayout = this.f65n;
            if (linearLayout != null && this.f64m == null) {
                if (this.f70s) {
                    this.f65n.measure(View.MeasureSpec.makeMeasureSpec(0, 0), makeMeasureSpec);
                    int measuredWidth = this.f65n.getMeasuredWidth();
                    boolean z = measuredWidth <= paddingLeft;
                    if (z) {
                        paddingLeft -= measuredWidth;
                    }
                    this.f65n.setVisibility(z ? 0 : 8);
                } else {
                    paddingLeft = a(linearLayout, paddingLeft, makeMeasureSpec, 0);
                }
            }
            View view2 = this.f64m;
            if (view2 != null) {
                ViewGroup.LayoutParams layoutParams = view2.getLayoutParams();
                int i7 = layoutParams.width != -2 ? 1073741824 : RecyclerView.UNDEFINED_DURATION;
                int i8 = layoutParams.width;
                if (i8 >= 0) {
                    paddingLeft = Math.min(i8, paddingLeft);
                }
                if (layoutParams.height == -2) {
                    i4 = RecyclerView.UNDEFINED_DURATION;
                }
                int i9 = layoutParams.height;
                if (i9 >= 0) {
                    i6 = Math.min(i9, i6);
                }
                this.f64m.measure(View.MeasureSpec.makeMeasureSpec(paddingLeft, i7), View.MeasureSpec.makeMeasureSpec(i6, i4));
            }
            if (super.f933f <= 0) {
                int childCount = getChildCount();
                int i10 = 0;
                for (int i11 = 0; i11 < childCount; i11++) {
                    int measuredHeight = getChildAt(i11).getMeasuredHeight() + paddingBottom;
                    if (measuredHeight > i10) {
                        i10 = measuredHeight;
                    }
                }
                setMeasuredDimension(size, i10);
                return;
            }
            setMeasuredDimension(size, i5);
        } else {
            throw new IllegalStateException(ActionBarContextView.class.getSimpleName() + " can only be used with android:layout_height=\"wrap_content\"");
        }
    }

    public void setContentHeight(int i2) {
        super.f933f = i2;
    }

    public void setCustomView(View view) {
        LinearLayout linearLayout;
        View view2 = this.f64m;
        if (view2 != null) {
            removeView(view2);
        }
        this.f64m = view;
        if (!(view == null || (linearLayout = this.f65n) == null)) {
            removeView(linearLayout);
            this.f65n = null;
        }
        if (view != null) {
            addView(view);
        }
        requestLayout();
    }

    public void setSubtitle(CharSequence charSequence) {
        this.f62k = charSequence;
        a();
    }

    public void setTitle(CharSequence charSequence) {
        this.f61j = charSequence;
        a();
    }

    public void setTitleOptional(boolean z) {
        if (z != this.f70s) {
            requestLayout();
        }
        this.f70s = z;
    }

    public /* bridge */ /* synthetic */ void setVisibility(int i2) {
        super.setVisibility(i2);
    }

    public boolean shouldDelayChildPressedState() {
        return false;
    }

    public ActionBarContextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, i.b.a.actionModeStyle);
    }

    public ActionBarContextView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        Drawable drawable;
        int resourceId;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, j.ActionMode, i2, 0);
        int i3 = j.ActionMode_background;
        if (!obtainStyledAttributes.hasValue(i3) || (resourceId = obtainStyledAttributes.getResourceId(i3, 0)) == 0) {
            drawable = obtainStyledAttributes.getDrawable(i3);
        } else {
            drawable = AppCompatResources.c(context, resourceId);
        }
        ViewCompat.a(this, drawable);
        this.f68q = obtainStyledAttributes.getResourceId(j.ActionMode_titleTextStyle, 0);
        this.f69r = obtainStyledAttributes.getResourceId(j.ActionMode_subtitleTextStyle, 0);
        super.f933f = obtainStyledAttributes.getLayoutDimension(j.ActionMode_height, 0);
        this.f71t = obtainStyledAttributes.getResourceId(j.ActionMode_closeItemLayout, g.abc_action_mode_close_item_material);
        obtainStyledAttributes.recycle();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, androidx.appcompat.widget.ActionBarContextView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public void a(ActionMode actionMode) {
        View view = this.f63l;
        if (view == null) {
            View inflate = LayoutInflater.from(getContext()).inflate(this.f71t, (ViewGroup) this, false);
            this.f63l = inflate;
            addView(inflate);
        } else if (view.getParent() == null) {
            addView(this.f63l);
        }
        this.f63l.findViewById(f.action_mode_close_button).setOnClickListener(new a(this, actionMode));
        MenuBuilder menuBuilder = (MenuBuilder) actionMode.c();
        ActionMenuPresenter actionMenuPresenter = super.f932e;
        if (actionMenuPresenter != null) {
            actionMenuPresenter.a();
        }
        ActionMenuPresenter actionMenuPresenter2 = new ActionMenuPresenter(getContext());
        super.f932e = actionMenuPresenter2;
        actionMenuPresenter2.f938m = true;
        actionMenuPresenter2.f939n = true;
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-2, -1);
        menuBuilder.a(super.f932e, super.c);
        ActionMenuPresenter actionMenuPresenter3 = super.f932e;
        MenuView menuView = actionMenuPresenter3.f860i;
        if (menuView == null) {
            MenuView menuView2 = (MenuView) actionMenuPresenter3.f858e.inflate(actionMenuPresenter3.g, (ViewGroup) this, false);
            actionMenuPresenter3.f860i = menuView2;
            menuView2.a(actionMenuPresenter3.d);
            actionMenuPresenter3.a(true);
        }
        MenuView menuView3 = actionMenuPresenter3.f860i;
        if (menuView != menuView3) {
            ((ActionMenuView) menuView3).setPresenter(actionMenuPresenter3);
        }
        ActionMenuView actionMenuView = (ActionMenuView) menuView3;
        super.d = actionMenuView;
        ViewCompat.a(actionMenuView, (Drawable) null);
        addView(super.d, layoutParams);
    }
}
