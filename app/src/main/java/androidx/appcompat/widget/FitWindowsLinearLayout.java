package androidx.appcompat.widget;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import i.b.k.AppCompatDelegateImpl2;
import i.b.q.FitWindowsViewGroup;

public class FitWindowsLinearLayout extends LinearLayout {
    public FitWindowsViewGroup b;

    public FitWindowsLinearLayout(Context context) {
        super(context);
    }

    public boolean fitSystemWindows(Rect rect) {
        FitWindowsViewGroup fitWindowsViewGroup = this.b;
        if (fitWindowsViewGroup != null) {
            rect.top = ((AppCompatDelegateImpl2) fitWindowsViewGroup).a.f(rect.top);
        }
        return super.fitSystemWindows(rect);
    }

    public void setOnFitSystemWindowsListener(FitWindowsViewGroup fitWindowsViewGroup) {
        this.b = fitWindowsViewGroup;
    }

    public FitWindowsLinearLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }
}
