package androidx.appcompat.widget;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import i.b.k.AppCompatDelegateImpl2;
import i.b.q.FitWindowsViewGroup;

public class FitWindowsFrameLayout extends FrameLayout {
    public FitWindowsViewGroup b;

    public FitWindowsFrameLayout(Context context) {
        super(context);
    }

    public boolean fitSystemWindows(Rect rect) {
        FitWindowsViewGroup fitWindowsViewGroup = this.b;
        if (fitWindowsViewGroup != null) {
            rect.top = ((AppCompatDelegateImpl2) fitWindowsViewGroup).a.f(rect.top);
        }
        return super.fitSystemWindows(rect);
    }

    public void setOnFitSystemWindowsListener(FitWindowsViewGroup fitWindowsViewGroup) {
        this.b = fitWindowsViewGroup;
    }

    public FitWindowsFrameLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }
}
