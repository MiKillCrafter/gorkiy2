package org.bouncycastle.crypto;

public class CryptoException extends Exception {
    public CryptoException() {
    }

    public CryptoException(String str) {
        super(str);
    }

    public Throwable getCause() {
        return null;
    }
}
