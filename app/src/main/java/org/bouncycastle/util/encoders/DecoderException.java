package org.bouncycastle.util.encoders;

public class DecoderException extends IllegalStateException {
    public Throwable b;

    public DecoderException(String str, Throwable th) {
        super(str);
        this.b = th;
    }

    public Throwable getCause() {
        return this.b;
    }
}
