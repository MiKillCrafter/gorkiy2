package okhttp3.internal.http2;

import java.io.IOException;
import n.n.c.Intrinsics;
import o.m0.g.ErrorCode;

/* compiled from: StreamResetException.kt */
public final class StreamResetException extends IOException {
    public final ErrorCode b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public StreamResetException(ErrorCode errorCode) {
        super("stream was reset: " + errorCode);
        if (errorCode != null) {
            this.b = errorCode;
            return;
        }
        Intrinsics.a("errorCode");
        throw null;
    }
}
