package o.m0.e;

import j.a.a.a.outline;
import java.io.IOException;
import java.net.ProtocolException;
import n.i.Collections;
import n.n.c.Intrinsics;
import n.r.Indent;
import o.Call;
import o.EventListener;
import o.Interceptor;
import o.Request;
import o.RequestBody;
import o.Response;
import o.ResponseBody;
import o.m0.Util;
import o.m0.d.Exchange;
import o.m0.d.RealConnection;
import p.BufferedSink;
import p.Source;

/* compiled from: CallServerInterceptor.kt */
public final class CallServerInterceptor implements Interceptor {
    public final boolean b;

    public CallServerInterceptor(boolean z) {
        this.b = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      n.r.Indent.a(java.lang.String, java.lang.String, int):java.lang.String
      n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean */
    public Response a(Interceptor.a aVar) {
        boolean z;
        Response.a aVar2;
        Response response;
        Long l2 = null;
        if (aVar != null) {
            RealInterceptorChain realInterceptorChain = (RealInterceptorChain) aVar;
            Exchange exchange = realInterceptorChain.d;
            if (exchange != null) {
                Request request = realInterceptorChain.f2976f;
                RequestBody requestBody = request.f2896e;
                long currentTimeMillis = System.currentTimeMillis();
                try {
                    EventListener eventListener = exchange.d;
                    Call call = exchange.c;
                    if (eventListener == null) {
                        throw null;
                    } else if (call != null) {
                        exchange.f2940f.a(request);
                        EventListener eventListener2 = exchange.d;
                        Call call2 = exchange.c;
                        if (eventListener2 == null) {
                            throw null;
                        } else if (call2 != null) {
                            if (!HttpMethod.a(request.c) || requestBody == null) {
                                exchange.b.a(exchange, true, false, null);
                                z = false;
                                aVar2 = null;
                            } else {
                                if (Indent.a("100-continue", request.a("Expect"), true)) {
                                    try {
                                        exchange.f2940f.c();
                                        EventListener eventListener3 = exchange.d;
                                        Call call3 = exchange.c;
                                        if (eventListener3 == null) {
                                            throw null;
                                        } else if (call3 != null) {
                                            aVar2 = exchange.a(true);
                                            z = true;
                                        } else {
                                            Intrinsics.a("call");
                                            throw null;
                                        }
                                    } catch (IOException e2) {
                                        exchange.d.a(exchange.c, e2);
                                        exchange.a(e2);
                                        throw e2;
                                    }
                                } else {
                                    z = false;
                                    aVar2 = null;
                                }
                                if (aVar2 == null) {
                                    BufferedSink a = Collections.a(exchange.a(request, false));
                                    requestBody.a(a);
                                    a.close();
                                } else {
                                    exchange.b.a(exchange, true, false, null);
                                    RealConnection a2 = exchange.a();
                                    if (a2 == null) {
                                        Intrinsics.a();
                                        throw null;
                                    } else if (!a2.b()) {
                                        RealConnection a3 = exchange.f2940f.a();
                                        if (a3 != null) {
                                            a3.c();
                                        } else {
                                            Intrinsics.a();
                                            throw null;
                                        }
                                    }
                                }
                            }
                            try {
                                exchange.f2940f.b();
                                if (!z) {
                                    EventListener eventListener4 = exchange.d;
                                    Call call4 = exchange.c;
                                    if (eventListener4 == null) {
                                        throw null;
                                    } else if (call4 == null) {
                                        Intrinsics.a("call");
                                        throw null;
                                    }
                                }
                                if (aVar2 == null && (aVar2 = exchange.a(false)) == null) {
                                    Intrinsics.a();
                                    throw null;
                                }
                                aVar2.a = request;
                                RealConnection a4 = exchange.a();
                                if (a4 != null) {
                                    aVar2.f2909e = a4.d;
                                    aVar2.f2913k = currentTimeMillis;
                                    aVar2.f2914l = System.currentTimeMillis();
                                    Response a5 = aVar2.a();
                                    int i2 = a5.f2901f;
                                    if (i2 == 100) {
                                        Response.a a6 = exchange.a(false);
                                        if (a6 != null) {
                                            a6.a = request;
                                            RealConnection a7 = exchange.a();
                                            if (a7 != null) {
                                                a6.f2909e = a7.d;
                                                a6.f2913k = currentTimeMillis;
                                                a6.f2914l = System.currentTimeMillis();
                                                a5 = a6.a();
                                                i2 = a5.f2901f;
                                            } else {
                                                Intrinsics.a();
                                                throw null;
                                            }
                                        } else {
                                            Intrinsics.a();
                                            throw null;
                                        }
                                    }
                                    EventListener eventListener5 = exchange.d;
                                    Call call5 = exchange.c;
                                    if (eventListener5 == null) {
                                        throw null;
                                    } else if (call5 != null) {
                                        if (!this.b || i2 != 101) {
                                            Response.a aVar3 = new Response.a(a5);
                                            try {
                                                EventListener eventListener6 = exchange.d;
                                                Call call6 = exchange.c;
                                                if (eventListener6 == null) {
                                                    throw null;
                                                } else if (call6 != null) {
                                                    String a8 = Response.a(a5, "Content-Type", null, 2);
                                                    long a9 = exchange.f2940f.a(a5);
                                                    aVar3.g = new RealResponseBody(a8, a9, Collections.a((Source) new Exchange.b(exchange, exchange.f2940f.b(a5), a9)));
                                                    response = aVar3.a();
                                                } else {
                                                    Intrinsics.a("call");
                                                    throw null;
                                                }
                                            } catch (IOException e3) {
                                                exchange.d.b(exchange.c, e3);
                                                exchange.a(e3);
                                                throw e3;
                                            }
                                        } else {
                                            Response.a aVar4 = new Response.a(a5);
                                            aVar4.g = Util.c;
                                            response = aVar4.a();
                                        }
                                        if (Indent.a("close", response.c.a("Connection"), true) || Indent.a("close", Response.a(response, "Connection", null, 2), true)) {
                                            RealConnection a10 = exchange.f2940f.a();
                                            if (a10 != null) {
                                                a10.c();
                                            } else {
                                                Intrinsics.a();
                                                throw null;
                                            }
                                        }
                                        if (i2 == 204 || i2 == 205) {
                                            ResponseBody responseBody = response.f2902i;
                                            if ((responseBody != null ? responseBody.a() : -1) > 0) {
                                                StringBuilder a11 = outline.a("HTTP ", i2, " had non-zero Content-Length: ");
                                                ResponseBody responseBody2 = response.f2902i;
                                                if (responseBody2 != null) {
                                                    l2 = Long.valueOf(responseBody2.a());
                                                }
                                                a11.append(l2);
                                                throw new ProtocolException(a11.toString());
                                            }
                                        }
                                        return response;
                                    } else {
                                        Intrinsics.a("call");
                                        throw null;
                                    }
                                } else {
                                    Intrinsics.a();
                                    throw null;
                                }
                            } catch (IOException e4) {
                                exchange.d.a(exchange.c, e4);
                                exchange.a(e4);
                                throw e4;
                            }
                        } else {
                            Intrinsics.a("call");
                            throw null;
                        }
                    } else {
                        Intrinsics.a("call");
                        throw null;
                    }
                } catch (IOException e5) {
                    exchange.d.a(exchange.c, e5);
                    exchange.a(e5);
                    throw e5;
                }
            } else {
                Intrinsics.a();
                throw null;
            }
        } else {
            Intrinsics.a("chain");
            throw null;
        }
    }
}
