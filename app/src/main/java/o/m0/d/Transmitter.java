package o.m0.d;

import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.net.Socket;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;
import n.AssertionsJVM;
import n.n.c.Intrinsics;
import o.Call;
import o.EventListener;
import o.Interceptor;
import o.OkHttpClient;
import o.Request;
import o.m0.Util;
import o.m0.e.ExchangeCode;
import o.m0.i.Platform;
import okhttp3.internal.connection.RouteException;
import p.AsyncTimeout;

/* compiled from: Transmitter.kt */
public final class Transmitter {
    public final RealConnectionPool a;
    public final EventListener b;
    public final b c;
    public Object d;

    /* renamed from: e  reason: collision with root package name */
    public Request f2965e;

    /* renamed from: f  reason: collision with root package name */
    public ExchangeFinder f2966f;
    public RealConnection g;
    public Exchange h;

    /* renamed from: i  reason: collision with root package name */
    public boolean f2967i;

    /* renamed from: j  reason: collision with root package name */
    public boolean f2968j;

    /* renamed from: k  reason: collision with root package name */
    public boolean f2969k;

    /* renamed from: l  reason: collision with root package name */
    public boolean f2970l;

    /* renamed from: m  reason: collision with root package name */
    public boolean f2971m;

    /* renamed from: n  reason: collision with root package name */
    public final OkHttpClient f2972n;

    /* renamed from: o  reason: collision with root package name */
    public final Call f2973o;

    /* compiled from: Transmitter.kt */
    public static final class a extends WeakReference<l> {
        public final Object a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Transmitter transmitter, Object obj) {
            super(transmitter);
            if (transmitter != null) {
                this.a = obj;
                return;
            }
            Intrinsics.a("referent");
            throw null;
        }
    }

    /* compiled from: Transmitter.kt */
    public static final class b extends AsyncTimeout {

        /* renamed from: l  reason: collision with root package name */
        public final /* synthetic */ Transmitter f2974l;

        public b(Transmitter transmitter) {
            this.f2974l = transmitter;
        }

        public void h() {
            this.f2974l.b();
        }
    }

    public Transmitter(OkHttpClient okHttpClient, Call call) {
        if (okHttpClient == null) {
            Intrinsics.a("client");
            throw null;
        } else if (call != null) {
            this.f2972n = okHttpClient;
            this.f2973o = call;
            this.a = okHttpClient.c.a;
            this.b = okHttpClient.f2867f.a(call);
            b bVar = new b(this);
            bVar.a((long) this.f2972n.y, TimeUnit.MILLISECONDS);
            this.c = bVar;
        } else {
            Intrinsics.a("call");
            throw null;
        }
    }

    public final void a() {
        Platform.a aVar = Platform.c;
        this.d = Platform.a.a("response.body().close()");
        EventListener eventListener = this.b;
        Call call = this.f2973o;
        if (eventListener == null) {
            throw null;
        } else if (call == null) {
            Intrinsics.a("call");
            throw null;
        }
    }

    public final void b() {
        Exchange exchange;
        RealConnection realConnection;
        Socket socket;
        synchronized (this.a) {
            this.f2969k = true;
            exchange = this.h;
            ExchangeFinder exchangeFinder = this.f2966f;
            if (exchangeFinder != null) {
                boolean holdsLock = Thread.holdsLock(exchangeFinder.g);
                if (AssertionsJVM.a) {
                    if (!holdsLock) {
                        throw new AssertionError("Assertion failed");
                    }
                }
                realConnection = exchangeFinder.c;
                if (realConnection != null) {
                }
            }
            realConnection = this.g;
        }
        if (exchange != null) {
            exchange.f2940f.cancel();
        } else if (realConnection != null && (socket = realConnection.b) != null) {
            Util.a(socket);
        }
    }

    public final void c() {
        synchronized (this.a) {
            if (!this.f2971m) {
                this.h = null;
            } else {
                throw new IllegalStateException("Check failed.".toString());
            }
        }
    }

    public final boolean d() {
        boolean z;
        synchronized (this.a) {
            z = this.h != null;
        }
        return z;
    }

    public final boolean e() {
        boolean z;
        synchronized (this.a) {
            z = this.f2969k;
        }
        return z;
    }

    public final Socket f() {
        boolean holdsLock = Thread.holdsLock(this.a);
        if (!AssertionsJVM.a || holdsLock) {
            RealConnection realConnection = this.g;
            if (realConnection != null) {
                Iterator<Reference<l>> it = realConnection.f2956n.iterator();
                boolean z = false;
                int i2 = 0;
                while (true) {
                    if (!it.hasNext()) {
                        i2 = -1;
                        break;
                    } else if (Intrinsics.a((Transmitter) it.next().get(), this)) {
                        break;
                    } else {
                        i2++;
                    }
                }
                if (i2 != -1) {
                    RealConnection realConnection2 = this.g;
                    if (realConnection2 != null) {
                        realConnection2.f2956n.remove(i2);
                        this.g = null;
                        if (realConnection2.f2956n.isEmpty()) {
                            realConnection2.f2957o = System.nanoTime();
                            RealConnectionPool realConnectionPool = this.a;
                            if (realConnectionPool != null) {
                                boolean holdsLock2 = Thread.holdsLock(realConnectionPool);
                                if (!AssertionsJVM.a || holdsLock2) {
                                    if (realConnection2.f2951i || realConnectionPool.f2962f == 0) {
                                        realConnectionPool.c.remove(realConnection2);
                                        z = true;
                                    } else {
                                        realConnectionPool.notifyAll();
                                    }
                                    if (z) {
                                        return realConnection2.d();
                                    }
                                } else {
                                    throw new AssertionError("Assertion failed");
                                }
                            } else {
                                throw null;
                            }
                        }
                        return null;
                    }
                    Intrinsics.a();
                    throw null;
                }
                throw new IllegalStateException("Check failed.".toString());
            }
            Intrinsics.a();
            throw null;
        }
        throw new AssertionError("Assertion failed");
    }

    public final Exchange a(Interceptor.a aVar, boolean z) {
        if (aVar != null) {
            synchronized (this.a) {
                boolean z2 = true;
                if (!this.f2971m) {
                    if (this.h != null) {
                        z2 = false;
                    }
                    if (!z2) {
                        throw new IllegalStateException("cannot make a new request because the previous response is still open: please call response.close()".toString());
                    }
                } else {
                    throw new IllegalStateException("released".toString());
                }
            }
            ExchangeFinder exchangeFinder = this.f2966f;
            if (exchangeFinder != null) {
                OkHttpClient okHttpClient = this.f2972n;
                if (exchangeFinder == null) {
                    throw null;
                } else if (okHttpClient != null) {
                    try {
                        ExchangeCode a2 = exchangeFinder.a(aVar.c(), aVar.d(), aVar.b(), okHttpClient.C, okHttpClient.g, z).a(okHttpClient, aVar);
                        Call call = this.f2973o;
                        EventListener eventListener = this.b;
                        ExchangeFinder exchangeFinder2 = this.f2966f;
                        if (exchangeFinder2 != null) {
                            Exchange exchange = new Exchange(this, call, eventListener, exchangeFinder2, a2);
                            synchronized (this.a) {
                                this.h = exchange;
                                this.f2967i = false;
                                this.f2968j = false;
                            }
                            return exchange;
                        }
                        Intrinsics.a();
                        throw null;
                    } catch (RouteException e2) {
                        exchangeFinder.d();
                        throw e2;
                    } catch (IOException e3) {
                        exchangeFinder.d();
                        throw new RouteException(e3);
                    }
                } else {
                    Intrinsics.a("client");
                    throw null;
                }
            } else {
                Intrinsics.a();
                throw null;
            }
        } else {
            Intrinsics.a("chain");
            throw null;
        }
    }

    public final void a(RealConnection realConnection) {
        if (realConnection != null) {
            boolean holdsLock = Thread.holdsLock(this.a);
            if (!AssertionsJVM.a || holdsLock) {
                if (this.g == null) {
                    this.g = realConnection;
                    realConnection.f2956n.add(new a(this, this.d));
                    return;
                }
                throw new IllegalStateException("Check failed.".toString());
            }
            throw new AssertionError("Assertion failed");
        }
        Intrinsics.a("connection");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: o.m0.d.Transmitter.a(java.io.IOException, boolean):E
     arg types: [E, int]
     candidates:
      o.m0.d.Transmitter.a(o.Interceptor$a, boolean):o.m0.d.Exchange
      o.m0.d.Transmitter.a(java.io.IOException, boolean):E */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x004b, code lost:
        if (r2 == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:?, code lost:
        return a((java.io.IOException) r7, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
        return r7;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final <E extends java.io.IOException> E a(o.m0.d.c r4, boolean r5, boolean r6, E r7) {
        /*
            r3 = this;
            r0 = 0
            if (r4 == 0) goto L_0x0055
            o.m0.d.RealConnectionPool r1 = r3.a
            monitor-enter(r1)
            o.m0.d.Exchange r2 = r3.h     // Catch:{ all -> 0x0052 }
            boolean r4 = n.n.c.Intrinsics.a(r4, r2)     // Catch:{ all -> 0x0052 }
            r2 = 1
            r4 = r4 ^ r2
            if (r4 == 0) goto L_0x0012
            monitor-exit(r1)
            return r7
        L_0x0012:
            r4 = 0
            if (r5 == 0) goto L_0x001b
            boolean r5 = r3.f2967i     // Catch:{ all -> 0x0052 }
            r5 = r5 ^ r2
            r3.f2967i = r2     // Catch:{ all -> 0x0052 }
            goto L_0x001c
        L_0x001b:
            r5 = 0
        L_0x001c:
            if (r6 == 0) goto L_0x0025
            boolean r6 = r3.f2968j     // Catch:{ all -> 0x0052 }
            if (r6 != 0) goto L_0x0023
            r5 = 1
        L_0x0023:
            r3.f2968j = r2     // Catch:{ all -> 0x0052 }
        L_0x0025:
            boolean r6 = r3.f2967i     // Catch:{ all -> 0x0052 }
            if (r6 == 0) goto L_0x0049
            boolean r6 = r3.f2968j     // Catch:{ all -> 0x0052 }
            if (r6 == 0) goto L_0x0049
            if (r5 == 0) goto L_0x0049
            o.m0.d.Exchange r5 = r3.h     // Catch:{ all -> 0x0052 }
            if (r5 == 0) goto L_0x0045
            o.m0.d.RealConnection r5 = r5.a()     // Catch:{ all -> 0x0052 }
            if (r5 == 0) goto L_0x0041
            int r6 = r5.f2953k     // Catch:{ all -> 0x0052 }
            int r6 = r6 + r2
            r5.f2953k = r6     // Catch:{ all -> 0x0052 }
            r3.h = r0     // Catch:{ all -> 0x0052 }
            goto L_0x004a
        L_0x0041:
            n.n.c.Intrinsics.a()     // Catch:{ all -> 0x0052 }
            throw r0
        L_0x0045:
            n.n.c.Intrinsics.a()     // Catch:{ all -> 0x0052 }
            throw r0
        L_0x0049:
            r2 = 0
        L_0x004a:
            monitor-exit(r1)
            if (r2 == 0) goto L_0x0051
            java.io.IOException r7 = r3.a(r7, r4)
        L_0x0051:
            return r7
        L_0x0052:
            r4 = move-exception
            monitor-exit(r1)
            throw r4
        L_0x0055:
            java.lang.String r4 = "exchange"
            n.n.c.Intrinsics.a(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: o.m0.d.Transmitter.a(o.m0.d.Exchange, boolean, boolean, java.io.IOException):java.io.IOException");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: o.m0.d.Transmitter.a(java.io.IOException, boolean):E
     arg types: [java.io.IOException, int]
     candidates:
      o.m0.d.Transmitter.a(o.Interceptor$a, boolean):o.m0.d.Exchange
      o.m0.d.Transmitter.a(java.io.IOException, boolean):E */
    public final IOException a(IOException iOException) {
        synchronized (this.a) {
            this.f2971m = true;
        }
        return a(iOException, false);
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0014 A[Catch:{ all -> 0x000e }] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x00a3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final <E extends java.io.IOException> E a(E r7, boolean r8) {
        /*
            r6 = this;
            o.m0.d.RealConnectionPool r0 = r6.a
            monitor-enter(r0)
            r1 = 1
            r2 = 0
            if (r8 == 0) goto L_0x0011
            o.m0.d.Exchange r3 = r6.h     // Catch:{ all -> 0x000e }
            if (r3 != 0) goto L_0x000c
            goto L_0x0011
        L_0x000c:
            r3 = 0
            goto L_0x0012
        L_0x000e:
            r7 = move-exception
            goto L_0x00af
        L_0x0011:
            r3 = 1
        L_0x0012:
            if (r3 == 0) goto L_0x00a3
            o.m0.d.RealConnection r3 = r6.g     // Catch:{ all -> 0x000e }
            o.m0.d.RealConnection r4 = r6.g     // Catch:{ all -> 0x000e }
            r5 = 0
            if (r4 == 0) goto L_0x002a
            o.m0.d.Exchange r4 = r6.h     // Catch:{ all -> 0x000e }
            if (r4 != 0) goto L_0x002a
            if (r8 != 0) goto L_0x0025
            boolean r8 = r6.f2971m     // Catch:{ all -> 0x000e }
            if (r8 == 0) goto L_0x002a
        L_0x0025:
            java.net.Socket r8 = r6.f()     // Catch:{ all -> 0x000e }
            goto L_0x002b
        L_0x002a:
            r8 = r5
        L_0x002b:
            o.m0.d.RealConnection r4 = r6.g     // Catch:{ all -> 0x000e }
            if (r4 == 0) goto L_0x0030
            r3 = r5
        L_0x0030:
            boolean r4 = r6.f2971m     // Catch:{ all -> 0x000e }
            if (r4 == 0) goto L_0x003a
            o.m0.d.Exchange r4 = r6.h     // Catch:{ all -> 0x000e }
            if (r4 != 0) goto L_0x003a
            r4 = 1
            goto L_0x003b
        L_0x003a:
            r4 = 0
        L_0x003b:
            monitor-exit(r0)
            if (r8 == 0) goto L_0x0041
            o.m0.Util.a(r8)
        L_0x0041:
            if (r3 == 0) goto L_0x0059
            o.EventListener r8 = r6.b
            o.Call r0 = r6.f2973o
            if (r3 == 0) goto L_0x0055
            if (r8 == 0) goto L_0x0054
            if (r0 == 0) goto L_0x004e
            goto L_0x0059
        L_0x004e:
            java.lang.String r7 = "call"
            n.n.c.Intrinsics.a(r7)
            throw r5
        L_0x0054:
            throw r5
        L_0x0055:
            n.n.c.Intrinsics.a()
            throw r5
        L_0x0059:
            if (r4 == 0) goto L_0x00a2
            if (r7 == 0) goto L_0x005e
            goto L_0x005f
        L_0x005e:
            r1 = 0
        L_0x005f:
            boolean r8 = r6.f2970l
            if (r8 == 0) goto L_0x0064
            goto L_0x007a
        L_0x0064:
            o.m0.d.Transmitter$b r8 = r6.c
            boolean r8 = r8.g()
            if (r8 != 0) goto L_0x006d
            goto L_0x007a
        L_0x006d:
            java.io.InterruptedIOException r8 = new java.io.InterruptedIOException
            java.lang.String r0 = "timeout"
            r8.<init>(r0)
            if (r7 == 0) goto L_0x0079
            r8.initCause(r7)
        L_0x0079:
            r7 = r8
        L_0x007a:
            if (r1 == 0) goto L_0x0092
            o.EventListener r8 = r6.b
            o.Call r0 = r6.f2973o
            if (r7 == 0) goto L_0x008e
            if (r8 == 0) goto L_0x008d
            if (r0 == 0) goto L_0x0087
            goto L_0x00a2
        L_0x0087:
            java.lang.String r7 = "call"
            n.n.c.Intrinsics.a(r7)
            throw r5
        L_0x008d:
            throw r5
        L_0x008e:
            n.n.c.Intrinsics.a()
            throw r5
        L_0x0092:
            o.EventListener r8 = r6.b
            o.Call r0 = r6.f2973o
            if (r8 == 0) goto L_0x00a1
            if (r0 == 0) goto L_0x009b
            goto L_0x00a2
        L_0x009b:
            java.lang.String r7 = "call"
            n.n.c.Intrinsics.a(r7)
            throw r5
        L_0x00a1:
            throw r5
        L_0x00a2:
            return r7
        L_0x00a3:
            java.lang.String r7 = "cannot release connection while it is in use"
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException     // Catch:{ all -> 0x000e }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x000e }
            r8.<init>(r7)     // Catch:{ all -> 0x000e }
            throw r8     // Catch:{ all -> 0x000e }
        L_0x00af:
            monitor-exit(r0)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: o.m0.d.Transmitter.a(java.io.IOException, boolean):java.io.IOException");
    }
}
