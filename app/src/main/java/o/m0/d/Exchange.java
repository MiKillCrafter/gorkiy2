package o.m0.d;

import j.a.a.a.outline;
import java.io.IOException;
import java.net.ProtocolException;
import n.n.c.Intrinsics;
import o.Call;
import o.EventListener;
import o.Request;
import o.RequestBody;
import o.Response;
import o.m0.e.ExchangeCode;
import p.Buffer;
import p.ForwardingSink;
import p.ForwardingSource;
import p.Sink;
import p.x;
import p.z;

/* compiled from: Exchange.kt */
public final class Exchange {
    public boolean a;
    public final Transmitter b;
    public final Call c;
    public final EventListener d;

    /* renamed from: e  reason: collision with root package name */
    public final ExchangeFinder f2939e;

    /* renamed from: f  reason: collision with root package name */
    public final ExchangeCode f2940f;

    /* compiled from: Exchange.kt */
    public final class b extends ForwardingSource {
        public long c;
        public boolean d;

        /* renamed from: e  reason: collision with root package name */
        public boolean f2943e;

        /* renamed from: f  reason: collision with root package name */
        public final long f2944f;
        public final /* synthetic */ Exchange g;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(Exchange exchange, z zVar, long j2) {
            super(zVar);
            if (zVar != null) {
                this.g = exchange;
                this.f2944f = j2;
                if (j2 == 0) {
                    a(null);
                    return;
                }
                return;
            }
            Intrinsics.a("delegate");
            throw null;
        }

        public final <E extends IOException> E a(E e2) {
            if (this.d) {
                return e2;
            }
            this.d = true;
            return this.g.a(this.c, true, false, e2);
        }

        public long b(Buffer buffer, long j2) {
            if (buffer == null) {
                Intrinsics.a("sink");
                throw null;
            } else if (!this.f2943e) {
                try {
                    long b = super.b.b(buffer, j2);
                    if (b == -1) {
                        a(null);
                        return -1;
                    }
                    long j3 = this.c + b;
                    if (this.f2944f != -1) {
                        if (j3 > this.f2944f) {
                            throw new ProtocolException("expected " + this.f2944f + " bytes but received " + j3);
                        }
                    }
                    this.c = j3;
                    if (j3 == this.f2944f) {
                        a(null);
                    }
                    return b;
                } catch (IOException e2) {
                    throw a(e2);
                }
            } else {
                throw new IllegalStateException("closed".toString());
            }
        }

        public void close() {
            if (!this.f2943e) {
                this.f2943e = true;
                try {
                    super.close();
                    a(null);
                } catch (IOException e2) {
                    throw a(e2);
                }
            }
        }
    }

    public Exchange(Transmitter transmitter, Call call, EventListener eventListener, ExchangeFinder exchangeFinder, ExchangeCode exchangeCode) {
        if (transmitter == null) {
            Intrinsics.a("transmitter");
            throw null;
        } else if (call == null) {
            Intrinsics.a("call");
            throw null;
        } else if (eventListener == null) {
            Intrinsics.a("eventListener");
            throw null;
        } else if (exchangeFinder == null) {
            Intrinsics.a("finder");
            throw null;
        } else if (exchangeCode != null) {
            this.b = transmitter;
            this.c = call;
            this.d = eventListener;
            this.f2939e = exchangeFinder;
            this.f2940f = exchangeCode;
        } else {
            Intrinsics.a("codec");
            throw null;
        }
    }

    public final RealConnection a() {
        return this.f2940f.a();
    }

    public final Sink a(Request request, boolean z) {
        if (request != null) {
            this.a = z;
            RequestBody requestBody = request.f2896e;
            if (requestBody != null) {
                long a2 = requestBody.a();
                EventListener eventListener = this.d;
                Call call = this.c;
                if (eventListener == null) {
                    throw null;
                } else if (call != null) {
                    return new a(this, this.f2940f.a(request, a2), a2);
                } else {
                    Intrinsics.a("call");
                    throw null;
                }
            } else {
                Intrinsics.a();
                throw null;
            }
        } else {
            Intrinsics.a("request");
            throw null;
        }
    }

    /* compiled from: Exchange.kt */
    public final class a extends ForwardingSink {
        public boolean c;
        public long d;

        /* renamed from: e  reason: collision with root package name */
        public boolean f2941e;

        /* renamed from: f  reason: collision with root package name */
        public final long f2942f;
        public final /* synthetic */ Exchange g;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Exchange exchange, x xVar, long j2) {
            super(xVar);
            if (xVar != null) {
                this.g = exchange;
                this.f2942f = j2;
                return;
            }
            Intrinsics.a("delegate");
            throw null;
        }

        public void a(Buffer buffer, long j2) {
            if (buffer == null) {
                Intrinsics.a("source");
                throw null;
            } else if (!this.f2941e) {
                long j3 = this.f2942f;
                if (j3 == -1 || this.d + j2 <= j3) {
                    try {
                        super.a(buffer, j2);
                        this.d += j2;
                    } catch (IOException e2) {
                        throw a(e2);
                    }
                } else {
                    StringBuilder a = outline.a("expected ");
                    a.append(this.f2942f);
                    a.append(" bytes but received ");
                    a.append(this.d + j2);
                    throw new ProtocolException(a.toString());
                }
            } else {
                throw new IllegalStateException("closed".toString());
            }
        }

        public void close() {
            if (!this.f2941e) {
                this.f2941e = true;
                long j2 = this.f2942f;
                if (j2 == -1 || this.d == j2) {
                    try {
                        super.close();
                        a(null);
                    } catch (IOException e2) {
                        throw a(e2);
                    }
                } else {
                    throw new ProtocolException("unexpected end of stream");
                }
            }
        }

        public void flush() {
            try {
                super.flush();
            } catch (IOException e2) {
                throw a(e2);
            }
        }

        public final <E extends IOException> E a(E e2) {
            if (this.c) {
                return e2;
            }
            this.c = true;
            return this.g.a(this.d, false, true, e2);
        }
    }

    public final Response.a a(boolean z) {
        try {
            Response.a a2 = this.f2940f.a(z);
            if (a2 != null) {
                a2.f2915m = this;
            }
            return a2;
        } catch (IOException e2) {
            this.d.b(this.c, e2);
            a(e2);
            throw e2;
        }
    }

    public final void a(IOException iOException) {
        this.f2939e.d();
        RealConnection a2 = this.f2940f.a();
        if (a2 != null) {
            a2.a(iOException);
        } else {
            Intrinsics.a();
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: o.EventListener.a(o.Call, java.io.IOException):void
     arg types: [o.Call, E]
     candidates:
      o.EventListener.a(o.Call, o.Connection):void
      o.EventListener.a(o.Call, java.io.IOException):void */
    public final <E extends IOException> E a(long j2, boolean z, boolean z2, E e2) {
        if (e2 != null) {
            a(e2);
        }
        if (z2) {
            if (e2 != null) {
                this.d.a(this.c, (IOException) e2);
            } else {
                EventListener eventListener = this.d;
                Call call = this.c;
                if (eventListener == null) {
                    throw null;
                } else if (call == null) {
                    Intrinsics.a("call");
                    throw null;
                }
            }
        }
        if (z) {
            if (e2 != null) {
                this.d.b(this.c, e2);
            } else {
                EventListener eventListener2 = this.d;
                Call call2 = this.c;
                if (eventListener2 == null) {
                    throw null;
                } else if (call2 == null) {
                    Intrinsics.a("call");
                    throw null;
                }
            }
        }
        return this.b.a(this, z2, z, e2);
    }
}
