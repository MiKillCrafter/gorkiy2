package o.m0.d;

import java.util.LinkedHashSet;
import java.util.Set;
import n.n.c.Intrinsics;
import o.Route;
import o.k0;

/* compiled from: RouteDatabase.kt */
public final class RouteDatabase {
    public final Set<k0> a = new LinkedHashSet();

    public final synchronized void a(Route route) {
        if (route != null) {
            this.a.remove(route);
        } else {
            Intrinsics.a("route");
            throw null;
        }
    }

    public final synchronized void b(Route route) {
        if (route != null) {
            this.a.add(route);
        } else {
            Intrinsics.a("failedRoute");
            throw null;
        }
    }

    public final synchronized boolean c(Route route) {
        if (route != null) {
        } else {
            Intrinsics.a("route");
            throw null;
        }
        return this.a.contains(route);
    }
}
