package o.m0.d;

import n.n.c.Intrinsics;
import o.Interceptor;
import o.Request;
import o.Response;
import o.m0.e.RealInterceptorChain;

/* compiled from: ConnectInterceptor.kt */
public final class ConnectInterceptor implements Interceptor {
    public static final ConnectInterceptor b = new ConnectInterceptor();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
    public Response a(Interceptor.a aVar) {
        if (aVar != null) {
            RealInterceptorChain realInterceptorChain = (RealInterceptorChain) aVar;
            Request request = realInterceptorChain.f2976f;
            Transmitter transmitter = realInterceptorChain.c;
            return realInterceptorChain.a(request, transmitter, transmitter.a(aVar, !Intrinsics.a((Object) request.c, (Object) "GET")));
        }
        Intrinsics.a("chain");
        throw null;
    }
}
