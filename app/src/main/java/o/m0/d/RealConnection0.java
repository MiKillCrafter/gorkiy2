package o.m0.d;

import java.security.cert.Certificate;
import java.util.List;
import n.n.b.Functions;
import n.n.c.Intrinsics;
import n.n.c.j;
import o.Address;
import o.CertificatePinner;
import o.Handshake;
import o.m0.k.CertificateChainCleaner;

/* compiled from: RealConnection.kt */
public final class RealConnection0 extends j implements Functions<List<? extends Certificate>> {
    public final /* synthetic */ CertificatePinner c;
    public final /* synthetic */ Handshake d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ Address f2960e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RealConnection0(CertificatePinner certificatePinner, Handshake handshake, Address address) {
        super(0);
        this.c = certificatePinner;
        this.d = handshake;
        this.f2960e = address;
    }

    public Object b() {
        CertificateChainCleaner certificateChainCleaner = this.c.b;
        if (certificateChainCleaner != null) {
            return certificateChainCleaner.a(this.d.a(), this.f2960e.a.f2851e);
        }
        Intrinsics.a();
        throw null;
    }
}
