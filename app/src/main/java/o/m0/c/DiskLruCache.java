package o.m0.c;

import j.a.a.a.outline;
import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.Flushable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.Executor;
import kotlin.TypeCastException;
import n.AssertionsJVM;
import n.Unit;
import n.g;
import n.i.Collections;
import n.n.b.Functions0;
import n.n.c.Intrinsics;
import n.n.c.j;
import n.r.Indent;
import n.r.Regex;
import o.m0.Util;
import o.m0.c.e;
import o.m0.h.FileSystem;
import o.m0.i.Platform;
import p.BufferedSink;
import p.Sink;
import p.Source;
import p.z;

/* compiled from: DiskLruCache.kt */
public final class DiskLruCache implements Closeable, Flushable {
    public static final DiskLruCache A = null;
    public static final Regex v = new Regex("[a-z0-9_-]{1,120}");
    public static final String w = w;
    public static final String x = x;
    public static final String y = y;
    public static final String z = z;
    public long b;
    public final File c;
    public final File d;

    /* renamed from: e  reason: collision with root package name */
    public final File f2922e;

    /* renamed from: f  reason: collision with root package name */
    public long f2923f;
    public BufferedSink g;
    public final LinkedHashMap<String, e.b> h;

    /* renamed from: i  reason: collision with root package name */
    public int f2924i;

    /* renamed from: j  reason: collision with root package name */
    public boolean f2925j;

    /* renamed from: k  reason: collision with root package name */
    public boolean f2926k;

    /* renamed from: l  reason: collision with root package name */
    public boolean f2927l;

    /* renamed from: m  reason: collision with root package name */
    public boolean f2928m;

    /* renamed from: n  reason: collision with root package name */
    public boolean f2929n;

    /* renamed from: o  reason: collision with root package name */
    public long f2930o;

    /* renamed from: p  reason: collision with root package name */
    public final Runnable f2931p;

    /* renamed from: q  reason: collision with root package name */
    public final FileSystem f2932q;

    /* renamed from: r  reason: collision with root package name */
    public final File f2933r;

    /* renamed from: s  reason: collision with root package name */
    public final int f2934s;

    /* renamed from: t  reason: collision with root package name */
    public final int f2935t;
    public final Executor u;

    /* compiled from: DiskLruCache.kt */
    public final class c implements Closeable {
        public final String b;
        public final long c;
        public final List<z> d;

        /* renamed from: e  reason: collision with root package name */
        public final /* synthetic */ DiskLruCache f2938e;

        public c(DiskLruCache diskLruCache, String str, long j2, List<? extends z> list, long[] jArr) {
            if (str == null) {
                Intrinsics.a("key");
                throw null;
            } else if (list == null) {
                Intrinsics.a("sources");
                throw null;
            } else if (jArr != null) {
                this.f2938e = diskLruCache;
                this.b = str;
                this.c = j2;
                this.d = list;
            } else {
                Intrinsics.a("lengths");
                throw null;
            }
        }

        public void close() {
            Iterator<z> it = this.d.iterator();
            while (it.hasNext()) {
                Util.a(it.next());
            }
        }
    }

    /* compiled from: DiskLruCache.kt */
    public static final class d implements Runnable {
        public final /* synthetic */ DiskLruCache b;

        public d(DiskLruCache diskLruCache) {
            this.b = diskLruCache;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
            r4.b.f2929n = true;
            r4.b.g = n.i.Collections.a((p.Sink) new p.Okio());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x0042, code lost:
            return;
         */
        /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
        /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x0017 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:16:0x002e */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void run() {
            /*
                r4 = this;
                o.m0.c.DiskLruCache r0 = r4.b
                monitor-enter(r0)
                o.m0.c.DiskLruCache r1 = r4.b     // Catch:{ all -> 0x0043 }
                boolean r1 = r1.f2926k     // Catch:{ all -> 0x0043 }
                if (r1 == 0) goto L_0x0041
                o.m0.c.DiskLruCache r1 = r4.b     // Catch:{ all -> 0x0043 }
                boolean r1 = r1.f2927l     // Catch:{ all -> 0x0043 }
                if (r1 == 0) goto L_0x0010
                goto L_0x0041
            L_0x0010:
                r1 = 1
                o.m0.c.DiskLruCache r2 = r4.b     // Catch:{ IOException -> 0x0017 }
                r2.o()     // Catch:{ IOException -> 0x0017 }
                goto L_0x001b
            L_0x0017:
                o.m0.c.DiskLruCache r2 = r4.b     // Catch:{ all -> 0x0043 }
                r2.f2928m = r1     // Catch:{ all -> 0x0043 }
            L_0x001b:
                o.m0.c.DiskLruCache r2 = r4.b     // Catch:{ IOException -> 0x002e }
                boolean r2 = r2.g()     // Catch:{ IOException -> 0x002e }
                if (r2 == 0) goto L_0x003f
                o.m0.c.DiskLruCache r2 = r4.b     // Catch:{ IOException -> 0x002e }
                r2.n()     // Catch:{ IOException -> 0x002e }
                o.m0.c.DiskLruCache r2 = r4.b     // Catch:{ IOException -> 0x002e }
                r3 = 0
                r2.f2924i = r3     // Catch:{ IOException -> 0x002e }
                goto L_0x003f
            L_0x002e:
                o.m0.c.DiskLruCache r2 = r4.b     // Catch:{ all -> 0x0043 }
                r2.f2929n = r1     // Catch:{ all -> 0x0043 }
                o.m0.c.DiskLruCache r1 = r4.b     // Catch:{ all -> 0x0043 }
                p.Okio r2 = new p.Okio     // Catch:{ all -> 0x0043 }
                r2.<init>()     // Catch:{ all -> 0x0043 }
                p.BufferedSink r2 = n.i.Collections.a(r2)     // Catch:{ all -> 0x0043 }
                r1.g = r2     // Catch:{ all -> 0x0043 }
            L_0x003f:
                monitor-exit(r0)
                return
            L_0x0041:
                monitor-exit(r0)
                return
            L_0x0043:
                r1 = move-exception
                monitor-exit(r0)
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: o.m0.c.DiskLruCache.d.run():void");
        }
    }

    /* compiled from: DiskLruCache.kt */
    public static final class e extends j implements Functions0<IOException, g> {
        public final /* synthetic */ DiskLruCache c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(DiskLruCache diskLruCache) {
            super(1);
            this.c = diskLruCache;
        }

        public Object a(Object obj) {
            if (((IOException) obj) != null) {
                boolean holdsLock = Thread.holdsLock(this.c);
                if (!AssertionsJVM.a || holdsLock) {
                    this.c.f2925j = true;
                    return Unit.a;
                }
                throw new AssertionError("Assertion failed");
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    public DiskLruCache(FileSystem fileSystem, File file, int i2, int i3, long j2, Executor executor) {
        if (fileSystem == null) {
            Intrinsics.a("fileSystem");
            throw null;
        } else if (file == null) {
            Intrinsics.a("directory");
            throw null;
        } else if (executor != null) {
            this.f2932q = fileSystem;
            this.f2933r = file;
            this.f2934s = i2;
            this.f2935t = i3;
            this.u = executor;
            this.b = j2;
            this.h = new LinkedHashMap<>(0, 0.75f, true);
            this.f2931p = new d(this);
            this.c = new File(this.f2933r, "journal");
            this.d = new File(this.f2933r, "journal.tmp");
            this.f2922e = new File(this.f2933r, "journal.bkp");
        } else {
            Intrinsics.a("executor");
            throw null;
        }
    }

    public static /* synthetic */ a a(DiskLruCache diskLruCache, String str, long j2, int i2) {
        if ((i2 & 2) != 0) {
            j2 = -1;
        }
        return diskLruCache.a(str, j2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [o.m0.c.DiskLruCache$b, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0057, code lost:
        return r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized o.m0.c.DiskLruCache.c b(java.lang.String r4) {
        /*
            r3 = this;
            monitor-enter(r3)
            java.lang.String r0 = "key"
            r1 = 0
            if (r4 == 0) goto L_0x0060
            r3.f()     // Catch:{ all -> 0x0064 }
            r3.a()     // Catch:{ all -> 0x0064 }
            r3.e(r4)     // Catch:{ all -> 0x0064 }
            java.util.LinkedHashMap<java.lang.String, o.m0.c.e$b> r0 = r3.h     // Catch:{ all -> 0x0064 }
            java.lang.Object r0 = r0.get(r4)     // Catch:{ all -> 0x0064 }
            o.m0.c.DiskLruCache$b r0 = (o.m0.c.DiskLruCache.b) r0     // Catch:{ all -> 0x0064 }
            if (r0 == 0) goto L_0x005e
            java.lang.String r2 = "lruEntries[key] ?: return null"
            n.n.c.Intrinsics.a(r0, r2)     // Catch:{ all -> 0x0064 }
            boolean r2 = r0.d     // Catch:{ all -> 0x0064 }
            if (r2 != 0) goto L_0x0024
            monitor-exit(r3)
            return r1
        L_0x0024:
            o.m0.c.DiskLruCache$c r0 = r0.a()     // Catch:{ all -> 0x0064 }
            if (r0 == 0) goto L_0x005c
            int r2 = r3.f2924i     // Catch:{ all -> 0x0064 }
            int r2 = r2 + 1
            r3.f2924i = r2     // Catch:{ all -> 0x0064 }
            p.BufferedSink r2 = r3.g     // Catch:{ all -> 0x0064 }
            if (r2 == 0) goto L_0x0058
            java.lang.String r1 = o.m0.c.DiskLruCache.z     // Catch:{ all -> 0x0064 }
            p.BufferedSink r1 = r2.a(r1)     // Catch:{ all -> 0x0064 }
            r2 = 32
            p.BufferedSink r1 = r1.writeByte(r2)     // Catch:{ all -> 0x0064 }
            p.BufferedSink r4 = r1.a(r4)     // Catch:{ all -> 0x0064 }
            r1 = 10
            r4.writeByte(r1)     // Catch:{ all -> 0x0064 }
            boolean r4 = r3.g()     // Catch:{ all -> 0x0064 }
            if (r4 == 0) goto L_0x0056
            java.util.concurrent.Executor r4 = r3.u     // Catch:{ all -> 0x0064 }
            java.lang.Runnable r1 = r3.f2931p     // Catch:{ all -> 0x0064 }
            r4.execute(r1)     // Catch:{ all -> 0x0064 }
        L_0x0056:
            monitor-exit(r3)
            return r0
        L_0x0058:
            n.n.c.Intrinsics.a()     // Catch:{ all -> 0x0064 }
            throw r1
        L_0x005c:
            monitor-exit(r3)
            return r1
        L_0x005e:
            monitor-exit(r3)
            return r1
        L_0x0060:
            n.n.c.Intrinsics.a(r0)     // Catch:{ all -> 0x0064 }
            throw r1
        L_0x0064:
            r4 = move-exception
            monitor-exit(r3)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: o.m0.c.DiskLruCache.b(java.lang.String):o.m0.c.DiskLruCache$c");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.CharSequence, char, int, boolean, int):int
     arg types: [java.lang.String, int, int, int, int]
     candidates:
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean, int):int
      n.r.Indent.a(java.lang.String, java.lang.String, java.lang.String, boolean, int):java.lang.String
      n.r.Indent.a(java.lang.CharSequence, char[], boolean, int, int):java.util.List
      n.r.Indent.a(java.lang.CharSequence, java.lang.String[], boolean, int, int):n.q.Sequence
      n.r.Indent.a(java.lang.String, java.lang.String, int, boolean, int):boolean
      n.r.Indent.a(java.lang.CharSequence, char, int, boolean, int):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.CharSequence, char[], boolean, int, int):java.util.List
     arg types: [java.lang.String, char[], int, int, int]
     candidates:
      n.r.Indent.a(java.lang.CharSequence, char, int, boolean, int):int
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean, int):int
      n.r.Indent.a(java.lang.String, java.lang.String, java.lang.String, boolean, int):java.lang.String
      n.r.Indent.a(java.lang.CharSequence, java.lang.String[], boolean, int, int):n.q.Sequence
      n.r.Indent.a(java.lang.String, java.lang.String, int, boolean, int):boolean
      n.r.Indent.a(java.lang.CharSequence, char[], boolean, int, int):java.util.List */
    public final void c(String str) {
        String str2;
        int a2 = Indent.a((CharSequence) str, ' ', 0, false, 6);
        if (a2 != -1) {
            int i2 = a2 + 1;
            int a3 = Indent.a((CharSequence) str, ' ', i2, false, 4);
            if (a3 == -1) {
                str2 = str.substring(i2);
                Intrinsics.a((Object) str2, "(this as java.lang.String).substring(startIndex)");
                if (a2 == y.length() && Indent.b(str, y, false, 2)) {
                    this.h.remove(str2);
                    return;
                }
            } else {
                str2 = str.substring(i2, a3);
                Intrinsics.a((Object) str2, "(this as java.lang.Strin…ing(startIndex, endIndex)");
            }
            b bVar = this.h.get(str2);
            if (bVar == null) {
                bVar = new b(this, str2);
                this.h.put(str2, bVar);
            }
            if (a3 != -1 && a2 == w.length() && Indent.b(str, w, false, 2)) {
                String substring = str.substring(a3 + 1);
                Intrinsics.a((Object) substring, "(this as java.lang.String).substring(startIndex)");
                List a4 = Indent.a((CharSequence) substring, new char[]{' '}, false, 0, 6);
                bVar.d = true;
                bVar.f2936e = null;
                if (a4 == null) {
                    Intrinsics.a("strings");
                    throw null;
                } else if (a4.size() == bVar.h.f2935t) {
                    try {
                        int size = a4.size();
                        for (int i3 = 0; i3 < size; i3++) {
                            bVar.a[i3] = Long.parseLong((String) a4.get(i3));
                        }
                    } catch (NumberFormatException unused) {
                        throw new IOException("unexpected journal line: " + a4);
                    }
                } else {
                    throw new IOException("unexpected journal line: " + a4);
                }
            } else if (a3 == -1 && a2 == x.length() && Indent.b(str, x, false, 2)) {
                bVar.f2936e = new a(this, bVar);
            } else if (a3 != -1 || a2 != z.length() || !Indent.b(str, z, false, 2)) {
                throw new IOException(outline.a("unexpected journal line: ", str));
            }
        } else {
            throw new IOException(outline.a("unexpected journal line: ", str));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Collection<o.m0.c.e$b>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public synchronized void close() {
        if (this.f2926k) {
            if (!this.f2927l) {
                Collection<e.b> values = this.h.values();
                Intrinsics.a((Object) values, "lruEntries.values");
                Object[] array = values.toArray(new b[0]);
                if (array != null) {
                    for (b bVar : (b[]) array) {
                        if (bVar.f2936e != null) {
                            a aVar = bVar.f2936e;
                            if (aVar != null) {
                                aVar.a();
                            } else {
                                Intrinsics.a();
                                throw null;
                            }
                        }
                    }
                    o();
                    BufferedSink bufferedSink = this.g;
                    if (bufferedSink != null) {
                        bufferedSink.close();
                        this.g = null;
                        this.f2927l = true;
                        return;
                    }
                    Intrinsics.a();
                    throw null;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
        this.f2927l = true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [o.m0.c.DiskLruCache$b, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002d, code lost:
        return true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized boolean d(java.lang.String r7) {
        /*
            r6 = this;
            monitor-enter(r6)
            java.lang.String r0 = "key"
            if (r7 == 0) goto L_0x0030
            r6.f()     // Catch:{ all -> 0x0035 }
            r6.a()     // Catch:{ all -> 0x0035 }
            r6.e(r7)     // Catch:{ all -> 0x0035 }
            java.util.LinkedHashMap<java.lang.String, o.m0.c.e$b> r0 = r6.h     // Catch:{ all -> 0x0035 }
            java.lang.Object r7 = r0.get(r7)     // Catch:{ all -> 0x0035 }
            o.m0.c.DiskLruCache$b r7 = (o.m0.c.DiskLruCache.b) r7     // Catch:{ all -> 0x0035 }
            r0 = 0
            if (r7 == 0) goto L_0x002e
            java.lang.String r1 = "lruEntries[key] ?: return false"
            n.n.c.Intrinsics.a(r7, r1)     // Catch:{ all -> 0x0035 }
            r6.a(r7)     // Catch:{ all -> 0x0035 }
            r7 = 1
            long r1 = r6.f2923f     // Catch:{ all -> 0x0035 }
            long r3 = r6.b     // Catch:{ all -> 0x0035 }
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 > 0) goto L_0x002c
            r6.f2928m = r0     // Catch:{ all -> 0x0035 }
        L_0x002c:
            monitor-exit(r6)
            return r7
        L_0x002e:
            monitor-exit(r6)
            return r0
        L_0x0030:
            n.n.c.Intrinsics.a(r0)     // Catch:{ all -> 0x0035 }
            r7 = 0
            throw r7
        L_0x0035:
            r7 = move-exception
            monitor-exit(r6)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: o.m0.c.DiskLruCache.d(java.lang.String):boolean");
    }

    public final void e(String str) {
        if (!v.a(str)) {
            throw new IllegalArgumentException(("keys must match regex [a-z0-9_-]{1,120}: \"" + str + '\"').toString());
        }
    }

    public final synchronized void f() {
        boolean holdsLock = Thread.holdsLock(this);
        if (AssertionsJVM.a) {
            if (!holdsLock) {
                throw new AssertionError("Assertion failed");
            }
        }
        if (!this.f2926k) {
            if (this.f2932q.f(this.f2922e)) {
                if (this.f2932q.f(this.c)) {
                    this.f2932q.a(this.f2922e);
                } else {
                    this.f2932q.a(this.f2922e, this.c);
                }
            }
            if (this.f2932q.f(this.c)) {
                try {
                    l();
                    i();
                    this.f2926k = true;
                    return;
                } catch (IOException e2) {
                    Platform.a aVar = Platform.c;
                    Platform platform = Platform.a;
                    platform.a(5, "DiskLruCache " + this.f2933r + " is corrupt: " + e2.getMessage() + ", removing", e2);
                    close();
                    this.f2932q.d(this.f2933r);
                    this.f2927l = false;
                } catch (Throwable th) {
                    this.f2927l = false;
                    throw th;
                }
            }
            n();
            this.f2926k = true;
        }
    }

    public synchronized void flush() {
        if (this.f2926k) {
            a();
            o();
            BufferedSink bufferedSink = this.g;
            if (bufferedSink != null) {
                bufferedSink.flush();
            } else {
                Intrinsics.a();
                throw null;
            }
        }
    }

    public final boolean g() {
        int i2 = this.f2924i;
        return i2 >= 2000 && i2 >= this.h.size();
    }

    public final BufferedSink h() {
        return Collections.a((Sink) new FaultHidingSink(this.f2932q.e(this.c), new e(this)));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [o.m0.c.DiskLruCache$b, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public final void i() {
        this.f2932q.a(this.d);
        Iterator<e.b> it = this.h.values().iterator();
        while (it.hasNext()) {
            b next = it.next();
            Intrinsics.a((Object) next, "i.next()");
            b bVar = next;
            int i2 = 0;
            if (bVar.f2936e == null) {
                int i3 = this.f2935t;
                while (i2 < i3) {
                    this.f2923f += bVar.a[i2];
                    i2++;
                }
            } else {
                bVar.f2936e = null;
                int i4 = this.f2935t;
                while (i2 < i4) {
                    this.f2932q.a(bVar.b.get(i2));
                    this.f2932q.a(bVar.c.get(i2));
                    i2++;
                }
                it.remove();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
     arg types: [p.BufferedSource, ?[OBJECT, ARRAY]]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(java.lang.Object, java.lang.String):T
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
     arg types: [p.BufferedSource, java.lang.Throwable]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(java.lang.Object, java.lang.String):T
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:18|19|(1:21)(1:22)|23|24) */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        r9.f2924i = r7 - r9.h.size();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0071, code lost:
        if (r1.j() == false) goto L_0x0073;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0073, code lost:
        n();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0077, code lost:
        r9.g = h();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x007d, code lost:
        n.i.Collections.a((java.io.Closeable) r1, (java.lang.Throwable) null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0081, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00b2, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00b3, code lost:
        n.i.Collections.a((java.io.Closeable) r1, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00b6, code lost:
        throw r2;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:18:0x0064 */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:25:0x0082=Splitter:B:25:0x0082, B:18:0x0064=Splitter:B:18:0x0064} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void l() {
        /*
            r9 = this;
            java.lang.String r0 = ", "
            o.m0.h.FileSystem r1 = r9.f2932q
            java.io.File r2 = r9.c
            p.Source r1 = r1.b(r2)
            p.BufferedSource r1 = n.i.Collections.a(r1)
            java.lang.String r2 = r1.e()     // Catch:{ all -> 0x00b0 }
            java.lang.String r3 = r1.e()     // Catch:{ all -> 0x00b0 }
            java.lang.String r4 = r1.e()     // Catch:{ all -> 0x00b0 }
            java.lang.String r5 = r1.e()     // Catch:{ all -> 0x00b0 }
            java.lang.String r6 = r1.e()     // Catch:{ all -> 0x00b0 }
            java.lang.String r7 = "libcore.io.DiskLruCache"
            boolean r7 = n.n.c.Intrinsics.a(r7, r2)     // Catch:{ all -> 0x00b0 }
            r8 = 1
            r7 = r7 ^ r8
            if (r7 != 0) goto L_0x0082
            java.lang.String r7 = "1"
            boolean r7 = n.n.c.Intrinsics.a(r7, r3)     // Catch:{ all -> 0x00b0 }
            r7 = r7 ^ r8
            if (r7 != 0) goto L_0x0082
            int r7 = r9.f2934s     // Catch:{ all -> 0x00b0 }
            java.lang.String r7 = java.lang.String.valueOf(r7)     // Catch:{ all -> 0x00b0 }
            boolean r4 = n.n.c.Intrinsics.a(r7, r4)     // Catch:{ all -> 0x00b0 }
            r4 = r4 ^ r8
            if (r4 != 0) goto L_0x0082
            int r4 = r9.f2935t     // Catch:{ all -> 0x00b0 }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ all -> 0x00b0 }
            boolean r4 = n.n.c.Intrinsics.a(r4, r5)     // Catch:{ all -> 0x00b0 }
            r4 = r4 ^ r8
            if (r4 != 0) goto L_0x0082
            int r4 = r6.length()     // Catch:{ all -> 0x00b0 }
            r7 = 0
            if (r4 <= 0) goto L_0x0057
            goto L_0x0058
        L_0x0057:
            r8 = 0
        L_0x0058:
            if (r8 != 0) goto L_0x0082
        L_0x005a:
            java.lang.String r0 = r1.e()     // Catch:{ EOFException -> 0x0064 }
            r9.c(r0)     // Catch:{ EOFException -> 0x0064 }
            int r7 = r7 + 1
            goto L_0x005a
        L_0x0064:
            java.util.LinkedHashMap<java.lang.String, o.m0.c.e$b> r0 = r9.h     // Catch:{ all -> 0x00b0 }
            int r0 = r0.size()     // Catch:{ all -> 0x00b0 }
            int r7 = r7 - r0
            r9.f2924i = r7     // Catch:{ all -> 0x00b0 }
            boolean r0 = r1.j()     // Catch:{ all -> 0x00b0 }
            if (r0 != 0) goto L_0x0077
            r9.n()     // Catch:{ all -> 0x00b0 }
            goto L_0x007d
        L_0x0077:
            p.BufferedSink r0 = r9.h()     // Catch:{ all -> 0x00b0 }
            r9.g = r0     // Catch:{ all -> 0x00b0 }
        L_0x007d:
            r0 = 0
            n.i.Collections.a(r1, r0)
            return
        L_0x0082:
            java.io.IOException r4 = new java.io.IOException     // Catch:{ all -> 0x00b0 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x00b0 }
            r7.<init>()     // Catch:{ all -> 0x00b0 }
            java.lang.String r8 = "unexpected journal header: ["
            r7.append(r8)     // Catch:{ all -> 0x00b0 }
            r7.append(r2)     // Catch:{ all -> 0x00b0 }
            r7.append(r0)     // Catch:{ all -> 0x00b0 }
            r7.append(r3)     // Catch:{ all -> 0x00b0 }
            r7.append(r0)     // Catch:{ all -> 0x00b0 }
            r7.append(r5)     // Catch:{ all -> 0x00b0 }
            r7.append(r0)     // Catch:{ all -> 0x00b0 }
            r7.append(r6)     // Catch:{ all -> 0x00b0 }
            r0 = 93
            r7.append(r0)     // Catch:{ all -> 0x00b0 }
            java.lang.String r0 = r7.toString()     // Catch:{ all -> 0x00b0 }
            r4.<init>(r0)     // Catch:{ all -> 0x00b0 }
            throw r4     // Catch:{ all -> 0x00b0 }
        L_0x00b0:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x00b2 }
        L_0x00b2:
            r2 = move-exception
            n.i.Collections.a(r1, r0)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: o.m0.c.DiskLruCache.l():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
     arg types: [p.BufferedSink, ?[OBJECT, ARRAY]]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(java.lang.Object, java.lang.String):T
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
     arg types: [p.BufferedSink, java.lang.Throwable]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(java.lang.Object, java.lang.String):T
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00b8, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        n.i.Collections.a((java.io.Closeable) r0, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00bc, code lost:
        throw r2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void n() {
        /*
            r7 = this;
            monitor-enter(r7)
            p.BufferedSink r0 = r7.g     // Catch:{ all -> 0x00bd }
            if (r0 == 0) goto L_0x0008
            r0.close()     // Catch:{ all -> 0x00bd }
        L_0x0008:
            o.m0.h.FileSystem r0 = r7.f2932q     // Catch:{ all -> 0x00bd }
            java.io.File r1 = r7.d     // Catch:{ all -> 0x00bd }
            p.Sink r0 = r0.c(r1)     // Catch:{ all -> 0x00bd }
            p.BufferedSink r0 = n.i.Collections.a(r0)     // Catch:{ all -> 0x00bd }
            r1 = 0
            java.lang.String r2 = "libcore.io.DiskLruCache"
            p.BufferedSink r2 = r0.a(r2)     // Catch:{ all -> 0x00b6 }
            r3 = 10
            r2.writeByte(r3)     // Catch:{ all -> 0x00b6 }
            java.lang.String r2 = "1"
            p.BufferedSink r2 = r0.a(r2)     // Catch:{ all -> 0x00b6 }
            r2.writeByte(r3)     // Catch:{ all -> 0x00b6 }
            int r2 = r7.f2934s     // Catch:{ all -> 0x00b6 }
            long r4 = (long) r2     // Catch:{ all -> 0x00b6 }
            p.BufferedSink r2 = r0.h(r4)     // Catch:{ all -> 0x00b6 }
            r2.writeByte(r3)     // Catch:{ all -> 0x00b6 }
            int r2 = r7.f2935t     // Catch:{ all -> 0x00b6 }
            long r4 = (long) r2     // Catch:{ all -> 0x00b6 }
            p.BufferedSink r2 = r0.h(r4)     // Catch:{ all -> 0x00b6 }
            r2.writeByte(r3)     // Catch:{ all -> 0x00b6 }
            r0.writeByte(r3)     // Catch:{ all -> 0x00b6 }
            java.util.LinkedHashMap<java.lang.String, o.m0.c.e$b> r2 = r7.h     // Catch:{ all -> 0x00b6 }
            java.util.Collection r2 = r2.values()     // Catch:{ all -> 0x00b6 }
            java.util.Iterator r2 = r2.iterator()     // Catch:{ all -> 0x00b6 }
        L_0x004a:
            boolean r4 = r2.hasNext()     // Catch:{ all -> 0x00b6 }
            if (r4 == 0) goto L_0x0083
            java.lang.Object r4 = r2.next()     // Catch:{ all -> 0x00b6 }
            o.m0.c.DiskLruCache$b r4 = (o.m0.c.DiskLruCache.b) r4     // Catch:{ all -> 0x00b6 }
            o.m0.c.DiskLruCache$a r5 = r4.f2936e     // Catch:{ all -> 0x00b6 }
            r6 = 32
            if (r5 == 0) goto L_0x006e
            java.lang.String r5 = o.m0.c.DiskLruCache.x     // Catch:{ all -> 0x00b6 }
            p.BufferedSink r5 = r0.a(r5)     // Catch:{ all -> 0x00b6 }
            r5.writeByte(r6)     // Catch:{ all -> 0x00b6 }
            java.lang.String r4 = r4.g     // Catch:{ all -> 0x00b6 }
            r0.a(r4)     // Catch:{ all -> 0x00b6 }
            r0.writeByte(r3)     // Catch:{ all -> 0x00b6 }
            goto L_0x004a
        L_0x006e:
            java.lang.String r5 = o.m0.c.DiskLruCache.w     // Catch:{ all -> 0x00b6 }
            p.BufferedSink r5 = r0.a(r5)     // Catch:{ all -> 0x00b6 }
            r5.writeByte(r6)     // Catch:{ all -> 0x00b6 }
            java.lang.String r5 = r4.g     // Catch:{ all -> 0x00b6 }
            r0.a(r5)     // Catch:{ all -> 0x00b6 }
            r4.a(r0)     // Catch:{ all -> 0x00b6 }
            r0.writeByte(r3)     // Catch:{ all -> 0x00b6 }
            goto L_0x004a
        L_0x0083:
            n.i.Collections.a(r0, r1)     // Catch:{ all -> 0x00bd }
            o.m0.h.FileSystem r0 = r7.f2932q     // Catch:{ all -> 0x00bd }
            java.io.File r1 = r7.c     // Catch:{ all -> 0x00bd }
            boolean r0 = r0.f(r1)     // Catch:{ all -> 0x00bd }
            if (r0 == 0) goto L_0x0099
            o.m0.h.FileSystem r0 = r7.f2932q     // Catch:{ all -> 0x00bd }
            java.io.File r1 = r7.c     // Catch:{ all -> 0x00bd }
            java.io.File r2 = r7.f2922e     // Catch:{ all -> 0x00bd }
            r0.a(r1, r2)     // Catch:{ all -> 0x00bd }
        L_0x0099:
            o.m0.h.FileSystem r0 = r7.f2932q     // Catch:{ all -> 0x00bd }
            java.io.File r1 = r7.d     // Catch:{ all -> 0x00bd }
            java.io.File r2 = r7.c     // Catch:{ all -> 0x00bd }
            r0.a(r1, r2)     // Catch:{ all -> 0x00bd }
            o.m0.h.FileSystem r0 = r7.f2932q     // Catch:{ all -> 0x00bd }
            java.io.File r1 = r7.f2922e     // Catch:{ all -> 0x00bd }
            r0.a(r1)     // Catch:{ all -> 0x00bd }
            p.BufferedSink r0 = r7.h()     // Catch:{ all -> 0x00bd }
            r7.g = r0     // Catch:{ all -> 0x00bd }
            r0 = 0
            r7.f2925j = r0     // Catch:{ all -> 0x00bd }
            r7.f2929n = r0     // Catch:{ all -> 0x00bd }
            monitor-exit(r7)
            return
        L_0x00b6:
            r1 = move-exception
            throw r1     // Catch:{ all -> 0x00b8 }
        L_0x00b8:
            r2 = move-exception
            n.i.Collections.a(r0, r1)     // Catch:{ all -> 0x00bd }
            throw r2     // Catch:{ all -> 0x00bd }
        L_0x00bd:
            r0 = move-exception
            monitor-exit(r7)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: o.m0.c.DiskLruCache.n():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [o.m0.c.DiskLruCache$b, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public final void o() {
        while (this.f2923f > this.b) {
            b next = this.h.values().iterator().next();
            Intrinsics.a((Object) next, "lruEntries.values.iterator().next()");
            a(next);
        }
        this.f2928m = false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0026, code lost:
        return null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized o.m0.c.DiskLruCache.a a(java.lang.String r6, long r7) {
        /*
            r5 = this;
            monitor-enter(r5)
            java.lang.String r0 = "key"
            r1 = 0
            if (r6 == 0) goto L_0x007e
            r5.f()     // Catch:{ all -> 0x0082 }
            r5.a()     // Catch:{ all -> 0x0082 }
            r5.e(r6)     // Catch:{ all -> 0x0082 }
            java.util.LinkedHashMap<java.lang.String, o.m0.c.e$b> r0 = r5.h     // Catch:{ all -> 0x0082 }
            java.lang.Object r0 = r0.get(r6)     // Catch:{ all -> 0x0082 }
            o.m0.c.DiskLruCache$b r0 = (o.m0.c.DiskLruCache.b) r0     // Catch:{ all -> 0x0082 }
            r2 = -1
            int r4 = (r7 > r2 ? 1 : (r7 == r2 ? 0 : -1))
            if (r4 == 0) goto L_0x0027
            if (r0 == 0) goto L_0x0025
            long r2 = r0.f2937f     // Catch:{ all -> 0x0082 }
            int r4 = (r2 > r7 ? 1 : (r2 == r7 ? 0 : -1))
            if (r4 == 0) goto L_0x0027
        L_0x0025:
            monitor-exit(r5)
            return r1
        L_0x0027:
            if (r0 == 0) goto L_0x002c
            o.m0.c.DiskLruCache$a r7 = r0.f2936e     // Catch:{ all -> 0x0082 }
            goto L_0x002d
        L_0x002c:
            r7 = r1
        L_0x002d:
            if (r7 == 0) goto L_0x0031
            monitor-exit(r5)
            return r1
        L_0x0031:
            boolean r7 = r5.f2928m     // Catch:{ all -> 0x0082 }
            if (r7 != 0) goto L_0x0075
            boolean r7 = r5.f2929n     // Catch:{ all -> 0x0082 }
            if (r7 == 0) goto L_0x003a
            goto L_0x0075
        L_0x003a:
            p.BufferedSink r7 = r5.g     // Catch:{ all -> 0x0082 }
            if (r7 == 0) goto L_0x0071
            java.lang.String r8 = o.m0.c.DiskLruCache.x     // Catch:{ all -> 0x0082 }
            p.BufferedSink r8 = r7.a(r8)     // Catch:{ all -> 0x0082 }
            r2 = 32
            p.BufferedSink r8 = r8.writeByte(r2)     // Catch:{ all -> 0x0082 }
            p.BufferedSink r8 = r8.a(r6)     // Catch:{ all -> 0x0082 }
            r2 = 10
            r8.writeByte(r2)     // Catch:{ all -> 0x0082 }
            r7.flush()     // Catch:{ all -> 0x0082 }
            boolean r7 = r5.f2925j     // Catch:{ all -> 0x0082 }
            if (r7 == 0) goto L_0x005c
            monitor-exit(r5)
            return r1
        L_0x005c:
            if (r0 != 0) goto L_0x0068
            o.m0.c.DiskLruCache$b r0 = new o.m0.c.DiskLruCache$b     // Catch:{ all -> 0x0082 }
            r0.<init>(r5, r6)     // Catch:{ all -> 0x0082 }
            java.util.LinkedHashMap<java.lang.String, o.m0.c.e$b> r7 = r5.h     // Catch:{ all -> 0x0082 }
            r7.put(r6, r0)     // Catch:{ all -> 0x0082 }
        L_0x0068:
            o.m0.c.DiskLruCache$a r6 = new o.m0.c.DiskLruCache$a     // Catch:{ all -> 0x0082 }
            r6.<init>(r5, r0)     // Catch:{ all -> 0x0082 }
            r0.f2936e = r6     // Catch:{ all -> 0x0082 }
            monitor-exit(r5)
            return r6
        L_0x0071:
            n.n.c.Intrinsics.a()     // Catch:{ all -> 0x0082 }
            throw r1
        L_0x0075:
            java.util.concurrent.Executor r6 = r5.u     // Catch:{ all -> 0x0082 }
            java.lang.Runnable r7 = r5.f2931p     // Catch:{ all -> 0x0082 }
            r6.execute(r7)     // Catch:{ all -> 0x0082 }
            monitor-exit(r5)
            return r1
        L_0x007e:
            n.n.c.Intrinsics.a(r0)     // Catch:{ all -> 0x0082 }
            throw r1
        L_0x0082:
            r6 = move-exception
            monitor-exit(r5)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: o.m0.c.DiskLruCache.a(java.lang.String, long):o.m0.c.DiskLruCache$a");
    }

    /* compiled from: DiskLruCache.kt */
    public final class b {
        public final long[] a;
        public final List<File> b;
        public final List<File> c;
        public boolean d;

        /* renamed from: e  reason: collision with root package name */
        public a f2936e;

        /* renamed from: f  reason: collision with root package name */
        public long f2937f;
        public final String g;
        public final /* synthetic */ DiskLruCache h;

        public b(DiskLruCache diskLruCache, String str) {
            if (str != null) {
                this.h = diskLruCache;
                this.g = str;
                this.a = new long[diskLruCache.f2935t];
                this.b = new ArrayList();
                this.c = new ArrayList();
                StringBuilder sb = new StringBuilder(this.g);
                sb.append('.');
                int length = sb.length();
                int i2 = diskLruCache.f2935t;
                for (int i3 = 0; i3 < i2; i3++) {
                    sb.append(i3);
                    this.b.add(new File(diskLruCache.f2933r, sb.toString()));
                    sb.append(".tmp");
                    this.c.add(new File(diskLruCache.f2933r, sb.toString()));
                    sb.setLength(length);
                }
                return;
            }
            Intrinsics.a("key");
            throw null;
        }

        public final void a(BufferedSink bufferedSink) {
            if (bufferedSink != null) {
                for (long h2 : this.a) {
                    bufferedSink.writeByte(32).h(h2);
                }
                return;
            }
            Intrinsics.a("writer");
            throw null;
        }

        public final c a() {
            boolean holdsLock = Thread.holdsLock(this.h);
            if (!AssertionsJVM.a || holdsLock) {
                ArrayList<Source> arrayList = new ArrayList<>();
                long[] jArr = (long[]) this.a.clone();
                try {
                    int i2 = this.h.f2935t;
                    for (int i3 = 0; i3 < i2; i3++) {
                        arrayList.add(this.h.f2932q.b(this.b.get(i3)));
                    }
                    return new c(this.h, this.g, this.f2937f, arrayList, jArr);
                } catch (FileNotFoundException unused) {
                    for (Source a2 : arrayList) {
                        Util.a(a2);
                    }
                    try {
                        this.h.a(this);
                        return null;
                    } catch (IOException unused2) {
                        return null;
                    }
                }
            } else {
                throw new AssertionError("Assertion failed");
            }
        }
    }

    /* compiled from: DiskLruCache.kt */
    public final class a {
        public final boolean[] a;
        public boolean b;
        public final b c;
        public final /* synthetic */ DiskLruCache d;

        /* renamed from: o.m0.c.DiskLruCache$a$a  reason: collision with other inner class name */
        /* compiled from: DiskLruCache.kt */
        public static final class C0043a extends j implements Functions0<IOException, g> {
            public final /* synthetic */ a c;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0043a(a aVar, int i2) {
                super(1);
                this.c = aVar;
            }

            public /* bridge */ /* synthetic */ Object a(Object obj) {
                a((IOException) obj);
                return Unit.a;
            }

            public final void a(IOException iOException) {
                if (iOException != null) {
                    synchronized (this.c.d) {
                        this.c.c();
                    }
                    return;
                }
                Intrinsics.a("it");
                throw null;
            }
        }

        public a(DiskLruCache diskLruCache, e.b bVar) {
            boolean[] zArr = null;
            if (bVar != null) {
                this.d = diskLruCache;
                this.c = bVar;
                this.a = !bVar.d ? new boolean[diskLruCache.f2935t] : zArr;
                return;
            }
            Intrinsics.a("entry");
            throw null;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
            r5 = new p.Okio();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x0051, code lost:
            return r5;
         */
        /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
        /* JADX WARNING: Missing exception handler attribute for start block: B:26:0x004b */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final p.Sink a(int r5) {
            /*
                r4 = this;
                o.m0.c.DiskLruCache r0 = r4.d
                monitor-enter(r0)
                boolean r1 = r4.b     // Catch:{ all -> 0x005e }
                r2 = 1
                r1 = r1 ^ r2
                if (r1 == 0) goto L_0x0052
                o.m0.c.DiskLruCache$b r1 = r4.c     // Catch:{ all -> 0x005e }
                o.m0.c.DiskLruCache$a r1 = r1.f2936e     // Catch:{ all -> 0x005e }
                boolean r1 = n.n.c.Intrinsics.a(r1, r4)     // Catch:{ all -> 0x005e }
                r1 = r1 ^ r2
                if (r1 == 0) goto L_0x001b
                p.Okio r5 = new p.Okio     // Catch:{ all -> 0x005e }
                r5.<init>()     // Catch:{ all -> 0x005e }
                monitor-exit(r0)
                return r5
            L_0x001b:
                o.m0.c.DiskLruCache$b r1 = r4.c     // Catch:{ all -> 0x005e }
                boolean r1 = r1.d     // Catch:{ all -> 0x005e }
                if (r1 != 0) goto L_0x002d
                boolean[] r1 = r4.a     // Catch:{ all -> 0x005e }
                if (r1 == 0) goto L_0x0028
                r1[r5] = r2     // Catch:{ all -> 0x005e }
                goto L_0x002d
            L_0x0028:
                n.n.c.Intrinsics.a()     // Catch:{ all -> 0x005e }
                r5 = 0
                throw r5
            L_0x002d:
                o.m0.c.DiskLruCache$b r1 = r4.c     // Catch:{ all -> 0x005e }
                java.util.List<java.io.File> r1 = r1.c     // Catch:{ all -> 0x005e }
                java.lang.Object r1 = r1.get(r5)     // Catch:{ all -> 0x005e }
                java.io.File r1 = (java.io.File) r1     // Catch:{ all -> 0x005e }
                o.m0.c.DiskLruCache r2 = r4.d     // Catch:{ FileNotFoundException -> 0x004b }
                o.m0.h.FileSystem r2 = r2.f2932q     // Catch:{ FileNotFoundException -> 0x004b }
                p.Sink r1 = r2.c(r1)     // Catch:{ FileNotFoundException -> 0x004b }
                o.m0.c.FaultHidingSink r2 = new o.m0.c.FaultHidingSink     // Catch:{ all -> 0x005e }
                o.m0.c.DiskLruCache$a$a r3 = new o.m0.c.DiskLruCache$a$a     // Catch:{ all -> 0x005e }
                r3.<init>(r4, r5)     // Catch:{ all -> 0x005e }
                r2.<init>(r1, r3)     // Catch:{ all -> 0x005e }
                monitor-exit(r0)
                return r2
            L_0x004b:
                p.Okio r5 = new p.Okio     // Catch:{ all -> 0x005e }
                r5.<init>()     // Catch:{ all -> 0x005e }
                monitor-exit(r0)
                return r5
            L_0x0052:
                java.lang.String r5 = "Check failed."
                java.lang.IllegalStateException r1 = new java.lang.IllegalStateException     // Catch:{ all -> 0x005e }
                java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x005e }
                r1.<init>(r5)     // Catch:{ all -> 0x005e }
                throw r1     // Catch:{ all -> 0x005e }
            L_0x005e:
                r5 = move-exception
                monitor-exit(r0)
                throw r5
            */
            throw new UnsupportedOperationException("Method not decompiled: o.m0.c.DiskLruCache.a.a(int):p.Sink");
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: o.m0.c.DiskLruCache.a(o.m0.c.DiskLruCache$a, boolean):void
         arg types: [o.m0.c.DiskLruCache$a, int]
         candidates:
          o.m0.c.DiskLruCache.a(java.lang.String, long):o.m0.c.DiskLruCache$a
          o.m0.c.DiskLruCache.a(o.m0.c.DiskLruCache$a, boolean):void */
        public final void b() {
            synchronized (this.d) {
                if (!this.b) {
                    if (Intrinsics.a(this.c.f2936e, this)) {
                        this.d.a(this, true);
                    }
                    this.b = true;
                } else {
                    throw new IllegalStateException("Check failed.".toString());
                }
            }
        }

        public final void c() {
            if (Intrinsics.a(this.c.f2936e, this)) {
                int i2 = this.d.f2935t;
                for (int i3 = 0; i3 < i2; i3++) {
                    try {
                        this.d.f2932q.a(this.c.c.get(i3));
                    } catch (IOException unused) {
                    }
                }
                this.c.f2936e = null;
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: o.m0.c.DiskLruCache.a(o.m0.c.DiskLruCache$a, boolean):void
         arg types: [o.m0.c.DiskLruCache$a, int]
         candidates:
          o.m0.c.DiskLruCache.a(java.lang.String, long):o.m0.c.DiskLruCache$a
          o.m0.c.DiskLruCache.a(o.m0.c.DiskLruCache$a, boolean):void */
        public final void a() {
            synchronized (this.d) {
                if (!this.b) {
                    if (Intrinsics.a(this.c.f2936e, this)) {
                        this.d.a(this, false);
                    }
                    this.b = true;
                } else {
                    throw new IllegalStateException("Check failed.".toString());
                }
            }
        }
    }

    public final synchronized void a() {
        if (!(!this.f2927l)) {
            throw new IllegalStateException("cache is closed".toString());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0104, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(o.m0.c.DiskLruCache.a r10, boolean r11) {
        /*
            r9 = this;
            monitor-enter(r9)
            java.lang.String r0 = "editor"
            r1 = 0
            if (r10 == 0) goto L_0x0115
            o.m0.c.DiskLruCache$b r0 = r10.c     // Catch:{ all -> 0x0119 }
            o.m0.c.DiskLruCache$a r2 = r0.f2936e     // Catch:{ all -> 0x0119 }
            boolean r2 = n.n.c.Intrinsics.a(r2, r10)     // Catch:{ all -> 0x0119 }
            if (r2 == 0) goto L_0x0109
            r2 = 0
            if (r11 == 0) goto L_0x005a
            boolean r3 = r0.d     // Catch:{ all -> 0x0119 }
            if (r3 != 0) goto L_0x005a
            int r3 = r9.f2935t     // Catch:{ all -> 0x0119 }
            r4 = 0
        L_0x001a:
            if (r4 >= r3) goto L_0x005a
            boolean[] r5 = r10.a     // Catch:{ all -> 0x0119 }
            if (r5 == 0) goto L_0x0056
            boolean r5 = r5[r4]     // Catch:{ all -> 0x0119 }
            if (r5 == 0) goto L_0x003c
            o.m0.h.FileSystem r5 = r9.f2932q     // Catch:{ all -> 0x0119 }
            java.util.List<java.io.File> r6 = r0.c     // Catch:{ all -> 0x0119 }
            java.lang.Object r6 = r6.get(r4)     // Catch:{ all -> 0x0119 }
            java.io.File r6 = (java.io.File) r6     // Catch:{ all -> 0x0119 }
            boolean r5 = r5.f(r6)     // Catch:{ all -> 0x0119 }
            if (r5 != 0) goto L_0x0039
            r10.a()     // Catch:{ all -> 0x0119 }
            monitor-exit(r9)
            return
        L_0x0039:
            int r4 = r4 + 1
            goto L_0x001a
        L_0x003c:
            r10.a()     // Catch:{ all -> 0x0119 }
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0119 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ all -> 0x0119 }
            r11.<init>()     // Catch:{ all -> 0x0119 }
            java.lang.String r0 = "Newly created entry didn't create value for index "
            r11.append(r0)     // Catch:{ all -> 0x0119 }
            r11.append(r4)     // Catch:{ all -> 0x0119 }
            java.lang.String r11 = r11.toString()     // Catch:{ all -> 0x0119 }
            r10.<init>(r11)     // Catch:{ all -> 0x0119 }
            throw r10     // Catch:{ all -> 0x0119 }
        L_0x0056:
            n.n.c.Intrinsics.a()     // Catch:{ all -> 0x0119 }
            throw r1
        L_0x005a:
            int r10 = r9.f2935t     // Catch:{ all -> 0x0119 }
        L_0x005c:
            if (r2 >= r10) goto L_0x009a
            java.util.List<java.io.File> r3 = r0.c     // Catch:{ all -> 0x0119 }
            java.lang.Object r3 = r3.get(r2)     // Catch:{ all -> 0x0119 }
            java.io.File r3 = (java.io.File) r3     // Catch:{ all -> 0x0119 }
            if (r11 == 0) goto L_0x0092
            o.m0.h.FileSystem r4 = r9.f2932q     // Catch:{ all -> 0x0119 }
            boolean r4 = r4.f(r3)     // Catch:{ all -> 0x0119 }
            if (r4 == 0) goto L_0x0097
            java.util.List<java.io.File> r4 = r0.b     // Catch:{ all -> 0x0119 }
            java.lang.Object r4 = r4.get(r2)     // Catch:{ all -> 0x0119 }
            java.io.File r4 = (java.io.File) r4     // Catch:{ all -> 0x0119 }
            o.m0.h.FileSystem r5 = r9.f2932q     // Catch:{ all -> 0x0119 }
            r5.a(r3, r4)     // Catch:{ all -> 0x0119 }
            long[] r3 = r0.a     // Catch:{ all -> 0x0119 }
            r5 = r3[r2]     // Catch:{ all -> 0x0119 }
            o.m0.h.FileSystem r3 = r9.f2932q     // Catch:{ all -> 0x0119 }
            long r3 = r3.g(r4)     // Catch:{ all -> 0x0119 }
            long[] r7 = r0.a     // Catch:{ all -> 0x0119 }
            r7[r2] = r3     // Catch:{ all -> 0x0119 }
            long r7 = r9.f2923f     // Catch:{ all -> 0x0119 }
            long r7 = r7 - r5
            long r7 = r7 + r3
            r9.f2923f = r7     // Catch:{ all -> 0x0119 }
            goto L_0x0097
        L_0x0092:
            o.m0.h.FileSystem r4 = r9.f2932q     // Catch:{ all -> 0x0119 }
            r4.a(r3)     // Catch:{ all -> 0x0119 }
        L_0x0097:
            int r2 = r2 + 1
            goto L_0x005c
        L_0x009a:
            int r10 = r9.f2924i     // Catch:{ all -> 0x0119 }
            r2 = 1
            int r10 = r10 + r2
            r9.f2924i = r10     // Catch:{ all -> 0x0119 }
            r0.f2936e = r1     // Catch:{ all -> 0x0119 }
            p.BufferedSink r10 = r9.g     // Catch:{ all -> 0x0119 }
            if (r10 == 0) goto L_0x0105
            boolean r1 = r0.d     // Catch:{ all -> 0x0119 }
            r3 = 10
            r4 = 32
            if (r1 != 0) goto L_0x00ca
            if (r11 == 0) goto L_0x00b1
            goto L_0x00ca
        L_0x00b1:
            java.util.LinkedHashMap<java.lang.String, o.m0.c.e$b> r11 = r9.h     // Catch:{ all -> 0x0119 }
            java.lang.String r1 = r0.g     // Catch:{ all -> 0x0119 }
            r11.remove(r1)     // Catch:{ all -> 0x0119 }
            java.lang.String r11 = o.m0.c.DiskLruCache.y     // Catch:{ all -> 0x0119 }
            p.BufferedSink r11 = r10.a(r11)     // Catch:{ all -> 0x0119 }
            r11.writeByte(r4)     // Catch:{ all -> 0x0119 }
            java.lang.String r11 = r0.g     // Catch:{ all -> 0x0119 }
            r10.a(r11)     // Catch:{ all -> 0x0119 }
            r10.writeByte(r3)     // Catch:{ all -> 0x0119 }
            goto L_0x00eb
        L_0x00ca:
            r0.d = r2     // Catch:{ all -> 0x0119 }
            java.lang.String r1 = o.m0.c.DiskLruCache.w     // Catch:{ all -> 0x0119 }
            p.BufferedSink r1 = r10.a(r1)     // Catch:{ all -> 0x0119 }
            r1.writeByte(r4)     // Catch:{ all -> 0x0119 }
            java.lang.String r1 = r0.g     // Catch:{ all -> 0x0119 }
            r10.a(r1)     // Catch:{ all -> 0x0119 }
            r0.a(r10)     // Catch:{ all -> 0x0119 }
            r10.writeByte(r3)     // Catch:{ all -> 0x0119 }
            if (r11 == 0) goto L_0x00eb
            long r1 = r9.f2930o     // Catch:{ all -> 0x0119 }
            r3 = 1
            long r3 = r3 + r1
            r9.f2930o = r3     // Catch:{ all -> 0x0119 }
            r0.f2937f = r1     // Catch:{ all -> 0x0119 }
        L_0x00eb:
            r10.flush()     // Catch:{ all -> 0x0119 }
            long r10 = r9.f2923f     // Catch:{ all -> 0x0119 }
            long r0 = r9.b     // Catch:{ all -> 0x0119 }
            int r2 = (r10 > r0 ? 1 : (r10 == r0 ? 0 : -1))
            if (r2 > 0) goto L_0x00fc
            boolean r10 = r9.g()     // Catch:{ all -> 0x0119 }
            if (r10 == 0) goto L_0x0103
        L_0x00fc:
            java.util.concurrent.Executor r10 = r9.u     // Catch:{ all -> 0x0119 }
            java.lang.Runnable r11 = r9.f2931p     // Catch:{ all -> 0x0119 }
            r10.execute(r11)     // Catch:{ all -> 0x0119 }
        L_0x0103:
            monitor-exit(r9)
            return
        L_0x0105:
            n.n.c.Intrinsics.a()     // Catch:{ all -> 0x0119 }
            throw r1
        L_0x0109:
            java.lang.String r10 = "Check failed."
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0119 }
            java.lang.String r10 = r10.toString()     // Catch:{ all -> 0x0119 }
            r11.<init>(r10)     // Catch:{ all -> 0x0119 }
            throw r11     // Catch:{ all -> 0x0119 }
        L_0x0115:
            n.n.c.Intrinsics.a(r0)     // Catch:{ all -> 0x0119 }
            throw r1
        L_0x0119:
            r10 = move-exception
            monitor-exit(r9)
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: o.m0.c.DiskLruCache.a(o.m0.c.DiskLruCache$a, boolean):void");
    }

    public final boolean a(b bVar) {
        if (bVar != null) {
            a aVar = bVar.f2936e;
            if (aVar != null) {
                aVar.c();
            }
            int i2 = this.f2935t;
            for (int i3 = 0; i3 < i2; i3++) {
                this.f2932q.a(bVar.b.get(i3));
                long j2 = this.f2923f;
                long[] jArr = bVar.a;
                this.f2923f = j2 - jArr[i3];
                jArr[i3] = 0;
            }
            this.f2924i++;
            BufferedSink bufferedSink = this.g;
            if (bufferedSink != null) {
                bufferedSink.a(y).writeByte(32).a(bVar.g).writeByte(10);
                this.h.remove(bVar.g);
                if (g()) {
                    this.u.execute(this.f2931p);
                }
                return true;
            }
            Intrinsics.a();
            throw null;
        }
        Intrinsics.a("entry");
        throw null;
    }
}
