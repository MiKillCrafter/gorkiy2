package o.m0.c;

import java.io.IOException;
import n.g;
import n.n.b.Functions0;
import n.n.c.Intrinsics;
import p.Buffer;
import p.ForwardingSink;

/* compiled from: FaultHidingSink.kt */
public class FaultHidingSink extends ForwardingSink {
    public boolean c;
    public final Functions0<IOException, g> d;

    /* JADX WARN: Type inference failed for: r3v0, types: [n.n.b.Functions0<java.io.IOException, n.g>, n.n.b.Functions0<? super java.io.IOException, n.g>] */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public FaultHidingSink(p.x r2, n.n.b.Functions0<? super java.io.IOException, n.g> r3) {
        /*
            r1 = this;
            r0 = 0
            if (r2 == 0) goto L_0x0011
            if (r3 == 0) goto L_0x000b
            r1.<init>(r2)
            r1.d = r3
            return
        L_0x000b:
            java.lang.String r2 = "onException"
            n.n.c.Intrinsics.a(r2)
            throw r0
        L_0x0011:
            java.lang.String r2 = "delegate"
            n.n.c.Intrinsics.a(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: o.m0.c.FaultHidingSink.<init>(p.Sink, n.n.b.Functions0):void");
    }

    public void a(Buffer buffer, long j2) {
        if (buffer == null) {
            Intrinsics.a("source");
            throw null;
        } else if (this.c) {
            buffer.skip(j2);
        } else {
            try {
                super.b.a(buffer, j2);
            } catch (IOException e2) {
                this.c = true;
                this.d.a(e2);
            }
        }
    }

    public void close() {
        if (!this.c) {
            try {
                super.close();
            } catch (IOException e2) {
                this.c = true;
                this.d.a(e2);
            }
        }
    }

    public void flush() {
        if (!this.c) {
            try {
                super.b.flush();
            } catch (IOException e2) {
                this.c = true;
                this.d.a(e2);
            }
        }
    }
}
