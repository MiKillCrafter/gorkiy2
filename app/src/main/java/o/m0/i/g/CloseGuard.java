package o.m0.i.g;

import java.lang.reflect.Method;

/* compiled from: CloseGuard.kt */
public final class CloseGuard {
    public final Method a;
    public final Method b;
    public final Method c;

    public CloseGuard(Method method, Method method2, Method method3) {
        this.a = method;
        this.b = method2;
        this.c = method3;
    }
}
