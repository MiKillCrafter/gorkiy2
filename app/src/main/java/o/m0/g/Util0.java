package o.m0.g;

import java.util.List;

/* compiled from: Util.kt */
public final class Util0 implements Runnable {
    public final /* synthetic */ String b;
    public final /* synthetic */ Http2Connection c;
    public final /* synthetic */ int d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ List f3041e;

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ boolean f3042f;

    public Util0(String str, Http2Connection http2Connection, int i2, List list, boolean z) {
        this.b = str;
        this.c = http2Connection;
        this.d = i2;
        this.f3041e = list;
        this.f3042f = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.Thread, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r6 = this;
            java.lang.String r0 = r6.b
            java.lang.Thread r1 = java.lang.Thread.currentThread()
            java.lang.String r2 = "currentThread"
            n.n.c.Intrinsics.a(r1, r2)
            java.lang.String r2 = r1.getName()
            r1.setName(r0)
            o.m0.g.Http2Connection r0 = r6.c     // Catch:{ all -> 0x004c }
            o.m0.g.PushObserver r0 = r0.f3002k     // Catch:{ all -> 0x004c }
            int r3 = r6.d     // Catch:{ all -> 0x004c }
            java.util.List r4 = r6.f3041e     // Catch:{ all -> 0x004c }
            boolean r5 = r6.f3042f     // Catch:{ all -> 0x004c }
            boolean r0 = r0.a(r3, r4, r5)     // Catch:{ all -> 0x004c }
            if (r0 == 0) goto L_0x002d
            o.m0.g.Http2Connection r3 = r6.c     // Catch:{ IOException -> 0x0048 }
            o.m0.g.Http2Writer r3 = r3.f3011t     // Catch:{ IOException -> 0x0048 }
            int r4 = r6.d     // Catch:{ IOException -> 0x0048 }
            o.m0.g.ErrorCode r5 = o.m0.g.ErrorCode.CANCEL     // Catch:{ IOException -> 0x0048 }
            r3.a(r4, r5)     // Catch:{ IOException -> 0x0048 }
        L_0x002d:
            if (r0 != 0) goto L_0x0033
            boolean r0 = r6.f3042f     // Catch:{ IOException -> 0x0048 }
            if (r0 == 0) goto L_0x0048
        L_0x0033:
            o.m0.g.Http2Connection r0 = r6.c     // Catch:{ IOException -> 0x0048 }
            monitor-enter(r0)     // Catch:{ IOException -> 0x0048 }
            o.m0.g.Http2Connection r3 = r6.c     // Catch:{ all -> 0x0045 }
            java.util.Set<java.lang.Integer> r3 = r3.v     // Catch:{ all -> 0x0045 }
            int r4 = r6.d     // Catch:{ all -> 0x0045 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x0045 }
            r3.remove(r4)     // Catch:{ all -> 0x0045 }
            monitor-exit(r0)     // Catch:{ IOException -> 0x0048 }
            goto L_0x0048
        L_0x0045:
            r3 = move-exception
            monitor-exit(r0)     // Catch:{ IOException -> 0x0048 }
            throw r3     // Catch:{ IOException -> 0x0048 }
        L_0x0048:
            r1.setName(r2)
            return
        L_0x004c:
            r0 = move-exception
            r1.setName(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: o.m0.g.Util0.run():void");
    }
}
