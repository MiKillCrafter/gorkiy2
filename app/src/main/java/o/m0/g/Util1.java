package o.m0.g;

import n.n.c.Intrinsics;

/* compiled from: Util.kt */
public final class Util1 implements Runnable {
    public final /* synthetic */ String b;
    public final /* synthetic */ Http2Connection c;
    public final /* synthetic */ int d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ ErrorCode f3043e;

    public Util1(String str, Http2Connection http2Connection, int i2, ErrorCode errorCode) {
        this.b = str;
        this.c = http2Connection;
        this.d = i2;
        this.f3043e = errorCode;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.Thread, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public final void run() {
        String str = this.b;
        Thread currentThread = Thread.currentThread();
        Intrinsics.a((Object) currentThread, "currentThread");
        String name = currentThread.getName();
        currentThread.setName(str);
        try {
            this.c.f3002k.a(this.d, this.f3043e);
            synchronized (this.c) {
                this.c.v.remove(Integer.valueOf(this.d));
            }
            currentThread.setName(name);
        } catch (Throwable th) {
            currentThread.setName(name);
            throw th;
        }
    }
}
