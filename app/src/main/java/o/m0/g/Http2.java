package o.m0.g;

import j.a.a.a.outline;
import n.n.c.Intrinsics;
import n.r.Indent;
import o.m0.Util;
import p.ByteString;

/* compiled from: Http2.kt */
public final class Http2 {
    public static final ByteString a = ByteString.f3063f.b("PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n");
    public static final String[] b = {"DATA", "HEADERS", "PRIORITY", "RST_STREAM", "SETTINGS", "PUSH_PROMISE", "PING", "GOAWAY", "WINDOW_UPDATE", "CONTINUATION"};
    public static final String[] c = new String[64];
    public static final String[] d;

    /* renamed from: e  reason: collision with root package name */
    public static final Http2 f2997e = new Http2();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    static {
        String[] strArr = new String[256];
        int i2 = 0;
        while (i2 < 256) {
            String binaryString = Integer.toBinaryString(i2);
            Intrinsics.a((Object) binaryString, "Integer.toBinaryString(it)");
            String a2 = Util.a("%8s", binaryString);
            if (a2 != null) {
                String replace = a2.replace(' ', '0');
                Intrinsics.a((Object) replace, "(this as java.lang.Strin…replace(oldChar, newChar)");
                strArr[i2] = replace;
                i2++;
            } else {
                Intrinsics.a("$this$replace");
                throw null;
            }
        }
        d = strArr;
        String[] strArr2 = c;
        strArr2[0] = "";
        strArr2[1] = "END_STREAM";
        int[] iArr = {1};
        strArr2[8] = "PADDED";
        for (int i3 = 0; i3 < 1; i3++) {
            int i4 = iArr[i3];
            String[] strArr3 = c;
            int i5 = i4 | 8;
            String str = strArr3[i4];
            strArr3[i5] = str + ((Object) "|PADDED");
        }
        String[] strArr4 = c;
        strArr4[4] = "END_HEADERS";
        strArr4[32] = "PRIORITY";
        strArr4[36] = "END_HEADERS|PRIORITY";
        int[] iArr2 = {4, 32, 36};
        for (int i6 = 0; i6 < 3; i6++) {
            int i7 = iArr2[i6];
            for (int i8 = 0; i8 < 1; i8++) {
                int i9 = iArr[i8];
                String[] strArr5 = c;
                int i10 = i9 | i7;
                strArr5[i10] = c[i9] + "|" + c[i7];
                StringBuilder sb = new StringBuilder();
                sb.append(c[i9]);
                sb.append("|");
                c[i10 | 8] = outline.a(sb, c[i7], "|PADDED");
            }
        }
        int length = c.length;
        for (int i11 = 0; i11 < length; i11++) {
            String[] strArr6 = c;
            if (strArr6[i11] == null) {
                strArr6[i11] = d[i11];
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.String, java.lang.String, java.lang.String, boolean, int):java.lang.String
     arg types: [java.lang.String, java.lang.String, java.lang.String, int, int]
     candidates:
      n.r.Indent.a(java.lang.CharSequence, char, int, boolean, int):int
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean, int):int
      n.r.Indent.a(java.lang.CharSequence, char[], boolean, int, int):java.util.List
      n.r.Indent.a(java.lang.CharSequence, java.lang.String[], boolean, int, int):n.q.Sequence
      n.r.Indent.a(java.lang.String, java.lang.String, int, boolean, int):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, java.lang.String, boolean, int):java.lang.String */
    public final String a(boolean z, int i2, int i3, int i4, int i5) {
        String str;
        String str2;
        String[] strArr = b;
        String a2 = i4 < strArr.length ? strArr[i4] : Util.a("0x%02x", Integer.valueOf(i4));
        if (i5 == 0) {
            str = "";
        } else {
            if (!(i4 == 2 || i4 == 3)) {
                if (i4 == 4 || i4 == 6) {
                    str = i5 == 1 ? "ACK" : d[i5];
                } else if (!(i4 == 7 || i4 == 8)) {
                    String[] strArr2 = c;
                    if (i5 < strArr2.length) {
                        str2 = strArr2[i5];
                        if (str2 == null) {
                            Intrinsics.a();
                            throw null;
                        }
                    } else {
                        str2 = d[i5];
                    }
                    str = (i4 != 5 || (i5 & 4) == 0) ? (i4 != 0 || (i5 & 32) == 0) ? str2 : Indent.a(str2, "PRIORITY", "COMPRESSED", false, 4) : Indent.a(str2, "HEADERS", "PUSH_PROMISE", false, 4);
                }
            }
            str = d[i5];
        }
        return Util.a("%s 0x%08x %5d %-13s %s", z ? "<<" : ">>", Integer.valueOf(i2), Integer.valueOf(i3), a2, str);
    }
}
