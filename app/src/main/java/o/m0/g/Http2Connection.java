package o.m0.g;

import com.crashlytics.android.answers.AnswersPreferenceManager;
import j.a.a.a.outline;
import java.io.Closeable;
import java.io.IOException;
import java.net.Socket;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import kotlin.TypeCastException;
import n.AssertionsJVM;
import n.n.c.Intrinsics;
import o.m0.Util;
import o.m0.g.Http2Reader;
import o.m0.i.Platform;
import p.Buffer;
import p.BufferedSink;
import p.BufferedSource;
import p.ByteString;

/* compiled from: Http2Connection.kt */
public final class Http2Connection implements Closeable {
    public static final ThreadPoolExecutor w = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60, TimeUnit.SECONDS, new SynchronousQueue(), Util.a("OkHttp Http2Connection", true));
    public final boolean b;
    public final c c;
    public final Map<Integer, k> d;

    /* renamed from: e  reason: collision with root package name */
    public final String f2998e;

    /* renamed from: f  reason: collision with root package name */
    public int f2999f;
    public int g;
    public boolean h;

    /* renamed from: i  reason: collision with root package name */
    public final ScheduledThreadPoolExecutor f3000i;

    /* renamed from: j  reason: collision with root package name */
    public final ThreadPoolExecutor f3001j;

    /* renamed from: k  reason: collision with root package name */
    public final PushObserver f3002k;

    /* renamed from: l  reason: collision with root package name */
    public boolean f3003l;

    /* renamed from: m  reason: collision with root package name */
    public final Settings f3004m;

    /* renamed from: n  reason: collision with root package name */
    public final Settings f3005n;

    /* renamed from: o  reason: collision with root package name */
    public long f3006o;

    /* renamed from: p  reason: collision with root package name */
    public long f3007p;

    /* renamed from: q  reason: collision with root package name */
    public long f3008q;

    /* renamed from: r  reason: collision with root package name */
    public long f3009r;

    /* renamed from: s  reason: collision with root package name */
    public final Socket f3010s;

    /* renamed from: t  reason: collision with root package name */
    public final Http2Writer f3011t;
    public final d u;
    public final Set<Integer> v;

    /* compiled from: Http2Connection.kt */
    public static final class a implements Runnable {
        public final /* synthetic */ Http2Connection b;

        public a(Http2Connection http2Connection) {
            this.b = http2Connection;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.Thread, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: o.m0.g.Http2Connection.a(boolean, int, int):void
         arg types: [int, int, int]
         candidates:
          o.m0.g.Http2Connection.a(int, java.util.List<o.m0.g.b>, boolean):o.m0.g.k
          o.m0.g.Http2Connection.a(o.m0.g.ErrorCode, o.m0.g.ErrorCode, java.io.IOException):void
          o.m0.g.Http2Connection.a(boolean, int, int):void */
        public final void run() {
            String a = outline.a(outline.a("OkHttp "), this.b.f2998e, " ping");
            Thread currentThread = Thread.currentThread();
            Intrinsics.a((Object) currentThread, "currentThread");
            String name = currentThread.getName();
            currentThread.setName(a);
            try {
                this.b.a(false, 0, 0);
            } finally {
                currentThread.setName(name);
            }
        }
    }

    /* compiled from: Http2Connection.kt */
    public static final class b {
        public Socket a;
        public String b;
        public BufferedSource c;
        public BufferedSink d;

        /* renamed from: e  reason: collision with root package name */
        public c f3012e = c.a;

        /* renamed from: f  reason: collision with root package name */
        public PushObserver f3013f = PushObserver.a;
        public int g;
        public boolean h;

        public b(boolean z) {
            this.h = z;
        }
    }

    /* compiled from: Http2Connection.kt */
    public static abstract class c {
        public static final c a = new a();

        /* compiled from: Http2Connection.kt */
        public static final class a extends c {
            public void a(Http2Stream http2Stream) {
                if (http2Stream != null) {
                    http2Stream.a(ErrorCode.REFUSED_STREAM, (IOException) null);
                } else {
                    Intrinsics.a("stream");
                    throw null;
                }
            }
        }

        public void a(Http2Connection http2Connection) {
            if (http2Connection == null) {
                Intrinsics.a("connection");
                throw null;
            }
        }

        public abstract void a(Http2Stream http2Stream);
    }

    /* compiled from: Util.kt */
    public static final class e implements Runnable {
        public final /* synthetic */ String b;
        public final /* synthetic */ Http2Connection c;
        public final /* synthetic */ int d;

        /* renamed from: e  reason: collision with root package name */
        public final /* synthetic */ List f3016e;

        public e(String str, Http2Connection http2Connection, int i2, List list) {
            this.b = str;
            this.c = http2Connection;
            this.d = i2;
            this.f3016e = list;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.Thread, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void run() {
            /*
                r5 = this;
                java.lang.String r0 = r5.b
                java.lang.Thread r1 = java.lang.Thread.currentThread()
                java.lang.String r2 = "currentThread"
                n.n.c.Intrinsics.a(r1, r2)
                java.lang.String r2 = r1.getName()
                r1.setName(r0)
                o.m0.g.Http2Connection r0 = r5.c     // Catch:{ all -> 0x0044 }
                o.m0.g.PushObserver r0 = r0.f3002k     // Catch:{ all -> 0x0044 }
                int r3 = r5.d     // Catch:{ all -> 0x0044 }
                java.util.List r4 = r5.f3016e     // Catch:{ all -> 0x0044 }
                boolean r0 = r0.a(r3, r4)     // Catch:{ all -> 0x0044 }
                if (r0 == 0) goto L_0x0040
                o.m0.g.Http2Connection r0 = r5.c     // Catch:{ IOException -> 0x0040 }
                o.m0.g.Http2Writer r0 = r0.f3011t     // Catch:{ IOException -> 0x0040 }
                int r3 = r5.d     // Catch:{ IOException -> 0x0040 }
                o.m0.g.ErrorCode r4 = o.m0.g.ErrorCode.CANCEL     // Catch:{ IOException -> 0x0040 }
                r0.a(r3, r4)     // Catch:{ IOException -> 0x0040 }
                o.m0.g.Http2Connection r0 = r5.c     // Catch:{ IOException -> 0x0040 }
                monitor-enter(r0)     // Catch:{ IOException -> 0x0040 }
                o.m0.g.Http2Connection r3 = r5.c     // Catch:{ all -> 0x003d }
                java.util.Set<java.lang.Integer> r3 = r3.v     // Catch:{ all -> 0x003d }
                int r4 = r5.d     // Catch:{ all -> 0x003d }
                java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x003d }
                r3.remove(r4)     // Catch:{ all -> 0x003d }
                monitor-exit(r0)     // Catch:{ IOException -> 0x0040 }
                goto L_0x0040
            L_0x003d:
                r3 = move-exception
                monitor-exit(r0)     // Catch:{ IOException -> 0x0040 }
                throw r3     // Catch:{ IOException -> 0x0040 }
            L_0x0040:
                r1.setName(r2)
                return
            L_0x0044:
                r0 = move-exception
                r1.setName(r2)
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: o.m0.g.Http2Connection.e.run():void");
        }
    }

    /* compiled from: Util.kt */
    public static final class f implements Runnable {
        public final /* synthetic */ String b;
        public final /* synthetic */ Http2Connection c;
        public final /* synthetic */ int d;

        /* renamed from: e  reason: collision with root package name */
        public final /* synthetic */ ErrorCode f3017e;

        public f(String str, Http2Connection http2Connection, int i2, ErrorCode errorCode) {
            this.b = str;
            this.c = http2Connection;
            this.d = i2;
            this.f3017e = errorCode;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.Thread, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        public final void run() {
            String str = this.b;
            Thread currentThread = Thread.currentThread();
            Intrinsics.a((Object) currentThread, "currentThread");
            String name = currentThread.getName();
            currentThread.setName(str);
            try {
                Http2Connection http2Connection = this.c;
                int i2 = this.d;
                ErrorCode errorCode = this.f3017e;
                if (errorCode != null) {
                    http2Connection.f3011t.a(i2, errorCode);
                    currentThread.setName(name);
                    return;
                }
                Intrinsics.a("statusCode");
                throw null;
            } catch (IOException e2) {
                Http2Connection.a(this.c, e2);
            } catch (Throwable th) {
                currentThread.setName(name);
                throw th;
            }
        }
    }

    /* compiled from: Util.kt */
    public static final class g implements Runnable {
        public final /* synthetic */ String b;
        public final /* synthetic */ Http2Connection c;
        public final /* synthetic */ int d;

        /* renamed from: e  reason: collision with root package name */
        public final /* synthetic */ long f3018e;

        public g(String str, Http2Connection http2Connection, int i2, long j2) {
            this.b = str;
            this.c = http2Connection;
            this.d = i2;
            this.f3018e = j2;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.Thread, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        public final void run() {
            String str = this.b;
            Thread currentThread = Thread.currentThread();
            Intrinsics.a((Object) currentThread, "currentThread");
            String name = currentThread.getName();
            currentThread.setName(str);
            try {
                this.c.f3011t.a(this.d, this.f3018e);
            } catch (IOException e2) {
                Http2Connection.a(this.c, e2);
            } catch (Throwable th) {
                currentThread.setName(name);
                throw th;
            }
            currentThread.setName(name);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: o.m0.Util.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
     arg types: [java.lang.String, int]
     candidates:
      o.m0.Util.a(byte, int):int
      o.m0.Util.a(java.lang.String, int):int
      o.m0.Util.a(java.lang.String, long):long
      o.m0.Util.a(java.lang.String, java.lang.Object[]):java.lang.String
      o.m0.Util.a(o.HttpUrl, boolean):java.lang.String
      o.m0.Util.a(p.BufferedSource, java.nio.charset.Charset):java.nio.charset.Charset
      o.m0.Util.a(java.lang.Object, long):void
      o.m0.Util.a(p.BufferedSink, int):void
      o.m0.Util.a(o.HttpUrl, o.HttpUrl):boolean
      o.m0.Util.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory */
    public Http2Connection(b bVar) {
        if (bVar != null) {
            this.b = bVar.h;
            this.c = bVar.f3012e;
            this.d = new LinkedHashMap();
            String str = bVar.b;
            if (str != null) {
                this.f2998e = str;
                this.g = bVar.h ? 3 : 2;
                this.f3000i = new ScheduledThreadPoolExecutor(1, Util.a(Util.a("OkHttp %s Writer", this.f2998e), false));
                this.f3001j = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), Util.a(Util.a("OkHttp %s Push Observer", this.f2998e), true));
                this.f3002k = bVar.f3013f;
                Settings settings = new Settings();
                if (bVar.h) {
                    settings.a(7, 16777216);
                }
                this.f3004m = settings;
                Settings settings2 = new Settings();
                settings2.a(7, 65535);
                settings2.a(5, 16384);
                this.f3005n = settings2;
                this.f3009r = (long) settings2.a();
                Socket socket = bVar.a;
                if (socket != null) {
                    this.f3010s = socket;
                    BufferedSink bufferedSink = bVar.d;
                    if (bufferedSink != null) {
                        this.f3011t = new Http2Writer(bufferedSink, this.b);
                        BufferedSource bufferedSource = bVar.c;
                        if (bufferedSource != null) {
                            this.u = new d(this, new Http2Reader(bufferedSource, this.b));
                            this.v = new LinkedHashSet();
                            if (bVar.g != 0) {
                                ScheduledThreadPoolExecutor scheduledThreadPoolExecutor = this.f3000i;
                                a aVar = new a(this);
                                int i2 = bVar.g;
                                scheduledThreadPoolExecutor.scheduleAtFixedRate(aVar, (long) i2, (long) i2, TimeUnit.MILLISECONDS);
                                return;
                            }
                            return;
                        }
                        Intrinsics.b("source");
                        throw null;
                    }
                    Intrinsics.b("sink");
                    throw null;
                }
                Intrinsics.b("socket");
                throw null;
            }
            Intrinsics.b("connectionName");
            throw null;
        }
        Intrinsics.a("builder");
        throw null;
    }

    public final synchronized boolean a() {
        return this.h;
    }

    public final boolean b(int i2) {
        return i2 != 0 && (i2 & 1) == 0;
    }

    public final synchronized Http2Stream c(int i2) {
        Http2Stream remove;
        remove = this.d.remove(Integer.valueOf(i2));
        notifyAll();
        return remove;
    }

    public void close() {
        a(ErrorCode.NO_ERROR, ErrorCode.CANCEL, (IOException) null);
    }

    public final synchronized void e(long j2) {
        long j3 = this.f3006o + j2;
        this.f3006o = j3;
        long j4 = j3 - this.f3007p;
        if (j4 >= ((long) (this.f3004m.a() / 2))) {
            a(0, j4);
            this.f3007p += j4;
        }
    }

    public final synchronized int f() {
        int i2;
        Settings settings = this.f3005n;
        i2 = Integer.MAX_VALUE;
        if ((settings.a & 16) != 0) {
            i2 = settings.b[4];
        }
        return i2;
    }

    public final synchronized Http2Stream a(int i2) {
        return this.d.get(Integer.valueOf(i2));
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0048 A[Catch:{ all -> 0x003f }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final o.m0.g.k a(int r11, java.util.List<o.m0.g.b> r12, boolean r13) {
        /*
            r10 = this;
            r6 = r13 ^ 1
            r4 = 0
            o.m0.g.Http2Writer r7 = r10.f3011t
            monitor-enter(r7)
            monitor-enter(r10)     // Catch:{ all -> 0x0081 }
            int r0 = r10.g     // Catch:{ all -> 0x003f }
            r1 = 1073741823(0x3fffffff, float:1.9999999)
            if (r0 <= r1) goto L_0x0013
            o.m0.g.ErrorCode r0 = o.m0.g.ErrorCode.REFUSED_STREAM     // Catch:{ all -> 0x003f }
            r10.a(r0)     // Catch:{ all -> 0x003f }
        L_0x0013:
            boolean r0 = r10.h     // Catch:{ all -> 0x003f }
            if (r0 != 0) goto L_0x0079
            int r8 = r10.g     // Catch:{ all -> 0x003f }
            int r0 = r10.g     // Catch:{ all -> 0x003f }
            int r0 = r0 + 2
            r10.g = r0     // Catch:{ all -> 0x003f }
            o.m0.g.Http2Stream r9 = new o.m0.g.Http2Stream     // Catch:{ all -> 0x003f }
            r5 = 0
            r0 = r9
            r1 = r8
            r2 = r10
            r3 = r6
            r0.<init>(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x003f }
            r0 = 1
            if (r13 == 0) goto L_0x0041
            long r1 = r10.f3008q     // Catch:{ all -> 0x003f }
            long r3 = r10.f3009r     // Catch:{ all -> 0x003f }
            int r13 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r13 >= 0) goto L_0x0041
            long r1 = r9.c     // Catch:{ all -> 0x003f }
            long r3 = r9.d     // Catch:{ all -> 0x003f }
            int r13 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r13 < 0) goto L_0x003d
            goto L_0x0041
        L_0x003d:
            r13 = 0
            goto L_0x0042
        L_0x003f:
            r11 = move-exception
            goto L_0x007f
        L_0x0041:
            r13 = 1
        L_0x0042:
            boolean r1 = r9.f()     // Catch:{ all -> 0x003f }
            if (r1 == 0) goto L_0x0051
            java.util.Map<java.lang.Integer, o.m0.g.k> r1 = r10.d     // Catch:{ all -> 0x003f }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r8)     // Catch:{ all -> 0x003f }
            r1.put(r2, r9)     // Catch:{ all -> 0x003f }
        L_0x0051:
            monitor-exit(r10)     // Catch:{ all -> 0x0081 }
            if (r11 != 0) goto L_0x005a
            o.m0.g.Http2Writer r11 = r10.f3011t     // Catch:{ all -> 0x0081 }
            r11.a(r6, r8, r12)     // Catch:{ all -> 0x0081 }
            goto L_0x0064
        L_0x005a:
            boolean r1 = r10.b     // Catch:{ all -> 0x0081 }
            r0 = r0 ^ r1
            if (r0 == 0) goto L_0x006d
            o.m0.g.Http2Writer r0 = r10.f3011t     // Catch:{ all -> 0x0081 }
            r0.a(r11, r8, r12)     // Catch:{ all -> 0x0081 }
        L_0x0064:
            monitor-exit(r7)
            if (r13 == 0) goto L_0x006c
            o.m0.g.Http2Writer r11 = r10.f3011t
            r11.flush()
        L_0x006c:
            return r9
        L_0x006d:
            java.lang.String r11 = "client streams shouldn't have associated stream IDs"
            java.lang.IllegalArgumentException r12 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0081 }
            java.lang.String r11 = r11.toString()     // Catch:{ all -> 0x0081 }
            r12.<init>(r11)     // Catch:{ all -> 0x0081 }
            throw r12     // Catch:{ all -> 0x0081 }
        L_0x0079:
            okhttp3.internal.http2.ConnectionShutdownException r11 = new okhttp3.internal.http2.ConnectionShutdownException     // Catch:{ all -> 0x003f }
            r11.<init>()     // Catch:{ all -> 0x003f }
            throw r11     // Catch:{ all -> 0x003f }
        L_0x007f:
            monitor-exit(r10)     // Catch:{ all -> 0x0081 }
            throw r11     // Catch:{ all -> 0x0081 }
        L_0x0081:
            r11 = move-exception
            monitor-exit(r7)
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: o.m0.g.Http2Connection.a(int, java.util.List, boolean):o.m0.g.Http2Stream");
    }

    /* compiled from: Http2Connection.kt */
    public final class d implements Runnable, Http2Reader.b {
        public final Http2Reader b;
        public final /* synthetic */ Http2Connection c;

        /* compiled from: Util.kt */
        public static final class a implements Runnable {
            public final /* synthetic */ String b;
            public final /* synthetic */ d c;

            public a(String str, d dVar) {
                this.b = str;
                this.c = dVar;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
             arg types: [java.lang.Thread, java.lang.String]
             candidates:
              n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
              n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
              n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
            public final void run() {
                String str = this.b;
                Thread currentThread = Thread.currentThread();
                Intrinsics.a((Object) currentThread, "currentThread");
                String name = currentThread.getName();
                currentThread.setName(str);
                try {
                    this.c.c.c.a(this.c.c);
                } finally {
                    currentThread.setName(name);
                }
            }
        }

        /* compiled from: Util.kt */
        public static final class b implements Runnable {
            public final /* synthetic */ String b;
            public final /* synthetic */ Http2Stream c;
            public final /* synthetic */ d d;

            public b(String str, Http2Stream http2Stream, d dVar, Http2Stream http2Stream2, int i2, List list, boolean z) {
                this.b = str;
                this.c = http2Stream;
                this.d = dVar;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
             arg types: [java.lang.Thread, java.lang.String]
             candidates:
              n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
              n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
              n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
            public final void run() {
                String str = this.b;
                Thread currentThread = Thread.currentThread();
                Intrinsics.a((Object) currentThread, "currentThread");
                String name = currentThread.getName();
                currentThread.setName(str);
                try {
                    this.d.c.c.a(this.c);
                } catch (IOException e2) {
                    Platform.a aVar = Platform.c;
                    Platform platform = Platform.a;
                    platform.a(4, "Http2Connection.Listener failure for " + this.d.c.f2998e, e2);
                    try {
                        this.c.a(ErrorCode.PROTOCOL_ERROR, e2);
                    } catch (IOException unused) {
                    }
                } catch (Throwable th) {
                    currentThread.setName(name);
                    throw th;
                }
                currentThread.setName(name);
            }
        }

        /* compiled from: Util.kt */
        public static final class c implements Runnable {
            public final /* synthetic */ String b;
            public final /* synthetic */ d c;
            public final /* synthetic */ int d;

            /* renamed from: e  reason: collision with root package name */
            public final /* synthetic */ int f3014e;

            public c(String str, d dVar, int i2, int i3) {
                this.b = str;
                this.c = dVar;
                this.d = i2;
                this.f3014e = i3;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
             arg types: [java.lang.Thread, java.lang.String]
             candidates:
              n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
              n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
              n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: o.m0.g.Http2Connection.a(boolean, int, int):void
             arg types: [int, int, int]
             candidates:
              o.m0.g.Http2Connection.a(int, java.util.List<o.m0.g.b>, boolean):o.m0.g.k
              o.m0.g.Http2Connection.a(o.m0.g.ErrorCode, o.m0.g.ErrorCode, java.io.IOException):void
              o.m0.g.Http2Connection.a(boolean, int, int):void */
            public final void run() {
                String str = this.b;
                Thread currentThread = Thread.currentThread();
                Intrinsics.a((Object) currentThread, "currentThread");
                String name = currentThread.getName();
                currentThread.setName(str);
                try {
                    this.c.c.a(true, this.d, this.f3014e);
                } finally {
                    currentThread.setName(name);
                }
            }
        }

        /* renamed from: o.m0.g.Http2Connection$d$d  reason: collision with other inner class name */
        /* compiled from: Util.kt */
        public static final class C0044d implements Runnable {
            public final /* synthetic */ String b;
            public final /* synthetic */ d c;
            public final /* synthetic */ boolean d;

            /* renamed from: e  reason: collision with root package name */
            public final /* synthetic */ Settings f3015e;

            public C0044d(String str, d dVar, boolean z, Settings settings) {
                this.b = str;
                this.c = dVar;
                this.d = z;
                this.f3015e = settings;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
             arg types: [java.lang.Thread, java.lang.String]
             candidates:
              n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
              n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
              n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
            public final void run() {
                String str = this.b;
                Thread currentThread = Thread.currentThread();
                Intrinsics.a((Object) currentThread, "currentThread");
                String name = currentThread.getName();
                currentThread.setName(str);
                try {
                    this.c.b(this.d, this.f3015e);
                } finally {
                    currentThread.setName(name);
                }
            }
        }

        public d(Http2Connection http2Connection, j jVar) {
            if (jVar != null) {
                this.c = http2Connection;
                this.b = jVar;
                return;
            }
            Intrinsics.a("reader");
            throw null;
        }

        public void a() {
        }

        public void a(int i2, int i3, int i4, boolean z) {
        }

        public void a(int i2, ErrorCode errorCode, ByteString byteString) {
            int i3;
            Http2Stream[] http2StreamArr;
            if (errorCode == null) {
                Intrinsics.a("errorCode");
                throw null;
            } else if (byteString != null) {
                int g = byteString.g();
                synchronized (this.c) {
                    Object[] array = this.c.d.values().toArray(new Http2Stream[0]);
                    if (array != null) {
                        http2StreamArr = (Http2Stream[]) array;
                        this.c.h = true;
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
                    }
                }
                for (Http2Stream http2Stream : http2StreamArr) {
                    if (http2Stream.f3031m > i2 && http2Stream.e()) {
                        http2Stream.b(ErrorCode.REFUSED_STREAM);
                        this.c.c(http2Stream.f3031m);
                    }
                }
            } else {
                Intrinsics.a("debugData");
                throw null;
            }
        }

        public final void b(boolean z, Settings settings) {
            int i2;
            long j2;
            Http2Stream[] http2StreamArr = null;
            if (settings != null) {
                synchronized (this.c.f3011t) {
                    synchronized (this.c) {
                        int a2 = this.c.f3005n.a();
                        if (z) {
                            Settings settings2 = this.c.f3005n;
                            settings2.a = 0;
                            int[] iArr = settings2.b;
                            Arrays.fill(iArr, 0, iArr.length, 0);
                        }
                        Settings settings3 = this.c.f3005n;
                        if (settings3 != null) {
                            int i3 = 0;
                            while (true) {
                                boolean z2 = true;
                                if (i3 >= 10) {
                                    break;
                                }
                                if (((1 << i3) & settings.a) == 0) {
                                    z2 = false;
                                }
                                if (z2) {
                                    settings3.a(i3, settings.b[i3]);
                                }
                                i3++;
                            }
                            int a3 = this.c.f3005n.a();
                            if (a3 == -1 || a3 == a2) {
                                j2 = 0;
                            } else {
                                j2 = (long) (a3 - a2);
                                if (!this.c.d.isEmpty()) {
                                    Object[] array = this.c.d.values().toArray(new Http2Stream[0]);
                                    if (array != null) {
                                        http2StreamArr = (Http2Stream[]) array;
                                    } else {
                                        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
                                    }
                                }
                            }
                        } else {
                            throw null;
                        }
                    }
                    try {
                        this.c.f3011t.a(this.c.f3005n);
                    } catch (IOException e2) {
                        Http2Connection.a(this.c, e2);
                    }
                }
                if (http2StreamArr != null) {
                    for (Http2Stream http2Stream : http2StreamArr) {
                        synchronized (http2Stream) {
                            http2Stream.d += j2;
                            if (j2 > 0) {
                                http2Stream.notifyAll();
                            }
                        }
                    }
                }
                Http2Connection.w.execute(new a(outline.a(outline.a("OkHttp "), this.c.f2998e, " settings"), this));
                return;
            }
            Intrinsics.a(AnswersPreferenceManager.PREF_STORE_NAME);
            throw null;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: o.m0.g.Http2Reader.a(boolean, o.m0.g.Http2Reader$b):boolean
         arg types: [int, o.m0.g.Http2Connection$d]
         candidates:
          o.m0.g.Http2Reader.a(o.m0.g.Http2Reader$b, int):void
          o.m0.g.Http2Reader.a(boolean, o.m0.g.Http2Reader$b):boolean */
        public void run() {
            ErrorCode errorCode;
            ErrorCode errorCode2;
            ErrorCode errorCode3 = ErrorCode.INTERNAL_ERROR;
            e = null;
            try {
                this.b.a(this);
                while (this.b.a(false, (Http2Reader.b) this)) {
                }
                errorCode = ErrorCode.NO_ERROR;
                try {
                    errorCode2 = ErrorCode.CANCEL;
                } catch (IOException e2) {
                    e = e2;
                }
            } catch (IOException e3) {
                e = e3;
                errorCode = errorCode3;
                try {
                    errorCode = ErrorCode.PROTOCOL_ERROR;
                    errorCode2 = ErrorCode.PROTOCOL_ERROR;
                    this.c.a(errorCode, errorCode2, e);
                    Util.a(this.b);
                } catch (Throwable th) {
                    th = th;
                    this.c.a(errorCode, errorCode3, e);
                    Util.a(this.b);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                errorCode = errorCode3;
                this.c.a(errorCode, errorCode3, e);
                Util.a(this.b);
                throw th;
            }
            this.c.a(errorCode, errorCode2, e);
            Util.a(this.b);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: o.m0.g.Http2Stream.a(o.Headers, boolean):void
         arg types: [o.Headers, int]
         candidates:
          o.m0.g.Http2Stream.a(o.m0.g.ErrorCode, java.io.IOException):void
          o.m0.g.Http2Stream.a(o.Headers, boolean):void */
        public void a(boolean z, int i2, BufferedSource bufferedSource, int i3) {
            if (bufferedSource == null) {
                Intrinsics.a("source");
                throw null;
            } else if (this.c.b(i2)) {
                Http2Connection http2Connection = this.c;
                if (http2Connection != null) {
                    Buffer buffer = new Buffer();
                    long j2 = (long) i3;
                    bufferedSource.g(j2);
                    bufferedSource.b(buffer, j2);
                    if (!http2Connection.h) {
                        ThreadPoolExecutor threadPoolExecutor = http2Connection.f3001j;
                        StringBuilder a2 = outline.a("OkHttp ");
                        a2.append(http2Connection.f2998e);
                        a2.append(" Push Data[");
                        a2.append(i2);
                        a2.append(']');
                        threadPoolExecutor.execute(new Util(a2.toString(), http2Connection, i2, buffer, i3, z));
                        return;
                    }
                    return;
                }
                throw null;
            } else {
                Http2Stream a3 = this.c.a(i2);
                if (a3 == null) {
                    this.c.a(i2, ErrorCode.PROTOCOL_ERROR);
                    long j3 = (long) i3;
                    this.c.e(j3);
                    bufferedSource.skip(j3);
                    return;
                }
                boolean z2 = !Thread.holdsLock(a3);
                if (!AssertionsJVM.a || z2) {
                    a3.g.a(bufferedSource, (long) i3);
                    if (z) {
                        a3.a(Util.b, true);
                        return;
                    }
                    return;
                }
                throw new AssertionError("Assertion failed");
            }
        }

        public void a(boolean z, int i2, int i3, List<b> list) {
            int i4 = i2;
            if (list == null) {
                Intrinsics.a("headerBlock");
                throw null;
            } else if (this.c.b(i2)) {
                Http2Connection http2Connection = this.c;
                if (!http2Connection.h) {
                    ThreadPoolExecutor threadPoolExecutor = http2Connection.f3001j;
                    StringBuilder a2 = outline.a("OkHttp ");
                    a2.append(http2Connection.f2998e);
                    a2.append(" Push Headers[");
                    a2.append(i2);
                    a2.append(']');
                    try {
                        threadPoolExecutor.execute(new Util0(a2.toString(), http2Connection, i2, list, z));
                    } catch (RejectedExecutionException unused) {
                    }
                }
            } else {
                synchronized (this.c) {
                    Http2Stream a3 = this.c.a(i2);
                    if (a3 != null) {
                        a3.a(Util.a(list), z);
                    } else if (!this.c.a()) {
                        if (i4 > this.c.f2999f) {
                            if (i4 % 2 != this.c.g % 2) {
                                int i5 = i2;
                                Http2Stream http2Stream = new Http2Stream(i5, this.c, false, z, Util.a(list));
                                this.c.f2999f = i4;
                                this.c.d.put(Integer.valueOf(i2), http2Stream);
                                ThreadPoolExecutor threadPoolExecutor2 = Http2Connection.w;
                                threadPoolExecutor2.execute(new b("OkHttp " + this.c.f2998e + " stream " + i2, http2Stream, this, a3, i2, list, z));
                            }
                        }
                    }
                }
            }
        }

        public void a(int i2, ErrorCode errorCode) {
            if (errorCode == null) {
                Intrinsics.a("errorCode");
                throw null;
            } else if (this.c.b(i2)) {
                Http2Connection http2Connection = this.c;
                if (!http2Connection.h) {
                    ThreadPoolExecutor threadPoolExecutor = http2Connection.f3001j;
                    StringBuilder a2 = outline.a("OkHttp ");
                    a2.append(http2Connection.f2998e);
                    a2.append(" Push Reset[");
                    a2.append(i2);
                    a2.append(']');
                    threadPoolExecutor.execute(new Util1(a2.toString(), http2Connection, i2, errorCode));
                }
            } else {
                Http2Stream c2 = this.c.c(i2);
                if (c2 != null) {
                    c2.b(errorCode);
                }
            }
        }

        public void a(boolean z, Settings settings) {
            if (settings != null) {
                try {
                    this.c.f3000i.execute(new C0044d(outline.a(outline.a("OkHttp "), this.c.f2998e, " ACK Settings"), this, z, settings));
                } catch (RejectedExecutionException unused) {
                }
            } else {
                Intrinsics.a(AnswersPreferenceManager.PREF_STORE_NAME);
                throw null;
            }
        }

        public void a(boolean z, int i2, int i3) {
            if (z) {
                synchronized (this.c) {
                    this.c.f3003l = false;
                    Http2Connection http2Connection = this.c;
                    if (http2Connection != null) {
                        http2Connection.notifyAll();
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type java.lang.Object");
                    }
                }
                return;
            }
            try {
                this.c.f3000i.execute(new c(outline.a(outline.a("OkHttp "), this.c.f2998e, " ping"), this, i2, i3));
            } catch (RejectedExecutionException unused) {
            }
        }

        public void a(int i2, long j2) {
            if (i2 == 0) {
                synchronized (this.c) {
                    this.c.f3009r += j2;
                    Http2Connection http2Connection = this.c;
                    if (http2Connection != null) {
                        http2Connection.notifyAll();
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type java.lang.Object");
                    }
                }
                return;
            }
            Http2Stream a2 = this.c.a(i2);
            if (a2 != null) {
                synchronized (a2) {
                    a2.d += j2;
                    if (j2 > 0) {
                        a2.notifyAll();
                    }
                }
            }
        }

        public void a(int i2, int i3, List<b> list) {
            if (list != null) {
                this.c.a(i3, list);
            } else {
                Intrinsics.a("requestHeaders");
                throw null;
            }
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(3:26|27|28) */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r3 = java.lang.Math.min((int) java.lang.Math.min(r12, r8.f3009r - r8.f3008q), r8.f3011t.c);
        r8.f3008q += (long) r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        java.lang.Thread.currentThread().interrupt();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x006a, code lost:
        throw new java.io.InterruptedIOException();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:26:0x005e */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(int r9, boolean r10, p.Buffer r11, long r12) {
        /*
            r8 = this;
            r0 = 0
            r1 = 0
            int r3 = (r12 > r1 ? 1 : (r12 == r1 ? 0 : -1))
            if (r3 != 0) goto L_0x000d
            o.m0.g.Http2Writer r12 = r8.f3011t
            r12.a(r10, r9, r11, r0)
            return
        L_0x000d:
            int r3 = (r12 > r1 ? 1 : (r12 == r1 ? 0 : -1))
            if (r3 <= 0) goto L_0x006d
            monitor-enter(r8)
        L_0x0012:
            long r3 = r8.f3008q     // Catch:{ InterruptedException -> 0x005e }
            long r5 = r8.f3009r     // Catch:{ InterruptedException -> 0x005e }
            int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r7 < 0) goto L_0x0032
            java.util.Map<java.lang.Integer, o.m0.g.k> r3 = r8.d     // Catch:{ InterruptedException -> 0x005e }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r9)     // Catch:{ InterruptedException -> 0x005e }
            boolean r3 = r3.containsKey(r4)     // Catch:{ InterruptedException -> 0x005e }
            if (r3 == 0) goto L_0x002a
            r8.wait()     // Catch:{ InterruptedException -> 0x005e }
            goto L_0x0012
        L_0x002a:
            java.io.IOException r9 = new java.io.IOException     // Catch:{ InterruptedException -> 0x005e }
            java.lang.String r10 = "stream closed"
            r9.<init>(r10)     // Catch:{ InterruptedException -> 0x005e }
            throw r9     // Catch:{ InterruptedException -> 0x005e }
        L_0x0032:
            long r3 = r8.f3009r     // Catch:{ all -> 0x005c }
            long r5 = r8.f3008q     // Catch:{ all -> 0x005c }
            long r3 = r3 - r5
            long r3 = java.lang.Math.min(r12, r3)     // Catch:{ all -> 0x005c }
            int r4 = (int) r3     // Catch:{ all -> 0x005c }
            o.m0.g.Http2Writer r3 = r8.f3011t     // Catch:{ all -> 0x005c }
            int r3 = r3.c     // Catch:{ all -> 0x005c }
            int r3 = java.lang.Math.min(r4, r3)     // Catch:{ all -> 0x005c }
            long r4 = r8.f3008q     // Catch:{ all -> 0x005c }
            long r6 = (long) r3     // Catch:{ all -> 0x005c }
            long r4 = r4 + r6
            r8.f3008q = r4     // Catch:{ all -> 0x005c }
            monitor-exit(r8)
            long r4 = (long) r3
            long r12 = r12 - r4
            o.m0.g.Http2Writer r4 = r8.f3011t
            if (r10 == 0) goto L_0x0057
            int r5 = (r12 > r1 ? 1 : (r12 == r1 ? 0 : -1))
            if (r5 != 0) goto L_0x0057
            r5 = 1
            goto L_0x0058
        L_0x0057:
            r5 = 0
        L_0x0058:
            r4.a(r5, r9, r11, r3)
            goto L_0x000d
        L_0x005c:
            r9 = move-exception
            goto L_0x006b
        L_0x005e:
            java.lang.Thread r9 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x005c }
            r9.interrupt()     // Catch:{ all -> 0x005c }
            java.io.InterruptedIOException r9 = new java.io.InterruptedIOException     // Catch:{ all -> 0x005c }
            r9.<init>()     // Catch:{ all -> 0x005c }
            throw r9     // Catch:{ all -> 0x005c }
        L_0x006b:
            monitor-exit(r8)
            throw r9
        L_0x006d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: o.m0.g.Http2Connection.a(int, boolean, p.Buffer, long):void");
    }

    public final void a(int i2, ErrorCode errorCode) {
        if (errorCode != null) {
            ScheduledThreadPoolExecutor scheduledThreadPoolExecutor = this.f3000i;
            StringBuilder a2 = outline.a("OkHttp ");
            a2.append(this.f2998e);
            a2.append(" stream ");
            a2.append(i2);
            try {
                scheduledThreadPoolExecutor.execute(new f(a2.toString(), this, i2, errorCode));
            } catch (RejectedExecutionException unused) {
            }
        } else {
            Intrinsics.a("errorCode");
            throw null;
        }
    }

    public final void a(int i2, long j2) {
        ScheduledThreadPoolExecutor scheduledThreadPoolExecutor = this.f3000i;
        StringBuilder a2 = outline.a("OkHttp Window Update ");
        a2.append(this.f2998e);
        a2.append(" stream ");
        a2.append(i2);
        try {
            scheduledThreadPoolExecutor.execute(new g(a2.toString(), this, i2, j2));
        } catch (RejectedExecutionException unused) {
        }
    }

    public final void a(boolean z, int i2, int i3) {
        boolean z2;
        if (!z) {
            synchronized (this) {
                z2 = this.f3003l;
                this.f3003l = true;
            }
            if (z2) {
                ErrorCode errorCode = ErrorCode.PROTOCOL_ERROR;
                a(errorCode, errorCode, (IOException) null);
                return;
            }
        }
        try {
            this.f3011t.a(z, i2, i3);
        } catch (IOException e2) {
            ErrorCode errorCode2 = ErrorCode.PROTOCOL_ERROR;
            a(errorCode2, errorCode2, e2);
        }
    }

    public final void a(ErrorCode errorCode) {
        if (errorCode != null) {
            synchronized (this.f3011t) {
                synchronized (this) {
                    if (!this.h) {
                        this.h = true;
                        int i2 = this.f2999f;
                        this.f3011t.a(i2, errorCode, Util.a);
                        return;
                    }
                    return;
                }
            }
        }
        Intrinsics.a("statusCode");
        throw null;
    }

    /* JADX WARN: Type inference failed for: r4v14, types: [java.lang.Object[]] */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x005b */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(o.m0.g.ErrorCode r4, o.m0.g.ErrorCode r5, java.io.IOException r6) {
        /*
            r3 = this;
            r0 = 0
            if (r4 == 0) goto L_0x0074
            if (r5 == 0) goto L_0x006e
            boolean r1 = java.lang.Thread.holdsLock(r3)
            r1 = r1 ^ 1
            boolean r2 = n.AssertionsJVM.a
            if (r2 == 0) goto L_0x001a
            if (r1 == 0) goto L_0x0012
            goto L_0x001a
        L_0x0012:
            java.lang.AssertionError r4 = new java.lang.AssertionError
            java.lang.String r5 = "Assertion failed"
            r4.<init>(r5)
            throw r4
        L_0x001a:
            r3.a(r4)     // Catch:{ IOException -> 0x001d }
        L_0x001d:
            monitor-enter(r3)
            java.util.Map<java.lang.Integer, o.m0.g.k> r4 = r3.d     // Catch:{ all -> 0x006b }
            boolean r4 = r4.isEmpty()     // Catch:{ all -> 0x006b }
            r4 = r4 ^ 1
            r1 = 0
            if (r4 == 0) goto L_0x0048
            java.util.Map<java.lang.Integer, o.m0.g.k> r4 = r3.d     // Catch:{ all -> 0x006b }
            java.util.Collection r4 = r4.values()     // Catch:{ all -> 0x006b }
            o.m0.g.Http2Stream[] r0 = new o.m0.g.Http2Stream[r1]     // Catch:{ all -> 0x006b }
            java.lang.Object[] r4 = r4.toArray(r0)     // Catch:{ all -> 0x006b }
            if (r4 == 0) goto L_0x0040
            r0 = r4
            o.m0.g.Http2Stream[] r0 = (o.m0.g.Http2Stream[]) r0     // Catch:{ all -> 0x006b }
            java.util.Map<java.lang.Integer, o.m0.g.k> r4 = r3.d     // Catch:{ all -> 0x006b }
            r4.clear()     // Catch:{ all -> 0x006b }
            goto L_0x0048
        L_0x0040:
            kotlin.TypeCastException r4 = new kotlin.TypeCastException     // Catch:{ all -> 0x006b }
            java.lang.String r5 = "null cannot be cast to non-null type kotlin.Array<T>"
            r4.<init>(r5)     // Catch:{ all -> 0x006b }
            throw r4     // Catch:{ all -> 0x006b }
        L_0x0048:
            monitor-exit(r3)
            if (r0 == 0) goto L_0x0056
            int r4 = r0.length
        L_0x004c:
            if (r1 >= r4) goto L_0x0056
            r2 = r0[r1]
            r2.a(r5, r6)     // Catch:{ IOException -> 0x0053 }
        L_0x0053:
            int r1 = r1 + 1
            goto L_0x004c
        L_0x0056:
            o.m0.g.Http2Writer r4 = r3.f3011t     // Catch:{ IOException -> 0x005b }
            r4.close()     // Catch:{ IOException -> 0x005b }
        L_0x005b:
            java.net.Socket r4 = r3.f3010s     // Catch:{ IOException -> 0x0060 }
            r4.close()     // Catch:{ IOException -> 0x0060 }
        L_0x0060:
            java.util.concurrent.ScheduledThreadPoolExecutor r4 = r3.f3000i
            r4.shutdown()
            java.util.concurrent.ThreadPoolExecutor r4 = r3.f3001j
            r4.shutdown()
            return
        L_0x006b:
            r4 = move-exception
            monitor-exit(r3)
            throw r4
        L_0x006e:
            java.lang.String r4 = "streamCode"
            n.n.c.Intrinsics.a(r4)
            throw r0
        L_0x0074:
            java.lang.String r4 = "connectionCode"
            n.n.c.Intrinsics.a(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: o.m0.g.Http2Connection.a(o.m0.g.ErrorCode, o.m0.g.ErrorCode, java.io.IOException):void");
    }

    public static final /* synthetic */ void a(Http2Connection http2Connection, IOException iOException) {
        if (http2Connection != null) {
            ErrorCode errorCode = ErrorCode.PROTOCOL_ERROR;
            http2Connection.a(errorCode, errorCode, iOException);
            return;
        }
        throw null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0022, code lost:
        if (r3.h != false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0024, code lost:
        r0 = r3.f3001j;
        r1 = j.a.a.a.outline.a("OkHttp ");
        r1.append(r3.f2998e);
        r1.append(" Push Request[");
        r1.append(r4);
        r1.append(']');
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        r0.execute(new o.m0.g.Http2Connection.e(r1.toString(), r3, r4, r5));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(int r4, java.util.List<o.m0.g.b> r5) {
        /*
            r3 = this;
            if (r5 == 0) goto L_0x004e
            monitor-enter(r3)
            java.util.Set<java.lang.Integer> r0 = r3.v     // Catch:{ all -> 0x004b }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x004b }
            boolean r0 = r0.contains(r1)     // Catch:{ all -> 0x004b }
            if (r0 == 0) goto L_0x0016
            o.m0.g.ErrorCode r5 = o.m0.g.ErrorCode.PROTOCOL_ERROR     // Catch:{ all -> 0x004b }
            r3.a(r4, r5)     // Catch:{ all -> 0x004b }
            monitor-exit(r3)
            return
        L_0x0016:
            java.util.Set<java.lang.Integer> r0 = r3.v     // Catch:{ all -> 0x004b }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x004b }
            r0.add(r1)     // Catch:{ all -> 0x004b }
            monitor-exit(r3)
            boolean r0 = r3.h
            if (r0 != 0) goto L_0x004a
            java.util.concurrent.ThreadPoolExecutor r0 = r3.f3001j
            java.lang.String r1 = "OkHttp "
            java.lang.StringBuilder r1 = j.a.a.a.outline.a(r1)
            java.lang.String r2 = r3.f2998e
            r1.append(r2)
            java.lang.String r2 = " Push Request["
            r1.append(r2)
            r1.append(r4)
            r2 = 93
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            o.m0.g.Http2Connection$e r2 = new o.m0.g.Http2Connection$e     // Catch:{ RejectedExecutionException -> 0x004a }
            r2.<init>(r1, r3, r4, r5)     // Catch:{ RejectedExecutionException -> 0x004a }
            r0.execute(r2)     // Catch:{ RejectedExecutionException -> 0x004a }
        L_0x004a:
            return
        L_0x004b:
            r4 = move-exception
            monitor-exit(r3)
            throw r4
        L_0x004e:
            java.lang.String r4 = "requestHeaders"
            n.n.c.Intrinsics.a(r4)
            r4 = 0
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: o.m0.g.Http2Connection.a(int, java.util.List):void");
    }
}
