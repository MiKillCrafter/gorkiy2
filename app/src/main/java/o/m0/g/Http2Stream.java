package o.m0.g;

import java.io.EOFException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.SocketTimeoutException;
import java.util.ArrayDeque;
import kotlin.TypeCastException;
import n.AssertionsJVM;
import n.n.c.Intrinsics;
import o.Headers;
import o.w;
import okhttp3.internal.http2.StreamResetException;
import p.AsyncTimeout;
import p.Buffer;
import p.BufferedSource;
import p.Sink;
import p.Source;
import p.Timeout;

/* compiled from: Http2Stream.kt */
public final class Http2Stream {
    public long a;
    public long b;
    public long c;
    public long d;

    /* renamed from: e  reason: collision with root package name */
    public final ArrayDeque<w> f3025e;

    /* renamed from: f  reason: collision with root package name */
    public boolean f3026f;
    public final b g;
    public final a h;

    /* renamed from: i  reason: collision with root package name */
    public final c f3027i;

    /* renamed from: j  reason: collision with root package name */
    public final c f3028j;

    /* renamed from: k  reason: collision with root package name */
    public ErrorCode f3029k;

    /* renamed from: l  reason: collision with root package name */
    public IOException f3030l;

    /* renamed from: m  reason: collision with root package name */
    public final int f3031m;

    /* renamed from: n  reason: collision with root package name */
    public final Http2Connection f3032n;

    /* compiled from: Http2Stream.kt */
    public final class c extends AsyncTimeout {
        public c() {
        }

        public IOException b(IOException iOException) {
            SocketTimeoutException socketTimeoutException = new SocketTimeoutException("timeout");
            if (iOException != null) {
                socketTimeoutException.initCause(iOException);
            }
            return socketTimeoutException;
        }

        public void h() {
            Http2Stream.this.a(ErrorCode.CANCEL);
        }

        public final void i() {
            if (g()) {
                throw b(null);
            }
        }
    }

    public Http2Stream(int i2, Http2Connection http2Connection, boolean z, boolean z2, Headers headers) {
        if (http2Connection != null) {
            this.f3031m = i2;
            this.f3032n = http2Connection;
            this.d = (long) http2Connection.f3005n.a();
            this.f3025e = new ArrayDeque<>();
            this.g = new b((long) this.f3032n.f3004m.a(), z2);
            this.h = new a(z);
            this.f3027i = new c();
            this.f3028j = new c();
            if (headers != null) {
                if (!e()) {
                    this.f3025e.add(headers);
                    return;
                }
                throw new IllegalStateException("locally-initiated streams shouldn't have headers yet".toString());
            } else if (!e()) {
                throw new IllegalStateException("remotely-initiated streams should have headers".toString());
            }
        } else {
            Intrinsics.a("connection");
            throw null;
        }
    }

    public final void a(ErrorCode errorCode, IOException iOException) {
        if (errorCode == null) {
            Intrinsics.a("rstStatusCode");
            throw null;
        } else if (b(errorCode, iOException)) {
            Http2Connection http2Connection = this.f3032n;
            http2Connection.f3011t.a(this.f3031m, errorCode);
        }
    }

    public final boolean b(ErrorCode errorCode, IOException iOException) {
        boolean z = !Thread.holdsLock(this);
        if (!AssertionsJVM.a || z) {
            synchronized (this) {
                if (this.f3029k != null) {
                    return false;
                }
                if (this.g.f3035f && this.h.d) {
                    return false;
                }
                this.f3029k = errorCode;
                this.f3030l = iOException;
                notifyAll();
                this.f3032n.c(this.f3031m);
                return true;
            }
        }
        throw new AssertionError("Assertion failed");
    }

    public final synchronized ErrorCode c() {
        return this.f3029k;
    }

    public final Sink d() {
        synchronized (this) {
            if (!(this.f3026f || e())) {
                throw new IllegalStateException("reply before requesting the sink".toString());
            }
        }
        return this.h;
    }

    public final boolean e() {
        if (this.f3032n.b == ((this.f3031m & 1) == 1)) {
            return true;
        }
        return false;
    }

    public final synchronized boolean f() {
        if (this.f3029k != null) {
            return false;
        }
        if ((this.g.f3035f || this.g.d) && ((this.h.d || this.h.c) && this.f3026f)) {
            return false;
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [o.Headers, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0047, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0048, code lost:
        r2.f3027i.i();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x004d, code lost:
        throw r0;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized o.Headers g() {
        /*
            r2 = this;
            monitor-enter(r2)
            o.m0.g.Http2Stream$c r0 = r2.f3027i     // Catch:{ all -> 0x004e }
            r0.f()     // Catch:{ all -> 0x004e }
        L_0x0006:
            java.util.ArrayDeque<o.w> r0 = r2.f3025e     // Catch:{ all -> 0x0047 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0047 }
            if (r0 == 0) goto L_0x0016
            o.m0.g.ErrorCode r0 = r2.f3029k     // Catch:{ all -> 0x0047 }
            if (r0 != 0) goto L_0x0016
            r2.h()     // Catch:{ all -> 0x0047 }
            goto L_0x0006
        L_0x0016:
            o.m0.g.Http2Stream$c r0 = r2.f3027i     // Catch:{ all -> 0x004e }
            r0.i()     // Catch:{ all -> 0x004e }
            java.util.ArrayDeque<o.w> r0 = r2.f3025e     // Catch:{ all -> 0x004e }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x004e }
            r0 = r0 ^ 1
            if (r0 == 0) goto L_0x0034
            java.util.ArrayDeque<o.w> r0 = r2.f3025e     // Catch:{ all -> 0x004e }
            java.lang.Object r0 = r0.removeFirst()     // Catch:{ all -> 0x004e }
            java.lang.String r1 = "headersQueue.removeFirst()"
            n.n.c.Intrinsics.a(r0, r1)     // Catch:{ all -> 0x004e }
            o.Headers r0 = (o.Headers) r0     // Catch:{ all -> 0x004e }
            monitor-exit(r2)
            return r0
        L_0x0034:
            java.io.IOException r0 = r2.f3030l     // Catch:{ all -> 0x004e }
            if (r0 != 0) goto L_0x0046
            okhttp3.internal.http2.StreamResetException r0 = new okhttp3.internal.http2.StreamResetException     // Catch:{ all -> 0x004e }
            o.m0.g.ErrorCode r1 = r2.f3029k     // Catch:{ all -> 0x004e }
            if (r1 != 0) goto L_0x0043
            n.n.c.Intrinsics.a()     // Catch:{ all -> 0x004e }
            r0 = 0
            throw r0
        L_0x0043:
            r0.<init>(r1)     // Catch:{ all -> 0x004e }
        L_0x0046:
            throw r0     // Catch:{ all -> 0x004e }
        L_0x0047:
            r0 = move-exception
            o.m0.g.Http2Stream$c r1 = r2.f3027i     // Catch:{ all -> 0x004e }
            r1.i()     // Catch:{ all -> 0x004e }
            throw r0     // Catch:{ all -> 0x004e }
        L_0x004e:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: o.m0.g.Http2Stream.g():o.Headers");
    }

    public final void h() {
        try {
            wait();
        } catch (InterruptedException unused) {
            Thread.currentThread().interrupt();
            throw new InterruptedIOException();
        }
    }

    public final void a(ErrorCode errorCode) {
        if (errorCode == null) {
            Intrinsics.a("errorCode");
            throw null;
        } else if (b(errorCode, null)) {
            this.f3032n.a(this.f3031m, errorCode);
        }
    }

    /* compiled from: Http2Stream.kt */
    public final class a implements Sink {
        public final Buffer b = new Buffer();
        public boolean c;
        public boolean d;

        public a(boolean z) {
            this.d = z;
        }

        public void a(Buffer buffer, long j2) {
            if (buffer != null) {
                boolean z = !Thread.holdsLock(Http2Stream.this);
                if (!AssertionsJVM.a || z) {
                    this.b.a(buffer, j2);
                    while (this.b.c >= 16384) {
                        a(false);
                    }
                    return;
                }
                throw new AssertionError("Assertion failed");
            }
            Intrinsics.a("source");
            throw null;
        }

        public Timeout b() {
            return Http2Stream.this.f3028j;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0027, code lost:
            if (r8.f3033e.h.d != false) goto L_0x0051;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0031, code lost:
            if (r8.b.c <= 0) goto L_0x0035;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x0033, code lost:
            r0 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x0035, code lost:
            r0 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x0036, code lost:
            if (r0 == false) goto L_0x0044;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x003e, code lost:
            if (r8.b.c <= 0) goto L_0x0051;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x0040, code lost:
            a(true);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x0044, code lost:
            r0 = r8.f3033e;
            r0.f3032n.a(r0.f3031m, true, null, 0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x0051, code lost:
            r0 = r8.f3033e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x0053, code lost:
            monitor-enter(r0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
            r8.c = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x0056, code lost:
            monitor-exit(r0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x0057, code lost:
            r8.f3033e.f3032n.f3011t.flush();
            r8.f3033e.a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x0065, code lost:
            return;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void close() {
            /*
                r8 = this;
                o.m0.g.Http2Stream r0 = o.m0.g.Http2Stream.this
                boolean r0 = java.lang.Thread.holdsLock(r0)
                r1 = 1
                r0 = r0 ^ r1
                boolean r2 = n.AssertionsJVM.a
                if (r2 == 0) goto L_0x0017
                if (r0 == 0) goto L_0x000f
                goto L_0x0017
            L_0x000f:
                java.lang.AssertionError r0 = new java.lang.AssertionError
                java.lang.String r1 = "Assertion failed"
                r0.<init>(r1)
                throw r0
            L_0x0017:
                o.m0.g.Http2Stream r0 = o.m0.g.Http2Stream.this
                monitor-enter(r0)
                boolean r2 = r8.c     // Catch:{ all -> 0x0069 }
                if (r2 == 0) goto L_0x0020
                monitor-exit(r0)
                return
            L_0x0020:
                monitor-exit(r0)
                o.m0.g.Http2Stream r0 = o.m0.g.Http2Stream.this
                o.m0.g.Http2Stream$a r0 = r0.h
                boolean r0 = r0.d
                if (r0 != 0) goto L_0x0051
                p.Buffer r0 = r8.b
                long r2 = r0.c
                r4 = 0
                int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                if (r0 <= 0) goto L_0x0035
                r0 = 1
                goto L_0x0036
            L_0x0035:
                r0 = 0
            L_0x0036:
                if (r0 == 0) goto L_0x0044
            L_0x0038:
                p.Buffer r0 = r8.b
                long r2 = r0.c
                int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                if (r0 <= 0) goto L_0x0051
                r8.a(r1)
                goto L_0x0038
            L_0x0044:
                o.m0.g.Http2Stream r0 = o.m0.g.Http2Stream.this
                o.m0.g.Http2Connection r2 = r0.f3032n
                int r3 = r0.f3031m
                r4 = 1
                r5 = 0
                r6 = 0
                r2.a(r3, r4, r5, r6)
            L_0x0051:
                o.m0.g.Http2Stream r0 = o.m0.g.Http2Stream.this
                monitor-enter(r0)
                r8.c = r1     // Catch:{ all -> 0x0066 }
                monitor-exit(r0)
                o.m0.g.Http2Stream r0 = o.m0.g.Http2Stream.this
                o.m0.g.Http2Connection r0 = r0.f3032n
                o.m0.g.Http2Writer r0 = r0.f3011t
                r0.flush()
                o.m0.g.Http2Stream r0 = o.m0.g.Http2Stream.this
                r0.a()
                return
            L_0x0066:
                r1 = move-exception
                monitor-exit(r0)
                throw r1
            L_0x0069:
                r1 = move-exception
                monitor-exit(r0)
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: o.m0.g.Http2Stream.a.close():void");
        }

        public void flush() {
            boolean z = !Thread.holdsLock(Http2Stream.this);
            if (!AssertionsJVM.a || z) {
                synchronized (Http2Stream.this) {
                    Http2Stream.this.b();
                }
                while (this.b.c > 0) {
                    a(false);
                    Http2Stream.this.f3032n.f3011t.flush();
                }
                return;
            }
            throw new AssertionError("Assertion failed");
        }

        /* JADX INFO: finally extract failed */
        public final void a(boolean z) {
            long min;
            boolean z2;
            synchronized (Http2Stream.this) {
                Http2Stream.this.f3028j.f();
                while (Http2Stream.this.c >= Http2Stream.this.d && !this.d && !this.c && Http2Stream.this.c() == null) {
                    try {
                        Http2Stream.this.h();
                    } catch (Throwable th) {
                        Http2Stream.this.f3028j.i();
                        throw th;
                    }
                }
                Http2Stream.this.f3028j.i();
                Http2Stream.this.b();
                min = Math.min(Http2Stream.this.d - Http2Stream.this.c, this.b.c);
                Http2Stream.this.c += min;
            }
            Http2Stream.this.f3028j.f();
            if (z) {
                try {
                    if (min == this.b.c) {
                        z2 = true;
                        Http2Stream.this.f3032n.a(Http2Stream.this.f3031m, z2, this.b, min);
                        Http2Stream.this.f3028j.i();
                    }
                } catch (Throwable th2) {
                    Http2Stream.this.f3028j.i();
                    throw th2;
                }
            }
            z2 = false;
            Http2Stream.this.f3032n.a(Http2Stream.this.f3031m, z2, this.b, min);
            Http2Stream.this.f3028j.i();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x002f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(o.Headers r5, boolean r6) {
        /*
            r4 = this;
            r0 = 0
            if (r5 == 0) goto L_0x0048
            boolean r1 = java.lang.Thread.holdsLock(r4)
            r2 = 1
            r1 = r1 ^ r2
            boolean r3 = n.AssertionsJVM.a
            if (r3 == 0) goto L_0x0018
            if (r1 == 0) goto L_0x0010
            goto L_0x0018
        L_0x0010:
            java.lang.AssertionError r5 = new java.lang.AssertionError
            java.lang.String r6 = "Assertion failed"
            r5.<init>(r6)
            throw r5
        L_0x0018:
            monitor-enter(r4)
            boolean r1 = r4.f3026f     // Catch:{ all -> 0x0045 }
            if (r1 == 0) goto L_0x0026
            if (r6 != 0) goto L_0x0020
            goto L_0x0026
        L_0x0020:
            o.m0.g.Http2Stream$b r5 = r4.g     // Catch:{ all -> 0x0045 }
            if (r5 == 0) goto L_0x0025
            goto L_0x002d
        L_0x0025:
            throw r0     // Catch:{ all -> 0x0045 }
        L_0x0026:
            r4.f3026f = r2     // Catch:{ all -> 0x0045 }
            java.util.ArrayDeque<o.w> r0 = r4.f3025e     // Catch:{ all -> 0x0045 }
            r0.add(r5)     // Catch:{ all -> 0x0045 }
        L_0x002d:
            if (r6 == 0) goto L_0x0033
            o.m0.g.Http2Stream$b r5 = r4.g     // Catch:{ all -> 0x0045 }
            r5.f3035f = r2     // Catch:{ all -> 0x0045 }
        L_0x0033:
            boolean r5 = r4.f()     // Catch:{ all -> 0x0045 }
            r4.notifyAll()     // Catch:{ all -> 0x0045 }
            monitor-exit(r4)
            if (r5 != 0) goto L_0x0044
            o.m0.g.Http2Connection r5 = r4.f3032n
            int r6 = r4.f3031m
            r5.c(r6)
        L_0x0044:
            return
        L_0x0045:
            r5 = move-exception
            monitor-exit(r4)
            throw r5
        L_0x0048:
            java.lang.String r5 = "headers"
            n.n.c.Intrinsics.a(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: o.m0.g.Http2Stream.a(o.Headers, boolean):void");
    }

    public final synchronized void b(ErrorCode errorCode) {
        if (errorCode == null) {
            Intrinsics.a("errorCode");
            throw null;
        } else if (this.f3029k == null) {
            this.f3029k = errorCode;
            notifyAll();
        }
    }

    public final void b() {
        a aVar = this.h;
        if (aVar.c) {
            throw new IOException("stream closed");
        } else if (aVar.d) {
            throw new IOException("stream finished");
        } else if (this.f3029k != null) {
            Throwable th = this.f3030l;
            if (th == null) {
                ErrorCode errorCode = this.f3029k;
                if (errorCode == null) {
                    Intrinsics.a();
                    throw null;
                }
                th = new StreamResetException(errorCode);
            }
            throw th;
        }
    }

    public final void a() {
        boolean f2;
        boolean z = true;
        boolean z2 = !Thread.holdsLock(this);
        if (!AssertionsJVM.a || z2) {
            synchronized (this) {
                if (!this.g.f3035f && this.g.d) {
                    if (!this.h.d) {
                        if (this.h.c) {
                        }
                    }
                    f2 = f();
                }
                z = false;
                f2 = f();
            }
            if (z) {
                a(ErrorCode.CANCEL, (IOException) null);
            } else if (!f2) {
                this.f3032n.c(this.f3031m);
            }
        } else {
            throw new AssertionError("Assertion failed");
        }
    }

    /* compiled from: Http2Stream.kt */
    public final class b implements Source {
        public final Buffer b = new Buffer();
        public final Buffer c = new Buffer();
        public boolean d;

        /* renamed from: e  reason: collision with root package name */
        public final long f3034e;

        /* renamed from: f  reason: collision with root package name */
        public boolean f3035f;

        public b(long j2, boolean z) {
            this.f3034e = j2;
            this.f3035f = z;
        }

        public final void a(BufferedSource bufferedSource, long j2) {
            boolean z;
            boolean z2;
            boolean z3;
            long j3;
            if (bufferedSource != null) {
                boolean z4 = !Thread.holdsLock(Http2Stream.this);
                if (!AssertionsJVM.a || z4) {
                    while (j2 > 0) {
                        synchronized (Http2Stream.this) {
                            z = this.f3035f;
                            z2 = false;
                            z3 = this.c.c + j2 > this.f3034e;
                        }
                        if (z3) {
                            bufferedSource.skip(j2);
                            Http2Stream.this.a(ErrorCode.FLOW_CONTROL_ERROR);
                            return;
                        } else if (z) {
                            bufferedSource.skip(j2);
                            return;
                        } else {
                            long b2 = bufferedSource.b(this.b, j2);
                            if (b2 != -1) {
                                j2 -= b2;
                                synchronized (Http2Stream.this) {
                                    if (this.d) {
                                        j3 = this.b.c;
                                        Buffer buffer = this.b;
                                        buffer.skip(buffer.c);
                                    } else {
                                        if (this.c.c == 0) {
                                            z2 = true;
                                        }
                                        this.c.a((Source) this.b);
                                        if (z2) {
                                            Http2Stream http2Stream = Http2Stream.this;
                                            if (http2Stream != null) {
                                                http2Stream.notifyAll();
                                            } else {
                                                throw new TypeCastException("null cannot be cast to non-null type java.lang.Object");
                                            }
                                        }
                                        j3 = 0;
                                    }
                                }
                                if (j3 > 0) {
                                    e(j3);
                                }
                            } else {
                                throw new EOFException();
                            }
                        }
                    }
                    return;
                }
                throw new AssertionError("Assertion failed");
            }
            Intrinsics.a("source");
            throw null;
        }

        public long b(Buffer buffer, long j2) {
            Throwable th;
            boolean z;
            long j3;
            Buffer buffer2 = buffer;
            long j4 = j2;
            if (buffer2 != null) {
                long j5 = 0;
                if (j4 >= 0) {
                    while (true) {
                        synchronized (Http2Stream.this) {
                            Http2Stream.this.f3027i.f();
                            try {
                                if (Http2Stream.this.c() != null) {
                                    th = Http2Stream.this.f3030l;
                                    if (th == null) {
                                        ErrorCode c2 = Http2Stream.this.c();
                                        if (c2 != null) {
                                            th = new StreamResetException(c2);
                                        } else {
                                            Intrinsics.a();
                                            throw null;
                                        }
                                    }
                                } else {
                                    th = null;
                                }
                                if (!this.d) {
                                    if (this.c.c > j5) {
                                        j3 = this.c.b(buffer2, Math.min(j4, this.c.c));
                                        Http2Stream.this.a += j3;
                                        long j6 = Http2Stream.this.a - Http2Stream.this.b;
                                        if (th == null && j6 >= ((long) (Http2Stream.this.f3032n.f3004m.a() / 2))) {
                                            Http2Stream.this.f3032n.a(Http2Stream.this.f3031m, j6);
                                            Http2Stream.this.b = Http2Stream.this.a;
                                        }
                                    } else if (this.f3035f || th != null) {
                                        j3 = -1;
                                    } else {
                                        Http2Stream.this.h();
                                        j3 = -1;
                                        z = true;
                                    }
                                    z = false;
                                } else {
                                    throw new IOException("stream closed");
                                }
                            } finally {
                                Http2Stream.this.f3027i.i();
                            }
                        }
                        if (z) {
                            j5 = 0;
                        } else if (j3 != -1) {
                            e(j3);
                            return j3;
                        } else if (th == null) {
                            return -1;
                        } else {
                            throw th;
                        }
                    }
                } else {
                    throw new IllegalArgumentException(("byteCount < 0: " + j4).toString());
                }
            } else {
                Intrinsics.a("sink");
                throw null;
            }
        }

        public void close() {
            long j2;
            synchronized (Http2Stream.this) {
                this.d = true;
                j2 = this.c.c;
                Buffer buffer = this.c;
                buffer.skip(buffer.c);
                Http2Stream http2Stream = Http2Stream.this;
                if (http2Stream != null) {
                    http2Stream.notifyAll();
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type java.lang.Object");
                }
            }
            if (j2 > 0) {
                e(j2);
            }
            Http2Stream.this.a();
        }

        public final void e(long j2) {
            boolean z = !Thread.holdsLock(Http2Stream.this);
            if (!AssertionsJVM.a || z) {
                Http2Stream.this.f3032n.e(j2);
                return;
            }
            throw new AssertionError("Assertion failed");
        }

        public Timeout b() {
            return Http2Stream.this.f3027i;
        }
    }
}
