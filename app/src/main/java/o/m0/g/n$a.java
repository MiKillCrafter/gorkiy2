package o.m0.g;

import java.util.List;
import n.n.c.Intrinsics;
import p.BufferedSource;

/* compiled from: PushObserver.kt */
public final class n$a implements PushObserver {
    public boolean a(int i2, BufferedSource bufferedSource, int i3, boolean z) {
        if (bufferedSource != null) {
            bufferedSource.skip((long) i3);
            return true;
        }
        Intrinsics.a("source");
        throw null;
    }

    public boolean a(int i2, List<b> list, boolean z) {
        if (list != null) {
            return true;
        }
        Intrinsics.a("responseHeaders");
        throw null;
    }

    public boolean a(int i2, List<b> list) {
        if (list != null) {
            return true;
        }
        Intrinsics.a("requestHeaders");
        throw null;
    }

    public void a(int i2, ErrorCode errorCode) {
        if (errorCode == null) {
            Intrinsics.a("errorCode");
            throw null;
        }
    }
}
