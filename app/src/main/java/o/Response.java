package o;

import com.crashlytics.android.core.DefaultAppMeasurementEventListenerRegistrar;
import j.a.a.a.outline;
import java.io.Closeable;
import n.n.c.Intrinsics;
import o.Headers;
import o.m0.d.Exchange;

/* compiled from: Response.kt */
public final class Response implements Closeable {
    public CacheControl b;
    public final Request c;
    public final Protocol d;

    /* renamed from: e  reason: collision with root package name */
    public final String f2900e;

    /* renamed from: f  reason: collision with root package name */
    public final int f2901f;
    public final Handshake g;
    public final Headers h;

    /* renamed from: i  reason: collision with root package name */
    public final ResponseBody f2902i;

    /* renamed from: j  reason: collision with root package name */
    public final Response f2903j;

    /* renamed from: k  reason: collision with root package name */
    public final Response f2904k;

    /* renamed from: l  reason: collision with root package name */
    public final Response f2905l;

    /* renamed from: m  reason: collision with root package name */
    public final long f2906m;

    /* renamed from: n  reason: collision with root package name */
    public final long f2907n;

    /* renamed from: o  reason: collision with root package name */
    public final Exchange f2908o;

    public Response(Request request, Protocol protocol, String str, int i2, Handshake handshake, Headers headers, ResponseBody responseBody, Response response, Response response2, Response response3, long j2, long j3, Exchange exchange) {
        Request request2 = request;
        Protocol protocol2 = protocol;
        String str2 = str;
        Headers headers2 = headers;
        if (request2 == null) {
            Intrinsics.a("request");
            throw null;
        } else if (protocol2 == null) {
            Intrinsics.a("protocol");
            throw null;
        } else if (str2 == null) {
            Intrinsics.a("message");
            throw null;
        } else if (headers2 != null) {
            this.c = request2;
            this.d = protocol2;
            this.f2900e = str2;
            this.f2901f = i2;
            this.g = handshake;
            this.h = headers2;
            this.f2902i = responseBody;
            this.f2903j = response;
            this.f2904k = response2;
            this.f2905l = response3;
            this.f2906m = j2;
            this.f2907n = j3;
            this.f2908o = exchange;
        } else {
            Intrinsics.a("headers");
            throw null;
        }
    }

    public static /* synthetic */ String a(Response response, String str, String str2, int i2) {
        if ((i2 & 2) != 0) {
            str2 = null;
        }
        if (str != null) {
            String a2 = response.h.a(str);
            return a2 != null ? a2 : str2;
        }
        Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
        throw null;
    }

    public void close() {
        ResponseBody responseBody = this.f2902i;
        if (responseBody != null) {
            responseBody.close();
            return;
        }
        throw new IllegalStateException("response is not eligible for a body and must not be closed".toString());
    }

    public final boolean f() {
        int i2 = this.f2901f;
        return 200 <= i2 && 299 >= i2;
    }

    public String toString() {
        StringBuilder a2 = outline.a("Response{protocol=");
        a2.append(this.d);
        a2.append(", code=");
        a2.append(this.f2901f);
        a2.append(", message=");
        a2.append(this.f2900e);
        a2.append(", url=");
        a2.append(this.c.b);
        a2.append('}');
        return a2.toString();
    }

    /* compiled from: Response.kt */
    public static class a {
        public Request a;
        public Protocol b;
        public int c;
        public String d;

        /* renamed from: e  reason: collision with root package name */
        public Handshake f2909e;

        /* renamed from: f  reason: collision with root package name */
        public Headers.a f2910f;
        public ResponseBody g;
        public Response h;

        /* renamed from: i  reason: collision with root package name */
        public Response f2911i;

        /* renamed from: j  reason: collision with root package name */
        public Response f2912j;

        /* renamed from: k  reason: collision with root package name */
        public long f2913k;

        /* renamed from: l  reason: collision with root package name */
        public long f2914l;

        /* renamed from: m  reason: collision with root package name */
        public Exchange f2915m;

        public a() {
            this.c = -1;
            this.f2910f = new Headers.a();
        }

        public final void a(String str, Response response) {
            if (response != null) {
                boolean z = false;
                if (response.f2902i == null) {
                    if (response.f2903j == null) {
                        if (response.f2904k == null) {
                            if (response.f2905l == null) {
                                z = true;
                            }
                            if (!z) {
                                throw new IllegalArgumentException(outline.a(str, ".priorResponse != null").toString());
                            }
                            return;
                        }
                        throw new IllegalArgumentException(outline.a(str, ".cacheResponse != null").toString());
                    }
                    throw new IllegalArgumentException(outline.a(str, ".networkResponse != null").toString());
                }
                throw new IllegalArgumentException(outline.a(str, ".body != null").toString());
            }
        }

        public a(Response response) {
            if (response != null) {
                this.c = -1;
                this.a = response.c;
                this.b = response.d;
                this.c = response.f2901f;
                this.d = response.f2900e;
                this.f2909e = response.g;
                this.f2910f = response.h.c();
                this.g = response.f2902i;
                this.h = response.f2903j;
                this.f2911i = response.f2904k;
                this.f2912j = response.f2905l;
                this.f2913k = response.f2906m;
                this.f2914l = response.f2907n;
                this.f2915m = response.f2908o;
                return;
            }
            Intrinsics.a("response");
            throw null;
        }

        public a a(Request request) {
            if (request != null) {
                this.a = request;
                return this;
            }
            Intrinsics.a("request");
            throw null;
        }

        public a a(Protocol protocol) {
            if (protocol != null) {
                this.b = protocol;
                return this;
            }
            Intrinsics.a("protocol");
            throw null;
        }

        public a a(String str) {
            if (str != null) {
                this.d = str;
                return this;
            }
            Intrinsics.a("message");
            throw null;
        }

        public a a(String str, String str2) {
            if (str == null) {
                Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
                throw null;
            } else if (str2 != null) {
                this.f2910f.a(str, str2);
                return this;
            } else {
                Intrinsics.a("value");
                throw null;
            }
        }

        public a a(Headers headers) {
            if (headers != null) {
                this.f2910f = headers.c();
                return this;
            }
            Intrinsics.a("headers");
            throw null;
        }

        public a a(Response response) {
            a("cacheResponse", response);
            this.f2911i = response;
            return this;
        }

        public Response a() {
            if (this.c >= 0) {
                Request request = this.a;
                if (request != null) {
                    Protocol protocol = this.b;
                    if (protocol != null) {
                        String str = this.d;
                        if (str != null) {
                            return new Response(request, protocol, str, this.c, this.f2909e, this.f2910f.a(), this.g, this.h, this.f2911i, this.f2912j, this.f2913k, this.f2914l, this.f2915m);
                        }
                        throw new IllegalStateException("message == null".toString());
                    }
                    throw new IllegalStateException("protocol == null".toString());
                }
                throw new IllegalStateException("request == null".toString());
            }
            StringBuilder a2 = outline.a("code < 0: ");
            a2.append(this.c);
            throw new IllegalStateException(a2.toString().toString());
        }
    }

    public final CacheControl a() {
        CacheControl cacheControl = this.b;
        if (cacheControl != null) {
            return cacheControl;
        }
        CacheControl a2 = CacheControl.f2812n.a(this.h);
        this.b = a2;
        return a2;
    }
}
