package o;

import j.a.a.a.outline;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;

/* compiled from: TlsVersion.kt */
public enum TlsVersion {
    TLS_1_3("TLSv1.3"),
    TLS_1_2("TLSv1.2"),
    TLS_1_1("TLSv1.1"),
    TLS_1_0("TLSv1"),
    SSL_3_0("SSLv3");
    
    public static final a Companion = new a(null);
    public final String javaName;

    /* compiled from: TlsVersion.kt */
    public static final class a {
        public /* synthetic */ a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final TlsVersion a(String str) {
            if (str != null) {
                int hashCode = str.hashCode();
                if (hashCode != 79201641) {
                    if (hashCode != 79923350) {
                        switch (hashCode) {
                            case -503070503:
                                if (str.equals("TLSv1.1")) {
                                    return TlsVersion.TLS_1_1;
                                }
                                break;
                            case -503070502:
                                if (str.equals("TLSv1.2")) {
                                    return TlsVersion.TLS_1_2;
                                }
                                break;
                            case -503070501:
                                if (str.equals("TLSv1.3")) {
                                    return TlsVersion.TLS_1_3;
                                }
                                break;
                        }
                    } else if (str.equals("TLSv1")) {
                        return TlsVersion.TLS_1_0;
                    }
                } else if (str.equals("SSLv3")) {
                    return TlsVersion.SSL_3_0;
                }
                throw new IllegalArgumentException(outline.a("Unexpected TLS version: ", str));
            }
            Intrinsics.a("javaName");
            throw null;
        }
    }

    /* access modifiers changed from: public */
    TlsVersion(String str) {
        this.javaName = str;
    }
}
