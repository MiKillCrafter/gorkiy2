package o;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;
import o.m0.Util;
import p.BufferedSource;

/* compiled from: ResponseBody.kt */
public abstract class ResponseBody implements Closeable {
    public static final b c = new b(null);
    public Reader b;

    /* compiled from: ResponseBody.kt */
    public static final class a extends Reader {
        public boolean b;
        public Reader c;
        public final BufferedSource d;

        /* renamed from: e  reason: collision with root package name */
        public final Charset f2916e;

        public a(BufferedSource bufferedSource, Charset charset) {
            if (bufferedSource == null) {
                Intrinsics.a("source");
                throw null;
            } else if (charset != null) {
                this.d = bufferedSource;
                this.f2916e = charset;
            } else {
                Intrinsics.a("charset");
                throw null;
            }
        }

        public void close() {
            this.b = true;
            Reader reader = this.c;
            if (reader != null) {
                super.close();
            } else {
                this.d.close();
            }
        }

        public int read(char[] cArr, int i2, int i3) {
            if (cArr == null) {
                Intrinsics.a("cbuf");
                throw null;
            } else if (!this.b) {
                Reader reader = this.c;
                if (reader == null) {
                    reader = new InputStreamReader(this.d.m(), Util.a(this.d, this.f2916e));
                    this.c = super;
                }
                return super.read(cArr, i2, i3);
            } else {
                throw new IOException("Stream closed");
            }
        }
    }

    /* compiled from: ResponseBody.kt */
    public static final class b {
        public /* synthetic */ b(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    public abstract long a();

    public void close() {
        Util.a((Closeable) g());
    }

    public abstract MediaType f();

    public abstract BufferedSource g();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
     arg types: [p.BufferedSource, ?[OBJECT, ARRAY]]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(java.lang.Object, java.lang.String):T
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
     arg types: [p.BufferedSource, java.lang.Throwable]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(java.lang.Object, java.lang.String):T
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0024, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0025, code lost:
        n.i.Collections.a((java.io.Closeable) r0, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0028, code lost:
        throw r2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String h() {
        /*
            r3 = this;
            p.BufferedSource r0 = r3.g()
            o.MediaType r1 = r3.f()     // Catch:{ all -> 0x0022 }
            if (r1 == 0) goto L_0x0013
            java.nio.charset.Charset r2 = n.r.Charsets.a     // Catch:{ all -> 0x0022 }
            java.nio.charset.Charset r1 = r1.a(r2)     // Catch:{ all -> 0x0022 }
            if (r1 == 0) goto L_0x0013
            goto L_0x0015
        L_0x0013:
            java.nio.charset.Charset r1 = n.r.Charsets.a     // Catch:{ all -> 0x0022 }
        L_0x0015:
            java.nio.charset.Charset r1 = o.m0.Util.a(r0, r1)     // Catch:{ all -> 0x0022 }
            java.lang.String r1 = r0.a(r1)     // Catch:{ all -> 0x0022 }
            r2 = 0
            n.i.Collections.a(r0, r2)
            return r1
        L_0x0022:
            r1 = move-exception
            throw r1     // Catch:{ all -> 0x0024 }
        L_0x0024:
            r2 = move-exception
            n.i.Collections.a(r0, r1)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: o.ResponseBody.h():java.lang.String");
    }
}
