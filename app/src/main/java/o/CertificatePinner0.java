package o;

import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import kotlin.TypeCastException;
import n.i.Collections;
import n.n.b.Functions;
import n.n.c.j;
import o.m0.k.CertificateChainCleaner;

/* compiled from: CertificatePinner.kt */
public final class CertificatePinner0 extends j implements Functions<List<? extends X509Certificate>> {
    public final /* synthetic */ CertificatePinner c;
    public final /* synthetic */ List d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ String f2820e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CertificatePinner0(CertificatePinner certificatePinner, List list, String str) {
        super(0);
        this.c = certificatePinner;
        this.d = list;
        this.f2820e = str;
    }

    public Object b() {
        List<Certificate> list;
        CertificateChainCleaner certificateChainCleaner = this.c.b;
        if (certificateChainCleaner == null || (list = certificateChainCleaner.a(this.d, this.f2820e)) == null) {
            list = this.d;
        }
        ArrayList arrayList = new ArrayList(Collections.a(list, 10));
        for (Certificate certificate : list) {
            if (certificate != null) {
                arrayList.add((X509Certificate) certificate);
            } else {
                throw new TypeCastException("null cannot be cast to non-null type java.security.cert.X509Certificate");
            }
        }
        return arrayList;
    }
}
