package o;

import j.a.a.a.outline;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import kotlin.TypeCastException;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;
import n.o.Progressions;
import n.o.d;
import n.r.Charsets;
import n.r.Indent;
import n.r.Regex;
import o.m0.Util;
import p.Buffer;

/* compiled from: HttpUrl.kt */
public final class HttpUrl {

    /* renamed from: k  reason: collision with root package name */
    public static final char[] f2849k = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    /* renamed from: l  reason: collision with root package name */
    public static final b f2850l = new b(null);
    public final boolean a;
    public final String b;
    public final String c;
    public final String d;

    /* renamed from: e  reason: collision with root package name */
    public final String f2851e;

    /* renamed from: f  reason: collision with root package name */
    public final int f2852f;
    public final List<String> g;
    public final List<String> h;

    /* renamed from: i  reason: collision with root package name */
    public final String f2853i;

    /* renamed from: j  reason: collision with root package name */
    public final String f2854j;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
    public HttpUrl(String str, String str2, String str3, String str4, int i2, List<String> list, List<String> list2, String str5, String str6) {
        if (str == null) {
            Intrinsics.a("scheme");
            throw null;
        } else if (str2 == null) {
            Intrinsics.a("username");
            throw null;
        } else if (str3 == null) {
            Intrinsics.a("password");
            throw null;
        } else if (str4 == null) {
            Intrinsics.a("host");
            throw null;
        } else if (list == null) {
            Intrinsics.a("pathSegments");
            throw null;
        } else if (str6 != null) {
            this.b = str;
            this.c = str2;
            this.d = str3;
            this.f2851e = str4;
            this.f2852f = i2;
            this.g = list;
            this.h = list2;
            this.f2853i = str5;
            this.f2854j = str6;
            this.a = Intrinsics.a((Object) str, (Object) "https");
        } else {
            Intrinsics.a("url");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.CharSequence, char, int, boolean, int):int
     arg types: [java.lang.String, int, int, int, int]
     candidates:
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean, int):int
      n.r.Indent.a(java.lang.String, java.lang.String, java.lang.String, boolean, int):java.lang.String
      n.r.Indent.a(java.lang.CharSequence, char[], boolean, int, int):java.util.List
      n.r.Indent.a(java.lang.CharSequence, java.lang.String[], boolean, int, int):n.q.Sequence
      n.r.Indent.a(java.lang.String, java.lang.String, int, boolean, int):boolean
      n.r.Indent.a(java.lang.CharSequence, char, int, boolean, int):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public final String a() {
        if (this.d.length() == 0) {
            return "";
        }
        int a2 = Indent.a((CharSequence) this.f2854j, ':', this.b.length() + 3, false, 4) + 1;
        int a3 = Indent.a((CharSequence) this.f2854j, '@', 0, false, 6);
        String str = this.f2854j;
        if (str != null) {
            String substring = str.substring(a2, a3);
            Intrinsics.a((Object) substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
            return substring;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.CharSequence, char, int, boolean, int):int
     arg types: [java.lang.String, int, int, int, int]
     candidates:
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean, int):int
      n.r.Indent.a(java.lang.String, java.lang.String, java.lang.String, boolean, int):java.lang.String
      n.r.Indent.a(java.lang.CharSequence, char[], boolean, int, int):java.util.List
      n.r.Indent.a(java.lang.CharSequence, java.lang.String[], boolean, int, int):n.q.Sequence
      n.r.Indent.a(java.lang.String, java.lang.String, int, boolean, int):boolean
      n.r.Indent.a(java.lang.CharSequence, char, int, boolean, int):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public final String b() {
        int a2 = Indent.a((CharSequence) this.f2854j, '/', this.b.length() + 3, false, 4);
        String str = this.f2854j;
        int a3 = Util.a(str, "?#", a2, str.length());
        String str2 = this.f2854j;
        if (str2 != null) {
            String substring = str2.substring(a2, a3);
            Intrinsics.a((Object) substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
            return substring;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.CharSequence, char, int, boolean, int):int
     arg types: [java.lang.String, int, int, int, int]
     candidates:
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean, int):int
      n.r.Indent.a(java.lang.String, java.lang.String, java.lang.String, boolean, int):java.lang.String
      n.r.Indent.a(java.lang.CharSequence, char[], boolean, int, int):java.util.List
      n.r.Indent.a(java.lang.CharSequence, java.lang.String[], boolean, int, int):n.q.Sequence
      n.r.Indent.a(java.lang.String, java.lang.String, int, boolean, int):boolean
      n.r.Indent.a(java.lang.CharSequence, char, int, boolean, int):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: o.m0.Util.a(java.lang.String, char, int, int):int
     arg types: [java.lang.String, int, int, int]
     candidates:
      o.m0.Util.a(java.lang.String, int, int, int):int
      o.m0.Util.a(java.lang.String, java.lang.String, int, int):int
      o.m0.Util.a(java.lang.String, char, int, int):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public final List<String> c() {
        int a2 = Indent.a((CharSequence) this.f2854j, '/', this.b.length() + 3, false, 4);
        String str = this.f2854j;
        int a3 = Util.a(str, "?#", a2, str.length());
        ArrayList arrayList = new ArrayList();
        while (a2 < a3) {
            int i2 = a2 + 1;
            int a4 = Util.a(this.f2854j, '/', i2, a3);
            String str2 = this.f2854j;
            if (str2 != null) {
                String substring = str2.substring(i2, a4);
                Intrinsics.a((Object) substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                arrayList.add(substring);
                a2 = a4;
            } else {
                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
            }
        }
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.CharSequence, char, int, boolean, int):int
     arg types: [java.lang.String, int, int, int, int]
     candidates:
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean, int):int
      n.r.Indent.a(java.lang.String, java.lang.String, java.lang.String, boolean, int):java.lang.String
      n.r.Indent.a(java.lang.CharSequence, char[], boolean, int, int):java.util.List
      n.r.Indent.a(java.lang.CharSequence, java.lang.String[], boolean, int, int):n.q.Sequence
      n.r.Indent.a(java.lang.String, java.lang.String, int, boolean, int):boolean
      n.r.Indent.a(java.lang.CharSequence, char, int, boolean, int):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: o.m0.Util.a(java.lang.String, char, int, int):int
     arg types: [java.lang.String, int, int, int]
     candidates:
      o.m0.Util.a(java.lang.String, int, int, int):int
      o.m0.Util.a(java.lang.String, java.lang.String, int, int):int
      o.m0.Util.a(java.lang.String, char, int, int):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public final String d() {
        if (this.h == null) {
            return null;
        }
        int a2 = Indent.a((CharSequence) this.f2854j, '?', 0, false, 6) + 1;
        String str = this.f2854j;
        int a3 = Util.a(str, '#', a2, str.length());
        String str2 = this.f2854j;
        if (str2 != null) {
            String substring = str2.substring(a2, a3);
            Intrinsics.a((Object) substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
            return substring;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public final String e() {
        if (this.c.length() == 0) {
            return "";
        }
        int length = this.b.length() + 3;
        String str = this.f2854j;
        int a2 = Util.a(str, ":@", length, str.length());
        String str2 = this.f2854j;
        if (str2 != null) {
            String substring = str2.substring(length, a2);
            Intrinsics.a((Object) substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
            return substring;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    public boolean equals(Object obj) {
        return (obj instanceof HttpUrl) && Intrinsics.a(((HttpUrl) obj).f2854j, this.f2854j);
    }

    public final String f() {
        a a2 = a("/...");
        if (a2 == null) {
            Intrinsics.a();
            throw null;
        } else if (a2 != null) {
            a2.b = b.a(f2850l, "", 0, 0, " \"':;<=>@[]^`{}|/\\?#", false, false, false, false, null, 251);
            a2.c = b.a(f2850l, "", 0, 0, " \"':;<=>@[]^`{}|/\\?#", false, false, false, false, null, 251);
            return a2.a().f2854j;
        } else {
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.CharSequence, char, int, boolean, int):int
     arg types: [java.lang.String, int, int, int, int]
     candidates:
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean, int):int
      n.r.Indent.a(java.lang.String, java.lang.String, java.lang.String, boolean, int):java.lang.String
      n.r.Indent.a(java.lang.CharSequence, char[], boolean, int, int):java.util.List
      n.r.Indent.a(java.lang.CharSequence, java.lang.String[], boolean, int, int):n.q.Sequence
      n.r.Indent.a(java.lang.String, java.lang.String, int, boolean, int):boolean
      n.r.Indent.a(java.lang.CharSequence, char, int, boolean, int):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.net.URI, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public final URI g() {
        String str;
        a aVar = new a();
        aVar.a = this.b;
        String e2 = e();
        if (e2 != null) {
            aVar.b = e2;
            String a2 = a();
            if (a2 != null) {
                aVar.c = a2;
                aVar.d = this.f2851e;
                aVar.f2856e = this.f2852f != f2850l.a(this.b) ? this.f2852f : -1;
                aVar.f2857f.clear();
                aVar.f2857f.addAll(c());
                aVar.a(d());
                if (this.f2853i == null) {
                    str = null;
                } else {
                    int a3 = Indent.a((CharSequence) this.f2854j, '#', 0, false, 6) + 1;
                    String str2 = this.f2854j;
                    if (str2 != null) {
                        str = str2.substring(a3);
                        Intrinsics.a((Object) str, "(this as java.lang.String).substring(startIndex)");
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                    }
                }
                aVar.h = str;
                int size = aVar.f2857f.size();
                for (int i2 = 0; i2 < size; i2++) {
                    List<String> list = aVar.f2857f;
                    list.set(i2, b.a(f2850l, list.get(i2), 0, 0, "[]", true, true, false, false, null, 227));
                }
                List<String> list2 = aVar.g;
                if (list2 != null) {
                    int size2 = list2.size();
                    for (int i3 = 0; i3 < size2; i3++) {
                        String str3 = list2.get(i3);
                        list2.set(i3, str3 != null ? b.a(f2850l, str3, 0, 0, "\\^`{|}", true, true, true, false, null, 195) : null);
                    }
                }
                String str4 = aVar.h;
                aVar.h = str4 != null ? b.a(f2850l, str4, 0, 0, " \"#<>\\^`{|}", true, true, false, true, null, 163) : null;
                String aVar2 = aVar.toString();
                try {
                    return new URI(aVar2);
                } catch (URISyntaxException e3) {
                    try {
                        Regex regex = new Regex("[\\u0000-\\u001F\\u007F-\\u009F\\p{javaWhitespace}]");
                        if (aVar2 != null) {
                            String replaceAll = regex.b.matcher(aVar2).replaceAll("");
                            Intrinsics.a((Object) replaceAll, "nativePattern.matcher(in…).replaceAll(replacement)");
                            URI create = URI.create(replaceAll);
                            Intrinsics.a((Object) create, "URI.create(stripped)");
                            return create;
                        }
                        Intrinsics.a("input");
                        throw null;
                    } catch (Exception unused) {
                        throw new RuntimeException(e3);
                    }
                }
            } else {
                Intrinsics.a("<set-?>");
                throw null;
            }
        } else {
            Intrinsics.a("<set-?>");
            throw null;
        }
    }

    public int hashCode() {
        return this.f2854j.hashCode();
    }

    public String toString() {
        return this.f2854j;
    }

    /* compiled from: HttpUrl.kt */
    public static final class b {
        public /* synthetic */ b(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final int a(String str) {
            if (str != null) {
                int hashCode = str.hashCode();
                if (hashCode != 3213448) {
                    if (hashCode == 99617003 && str.equals("https")) {
                        return 443;
                    }
                } else if (str.equals("http")) {
                    return 80;
                }
                return -1;
            }
            Intrinsics.a("scheme");
            throw null;
        }

        public final HttpUrl b(String str) {
            if (str != null) {
                a aVar = new a();
                aVar.a(null, str);
                return aVar.a();
            }
            Intrinsics.a("$this$toHttpUrl");
            throw null;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.r.Indent.a(java.lang.CharSequence, char, int, boolean, int):int
         arg types: [java.lang.String, int, int, int, int]
         candidates:
          n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean, int):int
          n.r.Indent.a(java.lang.String, java.lang.String, java.lang.String, boolean, int):java.lang.String
          n.r.Indent.a(java.lang.CharSequence, char[], boolean, int, int):java.util.List
          n.r.Indent.a(java.lang.CharSequence, java.lang.String[], boolean, int, int):n.q.Sequence
          n.r.Indent.a(java.lang.String, java.lang.String, int, boolean, int):boolean
          n.r.Indent.a(java.lang.CharSequence, char, int, boolean, int):int */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        public final List<String> c(String str) {
            if (str != null) {
                ArrayList arrayList = new ArrayList();
                int i2 = 0;
                while (i2 <= str.length()) {
                    int a = Indent.a((CharSequence) str, '&', i2, false, 4);
                    if (a == -1) {
                        a = str.length();
                    }
                    int a2 = Indent.a((CharSequence) str, '=', i2, false, 4);
                    if (a2 == -1 || a2 > a) {
                        String substring = str.substring(i2, a);
                        Intrinsics.a((Object) substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                        arrayList.add(substring);
                        arrayList.add(null);
                    } else {
                        String substring2 = str.substring(i2, a2);
                        Intrinsics.a((Object) substring2, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                        arrayList.add(substring2);
                        String substring3 = str.substring(a2 + 1, a);
                        Intrinsics.a((Object) substring3, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                        arrayList.add(substring3);
                    }
                    i2 = a + 1;
                }
                return arrayList;
            }
            Intrinsics.a("$this$toQueryNamesAndValues");
            throw null;
        }

        /* JADX WARN: Type inference failed for: r0v2, types: [n.o.Ranges0, n.o.Progressions] */
        public final void a(List<String> list, StringBuilder sb) {
            if (list == null) {
                Intrinsics.a("$this$toQueryString");
                throw null;
            } else if (sb != null) {
                Progressions a = d.a((Progressions) d.b(0, list.size()), 2);
                int i2 = a.b;
                int i3 = a.c;
                int i4 = a.d;
                if (i4 >= 0) {
                    if (i2 > i3) {
                        return;
                    }
                } else if (i2 < i3) {
                    return;
                }
                while (true) {
                    String str = list.get(i2);
                    String str2 = list.get(i2 + 1);
                    if (i2 > 0) {
                        sb.append('&');
                    }
                    sb.append(str);
                    if (str2 != null) {
                        sb.append('=');
                        sb.append(str2);
                    }
                    if (i2 != i3) {
                        i2 += i4;
                    } else {
                        return;
                    }
                }
            } else {
                Intrinsics.a("out");
                throw null;
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        public static /* synthetic */ String a(b bVar, String str, int i2, int i3, boolean z, int i4) {
            int i5;
            int i6;
            if ((i4 & 1) != 0) {
                i2 = 0;
            }
            if ((i4 & 2) != 0) {
                i3 = str.length();
            }
            if ((i4 & 4) != 0) {
                z = false;
            }
            if (bVar == null) {
                throw null;
            } else if (str != null) {
                int i7 = i2;
                while (i5 < i3) {
                    char charAt = str.charAt(i5);
                    if (charAt == '%' || (charAt == '+' && z)) {
                        Buffer buffer = new Buffer();
                        buffer.a(str, i2, i5);
                        while (i5 < i3) {
                            int codePointAt = str.codePointAt(i5);
                            if (codePointAt == 37 && (i6 = i5 + 2) < i3) {
                                int a = Util.a(str.charAt(i5 + 1));
                                int a2 = Util.a(str.charAt(i6));
                                if (!(a == -1 || a2 == -1)) {
                                    buffer.writeByte((a << 4) + a2);
                                    i5 = Character.charCount(codePointAt) + i6;
                                }
                            } else if (codePointAt == 43 && z) {
                                buffer.writeByte(32);
                                i5++;
                            }
                            buffer.b(codePointAt);
                            i5 += Character.charCount(codePointAt);
                        }
                        return buffer.o();
                    }
                    i7 = i5 + 1;
                }
                String substring = str.substring(i2, i3);
                Intrinsics.a((Object) substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                return substring;
            } else {
                Intrinsics.a("$this$percentDecode");
                throw null;
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [java.util.List<java.lang.String>, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        public final List<String> a(List<String> list, boolean z) {
            ArrayList arrayList = new ArrayList(list.size());
            Iterator<String> it = list.iterator();
            while (it.hasNext()) {
                String next = it.next();
                arrayList.add(next != null ? a(this, next, 0, 0, z, 3) : null);
            }
            List<String> unmodifiableList = Collections.unmodifiableList(arrayList);
            Intrinsics.a((Object) unmodifiableList, "Collections.unmodifiableList(result)");
            return unmodifiableList;
        }

        public final boolean a(String str, int i2, int i3) {
            int i4 = i2 + 2;
            if (i4 >= i3 || str.charAt(i2) != '%' || Util.a(str.charAt(i2 + 1)) == -1 || Util.a(str.charAt(i4)) == -1) {
                return false;
            }
            return true;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.r.Indent.a(java.lang.CharSequence, char, boolean, int):boolean
         arg types: [java.lang.String, char, int, int]
         candidates:
          n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean):int
          n.r.Indent.a(java.lang.CharSequence, char[], int, boolean):int
          n.r.Indent.a(java.lang.CharSequence, java.lang.String, boolean, int):java.util.List<java.lang.String>
          n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean, int):boolean
          n.r.Indent.a(java.lang.String, java.lang.String, int, boolean):boolean
          n.r.Indent.a(java.lang.String, java.lang.String, boolean, int):boolean
          n.r.Indent.a(java.lang.CharSequence, char, boolean, int):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [byte[], java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: p.Buffer.write(byte[], int, int):p.Buffer
         arg types: [byte[], int, int]
         candidates:
          p.Buffer.write(byte[], int, int):p.BufferedSink
          p.BufferedSink.write(byte[], int, int):p.BufferedSink
          p.Buffer.write(byte[], int, int):p.Buffer */
        public static /* synthetic */ String a(b bVar, String str, int i2, int i3, String str2, boolean z, boolean z2, boolean z3, boolean z4, Charset charset, int i4) {
            boolean z5;
            b bVar2 = bVar;
            String str3 = str;
            String str4 = str2;
            int i5 = i4;
            int i6 = (i5 & 1) != 0 ? 0 : i2;
            int length = (i5 & 2) != 0 ? str.length() : i3;
            boolean z6 = (i5 & 8) != 0 ? false : z;
            boolean z7 = (i5 & 16) != 0 ? false : z2;
            boolean z8 = (i5 & 32) != 0 ? false : z3;
            boolean z9 = (i5 & 64) != 0 ? false : z4;
            Charset charset2 = (i5 & 128) != 0 ? null : charset;
            if (bVar2 == null) {
                throw null;
            } else if (str3 == null) {
                Intrinsics.a("$this$canonicalize");
                throw null;
            } else if (str4 != null) {
                int i7 = i6;
                while (i7 < length) {
                    int codePointAt = str3.codePointAt(i7);
                    int i8 = 2;
                    if (codePointAt < 32 || codePointAt == 127 || ((codePointAt >= 128 && !z9) || Indent.a((CharSequence) str4, (char) codePointAt, false, 2) || ((codePointAt == 37 && (!z6 || (z7 && !bVar2.a(str3, i7, length)))) || (codePointAt == 43 && z8)))) {
                        Buffer buffer = new Buffer();
                        buffer.a(str3, i6, i7);
                        Buffer buffer2 = null;
                        while (i7 < length) {
                            int codePointAt2 = str3.codePointAt(i7);
                            if (!z6 || !(codePointAt2 == 9 || codePointAt2 == 10 || codePointAt2 == 12 || codePointAt2 == 13)) {
                                if (codePointAt2 != 43 || !z8) {
                                    if (codePointAt2 < 32 || codePointAt2 == 127 || (codePointAt2 >= 128 && !z9)) {
                                        z5 = z8;
                                    } else {
                                        z5 = z8;
                                        if (!Indent.a((CharSequence) str4, (char) codePointAt2, false, i8) && (codePointAt2 != 37 || (z6 && (!z7 || bVar2.a(str3, i7, length))))) {
                                            buffer.b(codePointAt2);
                                            i7 += Character.charCount(codePointAt2);
                                            i8 = 2;
                                            z8 = z5;
                                        }
                                    }
                                    if (buffer2 == null) {
                                        buffer2 = new Buffer();
                                    }
                                    if (charset2 == null || Intrinsics.a(charset2, StandardCharsets.UTF_8)) {
                                        buffer2.b(codePointAt2);
                                    } else {
                                        int charCount = Character.charCount(codePointAt2) + i7;
                                        boolean z10 = true;
                                        if (i7 >= 0) {
                                            if (charCount >= i7) {
                                                if (charCount > str.length()) {
                                                    z10 = false;
                                                }
                                                if (!z10) {
                                                    StringBuilder a = outline.a("endIndex > string.length: ", charCount, " > ");
                                                    a.append(str.length());
                                                    throw new IllegalArgumentException(a.toString().toString());
                                                } else if (Intrinsics.a(charset2, Charsets.a)) {
                                                    buffer2.a(str3, i7, charCount);
                                                } else {
                                                    String substring = str3.substring(i7, charCount);
                                                    Intrinsics.a((Object) substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                                                    byte[] bytes = substring.getBytes(charset2);
                                                    Intrinsics.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                                                    buffer2.write(bytes, 0, bytes.length);
                                                }
                                            } else {
                                                throw new IllegalArgumentException(outline.a("endIndex < beginIndex: ", charCount, " < ", i7).toString());
                                            }
                                        } else {
                                            throw new IllegalArgumentException(outline.b("beginIndex < 0: ", i7).toString());
                                        }
                                    }
                                    while (!buffer2.j()) {
                                        byte readByte = buffer2.readByte() & 255;
                                        buffer.writeByte(37);
                                        buffer.writeByte((int) HttpUrl.f2849k[(readByte >> 4) & 15]);
                                        buffer.writeByte((int) HttpUrl.f2849k[readByte & 15]);
                                    }
                                    i7 += Character.charCount(codePointAt2);
                                    i8 = 2;
                                    z8 = z5;
                                } else {
                                    buffer.a(z6 ? "+" : "%2B");
                                }
                            }
                            z5 = z8;
                            i7 += Character.charCount(codePointAt2);
                            i8 = 2;
                            z8 = z5;
                        }
                        return buffer.o();
                    }
                    i7 += Character.charCount(codePointAt);
                }
                String substring2 = str3.substring(i6, length);
                Intrinsics.a((Object) substring2, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                return substring2;
            } else {
                Intrinsics.a("encodeSet");
                throw null;
            }
        }
    }

    /* compiled from: HttpUrl.kt */
    public static final class a {

        /* renamed from: i  reason: collision with root package name */
        public static final C0041a f2855i = new C0041a(null);
        public String a;
        public String b = "";
        public String c = "";
        public String d;

        /* renamed from: e  reason: collision with root package name */
        public int f2856e = -1;

        /* renamed from: f  reason: collision with root package name */
        public final List<String> f2857f;
        public List<String> g;
        public String h;

        /* renamed from: o.HttpUrl$a$a  reason: collision with other inner class name */
        /* compiled from: HttpUrl.kt */
        public static final class C0041a {
            public /* synthetic */ C0041a(DefaultConstructorMarker defaultConstructorMarker) {
            }
        }

        public a() {
            ArrayList arrayList = new ArrayList();
            this.f2857f = arrayList;
            arrayList.add("");
        }

        public final a a(String str) {
            String a2;
            this.g = (str == null || (a2 = b.a(HttpUrl.f2850l, str, 0, 0, " \"'<>#", true, false, true, false, null, 211)) == null) ? null : HttpUrl.f2850l.c(a2);
            return this;
        }

        public final int b() {
            int i2 = this.f2856e;
            if (i2 != -1) {
                return i2;
            }
            b bVar = HttpUrl.f2850l;
            String str = this.a;
            if (str != null) {
                return bVar.a(str);
            }
            Intrinsics.a();
            throw null;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.r.Indent.a(java.lang.CharSequence, char, boolean, int):boolean
         arg types: [java.lang.String, int, int, int]
         candidates:
          n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean):int
          n.r.Indent.a(java.lang.CharSequence, char[], int, boolean):int
          n.r.Indent.a(java.lang.CharSequence, java.lang.String, boolean, int):java.util.List<java.lang.String>
          n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean, int):boolean
          n.r.Indent.a(java.lang.String, java.lang.String, int, boolean):boolean
          n.r.Indent.a(java.lang.String, java.lang.String, boolean, int):boolean
          n.r.Indent.a(java.lang.CharSequence, char, boolean, int):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0033, code lost:
            if ((r7.c.length() > 0) != false) goto L_0x0035;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:39:0x0094, code lost:
            if (r1 != r6.a(r5)) goto L_0x009b;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.String toString() {
            /*
                r7 = this;
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                r0.<init>()
                java.lang.String r1 = r7.a
                if (r1 == 0) goto L_0x0012
                r0.append(r1)
                java.lang.String r1 = "://"
                r0.append(r1)
                goto L_0x0017
            L_0x0012:
                java.lang.String r1 = "//"
                r0.append(r1)
            L_0x0017:
                java.lang.String r1 = r7.b
                int r1 = r1.length()
                r2 = 1
                r3 = 0
                if (r1 <= 0) goto L_0x0023
                r1 = 1
                goto L_0x0024
            L_0x0023:
                r1 = 0
            L_0x0024:
                r4 = 58
                if (r1 != 0) goto L_0x0035
                java.lang.String r1 = r7.c
                int r1 = r1.length()
                if (r1 <= 0) goto L_0x0032
                r1 = 1
                goto L_0x0033
            L_0x0032:
                r1 = 0
            L_0x0033:
                if (r1 == 0) goto L_0x0053
            L_0x0035:
                java.lang.String r1 = r7.b
                r0.append(r1)
                java.lang.String r1 = r7.c
                int r1 = r1.length()
                if (r1 <= 0) goto L_0x0043
                goto L_0x0044
            L_0x0043:
                r2 = 0
            L_0x0044:
                if (r2 == 0) goto L_0x004e
                r0.append(r4)
                java.lang.String r1 = r7.c
                r0.append(r1)
            L_0x004e:
                r1 = 64
                r0.append(r1)
            L_0x0053:
                java.lang.String r1 = r7.d
                r2 = 0
                if (r1 == 0) goto L_0x007b
                if (r1 == 0) goto L_0x0077
                r5 = 2
                boolean r1 = n.r.Indent.a(r1, r4, r3, r5)
                if (r1 == 0) goto L_0x0071
                r1 = 91
                r0.append(r1)
                java.lang.String r1 = r7.d
                r0.append(r1)
                r1 = 93
                r0.append(r1)
                goto L_0x007b
            L_0x0071:
                java.lang.String r1 = r7.d
                r0.append(r1)
                goto L_0x007b
            L_0x0077:
                n.n.c.Intrinsics.a()
                throw r2
            L_0x007b:
                int r1 = r7.f2856e
                r5 = -1
                if (r1 != r5) goto L_0x0084
                java.lang.String r1 = r7.a
                if (r1 == 0) goto L_0x00a1
            L_0x0084:
                int r1 = r7.b()
                java.lang.String r5 = r7.a
                if (r5 == 0) goto L_0x009b
                o.HttpUrl$b r6 = o.HttpUrl.f2850l
                if (r5 == 0) goto L_0x0097
                int r5 = r6.a(r5)
                if (r1 == r5) goto L_0x00a1
                goto L_0x009b
            L_0x0097:
                n.n.c.Intrinsics.a()
                throw r2
            L_0x009b:
                r0.append(r4)
                r0.append(r1)
            L_0x00a1:
                o.HttpUrl$b r1 = o.HttpUrl.f2850l
                java.util.List<java.lang.String> r1 = r7.f2857f
                if (r1 == 0) goto L_0x00ed
                int r4 = r1.size()
            L_0x00ab:
                if (r3 >= r4) goto L_0x00be
                r5 = 47
                r0.append(r5)
                java.lang.Object r5 = r1.get(r3)
                java.lang.String r5 = (java.lang.String) r5
                r0.append(r5)
                int r3 = r3 + 1
                goto L_0x00ab
            L_0x00be:
                java.util.List<java.lang.String> r1 = r7.g
                if (r1 == 0) goto L_0x00d5
                r1 = 63
                r0.append(r1)
                o.HttpUrl$b r1 = o.HttpUrl.f2850l
                java.util.List<java.lang.String> r3 = r7.g
                if (r3 == 0) goto L_0x00d1
                r1.a(r3, r0)
                goto L_0x00d5
            L_0x00d1:
                n.n.c.Intrinsics.a()
                throw r2
            L_0x00d5:
                java.lang.String r1 = r7.h
                if (r1 == 0) goto L_0x00e3
                r1 = 35
                r0.append(r1)
                java.lang.String r1 = r7.h
                r0.append(r1)
            L_0x00e3:
                java.lang.String r0 = r0.toString()
                java.lang.String r1 = "StringBuilder().apply(builderAction).toString()"
                n.n.c.Intrinsics.a(r0, r1)
                return r0
            L_0x00ed:
                java.lang.String r0 = "$this$toPathString"
                n.n.c.Intrinsics.a(r0)
                throw r2
            */
            throw new UnsupportedOperationException("Method not decompiled: o.HttpUrl.a.toString():java.lang.String");
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: o.HttpUrl.b.a(java.util.List<java.lang.String>, boolean):java.util.List<java.lang.String>
         arg types: [java.util.List<java.lang.String>, int]
         candidates:
          o.HttpUrl.b.a(java.util.List<java.lang.String>, java.lang.StringBuilder):void
          o.HttpUrl.b.a(java.util.List<java.lang.String>, boolean):java.util.List<java.lang.String> */
        public final HttpUrl a() {
            String str = this.a;
            if (str != null) {
                String a2 = b.a(HttpUrl.f2850l, this.b, 0, 0, false, 7);
                String a3 = b.a(HttpUrl.f2850l, this.c, 0, 0, false, 7);
                String str2 = this.d;
                if (str2 != null) {
                    int b2 = b();
                    List<String> a4 = HttpUrl.f2850l.a(this.f2857f, false);
                    if (a4 != null) {
                        List<String> list = this.g;
                        List<String> a5 = list != null ? HttpUrl.f2850l.a(list, true) : null;
                        String str3 = this.h;
                        return new HttpUrl(str, a2, a3, str2, b2, a4, a5, str3 != null ? b.a(HttpUrl.f2850l, str3, 0, 0, false, 7) : null, toString());
                    }
                    throw new TypeCastException("null cannot be cast to non-null type kotlin.collections.List<kotlin.String>");
                }
                throw new IllegalStateException("host == null");
            }
            throw new IllegalStateException("scheme == null");
        }

        /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
            jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
            	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:52)
            	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:86)
            */
        /* JADX WARNING: Removed duplicated region for block: B:117:0x0263  */
        /* JADX WARNING: Removed duplicated region for block: B:118:0x0265  */
        /* JADX WARNING: Removed duplicated region for block: B:120:0x0268  */
        /* JADX WARNING: Removed duplicated region for block: B:121:0x026b  */
        /* JADX WARNING: Removed duplicated region for block: B:143:0x031a  */
        /* JADX WARNING: Removed duplicated region for block: B:230:0x041b A[SYNTHETIC] */
        public final o.HttpUrl.a a(o.HttpUrl r29, java.lang.String r30) {
            /*
                r28 = this;
                r0 = r28
                r1 = r29
                r12 = r30
                if (r12 == 0) goto L_0x04a9
                r2 = 3
                r3 = 0
                int r2 = o.m0.Util.a(r12, r3, r3, r2)
                int r4 = r30.length()
                int r13 = o.m0.Util.b(r12, r2, r4)
                int r4 = r13 - r2
                r5 = 2
                r6 = 58
                r7 = -1
                r8 = 1
                if (r4 >= r5) goto L_0x0020
                goto L_0x0069
            L_0x0020:
                char r4 = r12.charAt(r2)
                r9 = 90
                r10 = 122(0x7a, float:1.71E-43)
                r11 = 65
                r14 = 97
                if (r4 < r14) goto L_0x0030
                if (r4 <= r10) goto L_0x0035
            L_0x0030:
                if (r4 < r11) goto L_0x0069
                if (r4 <= r9) goto L_0x0035
                goto L_0x0069
            L_0x0035:
                r4 = r2
            L_0x0036:
                int r4 = r4 + r8
                if (r4 >= r13) goto L_0x0069
                char r15 = r12.charAt(r4)
                if (r14 <= r15) goto L_0x0040
                goto L_0x0043
            L_0x0040:
                if (r10 < r15) goto L_0x0043
                goto L_0x0061
            L_0x0043:
                if (r11 <= r15) goto L_0x0046
                goto L_0x0049
            L_0x0046:
                if (r9 < r15) goto L_0x0049
                goto L_0x0061
            L_0x0049:
                r9 = 57
                r10 = 48
                if (r10 <= r15) goto L_0x0050
                goto L_0x0053
            L_0x0050:
                if (r9 < r15) goto L_0x0053
                goto L_0x0061
            L_0x0053:
                r9 = 43
                if (r15 != r9) goto L_0x0058
                goto L_0x0061
            L_0x0058:
                r9 = 45
                if (r15 != r9) goto L_0x005d
                goto L_0x0061
            L_0x005d:
                r9 = 46
                if (r15 != r9) goto L_0x0066
            L_0x0061:
                r9 = 90
                r10 = 122(0x7a, float:1.71E-43)
                goto L_0x0036
            L_0x0066:
                if (r15 != r6) goto L_0x0069
                goto L_0x006a
            L_0x0069:
                r4 = -1
            L_0x006a:
                java.lang.String r14 = "(this as java.lang.Strin…ing(startIndex, endIndex)"
                if (r4 == r7) goto L_0x00ab
                java.lang.String r9 = "https:"
                boolean r9 = n.r.Indent.a(r12, r9, r2, r8)
                if (r9 == 0) goto L_0x007d
                java.lang.String r3 = "https"
                r0.a = r3
                int r2 = r2 + 6
                goto L_0x00b1
            L_0x007d:
                java.lang.String r9 = "http:"
                boolean r9 = n.r.Indent.a(r12, r9, r2, r8)
                if (r9 == 0) goto L_0x008c
                java.lang.String r3 = "http"
                r0.a = r3
                int r2 = r2 + 5
                goto L_0x00b1
            L_0x008c:
                java.lang.String r1 = "Expected URL scheme 'http' or 'https' but was '"
                java.lang.StringBuilder r1 = j.a.a.a.outline.a(r1)
                java.lang.String r2 = r12.substring(r3, r4)
                n.n.c.Intrinsics.a(r2, r14)
                r1.append(r2)
                java.lang.String r2 = "'"
                r1.append(r2)
                java.lang.String r1 = r1.toString()
                java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
                r2.<init>(r1)
                throw r2
            L_0x00ab:
                if (r1 == 0) goto L_0x04a1
                java.lang.String r3 = r1.b
                r0.a = r3
            L_0x00b1:
                r3 = 0
                r4 = r2
            L_0x00b3:
                r9 = 47
                r10 = 92
                if (r4 >= r13) goto L_0x00c6
                char r11 = r12.charAt(r4)
                if (r11 == r10) goto L_0x00c1
                if (r11 != r9) goto L_0x00c6
            L_0x00c1:
                int r3 = r3 + 1
                int r4 = r4 + 1
                goto L_0x00b3
            L_0x00c6:
                r4 = 63
                r11 = 35
                if (r3 >= r5) goto L_0x0110
                if (r1 == 0) goto L_0x0110
                java.lang.String r5 = r1.b
                java.lang.String r15 = r0.a
                boolean r5 = n.n.c.Intrinsics.a(r5, r15)
                r5 = r5 ^ r8
                if (r5 == 0) goto L_0x00da
                goto L_0x0110
            L_0x00da:
                java.lang.String r3 = r29.e()
                r0.b = r3
                java.lang.String r3 = r29.a()
                r0.c = r3
                java.lang.String r3 = r1.f2851e
                r0.d = r3
                int r3 = r1.f2852f
                r0.f2856e = r3
                java.util.List<java.lang.String> r3 = r0.f2857f
                r3.clear()
                java.util.List<java.lang.String> r3 = r0.f2857f
                java.util.List r4 = r29.c()
                r3.addAll(r4)
                if (r2 == r13) goto L_0x0104
                char r3 = r12.charAt(r2)
                if (r3 != r11) goto L_0x010b
            L_0x0104:
                java.lang.String r1 = r29.d()
                r0.a(r1)
            L_0x010b:
                r1 = 1
                r17 = r13
                goto L_0x02c2
            L_0x0110:
                int r2 = r2 + r3
                r1 = 0
                r3 = 0
                r11 = r2
                r1 = 35
                r15 = 0
                r16 = 0
            L_0x0119:
                java.lang.String r2 = "@/\\?#"
                int r8 = o.m0.Util.a(r12, r2, r11, r13)
                if (r8 == r13) goto L_0x0126
                char r2 = r12.charAt(r8)
                goto L_0x0127
            L_0x0126:
                r2 = -1
            L_0x0127:
                if (r2 == r7) goto L_0x01ef
                if (r2 == r1) goto L_0x01ef
                if (r2 == r9) goto L_0x01ef
                if (r2 == r10) goto L_0x01ef
                if (r2 == r4) goto L_0x01ef
                r1 = 64
                if (r2 == r1) goto L_0x0139
                r17 = r13
                goto L_0x01e0
            L_0x0139:
                java.lang.String r10 = "%40"
                if (r15 != 0) goto L_0x01a5
                int r9 = o.m0.Util.a(r12, r6, r11, r8)
                o.HttpUrl$b r1 = o.HttpUrl.f2850l
                r6 = 1
                r7 = 0
                r17 = 0
                r18 = 0
                r19 = 0
                r20 = 240(0xf0, float:3.36E-43)
                java.lang.String r5 = " \"':;<=>@[]^`{}|/\\?#"
                r2 = r30
                r3 = r11
                r4 = r9
                r11 = r8
                r8 = r17
                r21 = r9
                r9 = r18
                r22 = r10
                r10 = r19
                r29 = r15
                r15 = r11
                r11 = r20
                java.lang.String r1 = o.HttpUrl.b.a(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11)
                if (r16 == 0) goto L_0x017f
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.String r3 = r0.b
                r2.append(r3)
                r3 = r22
                r2.append(r3)
                r2.append(r1)
                java.lang.String r1 = r2.toString()
            L_0x017f:
                r0.b = r1
                r1 = r21
                if (r1 == r15) goto L_0x019e
                o.HttpUrl$b r2 = o.HttpUrl.f2850l
                int r3 = r1 + 1
                r6 = 1
                r7 = 0
                r8 = 0
                r9 = 0
                r10 = 0
                r11 = 240(0xf0, float:3.36E-43)
                java.lang.String r5 = " \"':;<=>@[]^`{}|/\\?#"
                r1 = r2
                r2 = r30
                r4 = r15
                java.lang.String r1 = o.HttpUrl.b.a(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11)
                r0.c = r1
                r1 = 1
                goto L_0x01a0
            L_0x019e:
                r1 = r29
            L_0x01a0:
                r16 = 1
                r17 = r13
                goto L_0x01dd
            L_0x01a5:
                r3 = r10
                r29 = r15
                r15 = r8
                java.lang.StringBuilder r10 = new java.lang.StringBuilder
                r10.<init>()
                java.lang.String r1 = r0.c
                r10.append(r1)
                r10.append(r3)
                o.HttpUrl$b r1 = o.HttpUrl.f2850l
                r6 = 1
                r7 = 0
                r8 = 0
                r9 = 0
                r17 = 0
                r18 = 240(0xf0, float:3.36E-43)
                java.lang.String r5 = " \"':;<=>@[]^`{}|/\\?#"
                r2 = r30
                r3 = r11
                r4 = r15
                r11 = r10
                r10 = r17
                r17 = r13
                r13 = r11
                r11 = r18
                java.lang.String r1 = o.HttpUrl.b.a(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11)
                r13.append(r1)
                java.lang.String r1 = r13.toString()
                r0.c = r1
                r1 = r29
            L_0x01dd:
                int r11 = r15 + 1
                r15 = r1
            L_0x01e0:
                r1 = 35
                r4 = 63
                r10 = 92
                r9 = 47
                r7 = -1
                r6 = 58
                r13 = r17
                goto L_0x0119
            L_0x01ef:
                r15 = r8
                r17 = r13
                r8 = r11
            L_0x01f3:
                if (r8 >= r15) goto L_0x0213
                char r1 = r12.charAt(r8)
                r2 = 58
                if (r1 == r2) goto L_0x0211
                r2 = 91
                if (r1 == r2) goto L_0x0202
                goto L_0x020e
            L_0x0202:
                int r8 = r8 + 1
                if (r8 >= r15) goto L_0x020e
                char r1 = r12.charAt(r8)
                r2 = 93
                if (r1 != r2) goto L_0x0202
            L_0x020e:
                int r8 = r8 + 1
                goto L_0x01f3
            L_0x0211:
                r13 = r8
                goto L_0x0214
            L_0x0213:
                r13 = r15
            L_0x0214:
                int r10 = r13 + 1
                if (r10 >= r15) goto L_0x0290
                o.HttpUrl$b r1 = o.HttpUrl.f2850l
                r5 = 0
                r6 = 4
                r2 = r30
                r3 = r11
                r4 = r13
                java.lang.String r1 = o.HttpUrl.b.a(r1, r2, r3, r4, r5, r6)
                java.lang.String r1 = n.i.Collections.a(r1)
                r0.d = r1
                o.HttpUrl$b r1 = o.HttpUrl.f2850l     // Catch:{ NumberFormatException -> 0x0258 }
                java.lang.String r5 = ""
                r6 = 0
                r7 = 0
                r8 = 0
                r9 = 0
                r16 = 0
                r18 = 248(0xf8, float:3.48E-43)
                r4 = 1
                r2 = r30
                r3 = r10
                r4 = r15
                r24 = r10
                r10 = r16
                r29 = r11
                r11 = r18
                java.lang.String r1 = o.HttpUrl.b.a(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11)     // Catch:{ NumberFormatException -> 0x0256 }
                int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ NumberFormatException -> 0x0256 }
                r2 = 65535(0xffff, float:9.1834E-41)
                r3 = 1
                if (r3 <= r1) goto L_0x0252
                goto L_0x025c
            L_0x0252:
                if (r2 < r1) goto L_0x025c
                r4 = 1
                goto L_0x025e
            L_0x0256:
                r3 = 1
                goto L_0x025c
            L_0x0258:
                r24 = r10
                r29 = r11
            L_0x025c:
                r4 = 1
                r1 = -1
            L_0x025e:
                r0.f2856e = r1
                r2 = -1
                if (r1 == r2) goto L_0x0265
                r1 = 1
                goto L_0x0266
            L_0x0265:
                r1 = 0
            L_0x0266:
                if (r1 == 0) goto L_0x026b
                r1 = 34
                goto L_0x02b7
            L_0x026b:
                java.lang.String r1 = "Invalid URL port: \""
                java.lang.StringBuilder r1 = j.a.a.a.outline.a(r1)
                r13 = r24
                java.lang.String r2 = r12.substring(r13, r15)
                n.n.c.Intrinsics.a(r2, r14)
                r1.append(r2)
                r2 = 34
                r1.append(r2)
                java.lang.String r1 = r1.toString()
                java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
                java.lang.String r1 = r1.toString()
                r2.<init>(r1)
                throw r2
            L_0x0290:
                r29 = r11
                r7 = 34
                r8 = 1
                o.HttpUrl$b r1 = o.HttpUrl.f2850l
                r5 = 0
                r6 = 4
                r2 = r30
                r3 = r29
                r4 = r13
                java.lang.String r1 = o.HttpUrl.b.a(r1, r2, r3, r4, r5, r6)
                java.lang.String r1 = n.i.Collections.a(r1)
                r0.d = r1
                o.HttpUrl$b r1 = o.HttpUrl.f2850l
                java.lang.String r2 = r0.a
                if (r2 == 0) goto L_0x049c
                int r1 = r1.a(r2)
                r0.f2856e = r1
                r1 = 34
                r4 = 1
            L_0x02b7:
                java.lang.String r2 = r0.d
                if (r2 == 0) goto L_0x02bd
                r2 = 1
                goto L_0x02be
            L_0x02bd:
                r2 = 0
            L_0x02be:
                if (r2 == 0) goto L_0x0479
                r1 = r4
                r2 = r15
            L_0x02c2:
                java.lang.String r3 = "?#"
                r4 = r17
                int r3 = o.m0.Util.a(r12, r3, r2, r4)
                if (r2 != r3) goto L_0x02d1
                r15 = r0
                r14 = r4
                r13 = r12
                goto L_0x0421
            L_0x02d1:
                char r5 = r12.charAt(r2)
                java.lang.String r6 = ""
                r7 = 47
                if (r5 == r7) goto L_0x02f8
                r7 = 92
                if (r5 != r7) goto L_0x02e0
                goto L_0x02f8
            L_0x02e0:
                java.util.List<java.lang.String> r5 = r0.f2857f
                int r7 = r5.size()
                int r7 = r7 - r1
                r5.set(r7, r6)
                r10 = r0
                r16 = r10
                r14 = r1
                r15 = r3
                r17 = r4
                r11 = r6
                r1 = r12
                r9 = r1
                r13 = r9
                r4 = r2
                r12 = r15
                goto L_0x0318
            L_0x02f8:
                java.util.List<java.lang.String> r5 = r0.f2857f
                r5.clear()
                java.util.List<java.lang.String> r5 = r0.f2857f
                r5.add(r6)
                r8 = r0
                r9 = r8
                r5 = r3
                r13 = r4
                r7 = r6
                r4 = r2
                r6 = r5
                r2 = r12
                r3 = r1
                r1 = r2
            L_0x030c:
                int r4 = r4 + r3
                r14 = r3
                r15 = r5
                r11 = r7
                r16 = r8
                r10 = r9
                r9 = r12
                r17 = r13
                r13 = r2
                r12 = r6
            L_0x0318:
                if (r4 >= r12) goto L_0x041b
                java.lang.String r2 = "/\\"
                int r8 = o.m0.Util.a(r9, r2, r4, r12)
                if (r8 >= r12) goto L_0x0326
                r2 = 1
                r18 = 1
                goto L_0x0329
            L_0x0326:
                r2 = 0
                r18 = 0
            L_0x0329:
                r7 = 1
                o.HttpUrl$b r2 = o.HttpUrl.f2850l
                r19 = 0
                r20 = 0
                r21 = 0
                r22 = 0
                r23 = 240(0xf0, float:3.36E-43)
                java.lang.String r6 = " \"<>^`{}|/\\?#"
                r3 = r9
                r5 = r8
                r24 = r8
                r8 = r19
                r19 = r9
                r9 = r20
                r25 = r10
                r10 = r21
                r26 = r11
                r11 = r22
                r20 = r12
                r12 = r23
                java.lang.String r2 = o.HttpUrl.b.a(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12)
                java.lang.String r3 = "."
                boolean r3 = n.n.c.Intrinsics.a(r2, r3)
                if (r3 != 0) goto L_0x0365
                java.lang.String r3 = "%2e"
                boolean r3 = n.r.Indent.a(r2, r3, r14)
                if (r3 == 0) goto L_0x0363
                goto L_0x0365
            L_0x0363:
                r3 = 0
                goto L_0x0366
            L_0x0365:
                r3 = 1
            L_0x0366:
                if (r3 == 0) goto L_0x036e
                r9 = r25
                r6 = r26
                goto L_0x03ff
            L_0x036e:
                java.lang.String r3 = ".."
                boolean r3 = n.n.c.Intrinsics.a(r2, r3)
                if (r3 != 0) goto L_0x0391
                java.lang.String r3 = "%2e."
                boolean r3 = n.r.Indent.a(r2, r3, r14)
                if (r3 != 0) goto L_0x0391
                java.lang.String r3 = ".%2e"
                boolean r3 = n.r.Indent.a(r2, r3, r14)
                if (r3 != 0) goto L_0x0391
                java.lang.String r3 = "%2e%2e"
                boolean r3 = n.r.Indent.a(r2, r3, r14)
                if (r3 == 0) goto L_0x038f
                goto L_0x0391
            L_0x038f:
                r3 = 0
                goto L_0x0392
            L_0x0391:
                r3 = 1
            L_0x0392:
                if (r3 == 0) goto L_0x03cc
                r9 = r25
                java.util.List<java.lang.String> r2 = r9.f2857f
                int r3 = r2.size()
                int r3 = r3 - r14
                java.lang.Object r2 = r2.remove(r3)
                java.lang.String r2 = (java.lang.String) r2
                int r2 = r2.length()
                if (r2 != 0) goto L_0x03ab
                r2 = 1
                goto L_0x03ac
            L_0x03ab:
                r2 = 0
            L_0x03ac:
                if (r2 == 0) goto L_0x03c4
                java.util.List<java.lang.String> r2 = r9.f2857f
                boolean r2 = r2.isEmpty()
                r2 = r2 ^ r14
                if (r2 == 0) goto L_0x03c4
                java.util.List<java.lang.String> r2 = r9.f2857f
                int r3 = r2.size()
                int r3 = r3 - r14
                r6 = r26
                r2.set(r3, r6)
                goto L_0x03ff
            L_0x03c4:
                r6 = r26
                java.util.List<java.lang.String> r2 = r9.f2857f
                r2.add(r6)
                goto L_0x03ff
            L_0x03cc:
                r9 = r25
                r6 = r26
                java.util.List<java.lang.String> r3 = r9.f2857f
                int r4 = r3.size()
                int r4 = r4 - r14
                java.lang.Object r3 = r3.get(r4)
                java.lang.CharSequence r3 = (java.lang.CharSequence) r3
                int r3 = r3.length()
                if (r3 != 0) goto L_0x03e5
                r3 = 1
                goto L_0x03e6
            L_0x03e5:
                r3 = 0
            L_0x03e6:
                if (r3 == 0) goto L_0x03f3
                java.util.List<java.lang.String> r3 = r9.f2857f
                int r4 = r3.size()
                int r4 = r4 - r14
                r3.set(r4, r2)
                goto L_0x03f8
            L_0x03f3:
                java.util.List<java.lang.String> r3 = r9.f2857f
                r3.add(r2)
            L_0x03f8:
                if (r18 == 0) goto L_0x03ff
                java.util.List<java.lang.String> r2 = r9.f2857f
                r2.add(r6)
            L_0x03ff:
                if (r18 == 0) goto L_0x0411
                r7 = r6
                r2 = r13
                r3 = r14
                r5 = r15
                r8 = r16
                r13 = r17
                r12 = r19
                r6 = r20
                r4 = r24
                goto L_0x030c
            L_0x0411:
                r11 = r6
                r10 = r9
                r9 = r19
                r12 = r20
                r4 = r24
                goto L_0x0318
            L_0x041b:
                r12 = r1
                r3 = r15
                r15 = r16
                r14 = r17
            L_0x0421:
                if (r3 >= r14) goto L_0x0459
                char r1 = r13.charAt(r3)
                r2 = 63
                if (r1 != r2) goto L_0x0459
                r11 = 35
                int r16 = o.m0.Util.a(r13, r11, r3, r14)
                o.HttpUrl$b r10 = o.HttpUrl.f2850l
                int r3 = r3 + 1
                r6 = 1
                r7 = 0
                r8 = 1
                r9 = 0
                r17 = 0
                r18 = 208(0xd0, float:2.91E-43)
                java.lang.String r5 = " \"'<>#"
                r1 = r10
                r2 = r12
                r4 = r16
                r27 = r10
                r10 = r17
                r17 = 35
                r11 = r18
                java.lang.String r1 = o.HttpUrl.b.a(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11)
                r2 = r27
                java.util.List r1 = r2.c(r1)
                r15.g = r1
                r3 = r16
            L_0x0459:
                r11 = 35
                if (r3 >= r14) goto L_0x0478
                char r1 = r13.charAt(r3)
                if (r1 != r11) goto L_0x0478
                o.HttpUrl$b r1 = o.HttpUrl.f2850l
                int r3 = r3 + 1
                r6 = 1
                r7 = 0
                r8 = 0
                r9 = 1
                r10 = 0
                r11 = 176(0xb0, float:2.47E-43)
                java.lang.String r5 = ""
                r2 = r12
                r4 = r14
                java.lang.String r1 = o.HttpUrl.b.a(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11)
                r15.h = r1
            L_0x0478:
                return r15
            L_0x0479:
                java.lang.String r2 = "Invalid URL host: \""
                java.lang.StringBuilder r2 = j.a.a.a.outline.a(r2)
                r11 = r29
                java.lang.String r3 = r12.substring(r11, r13)
                n.n.c.Intrinsics.a(r3, r14)
                r2.append(r3)
                r2.append(r1)
                java.lang.String r1 = r2.toString()
                java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
                java.lang.String r1 = r1.toString()
                r2.<init>(r1)
                throw r2
            L_0x049c:
                n.n.c.Intrinsics.a()
                r1 = 0
                throw r1
            L_0x04a1:
                java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
                java.lang.String r2 = "Expected URL scheme 'http' or 'https' but no colon was found"
                r1.<init>(r2)
                throw r1
            L_0x04a9:
                r1 = 0
                java.lang.String r2 = "input"
                n.n.c.Intrinsics.a(r2)
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: o.HttpUrl.a.a(o.HttpUrl, java.lang.String):o.HttpUrl$a");
        }
    }

    public final HttpUrl b(String str) {
        if (str != null) {
            a a2 = a(str);
            if (a2 != null) {
                return a2.a();
            }
            return null;
        }
        Intrinsics.a("link");
        throw null;
    }

    public final a a(String str) {
        if (str != null) {
            try {
                a aVar = new a();
                aVar.a(this, str);
                return aVar;
            } catch (IllegalArgumentException unused) {
                return null;
            }
        } else {
            Intrinsics.a("link");
            throw null;
        }
    }
}
