package o;

import com.crashlytics.android.core.DefaultAppMeasurementEventListenerRegistrar;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import kotlin.TypeCastException;
import n.Tuples;
import n.i.Collections2;
import n.n.c.ArrayIterator;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;
import n.o.Progressions;
import n.o.d;
import n.r.Indent;
import o.m0.Util;

/* compiled from: Headers.kt */
public final class Headers implements Iterable<Tuples<? extends String, ? extends String>>, n.n.c.t.a {
    public static final b c = new b(null);
    public final String[] b;

    public /* synthetic */ Headers(String[] strArr, DefaultConstructorMarker defaultConstructorMarker) {
        this.b = strArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      n.r.Indent.a(java.lang.String, java.lang.String, int):java.lang.String
      n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean */
    public final String a(String str) {
        if (str != null) {
            String[] strArr = this.b;
            Progressions a2 = d.a(d.a(strArr.length - 2, 0), 2);
            int i2 = a2.b;
            int i3 = a2.c;
            int i4 = a2.d;
            if (i4 >= 0) {
                if (i2 > i3) {
                    return null;
                }
            } else if (i2 < i3) {
                return null;
            }
            while (!Indent.a(str, strArr[i2], true)) {
                if (i2 == i3) {
                    return null;
                }
                i2 += i4;
            }
            return strArr[i2 + 1];
        }
        Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      n.r.Indent.a(java.lang.String, java.lang.String, int):java.lang.String
      n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.List<java.lang.String>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public final List<String> b(String str) {
        ArrayList arrayList = null;
        if (str != null) {
            int size = size();
            for (int i2 = 0; i2 < size; i2++) {
                if (Indent.a(str, c(i2), true)) {
                    if (arrayList == null) {
                        arrayList = new ArrayList(2);
                    }
                    arrayList.add(d(i2));
                }
            }
            if (arrayList == null) {
                return Collections2.b;
            }
            List<String> unmodifiableList = Collections.unmodifiableList(arrayList);
            Intrinsics.a((Object) unmodifiableList, "Collections.unmodifiableList(result)");
            return unmodifiableList;
        }
        Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
        throw null;
    }

    public final String c(int i2) {
        return this.b[i2 * 2];
    }

    public final String d(int i2) {
        return this.b[(i2 * 2) + 1];
    }

    public boolean equals(Object obj) {
        return (obj instanceof Headers) && Arrays.equals(this.b, ((Headers) obj).b);
    }

    public int hashCode() {
        return Arrays.hashCode(this.b);
    }

    public Iterator<Tuples<String, String>> iterator() {
        int size = size();
        Tuples[] tuplesArr = new Tuples[size];
        for (int i2 = 0; i2 < size; i2++) {
            tuplesArr[i2] = new Tuples(c(i2), d(i2));
        }
        return new ArrayIterator(tuplesArr);
    }

    public final int size() {
        return this.b.length / 2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public String toString() {
        StringBuilder sb = new StringBuilder();
        int size = size();
        for (int i2 = 0; i2 < size; i2++) {
            sb.append(c(i2));
            sb.append(": ");
            sb.append(d(i2));
            sb.append("\n");
        }
        String sb2 = sb.toString();
        Intrinsics.a((Object) sb2, "StringBuilder().apply(builderAction).toString()");
        return sb2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
     arg types: [java.util.List<java.lang.String>, java.lang.String[]]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(java.lang.Object, java.lang.String):T
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean */
    public final a c() {
        a aVar = new a();
        n.i.Collections.a((Collection) aVar.a, (Object[]) this.b);
        return aVar;
    }

    /* compiled from: Headers.kt */
    public static final class a {
        public final List<String> a = new ArrayList(20);

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.r.Indent.a(java.lang.CharSequence, char, int, boolean, int):int
         arg types: [java.lang.String, int, int, int, int]
         candidates:
          n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean, int):int
          n.r.Indent.a(java.lang.String, java.lang.String, java.lang.String, boolean, int):java.lang.String
          n.r.Indent.a(java.lang.CharSequence, char[], boolean, int, int):java.util.List
          n.r.Indent.a(java.lang.CharSequence, java.lang.String[], boolean, int, int):n.q.Sequence
          n.r.Indent.a(java.lang.String, java.lang.String, int, boolean, int):boolean
          n.r.Indent.a(java.lang.CharSequence, char, int, boolean, int):int */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        public final a a(String str) {
            if (str != null) {
                int a2 = Indent.a((CharSequence) str, ':', 1, false, 4);
                if (a2 != -1) {
                    String substring = str.substring(0, a2);
                    Intrinsics.a((Object) substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                    String substring2 = str.substring(a2 + 1);
                    Intrinsics.a((Object) substring2, "(this as java.lang.String).substring(startIndex)");
                    b(substring, substring2);
                } else if (str.charAt(0) == ':') {
                    String substring3 = str.substring(1);
                    Intrinsics.a((Object) substring3, "(this as java.lang.String).substring(startIndex)");
                    b("", substring3);
                } else {
                    b("", str);
                }
                return this;
            }
            Intrinsics.a("line");
            throw null;
        }

        public final a b(String str, String str2) {
            if (str == null) {
                Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
                throw null;
            } else if (str2 != null) {
                this.a.add(str);
                this.a.add(Indent.c(str2).toString());
                return this;
            } else {
                Intrinsics.a("value");
                throw null;
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean
         arg types: [java.lang.String, java.lang.String, int]
         candidates:
          n.r.Indent.a(java.lang.String, java.lang.String, int):java.lang.String
          n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean):boolean
          n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean */
        public final a c(String str) {
            if (str != null) {
                int i2 = 0;
                while (i2 < this.a.size()) {
                    if (Indent.a(str, this.a.get(i2), true)) {
                        this.a.remove(i2);
                        this.a.remove(i2);
                        i2 -= 2;
                    }
                    i2 += 2;
                }
                return this;
            }
            Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
            throw null;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean
         arg types: [java.lang.String, java.lang.String, int]
         candidates:
          n.r.Indent.a(java.lang.String, java.lang.String, int):java.lang.String
          n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean):boolean
          n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean */
        public final String b(String str) {
            if (str != null) {
                Progressions a2 = d.a(d.a(this.a.size() - 2, 0), 2);
                int i2 = a2.b;
                int i3 = a2.c;
                int i4 = a2.d;
                if (i4 < 0 ? i2 >= i3 : i2 <= i3) {
                    while (!Indent.a(str, this.a.get(i2), true)) {
                        if (i2 != i3) {
                            i2 += i4;
                        }
                    }
                    return this.a.get(i2 + 1);
                }
                return null;
            }
            Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
            throw null;
        }

        public final a c(String str, String str2) {
            if (str == null) {
                Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
                throw null;
            } else if (str2 != null) {
                Headers.c.a(str);
                Headers.c.a(str2, str);
                c(str);
                b(str, str2);
                return this;
            } else {
                Intrinsics.a("value");
                throw null;
            }
        }

        public final a a(String str, String str2) {
            if (str == null) {
                Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
                throw null;
            } else if (str2 != null) {
                Headers.c.a(str);
                Headers.c.a(str2, str);
                b(str, str2);
                return this;
            } else {
                Intrinsics.a("value");
                throw null;
            }
        }

        public final Headers a() {
            Object[] array = this.a.toArray(new String[0]);
            if (array != null) {
                return new Headers((String[]) array, null);
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    /* compiled from: Headers.kt */
    public static final class b {
        public /* synthetic */ b(DefaultConstructorMarker defaultConstructorMarker) {
        }

        /* JADX WARN: Type inference failed for: r1v5, types: [n.o.Ranges0, n.o.Progressions] */
        public final Headers a(String... strArr) {
            if (strArr != null) {
                if (strArr.length % 2 == 0) {
                    Object clone = strArr.clone();
                    if (clone != null) {
                        String[] strArr2 = (String[]) clone;
                        int length = strArr2.length;
                        int i2 = 0;
                        while (i2 < length) {
                            if (strArr2[i2] != null) {
                                String str = strArr2[i2];
                                if (str != null) {
                                    strArr2[i2] = Indent.c(str).toString();
                                    i2++;
                                } else {
                                    throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
                                }
                            } else {
                                throw new IllegalArgumentException("Headers cannot be null".toString());
                            }
                        }
                        Progressions a = d.a((Progressions) d.b(0, strArr2.length), 2);
                        int i3 = a.b;
                        int i4 = a.c;
                        int i5 = a.d;
                        if (i5 < 0 ? i3 >= i4 : i3 <= i4) {
                            while (true) {
                                String str2 = strArr2[i3];
                                String str3 = strArr2[i3 + 1];
                                a(str2);
                                a(str3, str2);
                                if (i3 == i4) {
                                    break;
                                }
                                i3 += i5;
                            }
                        }
                        return new Headers(strArr2, null);
                    }
                    throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<kotlin.String>");
                }
                throw new IllegalArgumentException("Expected alternating header names and values".toString());
            }
            Intrinsics.a("namesAndValues");
            throw null;
        }

        public final void a(String str) {
            if (str.length() > 0) {
                int length = str.length();
                int i2 = 0;
                while (i2 < length) {
                    char charAt = str.charAt(i2);
                    if ('!' <= charAt && '~' >= charAt) {
                        i2++;
                    } else {
                        throw new IllegalArgumentException(Util.a("Unexpected char %#04x at %d in header name: %s", Integer.valueOf(charAt), Integer.valueOf(i2), str).toString());
                    }
                }
                return;
            }
            throw new IllegalArgumentException("name is empty".toString());
        }

        public final void a(String str, String str2) {
            int length = str.length();
            int i2 = 0;
            while (i2 < length) {
                char charAt = str.charAt(i2);
                if (charAt == 9 || (' ' <= charAt && '~' >= charAt)) {
                    i2++;
                } else {
                    throw new IllegalArgumentException(Util.a("Unexpected char %#04x at %d in %s value: %s", Integer.valueOf(charAt), Integer.valueOf(i2), str2, str).toString());
                }
            }
        }
    }
}
