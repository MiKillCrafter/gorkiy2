package o;

import com.crashlytics.android.answers.SessionEventTransform;
import com.crashlytics.android.core.DefaultAppMeasurementEventListenerRegistrar;
import j.a.a.a.outline;
import java.util.LinkedHashMap;
import java.util.Map;
import n.Tuples;
import n.i.Collections;
import n.n.c.Intrinsics;
import o.Headers;
import o.e0;
import o.m0.Util;
import o.m0.e.HttpMethod;

/* compiled from: Request.kt */
public final class Request {
    public CacheControl a;
    public final HttpUrl b;
    public final String c;
    public final Headers d;

    /* renamed from: e  reason: collision with root package name */
    public final RequestBody f2896e;

    /* renamed from: f  reason: collision with root package name */
    public final Map<Class<?>, Object> f2897f;

    public Request(x xVar, String str, w wVar, g0 g0Var, Map<Class<?>, ? extends Object> map) {
        if (xVar == null) {
            Intrinsics.a("url");
            throw null;
        } else if (str == null) {
            Intrinsics.a("method");
            throw null;
        } else if (wVar == null) {
            Intrinsics.a("headers");
            throw null;
        } else if (map != null) {
            this.b = xVar;
            this.c = str;
            this.d = wVar;
            this.f2896e = g0Var;
            this.f2897f = map;
        } else {
            Intrinsics.a("tags");
            throw null;
        }
    }

    public final String a(String str) {
        if (str != null) {
            return this.d.a(str);
        }
        Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public String toString() {
        StringBuilder a2 = outline.a("Request{method=");
        a2.append(this.c);
        a2.append(", url=");
        a2.append(this.b);
        if (this.d.size() != 0) {
            a2.append(", headers=[");
            int i2 = 0;
            for (Object next : this.d) {
                int i3 = i2 + 1;
                if (i2 >= 0) {
                    Tuples tuples = (Tuples) next;
                    String str = (String) tuples.b;
                    String str2 = (String) tuples.c;
                    if (i2 > 0) {
                        a2.append(", ");
                    }
                    a2.append(str);
                    a2.append(':');
                    a2.append(str2);
                    i2 = i3;
                } else {
                    Collections.a();
                    throw null;
                }
            }
            a2.append(']');
        }
        if (!this.f2897f.isEmpty()) {
            a2.append(", tags=");
            a2.append(this.f2897f);
        }
        a2.append('}');
        String sb = a2.toString();
        Intrinsics.a((Object) sb, "StringBuilder().apply(builderAction).toString()");
        return sb;
    }

    /* compiled from: Request.kt */
    public static class a {
        public HttpUrl a;
        public String b;
        public Headers.a c;
        public RequestBody d;

        /* renamed from: e  reason: collision with root package name */
        public Map<Class<?>, Object> f2898e;

        public a() {
            this.f2898e = new LinkedHashMap();
            this.b = "GET";
            this.c = new Headers.a();
        }

        public a a(HttpUrl httpUrl) {
            if (httpUrl != null) {
                this.a = httpUrl;
                return this;
            }
            Intrinsics.a("url");
            throw null;
        }

        public a a(String str, String str2) {
            if (str == null) {
                Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
                throw null;
            } else if (str2 != null) {
                this.c.c(str, str2);
                return this;
            } else {
                Intrinsics.a("value");
                throw null;
            }
        }

        public a(Request request) {
            LinkedHashMap linkedHashMap;
            if (request != null) {
                this.f2898e = new LinkedHashMap();
                this.a = request.b;
                this.b = request.c;
                this.d = request.f2896e;
                if (request.f2897f.isEmpty()) {
                    linkedHashMap = new LinkedHashMap();
                } else {
                    Map<Class<?>, Object> map = request.f2897f;
                    if (map != null) {
                        linkedHashMap = new LinkedHashMap(map);
                    } else {
                        Intrinsics.a("$this$toMutableMap");
                        throw null;
                    }
                }
                this.f2898e = linkedHashMap;
                this.c = request.d.c();
                return;
            }
            Intrinsics.a("request");
            throw null;
        }

        public a a(String str) {
            if (str != null) {
                this.c.c(str);
                return this;
            }
            Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
            throw null;
        }

        public a a(Headers headers) {
            if (headers != null) {
                this.c = headers.c();
                return this;
            }
            Intrinsics.a("headers");
            throw null;
        }

        public a a(String str, RequestBody requestBody) {
            if (str != null) {
                if (str.length() > 0) {
                    if (requestBody == null) {
                        if (!(!HttpMethod.b(str))) {
                            throw new IllegalArgumentException(outline.a("method ", str, " must have a request body.").toString());
                        }
                    } else if (!HttpMethod.a(str)) {
                        throw new IllegalArgumentException(outline.a("method ", str, " must not have a request body.").toString());
                    }
                    this.b = str;
                    this.d = requestBody;
                    return this;
                }
                throw new IllegalArgumentException("method.isEmpty() == true".toString());
            }
            Intrinsics.a("method");
            throw null;
        }

        public <T> e0.a a(Class<? super T> cls, T t2) {
            if (cls != null) {
                if (t2 == null) {
                    this.f2898e.remove(cls);
                } else {
                    if (this.f2898e.isEmpty()) {
                        this.f2898e = new LinkedHashMap();
                    }
                    Map<Class<?>, Object> map = this.f2898e;
                    Object cast = cls.cast(t2);
                    if (cast != null) {
                        map.put(cls, cast);
                    } else {
                        Intrinsics.a();
                        throw null;
                    }
                }
                return this;
            }
            Intrinsics.a(SessionEventTransform.TYPE_KEY);
            throw null;
        }

        public Request a() {
            HttpUrl httpUrl = this.a;
            if (httpUrl != null) {
                return new Request(httpUrl, this.b, this.c.a(), this.d, Util.a(this.f2898e));
            }
            throw new IllegalStateException("url == null".toString());
        }
    }

    public final CacheControl a() {
        CacheControl cacheControl = this.a;
        if (cacheControl != null) {
            return cacheControl;
        }
        CacheControl a2 = CacheControl.f2812n.a(this.d);
        this.a = a2;
        return a2;
    }
}
