package o;

import j.a.a.a.outline;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import kotlin.TypeCastException;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;
import n.r.Indent;

/* compiled from: CipherSuite.kt */
public final class CipherSuite {
    public static final Comparator<String> b = new a();
    public static final Map<String, j> c = new LinkedHashMap();
    public static final CipherSuite d = b.a(f2834t, "SSL_RSA_WITH_3DES_EDE_CBC_SHA", 10);

    /* renamed from: e  reason: collision with root package name */
    public static final CipherSuite f2821e = b.a(f2834t, "TLS_RSA_WITH_AES_128_CBC_SHA", 47);

    /* renamed from: f  reason: collision with root package name */
    public static final CipherSuite f2822f = b.a(f2834t, "TLS_RSA_WITH_AES_256_CBC_SHA", 53);
    public static final CipherSuite g = b.a(f2834t, "TLS_RSA_WITH_AES_128_GCM_SHA256", 156);
    public static final CipherSuite h = b.a(f2834t, "TLS_RSA_WITH_AES_256_GCM_SHA384", 157);

    /* renamed from: i  reason: collision with root package name */
    public static final CipherSuite f2823i = b.a(f2834t, "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA", 49171);

    /* renamed from: j  reason: collision with root package name */
    public static final CipherSuite f2824j = b.a(f2834t, "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA", 49172);

    /* renamed from: k  reason: collision with root package name */
    public static final CipherSuite f2825k = b.a(f2834t, "TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256", 49195);

    /* renamed from: l  reason: collision with root package name */
    public static final CipherSuite f2826l = b.a(f2834t, "TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384", 49196);

    /* renamed from: m  reason: collision with root package name */
    public static final CipherSuite f2827m = b.a(f2834t, "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256", 49199);

    /* renamed from: n  reason: collision with root package name */
    public static final CipherSuite f2828n = b.a(f2834t, "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384", 49200);

    /* renamed from: o  reason: collision with root package name */
    public static final CipherSuite f2829o = b.a(f2834t, "TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256", 52392);

    /* renamed from: p  reason: collision with root package name */
    public static final CipherSuite f2830p = b.a(f2834t, "TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256", 52393);

    /* renamed from: q  reason: collision with root package name */
    public static final CipherSuite f2831q = b.a(f2834t, "TLS_AES_128_GCM_SHA256", 4865);

    /* renamed from: r  reason: collision with root package name */
    public static final CipherSuite f2832r = b.a(f2834t, "TLS_AES_256_GCM_SHA384", 4866);

    /* renamed from: s  reason: collision with root package name */
    public static final CipherSuite f2833s = b.a(f2834t, "TLS_CHACHA20_POLY1305_SHA256", 4867);

    /* renamed from: t  reason: collision with root package name */
    public static final b f2834t = new b(null);
    public final String a;

    /* compiled from: CipherSuite.kt */
    public static final class a implements Comparator<String> {
        public int compare(Object obj, Object obj2) {
            String str = (String) obj;
            String str2 = (String) obj2;
            if (str == null) {
                Intrinsics.a("a");
                throw null;
            } else if (str2 != null) {
                int i2 = 4;
                int min = Math.min(str.length(), str2.length());
                while (true) {
                    if (i2 < min) {
                        char charAt = str.charAt(i2);
                        char charAt2 = str2.charAt(i2);
                        if (charAt == charAt2) {
                            i2++;
                        } else if (charAt < charAt2) {
                            return -1;
                        }
                    } else {
                        int length = str.length();
                        int length2 = str2.length();
                        if (length == length2) {
                            return 0;
                        }
                        if (length < length2) {
                            return -1;
                        }
                    }
                }
                return 1;
            } else {
                Intrinsics.a("b");
                throw null;
            }
        }
    }

    static {
        b.a(f2834t, "SSL_RSA_WITH_NULL_MD5", 1);
        b.a(f2834t, "SSL_RSA_WITH_NULL_SHA", 2);
        b.a(f2834t, "SSL_RSA_EXPORT_WITH_RC4_40_MD5", 3);
        b.a(f2834t, "SSL_RSA_WITH_RC4_128_MD5", 4);
        b.a(f2834t, "SSL_RSA_WITH_RC4_128_SHA", 5);
        b.a(f2834t, "SSL_RSA_EXPORT_WITH_DES40_CBC_SHA", 8);
        b.a(f2834t, "SSL_RSA_WITH_DES_CBC_SHA", 9);
        b.a(f2834t, "SSL_DHE_DSS_EXPORT_WITH_DES40_CBC_SHA", 17);
        b.a(f2834t, "SSL_DHE_DSS_WITH_DES_CBC_SHA", 18);
        b.a(f2834t, "SSL_DHE_DSS_WITH_3DES_EDE_CBC_SHA", 19);
        b.a(f2834t, "SSL_DHE_RSA_EXPORT_WITH_DES40_CBC_SHA", 20);
        b.a(f2834t, "SSL_DHE_RSA_WITH_DES_CBC_SHA", 21);
        b.a(f2834t, "SSL_DHE_RSA_WITH_3DES_EDE_CBC_SHA", 22);
        b.a(f2834t, "SSL_DH_anon_EXPORT_WITH_RC4_40_MD5", 23);
        b.a(f2834t, "SSL_DH_anon_WITH_RC4_128_MD5", 24);
        b.a(f2834t, "SSL_DH_anon_EXPORT_WITH_DES40_CBC_SHA", 25);
        b.a(f2834t, "SSL_DH_anon_WITH_DES_CBC_SHA", 26);
        b.a(f2834t, "SSL_DH_anon_WITH_3DES_EDE_CBC_SHA", 27);
        b.a(f2834t, "TLS_KRB5_WITH_DES_CBC_SHA", 30);
        b.a(f2834t, "TLS_KRB5_WITH_3DES_EDE_CBC_SHA", 31);
        b.a(f2834t, "TLS_KRB5_WITH_RC4_128_SHA", 32);
        b.a(f2834t, "TLS_KRB5_WITH_DES_CBC_MD5", 34);
        b.a(f2834t, "TLS_KRB5_WITH_3DES_EDE_CBC_MD5", 35);
        b.a(f2834t, "TLS_KRB5_WITH_RC4_128_MD5", 36);
        b.a(f2834t, "TLS_KRB5_EXPORT_WITH_DES_CBC_40_SHA", 38);
        b.a(f2834t, "TLS_KRB5_EXPORT_WITH_RC4_40_SHA", 40);
        b.a(f2834t, "TLS_KRB5_EXPORT_WITH_DES_CBC_40_MD5", 41);
        b.a(f2834t, "TLS_KRB5_EXPORT_WITH_RC4_40_MD5", 43);
        b.a(f2834t, "TLS_DHE_DSS_WITH_AES_128_CBC_SHA", 50);
        b.a(f2834t, "TLS_DHE_RSA_WITH_AES_128_CBC_SHA", 51);
        b.a(f2834t, "TLS_DH_anon_WITH_AES_128_CBC_SHA", 52);
        b.a(f2834t, "TLS_DHE_DSS_WITH_AES_256_CBC_SHA", 56);
        b.a(f2834t, "TLS_DHE_RSA_WITH_AES_256_CBC_SHA", 57);
        b.a(f2834t, "TLS_DH_anon_WITH_AES_256_CBC_SHA", 58);
        b.a(f2834t, "TLS_RSA_WITH_NULL_SHA256", 59);
        b.a(f2834t, "TLS_RSA_WITH_AES_128_CBC_SHA256", 60);
        b.a(f2834t, "TLS_RSA_WITH_AES_256_CBC_SHA256", 61);
        b.a(f2834t, "TLS_DHE_DSS_WITH_AES_128_CBC_SHA256", 64);
        b.a(f2834t, "TLS_RSA_WITH_CAMELLIA_128_CBC_SHA", 65);
        b.a(f2834t, "TLS_DHE_DSS_WITH_CAMELLIA_128_CBC_SHA", 68);
        b.a(f2834t, "TLS_DHE_RSA_WITH_CAMELLIA_128_CBC_SHA", 69);
        b.a(f2834t, "TLS_DHE_RSA_WITH_AES_128_CBC_SHA256", 103);
        b.a(f2834t, "TLS_DHE_DSS_WITH_AES_256_CBC_SHA256", 106);
        b.a(f2834t, "TLS_DHE_RSA_WITH_AES_256_CBC_SHA256", 107);
        b.a(f2834t, "TLS_DH_anon_WITH_AES_128_CBC_SHA256", 108);
        b.a(f2834t, "TLS_DH_anon_WITH_AES_256_CBC_SHA256", 109);
        b.a(f2834t, "TLS_RSA_WITH_CAMELLIA_256_CBC_SHA", 132);
        b.a(f2834t, "TLS_DHE_DSS_WITH_CAMELLIA_256_CBC_SHA", 135);
        b.a(f2834t, "TLS_DHE_RSA_WITH_CAMELLIA_256_CBC_SHA", 136);
        b.a(f2834t, "TLS_PSK_WITH_RC4_128_SHA", 138);
        b.a(f2834t, "TLS_PSK_WITH_3DES_EDE_CBC_SHA", 139);
        b.a(f2834t, "TLS_PSK_WITH_AES_128_CBC_SHA", 140);
        b.a(f2834t, "TLS_PSK_WITH_AES_256_CBC_SHA", 141);
        b.a(f2834t, "TLS_RSA_WITH_SEED_CBC_SHA", 150);
        b.a(f2834t, "TLS_DHE_RSA_WITH_AES_128_GCM_SHA256", 158);
        b.a(f2834t, "TLS_DHE_RSA_WITH_AES_256_GCM_SHA384", 159);
        b.a(f2834t, "TLS_DHE_DSS_WITH_AES_128_GCM_SHA256", 162);
        b.a(f2834t, "TLS_DHE_DSS_WITH_AES_256_GCM_SHA384", 163);
        b.a(f2834t, "TLS_DH_anon_WITH_AES_128_GCM_SHA256", 166);
        b.a(f2834t, "TLS_DH_anon_WITH_AES_256_GCM_SHA384", 167);
        b.a(f2834t, "TLS_EMPTY_RENEGOTIATION_INFO_SCSV", 255);
        b.a(f2834t, "TLS_FALLBACK_SCSV", 22016);
        b.a(f2834t, "TLS_ECDH_ECDSA_WITH_NULL_SHA", 49153);
        b.a(f2834t, "TLS_ECDH_ECDSA_WITH_RC4_128_SHA", 49154);
        b.a(f2834t, "TLS_ECDH_ECDSA_WITH_3DES_EDE_CBC_SHA", 49155);
        b.a(f2834t, "TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA", 49156);
        b.a(f2834t, "TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA", 49157);
        b.a(f2834t, "TLS_ECDHE_ECDSA_WITH_NULL_SHA", 49158);
        b.a(f2834t, "TLS_ECDHE_ECDSA_WITH_RC4_128_SHA", 49159);
        b.a(f2834t, "TLS_ECDHE_ECDSA_WITH_3DES_EDE_CBC_SHA", 49160);
        b.a(f2834t, "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA", 49161);
        b.a(f2834t, "TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA", 49162);
        b.a(f2834t, "TLS_ECDH_RSA_WITH_NULL_SHA", 49163);
        b.a(f2834t, "TLS_ECDH_RSA_WITH_RC4_128_SHA", 49164);
        b.a(f2834t, "TLS_ECDH_RSA_WITH_3DES_EDE_CBC_SHA", 49165);
        b.a(f2834t, "TLS_ECDH_RSA_WITH_AES_128_CBC_SHA", 49166);
        b.a(f2834t, "TLS_ECDH_RSA_WITH_AES_256_CBC_SHA", 49167);
        b.a(f2834t, "TLS_ECDHE_RSA_WITH_NULL_SHA", 49168);
        b.a(f2834t, "TLS_ECDHE_RSA_WITH_RC4_128_SHA", 49169);
        b.a(f2834t, "TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA", 49170);
        b.a(f2834t, "TLS_ECDH_anon_WITH_NULL_SHA", 49173);
        b.a(f2834t, "TLS_ECDH_anon_WITH_RC4_128_SHA", 49174);
        b.a(f2834t, "TLS_ECDH_anon_WITH_3DES_EDE_CBC_SHA", 49175);
        b.a(f2834t, "TLS_ECDH_anon_WITH_AES_128_CBC_SHA", 49176);
        b.a(f2834t, "TLS_ECDH_anon_WITH_AES_256_CBC_SHA", 49177);
        b.a(f2834t, "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256", 49187);
        b.a(f2834t, "TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384", 49188);
        b.a(f2834t, "TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA256", 49189);
        b.a(f2834t, "TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA384", 49190);
        b.a(f2834t, "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256", 49191);
        b.a(f2834t, "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384", 49192);
        b.a(f2834t, "TLS_ECDH_RSA_WITH_AES_128_CBC_SHA256", 49193);
        b.a(f2834t, "TLS_ECDH_RSA_WITH_AES_256_CBC_SHA384", 49194);
        b.a(f2834t, "TLS_ECDH_ECDSA_WITH_AES_128_GCM_SHA256", 49197);
        b.a(f2834t, "TLS_ECDH_ECDSA_WITH_AES_256_GCM_SHA384", 49198);
        b.a(f2834t, "TLS_ECDH_RSA_WITH_AES_128_GCM_SHA256", 49201);
        b.a(f2834t, "TLS_ECDH_RSA_WITH_AES_256_GCM_SHA384", 49202);
        b.a(f2834t, "TLS_ECDHE_PSK_WITH_AES_128_CBC_SHA", 49205);
        b.a(f2834t, "TLS_ECDHE_PSK_WITH_AES_256_CBC_SHA", 49206);
        b.a(f2834t, "TLS_DHE_RSA_WITH_CHACHA20_POLY1305_SHA256", 52394);
        b.a(f2834t, "TLS_ECDHE_PSK_WITH_CHACHA20_POLY1305_SHA256", 52396);
        b.a(f2834t, "TLS_AES_128_CCM_SHA256", 4868);
        b.a(f2834t, "TLS_AES_128_CCM_8_SHA256", 4869);
    }

    public /* synthetic */ CipherSuite(String str, DefaultConstructorMarker defaultConstructorMarker) {
        this.a = str;
    }

    public String toString() {
        return this.a;
    }

    /* compiled from: CipherSuite.kt */
    public static final class b {
        public /* synthetic */ b(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final synchronized CipherSuite a(String str) {
            CipherSuite cipherSuite;
            if (str != null) {
                cipherSuite = CipherSuite.c.get(str);
                if (cipherSuite == null) {
                    cipherSuite = (CipherSuite) CipherSuite.c.get(b(str));
                    if (cipherSuite == null) {
                        cipherSuite = new CipherSuite(str, null);
                    }
                    CipherSuite.c.put(str, cipherSuite);
                }
            } else {
                Intrinsics.a("javaName");
                throw null;
            }
            return cipherSuite;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        public final String b(String str) {
            if (Indent.b(str, "TLS_", false, 2)) {
                StringBuilder a = outline.a("SSL_");
                if (str != null) {
                    String substring = str.substring(4);
                    Intrinsics.a((Object) substring, "(this as java.lang.String).substring(startIndex)");
                    a.append(substring);
                    return a.toString();
                }
                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
            } else if (!Indent.b(str, "SSL_", false, 2)) {
                return str;
            } else {
                StringBuilder a2 = outline.a("TLS_");
                if (str != null) {
                    String substring2 = str.substring(4);
                    Intrinsics.a((Object) substring2, "(this as java.lang.String).substring(startIndex)");
                    a2.append(substring2);
                    return a2.toString();
                }
                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
            }
        }

        public static final /* synthetic */ CipherSuite a(b bVar, String str, int i2) {
            if (bVar != null) {
                CipherSuite cipherSuite = new CipherSuite(str, null);
                CipherSuite.c.put(str, cipherSuite);
                return cipherSuite;
            }
            throw null;
        }
    }
}
