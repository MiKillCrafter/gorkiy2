package o;

import p.BufferedSource;

/* compiled from: ResponseBody.kt */
public final class ResponseBody0 extends ResponseBody {
    public final /* synthetic */ BufferedSource d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ MediaType f2917e;

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ long f2918f;

    public ResponseBody0(BufferedSource bufferedSource, MediaType mediaType, long j2) {
        this.d = bufferedSource;
        this.f2917e = mediaType;
        this.f2918f = j2;
    }

    public long a() {
        return this.f2918f;
    }

    public MediaType f() {
        return this.f2917e;
    }

    public BufferedSource g() {
        return this.d;
    }
}
