package f;

import com.crashlytics.android.core.DefaultAppMeasurementEventListenerRegistrar;
import java.lang.ref.ReferenceQueue;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.Executor;
import leakcanary.KeyedWeakReference;
import n.n.b.Functions;
import n.n.c.Intrinsics;
import s.SharkLog;

/* compiled from: ObjectWatcher.kt */
public final class ObjectWatcher {
    public final Set<e> a;
    public final Map<String, KeyedWeakReference> b;
    public final ReferenceQueue<Object> c;
    public final Clock d;

    /* renamed from: e  reason: collision with root package name */
    public final Executor f728e;

    /* renamed from: f  reason: collision with root package name */
    public final Functions<Boolean> f729f;

    /* compiled from: ObjectWatcher.kt */
    public static final class a implements Runnable {
        public final /* synthetic */ ObjectWatcher b;
        public final /* synthetic */ String c;

        public a(ObjectWatcher objectWatcher, String str) {
            this.b = objectWatcher;
            this.c = str;
        }

        public final void run() {
            this.b.a(this.c);
        }
    }

    public ObjectWatcher(c cVar, Executor executor, Functions<Boolean> functions) {
        if (cVar == null) {
            Intrinsics.a("clock");
            throw null;
        } else if (executor == null) {
            Intrinsics.a("checkRetainedExecutor");
            throw null;
        } else if (functions != null) {
            this.d = cVar;
            this.f728e = executor;
            this.f729f = functions;
            this.a = new LinkedHashSet();
            this.b = new LinkedHashMap();
            this.c = new ReferenceQueue<>();
        } else {
            Intrinsics.a("isEnabled");
            throw null;
        }
    }

    public final synchronized void a(Object obj) {
        if (obj != null) {
            a(obj, "");
        } else {
            Intrinsics.a("watchedObject");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public final synchronized void a(Object obj, String str) {
        String str2;
        String str3;
        if (obj == null) {
            Intrinsics.a("watchedObject");
            throw null;
        } else if (str == null) {
            Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
            throw null;
        } else if (this.f729f.b().booleanValue()) {
            a();
            String uuid = UUID.randomUUID().toString();
            Intrinsics.a((Object) uuid, "UUID.randomUUID()\n        .toString()");
            KeyedWeakReference keyedWeakReference = new KeyedWeakReference(obj, uuid, str, this.d.a(), this.c);
            SharkLog.a aVar = SharkLog.a;
            if (aVar != null) {
                StringBuilder sb = new StringBuilder();
                sb.append("Watching ");
                if (obj instanceof Class) {
                    str2 = obj.toString();
                } else {
                    str2 = "instance of " + obj.getClass().getName();
                }
                sb.append(str2);
                if (str.length() > 0) {
                    str3 = " named " + str;
                } else {
                    str3 = "";
                }
                sb.append(str3);
                sb.append(" with key ");
                sb.append(uuid);
                aVar.a(sb.toString());
            }
            this.b.put(uuid, keyedWeakReference);
            this.f728e.execute(new a(this, uuid));
        }
    }

    public final synchronized void a(String str) {
        a();
        KeyedWeakReference keyedWeakReference = this.b.get(str);
        if (keyedWeakReference != null) {
            keyedWeakReference.setRetainedUptimeMillis(this.d.a());
            Iterator<T> it = this.a.iterator();
            while (it.hasNext()) {
                ((OnObjectRetainedListener) it.next()).d();
            }
        }
    }

    public final void a() {
        KeyedWeakReference keyedWeakReference;
        do {
            keyedWeakReference = (KeyedWeakReference) this.c.poll();
            if (keyedWeakReference != null) {
                this.b.remove(keyedWeakReference.getKey());
                continue;
            }
        } while (keyedWeakReference != null);
    }
}
