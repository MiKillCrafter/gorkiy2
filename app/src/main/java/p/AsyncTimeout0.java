package p;

import com.crashlytics.android.core.LogFileManager;
import j.a.a.a.outline;
import java.io.IOException;
import n.i.Collections;
import n.n.c.Intrinsics;

/* compiled from: AsyncTimeout.kt */
public final class AsyncTimeout0 implements Sink {
    public final /* synthetic */ AsyncTimeout b;
    public final /* synthetic */ Sink c;

    public AsyncTimeout0(AsyncTimeout asyncTimeout, x xVar) {
        this.b = asyncTimeout;
        this.c = xVar;
    }

    public void a(Buffer buffer, long j2) {
        if (buffer != null) {
            Collections.a(buffer.c, 0, j2);
            while (true) {
                long j3 = 0;
                if (j2 > 0) {
                    Segment segment = buffer.b;
                    if (segment != null) {
                        while (true) {
                            if (j3 >= ((long) LogFileManager.MAX_LOG_SIZE)) {
                                break;
                            }
                            j3 += (long) (segment.c - segment.b);
                            if (j3 >= j2) {
                                j3 = j2;
                                break;
                            }
                            segment = segment.f3071f;
                            if (segment == null) {
                                Intrinsics.a();
                                throw null;
                            }
                        }
                        this.b.f();
                        try {
                            this.c.a(buffer, j3);
                            j2 -= j3;
                            this.b.a(true);
                        } catch (IOException e2) {
                            throw this.b.a(e2);
                        } catch (Throwable th) {
                            this.b.a(false);
                            throw th;
                        }
                    } else {
                        Intrinsics.a();
                        throw null;
                    }
                } else {
                    return;
                }
            }
        } else {
            Intrinsics.a("source");
            throw null;
        }
    }

    public Timeout b() {
        return this.b;
    }

    public void close() {
        this.b.f();
        try {
            this.c.close();
            this.b.a(true);
        } catch (IOException e2) {
            throw this.b.a(e2);
        } catch (Throwable th) {
            this.b.a(false);
            throw th;
        }
    }

    public void flush() {
        this.b.f();
        try {
            this.c.flush();
            this.b.a(true);
        } catch (IOException e2) {
            throw this.b.a(e2);
        } catch (Throwable th) {
            this.b.a(false);
            throw th;
        }
    }

    public String toString() {
        StringBuilder a = outline.a("AsyncTimeout.sink(");
        a.append(this.c);
        a.append(')');
        return a.toString();
    }
}
