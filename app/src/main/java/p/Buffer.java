package p;

import androidx.recyclerview.widget.RecyclerView;
import com.crashlytics.android.answers.RetryManager;
import com.crashlytics.android.core.LogFileManager;
import j.a.a.a.outline;
import java.io.EOFException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.nio.charset.Charset;
import n.i.Collections;
import n.n.c.Intrinsics;
import n.r.Charsets;
import p.b0.ByteString;

/* compiled from: Buffer.kt */
public final class Buffer implements BufferedSource, BufferedSink, Cloneable, ByteChannel {
    public static final byte[] d;
    public Segment b;
    public long c;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [byte[], java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    static {
        byte[] bytes = "0123456789abcdef".getBytes(Charsets.a);
        Intrinsics.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
        d = bytes;
    }

    public ByteString b(long j2) {
        return new ByteString(i(j2));
    }

    public String c(long j2) {
        if (j2 >= 0) {
            long j3 = RecyclerView.FOREVER_NS;
            if (j2 != RecyclerView.FOREVER_NS) {
                j3 = j2 + 1;
            }
            byte b2 = (byte) 10;
            long a2 = a(b2, 0, j3);
            if (a2 != -1) {
                return f(a2);
            }
            if (j3 < this.c && e(j3 - 1) == ((byte) 13) && e(j3) == b2) {
                return f(j3);
            }
            Buffer buffer = new Buffer();
            a(buffer, 0, Math.min((long) 32, this.c));
            StringBuilder a3 = outline.a("\\n not found: limit=");
            a3.append(Math.min(this.c, j2));
            a3.append(" content=");
            a3.append(ByteString.d(buffer.l()));
            a3.append(8230);
            throw new EOFException(a3.toString());
        }
        throw new IllegalArgumentException(("limit < 0: " + j2).toString());
    }

    public BufferedSink c() {
        return this;
    }

    public void close() {
    }

    public boolean d(long j2) {
        return this.c >= j2;
    }

    public final byte e(long j2) {
        Collections.a(this.c, j2, 1);
        Segment segment = this.b;
        if (segment != null) {
            long j3 = this.c;
            if (j3 - j2 < j2) {
                while (j3 > j2) {
                    segment = segment.g;
                    if (segment != null) {
                        j3 -= (long) (segment.c - segment.b);
                    } else {
                        Intrinsics.a();
                        throw null;
                    }
                }
                return segment.a[(int) ((((long) segment.b) + j2) - j3)];
            }
            long j4 = 0;
            while (true) {
                int i2 = segment.c;
                int i3 = segment.b;
                long j5 = ((long) (i2 - i3)) + j4;
                if (j5 > j2) {
                    return segment.a[(int) ((((long) i3) + j2) - j4)];
                }
                segment = segment.f3071f;
                if (segment != null) {
                    j4 = j5;
                } else {
                    Intrinsics.a();
                    throw null;
                }
            }
        } else {
            Intrinsics.a();
            throw null;
        }
    }

    /* JADX WARN: Type inference failed for: r19v0, types: [java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r19) {
        /*
            r18 = this;
            r0 = r18
            r1 = r19
            r2 = 1
            if (r0 != r1) goto L_0x0008
            return r2
        L_0x0008:
            boolean r3 = r1 instanceof p.Buffer
            r4 = 0
            if (r3 != 0) goto L_0x000e
            return r4
        L_0x000e:
            long r5 = r0.c
            p.Buffer r1 = (p.Buffer) r1
            long r7 = r1.c
            int r3 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r3 == 0) goto L_0x0019
            return r4
        L_0x0019:
            r7 = 0
            int r3 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r3 != 0) goto L_0x0020
            return r2
        L_0x0020:
            p.Segment r3 = r0.b
            r5 = 0
            if (r3 == 0) goto L_0x0084
            p.Segment r1 = r1.b
            if (r1 == 0) goto L_0x0080
            int r6 = r3.b
            int r9 = r1.b
            r10 = r7
        L_0x002e:
            long r12 = r0.c
            int r14 = (r10 > r12 ? 1 : (r10 == r12 ? 0 : -1))
            if (r14 >= 0) goto L_0x007f
            int r12 = r3.c
            int r12 = r12 - r6
            int r13 = r1.c
            int r13 = r13 - r9
            int r12 = java.lang.Math.min(r12, r13)
            long r12 = (long) r12
            r14 = r7
        L_0x0040:
            int r16 = (r14 > r12 ? 1 : (r14 == r12 ? 0 : -1))
            if (r16 >= 0) goto L_0x005c
            byte[] r7 = r3.a
            int r8 = r6 + 1
            byte r6 = r7[r6]
            byte[] r7 = r1.a
            int r17 = r9 + 1
            byte r7 = r7[r9]
            if (r6 == r7) goto L_0x0053
            return r4
        L_0x0053:
            r6 = 1
            long r14 = r14 + r6
            r6 = r8
            r9 = r17
            r7 = 0
            goto L_0x0040
        L_0x005c:
            int r7 = r3.c
            if (r6 != r7) goto L_0x006b
            p.Segment r3 = r3.f3071f
            if (r3 == 0) goto L_0x0067
            int r6 = r3.b
            goto L_0x006b
        L_0x0067:
            n.n.c.Intrinsics.a()
            throw r5
        L_0x006b:
            int r7 = r1.c
            if (r9 != r7) goto L_0x007b
            p.Segment r1 = r1.f3071f
            if (r1 == 0) goto L_0x0077
            int r7 = r1.b
            r9 = r7
            goto L_0x007b
        L_0x0077:
            n.n.c.Intrinsics.a()
            throw r5
        L_0x007b:
            long r10 = r10 + r12
            r7 = 0
            goto L_0x002e
        L_0x007f:
            return r2
        L_0x0080:
            n.n.c.Intrinsics.a()
            throw r5
        L_0x0084:
            n.n.c.Intrinsics.a()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: p.Buffer.equals(java.lang.Object):boolean");
    }

    public final String f(long j2) {
        if (j2 > 0) {
            long j3 = j2 - 1;
            if (e(j3) == ((byte) 13)) {
                String a2 = a(j3, Charsets.a);
                skip(2);
                return a2;
            }
        }
        String a3 = a(j2, Charsets.a);
        skip(1);
        return a3;
    }

    public void flush() {
    }

    public void g(long j2) {
        if (this.c < j2) {
            throw new EOFException();
        }
    }

    public Buffer getBuffer() {
        return this;
    }

    public int hashCode() {
        Segment segment = this.b;
        if (segment == null) {
            return 0;
        }
        int i2 = 1;
        do {
            int i3 = segment.c;
            for (int i4 = segment.b; i4 < i3; i4++) {
                i2 = (i2 * 31) + segment.a[i4];
            }
            segment = segment.f3071f;
            if (segment == null) {
                Intrinsics.a();
                throw null;
            }
        } while (segment != this.b);
        return i2;
    }

    public final long i() {
        long j2 = this.c;
        if (j2 == 0) {
            return 0;
        }
        Segment segment = this.b;
        if (segment != null) {
            Segment segment2 = segment.g;
            if (segment2 != null) {
                int i2 = segment2.c;
                return (i2 >= 8192 || !segment2.f3070e) ? j2 : j2 - ((long) (i2 - segment2.b));
            }
            Intrinsics.a();
            throw null;
        }
        Intrinsics.a();
        throw null;
    }

    public boolean isOpen() {
        return true;
    }

    public boolean j() {
        return this.c == 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x008c, code lost:
        if (r8 != r9) goto L_0x0098;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x008e, code lost:
        r15.b = r6.a();
        p.SegmentPool.a(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0098, code lost:
        r6.b = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x009a, code lost:
        if (r1 != false) goto L_0x00a0;
     */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0075 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long k() {
        /*
            r15 = this;
            long r0 = r15.c
            r2 = 0
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 == 0) goto L_0x00ac
            r0 = 0
            r1 = 0
            r4 = r2
        L_0x000b:
            p.Segment r6 = r15.b
            if (r6 == 0) goto L_0x00a7
            byte[] r7 = r6.a
            int r8 = r6.b
            int r9 = r6.c
        L_0x0015:
            if (r8 >= r9) goto L_0x008c
            byte r10 = r7[r8]
            r11 = 48
            byte r11 = (byte) r11
            if (r10 < r11) goto L_0x0026
            r12 = 57
            byte r12 = (byte) r12
            if (r10 > r12) goto L_0x0026
            int r11 = r10 - r11
            goto L_0x003f
        L_0x0026:
            r11 = 97
            byte r11 = (byte) r11
            if (r10 < r11) goto L_0x0031
            r12 = 102(0x66, float:1.43E-43)
            byte r12 = (byte) r12
            if (r10 > r12) goto L_0x0031
            goto L_0x003b
        L_0x0031:
            r11 = 65
            byte r11 = (byte) r11
            if (r10 < r11) goto L_0x0071
            r12 = 70
            byte r12 = (byte) r12
            if (r10 > r12) goto L_0x0071
        L_0x003b:
            int r11 = r10 - r11
            int r11 = r11 + 10
        L_0x003f:
            r12 = -1152921504606846976(0xf000000000000000, double:-3.105036184601418E231)
            long r12 = r12 & r4
            int r14 = (r12 > r2 ? 1 : (r12 == r2 ? 0 : -1))
            if (r14 != 0) goto L_0x004f
            r10 = 4
            long r4 = r4 << r10
            long r10 = (long) r11
            long r4 = r4 | r10
            int r8 = r8 + 1
            int r0 = r0 + 1
            goto L_0x0015
        L_0x004f:
            p.Buffer r0 = new p.Buffer
            r0.<init>()
            r0.a(r4)
            r0.writeByte(r10)
            java.lang.NumberFormatException r1 = new java.lang.NumberFormatException
            java.lang.String r2 = "Number too large: "
            java.lang.StringBuilder r2 = j.a.a.a.outline.a(r2)
            java.lang.String r0 = r0.o()
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r1.<init>(r0)
            throw r1
        L_0x0071:
            if (r0 == 0) goto L_0x0075
            r1 = 1
            goto L_0x008c
        L_0x0075:
            java.lang.NumberFormatException r0 = new java.lang.NumberFormatException
            java.lang.String r1 = "Expected leading [0-9a-fA-F] character but was 0x"
            java.lang.StringBuilder r1 = j.a.a.a.outline.a(r1)
            java.lang.String r2 = java.lang.Integer.toHexString(r10)
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x008c:
            if (r8 != r9) goto L_0x0098
            p.Segment r7 = r6.a()
            r15.b = r7
            p.SegmentPool.a(r6)
            goto L_0x009a
        L_0x0098:
            r6.b = r8
        L_0x009a:
            if (r1 != 0) goto L_0x00a0
            p.Segment r6 = r15.b
            if (r6 != 0) goto L_0x000b
        L_0x00a0:
            long r1 = r15.c
            long r6 = (long) r0
            long r1 = r1 - r6
            r15.c = r1
            return r4
        L_0x00a7:
            n.n.c.Intrinsics.a()
            r0 = 0
            throw r0
        L_0x00ac:
            java.io.EOFException r0 = new java.io.EOFException
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: p.Buffer.k():long");
    }

    public ByteString l() {
        return new ByteString(i(this.c));
    }

    public InputStream m() {
        return new a(this);
    }

    public short n() {
        short readShort = readShort() & 65535;
        return (short) (((readShort & 255) << 8) | ((65280 & readShort) >>> 8));
    }

    public String o() {
        return a(this.c, Charsets.a);
    }

    public int p() {
        byte b2;
        int i2;
        byte b3;
        if (this.c != 0) {
            byte e2 = e(0);
            int i3 = 1;
            if ((e2 & 128) == 0) {
                b3 = e2 & Byte.MAX_VALUE;
                i2 = 1;
                b2 = 0;
            } else if ((e2 & 224) == 192) {
                b3 = e2 & 31;
                i2 = 2;
                b2 = 128;
            } else if ((e2 & 240) == 224) {
                b3 = e2 & 15;
                i2 = 3;
                b2 = 2048;
            } else if ((e2 & 248) == 240) {
                b3 = e2 & 7;
                i2 = 4;
                b2 = 65536;
            } else {
                skip(1);
                return 65533;
            }
            long j2 = (long) i2;
            if (this.c >= j2) {
                while (i3 < i2) {
                    long j3 = (long) i3;
                    byte e3 = e(j3);
                    if ((e3 & 192) == 128) {
                        b3 = (b3 << 6) | (e3 & 63);
                        i3++;
                    } else {
                        skip(j3);
                        return 65533;
                    }
                }
                skip(j2);
                if (b3 > 1114111) {
                    return 65533;
                }
                if ((55296 <= b3 && 57343 >= b3) || b3 < b2) {
                    return 65533;
                }
                return b3;
            }
            StringBuilder a2 = outline.a("size < ", i2, ": ");
            a2.append(this.c);
            a2.append(" (to read code point prefixed 0x");
            a2.append(Integer.toHexString(e2));
            a2.append(")");
            throw new EOFException(a2.toString());
        }
        throw new EOFException();
    }

    public int read(ByteBuffer byteBuffer) {
        if (byteBuffer != null) {
            Segment segment = this.b;
            if (segment == null) {
                return -1;
            }
            int min = Math.min(byteBuffer.remaining(), segment.c - segment.b);
            byteBuffer.put(segment.a, segment.b, min);
            int i2 = segment.b + min;
            segment.b = i2;
            this.c -= (long) min;
            if (i2 == segment.c) {
                this.b = segment.a();
                SegmentPool.a(segment);
            }
            return min;
        }
        Intrinsics.a("sink");
        throw null;
    }

    public byte readByte() {
        long j2 = this.c;
        if (j2 != 0) {
            Segment segment = this.b;
            if (segment != null) {
                int i2 = segment.b;
                int i3 = segment.c;
                int i4 = i2 + 1;
                byte b2 = segment.a[i2];
                this.c = j2 - 1;
                if (i4 == i3) {
                    this.b = segment.a();
                    SegmentPool.a(segment);
                } else {
                    segment.b = i4;
                }
                return b2;
            }
            Intrinsics.a();
            throw null;
        }
        throw new EOFException();
    }

    public int readInt() {
        long j2 = this.c;
        if (j2 >= 4) {
            Segment segment = this.b;
            if (segment != null) {
                int i2 = segment.b;
                int i3 = segment.c;
                if (((long) (i3 - i2)) < 4) {
                    return ((readByte() & 255) << 24) | ((readByte() & 255) << 16) | ((readByte() & 255) << 8) | (readByte() & 255);
                }
                byte[] bArr = segment.a;
                int i4 = i2 + 1;
                int i5 = i4 + 1;
                byte b2 = ((bArr[i2] & 255) << 24) | ((bArr[i4] & 255) << 16);
                int i6 = i5 + 1;
                byte b3 = b2 | ((bArr[i5] & 255) << 8);
                int i7 = i6 + 1;
                byte b4 = b3 | (bArr[i6] & 255);
                this.c = j2 - 4;
                if (i7 == i3) {
                    this.b = segment.a();
                    SegmentPool.a(segment);
                } else {
                    segment.b = i7;
                }
                return b4;
            }
            Intrinsics.a();
            throw null;
        }
        throw new EOFException();
    }

    public short readShort() {
        long j2 = this.c;
        if (j2 >= 2) {
            Segment segment = this.b;
            if (segment != null) {
                int i2 = segment.b;
                int i3 = segment.c;
                if (i3 - i2 < 2) {
                    return (short) (((readByte() & 255) << 8) | (readByte() & 255));
                }
                byte[] bArr = segment.a;
                int i4 = i2 + 1;
                int i5 = i4 + 1;
                byte b2 = ((bArr[i2] & 255) << 8) | (bArr[i4] & 255);
                this.c = j2 - 2;
                if (i5 == i3) {
                    this.b = segment.a();
                    SegmentPool.a(segment);
                } else {
                    segment.b = i5;
                }
                return (short) b2;
            }
            Intrinsics.a();
            throw null;
        }
        throw new EOFException();
    }

    public void skip(long j2) {
        while (j2 > 0) {
            Segment segment = this.b;
            if (segment != null) {
                int min = (int) Math.min(j2, (long) (segment.c - segment.b));
                long j3 = (long) min;
                this.c -= j3;
                j2 -= j3;
                int i2 = segment.b + min;
                segment.b = i2;
                if (i2 == segment.c) {
                    this.b = segment.a();
                    SegmentPool.a(segment);
                }
            } else {
                throw new EOFException();
            }
        }
    }

    public String toString() {
        ByteString byteString;
        int i2 = 0;
        if (this.c <= ((long) Integer.MAX_VALUE)) {
            long j2 = this.c;
            int i3 = (int) j2;
            if (i3 == 0) {
                byteString = ByteString.f3062e;
            } else {
                Collections.a(j2, 0, (long) i3);
                Segment segment = this.b;
                int i4 = 0;
                int i5 = 0;
                while (i4 < i3) {
                    if (segment != null) {
                        int i6 = segment.c;
                        int i7 = segment.b;
                        if (i6 != i7) {
                            i4 += i6 - i7;
                            i5++;
                            segment = segment.f3071f;
                        } else {
                            throw new AssertionError("s.limit == s.pos");
                        }
                    } else {
                        Intrinsics.a();
                        throw null;
                    }
                }
                byte[][] bArr = new byte[i5][];
                int[] iArr = new int[(i5 * 2)];
                Segment segment2 = this.b;
                int i8 = 0;
                while (i2 < i3) {
                    if (segment2 != null) {
                        bArr[i8] = segment2.a;
                        i2 += segment2.c - segment2.b;
                        iArr[i8] = Math.min(i2, i3);
                        iArr[i8 + i5] = segment2.b;
                        segment2.d = true;
                        i8++;
                        segment2 = segment2.f3071f;
                    } else {
                        Intrinsics.a();
                        throw null;
                    }
                }
                byteString = new SegmentedByteString(bArr, iArr, null);
            }
            return byteString.toString();
        }
        StringBuilder a2 = outline.a("size > Integer.MAX_VALUE: ");
        a2.append(this.c);
        throw new IllegalStateException(a2.toString().toString());
    }

    public Buffer b(int i2) {
        if (i2 < 128) {
            writeByte(i2);
        } else if (i2 < 2048) {
            Segment a2 = a(2);
            byte[] bArr = a2.a;
            int i3 = a2.c;
            bArr[i3] = (byte) ((i2 >> 6) | 192);
            bArr[i3 + 1] = (byte) ((i2 & 63) | 128);
            a2.c = i3 + 2;
            this.c += 2;
        } else if (55296 <= i2 && 57343 >= i2) {
            writeByte(63);
        } else if (i2 < 65536) {
            Segment a3 = a(3);
            byte[] bArr2 = a3.a;
            int i4 = a3.c;
            bArr2[i4] = (byte) ((i2 >> 12) | 224);
            bArr2[i4 + 1] = (byte) (((i2 >> 6) & 63) | 128);
            bArr2[i4 + 2] = (byte) ((i2 & 63) | 128);
            a3.c = i4 + 3;
            this.c += 3;
        } else if (i2 <= 1114111) {
            Segment a4 = a(4);
            byte[] bArr3 = a4.a;
            int i5 = a4.c;
            bArr3[i5] = (byte) ((i2 >> 18) | 240);
            bArr3[i5 + 1] = (byte) (((i2 >> 12) & 63) | 128);
            bArr3[i5 + 2] = (byte) (((i2 >> 6) & 63) | 128);
            bArr3[i5 + 3] = (byte) ((i2 & 63) | 128);
            a4.c = i5 + 4;
            this.c += 4;
        } else {
            StringBuilder a5 = outline.a("Unexpected code point: ");
            a5.append(Integer.toHexString(i2));
            throw new IllegalArgumentException(a5.toString());
        }
        return this;
    }

    public Buffer clone() {
        Buffer buffer = new Buffer();
        if (this.c == 0) {
            return buffer;
        }
        Segment segment = this.b;
        if (segment != null) {
            Segment b2 = segment.b();
            buffer.b = b2;
            if (b2 != null) {
                b2.g = b2;
                if (b2 == null) {
                    Intrinsics.a();
                    throw null;
                } else if (b2 != null) {
                    b2.f3071f = b2;
                    Segment segment2 = this.b;
                    if (segment2 != null) {
                        Segment segment3 = segment2.f3071f;
                        while (segment3 != this.b) {
                            Segment segment4 = buffer.b;
                            if (segment4 != null) {
                                Segment segment5 = segment4.g;
                                if (segment5 == null) {
                                    Intrinsics.a();
                                    throw null;
                                } else if (segment3 != null) {
                                    segment5.a(segment3.b());
                                    segment3 = segment3.f3071f;
                                } else {
                                    Intrinsics.a();
                                    throw null;
                                }
                            } else {
                                Intrinsics.a();
                                throw null;
                            }
                        }
                        buffer.c = this.c;
                        return buffer;
                    }
                    Intrinsics.a();
                    throw null;
                } else {
                    Intrinsics.a();
                    throw null;
                }
            } else {
                Intrinsics.a();
                throw null;
            }
        } else {
            Intrinsics.a();
            throw null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x004b, code lost:
        if (r6 != false) goto L_0x0050;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x004d, code lost:
        r1.readByte();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0050, code lost:
        r3 = j.a.a.a.outline.a("Number too large: ");
        r3.append(r1.o());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0066, code lost:
        throw new java.lang.NumberFormatException(r3.toString());
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long d() {
        /*
            r17 = this;
            r0 = r17
            long r1 = r0.c
            r3 = 0
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 == 0) goto L_0x00b6
            r1 = -7
            r5 = 0
            r6 = 0
            r7 = 0
        L_0x000f:
            p.Segment r8 = r0.b
            if (r8 == 0) goto L_0x00b1
            byte[] r9 = r8.a
            int r10 = r8.b
            int r11 = r8.c
        L_0x0019:
            if (r10 >= r11) goto L_0x0092
            byte r12 = r9[r10]
            r13 = 48
            byte r13 = (byte) r13
            if (r12 < r13) goto L_0x0067
            r14 = 57
            byte r14 = (byte) r14
            if (r12 > r14) goto L_0x0067
            int r13 = r13 - r12
            r14 = -922337203685477580(0xf333333333333334, double:-8.390303882365713E246)
            int r16 = (r3 > r14 ? 1 : (r3 == r14 ? 0 : -1))
            if (r16 < 0) goto L_0x0040
            if (r16 != 0) goto L_0x0039
            long r14 = (long) r13
            int r16 = (r14 > r1 ? 1 : (r14 == r1 ? 0 : -1))
            if (r16 >= 0) goto L_0x0039
            goto L_0x0040
        L_0x0039:
            r14 = 10
            long r3 = r3 * r14
            long r12 = (long) r13
            long r3 = r3 + r12
            goto L_0x0072
        L_0x0040:
            p.Buffer r1 = new p.Buffer
            r1.<init>()
            r1.h(r3)
            r1.writeByte(r12)
            if (r6 != 0) goto L_0x0050
            r1.readByte()
        L_0x0050:
            java.lang.NumberFormatException r2 = new java.lang.NumberFormatException
            java.lang.String r3 = "Number too large: "
            java.lang.StringBuilder r3 = j.a.a.a.outline.a(r3)
            java.lang.String r1 = r1.o()
            r3.append(r1)
            java.lang.String r1 = r3.toString()
            r2.<init>(r1)
            throw r2
        L_0x0067:
            r13 = 45
            byte r13 = (byte) r13
            if (r12 != r13) goto L_0x0077
            if (r5 != 0) goto L_0x0077
            r12 = 1
            long r1 = r1 - r12
            r6 = 1
        L_0x0072:
            int r10 = r10 + 1
            int r5 = r5 + 1
            goto L_0x0019
        L_0x0077:
            if (r5 == 0) goto L_0x007b
            r7 = 1
            goto L_0x0092
        L_0x007b:
            java.lang.NumberFormatException r1 = new java.lang.NumberFormatException
            java.lang.String r2 = "Expected leading [0-9] or '-' character but was 0x"
            java.lang.StringBuilder r2 = j.a.a.a.outline.a(r2)
            java.lang.String r3 = java.lang.Integer.toHexString(r12)
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2)
            throw r1
        L_0x0092:
            if (r10 != r11) goto L_0x009e
            p.Segment r9 = r8.a()
            r0.b = r9
            p.SegmentPool.a(r8)
            goto L_0x00a0
        L_0x009e:
            r8.b = r10
        L_0x00a0:
            if (r7 != 0) goto L_0x00a6
            p.Segment r8 = r0.b
            if (r8 != 0) goto L_0x000f
        L_0x00a6:
            long r1 = r0.c
            long r7 = (long) r5
            long r1 = r1 - r7
            r0.c = r1
            if (r6 == 0) goto L_0x00af
            goto L_0x00b0
        L_0x00af:
            long r3 = -r3
        L_0x00b0:
            return r3
        L_0x00b1:
            n.n.c.Intrinsics.a()
            r1 = 0
            throw r1
        L_0x00b6:
            java.io.EOFException r1 = new java.io.EOFException
            r1.<init>()
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: p.Buffer.d():long");
    }

    public Buffer h(long j2) {
        int i2 = (j2 > 0 ? 1 : (j2 == 0 ? 0 : -1));
        if (i2 == 0) {
            writeByte(48);
            return this;
        }
        boolean z = false;
        int i3 = 1;
        if (i2 < 0) {
            j2 = -j2;
            if (j2 < 0) {
                a("-9223372036854775808");
                return this;
            }
            z = true;
        }
        if (j2 >= 100000000) {
            i3 = j2 < 1000000000000L ? j2 < 10000000000L ? j2 < 1000000000 ? 9 : 10 : j2 < 100000000000L ? 11 : 12 : j2 < 1000000000000000L ? j2 < 10000000000000L ? 13 : j2 < 100000000000000L ? 14 : 15 : j2 < 100000000000000000L ? j2 < 10000000000000000L ? 16 : 17 : j2 < 1000000000000000000L ? 18 : 19;
        } else if (j2 >= 10000) {
            i3 = j2 < RetryManager.NANOSECONDS_IN_MS ? j2 < 100000 ? 5 : 6 : j2 < 10000000 ? 7 : 8;
        } else if (j2 >= 100) {
            i3 = j2 < 1000 ? 3 : 4;
        } else if (j2 >= 10) {
            i3 = 2;
        }
        if (z) {
            i3++;
        }
        Segment a2 = a(i3);
        byte[] bArr = a2.a;
        int i4 = a2.c + i3;
        while (j2 != 0) {
            long j3 = (long) 10;
            i4--;
            bArr[i4] = d[(int) (j2 % j3)];
            j2 /= j3;
        }
        if (z) {
            bArr[i4 - 1] = (byte) 45;
        }
        a2.c += i3;
        this.c += (long) i3;
        return this;
    }

    public Buffer writeByte(int i2) {
        Segment a2 = a(1);
        byte[] bArr = a2.a;
        int i3 = a2.c;
        a2.c = i3 + 1;
        bArr[i3] = (byte) i2;
        this.c++;
        return this;
    }

    public Buffer writeInt(int i2) {
        Segment a2 = a(4);
        byte[] bArr = a2.a;
        int i3 = a2.c;
        int i4 = i3 + 1;
        bArr[i3] = (byte) ((i2 >>> 24) & 255);
        int i5 = i4 + 1;
        bArr[i4] = (byte) ((i2 >>> 16) & 255);
        int i6 = i5 + 1;
        bArr[i5] = (byte) ((i2 >>> 8) & 255);
        bArr[i6] = (byte) (i2 & 255);
        a2.c = i6 + 1;
        this.c += 4;
        return this;
    }

    public Buffer writeShort(int i2) {
        Segment a2 = a(2);
        byte[] bArr = a2.a;
        int i3 = a2.c;
        int i4 = i3 + 1;
        bArr[i3] = (byte) ((i2 >>> 8) & 255);
        bArr[i4] = (byte) (i2 & 255);
        a2.c = i4 + 1;
        this.c += 2;
        return this;
    }

    /* compiled from: Buffer.kt */
    public static final class a extends InputStream {
        public final /* synthetic */ Buffer b;

        public a(Buffer buffer) {
            this.b = buffer;
        }

        public int available() {
            return (int) Math.min(this.b.c, (long) Integer.MAX_VALUE);
        }

        public void close() {
        }

        public int read() {
            Buffer buffer = this.b;
            if (buffer.c > 0) {
                return buffer.readByte() & 255;
            }
            return -1;
        }

        public String toString() {
            return this.b + ".inputStream()";
        }

        public int read(byte[] bArr, int i2, int i3) {
            if (bArr != null) {
                return this.b.a(bArr, i2, i3);
            }
            Intrinsics.a("sink");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: p.Buffer.write(byte[], int, int):p.Buffer
     arg types: [byte[], int, int]
     candidates:
      p.Buffer.write(byte[], int, int):p.BufferedSink
      p.BufferedSink.write(byte[], int, int):p.BufferedSink
      p.Buffer.write(byte[], int, int):p.Buffer */
    public Buffer write(byte[] bArr) {
        if (bArr != null) {
            write(bArr, 0, bArr.length);
            return this;
        }
        Intrinsics.a("source");
        throw null;
    }

    public final Buffer a(Buffer buffer, long j2, long j3) {
        if (buffer != null) {
            Collections.a(this.c, j2, j3);
            if (j3 == 0) {
                return this;
            }
            buffer.c += j3;
            Segment segment = this.b;
            while (segment != null) {
                int i2 = segment.c;
                int i3 = segment.b;
                if (j2 >= ((long) (i2 - i3))) {
                    j2 -= (long) (i2 - i3);
                    segment = segment.f3071f;
                } else {
                    while (j3 > 0) {
                        if (segment != null) {
                            Segment b2 = segment.b();
                            int i4 = b2.b + ((int) j2);
                            b2.b = i4;
                            b2.c = Math.min(i4 + ((int) j3), b2.c);
                            Segment segment2 = buffer.b;
                            if (segment2 == null) {
                                b2.g = b2;
                                b2.f3071f = b2;
                                buffer.b = b2;
                            } else if (segment2 != null) {
                                Segment segment3 = segment2.g;
                                if (segment3 != null) {
                                    segment3.a(b2);
                                } else {
                                    Intrinsics.a();
                                    throw null;
                                }
                            } else {
                                Intrinsics.a();
                                throw null;
                            }
                            j3 -= (long) (b2.c - b2.b);
                            segment = segment.f3071f;
                            j2 = 0;
                        } else {
                            Intrinsics.a();
                            throw null;
                        }
                    }
                    return this;
                }
            }
            Intrinsics.a();
            throw null;
        }
        Intrinsics.a("out");
        throw null;
    }

    public Buffer write(byte[] bArr, int i2, int i3) {
        if (bArr != null) {
            long j2 = (long) i3;
            Collections.a((long) bArr.length, (long) i2, j2);
            int i4 = i3 + i2;
            while (i2 < i4) {
                Segment a2 = a(1);
                int min = Math.min(i4 - i2, 8192 - a2.c);
                System.arraycopy(bArr, i2, a2.a, a2.c, min);
                i2 += min;
                a2.c += min;
            }
            this.c += j2;
            return this;
        }
        Intrinsics.a("source");
        throw null;
    }

    public byte[] i(long j2) {
        int i2 = 0;
        if (!(j2 >= 0 && j2 <= ((long) Integer.MAX_VALUE))) {
            throw new IllegalArgumentException(("byteCount: " + j2).toString());
        } else if (this.c >= j2) {
            int i3 = (int) j2;
            byte[] bArr = new byte[i3];
            while (i2 < i3) {
                int a2 = a(bArr, i2, i3 - i2);
                if (a2 != -1) {
                    i2 += a2;
                } else {
                    throw new EOFException();
                }
            }
            return bArr;
        } else {
            throw new EOFException();
        }
    }

    public String e() {
        return c(RecyclerView.FOREVER_NS);
    }

    public int write(ByteBuffer byteBuffer) {
        if (byteBuffer != null) {
            int remaining = byteBuffer.remaining();
            int i2 = remaining;
            while (i2 > 0) {
                Segment a2 = a(1);
                int min = Math.min(i2, 8192 - a2.c);
                byteBuffer.get(a2.a, a2.c, min);
                i2 -= min;
                a2.c += min;
            }
            this.c += (long) remaining;
            return remaining;
        }
        Intrinsics.a("source");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: p.Buffer.a(p.Options, boolean):int
     arg types: [p.Options, int]
     candidates:
      p.Buffer.a(long, java.nio.charset.Charset):java.lang.String
      p.Buffer.a(p.Buffer, long):void
      p.Sink.a(p.Buffer, long):void
      p.Buffer.a(p.Options, boolean):int */
    public int a(Options options) {
        if (options != null) {
            int a2 = a(options, false);
            if (a2 == -1) {
                return -1;
            }
            skip((long) options.b[a2].g());
            return a2;
        }
        Intrinsics.a("options");
        throw null;
    }

    public long b(Buffer buffer, long j2) {
        if (buffer != null) {
            if (j2 >= 0) {
                long j3 = this.c;
                if (j3 == 0) {
                    return -1;
                }
                if (j2 > j3) {
                    j2 = j3;
                }
                buffer.a(this, j2);
                return j2;
            }
            throw new IllegalArgumentException(("byteCount < 0: " + j2).toString());
        }
        Intrinsics.a("sink");
        throw null;
    }

    public Timeout b() {
        return Timeout.d;
    }

    public final int a(Options options, boolean z) {
        int i2;
        int i3;
        int i4;
        Segment segment;
        int i5;
        Options options2 = options;
        if (options2 != null) {
            Segment segment2 = this.b;
            int i6 = -2;
            if (segment2 == null) {
                return z ? -2 : -1;
            }
            byte[] bArr = segment2.a;
            int i7 = segment2.b;
            int i8 = segment2.c;
            int[] iArr = options2.c;
            Segment segment3 = segment2;
            int i9 = 0;
            int i10 = -1;
            loop0:
            while (true) {
                int i11 = i9 + 1;
                int i12 = iArr[i9];
                int i13 = i11 + 1;
                int i14 = iArr[i11];
                if (i14 != -1) {
                    i10 = i14;
                }
                if (segment3 == null) {
                    break;
                }
                if (i12 < 0) {
                    int i15 = (i12 * -1) + i13;
                    while (true) {
                        int i16 = i7 + 1;
                        int i17 = i13 + 1;
                        if ((bArr[i7] & 255) != iArr[i13]) {
                            return i10;
                        }
                        boolean z2 = i17 == i15;
                        if (i16 != i8) {
                            Segment segment4 = segment3;
                            i4 = i8;
                            i5 = i16;
                            segment = segment4;
                        } else if (segment3 != null) {
                            Segment segment5 = segment3.f3071f;
                            if (segment5 != null) {
                                i5 = segment5.b;
                                byte[] bArr2 = segment5.a;
                                i4 = segment5.c;
                                if (segment5 != segment2) {
                                    byte[] bArr3 = bArr2;
                                    segment = segment5;
                                    bArr = bArr3;
                                } else if (!z2) {
                                    break loop0;
                                } else {
                                    bArr = bArr2;
                                    segment = null;
                                }
                            } else {
                                Intrinsics.a();
                                throw null;
                            }
                        } else {
                            Intrinsics.a();
                            throw null;
                        }
                        if (z2) {
                            i2 = iArr[i17];
                            i3 = i5;
                            i8 = i4;
                            segment3 = segment;
                            break;
                        }
                        i7 = i5;
                        i8 = i4;
                        i13 = i17;
                        segment3 = segment;
                    }
                } else {
                    int i18 = i7 + 1;
                    byte b2 = bArr[i7] & 255;
                    int i19 = i13 + i12;
                    while (i13 != i19) {
                        if (b2 == iArr[i13]) {
                            i2 = iArr[i13 + i12];
                            if (i18 == i8) {
                                segment3 = segment3.f3071f;
                                if (segment3 != null) {
                                    i3 = segment3.b;
                                    bArr = segment3.a;
                                    i8 = segment3.c;
                                    if (segment3 == segment2) {
                                        segment3 = null;
                                    }
                                } else {
                                    Intrinsics.a();
                                    throw null;
                                }
                            } else {
                                i3 = i18;
                            }
                        } else {
                            i13++;
                        }
                    }
                    return i10;
                }
                if (i2 >= 0) {
                    return i2;
                }
                i9 = -i2;
                i7 = i3;
                i6 = -2;
            }
            return z ? i6 : i10;
        }
        Intrinsics.a("options");
        throw null;
    }

    public long a(Sink sink) {
        if (sink != null) {
            long j2 = this.c;
            if (j2 > 0) {
                sink.a(this, j2);
            }
            return j2;
        }
        Intrinsics.a("sink");
        throw null;
    }

    public String a(Charset charset) {
        if (charset != null) {
            return a(this.c, charset);
        }
        Intrinsics.a("charset");
        throw null;
    }

    public String a(long j2, Charset charset) {
        if (charset != null) {
            int i2 = (j2 > 0 ? 1 : (j2 == 0 ? 0 : -1));
            if (!(i2 >= 0 && j2 <= ((long) Integer.MAX_VALUE))) {
                throw new IllegalArgumentException(("byteCount: " + j2).toString());
            } else if (this.c < j2) {
                throw new EOFException();
            } else if (i2 == 0) {
                return "";
            } else {
                Segment segment = this.b;
                if (segment != null) {
                    int i3 = segment.b;
                    if (((long) i3) + j2 > ((long) segment.c)) {
                        return new String(i(j2), charset);
                    }
                    int i4 = (int) j2;
                    String str = new String(segment.a, i3, i4, charset);
                    int i5 = segment.b + i4;
                    segment.b = i5;
                    this.c -= j2;
                    if (i5 == segment.c) {
                        this.b = segment.a();
                        SegmentPool.a(segment);
                    }
                    return str;
                }
                Intrinsics.a();
                throw null;
            }
        } else {
            Intrinsics.a("charset");
            throw null;
        }
    }

    public int a(byte[] bArr, int i2, int i3) {
        if (bArr != null) {
            Collections.a((long) bArr.length, (long) i2, (long) i3);
            Segment segment = this.b;
            if (segment == null) {
                return -1;
            }
            int min = Math.min(i3, segment.c - segment.b);
            System.arraycopy(segment.a, segment.b, bArr, i2, min);
            int i4 = segment.b + min;
            segment.b = i4;
            this.c -= (long) min;
            if (i4 == segment.c) {
                this.b = segment.a();
                SegmentPool.a(segment);
            }
            return min;
        }
        Intrinsics.a("sink");
        throw null;
    }

    public Buffer a(ByteString byteString) {
        if (byteString != null) {
            byteString.a(this);
            return this;
        }
        Intrinsics.a("byteString");
        throw null;
    }

    public Buffer a(String str) {
        if (str != null) {
            a(str, 0, str.length());
            return this;
        }
        Intrinsics.a("string");
        throw null;
    }

    public Buffer a(String str, int i2, int i3) {
        char charAt;
        if (str != null) {
            if (i2 >= 0) {
                if (i3 >= i2) {
                    if (i3 <= str.length()) {
                        while (i2 < i3) {
                            char charAt2 = str.charAt(i2);
                            if (charAt2 < 128) {
                                Segment a2 = a(1);
                                byte[] bArr = a2.a;
                                int i4 = a2.c - i2;
                                int min = Math.min(i3, 8192 - i4);
                                int i5 = i2 + 1;
                                bArr[i2 + i4] = (byte) charAt2;
                                while (true) {
                                    i2 = i5;
                                    if (i2 >= min || (charAt = str.charAt(i2)) >= 128) {
                                        int i6 = a2.c;
                                        int i7 = (i4 + i2) - i6;
                                        a2.c = i6 + i7;
                                        this.c += (long) i7;
                                    } else {
                                        i5 = i2 + 1;
                                        bArr[i2 + i4] = (byte) charAt;
                                    }
                                }
                                int i62 = a2.c;
                                int i72 = (i4 + i2) - i62;
                                a2.c = i62 + i72;
                                this.c += (long) i72;
                            } else {
                                if (charAt2 < 2048) {
                                    Segment a3 = a(2);
                                    byte[] bArr2 = a3.a;
                                    int i8 = a3.c;
                                    bArr2[i8] = (byte) ((charAt2 >> 6) | 192);
                                    bArr2[i8 + 1] = (byte) ((charAt2 & '?') | 128);
                                    a3.c = i8 + 2;
                                    this.c += 2;
                                } else if (charAt2 < 55296 || charAt2 > 57343) {
                                    Segment a4 = a(3);
                                    byte[] bArr3 = a4.a;
                                    int i9 = a4.c;
                                    bArr3[i9] = (byte) ((charAt2 >> 12) | 224);
                                    bArr3[i9 + 1] = (byte) ((63 & (charAt2 >> 6)) | 128);
                                    bArr3[i9 + 2] = (byte) ((charAt2 & '?') | 128);
                                    a4.c = i9 + 3;
                                    this.c += 3;
                                } else {
                                    int i10 = i2 + 1;
                                    char charAt3 = i10 < i3 ? str.charAt(i10) : 0;
                                    if (charAt2 > 56319 || 56320 > charAt3 || 57343 < charAt3) {
                                        writeByte(63);
                                        i2 = i10;
                                    } else {
                                        int i11 = (((charAt2 & 1023) << 10) | (charAt3 & 1023)) + LogFileManager.MAX_LOG_SIZE;
                                        Segment a5 = a(4);
                                        byte[] bArr4 = a5.a;
                                        int i12 = a5.c;
                                        bArr4[i12] = (byte) ((i11 >> 18) | 240);
                                        bArr4[i12 + 1] = (byte) (((i11 >> 12) & 63) | 128);
                                        bArr4[i12 + 2] = (byte) (((i11 >> 6) & 63) | 128);
                                        bArr4[i12 + 3] = (byte) ((i11 & 63) | 128);
                                        a5.c = i12 + 4;
                                        this.c += 4;
                                        i2 += 2;
                                    }
                                }
                                i2++;
                            }
                        }
                        return this;
                    }
                    StringBuilder a6 = outline.a("endIndex > string.length: ", i3, " > ");
                    a6.append(str.length());
                    throw new IllegalArgumentException(a6.toString().toString());
                }
                throw new IllegalArgumentException(outline.a("endIndex < beginIndex: ", i3, " < ", i2).toString());
            }
            throw new IllegalArgumentException(outline.b("beginIndex < 0: ", i2).toString());
        }
        Intrinsics.a("string");
        throw null;
    }

    public long a(Source source) {
        if (source != null) {
            long j2 = 0;
            while (true) {
                long b2 = source.b(this, (long) 8192);
                if (b2 == -1) {
                    return j2;
                }
                j2 += b2;
            }
        } else {
            Intrinsics.a("source");
            throw null;
        }
    }

    public Buffer a(long j2) {
        if (j2 == 0) {
            writeByte(48);
            return this;
        }
        int numberOfTrailingZeros = (Long.numberOfTrailingZeros(Long.highestOneBit(j2)) / 4) + 1;
        Segment a2 = a(numberOfTrailingZeros);
        byte[] bArr = a2.a;
        int i2 = a2.c;
        for (int i3 = (i2 + numberOfTrailingZeros) - 1; i3 >= i2; i3--) {
            bArr[i3] = d[(int) (15 & j2)];
            j2 >>>= 4;
        }
        a2.c += numberOfTrailingZeros;
        this.c += (long) numberOfTrailingZeros;
        return this;
    }

    public final Segment a(int i2) {
        boolean z = true;
        if (i2 < 1 || i2 > 8192) {
            z = false;
        }
        if (z) {
            Segment segment = this.b;
            if (segment == null) {
                Segment a2 = SegmentPool.a();
                this.b = a2;
                a2.g = a2;
                a2.f3071f = a2;
                return a2;
            } else if (segment != null) {
                Segment segment2 = segment.g;
                if (segment2 == null) {
                    Intrinsics.a();
                    throw null;
                } else if (segment2.c + i2 <= 8192 && segment2.f3070e) {
                    return segment2;
                } else {
                    Segment a3 = SegmentPool.a();
                    segment2.a(a3);
                    return a3;
                }
            } else {
                Intrinsics.a();
                throw null;
            }
        } else {
            throw new IllegalArgumentException("unexpected capacity".toString());
        }
    }

    public void a(Buffer buffer, long j2) {
        Segment segment;
        Segment segment2;
        if (buffer != null) {
            if (buffer != this) {
                Collections.a(buffer.c, 0, j2);
                while (j2 > 0) {
                    Segment segment3 = buffer.b;
                    if (segment3 != null) {
                        int i2 = segment3.c;
                        if (segment3 != null) {
                            if (j2 < ((long) (i2 - segment3.b))) {
                                Segment segment4 = this.b;
                                if (segment4 == null) {
                                    segment = null;
                                } else if (segment4 != null) {
                                    segment = segment4.g;
                                } else {
                                    Intrinsics.a();
                                    throw null;
                                }
                                if (segment != null && segment.f3070e) {
                                    if ((((long) segment.c) + j2) - ((long) (segment.d ? 0 : segment.b)) <= ((long) 8192)) {
                                        Segment segment5 = buffer.b;
                                        if (segment5 != null) {
                                            segment5.a(segment, (int) j2);
                                            buffer.c -= j2;
                                            this.c += j2;
                                            return;
                                        }
                                        Intrinsics.a();
                                        throw null;
                                    }
                                }
                                Segment segment6 = buffer.b;
                                if (segment6 != null) {
                                    int i3 = (int) j2;
                                    if (segment6 != null) {
                                        if (i3 > 0 && i3 <= segment6.c - segment6.b) {
                                            if (i3 >= 1024) {
                                                segment2 = segment6.b();
                                            } else {
                                                segment2 = SegmentPool.a();
                                                Collections.b(segment6.a, segment6.b, segment2.a, 0, i3);
                                            }
                                            segment2.c = segment2.b + i3;
                                            segment6.b += i3;
                                            Segment segment7 = segment6.g;
                                            if (segment7 != null) {
                                                segment7.a(segment2);
                                                buffer.b = segment2;
                                            } else {
                                                Intrinsics.a();
                                                throw null;
                                            }
                                        } else {
                                            throw new IllegalArgumentException("byteCount out of range".toString());
                                        }
                                    } else {
                                        throw null;
                                    }
                                } else {
                                    Intrinsics.a();
                                    throw null;
                                }
                            }
                            Segment segment8 = buffer.b;
                            if (segment8 != null) {
                                long j3 = (long) (segment8.c - segment8.b);
                                buffer.b = segment8.a();
                                Segment segment9 = this.b;
                                if (segment9 == null) {
                                    this.b = segment8;
                                    segment8.g = segment8;
                                    segment8.f3071f = segment8;
                                } else if (segment9 != null) {
                                    Segment segment10 = segment9.g;
                                    if (segment10 != null) {
                                        segment10.a(segment8);
                                        if (segment8.g != segment8) {
                                            Segment segment11 = segment8.g;
                                            if (segment11 == null) {
                                                Intrinsics.a();
                                                throw null;
                                            } else if (!segment11.f3070e) {
                                                continue;
                                            } else {
                                                int i4 = segment8.c - segment8.b;
                                                if (i4 <= (8192 - segment11.c) + (segment11.d ? 0 : segment11.b)) {
                                                    Segment segment12 = segment8.g;
                                                    if (segment12 != null) {
                                                        segment8.a(segment12, i4);
                                                        segment8.a();
                                                        SegmentPool.a(segment8);
                                                    } else {
                                                        Intrinsics.a();
                                                        throw null;
                                                    }
                                                } else {
                                                    continue;
                                                }
                                            }
                                        } else {
                                            throw new IllegalStateException("cannot compact".toString());
                                        }
                                    } else {
                                        Intrinsics.a();
                                        throw null;
                                    }
                                } else {
                                    Intrinsics.a();
                                    throw null;
                                }
                                buffer.c -= j3;
                                this.c += j3;
                                j2 -= j3;
                            } else {
                                Intrinsics.a();
                                throw null;
                            }
                        } else {
                            Intrinsics.a();
                            throw null;
                        }
                    } else {
                        Intrinsics.a();
                        throw null;
                    }
                }
                return;
            }
            throw new IllegalArgumentException("source == this".toString());
        }
        Intrinsics.a("source");
        throw null;
    }

    public long a(byte b2, long j2, long j3) {
        Segment segment;
        byte b3 = b2;
        long j4 = j2;
        long j5 = j3;
        long j6 = 0;
        if (0 <= j4 && j5 >= j4) {
            long j7 = this.c;
            if (j5 > j7) {
                j5 = j7;
            }
            long j8 = -1;
            if (j4 == j5 || (segment = this.b) == null) {
                return -1;
            }
            long j9 = this.c;
            if (j9 - j4 < j4) {
                while (j9 > j4) {
                    segment = segment.g;
                    if (segment != null) {
                        j9 -= (long) (segment.c - segment.b);
                    } else {
                        Intrinsics.a();
                        throw null;
                    }
                }
                while (j9 < j5) {
                    byte[] bArr = segment.a;
                    int min = (int) Math.min((long) segment.c, (((long) segment.b) + j5) - j9);
                    for (int i2 = (int) ((((long) segment.b) + j4) - j9); i2 < min; i2++) {
                        if (bArr[i2] == b3) {
                            return ((long) (i2 - segment.b)) + j9;
                        }
                    }
                    j9 += (long) (segment.c - segment.b);
                    segment = segment.f3071f;
                    if (segment != null) {
                        j8 = -1;
                        j4 = j9;
                    } else {
                        Intrinsics.a();
                        throw null;
                    }
                }
                return j8;
            }
            while (true) {
                long j10 = ((long) (segment.c - segment.b)) + j6;
                if (j10 > j4) {
                    while (j6 < j5) {
                        byte[] bArr2 = segment.a;
                        int min2 = (int) Math.min((long) segment.c, (((long) segment.b) + j5) - j6);
                        for (int i3 = (int) ((((long) segment.b) + j4) - j6); i3 < min2; i3++) {
                            if (bArr2[i3] == b3) {
                                return ((long) (i3 - segment.b)) + j6;
                            }
                        }
                        j6 += (long) (segment.c - segment.b);
                        segment = segment.f3071f;
                        if (segment != null) {
                            j4 = j6;
                        } else {
                            Intrinsics.a();
                            throw null;
                        }
                    }
                    return -1;
                }
                segment = segment.f3071f;
                if (segment != null) {
                    j6 = j10;
                } else {
                    Intrinsics.a();
                    throw null;
                }
            }
        } else {
            StringBuilder a2 = outline.a("size=");
            a2.append(this.c);
            a2.append(" fromIndex=");
            a2.append(j4);
            a2.append(" toIndex=");
            a2.append(j5);
            throw new IllegalArgumentException(a2.toString().toString());
        }
    }
}
