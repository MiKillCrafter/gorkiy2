package p;

import java.io.Serializable;
import java.security.MessageDigest;
import n.i.Collections;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;

/* compiled from: ByteString.kt */
public class ByteString implements Serializable, Comparable<j> {

    /* renamed from: e  reason: collision with root package name */
    public static final ByteString f3062e = p.b0.ByteString.b;

    /* renamed from: f  reason: collision with root package name */
    public static final a f3063f = new a(null);
    public transient int b;
    public transient String c;
    public final byte[] d;

    public ByteString(byte[] bArr) {
        if (bArr != null) {
            this.d = bArr;
        } else {
            Intrinsics.a("data");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [byte[], java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public ByteString a(String str) {
        if (str != null) {
            byte[] digest = MessageDigest.getInstance(str).digest(this.d);
            Intrinsics.a((Object) digest, "MessageDigest.getInstance(algorithm).digest(data)");
            return new ByteString(digest);
        }
        Intrinsics.a("algorithm");
        throw null;
    }

    public int compareTo(Object obj) {
        ByteString byteString = (ByteString) obj;
        if (byteString != null) {
            return p.b0.ByteString.a(this, byteString);
        }
        Intrinsics.a("other");
        throw null;
    }

    public boolean equals(Object obj) {
        return p.b0.ByteString.a(this, obj);
    }

    public String f() {
        return p.b0.ByteString.a(this);
    }

    public int g() {
        return p.b0.ByteString.b(this);
    }

    public String h() {
        return p.b0.ByteString.d(this);
    }

    public int hashCode() {
        return p.b0.ByteString.c(this);
    }

    public byte[] i() {
        return p.b0.ByteString.e(this);
    }

    public ByteString j() {
        return p.b0.ByteString.f(this);
    }

    public String toString() {
        return p.b0.ByteString.g(this);
    }

    public byte a(int i2) {
        return p.b0.ByteString.a(this, i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: p.Buffer.write(byte[], int, int):p.Buffer
     arg types: [byte[], int, int]
     candidates:
      p.Buffer.write(byte[], int, int):p.BufferedSink
      p.BufferedSink.write(byte[], int, int):p.BufferedSink
      p.Buffer.write(byte[], int, int):p.Buffer */
    public void a(Buffer buffer) {
        if (buffer != null) {
            byte[] bArr = this.d;
            buffer.write(bArr, 0, bArr.length);
            return;
        }
        Intrinsics.a("buffer");
        throw null;
    }

    public boolean a(int i2, ByteString byteString, int i3, int i4) {
        if (byteString != null) {
            return p.b0.ByteString.a(this, i2, byteString, i3, i4);
        }
        Intrinsics.a("other");
        throw null;
    }

    /* compiled from: ByteString.kt */
    public static final class a {
        public /* synthetic */ a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public static ByteString a(a aVar, byte[] bArr, int i2, int i3, int i4) {
            if ((i4 & 1) != 0) {
                i2 = 0;
            }
            if ((i4 & 2) != 0) {
                i3 = bArr.length;
            }
            if (aVar == null) {
                throw null;
            } else if (bArr != null) {
                Collections.a((long) bArr.length, (long) i2, (long) i3);
                byte[] bArr2 = new byte[i3];
                System.arraycopy(bArr, i2, bArr2, 0, i3);
                return new ByteString(bArr2);
            } else {
                Intrinsics.a("$receiver");
                throw null;
            }
        }

        public final ByteString b(String str) {
            if (str != null) {
                return p.b0.ByteString.c(str);
            }
            Intrinsics.a("$receiver");
            throw null;
        }

        public final ByteString a(String str) {
            if (str != null) {
                return p.b0.ByteString.b(str);
            }
            Intrinsics.a("$receiver");
            throw null;
        }
    }

    public boolean a(int i2, byte[] bArr, int i3, int i4) {
        if (bArr != null) {
            return p.b0.ByteString.a(this, i2, bArr, i3, i4);
        }
        Intrinsics.a("other");
        throw null;
    }
}
