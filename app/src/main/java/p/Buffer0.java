package p;

import java.io.OutputStream;
import n.n.c.Intrinsics;

/* compiled from: Buffer.kt */
public final class Buffer0 extends OutputStream {
    public final /* synthetic */ Buffer b;

    public Buffer0(Buffer buffer) {
        this.b = buffer;
    }

    public void close() {
    }

    public void flush() {
    }

    public String toString() {
        return this.b + ".outputStream()";
    }

    public void write(int i2) {
        this.b.writeByte(i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: p.Buffer.write(byte[], int, int):p.Buffer
     arg types: [byte[], int, int]
     candidates:
      p.Buffer.write(byte[], int, int):p.BufferedSink
      p.BufferedSink.write(byte[], int, int):p.BufferedSink
      p.Buffer.write(byte[], int, int):p.Buffer */
    public void write(byte[] bArr, int i2, int i3) {
        if (bArr != null) {
            this.b.write(bArr, i2, i3);
        } else {
            Intrinsics.a("data");
            throw null;
        }
    }
}
