package p;

import com.crashlytics.android.answers.RetryManager;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.concurrent.TimeUnit;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;

/* compiled from: AsyncTimeout.kt */
public class AsyncTimeout extends Timeout {
    public static final long h = TimeUnit.SECONDS.toMillis(60);

    /* renamed from: i  reason: collision with root package name */
    public static final long f3057i = TimeUnit.MILLISECONDS.toNanos(h);

    /* renamed from: j  reason: collision with root package name */
    public static AsyncTimeout f3058j;

    /* renamed from: k  reason: collision with root package name */
    public static final a f3059k = new a(null);

    /* renamed from: e  reason: collision with root package name */
    public boolean f3060e;

    /* renamed from: f  reason: collision with root package name */
    public AsyncTimeout f3061f;
    public long g;

    /* compiled from: AsyncTimeout.kt */
    public static final class b extends Thread {
        public b() {
            super("Okio Watchdog");
            setDaemon(true);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:12:0x0013, code lost:
            if (r1 == null) goto L_0x0000;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x0015, code lost:
            r1.h();
         */
        /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r3 = this;
            L_0x0000:
                java.lang.Class<p.AsyncTimeout> r0 = p.AsyncTimeout.class
                monitor-enter(r0)     // Catch:{ InterruptedException -> 0x0000 }
                p.AsyncTimeout$a r1 = p.AsyncTimeout.f3059k     // Catch:{ all -> 0x0019 }
                p.AsyncTimeout r1 = r1.a()     // Catch:{ all -> 0x0019 }
                p.AsyncTimeout r2 = p.AsyncTimeout.f3058j     // Catch:{ all -> 0x0019 }
                if (r1 != r2) goto L_0x0012
                r1 = 0
                p.AsyncTimeout.f3058j = r1     // Catch:{ all -> 0x0019 }
                monitor-exit(r0)     // Catch:{ InterruptedException -> 0x0000 }
                return
            L_0x0012:
                monitor-exit(r0)     // Catch:{ InterruptedException -> 0x0000 }
                if (r1 == 0) goto L_0x0000
                r1.h()     // Catch:{ InterruptedException -> 0x0000 }
                goto L_0x0000
            L_0x0019:
                r1 = move-exception
                monitor-exit(r0)     // Catch:{ InterruptedException -> 0x0000 }
                throw r1     // Catch:{ InterruptedException -> 0x0000 }
            */
            throw new UnsupportedOperationException("Method not decompiled: p.AsyncTimeout.b.run():void");
        }
    }

    public final void a(boolean z) {
        if (g() && z) {
            throw b(null);
        }
    }

    public IOException b(IOException iOException) {
        InterruptedIOException interruptedIOException = new InterruptedIOException("timeout");
        if (iOException != null) {
            interruptedIOException.initCause(iOException);
        }
        return interruptedIOException;
    }

    public final void f() {
        if (!this.f3060e) {
            long j2 = super.c;
            boolean z = super.a;
            if (j2 != 0 || z) {
                this.f3060e = true;
                f3059k.a(this, j2, z);
                return;
            }
            return;
        }
        throw new IllegalStateException("Unbalanced enter/exit".toString());
    }

    public final boolean g() {
        if (!this.f3060e) {
            return false;
        }
        this.f3060e = false;
        return f3059k.a(this);
    }

    public void h() {
    }

    public final IOException a(IOException iOException) {
        if (iOException != null) {
            return !g() ? iOException : b(iOException);
        }
        Intrinsics.a("cause");
        throw null;
    }

    /* compiled from: AsyncTimeout.kt */
    public static final class a {
        public /* synthetic */ a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final AsyncTimeout a() {
            Class<AsyncTimeout> cls = AsyncTimeout.class;
            AsyncTimeout asyncTimeout = AsyncTimeout.f3058j;
            if (asyncTimeout != null) {
                AsyncTimeout asyncTimeout2 = asyncTimeout.f3061f;
                if (asyncTimeout2 == null) {
                    long nanoTime = System.nanoTime();
                    cls.wait(AsyncTimeout.h);
                    AsyncTimeout asyncTimeout3 = AsyncTimeout.f3058j;
                    if (asyncTimeout3 == null) {
                        Intrinsics.a();
                        throw null;
                    } else if (asyncTimeout3.f3061f != null || System.nanoTime() - nanoTime < AsyncTimeout.f3057i) {
                        return null;
                    } else {
                        return AsyncTimeout.f3058j;
                    }
                } else {
                    long nanoTime2 = asyncTimeout2.g - System.nanoTime();
                    if (nanoTime2 > 0) {
                        long j2 = nanoTime2 / RetryManager.NANOSECONDS_IN_MS;
                        cls.wait(j2, (int) (nanoTime2 - (RetryManager.NANOSECONDS_IN_MS * j2)));
                        return null;
                    }
                    AsyncTimeout asyncTimeout4 = AsyncTimeout.f3058j;
                    if (asyncTimeout4 != null) {
                        asyncTimeout4.f3061f = asyncTimeout2.f3061f;
                        asyncTimeout2.f3061f = null;
                        return asyncTimeout2;
                    }
                    Intrinsics.a();
                    throw null;
                }
            } else {
                Intrinsics.a();
                throw null;
            }
        }

        public final void a(AsyncTimeout asyncTimeout, long j2, boolean z) {
            Class<AsyncTimeout> cls = AsyncTimeout.class;
            synchronized (cls) {
                if (AsyncTimeout.f3058j == null) {
                    AsyncTimeout.f3058j = new AsyncTimeout();
                    new b().start();
                }
                long nanoTime = System.nanoTime();
                int i2 = (j2 > 0 ? 1 : (j2 == 0 ? 0 : -1));
                if (i2 != 0 && z) {
                    asyncTimeout.g = Math.min(j2, asyncTimeout.c() - nanoTime) + nanoTime;
                } else if (i2 != 0) {
                    asyncTimeout.g = j2 + nanoTime;
                } else if (z) {
                    asyncTimeout.g = asyncTimeout.c();
                } else {
                    throw new AssertionError();
                }
                long j3 = asyncTimeout.g - nanoTime;
                AsyncTimeout asyncTimeout2 = AsyncTimeout.f3058j;
                if (asyncTimeout2 != null) {
                    while (true) {
                        if (asyncTimeout2.f3061f != null) {
                            AsyncTimeout asyncTimeout3 = asyncTimeout2.f3061f;
                            if (asyncTimeout3 == null) {
                                Intrinsics.a();
                                throw null;
                            } else if (j3 < asyncTimeout3.g - nanoTime) {
                                break;
                            } else {
                                asyncTimeout2 = asyncTimeout2.f3061f;
                                if (asyncTimeout2 == null) {
                                    Intrinsics.a();
                                    throw null;
                                }
                            }
                        }
                    }
                    asyncTimeout.f3061f = asyncTimeout2.f3061f;
                    asyncTimeout2.f3061f = asyncTimeout;
                    if (asyncTimeout2 == AsyncTimeout.f3058j) {
                        cls.notify();
                    }
                } else {
                    Intrinsics.a();
                    throw null;
                }
            }
        }

        public final boolean a(AsyncTimeout asyncTimeout) {
            synchronized (AsyncTimeout.class) {
                for (AsyncTimeout asyncTimeout2 = AsyncTimeout.f3058j; asyncTimeout2 != null; asyncTimeout2 = asyncTimeout2.f3061f) {
                    if (asyncTimeout2.f3061f == asyncTimeout) {
                        asyncTimeout2.f3061f = asyncTimeout.f3061f;
                        asyncTimeout.f3061f = null;
                        return false;
                    }
                }
                return true;
            }
        }
    }
}
