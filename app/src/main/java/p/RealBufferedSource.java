package p;

import androidx.recyclerview.widget.RecyclerView;
import j.a.a.a.outline;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Arrays;
import n.i.Collections;
import n.n.c.Intrinsics;
import p.b0.ByteString;

/* compiled from: RealBufferedSource.kt */
public final class RealBufferedSource implements BufferedSource {
    public final Buffer b;
    public boolean c;
    public final Source d;

    public RealBufferedSource(Source source) {
        if (source != null) {
            this.d = source;
            this.b = new Buffer();
            return;
        }
        Intrinsics.a("source");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: p.Buffer.a(p.Options, boolean):int
     arg types: [p.Options, int]
     candidates:
      p.Buffer.a(long, java.nio.charset.Charset):java.lang.String
      p.Buffer.a(p.Buffer, long):void
      p.Sink.a(p.Buffer, long):void
      p.Buffer.a(p.Options, boolean):int */
    public int a(Options options) {
        if (options == null) {
            Intrinsics.a("options");
            throw null;
        } else if (!this.c) {
            do {
                int a2 = this.b.a(options, true);
                if (a2 != -2) {
                    if (a2 == -1) {
                        return -1;
                    }
                    this.b.skip((long) options.b[a2].g());
                    return a2;
                }
            } while (this.d.b(this.b, (long) 8192) != -1);
            return -1;
        } else {
            throw new IllegalStateException("closed".toString());
        }
    }

    public long b(Buffer buffer, long j2) {
        if (buffer != null) {
            if (!(j2 >= 0)) {
                throw new IllegalArgumentException(("byteCount < 0: " + j2).toString());
            } else if (true ^ this.c) {
                Buffer buffer2 = this.b;
                if (buffer2.c == 0 && this.d.b(buffer2, (long) 8192) == -1) {
                    return -1;
                }
                return this.b.b(buffer, Math.min(j2, this.b.c));
            } else {
                throw new IllegalStateException("closed".toString());
            }
        } else {
            Intrinsics.a("sink");
            throw null;
        }
    }

    public String c(long j2) {
        if (j2 >= 0) {
            long j3 = j2 == RecyclerView.FOREVER_NS ? Long.MAX_VALUE : j2 + 1;
            byte b2 = (byte) 10;
            long a2 = a(b2, 0, j3);
            if (a2 != -1) {
                return this.b.f(a2);
            }
            if (j3 < RecyclerView.FOREVER_NS && d(j3) && this.b.e(j3 - 1) == ((byte) 13) && d(1 + j3) && this.b.e(j3) == b2) {
                return this.b.f(j3);
            }
            Buffer buffer = new Buffer();
            Buffer buffer2 = this.b;
            buffer2.a(buffer, 0, Math.min((long) 32, buffer2.c));
            StringBuilder a3 = outline.a("\\n not found: limit=");
            a3.append(Math.min(this.b.c, j2));
            a3.append(" content=");
            a3.append(ByteString.d(buffer.l()));
            a3.append("…");
            throw new EOFException(a3.toString());
        }
        throw new IllegalArgumentException(("limit < 0: " + j2).toString());
    }

    public void close() {
        if (!this.c) {
            this.c = true;
            this.d.close();
            Buffer buffer = this.b;
            buffer.skip(buffer.c);
        }
    }

    public boolean d(long j2) {
        Buffer buffer;
        if (!(j2 >= 0)) {
            throw new IllegalArgumentException(("byteCount < 0: " + j2).toString());
        } else if (!this.c) {
            do {
                buffer = this.b;
                if (buffer.c >= j2) {
                    return true;
                }
            } while (this.d.b(buffer, (long) 8192) != -1);
            return false;
        } else {
            throw new IllegalStateException("closed".toString());
        }
    }

    public String e() {
        return c(RecyclerView.FOREVER_NS);
    }

    public void g(long j2) {
        if (!d(j2)) {
            throw new EOFException();
        }
    }

    public Buffer getBuffer() {
        return this.b;
    }

    public byte[] i(long j2) {
        if (d(j2)) {
            return this.b.i(j2);
        }
        throw new EOFException();
    }

    public boolean isOpen() {
        return !this.c;
    }

    public boolean j() {
        if (!(!this.c)) {
            throw new IllegalStateException("closed".toString());
        } else if (!this.b.j() || this.d.b(this.b, (long) 8192) != -1) {
            return false;
        } else {
            return true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x003b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long k() {
        /*
            r6 = this;
            r0 = 1
            r6.g(r0)
            r0 = 0
            r1 = 0
        L_0x0007:
            int r2 = r1 + 1
            long r3 = (long) r2
            boolean r3 = r6.d(r3)
            if (r3 == 0) goto L_0x0059
            p.Buffer r3 = r6.b
            long r4 = (long) r1
            byte r3 = r3.e(r4)
            r4 = 48
            byte r4 = (byte) r4
            if (r3 < r4) goto L_0x0021
            r4 = 57
            byte r4 = (byte) r4
            if (r3 <= r4) goto L_0x0036
        L_0x0021:
            r4 = 97
            byte r4 = (byte) r4
            if (r3 < r4) goto L_0x002b
            r4 = 102(0x66, float:1.43E-43)
            byte r4 = (byte) r4
            if (r3 <= r4) goto L_0x0036
        L_0x002b:
            r4 = 65
            byte r4 = (byte) r4
            if (r3 < r4) goto L_0x0038
            r4 = 70
            byte r4 = (byte) r4
            if (r3 <= r4) goto L_0x0036
            goto L_0x0038
        L_0x0036:
            r1 = r2
            goto L_0x0007
        L_0x0038:
            if (r1 == 0) goto L_0x003b
            goto L_0x0059
        L_0x003b:
            java.lang.NumberFormatException r1 = new java.lang.NumberFormatException
            r2 = 1
            java.lang.Object[] r4 = new java.lang.Object[r2]
            java.lang.Byte r3 = java.lang.Byte.valueOf(r3)
            r4[r0] = r3
            java.lang.Object[] r0 = java.util.Arrays.copyOf(r4, r2)
            java.lang.String r2 = "Expected leading [0-9a-fA-F] character but was %#x"
            java.lang.String r0 = java.lang.String.format(r2, r0)
            java.lang.String r2 = "java.lang.String.format(format, *args)"
            n.n.c.Intrinsics.a(r0, r2)
            r1.<init>(r0)
            throw r1
        L_0x0059:
            p.Buffer r0 = r6.b
            long r0 = r0.k()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: p.RealBufferedSource.k():long");
    }

    public InputStream m() {
        return new a(this);
    }

    public int read(ByteBuffer byteBuffer) {
        if (byteBuffer != null) {
            Buffer buffer = this.b;
            if (buffer.c == 0 && this.d.b(buffer, (long) 8192) == -1) {
                return -1;
            }
            return this.b.read(byteBuffer);
        }
        Intrinsics.a("sink");
        throw null;
    }

    public byte readByte() {
        g(1);
        return this.b.readByte();
    }

    public int readInt() {
        g(4);
        return this.b.readInt();
    }

    public short readShort() {
        g(2);
        return this.b.readShort();
    }

    public void skip(long j2) {
        if (!this.c) {
            while (j2 > 0) {
                Buffer buffer = this.b;
                if (buffer.c == 0 && this.d.b(buffer, (long) 8192) == -1) {
                    throw new EOFException();
                }
                long min = Math.min(j2, this.b.c);
                this.b.skip(min);
                j2 -= min;
            }
            return;
        }
        throw new IllegalStateException("closed".toString());
    }

    public String toString() {
        StringBuilder a2 = outline.a("buffer(");
        a2.append(this.d);
        a2.append(')');
        return a2.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public long d() {
        int i2;
        g(1);
        long j2 = 0;
        while (true) {
            long j3 = j2 + 1;
            if (!d(j3)) {
                break;
            }
            byte e2 = this.b.e(j2);
            if ((e2 >= ((byte) 48) && e2 <= ((byte) 57)) || (j2 == 0 && e2 == ((byte) 45))) {
                j2 = j3;
            } else if (i2 == 0) {
                String format = String.format("Expected leading [0-9] or '-' character but was %#x", Arrays.copyOf(new Object[]{Byte.valueOf(e2)}, 1));
                Intrinsics.a((Object) format, "java.lang.String.format(format, *args)");
                throw new NumberFormatException(format);
            }
        }
        return this.b.d();
    }

    /* compiled from: RealBufferedSource.kt */
    public static final class a extends InputStream {
        public final /* synthetic */ RealBufferedSource b;

        public a(RealBufferedSource realBufferedSource) {
            this.b = realBufferedSource;
        }

        public int available() {
            RealBufferedSource realBufferedSource = this.b;
            if (!realBufferedSource.c) {
                return (int) Math.min(realBufferedSource.b.c, (long) Integer.MAX_VALUE);
            }
            throw new IOException("closed");
        }

        public void close() {
            this.b.close();
        }

        public int read() {
            RealBufferedSource realBufferedSource = this.b;
            if (!realBufferedSource.c) {
                Buffer buffer = realBufferedSource.b;
                if (buffer.c == 0 && realBufferedSource.d.b(buffer, (long) 8192) == -1) {
                    return -1;
                }
                return this.b.b.readByte() & 255;
            }
            throw new IOException("closed");
        }

        public String toString() {
            return this.b + ".inputStream()";
        }

        public int read(byte[] bArr, int i2, int i3) {
            if (bArr == null) {
                Intrinsics.a("data");
                throw null;
            } else if (!this.b.c) {
                Collections.a((long) bArr.length, (long) i2, (long) i3);
                RealBufferedSource realBufferedSource = this.b;
                Buffer buffer = realBufferedSource.b;
                if (buffer.c == 0 && realBufferedSource.d.b(buffer, (long) 8192) == -1) {
                    return -1;
                }
                return this.b.b.a(bArr, i2, i3);
            } else {
                throw new IOException("closed");
            }
        }
    }

    public ByteString b(long j2) {
        if (d(j2)) {
            return this.b.b(j2);
        }
        throw new EOFException();
    }

    public long a(Sink sink) {
        if (sink != null) {
            long j2 = 0;
            while (this.d.b(this.b, (long) 8192) != -1) {
                long i2 = this.b.i();
                if (i2 > 0) {
                    j2 += i2;
                    sink.a(this.b, i2);
                }
            }
            Buffer buffer = this.b;
            long j3 = buffer.c;
            if (j3 <= 0) {
                return j2;
            }
            long j4 = j2 + j3;
            sink.a(buffer, j3);
            return j4;
        }
        Intrinsics.a("sink");
        throw null;
    }

    public Timeout b() {
        return this.d.b();
    }

    public int a() {
        g(4);
        int readInt = this.b.readInt();
        return ((readInt & 255) << 24) | ((-16777216 & readInt) >>> 24) | ((16711680 & readInt) >>> 8) | ((65280 & readInt) << 8);
    }

    public long a(byte b2, long j2, long j3) {
        boolean z = true;
        if (!this.c) {
            if (0 > j2 || j3 < j2) {
                z = false;
            }
            if (z) {
                while (j2 < j3) {
                    long a2 = this.b.a(b2, j2, j3);
                    if (a2 == -1) {
                        Buffer buffer = this.b;
                        long j4 = buffer.c;
                        if (j4 >= j3 || this.d.b(buffer, (long) 8192) == -1) {
                            break;
                        }
                        j2 = Math.max(j2, j4);
                    } else {
                        return a2;
                    }
                }
                return -1;
            }
            throw new IllegalArgumentException(("fromIndex=" + j2 + " toIndex=" + j3).toString());
        }
        throw new IllegalStateException("closed".toString());
    }

    public String a(Charset charset) {
        if (charset != null) {
            this.b.a(this.d);
            return this.b.a(charset);
        }
        Intrinsics.a("charset");
        throw null;
    }
}
