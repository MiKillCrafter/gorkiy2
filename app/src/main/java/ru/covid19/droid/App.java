package ru.covid19.droid;

import com.crashlytics.android.Crashlytics;
import e.a.b.BaseApp;
import e.a.b.c.CrashlyticsTree;
import l.a.a.a.Fabric;
import t.a.Timber;

/* compiled from: App.kt */
public final class App extends BaseApp {
    public void onCreate() {
        super.onCreate();
        Fabric.a(this, new Crashlytics());
        Timber.a(new CrashlyticsTree());
    }
}
