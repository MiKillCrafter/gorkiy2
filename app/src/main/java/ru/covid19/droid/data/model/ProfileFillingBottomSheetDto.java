package ru.covid19.droid.data.model;

import j.a.a.a.outline;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;

/* compiled from: ProfileFillingBottomSheetDto.kt */
public final class ProfileFillingBottomSheetDto {
    public final String citizenshipBottomSheetTitle;
    public final String genderBottomSheetTitle;
    public final String genderFemaleLabel;
    public final String genderMaleLabel;

    public ProfileFillingBottomSheetDto() {
        this(null, null, null, null, 15, null);
    }

    public ProfileFillingBottomSheetDto(String str, String str2, String str3, String str4) {
        if (str == null) {
            Intrinsics.a("genderBottomSheetTitle");
            throw null;
        } else if (str2 == null) {
            Intrinsics.a("genderMaleLabel");
            throw null;
        } else if (str3 == null) {
            Intrinsics.a("genderFemaleLabel");
            throw null;
        } else if (str4 != null) {
            this.genderBottomSheetTitle = str;
            this.genderMaleLabel = str2;
            this.genderFemaleLabel = str3;
            this.citizenshipBottomSheetTitle = str4;
        } else {
            Intrinsics.a("citizenshipBottomSheetTitle");
            throw null;
        }
    }

    public static /* synthetic */ ProfileFillingBottomSheetDto copy$default(ProfileFillingBottomSheetDto profileFillingBottomSheetDto, String str, String str2, String str3, String str4, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            str = profileFillingBottomSheetDto.genderBottomSheetTitle;
        }
        if ((i2 & 2) != 0) {
            str2 = profileFillingBottomSheetDto.genderMaleLabel;
        }
        if ((i2 & 4) != 0) {
            str3 = profileFillingBottomSheetDto.genderFemaleLabel;
        }
        if ((i2 & 8) != 0) {
            str4 = profileFillingBottomSheetDto.citizenshipBottomSheetTitle;
        }
        return profileFillingBottomSheetDto.copy(str, str2, str3, str4);
    }

    public final String component1() {
        return this.genderBottomSheetTitle;
    }

    public final String component2() {
        return this.genderMaleLabel;
    }

    public final String component3() {
        return this.genderFemaleLabel;
    }

    public final String component4() {
        return this.citizenshipBottomSheetTitle;
    }

    public final ProfileFillingBottomSheetDto copy(String str, String str2, String str3, String str4) {
        if (str == null) {
            Intrinsics.a("genderBottomSheetTitle");
            throw null;
        } else if (str2 == null) {
            Intrinsics.a("genderMaleLabel");
            throw null;
        } else if (str3 == null) {
            Intrinsics.a("genderFemaleLabel");
            throw null;
        } else if (str4 != null) {
            return new ProfileFillingBottomSheetDto(str, str2, str3, str4);
        } else {
            Intrinsics.a("citizenshipBottomSheetTitle");
            throw null;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ProfileFillingBottomSheetDto)) {
            return false;
        }
        ProfileFillingBottomSheetDto profileFillingBottomSheetDto = (ProfileFillingBottomSheetDto) obj;
        return Intrinsics.a(this.genderBottomSheetTitle, profileFillingBottomSheetDto.genderBottomSheetTitle) && Intrinsics.a(this.genderMaleLabel, profileFillingBottomSheetDto.genderMaleLabel) && Intrinsics.a(this.genderFemaleLabel, profileFillingBottomSheetDto.genderFemaleLabel) && Intrinsics.a(this.citizenshipBottomSheetTitle, profileFillingBottomSheetDto.citizenshipBottomSheetTitle);
    }

    public final String getCitizenshipBottomSheetTitle() {
        return this.citizenshipBottomSheetTitle;
    }

    public final String getGenderBottomSheetTitle() {
        return this.genderBottomSheetTitle;
    }

    public final String getGenderFemaleLabel() {
        return this.genderFemaleLabel;
    }

    public final String getGenderMaleLabel() {
        return this.genderMaleLabel;
    }

    public int hashCode() {
        String str = this.genderBottomSheetTitle;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.genderMaleLabel;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.genderFemaleLabel;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.citizenshipBottomSheetTitle;
        if (str4 != null) {
            i2 = str4.hashCode();
        }
        return hashCode3 + i2;
    }

    public String toString() {
        StringBuilder a = outline.a("ProfileFillingBottomSheetDto(genderBottomSheetTitle=");
        a.append(this.genderBottomSheetTitle);
        a.append(", genderMaleLabel=");
        a.append(this.genderMaleLabel);
        a.append(", genderFemaleLabel=");
        a.append(this.genderFemaleLabel);
        a.append(", citizenshipBottomSheetTitle=");
        return outline.a(a, this.citizenshipBottomSheetTitle, ")");
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ProfileFillingBottomSheetDto(String str, String str2, String str3, String str4, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this((i2 & 1) != 0 ? "" : str, (i2 & 2) != 0 ? "" : str2, (i2 & 4) != 0 ? "" : str3, (i2 & 8) != 0 ? "" : str4);
    }
}
