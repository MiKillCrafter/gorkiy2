package ru.covid19.droid.data.model;

/* compiled from: Statistic.kt */
public final class Statistic {
    public final Integer cases;
    public final Integer checked;
    public final Integer deaths;
    public final Integer recovered;

    public Statistic(Integer num, Integer num2, Integer num3, Integer num4) {
        this.cases = num;
        this.deaths = num2;
        this.recovered = num3;
        this.checked = num4;
    }

    public final Integer getCases() {
        return this.cases;
    }

    public final Integer getChecked() {
        return this.checked;
    }

    public final Integer getDeaths() {
        return this.deaths;
    }

    public final Integer getRecovered() {
        return this.recovered;
    }
}
