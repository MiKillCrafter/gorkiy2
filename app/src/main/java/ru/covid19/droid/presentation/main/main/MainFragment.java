package ru.covid19.droid.presentation.main.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.constraintlayout.widget.Group;
import com.google.android.material.button.MaterialButton;
import com.minsvyaz.gosuslugi.stopcorona.R;
import e.a.a.i.CoreComponentsHolder;
import e.a.a.i.b.CoreComponent;
import e.a.b.f.a.DaggerMainComponent;
import e.a.b.f.a.MainComponent;
import e.a.b.f.b.AppComponentsHolder;
import e.a.b.f.c.AddProfileStepsNavigationModule;
import e.a.b.f.c.MainModule;
import e.a.b.h.b.f.MainFragmentViewState;
import e.a.b.h.b.f.MainFragmentViewState0;
import e.a.b.h.b.f.MainFragmentVm;
import j.e.a.b.AnyToUnit;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import l.b.Observable;
import l.b.t.Function;
import n.Unit;
import n.i.Collections;
import n.i._Arrays;
import n.n.c.Intrinsics;
import ru.covid19.core.presentation.base.BaseDpFragment;
import ru.covid19.droid.data.model.Statistic;

/* compiled from: MainFragment.kt */
public final class MainFragment extends BaseDpFragment<e.a.b.h.b.f.c, e.a.b.h.b.f.d> {
    public HashMap d0;

    /* compiled from: MainFragment.kt */
    public static final class a<T, R> implements Function<T, R> {
        public static final a a = new a();

        public Object a(Object obj) {
            if (((Unit) obj) != null) {
                return MainFragmentViewState0.d.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: MainFragment.kt */
    public static final class b<T, R> implements Function<T, R> {
        public static final b a = new b();

        public Object a(Object obj) {
            if (((Unit) obj) != null) {
                return MainFragmentViewState0.e.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: MainFragment.kt */
    public static final class c<T, R> implements Function<T, R> {
        public static final c a = new c();

        public Object a(Object obj) {
            if (((Unit) obj) != null) {
                return MainFragmentViewState0.a.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: MainFragment.kt */
    public static final class d<T, R> implements Function<T, R> {
        public static final d a = new d();

        public Object a(Object obj) {
            if (((Unit) obj) != null) {
                return MainFragmentViewState0.b.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: MainFragment.kt */
    public static final class e<T, R> implements Function<T, R> {
        public static final e a = new e();

        public Object a(Object obj) {
            if (((Unit) obj) != null) {
                return MainFragmentViewState0.f.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: MainFragment.kt */
    public static final class f<T, R> implements Function<T, R> {
        public static final f a = new f();

        public Object a(Object obj) {
            if (((Unit) obj) != null) {
                return MainFragmentViewState0.c.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    public /* synthetic */ void C() {
        super.C();
        N();
    }

    public void N() {
        HashMap hashMap = this.d0;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public Class<e.a.b.h.b.f.d> P() {
        return MainFragmentVm.class;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.ProgressBar, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [androidx.constraintlayout.widget.Group, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void a(Object obj) {
        MainFragmentViewState mainFragmentViewState = (MainFragmentViewState) obj;
        String str = null;
        if (mainFragmentViewState != null) {
            ProgressBar progressBar = (ProgressBar) c(e.a.b.b.frag_main_pb_statistic);
            Intrinsics.a((Object) progressBar, "frag_main_pb_statistic");
            Collections.a(progressBar, mainFragmentViewState.a);
            Statistic statistic = mainFragmentViewState.b;
            if (statistic == null) {
                Group group = (Group) c(e.a.b.b.frag_main_group_statistic);
                Intrinsics.a((Object) group, "frag_main_group_statistic");
                Collections.a((View) group);
                return;
            }
            Group group2 = (Group) c(e.a.b.b.frag_main_group_statistic);
            Intrinsics.a((Object) group2, "frag_main_group_statistic");
            Collections.b(group2);
            TextView textView = (TextView) c(e.a.b.b.frag_main_tv_statistic_cases);
            Intrinsics.a((Object) textView, "frag_main_tv_statistic_cases");
            Integer cases = statistic.getCases();
            textView.setText(cases != null ? String.valueOf(cases.intValue()) : null);
            TextView textView2 = (TextView) c(e.a.b.b.frag_main_tv_statistic_recovered);
            Intrinsics.a((Object) textView2, "frag_main_tv_statistic_recovered");
            Integer recovered = statistic.getRecovered();
            textView2.setText(recovered != null ? String.valueOf(recovered.intValue()) : null);
            TextView textView3 = (TextView) c(e.a.b.b.frag_main_tv_statistic_deaths);
            Intrinsics.a((Object) textView3, "frag_main_tv_statistic_deaths");
            Integer deaths = statistic.getDeaths();
            if (deaths != null) {
                str = String.valueOf(deaths.intValue());
            }
            textView3.setText(str);
            return;
        }
        Intrinsics.a("vs");
        throw null;
    }

    public void b(Bundle bundle) {
        if (AppComponentsHolder.b == null) {
            CoreComponent coreComponent = CoreComponentsHolder.b;
            if (coreComponent == null) {
                Intrinsics.b("coreComponent");
                throw null;
            } else if (coreComponent != null) {
                MainModule mainModule = new MainModule();
                AddProfileStepsNavigationModule addProfileStepsNavigationModule = new AddProfileStepsNavigationModule();
                j.c.a.a.c.n.c.a(coreComponent, CoreComponent.class);
                AppComponentsHolder.b = new DaggerMainComponent(mainModule, addProfileStepsNavigationModule, coreComponent, null);
            } else {
                throw null;
            }
        }
        MainComponent mainComponent = AppComponentsHolder.b;
        if (mainComponent != null) {
            mainComponent.a(this.a0);
            super.b(bundle);
            return;
        }
        Intrinsics.a();
        throw null;
    }

    public View c(int i2) {
        if (this.d0 == null) {
            this.d0 = new HashMap();
        }
        View view = (View) this.d0.get(Integer.valueOf(i2));
        if (view != null) {
            return view;
        }
        View view2 = this.H;
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i2);
        this.d0.put(Integer.valueOf(i2), findViewById);
        return findViewById;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [com.google.android.material.button.MaterialButton, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<R>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i._Arrays.a(java.util.Collection, java.lang.Iterable):java.util.List<T>
     arg types: [java.util.List<l.b.Observable<? extends e.c.c.b>>, java.util.List]
     candidates:
      n.i._Arrays.a(java.lang.Iterable, java.util.Collection):C
      n.i._Arrays.a(java.util.Collection, java.lang.Iterable):java.util.List<T> */
    public List<Observable<? extends e.c.c.b>> g() {
        List<Observable<? extends e.c.c.b>> g = super.g();
        MaterialButton materialButton = (MaterialButton) c(e.a.b.b.frag_main_btn_survey_accept);
        Intrinsics.a((Object) materialButton, "frag_main_btn_survey_accept");
        Observable<R> c2 = j.c.a.a.c.n.c.a((View) materialButton).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c2, "RxView.clicks(this).map(AnyToUnit)");
        TextView textView = (TextView) c(e.a.b.b.frag_main_tv_virus);
        Intrinsics.a((Object) textView, "frag_main_tv_virus");
        Observable<R> c3 = j.c.a.a.c.n.c.a((View) textView).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c3, "RxView.clicks(this).map(AnyToUnit)");
        TextView textView2 = (TextView) c(e.a.b.b.frag_main_tv_flight);
        Intrinsics.a((Object) textView2, "frag_main_tv_flight");
        Observable<R> c4 = j.c.a.a.c.n.c.a((View) textView2).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c4, "RxView.clicks(this).map(AnyToUnit)");
        TextView textView3 = (TextView) c(e.a.b.b.frag_main_tv_call);
        Intrinsics.a((Object) textView3, "frag_main_tv_call");
        Observable<R> c5 = j.c.a.a.c.n.c.a((View) textView3).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c5, "RxView.clicks(this).map(AnyToUnit)");
        TextView textView4 = (TextView) c(e.a.b.b.frag_main_tv_contact);
        Intrinsics.a((Object) textView4, "frag_main_tv_contact");
        Observable<R> c6 = j.c.a.a.c.n.c.a((View) textView4).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c6, "RxView.clicks(this).map(AnyToUnit)");
        TextView textView5 = (TextView) c(e.a.b.b.frag_main_tv_sick);
        Intrinsics.a((Object) textView5, "frag_main_tv_sick");
        Observable<R> c7 = j.c.a.a.c.n.c.a((View) textView5).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c7, "RxView.clicks(this).map(AnyToUnit)");
        return _Arrays.a((Collection) g, (Iterable) Collections.a((Object[]) new Observable[]{c2.c((Function) a.a), c3.c((Function) b.a), c4.c((Function) c.a), c5.c((Function) d.a), c6.c((Function) e.a), c7.c((Function) f.a)}));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        if (layoutInflater != null) {
            return layoutInflater.inflate((int) R.layout.frag_main, viewGroup, false);
        }
        Intrinsics.a("inflater");
        throw null;
    }
}
