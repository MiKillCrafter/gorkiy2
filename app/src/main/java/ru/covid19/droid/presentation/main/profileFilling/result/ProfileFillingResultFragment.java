package ru.covid19.droid.presentation.main.profileFilling.result;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.minsvyaz.gosuslugi.stopcorona.R;
import e.a.a.a.b.BaseEvents;
import e.a.a.g.a.g.IApiConfig2;
import e.a.a.i.CoreComponentsHolder;
import e.a.a.i.b.CoreComponent;
import e.a.a.m.a.GlideAuthHeaderFactory;
import e.a.a.m.a.GlideHelper;
import e.a.b.f.a.DaggerMainComponent;
import e.a.b.f.a.MainComponent;
import e.a.b.f.b.AppComponentsHolder;
import e.a.b.f.c.AddProfileStepsNavigationModule;
import e.a.b.f.c.MainModule;
import e.a.b.h.b.h.n.ProfileFillingResultFragmentViewState;
import e.a.b.h.b.h.n.ProfileFillingResultFragmentViewState0;
import e.a.b.h.b.h.n.ProfileFillingResultFragmentViewState1;
import e.a.b.h.b.h.n.ProfileFillingResultFragmentViewState2;
import e.a.b.h.b.h.n.ProfileFillingResultFragmentViewState3;
import e.a.b.h.b.h.n.ProfileFillingResultFragmentVm;
import e.a.b.h.b.h.n.f;
import e.a.b.h.b.h.n.g;
import j.b.a.Glide;
import j.b.a.RequestBuilder;
import j.b.a.RequestManager;
import j.b.a.m.m.DiskCacheStrategy;
import j.b.a.m.m.k;
import j.b.a.m.n.GlideUrl;
import j.b.a.m.n.LazyHeaders;
import j.e.a.b.AnyToUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import l.b.Observable;
import l.b.t.Function;
import n.Unit;
import n.i.Collections;
import n.i._Arrays;
import n.n.c.Intrinsics;
import ru.covid19.core.presentation.base.BaseDpFragment;
import ru.covid19.droid.data.model.profileData.Children;
import ru.covid19.droid.data.model.profileData.ProfileData;

/* compiled from: ProfileFillingResultFragment.kt */
public final class ProfileFillingResultFragment extends BaseDpFragment<f, g> {
    public HashMap d0;

    /* compiled from: ProfileFillingResultFragment.kt */
    public static final class a<T, R> implements Function<T, R> {
        public static final a a = new a();

        public Object a(Object obj) {
            if (((Unit) obj) != null) {
                return BaseEvents.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingResultFragment.kt */
    public static final class b<T, R> implements Function<T, R> {
        public static final b a = new b();

        public Object a(Object obj) {
            if (((Unit) obj) != null) {
                return ProfileFillingResultFragmentViewState3.b.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingResultFragment.kt */
    public static final class c<T, R> implements Function<T, R> {
        public static final c a = new c();

        public Object a(Object obj) {
            if (((Unit) obj) != null) {
                return ProfileFillingResultFragmentViewState3.a.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    public /* synthetic */ void C() {
        super.C();
        N();
    }

    public void N() {
        HashMap hashMap = this.d0;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public Class<g> P() {
        return ProfileFillingResultFragmentVm.class;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void a(Object obj) {
        int i2;
        View view;
        ProfileFillingResultFragmentViewState0 profileFillingResultFragmentViewState0 = (ProfileFillingResultFragmentViewState0) obj;
        if (profileFillingResultFragmentViewState0 != null) {
            ProfileData profileData = profileFillingResultFragmentViewState0.a;
            ProfileFillingResultFragmentVm profileFillingResultFragmentVm = (ProfileFillingResultFragmentVm) h();
            GlideHelper glideHelper = profileFillingResultFragmentVm.f697k;
            String b2 = profileFillingResultFragmentVm.f698l.b();
            if (b2 != null) {
                Map<e.c.b.j.b, String> a2 = glideHelper.a.a();
                String str = (a2 != null ? a2.get(IApiConfig2.a) : null) + "api/storage/v1/files/" + b2 + "/88/face_photo/download";
                LazyHeaders.a aVar = new LazyHeaders.a();
                GlideAuthHeaderFactory glideAuthHeaderFactory = new GlideAuthHeaderFactory(glideHelper.b);
                aVar.a();
                Object obj2 = aVar.b.get("Cookie");
                if (obj2 == null) {
                    obj2 = new ArrayList();
                    aVar.b.put("Cookie", obj2);
                }
                obj2.add(glideAuthHeaderFactory);
                aVar.a = true;
                GlideUrl glideUrl = new GlideUrl(str, new LazyHeaders(aVar.b));
                if (profileData != null) {
                    ArrayList arrayList = new ArrayList();
                    arrayList.add(new ProfileFillingResultFragmentViewState1(glideUrl));
                    if ((profileData.getPassenger().getFullName().length() == 0) || profileData.getPassenger().getBirthDate() == null) {
                        i2 = 1;
                    } else {
                        String fullName = profileData.getPassenger().getFullName();
                        Integer birthDate = profileData.getPassenger().getBirthDate();
                        if (birthDate != null) {
                            String a3 = Collections.a(birthDate.intValue(), (String) null, 1);
                            if (a3 == null) {
                                a3 = "";
                            }
                            arrayList.add(new ProfileFillingResultFragmentViewState2(fullName, a3, 1));
                            i2 = 2;
                        } else {
                            Intrinsics.a();
                            throw null;
                        }
                    }
                    for (Children next : profileData.getChildren()) {
                        if (!(next.getFullName().length() == 0)) {
                            String fullName2 = next.getFullName();
                            String a4 = Collections.a(next.getBirthDate(), (String) null, 1);
                            if (a4 == null) {
                                a4 = "";
                            }
                            arrayList.add(new ProfileFillingResultFragmentViewState2(fullName2, a4, i2));
                            i2++;
                        }
                    }
                    ((LinearLayout) c(e.a.b.b.frag_profile_filling_result_info_items_container)).removeAllViews();
                    Iterator it = arrayList.iterator();
                    int i3 = 0;
                    while (it.hasNext()) {
                        Object next2 = it.next();
                        int i4 = i3 + 1;
                        if (i3 >= 0) {
                            ProfileFillingResultFragmentViewState profileFillingResultFragmentViewState = (ProfileFillingResultFragmentViewState) next2;
                            LinearLayout linearLayout = (LinearLayout) c(e.a.b.b.frag_profile_filling_result_info_items_container);
                            if (profileFillingResultFragmentViewState instanceof ProfileFillingResultFragmentViewState2) {
                                LayoutInflater layoutInflater = this.P;
                                if (layoutInflater == null) {
                                    layoutInflater = e(null);
                                }
                                view = layoutInflater.inflate((int) R.layout.item_profile_filling_result_person_info, (ViewGroup) null, false);
                                Intrinsics.a((Object) view, "viewPassenger");
                                TextView textView = (TextView) view.findViewById(e.a.b.b.item_filling_profile_result_info_person_info);
                                Intrinsics.a((Object) textView, "viewPassenger.item_filli…e_result_info_person_info");
                                ProfileFillingResultFragmentViewState2 profileFillingResultFragmentViewState2 = (ProfileFillingResultFragmentViewState2) profileFillingResultFragmentViewState;
                                textView.setText(v().getString(R.string.frag_filling_profile_result_person_item_placeholder, Integer.valueOf(profileFillingResultFragmentViewState2.c), profileFillingResultFragmentViewState2.a, profileFillingResultFragmentViewState2.b));
                            } else if (profileFillingResultFragmentViewState instanceof ProfileFillingResultFragmentViewState1) {
                                LayoutInflater layoutInflater2 = this.P;
                                if (layoutInflater2 == null) {
                                    layoutInflater2 = e(null);
                                }
                                view = layoutInflater2.inflate((int) R.layout.item_profile_filling_result_image, (ViewGroup) null, false);
                                RequestManager b3 = Glide.b(J());
                                GlideUrl glideUrl2 = ((ProfileFillingResultFragmentViewState1) profileFillingResultFragmentViewState).a;
                                if (b3 != null) {
                                    RequestBuilder requestBuilder = new RequestBuilder(b3.b, b3, Drawable.class, b3.c);
                                    requestBuilder.G = glideUrl2;
                                    requestBuilder.J = true;
                                    Intrinsics.a((Object) view, "viewPassenger");
                                    ((RequestBuilder) ((RequestBuilder) ((RequestBuilder) requestBuilder.a((k) DiskCacheStrategy.a)).a(true)).b()).a((ImageView) view.findViewById(e.a.b.b.item_filling_profile_result_info_image));
                                } else {
                                    throw null;
                                }
                            } else {
                                view = null;
                            }
                            if (view != null) {
                                linearLayout.addView(view);
                                i3 = i4;
                            } else {
                                throw new IllegalArgumentException("Item can be only listed type");
                            }
                        } else {
                            Collections.a();
                            throw null;
                        }
                    }
                    return;
                }
                Intrinsics.a("$this$toFillingProfileResultItems");
                throw null;
            }
            Intrinsics.a("oid");
            throw null;
        }
        Intrinsics.a("vs");
        throw null;
    }

    public void b(Bundle bundle) {
        if (AppComponentsHolder.b == null) {
            CoreComponent coreComponent = CoreComponentsHolder.b;
            if (coreComponent == null) {
                Intrinsics.b("coreComponent");
                throw null;
            } else if (coreComponent != null) {
                MainModule mainModule = new MainModule();
                AddProfileStepsNavigationModule addProfileStepsNavigationModule = new AddProfileStepsNavigationModule();
                j.c.a.a.c.n.c.a(coreComponent, CoreComponent.class);
                AppComponentsHolder.b = new DaggerMainComponent(mainModule, addProfileStepsNavigationModule, coreComponent, null);
            } else {
                throw null;
            }
        }
        MainComponent mainComponent = AppComponentsHolder.b;
        if (mainComponent != null) {
            mainComponent.a(this.a0);
            super.b(bundle);
            return;
        }
        Intrinsics.a();
        throw null;
    }

    public View c(int i2) {
        if (this.d0 == null) {
            this.d0 = new HashMap();
        }
        View view = (View) this.d0.get(Integer.valueOf(i2));
        if (view != null) {
            return view;
        }
        View view2 = this.H;
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i2);
        this.d0.put(Integer.valueOf(i2), findViewById);
        return findViewById;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.ImageButton, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<R>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [androidx.constraintlayout.widget.ConstraintLayout, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i._Arrays.a(java.util.Collection, java.lang.Iterable):java.util.List<T>
     arg types: [java.util.List<l.b.Observable<? extends e.c.c.b>>, java.util.List]
     candidates:
      n.i._Arrays.a(java.lang.Iterable, java.util.Collection):C
      n.i._Arrays.a(java.util.Collection, java.lang.Iterable):java.util.List<T> */
    public List<Observable<? extends e.c.c.b>> g() {
        List<Observable<? extends e.c.c.b>> g = super.g();
        ImageButton imageButton = (ImageButton) c(e.a.b.b.frag_profile_filling_btn_exit);
        Intrinsics.a((Object) imageButton, "frag_profile_filling_btn_exit");
        Observable<R> c2 = j.c.a.a.c.n.c.a((View) imageButton).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c2, "RxView.clicks(this).map(AnyToUnit)");
        ConstraintLayout constraintLayout = (ConstraintLayout) c(e.a.b.b.frag_filling_profile_result_self_isolation_rules_container);
        Intrinsics.a((Object) constraintLayout, "frag_filling_profile_res…isolation_rules_container");
        Observable<R> c3 = j.c.a.a.c.n.c.a((View) constraintLayout).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c3, "RxView.clicks(this).map(AnyToUnit)");
        ConstraintLayout constraintLayout2 = (ConstraintLayout) c(e.a.b.b.frag_filling_profile_result_self_quarantine_termination_responsibility_container);
        Intrinsics.a((Object) constraintLayout2, "frag_filling_profile_res…_responsibility_container");
        Observable<R> c4 = j.c.a.a.c.n.c.a((View) constraintLayout2).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c4, "RxView.clicks(this).map(AnyToUnit)");
        return _Arrays.a((Collection) g, (Iterable) Collections.a((Object[]) new Observable[]{c2.c((Function) a.a), c3.c((Function) b.a), c4.c((Function) c.a)}));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        if (layoutInflater != null) {
            return layoutInflater.inflate((int) R.layout.frag_filling_profile_result, viewGroup, false);
        }
        Intrinsics.a("inflater");
        throw null;
    }
}
