package ru.covid19.droid.presentation.main.questionnaire.pages;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.textfield.TextInputEditText;
import com.minsvyaz.gosuslugi.stopcorona.R;
import e.a.a.i.CoreComponentsHolder;
import e.a.a.i.b.CoreComponent;
import e.a.b.f.a.DaggerMainComponent;
import e.a.b.f.a.MainComponent;
import e.a.b.f.b.AppComponentsHolder;
import e.a.b.f.c.AddProfileStepsNavigationModule;
import e.a.b.f.c.MainModule;
import e.a.b.h.b.i.c.FillingProfileFullNamePageFragmentViewState;
import e.a.b.h.b.i.c.FillingProfileFullNamePageFragmentViewState1;
import e.a.b.h.b.i.c.FillingProfileFullNamePageFragmentViewState3;
import e.a.b.h.b.i.c.FillingProfileFullNamePageFragmentVm;
import e.c.c.BaseViewState2;
import j.e.a.InitialValueObservable;
import j.e.a.b.AnyToUnit;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import l.b.Observable;
import l.b.t.Function;
import n.Unit;
import n.i.Collections;
import n.i._Arrays;
import n.n.b.Functions0;
import n.n.c.Intrinsics;
import q.b.a.Instant;
import q.b.a.LocalDate;
import q.b.a.LocalDateTime;
import q.b.a.LocalTime;
import q.b.a.ZoneId;
import q.b.a.ZoneOffset;
import q.b.a.ZonedDateTime;
import q.b.a.t.DateTimeFormatter;
import q.b.a.w.ZoneOffsetTransition;
import ru.covid19.core.data.network.model.PersonalResponse;
import ru.covid19.droid.data.model.ProfileFillingBottomSheetDto;
import ru.covid19.droid.presentation.main.profileFilling.BaseStepValidatedFragment;

/* compiled from: FillingProfileFullNamePageFragment.kt */
public final class FillingProfileFullNamePageFragment extends BaseStepValidatedFragment<e.a.b.h.b.i.c.d, e.a.b.h.b.i.c.e> {
    public HashMap d0;

    /* compiled from: java-style lambda group */
    public static final class a<T, R> implements Function<T, R> {
        public static final a b = new a(0);
        public static final a c = new a(1);
        public final /* synthetic */ int a;

        public a(int i2) {
            this.a = i2;
        }

        public final Object a(Object obj) {
            int i2 = this.a;
            if (i2 != 0) {
                if (i2 != 1) {
                    throw null;
                } else if (((Unit) obj) != null) {
                    return FillingProfileFullNamePageFragmentViewState1.a.a;
                } else {
                    Intrinsics.a("it");
                    throw null;
                }
            } else if (((Unit) obj) != null) {
                return FillingProfileFullNamePageFragmentViewState1.a.a;
            } else {
                Intrinsics.a("it");
                throw null;
            }
        }
    }

    /* compiled from: FillingProfileFullNamePageFragment.kt */
    public static final class b<T, R> implements Function<T, R> {
        public static final b a = new b();

        public Object a(Object obj) {
            Boolean bool = (Boolean) obj;
            if (bool != null) {
                return new FillingProfileFullNamePageFragmentViewState1.e(bool.booleanValue());
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: FillingProfileFullNamePageFragment.kt */
    public static final class c<T, R> implements Function<T, R> {
        public static final c a = new c();

        public Object a(Object obj) {
            if (((Unit) obj) != null) {
                return FillingProfileFullNamePageFragmentViewState1.n.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: FillingProfileFullNamePageFragment.kt */
    public static final class d<T, R> implements Function<T, R> {
        public static final d a = new d();

        public Object a(Object obj) {
            if (((Unit) obj) != null) {
                return FillingProfileFullNamePageFragmentViewState1.d.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: FillingProfileFullNamePageFragment.kt */
    public static final class e<T, R> implements Function<T, R> {
        public static final e a = new e();

        public Object a(Object obj) {
            CharSequence charSequence = (CharSequence) obj;
            if (charSequence != null) {
                return new FillingProfileFullNamePageFragmentViewState1.k(charSequence.toString());
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: FillingProfileFullNamePageFragment.kt */
    public static final class f<T, R> implements Function<T, R> {
        public static final f a = new f();

        public Object a(Object obj) {
            CharSequence charSequence = (CharSequence) obj;
            if (charSequence != null) {
                return new FillingProfileFullNamePageFragmentViewState1.h(charSequence.toString());
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: FillingProfileFullNamePageFragment.kt */
    public static final class g<T, R> implements Function<T, R> {
        public static final g a = new g();

        public Object a(Object obj) {
            CharSequence charSequence = (CharSequence) obj;
            if (charSequence != null) {
                return new FillingProfileFullNamePageFragmentViewState1.i(charSequence.toString());
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: FillingProfileFullNamePageFragment.kt */
    public static final class h<T, R> implements Function<T, R> {
        public static final h a = new h();

        public Object a(Object obj) {
            CharSequence charSequence = (CharSequence) obj;
            if (charSequence != null) {
                return new FillingProfileFullNamePageFragmentViewState1.j(charSequence.toString());
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: FillingProfileFullNamePageFragment.kt */
    public static final class i<T, R> implements Function<T, R> {
        public static final i a = new i();

        public Object a(Object obj) {
            CharSequence charSequence = (CharSequence) obj;
            if (charSequence != null) {
                return new FillingProfileFullNamePageFragmentViewState1.g(charSequence.toString());
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: FillingProfileFullNamePageFragment.kt */
    public static final class j extends n.n.c.j implements Functions0<Date, n.g> {
        public final /* synthetic */ FillingProfileFullNamePageFragment c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(FillingProfileFullNamePageFragment fillingProfileFullNamePageFragment) {
            super(1);
            this.c = fillingProfileFullNamePageFragment;
        }

        public Object a(Object obj) {
            Date date = (Date) obj;
            if (date != null) {
                this.c.c0.a((e.c.c.b) new FillingProfileFullNamePageFragmentViewState1.f(date));
                return Unit.a;
            }
            Intrinsics.a("date");
            throw null;
        }
    }

    public /* synthetic */ void C() {
        super.C();
        N();
    }

    public void N() {
        HashMap hashMap = this.d0;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public Class<e.a.b.h.b.i.c.e> P() {
        return FillingProfileFullNamePageFragmentVm.class;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [com.google.android.material.textfield.TextInputLayout, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.Boolean, boolean]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [androidx.appcompat.widget.AppCompatCheckBox, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.ImageView, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [androidx.constraintlayout.widget.ConstraintLayout, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x010d  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0157  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.lang.Object r9) {
        /*
            r8 = this;
            e.a.b.h.b.i.c.FillingProfileFullNamePageFragmentViewState r9 = (e.a.b.h.b.i.c.FillingProfileFullNamePageFragmentViewState) r9
            r0 = 0
            if (r9 == 0) goto L_0x0174
            int r1 = e.a.b.b.frag_filling_profile_full_name_page_til_living_place
            android.view.View r1 = r8.c(r1)
            com.google.android.material.textfield.TextInputLayout r1 = (com.google.android.material.textfield.TextInputLayout) r1
            java.lang.String r2 = "frag_filling_profile_ful…ame_page_til_living_place"
            n.n.c.Intrinsics.a(r1, r2)
            ru.covid19.droid.data.model.profileData.Passenger r2 = r9.c
            java.lang.Boolean r2 = r2.getRegistrationAddressMatchesPlacement()
            r3 = 0
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r3)
            boolean r2 = n.n.c.Intrinsics.a(r2, r3)
            r1.setEnabled(r2)
            int r1 = e.a.b.b.frag_filling_profile_full_name_page_cb_matches_fact_address
            android.view.View r1 = r8.c(r1)
            androidx.appcompat.widget.AppCompatCheckBox r1 = (androidx.appcompat.widget.AppCompatCheckBox) r1
            java.lang.String r2 = "frag_filling_profile_ful…e_cb_matches_fact_address"
            n.n.c.Intrinsics.a(r1, r2)
            ru.covid19.droid.data.model.profileData.Passenger r2 = r9.c
            java.lang.Boolean r2 = r2.getRegistrationAddressMatchesPlacement()
            r3 = 1
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r3)
            boolean r2 = n.n.c.Intrinsics.a(r2, r4)
            r1.setChecked(r2)
            int r1 = e.a.b.b.frag_filling_profile_full_name_page_ed_living_place
            android.view.View r1 = r8.c(r1)
            com.google.android.material.textfield.TextInputEditText r1 = (com.google.android.material.textfield.TextInputEditText) r1
            ru.covid19.droid.data.model.profileData.Passenger r2 = r9.c
            java.lang.String r2 = r2.getPlacementAddress()
            r1.setText(r2)
            int r1 = e.a.b.b.frag_filling_profile_full_name_page_ed_name
            android.view.View r1 = r8.c(r1)
            com.google.android.material.textfield.TextInputEditText r1 = (com.google.android.material.textfield.TextInputEditText) r1
            ru.covid19.droid.data.model.profileData.Passenger r2 = r9.c
            java.lang.String r2 = r2.getFirstName()
            r1.setText(r2)
            int r1 = e.a.b.b.frag_filling_profile_full_name_page_ed_surname
            android.view.View r1 = r8.c(r1)
            com.google.android.material.textfield.TextInputEditText r1 = (com.google.android.material.textfield.TextInputEditText) r1
            ru.covid19.droid.data.model.profileData.Passenger r2 = r9.c
            java.lang.String r2 = r2.getLastName()
            r1.setText(r2)
            int r1 = e.a.b.b.frag_filling_profile_full_name_page_ed_patronymic
            android.view.View r1 = r8.c(r1)
            com.google.android.material.textfield.TextInputEditText r1 = (com.google.android.material.textfield.TextInputEditText) r1
            ru.covid19.droid.data.model.profileData.Passenger r2 = r9.c
            java.lang.String r2 = r2.getPatronymic()
            r1.setText(r2)
            int r1 = e.a.b.b.frag_filling_profile_full_name_page_ed_birthday
            android.view.View r1 = r8.c(r1)
            com.google.android.material.textfield.TextInputEditText r1 = (com.google.android.material.textfield.TextInputEditText) r1
            ru.covid19.droid.data.model.profileData.Passenger r2 = r9.c
            java.lang.Integer r2 = r2.getBirthDate()
            if (r2 == 0) goto L_0x00a0
            int r2 = r2.intValue()
            java.lang.String r2 = n.i.Collections.a(r2, r0, r3)
            goto L_0x00a1
        L_0x00a0:
            r2 = r0
        L_0x00a1:
            r1.setText(r2)
            int r1 = e.a.b.b.frag_filling_profile_full_name_page_ed_gender
            android.view.View r1 = r8.c(r1)
            com.google.android.material.textfield.TextInputEditText r1 = (com.google.android.material.textfield.TextInputEditText) r1
            ru.covid19.droid.data.model.profileData.Passenger r2 = r9.c
            ru.covid19.droid.data.model.profileData.ProfileData0 r2 = r2.getGender()
            if (r2 != 0) goto L_0x00b5
            goto L_0x00bd
        L_0x00b5:
            int r2 = r2.ordinal()
            if (r2 == 0) goto L_0x00c8
            if (r2 == r3) goto L_0x00c0
        L_0x00bd:
            java.lang.String r2 = ""
            goto L_0x00cf
        L_0x00c0:
            r2 = 2131623973(0x7f0e0025, float:1.8875113E38)
            java.lang.String r2 = r8.a(r2)
            goto L_0x00cf
        L_0x00c8:
            r2 = 2131623974(0x7f0e0026, float:1.8875115E38)
            java.lang.String r2 = r8.a(r2)
        L_0x00cf:
            r1.setText(r2)
            int r1 = e.a.b.b.frag_filling_profile_full_name_page_ed_citizenship
            android.view.View r1 = r8.c(r1)
            com.google.android.material.textfield.TextInputEditText r1 = (com.google.android.material.textfield.TextInputEditText) r1
            ru.covid19.droid.data.model.profileData.Passenger r2 = r9.c
            java.lang.String r2 = r2.getCitizenshipName()
            r1.setText(r2)
            int r1 = e.a.b.b.frag_filling_profile_full_name_page_ed_registration_address
            android.view.View r1 = r8.c(r1)
            com.google.android.material.textfield.TextInputEditText r1 = (com.google.android.material.textfield.TextInputEditText) r1
            ru.covid19.droid.data.model.profileData.Passenger r2 = r9.c
            java.lang.String r2 = r2.getRegistrationAddress()
            r1.setText(r2)
            int r1 = e.a.b.b.frag_filling_profile_full_name_page_ed_living_place
            android.view.View r1 = r8.c(r1)
            com.google.android.material.textfield.TextInputEditText r1 = (com.google.android.material.textfield.TextInputEditText) r1
            ru.covid19.droid.data.model.profileData.Passenger r2 = r9.c
            java.lang.String r2 = r2.getPlacementAddress()
            r1.setText(r2)
            android.net.Uri r1 = r9.b
            java.lang.String r2 = "frag_filling_profile_full_name_page_add_photo"
            java.lang.String r4 = "frag_filling_profile_full_name_page_iv_photo"
            if (r1 == 0) goto L_0x0157
            android.content.Context r1 = r8.J()
            j.b.a.RequestManager r1 = j.b.a.Glide.b(r1)
            android.net.Uri r9 = r9.b
            if (r1 == 0) goto L_0x0156
            java.lang.Class<android.graphics.drawable.Drawable> r0 = android.graphics.drawable.Drawable.class
            j.b.a.RequestBuilder r5 = new j.b.a.RequestBuilder
            j.b.a.Glide r6 = r1.b
            android.content.Context r7 = r1.c
            r5.<init>(r6, r1, r0, r7)
            r5.G = r9
            r5.J = r3
            j.b.a.q.BaseRequestOptions r9 = r5.b()
            j.b.a.RequestBuilder r9 = (j.b.a.RequestBuilder) r9
            int r0 = e.a.b.b.frag_filling_profile_full_name_page_iv_photo
            android.view.View r0 = r8.c(r0)
            android.widget.ImageView r0 = (android.widget.ImageView) r0
            r9.a(r0)
            int r9 = e.a.b.b.frag_filling_profile_full_name_page_iv_photo
            android.view.View r9 = r8.c(r9)
            android.widget.ImageView r9 = (android.widget.ImageView) r9
            n.n.c.Intrinsics.a(r9, r4)
            n.i.Collections.b(r9)
            int r9 = e.a.b.b.frag_filling_profile_full_name_page_add_photo
            android.view.View r9 = r8.c(r9)
            androidx.constraintlayout.widget.ConstraintLayout r9 = (androidx.constraintlayout.widget.ConstraintLayout) r9
            n.n.c.Intrinsics.a(r9, r2)
            n.i.Collections.a(r9)
            goto L_0x0173
        L_0x0156:
            throw r0
        L_0x0157:
            int r9 = e.a.b.b.frag_filling_profile_full_name_page_iv_photo
            android.view.View r9 = r8.c(r9)
            android.widget.ImageView r9 = (android.widget.ImageView) r9
            n.n.c.Intrinsics.a(r9, r4)
            n.i.Collections.a(r9)
            int r9 = e.a.b.b.frag_filling_profile_full_name_page_add_photo
            android.view.View r9 = r8.c(r9)
            androidx.constraintlayout.widget.ConstraintLayout r9 = (androidx.constraintlayout.widget.ConstraintLayout) r9
            n.n.c.Intrinsics.a(r9, r2)
            n.i.Collections.b(r9)
        L_0x0173:
            return
        L_0x0174:
            java.lang.String r9 = "vs"
            n.n.c.Intrinsics.a(r9)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: ru.covid19.droid.presentation.main.questionnaire.pages.FillingProfileFullNamePageFragment.a(java.lang.Object):void");
    }

    public void b(Bundle bundle) {
        if (AppComponentsHolder.b == null) {
            CoreComponent coreComponent = CoreComponentsHolder.b;
            if (coreComponent == null) {
                Intrinsics.b("coreComponent");
                throw null;
            } else if (coreComponent != null) {
                MainModule mainModule = new MainModule();
                AddProfileStepsNavigationModule addProfileStepsNavigationModule = new AddProfileStepsNavigationModule();
                j.c.a.a.c.n.c.a(coreComponent, CoreComponent.class);
                AppComponentsHolder.b = new DaggerMainComponent(mainModule, addProfileStepsNavigationModule, coreComponent, null);
            } else {
                throw null;
            }
        }
        MainComponent mainComponent = AppComponentsHolder.b;
        if (mainComponent != null) {
            mainComponent.a(this.a0);
            super.b(bundle);
            return;
        }
        Intrinsics.a();
        throw null;
    }

    public View c(int i2) {
        if (this.d0 == null) {
            this.d0 = new HashMap();
        }
        View view = (View) this.d0.get(Integer.valueOf(i2));
        if (view != null) {
            return view;
        }
        View view2 = this.H;
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i2);
        this.d0.put(Integer.valueOf(i2), findViewById);
        return findViewById;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [com.google.android.material.textfield.TextInputEditText, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<R>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [androidx.constraintlayout.widget.ConstraintLayout, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.ImageView, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [androidx.appcompat.widget.AppCompatCheckBox, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [j.e.a.InitialValueObservable<java.lang.Boolean>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i._Arrays.a(java.util.Collection, java.lang.Iterable):java.util.List<T>
     arg types: [java.util.List<l.b.Observable<? extends e.c.c.b>>, java.util.List]
     candidates:
      n.i._Arrays.a(java.lang.Iterable, java.util.Collection):C
      n.i._Arrays.a(java.util.Collection, java.lang.Iterable):java.util.List<T> */
    public List<Observable<? extends e.c.c.b>> g() {
        List<Observable<? extends e.c.c.b>> g2 = super.g();
        TextInputEditText textInputEditText = (TextInputEditText) c(e.a.b.b.frag_filling_profile_full_name_page_ed_gender);
        Intrinsics.a((Object) textInputEditText, "frag_filling_profile_full_name_page_ed_gender");
        Observable<R> c2 = j.c.a.a.c.n.c.a((View) textInputEditText).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c2, "RxView.clicks(this).map(AnyToUnit)");
        TextInputEditText textInputEditText2 = (TextInputEditText) c(e.a.b.b.frag_filling_profile_full_name_page_ed_citizenship);
        Intrinsics.a((Object) textInputEditText2, "frag_filling_profile_full_name_page_ed_citizenship");
        Observable<R> c3 = j.c.a.a.c.n.c.a((View) textInputEditText2).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c3, "RxView.clicks(this).map(AnyToUnit)");
        ConstraintLayout constraintLayout = (ConstraintLayout) c(e.a.b.b.frag_filling_profile_full_name_page_add_photo);
        Intrinsics.a((Object) constraintLayout, "frag_filling_profile_full_name_page_add_photo");
        Observable<R> c4 = j.c.a.a.c.n.c.a((View) constraintLayout).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c4, "RxView.clicks(this).map(AnyToUnit)");
        ImageView imageView = (ImageView) c(e.a.b.b.frag_filling_profile_full_name_page_iv_photo);
        Intrinsics.a((Object) imageView, "frag_filling_profile_full_name_page_iv_photo");
        Observable<R> c5 = j.c.a.a.c.n.c.a((View) imageView).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c5, "RxView.clicks(this).map(AnyToUnit)");
        TextInputEditText textInputEditText3 = (TextInputEditText) c(e.a.b.b.frag_filling_profile_full_name_page_ed_surname);
        TextInputEditText textInputEditText4 = (TextInputEditText) c(e.a.b.b.frag_filling_profile_full_name_page_ed_name);
        TextInputEditText textInputEditText5 = (TextInputEditText) c(e.a.b.b.frag_filling_profile_full_name_page_ed_patronymic);
        TextInputEditText textInputEditText6 = (TextInputEditText) c(e.a.b.b.frag_filling_profile_full_name_page_ed_registration_address);
        TextInputEditText textInputEditText7 = (TextInputEditText) c(e.a.b.b.frag_filling_profile_full_name_page_ed_living_place);
        AppCompatCheckBox appCompatCheckBox = (AppCompatCheckBox) c(e.a.b.b.frag_filling_profile_full_name_page_cb_matches_fact_address);
        Intrinsics.a((Object) appCompatCheckBox, "frag_filling_profile_ful…e_cb_matches_fact_address");
        InitialValueObservable<Boolean> a2 = j.c.a.a.c.n.c.a((CompoundButton) appCompatCheckBox);
        Intrinsics.a((Object) a2, "RxCompoundButton.checkedChanges(this)");
        return _Arrays.a((Collection) g2, (Iterable) Collections.a((Object[]) new Observable[]{c2.c((Function) c.a), c3.c((Function) d.a), c4.c((Function) a.b), c5.c((Function) a.c), new InitialValueObservable.a().c((Function) e.a), new InitialValueObservable.a().c((Function) f.a), new InitialValueObservable.a().c((Function) g.a), new InitialValueObservable.a().c((Function) h.a), new InitialValueObservable.a().c((Function) i.a), new InitialValueObservable.a().c((Function) b.a)}));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        if (layoutInflater != null) {
            return layoutInflater.inflate((int) R.layout.frag_filling_profile_full_name_page, viewGroup, false);
        }
        Intrinsics.a("inflater");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [com.google.android.material.textfield.TextInputEditText, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Calendar, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Locale, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [q.b.a.ZoneId, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [q.b.a.t.DateTimeFormatter, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.ZoneId, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public void a(View view, Bundle bundle) {
        Date date;
        String birthDate;
        long j2;
        ZoneOffsetTransition a2;
        if (view != null) {
            FillingProfileFullNamePageFragmentVm fillingProfileFullNamePageFragmentVm = (FillingProfileFullNamePageFragmentVm) h();
            String a3 = a((int) R.string.frag_filling_profile_gender_hint);
            Intrinsics.a((Object) a3, "getString(R.string.frag_…ling_profile_gender_hint)");
            String a4 = a((int) R.string.common_gender_male);
            Intrinsics.a((Object) a4, "getString(R.string.common_gender_male)");
            String a5 = a((int) R.string.common_gender_female);
            Intrinsics.a((Object) a5, "getString(R.string.common_gender_female)");
            String a6 = a((int) R.string.frag_filling_profile_citizenship_hint);
            Intrinsics.a((Object) a6, "getString(R.string.frag_…profile_citizenship_hint)");
            fillingProfileFullNamePageFragmentVm.d.a(FillingProfileFullNamePageFragmentViewState.a((FillingProfileFullNamePageFragmentViewState) fillingProfileFullNamePageFragmentVm.e(), new ProfileFillingBottomSheetDto(a3, a4, a5, a6), null, null, null, 14));
            TextInputEditText textInputEditText = (TextInputEditText) c(e.a.b.b.frag_filling_profile_full_name_page_ed_birthday);
            Intrinsics.a((Object) textInputEditText, "frag_filling_profile_full_name_page_ed_birthday");
            Calendar instance = Calendar.getInstance();
            Intrinsics.a((Object) instance, "Calendar.getInstance()");
            Long valueOf = Long.valueOf(instance.getTimeInMillis());
            FillingProfileFullNamePageFragmentVm fillingProfileFullNamePageFragmentVm2 = (FillingProfileFullNamePageFragmentVm) h();
            PersonalResponse a7 = fillingProfileFullNamePageFragmentVm2.f705p.a(fillingProfileFullNamePageFragmentVm2.f706q.e().b);
            if (!(a7 == null || (birthDate = a7.getBirthDate()) == null)) {
                Locale locale = Locale.getDefault();
                Intrinsics.a((Object) locale, "Locale.getDefault()");
                ZoneId i2 = ZoneId.i();
                Intrinsics.a((Object) i2, "ZoneId.systemDefault()");
                try {
                    DateTimeFormatter a8 = DateTimeFormatter.a("dd.MM.yyyy", locale);
                    Intrinsics.a((Object) a8, "DateTimeFormatter.ofPattern(pattern, locale)");
                    LocalDate a9 = LocalDate.a(birthDate, a8);
                    if (a9 != null) {
                        Collections.a((Object) i2, "zone");
                        LocalDateTime b2 = LocalDateTime.b(a9, LocalTime.h);
                        if (!(i2 instanceof ZoneOffset) && (a2 = i2.g().a(b2)) != null && a2.h()) {
                            b2 = a2.f();
                        }
                        ZonedDateTime a10 = ZonedDateTime.a(b2, i2);
                        Instant b3 = Instant.b(a10.l(), (long) a10.q().f3094e);
                        long j3 = b3.b;
                        if (j3 >= 0) {
                            j2 = Collections.d(Collections.e(j3, 1000), (long) (b3.c / 1000000));
                        } else {
                            j2 = Collections.f(Collections.e(j3 + 1, 1000), 1000 - ((long) (b3.c / 1000000)));
                        }
                        date = new Date(j2);
                        Collections.a(textInputEditText, valueOf, null, date, new j(this), 2);
                        return;
                    }
                    throw null;
                } catch (ArithmeticException e2) {
                    throw new IllegalArgumentException(e2);
                } catch (Exception unused) {
                }
            }
            date = null;
            Collections.a(textInputEditText, valueOf, null, date, new j(this), 2);
            return;
        }
        Intrinsics.a("view");
        throw null;
    }

    public void a(BaseViewState2 baseViewState2) {
        if (baseViewState2 == null) {
            Intrinsics.a("ve");
            throw null;
        } else if (baseViewState2 instanceof FillingProfileFullNamePageFragmentViewState3) {
            ((TextInputEditText) c(e.a.b.b.frag_filling_profile_full_name_page_ed_living_place)).setText(((FillingProfileFullNamePageFragmentViewState3) baseViewState2).a);
        } else {
            super.a(baseViewState2);
        }
    }
}
