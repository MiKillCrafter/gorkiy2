package ru.covid19.droid.presentation.main.profileFilling.transportHealthStep;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.minsvyaz.gosuslugi.stopcorona.R;
import e.a.a.i.CoreComponentsHolder;
import e.a.a.i.b.CoreComponent;
import e.a.b.f.a.DaggerMainComponent;
import e.a.b.f.a.MainComponent;
import e.a.b.f.b.AppComponentsHolder;
import e.a.b.f.c.AddProfileStepsNavigationModule;
import e.a.b.f.c.MainModule;
import e.a.b.h.b.h.o.ChildrenAdapter0;
import e.a.b.h.b.h.o.TransportHealthStepViewState;
import e.a.b.h.b.h.o.TransportHealthStepViewState0;
import e.a.b.h.b.h.o.TransportHealthStepViewState1;
import e.a.b.h.b.h.o.TransportHealthStepViewState2;
import e.a.b.h.b.h.o.TransportHealthStepViewState3;
import e.a.b.h.b.h.o.TransportHealthStepViewState4;
import e.a.b.h.b.h.o.TransportHealthStepVm;
import e.a.b.h.b.h.o.e;
import e.c.c.BaseMviView1;
import e.c.c.ViewExtensions;
import e.c.c.ViewExtensions0;
import j.e.a.InitialValueObservable;
import j.e.a.b.AnyToUnit;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import kotlin.NoWhenBranchMatchedException;
import l.b.Observable;
import l.b.s.CompositeDisposable;
import l.b.s.Disposable;
import l.b.t.Function;
import n.Unit;
import n.i.Collections;
import n.i._Arrays;
import n.n.b.Functions0;
import n.n.c.Intrinsics;
import n.n.c.j;
import ru.covid19.droid.data.model.profileData.Children;
import ru.covid19.droid.data.model.profileData.ProfileData;
import ru.covid19.droid.data.model.profileData.Transport;
import ru.covid19.droid.presentation.main.profileFilling.BaseStepValidatedFragment;

/* compiled from: TransportHealthStepFragment.kt */
public final class TransportHealthStepFragment extends BaseStepValidatedFragment<e.a.b.h.b.h.o.f, e.a.b.h.b.h.o.g> {
    public ChildrenAdapter0 d0;
    public HashMap e0;

    /* compiled from: TransportHealthStepFragment.kt */
    public static final class a extends j implements Functions0<Children, n.g> {
        public final /* synthetic */ TransportHealthStepFragment c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(TransportHealthStepFragment transportHealthStepFragment) {
            super(1);
            this.c = transportHealthStepFragment;
        }

        public Object a(Object obj) {
            Children children = (Children) obj;
            if (children != null) {
                this.c.c0.a((e.c.c.b) new TransportHealthStepViewState4.c(children));
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: TransportHealthStepFragment.kt */
    public static final class b<T, R> implements Function<T, R> {
        public final /* synthetic */ TransportHealthStepFragment a;

        public b(TransportHealthStepFragment transportHealthStepFragment) {
            this.a = transportHealthStepFragment;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
         arg types: [java.lang.Boolean, boolean]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
        public Object a(Object obj) {
            ProfileData profileData;
            ProfileData profileData2;
            Boolean bool = null;
            if (((Unit) obj) != null) {
                String a2 = this.a.a((int) R.string.frag_filling_profile_symptoms);
                Intrinsics.a((Object) a2, "getString(R.string.frag_filling_profile_symptoms)");
                TransportHealthStepViewState[] transportHealthStepViewStateArr = new TransportHealthStepViewState[2];
                String a3 = this.a.a((int) R.string.frag_filling_profile_radio_button_yes);
                Intrinsics.a((Object) a3, "getString(R.string.frag_…profile_radio_button_yes)");
                TransportHealthStepViewState1 transportHealthStepViewState1 = (TransportHealthStepViewState1) ((TransportHealthStepVm) this.a.h()).d.c();
                transportHealthStepViewStateArr[0] = new TransportHealthStepViewState(true, a3, Intrinsics.a((Object) ((transportHealthStepViewState1 == null || (profileData2 = transportHealthStepViewState1.a) == null) ? null : profileData2.getSympthoms()), (Object) true));
                String a4 = this.a.a((int) R.string.frag_filling_profile_radio_button_no);
                Intrinsics.a((Object) a4, "getString(R.string.frag_…_profile_radio_button_no)");
                TransportHealthStepViewState1 transportHealthStepViewState12 = (TransportHealthStepViewState1) ((TransportHealthStepVm) this.a.h()).d.c();
                if (!(transportHealthStepViewState12 == null || (profileData = transportHealthStepViewState12.a) == null)) {
                    bool = profileData.getSympthoms();
                }
                transportHealthStepViewStateArr[1] = new TransportHealthStepViewState(false, a4, Intrinsics.a((Object) bool, (Object) false));
                return new TransportHealthStepViewState4.g(a2, Collections.a((Object[]) transportHealthStepViewStateArr));
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: TransportHealthStepFragment.kt */
    public static final class c<T, R> implements Function<T, R> {
        public final /* synthetic */ TransportHealthStepFragment a;

        public c(TransportHealthStepFragment transportHealthStepFragment) {
            this.a = transportHealthStepFragment;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        public Object a(Object obj) {
            ProfileData profileData;
            Transport transport;
            TransportHealthStepViewState2 transportHealthStepViewState2 = null;
            if (((Unit) obj) != null) {
                TransportHealthStepViewState1 transportHealthStepViewState1 = (TransportHealthStepViewState1) ((TransportHealthStepVm) this.a.h()).d.c();
                if (!(transportHealthStepViewState1 == null || (profileData = transportHealthStepViewState1.a) == null || (transport = profileData.getTransport()) == null)) {
                    transportHealthStepViewState2 = transport.getType();
                }
                String a2 = this.a.a((int) R.string.frag_filling_profile_transport_type);
                Intrinsics.a((Object) a2, "getString(R.string.frag_…g_profile_transport_type)");
                TransportHealthStepViewState3[] transportHealthStepViewState3Arr = new TransportHealthStepViewState3[4];
                TransportHealthStepViewState2 transportHealthStepViewState22 = TransportHealthStepViewState2.plane;
                String a3 = this.a.a((int) R.string.frag_filling_profile_transport_type_avia);
                Intrinsics.a((Object) a3, "getString(R.string.frag_…file_transport_type_avia)");
                boolean z = true;
                transportHealthStepViewState3Arr[0] = new TransportHealthStepViewState3(transportHealthStepViewState22, a3, transportHealthStepViewState2 == TransportHealthStepViewState2.plane);
                TransportHealthStepViewState2 transportHealthStepViewState23 = TransportHealthStepViewState2.train;
                String a4 = this.a.a((int) R.string.frag_filling_profile_transport_type_rail);
                Intrinsics.a((Object) a4, "getString(R.string.frag_…file_transport_type_rail)");
                transportHealthStepViewState3Arr[1] = new TransportHealthStepViewState3(transportHealthStepViewState23, a4, transportHealthStepViewState2 == TransportHealthStepViewState2.train);
                TransportHealthStepViewState2 transportHealthStepViewState24 = TransportHealthStepViewState2.car;
                String a5 = this.a.a((int) R.string.frag_filling_profile_transport_type_car);
                Intrinsics.a((Object) a5, "getString(R.string.frag_…ofile_transport_type_car)");
                transportHealthStepViewState3Arr[2] = new TransportHealthStepViewState3(transportHealthStepViewState24, a5, transportHealthStepViewState2 == TransportHealthStepViewState2.car);
                TransportHealthStepViewState2 transportHealthStepViewState25 = TransportHealthStepViewState2.ship;
                String a6 = this.a.a((int) R.string.frag_filling_profile_transport_type_river);
                Intrinsics.a((Object) a6, "getString(R.string.frag_…ile_transport_type_river)");
                if (transportHealthStepViewState2 != TransportHealthStepViewState2.ship) {
                    z = false;
                }
                transportHealthStepViewState3Arr[3] = new TransportHealthStepViewState3(transportHealthStepViewState25, a6, z);
                return new TransportHealthStepViewState4.h(new TransportHealthStepViewState0(a2, Collections.a((Object[]) transportHealthStepViewState3Arr)));
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: TransportHealthStepFragment.kt */
    public static final class d extends j implements Functions0<String, e.b> {
        public static final d c = new d();

        public d() {
            super(1);
        }

        public Object a(Object obj) {
            String str = (String) obj;
            if (str != null) {
                return new TransportHealthStepViewState4.b(str);
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: TransportHealthStepFragment.kt */
    public static final class e<T, R> implements Function<T, R> {
        public static final e a = new e();

        public Object a(Object obj) {
            if (((Unit) obj) != null) {
                return TransportHealthStepViewState4.a.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: TransportHealthStepFragment.kt */
    public static final class f<T, R> implements Function<T, R> {
        public static final f a = new f();

        public Object a(Object obj) {
            Boolean bool = (Boolean) obj;
            if (bool != null) {
                return new TransportHealthStepViewState4.d(bool.booleanValue());
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: TransportHealthStepFragment.kt */
    public static final class g<T, R> implements Function<T, R> {
        public static final g a = new g();

        public Object a(Object obj) {
            CharSequence charSequence = (CharSequence) obj;
            if (charSequence != null) {
                return new TransportHealthStepViewState4.e(charSequence.toString());
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    public /* synthetic */ void C() {
        super.C();
        N();
    }

    public void N() {
        HashMap hashMap = this.e0;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public void O() {
        Collections.a((BaseMviView1) this);
        CompositeDisposable compositeDisposable = this.Z;
        Disposable[] disposableArr = new Disposable[1];
        ChildrenAdapter0 childrenAdapter0 = this.d0;
        if (childrenAdapter0 != null) {
            disposableArr[0] = childrenAdapter0.d.a(new a(this));
            compositeDisposable.a(disposableArr);
            return;
        }
        Intrinsics.b("childrenAdapter");
        throw null;
    }

    public Class<e.a.b.h.b.h.o.g> P() {
        return TransportHealthStepVm.class;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.Boolean, boolean]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [com.google.android.material.textfield.TextInputLayout, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void a(Object obj) {
        String str;
        TransportHealthStepViewState1 transportHealthStepViewState1 = (TransportHealthStepViewState1) obj;
        if (transportHealthStepViewState1 != null) {
            ChildrenAdapter0 childrenAdapter0 = this.d0;
            if (childrenAdapter0 != null) {
                List<Children> children = transportHealthStepViewState1.a.getChildren();
                if (children != null) {
                    childrenAdapter0.c = children;
                    ChildrenAdapter0 childrenAdapter02 = this.d0;
                    if (childrenAdapter02 != null) {
                        childrenAdapter02.a.b();
                        TextView textView = (TextView) c(e.a.b.b.frag_filling_profile_transport_tv_children_step_title);
                        Intrinsics.a((Object) textView, "frag_filling_profile_tra…rt_tv_children_step_title");
                        textView.setVisibility(transportHealthStepViewState1.a.getChildren().isEmpty() ^ true ? 0 : 8);
                        boolean a2 = Intrinsics.a((Object) transportHealthStepViewState1.a.getPassenger().getQuarantineAddressMatchesPlacement(), (Object) true);
                        TextInputLayout textInputLayout = (TextInputLayout) c(e.a.b.b.frag_filling_profile_transport_til_quarantine_address);
                        Intrinsics.a((Object) textInputLayout, "frag_filling_profile_tra…rt_til_quarantine_address");
                        textInputLayout.setEnabled(!a2);
                        ((TextInputEditText) c(e.a.b.b.frag_filling_profile_transport_et_quarantine_address)).setText(transportHealthStepViewState1.a.getPassenger().getQuarantineAddress());
                        TransportHealthStepViewState2 type = transportHealthStepViewState1.a.getTransport().getType();
                        if (type != null) {
                            int ordinal = type.ordinal();
                            if (ordinal == 0) {
                                ((TextInputEditText) c(e.a.b.b.frag_filling_profile_transport_et_transport_type)).setText((int) R.string.frag_filling_profile_transport_type_avia);
                                TextInputLayout textInputLayout2 = (TextInputLayout) c(e.a.b.b.frag_filling_profile_transport_til_addition_transport_info);
                                Intrinsics.a((Object) textInputLayout2, "frag_filling_profile_tra…l_addition_transport_info");
                                textInputLayout2.setHint(v().getString(R.string.frag_filling_profile_transport_trip_number));
                                TextInputLayout textInputLayout3 = (TextInputLayout) c(e.a.b.b.frag_filling_profile_transport_til_addition_transport_info);
                                Intrinsics.a((Object) textInputLayout3, "frag_filling_profile_tra…l_addition_transport_info");
                                textInputLayout3.setVisibility(0);
                            } else if (ordinal == 1) {
                                ((TextInputEditText) c(e.a.b.b.frag_filling_profile_transport_et_transport_type)).setText((int) R.string.frag_filling_profile_transport_type_rail);
                                TextInputLayout textInputLayout4 = (TextInputLayout) c(e.a.b.b.frag_filling_profile_transport_til_addition_transport_info);
                                Intrinsics.a((Object) textInputLayout4, "frag_filling_profile_tra…l_addition_transport_info");
                                textInputLayout4.setHint(v().getString(R.string.frag_filling_profile_transport_train_number));
                                TextInputLayout textInputLayout5 = (TextInputLayout) c(e.a.b.b.frag_filling_profile_transport_til_addition_transport_info);
                                Intrinsics.a((Object) textInputLayout5, "frag_filling_profile_tra…l_addition_transport_info");
                                textInputLayout5.setVisibility(0);
                            } else if (ordinal == 2) {
                                ((TextInputEditText) c(e.a.b.b.frag_filling_profile_transport_et_transport_type)).setText((int) R.string.frag_filling_profile_transport_type_car);
                                TextInputLayout textInputLayout6 = (TextInputLayout) c(e.a.b.b.frag_filling_profile_transport_til_addition_transport_info);
                                Intrinsics.a((Object) textInputLayout6, "frag_filling_profile_tra…l_addition_transport_info");
                                textInputLayout6.setHint(v().getString(R.string.frag_filling_profile_transport_car_number));
                                TextInputLayout textInputLayout7 = (TextInputLayout) c(e.a.b.b.frag_filling_profile_transport_til_addition_transport_info);
                                Intrinsics.a((Object) textInputLayout7, "frag_filling_profile_tra…l_addition_transport_info");
                                textInputLayout7.setVisibility(0);
                            } else if (ordinal == 3) {
                                ((TextInputEditText) c(e.a.b.b.frag_filling_profile_transport_et_transport_type)).setText((int) R.string.frag_filling_profile_transport_type_river);
                                TextInputLayout textInputLayout8 = (TextInputLayout) c(e.a.b.b.frag_filling_profile_transport_til_addition_transport_info);
                                Intrinsics.a((Object) textInputLayout8, "frag_filling_profile_tra…l_addition_transport_info");
                                textInputLayout8.setHint(v().getString(R.string.frag_filling_profile_transport_ship_name));
                                TextInputLayout textInputLayout9 = (TextInputLayout) c(e.a.b.b.frag_filling_profile_transport_til_addition_transport_info);
                                Intrinsics.a((Object) textInputLayout9, "frag_filling_profile_tra…l_addition_transport_info");
                                textInputLayout9.setVisibility(0);
                            }
                        }
                        ((TextInputEditText) c(e.a.b.b.frag_filling_profile_transport_et_addition_transport_info)).setText(transportHealthStepViewState1.a.getTransport().getName());
                        TextInputEditText textInputEditText = (TextInputEditText) c(e.a.b.b.frag_filling_profile_transport_et_symptoms_step_answer);
                        Boolean sympthoms = transportHealthStepViewState1.a.getSympthoms();
                        if (Intrinsics.a((Object) sympthoms, (Object) true)) {
                            str = a((int) R.string.frag_filling_profile_radio_button_yes);
                        } else if (Intrinsics.a((Object) sympthoms, (Object) false)) {
                            str = a((int) R.string.frag_filling_profile_radio_button_no);
                        } else if (sympthoms == null) {
                            str = "";
                        } else {
                            throw new NoWhenBranchMatchedException();
                        }
                        textInputEditText.setText(str);
                        return;
                    }
                    Intrinsics.b("childrenAdapter");
                    throw null;
                }
                Intrinsics.a("<set-?>");
                throw null;
            }
            Intrinsics.b("childrenAdapter");
            throw null;
        }
        Intrinsics.a("vs");
        throw null;
    }

    public void b(Bundle bundle) {
        if (AppComponentsHolder.b == null) {
            CoreComponent coreComponent = CoreComponentsHolder.b;
            if (coreComponent == null) {
                Intrinsics.b("coreComponent");
                throw null;
            } else if (coreComponent != null) {
                MainModule mainModule = new MainModule();
                AddProfileStepsNavigationModule addProfileStepsNavigationModule = new AddProfileStepsNavigationModule();
                j.c.a.a.c.n.c.a(coreComponent, CoreComponent.class);
                AppComponentsHolder.b = new DaggerMainComponent(mainModule, addProfileStepsNavigationModule, coreComponent, null);
            } else {
                throw null;
            }
        }
        MainComponent mainComponent = AppComponentsHolder.b;
        if (mainComponent != null) {
            mainComponent.a(this.a0);
            super.b(bundle);
            return;
        }
        Intrinsics.a();
        throw null;
    }

    public View c(int i2) {
        if (this.e0 == null) {
            this.e0 = new HashMap();
        }
        View view = (View) this.e0.get(Integer.valueOf(i2));
        if (view != null) {
            return view;
        }
        View view2 = this.H;
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i2);
        this.e0.put(Integer.valueOf(i2), findViewById);
        return findViewById;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [com.google.android.material.textfield.TextInputEditText, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<R>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.FrameLayout, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [androidx.appcompat.widget.AppCompatCheckBox, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [j.e.a.InitialValueObservable<java.lang.Boolean>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i._Arrays.a(java.util.Collection, java.lang.Iterable):java.util.List<T>
     arg types: [java.util.List<l.b.Observable<? extends e.c.c.b>>, java.util.List]
     candidates:
      n.i._Arrays.a(java.lang.Iterable, java.util.Collection):C
      n.i._Arrays.a(java.util.Collection, java.lang.Iterable):java.util.List<T> */
    public List<Observable<? extends e.c.c.b>> g() {
        List<Observable<? extends e.c.c.b>> g2 = super.g();
        TextInputEditText textInputEditText = (TextInputEditText) c(e.a.b.b.frag_filling_profile_transport_et_symptoms_step_answer);
        Intrinsics.a((Object) textInputEditText, "frag_filling_profile_tra…t_et_symptoms_step_answer");
        Observable<R> c2 = j.c.a.a.c.n.c.a((View) textInputEditText).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c2, "RxView.clicks(this).map(AnyToUnit)");
        TextInputEditText textInputEditText2 = (TextInputEditText) c(e.a.b.b.frag_filling_profile_transport_et_transport_type);
        Intrinsics.a((Object) textInputEditText2, "frag_filling_profile_transport_et_transport_type");
        Observable<R> c3 = j.c.a.a.c.n.c.a((View) textInputEditText2).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c3, "RxView.clicks(this).map(AnyToUnit)");
        TextInputEditText textInputEditText3 = (TextInputEditText) c(e.a.b.b.frag_filling_profile_transport_et_addition_transport_info);
        Intrinsics.a((Object) textInputEditText3, "frag_filling_profile_tra…t_addition_transport_info");
        Observable c4 = new InitialValueObservable.a().c((Function) ViewExtensions.a).c((Function) new ViewExtensions0(d.c));
        Intrinsics.a((Object) c4, "RxTextView.textChanges(t… .map { eventAction(it) }");
        FrameLayout frameLayout = (FrameLayout) c(e.a.b.b.frag_filling_profile_transport_add_child);
        Intrinsics.a((Object) frameLayout, "frag_filling_profile_transport_add_child");
        Observable<R> c5 = j.c.a.a.c.n.c.a((View) frameLayout).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c5, "RxView.clicks(this).map(AnyToUnit)");
        AppCompatCheckBox appCompatCheckBox = (AppCompatCheckBox) c(e.a.b.b.frag_filling_profile_transport_cb_matches_fact_address);
        Intrinsics.a((Object) appCompatCheckBox, "frag_filling_profile_tra…t_cb_matches_fact_address");
        InitialValueObservable<Boolean> a2 = j.c.a.a.c.n.c.a((CompoundButton) appCompatCheckBox);
        Intrinsics.a((Object) a2, "RxCompoundButton.checkedChanges(this)");
        TextInputEditText textInputEditText4 = (TextInputEditText) c(e.a.b.b.frag_filling_profile_transport_et_quarantine_address);
        return _Arrays.a((Collection) g2, (Iterable) Collections.a((Object[]) new Observable[]{c2.c((Function) new b(this)), c3.c((Function) new c(this)), c4, c5.c((Function) e.a), new InitialValueObservable.a().c((Function) f.a), new InitialValueObservable.a().c((Function) g.a)}));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        if (layoutInflater != null) {
            return layoutInflater.inflate((int) R.layout.frag_filling_profile_transport, viewGroup, false);
        }
        Intrinsics.a("inflater");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [androidx.recyclerview.widget.RecyclerView, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void a(View view, Bundle bundle) {
        if (view != null) {
            this.d0 = new ChildrenAdapter0();
            RecyclerView recyclerView = (RecyclerView) c(e.a.b.b.frag_filling_profile_transport_rv_children);
            Intrinsics.a((Object) recyclerView, "frag_filling_profile_transport_rv_children");
            ChildrenAdapter0 childrenAdapter0 = this.d0;
            if (childrenAdapter0 != null) {
                recyclerView.setAdapter(childrenAdapter0);
            } else {
                Intrinsics.b("childrenAdapter");
                throw null;
            }
        } else {
            Intrinsics.a("view");
            throw null;
        }
    }
}
