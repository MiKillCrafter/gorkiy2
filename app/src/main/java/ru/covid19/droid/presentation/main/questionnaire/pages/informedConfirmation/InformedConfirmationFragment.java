package ru.covid19.droid.presentation.main.questionnaire.pages.informedConfirmation;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.minsvyaz.gosuslugi.stopcorona.R;
import e.a.a.i.CoreComponentsHolder;
import e.a.a.i.b.CoreComponent;
import e.a.b.f.a.DaggerMainComponent;
import e.a.b.f.a.MainComponent;
import e.a.b.f.b.AppComponentsHolder;
import e.a.b.f.c.AddProfileStepsNavigationModule;
import e.a.b.f.c.MainModule;
import e.a.b.h.b.i.c.m.InformedConfirmationFragmentViewState;
import e.a.b.h.b.i.c.m.InformedConfirmationFragmentViewState0;
import e.a.b.h.b.i.c.m.InformedConfirmationFragmentVm;
import j.e.a.InitialValueObservable;
import j.e.a.b.AnyToUnit;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import l.b.Observable;
import l.b.t.Function;
import n.Unit;
import n.i.Collections;
import n.i._Arrays;
import n.n.c.Intrinsics;
import ru.covid19.droid.presentation.main.profileFilling.BaseStepValidatedFragment;

/* compiled from: InformedConfirmationFragment.kt */
public final class InformedConfirmationFragment extends BaseStepValidatedFragment<e.a.b.h.b.i.c.m.b, e.a.b.h.b.i.c.m.c> {
    public HashMap d0;

    /* compiled from: InformedConfirmationFragment.kt */
    public static final class a<T, R> implements Function<T, R> {
        public static final a a = new a();

        public Object a(Object obj) {
            if (((Unit) obj) != null) {
                return InformedConfirmationFragmentViewState0.e.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: InformedConfirmationFragment.kt */
    public static final class b<T, R> implements Function<T, R> {
        public static final b a = new b();

        public Object a(Object obj) {
            if (((Unit) obj) != null) {
                return InformedConfirmationFragmentViewState0.d.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: InformedConfirmationFragment.kt */
    public static final class c<T, R> implements Function<T, R> {
        public static final c a = new c();

        public Object a(Object obj) {
            Boolean bool = (Boolean) obj;
            if (bool != null) {
                return new InformedConfirmationFragmentViewState0.c(bool.booleanValue());
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: InformedConfirmationFragment.kt */
    public static final class d<T, R> implements Function<T, R> {
        public static final d a = new d();

        public Object a(Object obj) {
            Boolean bool = (Boolean) obj;
            if (bool != null) {
                return new InformedConfirmationFragmentViewState0.b(bool.booleanValue());
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: InformedConfirmationFragment.kt */
    public static final class e<T, R> implements Function<T, R> {
        public static final e a = new e();

        public Object a(Object obj) {
            Boolean bool = (Boolean) obj;
            if (bool != null) {
                return new InformedConfirmationFragmentViewState0.a(bool.booleanValue());
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    public /* synthetic */ void C() {
        super.C();
        N();
    }

    public void N() {
        HashMap hashMap = this.d0;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public Class<e.a.b.h.b.i.c.m.c> P() {
        return InformedConfirmationFragmentVm.class;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [androidx.appcompat.widget.AppCompatCheckBox, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(android.widget.CompoundButton, boolean):void
     arg types: [androidx.appcompat.widget.AppCompatCheckBox, boolean]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(java.lang.Object, java.lang.String):T
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(android.widget.CompoundButton, boolean):void */
    public void a(Object obj) {
        InformedConfirmationFragmentViewState informedConfirmationFragmentViewState = (InformedConfirmationFragmentViewState) obj;
        if (informedConfirmationFragmentViewState != null) {
            AppCompatCheckBox appCompatCheckBox = (AppCompatCheckBox) c(e.a.b.b.frag_informed_confirmation_self_isolation_rules_cb);
            Intrinsics.a((Object) appCompatCheckBox, "frag_informed_confirmation_self_isolation_rules_cb");
            Collections.a((CompoundButton) appCompatCheckBox, informedConfirmationFragmentViewState.a);
            AppCompatCheckBox appCompatCheckBox2 = (AppCompatCheckBox) c(e.a.b.b.frag_informed_confirmation_self_quarantine_termination_responsibility_cb);
            Intrinsics.a((Object) appCompatCheckBox2, "frag_informed_confirmati…ination_responsibility_cb");
            Collections.a((CompoundButton) appCompatCheckBox2, informedConfirmationFragmentViewState.b);
            AppCompatCheckBox appCompatCheckBox3 = (AppCompatCheckBox) c(e.a.b.b.frag_informed_confirmation_all_info_is_correct_cb);
            Intrinsics.a((Object) appCompatCheckBox3, "frag_informed_confirmation_all_info_is_correct_cb");
            Collections.a((CompoundButton) appCompatCheckBox3, informedConfirmationFragmentViewState.c);
            return;
        }
        Intrinsics.a("vs");
        throw null;
    }

    public void b(Bundle bundle) {
        if (AppComponentsHolder.b == null) {
            CoreComponent coreComponent = CoreComponentsHolder.b;
            if (coreComponent == null) {
                Intrinsics.b("coreComponent");
                throw null;
            } else if (coreComponent != null) {
                MainModule mainModule = new MainModule();
                AddProfileStepsNavigationModule addProfileStepsNavigationModule = new AddProfileStepsNavigationModule();
                j.c.a.a.c.n.c.a(coreComponent, CoreComponent.class);
                AppComponentsHolder.b = new DaggerMainComponent(mainModule, addProfileStepsNavigationModule, coreComponent, null);
            } else {
                throw null;
            }
        }
        MainComponent mainComponent = AppComponentsHolder.b;
        if (mainComponent != null) {
            mainComponent.a(this.a0);
            super.b(bundle);
            return;
        }
        Intrinsics.a();
        throw null;
    }

    public View c(int i2) {
        if (this.d0 == null) {
            this.d0 = new HashMap();
        }
        View view = (View) this.d0.get(Integer.valueOf(i2));
        if (view != null) {
            return view;
        }
        View view2 = this.H;
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i2);
        this.d0.put(Integer.valueOf(i2), findViewById);
        return findViewById;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [androidx.constraintlayout.widget.ConstraintLayout, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<R>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [androidx.appcompat.widget.AppCompatCheckBox, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [j.e.a.InitialValueObservable<java.lang.Boolean>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i._Arrays.a(java.util.Collection, java.lang.Iterable):java.util.List<T>
     arg types: [java.util.List<l.b.Observable<? extends e.c.c.b>>, java.util.List]
     candidates:
      n.i._Arrays.a(java.lang.Iterable, java.util.Collection):C
      n.i._Arrays.a(java.util.Collection, java.lang.Iterable):java.util.List<T> */
    public List<Observable<? extends e.c.c.b>> g() {
        List<Observable<? extends e.c.c.b>> g = super.g();
        ConstraintLayout constraintLayout = (ConstraintLayout) c(e.a.b.b.frag_informed_confirmation_self_isolation_rules_container);
        Intrinsics.a((Object) constraintLayout, "frag_informed_confirmati…isolation_rules_container");
        Observable<R> c2 = j.c.a.a.c.n.c.a((View) constraintLayout).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c2, "RxView.clicks(this).map(AnyToUnit)");
        ConstraintLayout constraintLayout2 = (ConstraintLayout) c(e.a.b.b.frag_informed_confirmation_self_quarantine_termination_responsibility_container);
        Intrinsics.a((Object) constraintLayout2, "frag_informed_confirmati…_responsibility_container");
        Observable<R> c3 = j.c.a.a.c.n.c.a((View) constraintLayout2).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c3, "RxView.clicks(this).map(AnyToUnit)");
        AppCompatCheckBox appCompatCheckBox = (AppCompatCheckBox) c(e.a.b.b.frag_informed_confirmation_self_isolation_rules_cb);
        Intrinsics.a((Object) appCompatCheckBox, "frag_informed_confirmation_self_isolation_rules_cb");
        InitialValueObservable<Boolean> a2 = j.c.a.a.c.n.c.a((CompoundButton) appCompatCheckBox);
        Intrinsics.a((Object) a2, "RxCompoundButton.checkedChanges(this)");
        AppCompatCheckBox appCompatCheckBox2 = (AppCompatCheckBox) c(e.a.b.b.frag_informed_confirmation_self_quarantine_termination_responsibility_cb);
        Intrinsics.a((Object) appCompatCheckBox2, "frag_informed_confirmati…ination_responsibility_cb");
        InitialValueObservable<Boolean> a3 = j.c.a.a.c.n.c.a((CompoundButton) appCompatCheckBox2);
        Intrinsics.a((Object) a3, "RxCompoundButton.checkedChanges(this)");
        AppCompatCheckBox appCompatCheckBox3 = (AppCompatCheckBox) c(e.a.b.b.frag_informed_confirmation_all_info_is_correct_cb);
        Intrinsics.a((Object) appCompatCheckBox3, "frag_informed_confirmation_all_info_is_correct_cb");
        InitialValueObservable<Boolean> a4 = j.c.a.a.c.n.c.a((CompoundButton) appCompatCheckBox3);
        Intrinsics.a((Object) a4, "RxCompoundButton.checkedChanges(this)");
        return _Arrays.a((Collection) g, (Iterable) Collections.a((Object[]) new Observable[]{c2.c((Function) a.a), c3.c((Function) b.a), a2.c((Function) c.a), a3.c((Function) d.a), a4.c((Function) e.a)}));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        if (layoutInflater != null) {
            return layoutInflater.inflate((int) R.layout.frag_informed_confirmation, viewGroup, false);
        }
        Intrinsics.a("inflater");
        throw null;
    }
}
