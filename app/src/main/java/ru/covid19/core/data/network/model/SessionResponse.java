package ru.covid19.core.data.network.model;

import com.crashlytics.android.answers.SessionEvent;
import j.a.a.a.outline;
import n.n.c.Intrinsics;

/* compiled from: SessionResponse.kt */
public final class SessionResponse extends BaseResponse {
    public final String accessToken;
    public final String sessionId;

    public SessionResponse(String str, String str2) {
        if (str == null) {
            Intrinsics.a(SessionEvent.SESSION_ID_KEY);
            throw null;
        } else if (str2 != null) {
            this.sessionId = str;
            this.accessToken = str2;
        } else {
            Intrinsics.a("accessToken");
            throw null;
        }
    }

    public static /* synthetic */ SessionResponse copy$default(SessionResponse sessionResponse, String str, String str2, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            str = sessionResponse.sessionId;
        }
        if ((i2 & 2) != 0) {
            str2 = sessionResponse.accessToken;
        }
        return sessionResponse.copy(str, str2);
    }

    public final String component1() {
        return this.sessionId;
    }

    public final String component2() {
        return this.accessToken;
    }

    public final SessionResponse copy(String str, String str2) {
        if (str == null) {
            Intrinsics.a(SessionEvent.SESSION_ID_KEY);
            throw null;
        } else if (str2 != null) {
            return new SessionResponse(str, str2);
        } else {
            Intrinsics.a("accessToken");
            throw null;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SessionResponse)) {
            return false;
        }
        SessionResponse sessionResponse = (SessionResponse) obj;
        return Intrinsics.a(this.sessionId, sessionResponse.sessionId) && Intrinsics.a(this.accessToken, sessionResponse.accessToken);
    }

    public final String getAccessToken() {
        return this.accessToken;
    }

    public final String getSessionId() {
        return this.sessionId;
    }

    public int hashCode() {
        String str = this.sessionId;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.accessToken;
        if (str2 != null) {
            i2 = str2.hashCode();
        }
        return hashCode + i2;
    }

    public String toString() {
        StringBuilder a = outline.a("SessionResponse(sessionId=");
        a.append(this.sessionId);
        a.append(", accessToken=");
        return outline.a(a, this.accessToken, ")");
    }
}
