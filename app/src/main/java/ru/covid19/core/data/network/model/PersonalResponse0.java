package ru.covid19.core.data.network.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.crashlytics.android.answers.SessionEventTransform;
import com.crashlytics.android.core.CodedOutputStream;
import com.crashlytics.android.core.LogFileManager;
import j.a.a.a.outline;
import j.c.d.a0.SerializedName;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import n.n.c.Intrinsics;

/* compiled from: PersonalResponse.kt */
public final class PersonalResponse0 implements Parcelable {
    public static final Parcelable.Creator CREATOR = new Creator();
    public final String actNo;
    public final List<String> categories;
    public final String eTag;
    public final String expiryDate;
    public final String firstName;
    public final String fmsState;
    public final Boolean fmsValid;
    public final Integer id;
    public final String issueDate;
    public final String issueId;
    public final String issuedBy;
    public final String lastName;
    public final String latinFirstName;
    public final String latinLastName;
    public final String number;
    public final String parentCount;
    @SerializedName("record_date")
    public final String recordDate;
    public final String series;
    public final List<String> stateFacts;
    public final PersonalResponse2 type;
    public final Map<String, String> verifyingValue;
    public final Integer vrfReqId;
    public final PersonalResponse1 vrfStu;
    public final String vrfValStu;

    public static class Creator implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            Boolean bool;
            String str;
            String str2;
            PersonalResponse2 personalResponse2;
            LinkedHashMap linkedHashMap;
            if (parcel != null) {
                String readString = parcel.readString();
                ArrayList<String> createStringArrayList = parcel.createStringArrayList();
                String readString2 = parcel.readString();
                String readString3 = parcel.readString();
                String readString4 = parcel.readString();
                String readString5 = parcel.readString();
                String readString6 = parcel.readString();
                if (parcel.readInt() != 0) {
                    bool = Boolean.valueOf(parcel.readInt() != 0);
                } else {
                    bool = null;
                }
                Integer valueOf = parcel.readInt() != 0 ? Integer.valueOf(parcel.readInt()) : null;
                String readString7 = parcel.readString();
                String readString8 = parcel.readString();
                String readString9 = parcel.readString();
                String readString10 = parcel.readString();
                String readString11 = parcel.readString();
                String readString12 = parcel.readString();
                String readString13 = parcel.readString();
                String readString14 = parcel.readString();
                String readString15 = parcel.readString();
                ArrayList<String> createStringArrayList2 = parcel.createStringArrayList();
                PersonalResponse2 personalResponse22 = (PersonalResponse2) Enum.valueOf(PersonalResponse2.class, parcel.readString());
                if (parcel.readInt() != 0) {
                    int readInt = parcel.readInt();
                    personalResponse2 = personalResponse22;
                    linkedHashMap = new LinkedHashMap(readInt);
                    while (readInt != 0) {
                        linkedHashMap.put(parcel.readString(), parcel.readString());
                        readInt--;
                        readString11 = readString11;
                        readString10 = readString10;
                    }
                    str = readString10;
                    str2 = readString11;
                } else {
                    personalResponse2 = personalResponse22;
                    str = readString10;
                    str2 = readString11;
                    linkedHashMap = null;
                }
                return new PersonalResponse0(readString, createStringArrayList, readString2, readString3, readString4, readString5, readString6, bool, valueOf, readString7, readString8, readString9, str, str2, readString12, readString13, readString14, readString15, createStringArrayList2, personalResponse2, linkedHashMap, parcel.readInt() != 0 ? Integer.valueOf(parcel.readInt()) : null, parcel.readInt() != 0 ? (PersonalResponse1) Enum.valueOf(PersonalResponse1.class, parcel.readString()) : null, parcel.readString());
            }
            Intrinsics.a("in");
            throw null;
        }

        public final Object[] newArray(int i2) {
            return new PersonalResponse0[i2];
        }
    }

    public PersonalResponse0(String str, List<String> list, String str2, String str3, String str4, String str5, String str6, Boolean bool, Integer num, String str7, String str8, String str9, String str10, String str11, String str12, String str13, String str14, String str15, List<String> list2, DocumentType documentType, Map<String, String> map, Integer num2, DocumentStatus documentStatus, String str16) {
        DocumentType documentType2 = documentType;
        if (documentType2 != null) {
            this.actNo = str;
            this.categories = list;
            this.recordDate = str2;
            this.eTag = str3;
            this.expiryDate = str4;
            this.firstName = str5;
            this.fmsState = str6;
            this.fmsValid = bool;
            this.id = num;
            this.issueDate = str7;
            this.issueId = str8;
            this.issuedBy = str9;
            this.lastName = str10;
            this.latinFirstName = str11;
            this.latinLastName = str12;
            this.number = str13;
            this.parentCount = str14;
            this.series = str15;
            this.stateFacts = list2;
            this.type = documentType2;
            this.verifyingValue = map;
            this.vrfReqId = num2;
            this.vrfStu = documentStatus;
            this.vrfValStu = str16;
            return;
        }
        Intrinsics.a(SessionEventTransform.TYPE_KEY);
        throw null;
    }

    public static /* synthetic */ PersonalResponse0 copy$default(PersonalResponse0 personalResponse0, String str, List list, String str2, String str3, String str4, String str5, String str6, Boolean bool, Integer num, String str7, String str8, String str9, String str10, String str11, String str12, String str13, String str14, String str15, List list2, PersonalResponse2 personalResponse2, Map map, Integer num2, PersonalResponse1 personalResponse1, String str16, int i2, Object obj) {
        PersonalResponse0 personalResponse02 = personalResponse0;
        int i3 = i2;
        return personalResponse0.copy((i3 & 1) != 0 ? personalResponse02.actNo : str, (i3 & 2) != 0 ? personalResponse02.categories : list, (i3 & 4) != 0 ? personalResponse02.recordDate : str2, (i3 & 8) != 0 ? personalResponse02.eTag : str3, (i3 & 16) != 0 ? personalResponse02.expiryDate : str4, (i3 & 32) != 0 ? personalResponse02.firstName : str5, (i3 & 64) != 0 ? personalResponse02.fmsState : str6, (i3 & 128) != 0 ? personalResponse02.fmsValid : bool, (i3 & 256) != 0 ? personalResponse02.id : num, (i3 & 512) != 0 ? personalResponse02.issueDate : str7, (i3 & 1024) != 0 ? personalResponse02.issueId : str8, (i3 & 2048) != 0 ? personalResponse02.issuedBy : str9, (i3 & CodedOutputStream.DEFAULT_BUFFER_SIZE) != 0 ? personalResponse02.lastName : str10, (i3 & 8192) != 0 ? personalResponse02.latinFirstName : str11, (i3 & 16384) != 0 ? personalResponse02.latinLastName : str12, (i3 & 32768) != 0 ? personalResponse02.number : str13, (i3 & LogFileManager.MAX_LOG_SIZE) != 0 ? personalResponse02.parentCount : str14, (i3 & 131072) != 0 ? personalResponse02.series : str15, (i3 & 262144) != 0 ? personalResponse02.stateFacts : list2, (i3 & 524288) != 0 ? personalResponse02.type : personalResponse2, (i3 & 1048576) != 0 ? personalResponse02.verifyingValue : map, (i3 & 2097152) != 0 ? personalResponse02.vrfReqId : num2, (i3 & 4194304) != 0 ? personalResponse02.vrfStu : personalResponse1, (i3 & 8388608) != 0 ? personalResponse02.vrfValStu : str16);
    }

    public final String component1() {
        return this.actNo;
    }

    public final String component10() {
        return this.issueDate;
    }

    public final String component11() {
        return this.issueId;
    }

    public final String component12() {
        return this.issuedBy;
    }

    public final String component13() {
        return this.lastName;
    }

    public final String component14() {
        return this.latinFirstName;
    }

    public final String component15() {
        return this.latinLastName;
    }

    public final String component16() {
        return this.number;
    }

    public final String component17() {
        return this.parentCount;
    }

    public final String component18() {
        return this.series;
    }

    public final List<String> component19() {
        return this.stateFacts;
    }

    public final List<String> component2() {
        return this.categories;
    }

    public final PersonalResponse2 component20() {
        return this.type;
    }

    public final Map<String, String> component21() {
        return this.verifyingValue;
    }

    public final Integer component22() {
        return this.vrfReqId;
    }

    public final PersonalResponse1 component23() {
        return this.vrfStu;
    }

    public final String component24() {
        return this.vrfValStu;
    }

    public final String component3() {
        return this.recordDate;
    }

    public final String component4() {
        return this.eTag;
    }

    public final String component5() {
        return this.expiryDate;
    }

    public final String component6() {
        return this.firstName;
    }

    public final String component7() {
        return this.fmsState;
    }

    public final Boolean component8() {
        return this.fmsValid;
    }

    public final Integer component9() {
        return this.id;
    }

    public final Document copy(String str, List<String> list, String str2, String str3, String str4, String str5, String str6, Boolean bool, Integer num, String str7, String str8, String str9, String str10, String str11, String str12, String str13, String str14, String str15, List<String> list2, DocumentType documentType, Map<String, String> map, Integer num2, DocumentStatus documentStatus, String str16) {
        if (documentType != null) {
            return new PersonalResponse0(str, list, str2, str3, str4, str5, str6, bool, num, str7, str8, str9, str10, str11, str12, str13, str14, str15, list2, documentType, map, num2, documentStatus, str16);
        }
        Intrinsics.a(SessionEventTransform.TYPE_KEY);
        throw null;
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PersonalResponse0)) {
            return false;
        }
        PersonalResponse0 personalResponse0 = (PersonalResponse0) obj;
        return Intrinsics.a(this.actNo, personalResponse0.actNo) && Intrinsics.a(this.categories, personalResponse0.categories) && Intrinsics.a(this.recordDate, personalResponse0.recordDate) && Intrinsics.a(this.eTag, personalResponse0.eTag) && Intrinsics.a(this.expiryDate, personalResponse0.expiryDate) && Intrinsics.a(this.firstName, personalResponse0.firstName) && Intrinsics.a(this.fmsState, personalResponse0.fmsState) && Intrinsics.a(this.fmsValid, personalResponse0.fmsValid) && Intrinsics.a(this.id, personalResponse0.id) && Intrinsics.a(this.issueDate, personalResponse0.issueDate) && Intrinsics.a(this.issueId, personalResponse0.issueId) && Intrinsics.a(this.issuedBy, personalResponse0.issuedBy) && Intrinsics.a(this.lastName, personalResponse0.lastName) && Intrinsics.a(this.latinFirstName, personalResponse0.latinFirstName) && Intrinsics.a(this.latinLastName, personalResponse0.latinLastName) && Intrinsics.a(this.number, personalResponse0.number) && Intrinsics.a(this.parentCount, personalResponse0.parentCount) && Intrinsics.a(this.series, personalResponse0.series) && Intrinsics.a(this.stateFacts, personalResponse0.stateFacts) && Intrinsics.a(this.type, personalResponse0.type) && Intrinsics.a(this.verifyingValue, personalResponse0.verifyingValue) && Intrinsics.a(this.vrfReqId, personalResponse0.vrfReqId) && Intrinsics.a(this.vrfStu, personalResponse0.vrfStu) && Intrinsics.a(this.vrfValStu, personalResponse0.vrfValStu);
    }

    public final String getActNo() {
        return this.actNo;
    }

    public final List<String> getCategories() {
        return this.categories;
    }

    public final String getETag() {
        return this.eTag;
    }

    public final String getExpiryDate() {
        return this.expiryDate;
    }

    public final String getFirstName() {
        return this.firstName;
    }

    public final String getFmsState() {
        return this.fmsState;
    }

    public final Boolean getFmsValid() {
        return this.fmsValid;
    }

    public final Integer getId() {
        return this.id;
    }

    public final String getIssueDate() {
        return this.issueDate;
    }

    public final String getIssueId() {
        return this.issueId;
    }

    public final String getIssuedBy() {
        return this.issuedBy;
    }

    public final String getLastName() {
        return this.lastName;
    }

    public final String getLatinFirstName() {
        return this.latinFirstName;
    }

    public final String getLatinLastName() {
        return this.latinLastName;
    }

    public final String getNumber() {
        return this.number;
    }

    public final String getParentCount() {
        return this.parentCount;
    }

    public final String getRecordDate() {
        return this.recordDate;
    }

    public final String getSeries() {
        return this.series;
    }

    public final List<String> getStateFacts() {
        return this.stateFacts;
    }

    public final PersonalResponse2 getType() {
        return this.type;
    }

    public final Map<String, String> getVerifyingValue() {
        return this.verifyingValue;
    }

    public final Integer getVrfReqId() {
        return this.vrfReqId;
    }

    public final PersonalResponse1 getVrfStu() {
        return this.vrfStu;
    }

    public final String getVrfValStu() {
        return this.vrfValStu;
    }

    public int hashCode() {
        String str = this.actNo;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        List<String> list = this.categories;
        int hashCode2 = (hashCode + (list != null ? list.hashCode() : 0)) * 31;
        String str2 = this.recordDate;
        int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.eTag;
        int hashCode4 = (hashCode3 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.expiryDate;
        int hashCode5 = (hashCode4 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.firstName;
        int hashCode6 = (hashCode5 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.fmsState;
        int hashCode7 = (hashCode6 + (str6 != null ? str6.hashCode() : 0)) * 31;
        Boolean bool = this.fmsValid;
        int hashCode8 = (hashCode7 + (bool != null ? bool.hashCode() : 0)) * 31;
        Integer num = this.id;
        int hashCode9 = (hashCode8 + (num != null ? num.hashCode() : 0)) * 31;
        String str7 = this.issueDate;
        int hashCode10 = (hashCode9 + (str7 != null ? str7.hashCode() : 0)) * 31;
        String str8 = this.issueId;
        int hashCode11 = (hashCode10 + (str8 != null ? str8.hashCode() : 0)) * 31;
        String str9 = this.issuedBy;
        int hashCode12 = (hashCode11 + (str9 != null ? str9.hashCode() : 0)) * 31;
        String str10 = this.lastName;
        int hashCode13 = (hashCode12 + (str10 != null ? str10.hashCode() : 0)) * 31;
        String str11 = this.latinFirstName;
        int hashCode14 = (hashCode13 + (str11 != null ? str11.hashCode() : 0)) * 31;
        String str12 = this.latinLastName;
        int hashCode15 = (hashCode14 + (str12 != null ? str12.hashCode() : 0)) * 31;
        String str13 = this.number;
        int hashCode16 = (hashCode15 + (str13 != null ? str13.hashCode() : 0)) * 31;
        String str14 = this.parentCount;
        int hashCode17 = (hashCode16 + (str14 != null ? str14.hashCode() : 0)) * 31;
        String str15 = this.series;
        int hashCode18 = (hashCode17 + (str15 != null ? str15.hashCode() : 0)) * 31;
        List<String> list2 = this.stateFacts;
        int hashCode19 = (hashCode18 + (list2 != null ? list2.hashCode() : 0)) * 31;
        PersonalResponse2 personalResponse2 = this.type;
        int hashCode20 = (hashCode19 + (personalResponse2 != null ? personalResponse2.hashCode() : 0)) * 31;
        Map<String, String> map = this.verifyingValue;
        int hashCode21 = (hashCode20 + (map != null ? map.hashCode() : 0)) * 31;
        Integer num2 = this.vrfReqId;
        int hashCode22 = (hashCode21 + (num2 != null ? num2.hashCode() : 0)) * 31;
        PersonalResponse1 personalResponse1 = this.vrfStu;
        int hashCode23 = (hashCode22 + (personalResponse1 != null ? personalResponse1.hashCode() : 0)) * 31;
        String str16 = this.vrfValStu;
        if (str16 != null) {
            i2 = str16.hashCode();
        }
        return hashCode23 + i2;
    }

    public String toString() {
        StringBuilder a = outline.a("Document(actNo=");
        a.append(this.actNo);
        a.append(", categories=");
        a.append(this.categories);
        a.append(", recordDate=");
        a.append(this.recordDate);
        a.append(", eTag=");
        a.append(this.eTag);
        a.append(", expiryDate=");
        a.append(this.expiryDate);
        a.append(", firstName=");
        a.append(this.firstName);
        a.append(", fmsState=");
        a.append(this.fmsState);
        a.append(", fmsValid=");
        a.append(this.fmsValid);
        a.append(", id=");
        a.append(this.id);
        a.append(", issueDate=");
        a.append(this.issueDate);
        a.append(", issueId=");
        a.append(this.issueId);
        a.append(", issuedBy=");
        a.append(this.issuedBy);
        a.append(", lastName=");
        a.append(this.lastName);
        a.append(", latinFirstName=");
        a.append(this.latinFirstName);
        a.append(", latinLastName=");
        a.append(this.latinLastName);
        a.append(", number=");
        a.append(this.number);
        a.append(", parentCount=");
        a.append(this.parentCount);
        a.append(", series=");
        a.append(this.series);
        a.append(", stateFacts=");
        a.append(this.stateFacts);
        a.append(", type=");
        a.append(this.type);
        a.append(", verifyingValue=");
        a.append(this.verifyingValue);
        a.append(", vrfReqId=");
        a.append(this.vrfReqId);
        a.append(", vrfStu=");
        a.append(this.vrfStu);
        a.append(", vrfValStu=");
        return outline.a(a, this.vrfValStu, ")");
    }

    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeString(this.actNo);
            parcel.writeStringList(this.categories);
            parcel.writeString(this.recordDate);
            parcel.writeString(this.eTag);
            parcel.writeString(this.expiryDate);
            parcel.writeString(this.firstName);
            parcel.writeString(this.fmsState);
            Boolean bool = this.fmsValid;
            if (bool != null) {
                parcel.writeInt(1);
                parcel.writeInt(bool.booleanValue() ? 1 : 0);
            } else {
                parcel.writeInt(0);
            }
            Integer num = this.id;
            if (num != null) {
                parcel.writeInt(1);
                parcel.writeInt(num.intValue());
            } else {
                parcel.writeInt(0);
            }
            parcel.writeString(this.issueDate);
            parcel.writeString(this.issueId);
            parcel.writeString(this.issuedBy);
            parcel.writeString(this.lastName);
            parcel.writeString(this.latinFirstName);
            parcel.writeString(this.latinLastName);
            parcel.writeString(this.number);
            parcel.writeString(this.parentCount);
            parcel.writeString(this.series);
            parcel.writeStringList(this.stateFacts);
            parcel.writeString(this.type.name());
            Map<String, String> map = this.verifyingValue;
            if (map != null) {
                parcel.writeInt(1);
                parcel.writeInt(map.size());
                for (Map.Entry<String, String> next : map.entrySet()) {
                    parcel.writeString(next.getKey());
                    parcel.writeString(next.getValue());
                }
            } else {
                parcel.writeInt(0);
            }
            Integer num2 = this.vrfReqId;
            if (num2 != null) {
                parcel.writeInt(1);
                parcel.writeInt(num2.intValue());
            } else {
                parcel.writeInt(0);
            }
            PersonalResponse1 personalResponse1 = this.vrfStu;
            if (personalResponse1 != null) {
                parcel.writeInt(1);
                parcel.writeString(personalResponse1.name());
            } else {
                parcel.writeInt(0);
            }
            parcel.writeString(this.vrfValStu);
            return;
        }
        Intrinsics.a("parcel");
        throw null;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ PersonalResponse0(java.lang.String r29, java.util.List r30, java.lang.String r31, java.lang.String r32, java.lang.String r33, java.lang.String r34, java.lang.String r35, java.lang.Boolean r36, java.lang.Integer r37, java.lang.String r38, java.lang.String r39, java.lang.String r40, java.lang.String r41, java.lang.String r42, java.lang.String r43, java.lang.String r44, java.lang.String r45, java.lang.String r46, java.util.List r47, ru.covid19.core.data.network.model.PersonalResponse2 r48, java.util.Map r49, java.lang.Integer r50, ru.covid19.core.data.network.model.PersonalResponse1 r51, java.lang.String r52, int r53, n.n.c.DefaultConstructorMarker r54) {
        /*
            r28 = this;
            r0 = r53
            r1 = r0 & 1
            r2 = 0
            if (r1 == 0) goto L_0x0009
            r4 = r2
            goto L_0x000b
        L_0x0009:
            r4 = r29
        L_0x000b:
            r1 = r0 & 2
            if (r1 == 0) goto L_0x0011
            r5 = r2
            goto L_0x0013
        L_0x0011:
            r5 = r30
        L_0x0013:
            r1 = r0 & 4
            if (r1 == 0) goto L_0x0019
            r6 = r2
            goto L_0x001b
        L_0x0019:
            r6 = r31
        L_0x001b:
            r1 = r0 & 8
            if (r1 == 0) goto L_0x0021
            r7 = r2
            goto L_0x0023
        L_0x0021:
            r7 = r32
        L_0x0023:
            r1 = r0 & 16
            if (r1 == 0) goto L_0x0029
            r8 = r2
            goto L_0x002b
        L_0x0029:
            r8 = r33
        L_0x002b:
            r1 = r0 & 32
            if (r1 == 0) goto L_0x0031
            r9 = r2
            goto L_0x0033
        L_0x0031:
            r9 = r34
        L_0x0033:
            r1 = r0 & 64
            if (r1 == 0) goto L_0x0039
            r10 = r2
            goto L_0x003b
        L_0x0039:
            r10 = r35
        L_0x003b:
            r1 = r0 & 128(0x80, float:1.794E-43)
            if (r1 == 0) goto L_0x0041
            r11 = r2
            goto L_0x0043
        L_0x0041:
            r11 = r36
        L_0x0043:
            r1 = r0 & 256(0x100, float:3.59E-43)
            if (r1 == 0) goto L_0x0049
            r12 = r2
            goto L_0x004b
        L_0x0049:
            r12 = r37
        L_0x004b:
            r1 = r0 & 512(0x200, float:7.175E-43)
            if (r1 == 0) goto L_0x0051
            r13 = r2
            goto L_0x0053
        L_0x0051:
            r13 = r38
        L_0x0053:
            r1 = r0 & 1024(0x400, float:1.435E-42)
            if (r1 == 0) goto L_0x0059
            r14 = r2
            goto L_0x005b
        L_0x0059:
            r14 = r39
        L_0x005b:
            r1 = r0 & 2048(0x800, float:2.87E-42)
            if (r1 == 0) goto L_0x0061
            r15 = r2
            goto L_0x0063
        L_0x0061:
            r15 = r40
        L_0x0063:
            r1 = r0 & 4096(0x1000, float:5.74E-42)
            if (r1 == 0) goto L_0x006a
            r16 = r2
            goto L_0x006c
        L_0x006a:
            r16 = r41
        L_0x006c:
            r1 = r0 & 8192(0x2000, float:1.14794E-41)
            if (r1 == 0) goto L_0x0073
            r17 = r2
            goto L_0x0075
        L_0x0073:
            r17 = r42
        L_0x0075:
            r1 = r0 & 16384(0x4000, float:2.2959E-41)
            if (r1 == 0) goto L_0x007c
            r18 = r2
            goto L_0x007e
        L_0x007c:
            r18 = r43
        L_0x007e:
            r1 = 32768(0x8000, float:4.5918E-41)
            r1 = r1 & r0
            if (r1 == 0) goto L_0x0087
            r19 = r2
            goto L_0x0089
        L_0x0087:
            r19 = r44
        L_0x0089:
            r1 = 65536(0x10000, float:9.18355E-41)
            r1 = r1 & r0
            if (r1 == 0) goto L_0x0091
            r20 = r2
            goto L_0x0093
        L_0x0091:
            r20 = r45
        L_0x0093:
            r1 = 131072(0x20000, float:1.83671E-40)
            r1 = r1 & r0
            if (r1 == 0) goto L_0x009b
            r21 = r2
            goto L_0x009d
        L_0x009b:
            r21 = r46
        L_0x009d:
            r1 = 262144(0x40000, float:3.67342E-40)
            r1 = r1 & r0
            if (r1 == 0) goto L_0x00a5
            r22 = r2
            goto L_0x00a7
        L_0x00a5:
            r22 = r47
        L_0x00a7:
            r1 = 1048576(0x100000, float:1.469368E-39)
            r1 = r1 & r0
            if (r1 == 0) goto L_0x00af
            r24 = r2
            goto L_0x00b1
        L_0x00af:
            r24 = r49
        L_0x00b1:
            r1 = 2097152(0x200000, float:2.938736E-39)
            r1 = r1 & r0
            if (r1 == 0) goto L_0x00b9
            r25 = r2
            goto L_0x00bb
        L_0x00b9:
            r25 = r50
        L_0x00bb:
            r1 = 4194304(0x400000, float:5.877472E-39)
            r1 = r1 & r0
            if (r1 == 0) goto L_0x00c3
            r26 = r2
            goto L_0x00c5
        L_0x00c3:
            r26 = r51
        L_0x00c5:
            r1 = 8388608(0x800000, float:1.17549435E-38)
            r0 = r0 & r1
            if (r0 == 0) goto L_0x00cd
            r27 = r2
            goto L_0x00cf
        L_0x00cd:
            r27 = r52
        L_0x00cf:
            r3 = r28
            r23 = r48
            r3.<init>(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: ru.covid19.core.data.network.model.PersonalResponse0.<init>(java.lang.String, java.util.List, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.Boolean, java.lang.Integer, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.util.List, ru.covid19.core.data.network.model.PersonalResponse2, java.util.Map, java.lang.Integer, ru.covid19.core.data.network.model.PersonalResponse1, java.lang.String, int, n.n.c.DefaultConstructorMarker):void");
    }
}
