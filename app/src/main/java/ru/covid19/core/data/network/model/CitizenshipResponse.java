package ru.covid19.core.data.network.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.crashlytics.android.core.DefaultAppMeasurementEventListenerRegistrar;
import j.a.a.a.outline;
import j.c.d.a0.SerializedName;
import n.n.c.Intrinsics;

/* compiled from: CitizenshipResponse.kt */
public final class CitizenshipResponse implements Parcelable {
    public static final Parcelable.Creator CREATOR = new Creator();
    @SerializedName("char3Code")
    public final String code;
    public final String name;
    @SerializedName("msgKey")
    public final String nameKey;

    public static class Creator implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            if (parcel != null) {
                return new CitizenshipResponse(parcel.readString(), parcel.readString(), parcel.readString());
            }
            Intrinsics.a("in");
            throw null;
        }

        public final Object[] newArray(int i2) {
            return new CitizenshipResponse[i2];
        }
    }

    public CitizenshipResponse(String str, String str2, String str3) {
        if (str == null) {
            Intrinsics.a("code");
            throw null;
        } else if (str2 == null) {
            Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
            throw null;
        } else if (str3 != null) {
            this.code = str;
            this.name = str2;
            this.nameKey = str3;
        } else {
            Intrinsics.a("nameKey");
            throw null;
        }
    }

    public static /* synthetic */ CitizenshipResponse copy$default(CitizenshipResponse citizenshipResponse, String str, String str2, String str3, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            str = citizenshipResponse.code;
        }
        if ((i2 & 2) != 0) {
            str2 = citizenshipResponse.name;
        }
        if ((i2 & 4) != 0) {
            str3 = citizenshipResponse.nameKey;
        }
        return citizenshipResponse.copy(str, str2, str3);
    }

    public final String component1() {
        return this.code;
    }

    public final String component2() {
        return this.name;
    }

    public final String component3() {
        return this.nameKey;
    }

    public final CitizenshipResponse copy(String str, String str2, String str3) {
        if (str == null) {
            Intrinsics.a("code");
            throw null;
        } else if (str2 == null) {
            Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
            throw null;
        } else if (str3 != null) {
            return new CitizenshipResponse(str, str2, str3);
        } else {
            Intrinsics.a("nameKey");
            throw null;
        }
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CitizenshipResponse)) {
            return false;
        }
        CitizenshipResponse citizenshipResponse = (CitizenshipResponse) obj;
        return Intrinsics.a(this.code, citizenshipResponse.code) && Intrinsics.a(this.name, citizenshipResponse.name) && Intrinsics.a(this.nameKey, citizenshipResponse.nameKey);
    }

    public final String getCode() {
        return this.code;
    }

    public final String getName() {
        return this.name;
    }

    public final String getNameKey() {
        return this.nameKey;
    }

    public int hashCode() {
        String str = this.code;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.name;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.nameKey;
        if (str3 != null) {
            i2 = str3.hashCode();
        }
        return hashCode2 + i2;
    }

    public String toString() {
        StringBuilder a = outline.a("Citizenship(code=");
        a.append(this.code);
        a.append(", name=");
        a.append(this.name);
        a.append(", nameKey=");
        return outline.a(a, this.nameKey, ")");
    }

    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeString(this.code);
            parcel.writeString(this.name);
            parcel.writeString(this.nameKey);
            return;
        }
        Intrinsics.a("parcel");
        throw null;
    }
}
