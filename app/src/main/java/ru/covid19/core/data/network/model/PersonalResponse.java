package ru.covid19.core.data.network.model;

import com.crashlytics.android.core.CodedOutputStream;
import com.crashlytics.android.core.LogFileManager;
import j.a.a.a.outline;
import java.util.List;
import n.n.c.Intrinsics;

/* compiled from: PersonalResponse.kt */
public final class PersonalResponse extends BaseResponse {
    public final Addresses addresses;
    public int avatarRes;
    public String avatarUrl;
    public final String avatarUuid;
    public boolean biomStu;
    public final String birthDate;
    public final String birthPlace;
    public final List<String> chosenCfmTypes;
    public final String citizenship;
    public final Boolean containsUpCfmCode;
    public final PersonalResponse3 documents;
    public final String eTag;
    public final String firstName;
    public final PersonalResponse4 gender;
    public final String inn;
    public final String lastName;
    public final String middleName;
    public final Integer rIdDoc;
    public final PersonalResponse5 regCtxCfmSte;
    public final String regType;
    public final String snils;
    public final Integer social;
    public final List<String> stateFacts;
    public final String status;
    public final boolean trusted;
    public final int updatedOn;
    public final boolean verifying;

    public PersonalResponse(Addresses addresses2, String str, String str2, String str3, List<String> list, String str4, Boolean bool, Documents documents2, String str5, String str6, Gender gender2, String str7, String str8, String str9, Integer num, RegCtxCfmSte regCtxCfmSte2, String str10, String str11, Integer num2, List<String> list2, String str12, boolean z, int i2, boolean z2, String str13, int i3, boolean z3) {
        List<String> list3 = list;
        String str14 = str5;
        RegCtxCfmSte regCtxCfmSte3 = regCtxCfmSte2;
        List<String> list4 = list2;
        String str15 = str12;
        if (list3 == null) {
            Intrinsics.a("chosenCfmTypes");
            throw null;
        } else if (str14 == null) {
            Intrinsics.a("eTag");
            throw null;
        } else if (regCtxCfmSte3 == null) {
            Intrinsics.a("regCtxCfmSte");
            throw null;
        } else if (list4 == null) {
            Intrinsics.a("stateFacts");
            throw null;
        } else if (str15 != null) {
            this.addresses = addresses2;
            this.avatarUuid = str;
            this.birthDate = str2;
            this.birthPlace = str3;
            this.chosenCfmTypes = list3;
            this.citizenship = str4;
            this.containsUpCfmCode = bool;
            this.documents = documents2;
            this.eTag = str14;
            this.firstName = str6;
            this.gender = gender2;
            this.inn = str7;
            this.lastName = str8;
            this.middleName = str9;
            this.rIdDoc = num;
            this.regCtxCfmSte = regCtxCfmSte3;
            this.regType = str10;
            this.snils = str11;
            this.social = num2;
            this.stateFacts = list4;
            this.status = str15;
            this.trusted = z;
            this.updatedOn = i2;
            this.verifying = z2;
            this.avatarUrl = str13;
            this.avatarRes = i3;
            this.biomStu = z3;
        } else {
            Intrinsics.a("status");
            throw null;
        }
    }

    public static /* synthetic */ PersonalResponse copy$default(PersonalResponse personalResponse, Addresses addresses2, String str, String str2, String str3, List list, String str4, Boolean bool, PersonalResponse3 personalResponse3, String str5, String str6, PersonalResponse4 personalResponse4, String str7, String str8, String str9, Integer num, PersonalResponse5 personalResponse5, String str10, String str11, Integer num2, List list2, String str12, boolean z, int i2, boolean z2, String str13, int i3, boolean z3, int i4, Object obj) {
        PersonalResponse personalResponse2 = personalResponse;
        int i5 = i4;
        return personalResponse.copy((i5 & 1) != 0 ? personalResponse2.addresses : addresses2, (i5 & 2) != 0 ? personalResponse2.avatarUuid : str, (i5 & 4) != 0 ? personalResponse2.birthDate : str2, (i5 & 8) != 0 ? personalResponse2.birthPlace : str3, (i5 & 16) != 0 ? personalResponse2.chosenCfmTypes : list, (i5 & 32) != 0 ? personalResponse2.citizenship : str4, (i5 & 64) != 0 ? personalResponse2.containsUpCfmCode : bool, (i5 & 128) != 0 ? personalResponse2.documents : personalResponse3, (i5 & 256) != 0 ? personalResponse2.eTag : str5, (i5 & 512) != 0 ? personalResponse2.firstName : str6, (i5 & 1024) != 0 ? personalResponse2.gender : personalResponse4, (i5 & 2048) != 0 ? personalResponse2.inn : str7, (i5 & CodedOutputStream.DEFAULT_BUFFER_SIZE) != 0 ? personalResponse2.lastName : str8, (i5 & 8192) != 0 ? personalResponse2.middleName : str9, (i5 & 16384) != 0 ? personalResponse2.rIdDoc : num, (i5 & 32768) != 0 ? personalResponse2.regCtxCfmSte : personalResponse5, (i5 & LogFileManager.MAX_LOG_SIZE) != 0 ? personalResponse2.regType : str10, (i5 & 131072) != 0 ? personalResponse2.snils : str11, (i5 & 262144) != 0 ? personalResponse2.social : num2, (i5 & 524288) != 0 ? personalResponse2.stateFacts : list2, (i5 & 1048576) != 0 ? personalResponse2.status : str12, (i5 & 2097152) != 0 ? personalResponse2.trusted : z, (i5 & 4194304) != 0 ? personalResponse2.updatedOn : i2, (i5 & 8388608) != 0 ? personalResponse2.verifying : z2, (i5 & 16777216) != 0 ? personalResponse2.avatarUrl : str13, (i5 & 33554432) != 0 ? personalResponse2.avatarRes : i3, (i5 & 67108864) != 0 ? personalResponse2.biomStu : z3);
    }

    public final Addresses component1() {
        return this.addresses;
    }

    public final String component10() {
        return this.firstName;
    }

    public final PersonalResponse4 component11() {
        return this.gender;
    }

    public final String component12() {
        return this.inn;
    }

    public final String component13() {
        return this.lastName;
    }

    public final String component14() {
        return this.middleName;
    }

    public final Integer component15() {
        return this.rIdDoc;
    }

    public final PersonalResponse5 component16() {
        return this.regCtxCfmSte;
    }

    public final String component17() {
        return this.regType;
    }

    public final String component18() {
        return this.snils;
    }

    public final Integer component19() {
        return this.social;
    }

    public final String component2() {
        return this.avatarUuid;
    }

    public final List<String> component20() {
        return this.stateFacts;
    }

    public final String component21() {
        return this.status;
    }

    public final boolean component22() {
        return this.trusted;
    }

    public final int component23() {
        return this.updatedOn;
    }

    public final boolean component24() {
        return this.verifying;
    }

    public final String component25() {
        return this.avatarUrl;
    }

    public final int component26() {
        return this.avatarRes;
    }

    public final boolean component27() {
        return this.biomStu;
    }

    public final String component3() {
        return this.birthDate;
    }

    public final String component4() {
        return this.birthPlace;
    }

    public final List<String> component5() {
        return this.chosenCfmTypes;
    }

    public final String component6() {
        return this.citizenship;
    }

    public final Boolean component7() {
        return this.containsUpCfmCode;
    }

    public final PersonalResponse3 component8() {
        return this.documents;
    }

    public final String component9() {
        return this.eTag;
    }

    public final PersonalResponse copy(Addresses addresses2, String str, String str2, String str3, List<String> list, String str4, Boolean bool, Documents documents2, String str5, String str6, Gender gender2, String str7, String str8, String str9, Integer num, RegCtxCfmSte regCtxCfmSte2, String str10, String str11, Integer num2, List<String> list2, String str12, boolean z, int i2, boolean z2, String str13, int i3, boolean z3) {
        if (list == null) {
            Intrinsics.a("chosenCfmTypes");
            throw null;
        } else if (str5 == null) {
            Intrinsics.a("eTag");
            throw null;
        } else if (regCtxCfmSte2 == null) {
            Intrinsics.a("regCtxCfmSte");
            throw null;
        } else if (list2 == null) {
            Intrinsics.a("stateFacts");
            throw null;
        } else if (str12 != null) {
            return new PersonalResponse(addresses2, str, str2, str3, list, str4, bool, documents2, str5, str6, gender2, str7, str8, str9, num, regCtxCfmSte2, str10, str11, num2, list2, str12, z, i2, z2, str13, i3, z3);
        } else {
            Intrinsics.a("status");
            throw null;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PersonalResponse)) {
            return false;
        }
        PersonalResponse personalResponse = (PersonalResponse) obj;
        return Intrinsics.a(this.addresses, personalResponse.addresses) && Intrinsics.a(this.avatarUuid, personalResponse.avatarUuid) && Intrinsics.a(this.birthDate, personalResponse.birthDate) && Intrinsics.a(this.birthPlace, personalResponse.birthPlace) && Intrinsics.a(this.chosenCfmTypes, personalResponse.chosenCfmTypes) && Intrinsics.a(this.citizenship, personalResponse.citizenship) && Intrinsics.a(this.containsUpCfmCode, personalResponse.containsUpCfmCode) && Intrinsics.a(this.documents, personalResponse.documents) && Intrinsics.a(this.eTag, personalResponse.eTag) && Intrinsics.a(this.firstName, personalResponse.firstName) && Intrinsics.a(this.gender, personalResponse.gender) && Intrinsics.a(this.inn, personalResponse.inn) && Intrinsics.a(this.lastName, personalResponse.lastName) && Intrinsics.a(this.middleName, personalResponse.middleName) && Intrinsics.a(this.rIdDoc, personalResponse.rIdDoc) && Intrinsics.a(this.regCtxCfmSte, personalResponse.regCtxCfmSte) && Intrinsics.a(this.regType, personalResponse.regType) && Intrinsics.a(this.snils, personalResponse.snils) && Intrinsics.a(this.social, personalResponse.social) && Intrinsics.a(this.stateFacts, personalResponse.stateFacts) && Intrinsics.a(this.status, personalResponse.status) && this.trusted == personalResponse.trusted && this.updatedOn == personalResponse.updatedOn && this.verifying == personalResponse.verifying && Intrinsics.a(this.avatarUrl, personalResponse.avatarUrl) && this.avatarRes == personalResponse.avatarRes && this.biomStu == personalResponse.biomStu;
    }

    public final Addresses getAddresses() {
        return this.addresses;
    }

    public final int getAvatarRes() {
        return this.avatarRes;
    }

    public final String getAvatarUrl() {
        return this.avatarUrl;
    }

    public final String getAvatarUuid() {
        return this.avatarUuid;
    }

    public final boolean getBiomStu() {
        return this.biomStu;
    }

    public final String getBirthDate() {
        return this.birthDate;
    }

    public final String getBirthPlace() {
        return this.birthPlace;
    }

    public final List<String> getChosenCfmTypes() {
        return this.chosenCfmTypes;
    }

    public final String getCitizenship() {
        return this.citizenship;
    }

    public final Boolean getContainsUpCfmCode() {
        return this.containsUpCfmCode;
    }

    public final PersonalResponse3 getDocuments() {
        return this.documents;
    }

    public final String getETag() {
        return this.eTag;
    }

    public final String getFirstName() {
        return this.firstName;
    }

    public final PersonalResponse4 getGender() {
        return this.gender;
    }

    public final String getInn() {
        return this.inn;
    }

    public final String getLastName() {
        return this.lastName;
    }

    public final String getMiddleName() {
        return this.middleName;
    }

    public final Integer getRIdDoc() {
        return this.rIdDoc;
    }

    public final PersonalResponse5 getRegCtxCfmSte() {
        return this.regCtxCfmSte;
    }

    public final String getRegType() {
        return this.regType;
    }

    public final String getSnils() {
        return this.snils;
    }

    public final Integer getSocial() {
        return this.social;
    }

    public final List<String> getStateFacts() {
        return this.stateFacts;
    }

    public final String getStatus() {
        return this.status;
    }

    public final boolean getTrusted() {
        return this.trusted;
    }

    public final int getUpdatedOn() {
        return this.updatedOn;
    }

    public final boolean getVerifying() {
        return this.verifying;
    }

    public int hashCode() {
        Addresses addresses2 = this.addresses;
        int i2 = 0;
        int hashCode = (addresses2 != null ? addresses2.hashCode() : 0) * 31;
        String str = this.avatarUuid;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.birthDate;
        int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.birthPlace;
        int hashCode4 = (hashCode3 + (str3 != null ? str3.hashCode() : 0)) * 31;
        List<String> list = this.chosenCfmTypes;
        int hashCode5 = (hashCode4 + (list != null ? list.hashCode() : 0)) * 31;
        String str4 = this.citizenship;
        int hashCode6 = (hashCode5 + (str4 != null ? str4.hashCode() : 0)) * 31;
        Boolean bool = this.containsUpCfmCode;
        int hashCode7 = (hashCode6 + (bool != null ? bool.hashCode() : 0)) * 31;
        PersonalResponse3 personalResponse3 = this.documents;
        int hashCode8 = (hashCode7 + (personalResponse3 != null ? personalResponse3.hashCode() : 0)) * 31;
        String str5 = this.eTag;
        int hashCode9 = (hashCode8 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.firstName;
        int hashCode10 = (hashCode9 + (str6 != null ? str6.hashCode() : 0)) * 31;
        PersonalResponse4 personalResponse4 = this.gender;
        int hashCode11 = (hashCode10 + (personalResponse4 != null ? personalResponse4.hashCode() : 0)) * 31;
        String str7 = this.inn;
        int hashCode12 = (hashCode11 + (str7 != null ? str7.hashCode() : 0)) * 31;
        String str8 = this.lastName;
        int hashCode13 = (hashCode12 + (str8 != null ? str8.hashCode() : 0)) * 31;
        String str9 = this.middleName;
        int hashCode14 = (hashCode13 + (str9 != null ? str9.hashCode() : 0)) * 31;
        Integer num = this.rIdDoc;
        int hashCode15 = (hashCode14 + (num != null ? num.hashCode() : 0)) * 31;
        PersonalResponse5 personalResponse5 = this.regCtxCfmSte;
        int hashCode16 = (hashCode15 + (personalResponse5 != null ? personalResponse5.hashCode() : 0)) * 31;
        String str10 = this.regType;
        int hashCode17 = (hashCode16 + (str10 != null ? str10.hashCode() : 0)) * 31;
        String str11 = this.snils;
        int hashCode18 = (hashCode17 + (str11 != null ? str11.hashCode() : 0)) * 31;
        Integer num2 = this.social;
        int hashCode19 = (hashCode18 + (num2 != null ? num2.hashCode() : 0)) * 31;
        List<String> list2 = this.stateFacts;
        int hashCode20 = (hashCode19 + (list2 != null ? list2.hashCode() : 0)) * 31;
        String str12 = this.status;
        int hashCode21 = (hashCode20 + (str12 != null ? str12.hashCode() : 0)) * 31;
        boolean z = this.trusted;
        boolean z2 = true;
        if (z) {
            z = true;
        }
        int i3 = (((hashCode21 + (z ? 1 : 0)) * 31) + this.updatedOn) * 31;
        boolean z3 = this.verifying;
        if (z3) {
            z3 = true;
        }
        int i4 = (i3 + (z3 ? 1 : 0)) * 31;
        String str13 = this.avatarUrl;
        if (str13 != null) {
            i2 = str13.hashCode();
        }
        int i5 = (((i4 + i2) * 31) + this.avatarRes) * 31;
        boolean z4 = this.biomStu;
        if (!z4) {
            z2 = z4;
        }
        return i5 + (z2 ? 1 : 0);
    }

    public final void setAvatarRes(int i2) {
        this.avatarRes = i2;
    }

    public final void setAvatarUrl(String str) {
        this.avatarUrl = str;
    }

    public final void setBiomStu(boolean z) {
        this.biomStu = z;
    }

    public String toString() {
        StringBuilder a = outline.a("PersonalResponse(addresses=");
        a.append(this.addresses);
        a.append(", avatarUuid=");
        a.append(this.avatarUuid);
        a.append(", birthDate=");
        a.append(this.birthDate);
        a.append(", birthPlace=");
        a.append(this.birthPlace);
        a.append(", chosenCfmTypes=");
        a.append(this.chosenCfmTypes);
        a.append(", citizenship=");
        a.append(this.citizenship);
        a.append(", containsUpCfmCode=");
        a.append(this.containsUpCfmCode);
        a.append(", documents=");
        a.append(this.documents);
        a.append(", eTag=");
        a.append(this.eTag);
        a.append(", firstName=");
        a.append(this.firstName);
        a.append(", gender=");
        a.append(this.gender);
        a.append(", inn=");
        a.append(this.inn);
        a.append(", lastName=");
        a.append(this.lastName);
        a.append(", middleName=");
        a.append(this.middleName);
        a.append(", rIdDoc=");
        a.append(this.rIdDoc);
        a.append(", regCtxCfmSte=");
        a.append(this.regCtxCfmSte);
        a.append(", regType=");
        a.append(this.regType);
        a.append(", snils=");
        a.append(this.snils);
        a.append(", social=");
        a.append(this.social);
        a.append(", stateFacts=");
        a.append(this.stateFacts);
        a.append(", status=");
        a.append(this.status);
        a.append(", trusted=");
        a.append(this.trusted);
        a.append(", updatedOn=");
        a.append(this.updatedOn);
        a.append(", verifying=");
        a.append(this.verifying);
        a.append(", avatarUrl=");
        a.append(this.avatarUrl);
        a.append(", avatarRes=");
        a.append(this.avatarRes);
        a.append(", biomStu=");
        return outline.a(a, this.biomStu, ")");
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ PersonalResponse(ru.covid19.core.data.network.model.Addresses r32, java.lang.String r33, java.lang.String r34, java.lang.String r35, java.util.List r36, java.lang.String r37, java.lang.Boolean r38, ru.covid19.core.data.network.model.PersonalResponse3 r39, java.lang.String r40, java.lang.String r41, ru.covid19.core.data.network.model.PersonalResponse4 r42, java.lang.String r43, java.lang.String r44, java.lang.String r45, java.lang.Integer r46, ru.covid19.core.data.network.model.PersonalResponse5 r47, java.lang.String r48, java.lang.String r49, java.lang.Integer r50, java.util.List r51, java.lang.String r52, boolean r53, int r54, boolean r55, java.lang.String r56, int r57, boolean r58, int r59, n.n.c.DefaultConstructorMarker r60) {
        /*
            r31 = this;
            r0 = r59
            r1 = r0 & 1
            r2 = 0
            if (r1 == 0) goto L_0x0009
            r4 = r2
            goto L_0x000b
        L_0x0009:
            r4 = r32
        L_0x000b:
            r1 = r0 & 2
            if (r1 == 0) goto L_0x0011
            r5 = r2
            goto L_0x0013
        L_0x0011:
            r5 = r33
        L_0x0013:
            r1 = r0 & 4
            if (r1 == 0) goto L_0x0019
            r6 = r2
            goto L_0x001b
        L_0x0019:
            r6 = r34
        L_0x001b:
            r1 = r0 & 8
            if (r1 == 0) goto L_0x0021
            r7 = r2
            goto L_0x0023
        L_0x0021:
            r7 = r35
        L_0x0023:
            r1 = r0 & 16
            if (r1 == 0) goto L_0x002b
            n.i.Collections2 r1 = n.i.Collections2.b
            r8 = r1
            goto L_0x002d
        L_0x002b:
            r8 = r36
        L_0x002d:
            r1 = r0 & 32
            if (r1 == 0) goto L_0x0033
            r9 = r2
            goto L_0x0035
        L_0x0033:
            r9 = r37
        L_0x0035:
            r1 = r0 & 64
            if (r1 == 0) goto L_0x003b
            r10 = r2
            goto L_0x003d
        L_0x003b:
            r10 = r38
        L_0x003d:
            r1 = r0 & 128(0x80, float:1.794E-43)
            if (r1 == 0) goto L_0x0043
            r11 = r2
            goto L_0x0045
        L_0x0043:
            r11 = r39
        L_0x0045:
            r1 = r0 & 1024(0x400, float:1.435E-42)
            if (r1 == 0) goto L_0x004b
            r14 = r2
            goto L_0x004d
        L_0x004b:
            r14 = r42
        L_0x004d:
            r1 = r0 & 2048(0x800, float:2.87E-42)
            if (r1 == 0) goto L_0x0053
            r15 = r2
            goto L_0x0055
        L_0x0053:
            r15 = r43
        L_0x0055:
            r1 = r0 & 8192(0x2000, float:1.14794E-41)
            if (r1 == 0) goto L_0x005c
            r17 = r2
            goto L_0x005e
        L_0x005c:
            r17 = r45
        L_0x005e:
            r1 = r0 & 16384(0x4000, float:2.2959E-41)
            if (r1 == 0) goto L_0x0065
            r18 = r2
            goto L_0x0067
        L_0x0065:
            r18 = r46
        L_0x0067:
            r1 = 65536(0x10000, float:9.18355E-41)
            r1 = r1 & r0
            if (r1 == 0) goto L_0x006f
            r20 = r2
            goto L_0x0071
        L_0x006f:
            r20 = r48
        L_0x0071:
            r1 = 131072(0x20000, float:1.83671E-40)
            r1 = r1 & r0
            if (r1 == 0) goto L_0x0079
            r21 = r2
            goto L_0x007b
        L_0x0079:
            r21 = r49
        L_0x007b:
            r1 = 262144(0x40000, float:3.67342E-40)
            r1 = r1 & r0
            if (r1 == 0) goto L_0x0083
            r22 = r2
            goto L_0x0085
        L_0x0083:
            r22 = r50
        L_0x0085:
            r1 = 524288(0x80000, float:7.34684E-40)
            r1 = r1 & r0
            if (r1 == 0) goto L_0x008f
            n.i.Collections2 r1 = n.i.Collections2.b
            r23 = r1
            goto L_0x0091
        L_0x008f:
            r23 = r51
        L_0x0091:
            r1 = 16777216(0x1000000, float:2.3509887E-38)
            r0 = r0 & r1
            if (r0 == 0) goto L_0x0099
            r28 = r2
            goto L_0x009b
        L_0x0099:
            r28 = r56
        L_0x009b:
            r3 = r31
            r12 = r40
            r13 = r41
            r16 = r44
            r19 = r47
            r24 = r52
            r25 = r53
            r26 = r54
            r27 = r55
            r29 = r57
            r30 = r58
            r3.<init>(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r30)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: ru.covid19.core.data.network.model.PersonalResponse.<init>(ru.covid19.core.data.network.model.Addresses, java.lang.String, java.lang.String, java.lang.String, java.util.List, java.lang.String, java.lang.Boolean, ru.covid19.core.data.network.model.PersonalResponse3, java.lang.String, java.lang.String, ru.covid19.core.data.network.model.PersonalResponse4, java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, ru.covid19.core.data.network.model.PersonalResponse5, java.lang.String, java.lang.String, java.lang.Integer, java.util.List, java.lang.String, boolean, int, boolean, java.lang.String, int, boolean, int, n.n.c.DefaultConstructorMarker):void");
    }
}
