package ru.covid19.core.data.network.model;

import j.a.a.a.outline;
import n.n.c.Intrinsics;

/* compiled from: SignResponse.kt */
public final class SignResponse extends BaseResponse {
    public final String secret;
    public final String stateMarker;
    public final String time;

    public SignResponse(String str, String str2, String str3) {
        if (str == null) {
            Intrinsics.a("secret");
            throw null;
        } else if (str2 == null) {
            Intrinsics.a("time");
            throw null;
        } else if (str3 != null) {
            this.secret = str;
            this.time = str2;
            this.stateMarker = str3;
        } else {
            Intrinsics.a("stateMarker");
            throw null;
        }
    }

    public static /* synthetic */ SignResponse copy$default(SignResponse signResponse, String str, String str2, String str3, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            str = signResponse.secret;
        }
        if ((i2 & 2) != 0) {
            str2 = signResponse.time;
        }
        if ((i2 & 4) != 0) {
            str3 = signResponse.stateMarker;
        }
        return signResponse.copy(str, str2, str3);
    }

    public final String component1() {
        return this.secret;
    }

    public final String component2() {
        return this.time;
    }

    public final String component3() {
        return this.stateMarker;
    }

    public final SignResponse copy(String str, String str2, String str3) {
        if (str == null) {
            Intrinsics.a("secret");
            throw null;
        } else if (str2 == null) {
            Intrinsics.a("time");
            throw null;
        } else if (str3 != null) {
            return new SignResponse(str, str2, str3);
        } else {
            Intrinsics.a("stateMarker");
            throw null;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SignResponse)) {
            return false;
        }
        SignResponse signResponse = (SignResponse) obj;
        return Intrinsics.a(this.secret, signResponse.secret) && Intrinsics.a(this.time, signResponse.time) && Intrinsics.a(this.stateMarker, signResponse.stateMarker);
    }

    public final String getSecret() {
        return this.secret;
    }

    public final String getStateMarker() {
        return this.stateMarker;
    }

    public final String getTime() {
        return this.time;
    }

    public int hashCode() {
        String str = this.secret;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.time;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.stateMarker;
        if (str3 != null) {
            i2 = str3.hashCode();
        }
        return hashCode2 + i2;
    }

    public String toString() {
        StringBuilder a = outline.a("SignResponse(secret=");
        a.append(this.secret);
        a.append(", time=");
        a.append(this.time);
        a.append(", stateMarker=");
        return outline.a(a, this.stateMarker, ")");
    }
}
