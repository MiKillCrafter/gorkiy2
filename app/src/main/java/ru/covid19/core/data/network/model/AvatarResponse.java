package ru.covid19.core.data.network.model;

import j.a.a.a.outline;
import n.n.c.Intrinsics;

/* compiled from: AvatarResponse.kt */
public final class AvatarResponse extends BaseResponse {
    public final String url;

    public AvatarResponse(String str) {
        if (str != null) {
            this.url = str;
        } else {
            Intrinsics.a("url");
            throw null;
        }
    }

    public static /* synthetic */ AvatarResponse copy$default(AvatarResponse avatarResponse, String str, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            str = avatarResponse.url;
        }
        return avatarResponse.copy(str);
    }

    public final String component1() {
        return this.url;
    }

    public final AvatarResponse copy(String str) {
        if (str != null) {
            return new AvatarResponse(str);
        }
        Intrinsics.a("url");
        throw null;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof AvatarResponse) && Intrinsics.a(this.url, ((AvatarResponse) obj).url);
        }
        return true;
    }

    public final String getUrl() {
        return this.url;
    }

    public int hashCode() {
        String str = this.url;
        if (str != null) {
            return str.hashCode();
        }
        return 0;
    }

    public String toString() {
        return outline.a(outline.a("AvatarResponse(url="), this.url, ")");
    }
}
