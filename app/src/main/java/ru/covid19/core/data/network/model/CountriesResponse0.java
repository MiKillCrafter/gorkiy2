package ru.covid19.core.data.network.model;

import android.os.Parcel;
import android.os.Parcelable;
import j.a.a.a.outline;
import j.c.d.a0.SerializedName;
import n.n.c.Intrinsics;

/* compiled from: CountriesResponse.kt */
public final class CountriesResponse0 implements Parcelable {
    public static final Parcelable.Creator CREATOR = new Creator();
    @SerializedName("value")
    public final String code;
    public final String title;

    public static class Creator implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            if (parcel != null) {
                return new CountriesResponse0(parcel.readString(), parcel.readString());
            }
            Intrinsics.a("in");
            throw null;
        }

        public final Object[] newArray(int i2) {
            return new CountriesResponse0[i2];
        }
    }

    public CountriesResponse0(String str, String str2) {
        if (str == null) {
            Intrinsics.a("title");
            throw null;
        } else if (str2 != null) {
            this.title = str;
            this.code = str2;
        } else {
            Intrinsics.a("code");
            throw null;
        }
    }

    public static /* synthetic */ CountriesResponse0 copy$default(CountriesResponse0 countriesResponse0, String str, String str2, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            str = countriesResponse0.title;
        }
        if ((i2 & 2) != 0) {
            str2 = countriesResponse0.code;
        }
        return countriesResponse0.copy(str, str2);
    }

    public final String component1() {
        return this.title;
    }

    public final String component2() {
        return this.code;
    }

    public final CountriesResponse0 copy(String str, String str2) {
        if (str == null) {
            Intrinsics.a("title");
            throw null;
        } else if (str2 != null) {
            return new CountriesResponse0(str, str2);
        } else {
            Intrinsics.a("code");
            throw null;
        }
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CountriesResponse0)) {
            return false;
        }
        CountriesResponse0 countriesResponse0 = (CountriesResponse0) obj;
        return Intrinsics.a(this.title, countriesResponse0.title) && Intrinsics.a(this.code, countriesResponse0.code);
    }

    public final String getCode() {
        return this.code;
    }

    public final String getTitle() {
        return this.title;
    }

    public int hashCode() {
        String str = this.title;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.code;
        if (str2 != null) {
            i2 = str2.hashCode();
        }
        return hashCode + i2;
    }

    public String toString() {
        StringBuilder a = outline.a("Country(title=");
        a.append(this.title);
        a.append(", code=");
        return outline.a(a, this.code, ")");
    }

    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeString(this.title);
            parcel.writeString(this.code);
            return;
        }
        Intrinsics.a("parcel");
        throw null;
    }
}
