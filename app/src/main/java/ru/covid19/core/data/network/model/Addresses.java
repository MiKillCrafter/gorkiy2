package ru.covid19.core.data.network.model;

import j.a.a.a.outline;
import java.util.List;
import n.n.c.Intrinsics;

/* compiled from: Addresses.kt */
public final class Addresses {
    public final String eTag;
    public final List<Address> elements;
    public final int pageIndex;
    public final int pageSize;
    public final int size;
    public final List<String> stateFacts;
    public final int totalSize;

    public Addresses(String str, List<Address> list, int i2, int i3, int i4, List<String> list2, int i5) {
        if (str == null) {
            Intrinsics.a("eTag");
            throw null;
        } else if (list == null) {
            Intrinsics.a("elements");
            throw null;
        } else if (list2 != null) {
            this.eTag = str;
            this.elements = list;
            this.pageIndex = i2;
            this.pageSize = i3;
            this.size = i4;
            this.stateFacts = list2;
            this.totalSize = i5;
        } else {
            Intrinsics.a("stateFacts");
            throw null;
        }
    }

    public static /* synthetic */ Addresses copy$default(Addresses addresses, String str, List list, int i2, int i3, int i4, List list2, int i5, int i6, Object obj) {
        if ((i6 & 1) != 0) {
            str = addresses.eTag;
        }
        if ((i6 & 2) != 0) {
            list = addresses.elements;
        }
        List list3 = list;
        if ((i6 & 4) != 0) {
            i2 = addresses.pageIndex;
        }
        int i7 = i2;
        if ((i6 & 8) != 0) {
            i3 = addresses.pageSize;
        }
        int i8 = i3;
        if ((i6 & 16) != 0) {
            i4 = addresses.size;
        }
        int i9 = i4;
        if ((i6 & 32) != 0) {
            list2 = addresses.stateFacts;
        }
        List list4 = list2;
        if ((i6 & 64) != 0) {
            i5 = addresses.totalSize;
        }
        return addresses.copy(str, list3, i7, i8, i9, list4, i5);
    }

    public final String component1() {
        return this.eTag;
    }

    public final List<Address> component2() {
        return this.elements;
    }

    public final int component3() {
        return this.pageIndex;
    }

    public final int component4() {
        return this.pageSize;
    }

    public final int component5() {
        return this.size;
    }

    public final List<String> component6() {
        return this.stateFacts;
    }

    public final int component7() {
        return this.totalSize;
    }

    public final Addresses copy(String str, List<Address> list, int i2, int i3, int i4, List<String> list2, int i5) {
        if (str == null) {
            Intrinsics.a("eTag");
            throw null;
        } else if (list == null) {
            Intrinsics.a("elements");
            throw null;
        } else if (list2 != null) {
            return new Addresses(str, list, i2, i3, i4, list2, i5);
        } else {
            Intrinsics.a("stateFacts");
            throw null;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Addresses)) {
            return false;
        }
        Addresses addresses = (Addresses) obj;
        return Intrinsics.a(this.eTag, addresses.eTag) && Intrinsics.a(this.elements, addresses.elements) && this.pageIndex == addresses.pageIndex && this.pageSize == addresses.pageSize && this.size == addresses.size && Intrinsics.a(this.stateFacts, addresses.stateFacts) && this.totalSize == addresses.totalSize;
    }

    public final String getETag() {
        return this.eTag;
    }

    public final List<Address> getElements() {
        return this.elements;
    }

    public final int getPageIndex() {
        return this.pageIndex;
    }

    public final int getPageSize() {
        return this.pageSize;
    }

    public final int getSize() {
        return this.size;
    }

    public final List<String> getStateFacts() {
        return this.stateFacts;
    }

    public final int getTotalSize() {
        return this.totalSize;
    }

    public int hashCode() {
        String str = this.eTag;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        List<Address> list = this.elements;
        int hashCode2 = (((((((hashCode + (list != null ? list.hashCode() : 0)) * 31) + this.pageIndex) * 31) + this.pageSize) * 31) + this.size) * 31;
        List<String> list2 = this.stateFacts;
        if (list2 != null) {
            i2 = list2.hashCode();
        }
        return ((hashCode2 + i2) * 31) + this.totalSize;
    }

    public String toString() {
        StringBuilder a = outline.a("Addresses(eTag=");
        a.append(this.eTag);
        a.append(", elements=");
        a.append(this.elements);
        a.append(", pageIndex=");
        a.append(this.pageIndex);
        a.append(", pageSize=");
        a.append(this.pageSize);
        a.append(", size=");
        a.append(this.size);
        a.append(", stateFacts=");
        a.append(this.stateFacts);
        a.append(", totalSize=");
        a.append(this.totalSize);
        a.append(")");
        return a.toString();
    }
}
