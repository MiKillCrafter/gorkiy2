package ru.covid19.core.data.network.model;

import j.a.a.a.outline;
import java.util.List;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;

/* compiled from: PersonalResponse.kt */
public final class PersonalResponse3 {
    public final String eTag;
    public final List<Document> elements;
    public final Integer pageIndex;
    public final Integer pageSize;
    public final int size;
    public final List<String> stateFacts;
    public final Integer totalSize;

    public PersonalResponse3(String str, List<Document> list, Integer num, Integer num2, int i2, List<String> list2, Integer num3) {
        if (list != null) {
            this.eTag = str;
            this.elements = list;
            this.pageIndex = num;
            this.pageSize = num2;
            this.size = i2;
            this.stateFacts = list2;
            this.totalSize = num3;
            return;
        }
        Intrinsics.a("elements");
        throw null;
    }

    public static /* synthetic */ PersonalResponse3 copy$default(PersonalResponse3 personalResponse3, String str, List list, Integer num, Integer num2, int i2, List list2, Integer num3, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            str = personalResponse3.eTag;
        }
        if ((i3 & 2) != 0) {
            list = personalResponse3.elements;
        }
        List list3 = list;
        if ((i3 & 4) != 0) {
            num = personalResponse3.pageIndex;
        }
        Integer num4 = num;
        if ((i3 & 8) != 0) {
            num2 = personalResponse3.pageSize;
        }
        Integer num5 = num2;
        if ((i3 & 16) != 0) {
            i2 = personalResponse3.size;
        }
        int i4 = i2;
        if ((i3 & 32) != 0) {
            list2 = personalResponse3.stateFacts;
        }
        List list4 = list2;
        if ((i3 & 64) != 0) {
            num3 = personalResponse3.totalSize;
        }
        return personalResponse3.copy(str, list3, num4, num5, i4, list4, num3);
    }

    public final String component1() {
        return this.eTag;
    }

    public final List<Document> component2() {
        return this.elements;
    }

    public final Integer component3() {
        return this.pageIndex;
    }

    public final Integer component4() {
        return this.pageSize;
    }

    public final int component5() {
        return this.size;
    }

    public final List<String> component6() {
        return this.stateFacts;
    }

    public final Integer component7() {
        return this.totalSize;
    }

    public final Documents copy(String str, List<Document> list, Integer num, Integer num2, int i2, List<String> list2, Integer num3) {
        if (list != null) {
            return new PersonalResponse3(str, list, num, num2, i2, list2, num3);
        }
        Intrinsics.a("elements");
        throw null;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PersonalResponse3)) {
            return false;
        }
        PersonalResponse3 personalResponse3 = (PersonalResponse3) obj;
        return Intrinsics.a(this.eTag, personalResponse3.eTag) && Intrinsics.a(this.elements, personalResponse3.elements) && Intrinsics.a(this.pageIndex, personalResponse3.pageIndex) && Intrinsics.a(this.pageSize, personalResponse3.pageSize) && this.size == personalResponse3.size && Intrinsics.a(this.stateFacts, personalResponse3.stateFacts) && Intrinsics.a(this.totalSize, personalResponse3.totalSize);
    }

    public final String getETag() {
        return this.eTag;
    }

    public final List<Document> getElements() {
        return this.elements;
    }

    public final Integer getPageIndex() {
        return this.pageIndex;
    }

    public final Integer getPageSize() {
        return this.pageSize;
    }

    public final int getSize() {
        return this.size;
    }

    public final List<String> getStateFacts() {
        return this.stateFacts;
    }

    public final Integer getTotalSize() {
        return this.totalSize;
    }

    public int hashCode() {
        String str = this.eTag;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        List<Document> list = this.elements;
        int hashCode2 = (hashCode + (list != null ? list.hashCode() : 0)) * 31;
        Integer num = this.pageIndex;
        int hashCode3 = (hashCode2 + (num != null ? num.hashCode() : 0)) * 31;
        Integer num2 = this.pageSize;
        int hashCode4 = (((hashCode3 + (num2 != null ? num2.hashCode() : 0)) * 31) + this.size) * 31;
        List<String> list2 = this.stateFacts;
        int hashCode5 = (hashCode4 + (list2 != null ? list2.hashCode() : 0)) * 31;
        Integer num3 = this.totalSize;
        if (num3 != null) {
            i2 = num3.hashCode();
        }
        return hashCode5 + i2;
    }

    public String toString() {
        StringBuilder a = outline.a("Documents(eTag=");
        a.append(this.eTag);
        a.append(", elements=");
        a.append(this.elements);
        a.append(", pageIndex=");
        a.append(this.pageIndex);
        a.append(", pageSize=");
        a.append(this.pageSize);
        a.append(", size=");
        a.append(this.size);
        a.append(", stateFacts=");
        a.append(this.stateFacts);
        a.append(", totalSize=");
        a.append(this.totalSize);
        a.append(")");
        return a.toString();
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ PersonalResponse3(String str, List list, Integer num, Integer num2, int i2, List list2, Integer num3, int i3, DefaultConstructorMarker defaultConstructorMarker) {
        this((i3 & 1) != 0 ? null : str, list, (i3 & 4) != 0 ? null : num, (i3 & 8) != 0 ? null : num2, i2, (i3 & 32) != 0 ? null : list2, (i3 & 64) != 0 ? null : num3);
    }
}
