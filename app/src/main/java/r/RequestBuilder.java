package r;

import com.crashlytics.android.answers.SessionEventTransform;
import com.crashlytics.android.core.DefaultAppMeasurementEventListenerRegistrar;
import j.a.a.a.outline;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import javax.annotation.Nullable;
import n.n.c.Intrinsics;
import o.FormBody;
import o.Headers;
import o.HttpUrl;
import o.MediaType;
import o.MultipartBody;
import o.Request;
import o.RequestBody;
import p.BufferedSink;

public final class RequestBuilder {

    /* renamed from: l  reason: collision with root package name */
    public static final char[] f3176l = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    /* renamed from: m  reason: collision with root package name */
    public static final Pattern f3177m = Pattern.compile("(.*/)?(\\.|%2e|%2E){1,2}(/.*)?");
    public final String a;
    public final HttpUrl b;
    @Nullable
    public String c;
    @Nullable
    public HttpUrl.a d;

    /* renamed from: e  reason: collision with root package name */
    public final Request.a f3178e = new Request.a();

    /* renamed from: f  reason: collision with root package name */
    public final Headers.a f3179f;
    @Nullable
    public MediaType g;
    public final boolean h;
    @Nullable

    /* renamed from: i  reason: collision with root package name */
    public MultipartBody.a f3180i;
    @Nullable

    /* renamed from: j  reason: collision with root package name */
    public FormBody.a f3181j;
    @Nullable

    /* renamed from: k  reason: collision with root package name */
    public RequestBody f3182k;

    public static class a extends RequestBody {
        public final RequestBody b;
        public final MediaType c;

        public a(RequestBody requestBody, MediaType mediaType) {
            this.b = super;
            this.c = mediaType;
        }

        public long a() {
            return this.b.a();
        }

        public MediaType b() {
            return this.c;
        }

        public void a(BufferedSink bufferedSink) {
            this.b.a(bufferedSink);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
    public RequestBuilder(String str, HttpUrl httpUrl, @Nullable String str2, @Nullable Headers headers, @Nullable MediaType mediaType, boolean z, boolean z2, boolean z3) {
        this.a = str;
        this.b = httpUrl;
        this.c = str2;
        this.g = mediaType;
        this.h = z;
        if (headers != null) {
            this.f3179f = headers.c();
        } else {
            this.f3179f = new Headers.a();
        }
        if (z2) {
            this.f3181j = new FormBody.a();
        } else if (z3) {
            MultipartBody.a aVar = new MultipartBody.a();
            this.f3180i = aVar;
            MediaType mediaType2 = MultipartBody.h;
            if (aVar == null) {
                throw null;
            } else if (mediaType2 == null) {
                Intrinsics.a(SessionEventTransform.TYPE_KEY);
                throw null;
            } else if (Intrinsics.a((Object) mediaType2.b, (Object) "multipart")) {
                aVar.b = mediaType2;
            } else {
                throw new IllegalArgumentException(("multipart != " + mediaType2).toString());
            }
        }
    }

    public void a(String str, String str2) {
        if ("Content-Type".equalsIgnoreCase(str)) {
            try {
                this.g = MediaType.a(str2);
            } catch (IllegalArgumentException e2) {
                throw new IllegalArgumentException(outline.a("Malformed content type: ", str2), e2);
            }
        } else {
            this.f3179f.a(str, str2);
        }
    }

    public void b(String str, @Nullable String str2, boolean z) {
        String str3 = this.c;
        String str4 = null;
        if (str3 != null) {
            HttpUrl.a a2 = this.b.a(str3);
            this.d = a2;
            if (a2 != null) {
                this.c = null;
            } else {
                StringBuilder a3 = outline.a("Malformed URL. Base: ");
                a3.append(this.b);
                a3.append(", Relative: ");
                a3.append(this.c);
                throw new IllegalArgumentException(a3.toString());
            }
        }
        if (z) {
            HttpUrl.a aVar = this.d;
            if (str != null) {
                if (aVar.g == null) {
                    aVar.g = new ArrayList();
                }
                List<String> list = aVar.g;
                if (list != null) {
                    list.add(HttpUrl.b.a(HttpUrl.f2850l, str, 0, 0, " \"'<>#&=", true, false, true, false, null, 211));
                    List<String> list2 = aVar.g;
                    if (list2 != null) {
                        if (str2 != null) {
                            str4 = HttpUrl.b.a(HttpUrl.f2850l, str2, 0, 0, " \"'<>#&=", true, false, true, false, null, 211);
                        }
                        list2.add(str4);
                        return;
                    }
                    Intrinsics.a();
                    throw null;
                }
                Intrinsics.a();
                throw null;
            }
            Intrinsics.a("encodedName");
            throw null;
        }
        HttpUrl.a aVar2 = this.d;
        if (str != null) {
            if (aVar2.g == null) {
                aVar2.g = new ArrayList();
            }
            List<String> list3 = aVar2.g;
            if (list3 != null) {
                list3.add(HttpUrl.b.a(HttpUrl.f2850l, str, 0, 0, " !\"#$&'(),/:;<=>?@[]\\^`{|}~", false, false, true, false, null, 219));
                List<String> list4 = aVar2.g;
                if (list4 != null) {
                    if (str2 != null) {
                        str4 = HttpUrl.b.a(HttpUrl.f2850l, str2, 0, 0, " !\"#$&'(),/:;<=>?@[]\\^`{|}~", false, false, true, false, null, 219);
                    }
                    list4.add(str4);
                    return;
                }
                Intrinsics.a();
                throw null;
            }
            Intrinsics.a();
            throw null;
        }
        Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
        throw null;
    }

    public void a(String str, String str2, boolean z) {
        if (z) {
            FormBody.a aVar = this.f3181j;
            if (str == null) {
                Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
                throw null;
            } else if (str2 != null) {
                aVar.a.add(HttpUrl.b.a(HttpUrl.f2850l, str, 0, 0, " \"':;<=>@[]^`{}|/\\?#&!$(),~", true, false, true, false, aVar.c, 83));
                aVar.b.add(HttpUrl.b.a(HttpUrl.f2850l, str2, 0, 0, " \"':;<=>@[]^`{}|/\\?#&!$(),~", true, false, true, false, aVar.c, 83));
            } else {
                Intrinsics.a("value");
                throw null;
            }
        } else {
            FormBody.a aVar2 = this.f3181j;
            if (str == null) {
                Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
                throw null;
            } else if (str2 != null) {
                aVar2.a.add(HttpUrl.b.a(HttpUrl.f2850l, str, 0, 0, " \"':;<=>@[]^`{}|/\\?#&!$(),~", false, false, true, false, aVar2.c, 91));
                aVar2.b.add(HttpUrl.b.a(HttpUrl.f2850l, str2, 0, 0, " \"':;<=>@[]^`{}|/\\?#&!$(),~", false, false, true, false, aVar2.c, 91));
            } else {
                Intrinsics.a("value");
                throw null;
            }
        }
    }

    public void a(Headers headers, RequestBody requestBody) {
        MultipartBody.a aVar = this.f3180i;
        if (aVar == null) {
            throw null;
        } else if (requestBody != null) {
            aVar.c.add(MultipartBody.c.a(headers, requestBody));
        } else {
            Intrinsics.a("body");
            throw null;
        }
    }
}
