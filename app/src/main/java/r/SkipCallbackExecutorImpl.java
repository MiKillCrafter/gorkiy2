package r;

import j.a.a.a.outline;
import java.lang.annotation.Annotation;

public final class SkipCallbackExecutorImpl implements SkipCallbackExecutor {
    public static final SkipCallbackExecutor a = new SkipCallbackExecutorImpl();

    public static Annotation[] a(Annotation[] annotationArr) {
        if (Utils.a(annotationArr, SkipCallbackExecutor.class)) {
            return annotationArr;
        }
        Annotation[] annotationArr2 = new Annotation[(annotationArr.length + 1)];
        annotationArr2[0] = a;
        System.arraycopy(annotationArr, 0, annotationArr2, 1, annotationArr.length);
        return annotationArr2;
    }

    public Class<? extends Annotation> annotationType() {
        return SkipCallbackExecutor.class;
    }

    public boolean equals(Object obj) {
        return obj instanceof SkipCallbackExecutor;
    }

    public int hashCode() {
        return 0;
    }

    public String toString() {
        StringBuilder a2 = outline.a("@");
        a2.append(SkipCallbackExecutor.class.getName());
        a2.append("()");
        return a2.toString();
    }
}
