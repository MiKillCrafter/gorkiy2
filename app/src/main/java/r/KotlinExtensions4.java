package r;

import n.Unit;
import n.g;
import n.n.b.Functions0;
import n.n.c.j;

/* compiled from: KotlinExtensions.kt */
public final class KotlinExtensions4 extends j implements Functions0<Throwable, g> {
    public final /* synthetic */ Call c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public KotlinExtensions4(Call call) {
        super(1);
        this.c = call;
    }

    public Object a(Object obj) {
        Throwable th = (Throwable) obj;
        this.c.cancel();
        return Unit.a;
    }
}
