package r;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.concurrent.Executor;
import javax.annotation.Nullable;
import o.Request;
import r.CallAdapter;

public final class DefaultCallAdapterFactory extends CallAdapter.a {
    @Nullable
    public final Executor a;

    public class a implements CallAdapter<Object, Call<?>> {
        public final /* synthetic */ Type a;
        public final /* synthetic */ Executor b;

        public a(DefaultCallAdapterFactory defaultCallAdapterFactory, Type type, Executor executor) {
            this.a = type;
            this.b = executor;
        }

        public Type a() {
            return this.a;
        }

        public Object a(Call call) {
            Executor executor = this.b;
            return executor == null ? call : new b(executor, call);
        }
    }

    public static final class b<T> implements Call<T> {
        public final Executor b;
        public final Call<T> c;

        public class a implements Callback<T> {
            public final /* synthetic */ Callback b;

            /* renamed from: r.DefaultCallAdapterFactory$b$a$a  reason: collision with other inner class name */
            public class C0046a implements Runnable {
                public final /* synthetic */ Response b;

                public C0046a(Response response) {
                    this.b = response;
                }

                public void run() {
                    if (b.this.c.h()) {
                        a aVar = a.this;
                        aVar.b.a(b.this, new IOException("Canceled"));
                        return;
                    }
                    a aVar2 = a.this;
                    aVar2.b.a(b.this, this.b);
                }
            }

            /* renamed from: r.DefaultCallAdapterFactory$b$a$b  reason: collision with other inner class name */
            public class C0047b implements Runnable {
                public final /* synthetic */ Throwable b;

                public C0047b(Throwable th) {
                    this.b = th;
                }

                public void run() {
                    a aVar = a.this;
                    aVar.b.a(b.this, this.b);
                }
            }

            public a(Callback callback) {
                this.b = callback;
            }

            public void a(Call<T> call, Response<T> response) {
                b.this.b.execute(new C0046a(response));
            }

            public void a(Call<T> call, Throwable th) {
                b.this.b.execute(new C0047b(th));
            }
        }

        public b(Executor executor, Call<T> call) {
            this.b = executor;
            this.c = call;
        }

        public void a(Callback<T> callback) {
            Utils.a(callback, "callback == null");
            this.c.a(new a(callback));
        }

        public void cancel() {
            this.c.cancel();
        }

        public Request f() {
            return this.c.f();
        }

        public Response<T> g() {
            return this.c.g();
        }

        public boolean h() {
            return this.c.h();
        }

        public Call<T> clone() {
            return new b(this.b, this.c.clone());
        }
    }

    public DefaultCallAdapterFactory(@Nullable Executor executor) {
        this.a = executor;
    }

    @Nullable
    public CallAdapter<?, ?> a(Type type, Annotation[] annotationArr, e0 e0Var) {
        Executor executor = null;
        if (Utils.b(type) != Call.class) {
            return null;
        }
        if (type instanceof ParameterizedType) {
            Type a2 = Utils.a(0, (ParameterizedType) type);
            if (!Utils.a(annotationArr, SkipCallbackExecutor.class)) {
                executor = this.a;
            }
            return new a(this, a2, executor);
        }
        throw new IllegalArgumentException("Call return type must be parameterized as Call<Foo> or Call<? extends Foo>");
    }
}
