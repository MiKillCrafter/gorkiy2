package r.j0.a;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import javax.annotation.Nullable;
import l.b.Completable;
import l.b.Flowable;
import l.b.Maybe;
import l.b.Observable;
import l.b.Scheduler;
import l.b.Single;
import r.CallAdapter;
import r.Response;
import r.Utils;
import r.e0;

public final class RxJava2CallAdapterFactory extends CallAdapter.a {
    @Nullable
    public final Scheduler a;
    public final boolean b;

    public RxJava2CallAdapterFactory(@Nullable Scheduler scheduler, boolean z) {
        this.a = scheduler;
        this.b = z;
    }

    @Nullable
    public CallAdapter<?, ?> a(Type type, Annotation[] annotationArr, e0 e0Var) {
        boolean z;
        boolean z2;
        Type type2;
        Class<?> b2 = Utils.b(type);
        if (b2 == Completable.class) {
            return new RxJava2CallAdapter(Void.class, this.a, this.b, false, true, false, false, false, true);
        }
        boolean z3 = b2 == Flowable.class;
        boolean z4 = b2 == Single.class;
        boolean z5 = b2 == Maybe.class;
        if (b2 != Observable.class && !z3 && !z4 && !z5) {
            return null;
        }
        if (!(type instanceof ParameterizedType)) {
            String str = !z3 ? !z4 ? z5 ? "Maybe" : "Observable" : "Single" : "Flowable";
            throw new IllegalStateException(str + " return type must be parameterized as " + str + "<Foo> or " + str + "<? extends Foo>");
        }
        Type a2 = Utils.a(0, (ParameterizedType) type);
        Class<?> b3 = Utils.b(a2);
        if (b3 == Response.class) {
            if (a2 instanceof ParameterizedType) {
                type2 = Utils.a(0, (ParameterizedType) a2);
                z2 = false;
            } else {
                throw new IllegalStateException("Response must be parameterized as Response<Foo> or Response<? extends Foo>");
            }
        } else if (b3 != Result.class) {
            type2 = a2;
            z2 = false;
            z = true;
            return new RxJava2CallAdapter(type2, this.a, this.b, z2, z, z3, z4, z5, false);
        } else if (a2 instanceof ParameterizedType) {
            type2 = Utils.a(0, (ParameterizedType) a2);
            z2 = true;
        } else {
            throw new IllegalStateException("Result must be parameterized as Result<Foo> or Result<? extends Foo>");
        }
        z = false;
        return new RxJava2CallAdapter(type2, this.a, this.b, z2, z, z3, z4, z5, false);
    }
}
