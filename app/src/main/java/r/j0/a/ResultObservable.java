package r.j0.a;

import io.reactivex.exceptions.CompositeException;
import j.c.a.a.c.n.c;
import l.b.Observable;
import l.b.Observer;
import l.b.s.Disposable;
import r.Response;

public final class ResultObservable<T> extends Observable<Result<T>> {
    public final Observable<Response<T>> b;

    public static class a<R> implements Observer<Response<R>> {
        public final Observer<? super Result<R>> b;

        public a(Observer<? super Result<R>> observer) {
            this.b = observer;
        }

        public void a(Disposable disposable) {
            this.b.a(disposable);
        }

        public void b(Object obj) {
            Response response = (Response) obj;
            Observer<? super Result<R>> observer = this.b;
            if (response != null) {
                observer.b(new Result(response, null));
                return;
            }
            throw new NullPointerException("response == null");
        }

        public void a(Throwable th) {
            try {
                Observer<? super Result<R>> observer = this.b;
                if (th != null) {
                    observer.b(new Result(null, th));
                    this.b.a();
                    return;
                }
                throw new NullPointerException("error == null");
            } catch (Throwable th2) {
                c.c(th2);
                c.b((Throwable) new CompositeException(th, th2));
            }
        }

        public void a() {
            this.b.a();
        }
    }

    public ResultObservable(Observable<Response<T>> observable) {
        this.b = super;
    }

    public void b(Observer<? super Result<T>> observer) {
        this.b.a(new a(observer));
    }
}
