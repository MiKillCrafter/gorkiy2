package r;

import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;
import javax.annotation.Nullable;
import org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement;
import r.c;
import r.j;

public class Platform {
    public static final Platform a;

    @IgnoreJRERequirement
    public static class b extends Platform {
        public boolean a(Method method) {
            return method.isDefault();
        }

        public List<? extends j.a> b() {
            return Collections.singletonList(OptionalConverterFactory.a);
        }

        public int c() {
            return 1;
        }

        public Object a(Method method, Class<?> cls, Object obj, @Nullable Object... objArr) {
            Constructor<MethodHandles.Lookup> declaredConstructor = MethodHandles.Lookup.class.getDeclaredConstructor(Class.class, Integer.TYPE);
            declaredConstructor.setAccessible(true);
            return declaredConstructor.newInstance(cls, -1).unreflectSpecial(method, cls).bindTo(obj).invokeWithArguments(objArr);
        }

        public List<? extends c.a> a(@Nullable Executor executor) {
            ArrayList arrayList = new ArrayList(2);
            arrayList.add(CompletableFutureCallAdapterFactory2.a);
            arrayList.add(new DefaultCallAdapterFactory(executor));
            return Collections.unmodifiableList(arrayList);
        }
    }

    static {
        Platform platform;
        try {
            Class.forName("android.os.Build");
            platform = new a();
        } catch (ClassNotFoundException unused) {
            try {
                Class.forName("java.util.Optional");
                platform = new b();
            } catch (ClassNotFoundException unused2) {
                platform = new Platform();
            }
        }
        a = platform;
    }

    public List<? extends c.a> a(@Nullable Executor executor) {
        return Collections.singletonList(new DefaultCallAdapterFactory(executor));
    }

    @Nullable
    public Executor a() {
        return null;
    }

    public boolean a(Method method) {
        return false;
    }

    public List<? extends j.a> b() {
        return Collections.emptyList();
    }

    public int c() {
        return 0;
    }

    public static class a extends Platform {

        /* renamed from: r.Platform$a$a  reason: collision with other inner class name */
        public static class C0048a implements Executor {
            public final Handler a = new Handler(Looper.getMainLooper());

            public void execute(Runnable runnable) {
                this.a.post(runnable);
            }
        }

        @IgnoreJRERequirement
        public boolean a(Method method) {
            if (Build.VERSION.SDK_INT < 24) {
                return false;
            }
            return method.isDefault();
        }

        public List<? extends j.a> b() {
            if (Build.VERSION.SDK_INT >= 24) {
                return Collections.singletonList(OptionalConverterFactory.a);
            }
            return Collections.emptyList();
        }

        public int c() {
            return Build.VERSION.SDK_INT >= 24 ? 1 : 0;
        }

        public Executor a() {
            return new C0048a();
        }

        public List<? extends c.a> a(@Nullable Executor executor) {
            if (executor != null) {
                DefaultCallAdapterFactory defaultCallAdapterFactory = new DefaultCallAdapterFactory(executor);
                if (Build.VERSION.SDK_INT < 24) {
                    return Collections.singletonList(defaultCallAdapterFactory);
                }
                return Arrays.asList(CompletableFutureCallAdapterFactory2.a, defaultCallAdapterFactory);
            }
            throw new AssertionError();
        }
    }

    @Nullable
    public Object a(Method method, Class<?> cls, Object obj, @Nullable Object... objArr) {
        throw new UnsupportedOperationException();
    }
}
