package i.o;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import i.o.Lifecycle;

public class ReportFragment extends Fragment {
    public a b;

    public interface a {
    }

    public static ReportFragment a(Activity activity) {
        return (ReportFragment) activity.getFragmentManager().findFragmentByTag("androidx.lifecycle.LifecycleDispatcher.report_fragment_tag");
    }

    public static void b(Activity activity) {
        FragmentManager fragmentManager = activity.getFragmentManager();
        if (fragmentManager.findFragmentByTag("androidx.lifecycle.LifecycleDispatcher.report_fragment_tag") == null) {
            fragmentManager.beginTransaction().add(new ReportFragment(), "androidx.lifecycle.LifecycleDispatcher.report_fragment_tag").commit();
            fragmentManager.executePendingTransactions();
        }
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        a(Lifecycle.a.ON_CREATE);
    }

    public void onDestroy() {
        super.onDestroy();
        a(Lifecycle.a.ON_DESTROY);
        this.b = null;
    }

    public void onPause() {
        super.onPause();
        a(Lifecycle.a.ON_PAUSE);
    }

    public void onResume() {
        super.onResume();
        a aVar = this.b;
        if (aVar != null) {
            ProcessLifecycleOwner processLifecycleOwner = ProcessLifecycleOwner.this;
            int i2 = processLifecycleOwner.c + 1;
            processLifecycleOwner.c = i2;
            if (i2 == 1) {
                if (processLifecycleOwner.d) {
                    processLifecycleOwner.g.a(Lifecycle.a.ON_RESUME);
                    processLifecycleOwner.d = false;
                } else {
                    processLifecycleOwner.f1360f.removeCallbacks(processLifecycleOwner.h);
                }
            }
        }
        a(Lifecycle.a.ON_RESUME);
    }

    public void onStart() {
        super.onStart();
        a aVar = this.b;
        if (aVar != null) {
            ProcessLifecycleOwner processLifecycleOwner = ProcessLifecycleOwner.this;
            int i2 = processLifecycleOwner.b + 1;
            processLifecycleOwner.b = i2;
            if (i2 == 1 && processLifecycleOwner.f1359e) {
                processLifecycleOwner.g.a(Lifecycle.a.ON_START);
                processLifecycleOwner.f1359e = false;
            }
        }
        a(Lifecycle.a.ON_START);
    }

    public void onStop() {
        super.onStop();
        a(Lifecycle.a.ON_STOP);
    }

    public final void a(Lifecycle.a aVar) {
        Activity activity = getActivity();
        if (activity instanceof LifecycleRegistryOwner) {
            ((LifecycleRegistryOwner) activity).a().a(aVar);
        } else if (activity instanceof LifecycleOwner) {
            Lifecycle a2 = ((LifecycleOwner) activity).a();
            if (a2 instanceof LifecycleRegistry) {
                ((LifecycleRegistry) a2).a(aVar);
            }
        }
    }
}
