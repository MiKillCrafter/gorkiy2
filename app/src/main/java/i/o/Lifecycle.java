package i.o;

import java.util.concurrent.atomic.AtomicReference;

public abstract class Lifecycle {

    public enum a {
        ON_CREATE,
        ON_START,
        ON_RESUME,
        ON_PAUSE,
        ON_STOP,
        ON_DESTROY,
        ON_ANY
    }

    public enum b {
        DESTROYED,
        INITIALIZED,
        CREATED,
        STARTED,
        RESUMED;

        public boolean a(b bVar) {
            return compareTo(super) >= 0;
        }
    }

    public Lifecycle() {
        new AtomicReference();
    }

    public abstract void a(LifecycleObserver lifecycleObserver);
}
