package i.r;

public final class a {
    public static final int alpha = 2130903084;
    public static final int fastScrollEnabled = 2130903346;
    public static final int fastScrollHorizontalThumbDrawable = 2130903347;
    public static final int fastScrollHorizontalTrackDrawable = 2130903348;
    public static final int fastScrollVerticalThumbDrawable = 2130903349;
    public static final int fastScrollVerticalTrackDrawable = 2130903350;
    public static final int font = 2130903353;
    public static final int fontProviderAuthority = 2130903355;
    public static final int fontProviderCerts = 2130903356;
    public static final int fontProviderFetchStrategy = 2130903357;
    public static final int fontProviderFetchTimeout = 2130903358;
    public static final int fontProviderPackage = 2130903359;
    public static final int fontProviderQuery = 2130903360;
    public static final int fontStyle = 2130903361;
    public static final int fontVariationSettings = 2130903362;
    public static final int fontWeight = 2130903363;
    public static final int layoutManager = 2130903438;
    public static final int recyclerViewStyle = 2130903609;
    public static final int reverseLayout = 2130903610;
    public static final int spanCount = 2130903647;
    public static final int stackFromEnd = 2130903653;
    public static final int ttcIndex = 2130903790;
}
