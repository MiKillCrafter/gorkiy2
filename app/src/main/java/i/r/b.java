package i.r;

public final class b {
    public static final int compat_button_inset_horizontal_material = 2131099738;
    public static final int compat_button_inset_vertical_material = 2131099739;
    public static final int compat_button_padding_horizontal_material = 2131099740;
    public static final int compat_button_padding_vertical_material = 2131099741;
    public static final int compat_control_corner_material = 2131099742;
    public static final int compat_notification_large_icon_max_height = 2131099743;
    public static final int compat_notification_large_icon_max_width = 2131099744;
    public static final int fastscroll_default_thickness = 2131099800;
    public static final int fastscroll_margin = 2131099801;
    public static final int fastscroll_minimum_range = 2131099802;
    public static final int item_touch_helper_max_drag_scroll_per_frame = 2131099817;
    public static final int item_touch_helper_swipe_escape_max_velocity = 2131099818;
    public static final int item_touch_helper_swipe_escape_velocity = 2131099819;
    public static final int notification_action_icon_size = 2131099988;
    public static final int notification_action_text_size = 2131099989;
    public static final int notification_big_circle_margin = 2131099990;
    public static final int notification_content_margin_start = 2131099991;
    public static final int notification_large_icon_height = 2131099992;
    public static final int notification_large_icon_width = 2131099993;
    public static final int notification_main_column_padding_top = 2131099994;
    public static final int notification_media_narrow_margin = 2131099995;
    public static final int notification_right_icon_size = 2131099996;
    public static final int notification_right_side_padding_top = 2131099997;
    public static final int notification_small_icon_background_padding = 2131099998;
    public static final int notification_small_icon_size_as_large = 2131099999;
    public static final int notification_subtext_size = 2131100000;
    public static final int notification_top_pad = 2131100001;
    public static final int notification_top_pad_large_text = 2131100002;
}
