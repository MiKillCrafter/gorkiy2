package i.r.d;

import android.annotation.SuppressLint;
import android.os.Trace;
import androidx.recyclerview.widget.RecyclerView;
import i.h.h.TraceCompat;
import i.r.d.m;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.TimeUnit;

public final class GapWorker implements Runnable {

    /* renamed from: f  reason: collision with root package name */
    public static final ThreadLocal<m> f1396f = new ThreadLocal<>();
    public static Comparator<m.c> g = new a();
    public ArrayList<RecyclerView> b = new ArrayList<>();
    public long c;
    public long d;

    /* renamed from: e  reason: collision with root package name */
    public ArrayList<m.c> f1397e = new ArrayList<>();

    public static class a implements Comparator<m.c> {
        /* JADX WARNING: Code restructure failed: missing block: B:11:0x0019, code lost:
            if (r6.d == null) goto L_0x0025;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0022, code lost:
            if (r0 != false) goto L_0x0024;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public int compare(java.lang.Object r6, java.lang.Object r7) {
            /*
                r5 = this;
                i.r.d.GapWorker$c r6 = (i.r.d.GapWorker.c) r6
                i.r.d.GapWorker$c r7 = (i.r.d.GapWorker.c) r7
                androidx.recyclerview.widget.RecyclerView r0 = r6.d
                r1 = 1
                r2 = 0
                if (r0 != 0) goto L_0x000c
                r0 = 1
                goto L_0x000d
            L_0x000c:
                r0 = 0
            L_0x000d:
                androidx.recyclerview.widget.RecyclerView r3 = r7.d
                if (r3 != 0) goto L_0x0013
                r3 = 1
                goto L_0x0014
            L_0x0013:
                r3 = 0
            L_0x0014:
                r4 = -1
                if (r0 == r3) goto L_0x001c
                androidx.recyclerview.widget.RecyclerView r6 = r6.d
                if (r6 != 0) goto L_0x0024
                goto L_0x0025
            L_0x001c:
                boolean r0 = r6.a
                boolean r3 = r7.a
                if (r0 == r3) goto L_0x0027
                if (r0 == 0) goto L_0x0025
            L_0x0024:
                r1 = -1
            L_0x0025:
                r2 = r1
                goto L_0x0038
            L_0x0027:
                int r0 = r7.b
                int r1 = r6.b
                int r0 = r0 - r1
                if (r0 == 0) goto L_0x0030
                r2 = r0
                goto L_0x0038
            L_0x0030:
                int r6 = r6.c
                int r7 = r7.c
                int r6 = r6 - r7
                if (r6 == 0) goto L_0x0038
                r2 = r6
            L_0x0038:
                return r2
            */
            throw new UnsupportedOperationException("Method not decompiled: i.r.d.GapWorker.a.compare(java.lang.Object, java.lang.Object):int");
        }
    }

    public static class c {
        public boolean a;
        public int b;
        public int c;
        public RecyclerView d;

        /* renamed from: e  reason: collision with root package name */
        public int f1398e;
    }

    public void a(RecyclerView recyclerView, int i2, int i3) {
        if (recyclerView.isAttachedToWindow() && this.c == 0) {
            this.c = recyclerView.getNanoTime();
            recyclerView.post(this);
        }
        b bVar = recyclerView.mPrefetchRegistry;
        bVar.a = i2;
        bVar.b = i3;
    }

    public void run() {
        try {
            TraceCompat.a(RecyclerView.TRACE_PREFETCH_TAG);
            if (this.b.isEmpty()) {
                this.c = 0;
                Trace.endSection();
                return;
            }
            int size = this.b.size();
            long j2 = 0;
            for (int i2 = 0; i2 < size; i2++) {
                RecyclerView recyclerView = this.b.get(i2);
                if (recyclerView.getWindowVisibility() == 0) {
                    j2 = Math.max(recyclerView.getDrawingTime(), j2);
                }
            }
            if (j2 == 0) {
                this.c = 0;
                Trace.endSection();
                return;
            }
            a(TimeUnit.MILLISECONDS.toNanos(j2) + this.d);
            this.c = 0;
            Trace.endSection();
        } catch (Throwable th) {
            this.c = 0;
            TraceCompat.a();
            throw th;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.r.d.GapWorker.b.a(androidx.recyclerview.widget.RecyclerView, boolean):void
     arg types: [androidx.recyclerview.widget.RecyclerView, int]
     candidates:
      i.r.d.GapWorker.b.a(int, int):void
      i.r.d.GapWorker.b.a(androidx.recyclerview.widget.RecyclerView, boolean):void */
    public void a(long j2) {
        RecyclerView recyclerView;
        c cVar;
        int size = this.b.size();
        int i2 = 0;
        for (int i3 = 0; i3 < size; i3++) {
            RecyclerView recyclerView2 = this.b.get(i3);
            if (recyclerView2.getWindowVisibility() == 0) {
                recyclerView2.mPrefetchRegistry.a(recyclerView2, false);
                i2 += recyclerView2.mPrefetchRegistry.d;
            }
        }
        this.f1397e.ensureCapacity(i2);
        int i4 = 0;
        for (int i5 = 0; i5 < size; i5++) {
            RecyclerView recyclerView3 = this.b.get(i5);
            if (recyclerView3.getWindowVisibility() == 0) {
                b bVar = recyclerView3.mPrefetchRegistry;
                int abs = Math.abs(bVar.b) + Math.abs(bVar.a);
                for (int i6 = 0; i6 < bVar.d * 2; i6 += 2) {
                    if (i4 >= this.f1397e.size()) {
                        cVar = new c();
                        this.f1397e.add(cVar);
                    } else {
                        cVar = (c) this.f1397e.get(i4);
                    }
                    int i7 = bVar.c[i6 + 1];
                    cVar.a = i7 <= abs;
                    cVar.b = abs;
                    cVar.c = i7;
                    cVar.d = recyclerView3;
                    cVar.f1398e = bVar.c[i6];
                    i4++;
                }
            }
        }
        Collections.sort(this.f1397e, g);
        int i8 = 0;
        while (i8 < this.f1397e.size()) {
            c cVar2 = this.f1397e.get(i8);
            if (cVar2.d != null) {
                RecyclerView.d0 a2 = a(cVar2.d, cVar2.f1398e, cVar2.a ? RecyclerView.FOREVER_NS : j2);
                if (!(a2 == null || a2.b == null || !a2.g() || a2.h() || (recyclerView = a2.b.get()) == null)) {
                    if (recyclerView.mDataSetHasChangedAfterLayout && recyclerView.mChildHelper.b() != 0) {
                        recyclerView.removeAndRecycleViews();
                    }
                    b bVar2 = recyclerView.mPrefetchRegistry;
                    bVar2.a(recyclerView, true);
                    if (bVar2.d != 0) {
                        try {
                            TraceCompat.a(RecyclerView.TRACE_NESTED_PREFETCH_TAG);
                            RecyclerView.a0 a0Var = recyclerView.mState;
                            RecyclerView.g gVar = recyclerView.mAdapter;
                            a0Var.d = 1;
                            a0Var.f290e = gVar.a();
                            a0Var.g = false;
                            a0Var.h = false;
                            a0Var.f292i = false;
                            for (int i9 = 0; i9 < bVar2.d * 2; i9 += 2) {
                                a(recyclerView, bVar2.c[i9], j2);
                            }
                            Trace.endSection();
                        } catch (Throwable th) {
                            TraceCompat.a();
                            throw th;
                        }
                    }
                }
                cVar2.a = false;
                cVar2.b = 0;
                cVar2.c = 0;
                cVar2.d = null;
                cVar2.f1398e = 0;
                i8++;
            } else {
                return;
            }
        }
    }

    @SuppressLint({"VisibleForTests"})
    public static class b implements RecyclerView.o.c {
        public int a;
        public int b;
        public int[] c;
        public int d;

        public void a(RecyclerView recyclerView, boolean z) {
            this.d = 0;
            int[] iArr = this.c;
            if (iArr != null) {
                Arrays.fill(iArr, -1);
            }
            RecyclerView.o oVar = recyclerView.mLayout;
            if (recyclerView.mAdapter != null && oVar != null && oVar.f321l) {
                if (z) {
                    if (!recyclerView.mAdapterHelper.c()) {
                        oVar.a(recyclerView.mAdapter.a(), this);
                    }
                } else if (!recyclerView.hasPendingAdapterUpdates()) {
                    oVar.a(this.a, this.b, recyclerView.mState, this);
                }
                int i2 = this.d;
                if (i2 > oVar.f322m) {
                    oVar.f322m = i2;
                    oVar.f323n = z;
                    recyclerView.mRecycler.d();
                }
            }
        }

        public void a(int i2, int i3) {
            if (i2 < 0) {
                throw new IllegalArgumentException("Layout positions must be non-negative");
            } else if (i3 >= 0) {
                int i4 = this.d * 2;
                int[] iArr = this.c;
                if (iArr == null) {
                    int[] iArr2 = new int[4];
                    this.c = iArr2;
                    Arrays.fill(iArr2, -1);
                } else if (i4 >= iArr.length) {
                    int[] iArr3 = new int[(i4 * 2)];
                    this.c = iArr3;
                    System.arraycopy(iArr, 0, iArr3, 0, iArr.length);
                }
                int[] iArr4 = this.c;
                iArr4[i4] = i2;
                iArr4[i4 + 1] = i3;
                this.d++;
            } else {
                throw new IllegalArgumentException("Pixel distance must be non-negative");
            }
        }

        public boolean a(int i2) {
            if (this.c != null) {
                int i3 = this.d * 2;
                for (int i4 = 0; i4 < i3; i4 += 2) {
                    if (this.c[i4] == i2) {
                        return true;
                    }
                }
            }
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.RecyclerView.v.a(androidx.recyclerview.widget.RecyclerView$d0, boolean):void
     arg types: [androidx.recyclerview.widget.RecyclerView$d0, int]
     candidates:
      androidx.recyclerview.widget.RecyclerView.v.a(android.view.ViewGroup, boolean):void
      androidx.recyclerview.widget.RecyclerView.v.a(androidx.recyclerview.widget.RecyclerView$d0, boolean):void */
    public final RecyclerView.d0 a(RecyclerView recyclerView, int i2, long j2) {
        boolean z;
        int b2 = recyclerView.mChildHelper.b();
        int i3 = 0;
        while (true) {
            if (i3 >= b2) {
                z = false;
                break;
            }
            RecyclerView.d0 childViewHolderInt = RecyclerView.getChildViewHolderInt(recyclerView.mChildHelper.d(i3));
            if (childViewHolderInt.c == i2 && !childViewHolderInt.h()) {
                z = true;
                break;
            }
            i3++;
        }
        if (z) {
            return null;
        }
        RecyclerView.v vVar = recyclerView.mRecycler;
        try {
            recyclerView.onEnterLayoutOrScroll();
            RecyclerView.d0 a2 = vVar.a(i2, false, j2);
            if (a2 != null) {
                if (!a2.g() || a2.h()) {
                    vVar.a(a2, false);
                } else {
                    vVar.a(a2.a);
                }
            }
            return a2;
        } finally {
            recyclerView.onExitLayoutOrScroll(false);
        }
    }
}
