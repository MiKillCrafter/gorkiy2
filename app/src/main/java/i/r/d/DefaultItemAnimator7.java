package i.r.d;

import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import i.r.d.k;
import j.a.a.a.outline;
import java.util.ArrayList;
import java.util.List;

/* compiled from: DefaultItemAnimator */
public class DefaultItemAnimator7 extends SimpleItemAnimator {

    /* renamed from: s  reason: collision with root package name */
    public static TimeInterpolator f1368s;
    public ArrayList<RecyclerView.d0> h = new ArrayList<>();

    /* renamed from: i  reason: collision with root package name */
    public ArrayList<RecyclerView.d0> f1369i = new ArrayList<>();

    /* renamed from: j  reason: collision with root package name */
    public ArrayList<k.b> f1370j = new ArrayList<>();

    /* renamed from: k  reason: collision with root package name */
    public ArrayList<k.a> f1371k = new ArrayList<>();

    /* renamed from: l  reason: collision with root package name */
    public ArrayList<ArrayList<RecyclerView.d0>> f1372l = new ArrayList<>();

    /* renamed from: m  reason: collision with root package name */
    public ArrayList<ArrayList<k.b>> f1373m = new ArrayList<>();

    /* renamed from: n  reason: collision with root package name */
    public ArrayList<ArrayList<k.a>> f1374n = new ArrayList<>();

    /* renamed from: o  reason: collision with root package name */
    public ArrayList<RecyclerView.d0> f1375o = new ArrayList<>();

    /* renamed from: p  reason: collision with root package name */
    public ArrayList<RecyclerView.d0> f1376p = new ArrayList<>();

    /* renamed from: q  reason: collision with root package name */
    public ArrayList<RecyclerView.d0> f1377q = new ArrayList<>();

    /* renamed from: r  reason: collision with root package name */
    public ArrayList<RecyclerView.d0> f1378r = new ArrayList<>();

    /* compiled from: DefaultItemAnimator */
    public static class a {
        public RecyclerView.d0 a;
        public RecyclerView.d0 b;
        public int c;
        public int d;

        /* renamed from: e  reason: collision with root package name */
        public int f1379e;

        /* renamed from: f  reason: collision with root package name */
        public int f1380f;

        public a(RecyclerView.d0 d0Var, RecyclerView.d0 d0Var2, int i2, int i3, int i4, int i5) {
            this.a = d0Var;
            this.b = d0Var2;
            this.c = i2;
            this.d = i3;
            this.f1379e = i4;
            this.f1380f = i5;
        }

        public String toString() {
            StringBuilder a2 = outline.a("ChangeInfo{oldHolder=");
            a2.append(this.a);
            a2.append(", newHolder=");
            a2.append(this.b);
            a2.append(", fromX=");
            a2.append(this.c);
            a2.append(", fromY=");
            a2.append(this.d);
            a2.append(", toX=");
            a2.append(this.f1379e);
            a2.append(", toY=");
            a2.append(this.f1380f);
            a2.append('}');
            return a2.toString();
        }
    }

    /* compiled from: DefaultItemAnimator */
    public static class b {
        public RecyclerView.d0 a;
        public int b;
        public int c;
        public int d;

        /* renamed from: e  reason: collision with root package name */
        public int f1381e;

        public b(RecyclerView.d0 d0Var, int i2, int i3, int i4, int i5) {
            this.a = d0Var;
            this.b = i2;
            this.c = i3;
            this.d = i4;
            this.f1381e = i5;
        }
    }

    public boolean a(RecyclerView.d0 d0Var, int i2, int i3, int i4, int i5) {
        View view = d0Var.a;
        int translationX = i2 + ((int) view.getTranslationX());
        int translationY = i3 + ((int) d0Var.a.getTranslationY());
        e(d0Var);
        int i6 = i4 - translationX;
        int i7 = i5 - translationY;
        if (i6 == 0 && i7 == 0) {
            a(d0Var);
            return false;
        }
        if (i6 != 0) {
            view.setTranslationX((float) (-i6));
        }
        if (i7 != 0) {
            view.setTranslationY((float) (-i7));
        }
        this.f1370j.add(new b(d0Var, translationX, translationY, i4, i5));
        return true;
    }

    public void b(RecyclerView.d0 d0Var) {
        View view = d0Var.a;
        view.animate().cancel();
        int size = this.f1370j.size();
        while (true) {
            size--;
            if (size < 0) {
                break;
            } else if (this.f1370j.get(size).a == d0Var) {
                view.setTranslationY(0.0f);
                view.setTranslationX(0.0f);
                a(d0Var);
                this.f1370j.remove(size);
            }
        }
        a(this.f1371k, d0Var);
        if (this.h.remove(d0Var)) {
            view.setAlpha(1.0f);
            a(d0Var);
        }
        if (this.f1369i.remove(d0Var)) {
            view.setAlpha(1.0f);
            a(d0Var);
        }
        for (int size2 = this.f1374n.size() - 1; size2 >= 0; size2--) {
            ArrayList arrayList = this.f1374n.get(size2);
            a(arrayList, d0Var);
            if (arrayList.isEmpty()) {
                this.f1374n.remove(size2);
            }
        }
        for (int size3 = this.f1373m.size() - 1; size3 >= 0; size3--) {
            ArrayList arrayList2 = this.f1373m.get(size3);
            int size4 = arrayList2.size() - 1;
            while (true) {
                if (size4 < 0) {
                    break;
                } else if (((b) arrayList2.get(size4)).a == d0Var) {
                    view.setTranslationY(0.0f);
                    view.setTranslationX(0.0f);
                    a(d0Var);
                    arrayList2.remove(size4);
                    if (arrayList2.isEmpty()) {
                        this.f1373m.remove(size3);
                    }
                } else {
                    size4--;
                }
            }
        }
        for (int size5 = this.f1372l.size() - 1; size5 >= 0; size5--) {
            ArrayList arrayList3 = this.f1372l.get(size5);
            if (arrayList3.remove(d0Var)) {
                view.setAlpha(1.0f);
                a(d0Var);
                if (arrayList3.isEmpty()) {
                    this.f1372l.remove(size5);
                }
            }
        }
        this.f1377q.remove(d0Var);
        this.f1375o.remove(d0Var);
        this.f1378r.remove(d0Var);
        this.f1376p.remove(d0Var);
        d();
    }

    public boolean c() {
        return !this.f1369i.isEmpty() || !this.f1371k.isEmpty() || !this.f1370j.isEmpty() || !this.h.isEmpty() || !this.f1376p.isEmpty() || !this.f1377q.isEmpty() || !this.f1375o.isEmpty() || !this.f1378r.isEmpty() || !this.f1373m.isEmpty() || !this.f1372l.isEmpty() || !this.f1374n.isEmpty();
    }

    public void d() {
        if (!c()) {
            a();
        }
    }

    public final void e(RecyclerView.d0 d0Var) {
        if (f1368s == null) {
            f1368s = new ValueAnimator().getInterpolator();
        }
        d0Var.a.animate().setInterpolator(f1368s);
        b(d0Var);
    }

    public final void a(List<k.a> list, RecyclerView.d0 d0Var) {
        for (int size = list.size() - 1; size >= 0; size--) {
            a aVar = list.get(size);
            if (a(aVar, d0Var) && aVar.a == null && aVar.b == null) {
                list.remove(aVar);
            }
        }
    }

    public final boolean a(a aVar, RecyclerView.d0 d0Var) {
        if (aVar.b == d0Var) {
            aVar.b = null;
        } else if (aVar.a != d0Var) {
            return false;
        } else {
            aVar.a = null;
        }
        d0Var.a.setAlpha(1.0f);
        d0Var.a.setTranslationX(0.0f);
        d0Var.a.setTranslationY(0.0f);
        a(d0Var);
        return true;
    }

    public void a(List<RecyclerView.d0> list) {
        for (int size = list.size() - 1; size >= 0; size--) {
            list.get(size).a.animate().cancel();
        }
    }

    public boolean a(RecyclerView.d0 d0Var, List<Object> list) {
        return !list.isEmpty() || super.a(d0Var, list);
    }

    public void b() {
        int size = this.f1370j.size();
        while (true) {
            size--;
            if (size < 0) {
                break;
            }
            b bVar = this.f1370j.get(size);
            View view = bVar.a.a;
            view.setTranslationY(0.0f);
            view.setTranslationX(0.0f);
            a(bVar.a);
            this.f1370j.remove(size);
        }
        int size2 = this.h.size();
        while (true) {
            size2--;
            if (size2 < 0) {
                break;
            }
            a(this.h.get(size2));
            this.h.remove(size2);
        }
        int size3 = this.f1369i.size();
        while (true) {
            size3--;
            if (size3 < 0) {
                break;
            }
            RecyclerView.d0 d0Var = this.f1369i.get(size3);
            d0Var.a.setAlpha(1.0f);
            a(d0Var);
            this.f1369i.remove(size3);
        }
        int size4 = this.f1371k.size();
        while (true) {
            size4--;
            if (size4 < 0) {
                break;
            }
            a aVar = this.f1371k.get(size4);
            RecyclerView.d0 d0Var2 = aVar.a;
            if (d0Var2 != null) {
                a(aVar, d0Var2);
            }
            RecyclerView.d0 d0Var3 = aVar.b;
            if (d0Var3 != null) {
                a(aVar, d0Var3);
            }
        }
        this.f1371k.clear();
        if (c()) {
            int size5 = this.f1373m.size();
            while (true) {
                size5--;
                if (size5 < 0) {
                    break;
                }
                ArrayList arrayList = this.f1373m.get(size5);
                int size6 = arrayList.size();
                while (true) {
                    size6--;
                    if (size6 >= 0) {
                        b bVar2 = (b) arrayList.get(size6);
                        View view2 = bVar2.a.a;
                        view2.setTranslationY(0.0f);
                        view2.setTranslationX(0.0f);
                        a(bVar2.a);
                        arrayList.remove(size6);
                        if (arrayList.isEmpty()) {
                            this.f1373m.remove(arrayList);
                        }
                    }
                }
            }
            int size7 = this.f1372l.size();
            while (true) {
                size7--;
                if (size7 < 0) {
                    break;
                }
                ArrayList arrayList2 = this.f1372l.get(size7);
                int size8 = arrayList2.size();
                while (true) {
                    size8--;
                    if (size8 >= 0) {
                        RecyclerView.d0 d0Var4 = (RecyclerView.d0) arrayList2.get(size8);
                        d0Var4.a.setAlpha(1.0f);
                        a(d0Var4);
                        arrayList2.remove(size8);
                        if (arrayList2.isEmpty()) {
                            this.f1372l.remove(arrayList2);
                        }
                    }
                }
            }
            int size9 = this.f1374n.size();
            while (true) {
                size9--;
                if (size9 >= 0) {
                    ArrayList arrayList3 = this.f1374n.get(size9);
                    int size10 = arrayList3.size();
                    while (true) {
                        size10--;
                        if (size10 >= 0) {
                            a aVar2 = (a) arrayList3.get(size10);
                            RecyclerView.d0 d0Var5 = aVar2.a;
                            if (d0Var5 != null) {
                                a(aVar2, d0Var5);
                            }
                            RecyclerView.d0 d0Var6 = aVar2.b;
                            if (d0Var6 != null) {
                                a(aVar2, d0Var6);
                            }
                            if (arrayList3.isEmpty()) {
                                this.f1374n.remove(arrayList3);
                            }
                        }
                    }
                } else {
                    a(this.f1377q);
                    a(this.f1376p);
                    a(this.f1375o);
                    a(this.f1378r);
                    a();
                    return;
                }
            }
        }
    }
}
