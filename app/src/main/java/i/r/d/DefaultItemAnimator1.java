package i.r.d;

import android.view.View;
import android.view.ViewPropertyAnimator;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: DefaultItemAnimator */
public class DefaultItemAnimator1 implements Runnable {
    public final /* synthetic */ ArrayList b;
    public final /* synthetic */ DefaultItemAnimator7 c;

    public DefaultItemAnimator1(DefaultItemAnimator7 defaultItemAnimator7, ArrayList arrayList) {
        this.c = defaultItemAnimator7;
        this.b = arrayList;
    }

    public void run() {
        Iterator it = this.b.iterator();
        while (it.hasNext()) {
            RecyclerView.d0 d0Var = (RecyclerView.d0) it.next();
            DefaultItemAnimator7 defaultItemAnimator7 = this.c;
            if (defaultItemAnimator7 != null) {
                View view = d0Var.a;
                ViewPropertyAnimator animate = view.animate();
                defaultItemAnimator7.f1375o.add(d0Var);
                animate.alpha(1.0f).setDuration(defaultItemAnimator7.c).setListener(new DefaultItemAnimator3(defaultItemAnimator7, d0Var, view, animate)).start();
            } else {
                throw null;
            }
        }
        this.b.clear();
        this.c.f1372l.remove(this.b);
    }
}
