package i.r.d;

import android.view.View;
import android.view.ViewPropertyAnimator;
import androidx.recyclerview.widget.RecyclerView;
import i.r.d.DefaultItemAnimator7;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: DefaultItemAnimator */
public class DefaultItemAnimator0 implements Runnable {
    public final /* synthetic */ ArrayList b;
    public final /* synthetic */ DefaultItemAnimator7 c;

    public DefaultItemAnimator0(DefaultItemAnimator7 defaultItemAnimator7, ArrayList arrayList) {
        this.c = defaultItemAnimator7;
        this.b = arrayList;
    }

    public void run() {
        View view;
        Iterator it = this.b.iterator();
        while (it.hasNext()) {
            DefaultItemAnimator7.a aVar = (DefaultItemAnimator7.a) it.next();
            DefaultItemAnimator7 defaultItemAnimator7 = this.c;
            View view2 = null;
            if (defaultItemAnimator7 != null) {
                RecyclerView.d0 d0Var = aVar.a;
                if (d0Var == null) {
                    view = null;
                } else {
                    view = d0Var.a;
                }
                RecyclerView.d0 d0Var2 = aVar.b;
                if (d0Var2 != null) {
                    view2 = d0Var2.a;
                }
                if (view != null) {
                    ViewPropertyAnimator duration = view.animate().setDuration(defaultItemAnimator7.f315f);
                    defaultItemAnimator7.f1378r.add(aVar.a);
                    duration.translationX((float) (aVar.f1379e - aVar.c));
                    duration.translationY((float) (aVar.f1380f - aVar.d));
                    duration.alpha(0.0f).setListener(new DefaultItemAnimator5(defaultItemAnimator7, aVar, duration, view)).start();
                }
                if (view2 != null) {
                    ViewPropertyAnimator animate = view2.animate();
                    defaultItemAnimator7.f1378r.add(aVar.b);
                    animate.translationX(0.0f).translationY(0.0f).setDuration(defaultItemAnimator7.f315f).alpha(1.0f).setListener(new DefaultItemAnimator6(defaultItemAnimator7, aVar, animate, view2)).start();
                }
            } else {
                throw null;
            }
        }
        this.b.clear();
        this.c.f1374n.remove(this.b);
    }
}
