package i.w;

import android.annotation.SuppressLint;
import android.view.View;

public class ViewUtilsApi19 extends ViewUtilsBase {

    /* renamed from: e  reason: collision with root package name */
    public static boolean f1467e = true;

    public void a(View view) {
    }

    @SuppressLint({"NewApi"})
    public void a(View view, float f2) {
        if (f1467e) {
            try {
                view.setTransitionAlpha(f2);
                return;
            } catch (NoSuchMethodError unused) {
                f1467e = false;
            }
        }
        view.setAlpha(f2);
    }

    @SuppressLint({"NewApi"})
    public float b(View view) {
        if (f1467e) {
            try {
                return view.getTransitionAlpha();
            } catch (NoSuchMethodError unused) {
                f1467e = false;
            }
        }
        return view.getAlpha();
    }

    public void c(View view) {
    }
}
