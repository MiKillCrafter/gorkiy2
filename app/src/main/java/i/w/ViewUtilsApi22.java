package i.w;

import android.annotation.SuppressLint;
import android.view.View;

public class ViewUtilsApi22 extends ViewUtilsApi21 {
    public static boolean h = true;

    @SuppressLint({"NewApi"})
    public void a(View view, int i2, int i3, int i4, int i5) {
        if (h) {
            try {
                view.setLeftTopRightBottom(i2, i3, i4, i5);
            } catch (NoSuchMethodError unused) {
                h = false;
            }
        }
    }
}
