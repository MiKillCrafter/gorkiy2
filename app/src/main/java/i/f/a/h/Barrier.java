package i.f.a.h;

import i.f.a.ArrayRow;
import i.f.a.LinearSystem;
import i.f.a.SolverVariable;
import i.f.a.h.ConstraintWidget;
import java.util.ArrayList;

public class Barrier extends Helper {
    public int m0 = 0;
    public ArrayList<j> n0 = new ArrayList<>(4);
    public boolean o0 = true;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.f.a.h.ResolutionAnchor.a(i.f.a.h.ResolutionAnchor, float):void
     arg types: [?[OBJECT, ARRAY], int]
     candidates:
      i.f.a.h.ResolutionAnchor.a(i.f.a.h.ResolutionAnchor, int):void
      i.f.a.h.ResolutionAnchor.a(i.f.a.h.ResolutionAnchor, float):void */
    public void a(int i2) {
        ResolutionAnchor resolutionAnchor;
        ResolutionAnchor resolutionAnchor2;
        ConstraintWidget constraintWidget = this.D;
        if (constraintWidget != null && ((ConstraintWidgetContainer) constraintWidget).g(2)) {
            int i3 = this.m0;
            if (i3 == 0) {
                resolutionAnchor = this.f1121s.a;
            } else if (i3 == 1) {
                resolutionAnchor = this.u.a;
            } else if (i3 == 2) {
                resolutionAnchor = this.f1122t.a;
            } else if (i3 == 3) {
                resolutionAnchor = this.v.a;
            } else {
                return;
            }
            resolutionAnchor.h = 5;
            int i4 = this.m0;
            if (i4 == 0 || i4 == 1) {
                this.f1122t.a.a((ResolutionAnchor) null, 0.0f);
                this.v.a.a((ResolutionAnchor) null, 0.0f);
            } else {
                this.f1121s.a.a((ResolutionAnchor) null, 0.0f);
                this.u.a.a((ResolutionAnchor) null, 0.0f);
            }
            this.n0.clear();
            for (int i5 = 0; i5 < super.l0; i5++) {
                ConstraintWidget constraintWidget2 = super.k0[i5];
                if (this.o0 || constraintWidget2.a()) {
                    int i6 = this.m0;
                    if (i6 == 0) {
                        resolutionAnchor2 = constraintWidget2.f1121s.a;
                    } else if (i6 == 1) {
                        resolutionAnchor2 = constraintWidget2.u.a;
                    } else if (i6 == 2) {
                        resolutionAnchor2 = constraintWidget2.f1122t.a;
                    } else if (i6 != 3) {
                        resolutionAnchor2 = null;
                    } else {
                        resolutionAnchor2 = constraintWidget2.v.a;
                    }
                    if (resolutionAnchor2 != null) {
                        this.n0.add(resolutionAnchor2);
                        resolutionAnchor2.a.add(resolutionAnchor);
                    }
                }
            }
        }
    }

    public boolean a() {
        return true;
    }

    public void l() {
        super.l();
        this.n0.clear();
    }

    public void m() {
        ResolutionAnchor resolutionAnchor;
        float f2;
        ResolutionAnchor resolutionAnchor2;
        int i2 = this.m0;
        float f3 = Float.MAX_VALUE;
        if (i2 != 0) {
            if (i2 == 1) {
                resolutionAnchor = this.u.a;
            } else if (i2 == 2) {
                resolutionAnchor = this.f1122t.a;
            } else if (i2 == 3) {
                resolutionAnchor = this.v.a;
            } else {
                return;
            }
            f3 = 0.0f;
        } else {
            resolutionAnchor = this.f1121s.a;
        }
        int size = this.n0.size();
        ResolutionAnchor resolutionAnchor3 = null;
        int i3 = 0;
        while (i3 < size) {
            ResolutionAnchor resolutionAnchor4 = this.n0.get(i3);
            if (resolutionAnchor4.b == 1) {
                int i4 = this.m0;
                if (i4 == 0 || i4 == 2) {
                    f2 = resolutionAnchor4.g;
                    if (f2 < f3) {
                        resolutionAnchor2 = resolutionAnchor4.f1129f;
                    } else {
                        i3++;
                    }
                } else {
                    f2 = resolutionAnchor4.g;
                    if (f2 > f3) {
                        resolutionAnchor2 = resolutionAnchor4.f1129f;
                    } else {
                        i3++;
                    }
                }
                resolutionAnchor3 = resolutionAnchor2;
                f3 = f2;
                i3++;
            } else {
                return;
            }
        }
        resolutionAnchor.f1129f = resolutionAnchor3;
        resolutionAnchor.g = f3;
        resolutionAnchor.a();
        int i5 = this.m0;
        if (i5 == 0) {
            this.u.a.a(resolutionAnchor3, f3);
        } else if (i5 == 1) {
            this.f1121s.a.a(resolutionAnchor3, f3);
        } else if (i5 == 2) {
            this.v.a.a(resolutionAnchor3, f3);
        } else if (i5 == 3) {
            this.f1122t.a.a(resolutionAnchor3, f3);
        }
    }

    public void a(LinearSystem linearSystem) {
        ConstraintAnchor[] constraintAnchorArr;
        boolean z;
        int i2;
        int i3;
        ConstraintAnchor[] constraintAnchorArr2 = this.A;
        constraintAnchorArr2[0] = this.f1121s;
        constraintAnchorArr2[2] = this.f1122t;
        constraintAnchorArr2[1] = this.u;
        constraintAnchorArr2[3] = this.v;
        int i4 = 0;
        while (true) {
            constraintAnchorArr = this.A;
            if (i4 >= constraintAnchorArr.length) {
                break;
            }
            constraintAnchorArr[i4].f1108i = linearSystem.a(constraintAnchorArr[i4]);
            i4++;
        }
        int i5 = this.m0;
        if (i5 >= 0 && i5 < 4) {
            ConstraintAnchor constraintAnchor = constraintAnchorArr[i5];
            int i6 = 0;
            while (true) {
                if (i6 >= super.l0) {
                    z = false;
                    break;
                }
                ConstraintWidget constraintWidget = super.k0[i6];
                if ((this.o0 || constraintWidget.a()) && ((((i2 = this.m0) == 0 || i2 == 1) && constraintWidget.e() == ConstraintWidget.a.MATCH_CONSTRAINT) || (((i3 = this.m0) == 2 || i3 == 3) && constraintWidget.h() == ConstraintWidget.a.MATCH_CONSTRAINT))) {
                    z = true;
                } else {
                    i6++;
                }
            }
            int i7 = this.m0;
            if (i7 == 0 || i7 == 1 ? this.D.e() == ConstraintWidget.a.WRAP_CONTENT : this.D.h() == ConstraintWidget.a.WRAP_CONTENT) {
                z = false;
            }
            for (int i8 = 0; i8 < super.l0; i8++) {
                ConstraintWidget constraintWidget2 = super.k0[i8];
                if (this.o0 || constraintWidget2.a()) {
                    SolverVariable a = linearSystem.a(constraintWidget2.A[this.m0]);
                    ConstraintAnchor[] constraintAnchorArr3 = constraintWidget2.A;
                    int i9 = this.m0;
                    constraintAnchorArr3[i9].f1108i = a;
                    if (i9 == 0 || i9 == 2) {
                        SolverVariable solverVariable = constraintAnchor.f1108i;
                        ArrayRow b = linearSystem.b();
                        SolverVariable c = linearSystem.c();
                        c.d = 0;
                        b.b(solverVariable, a, c, 0);
                        if (z) {
                            b.d.a(linearSystem.a(1, (String) null), (float) ((int) (b.d.a(c) * -1.0f)));
                        }
                        linearSystem.a(b);
                    } else {
                        SolverVariable solverVariable2 = constraintAnchor.f1108i;
                        ArrayRow b2 = linearSystem.b();
                        SolverVariable c2 = linearSystem.c();
                        c2.d = 0;
                        b2.a(solverVariable2, a, c2, 0);
                        if (z) {
                            b2.d.a(linearSystem.a(1, (String) null), (float) ((int) (b2.d.a(c2) * -1.0f)));
                        }
                        linearSystem.a(b2);
                    }
                }
            }
            int i10 = this.m0;
            if (i10 == 0) {
                linearSystem.a(this.u.f1108i, this.f1121s.f1108i, 0, 6);
                if (!z) {
                    linearSystem.a(this.f1121s.f1108i, this.D.u.f1108i, 0, 5);
                }
            } else if (i10 == 1) {
                linearSystem.a(this.f1121s.f1108i, this.u.f1108i, 0, 6);
                if (!z) {
                    linearSystem.a(this.f1121s.f1108i, this.D.f1121s.f1108i, 0, 5);
                }
            } else if (i10 == 2) {
                linearSystem.a(this.v.f1108i, this.f1122t.f1108i, 0, 6);
                if (!z) {
                    linearSystem.a(this.f1122t.f1108i, this.D.v.f1108i, 0, 5);
                }
            } else if (i10 == 3) {
                linearSystem.a(this.f1122t.f1108i, this.v.f1108i, 0, 6);
                if (!z) {
                    linearSystem.a(this.f1122t.f1108i, this.D.f1122t.f1108i, 0, 5);
                }
            }
        }
    }
}
