package i.f.a.h;

import i.f.a.ArrayRow;
import i.f.a.LinearSystem;
import i.f.a.SolverVariable;
import i.f.a.h.ConstraintAnchor;
import i.f.a.h.ConstraintWidget;
import java.util.ArrayList;

public class Guideline extends ConstraintWidget {
    public float k0 = -1.0f;
    public int l0 = -1;
    public int m0 = -1;
    public ConstraintAnchor n0 = super.f1122t;
    public int o0;
    public boolean p0;

    public Guideline() {
        this.o0 = 0;
        this.p0 = false;
        super.B.clear();
        super.B.add(this.n0);
        int length = super.A.length;
        for (int i2 = 0; i2 < length; i2++) {
            super.A[i2] = this.n0;
        }
    }

    public ConstraintAnchor a(ConstraintAnchor.c cVar) {
        switch (cVar.ordinal()) {
            case 0:
            case 5:
            case 6:
            case 7:
            case 8:
                return null;
            case 1:
            case 3:
                if (this.o0 == 1) {
                    return this.n0;
                }
                break;
            case 2:
            case 4:
                if (this.o0 == 0) {
                    return this.n0;
                }
                break;
        }
        throw new AssertionError(cVar.name());
    }

    public boolean a() {
        return true;
    }

    public ArrayList<c> b() {
        return super.B;
    }

    public void c(LinearSystem linearSystem) {
        if (super.D != null) {
            int b = linearSystem.b(this.n0);
            if (this.o0 == 1) {
                super.I = b;
                super.J = 0;
                e(super.D.d());
                f(0);
                return;
            }
            super.I = 0;
            super.J = b;
            f(super.D.i());
            e(0);
        }
    }

    public void g(int i2) {
        if (this.o0 != i2) {
            this.o0 = i2;
            super.B.clear();
            if (this.o0 == 1) {
                this.n0 = super.f1121s;
            } else {
                this.n0 = super.f1122t;
            }
            super.B.add(this.n0);
            int length = super.A.length;
            for (int i3 = 0; i3 < length; i3++) {
                super.A[i3] = this.n0;
            }
        }
    }

    public void a(LinearSystem linearSystem) {
        ConstraintWidgetContainer constraintWidgetContainer = (ConstraintWidgetContainer) super.D;
        if (constraintWidgetContainer != null) {
            ConstraintAnchor a = super.a(ConstraintAnchor.c.LEFT);
            ConstraintAnchor a2 = super.a(ConstraintAnchor.c.RIGHT);
            ConstraintWidget constraintWidget = super.D;
            boolean z = true;
            boolean z2 = constraintWidget != null && super.C[0] == ConstraintWidget.a.WRAP_CONTENT;
            if (this.o0 == 0) {
                a = super.a(ConstraintAnchor.c.TOP);
                a2 = super.a(ConstraintAnchor.c.BOTTOM);
                ConstraintWidget constraintWidget2 = super.D;
                if (constraintWidget2 == null || super.C[1] != ConstraintWidget.a.WRAP_CONTENT) {
                    z = false;
                }
                z2 = z;
            }
            if (this.l0 != -1) {
                SolverVariable a3 = linearSystem.a(this.n0);
                linearSystem.a(a3, linearSystem.a(a), this.l0, 6);
                if (z2) {
                    linearSystem.b(linearSystem.a(a2), a3, 0, 5);
                }
            } else if (this.m0 != -1) {
                SolverVariable a4 = linearSystem.a(this.n0);
                SolverVariable a5 = linearSystem.a(a2);
                linearSystem.a(a4, a5, -this.m0, 6);
                if (z2) {
                    linearSystem.b(a4, linearSystem.a(a), 0, 5);
                    linearSystem.b(a5, a4, 0, 5);
                }
            } else if (this.k0 != -1.0f) {
                SolverVariable a6 = linearSystem.a(this.n0);
                SolverVariable a7 = linearSystem.a(a);
                SolverVariable a8 = linearSystem.a(a2);
                float f2 = this.k0;
                boolean z3 = this.p0;
                ArrayRow b = linearSystem.b();
                if (z3) {
                    b.a(linearSystem, 0);
                }
                b.d.a(a6, -1.0f);
                b.d.a(a7, 1.0f - f2);
                b.d.a(a8, f2);
                linearSystem.a(b);
            }
        }
    }

    public void a(int i2) {
        ConstraintWidget constraintWidget = super.D;
        if (constraintWidget != null) {
            if (this.o0 == 1) {
                super.f1122t.a.a(1, super.f1122t.a, 0);
                super.v.a.a(1, super.f1122t.a, 0);
                int i3 = this.l0;
                if (i3 != -1) {
                    super.f1121s.a.a(1, super.f1121s.a, i3);
                    super.u.a.a(1, super.f1121s.a, this.l0);
                    return;
                }
                int i4 = this.m0;
                if (i4 != -1) {
                    super.f1121s.a.a(1, super.u.a, -i4);
                    super.u.a.a(1, super.u.a, -this.m0);
                } else if (this.k0 != -1.0f && super.e() == ConstraintWidget.a.FIXED) {
                    int i5 = (int) (((float) super.E) * this.k0);
                    super.f1121s.a.a(1, super.f1121s.a, i5);
                    super.u.a.a(1, super.f1121s.a, i5);
                }
            } else {
                super.f1121s.a.a(1, super.f1121s.a, 0);
                super.u.a.a(1, super.f1121s.a, 0);
                int i6 = this.l0;
                if (i6 != -1) {
                    super.f1122t.a.a(1, super.f1122t.a, i6);
                    super.v.a.a(1, super.f1122t.a, this.l0);
                    return;
                }
                int i7 = this.m0;
                if (i7 != -1) {
                    super.f1122t.a.a(1, super.v.a, -i7);
                    super.v.a.a(1, super.v.a, -this.m0);
                } else if (this.k0 != -1.0f && super.h() == ConstraintWidget.a.FIXED) {
                    int i8 = (int) (((float) super.F) * this.k0);
                    super.f1122t.a.a(1, super.f1122t.a, i8);
                    super.v.a.a(1, super.f1122t.a, i8);
                }
            }
        }
    }
}
