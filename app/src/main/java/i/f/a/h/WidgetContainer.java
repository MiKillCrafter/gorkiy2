package i.f.a.h;

import i.f.a.Cache;
import java.util.ArrayList;

public class WidgetContainer extends ConstraintWidget {
    public ArrayList<d> k0 = new ArrayList<>();

    public void a(Cache cache) {
        super.a(cache);
        int size = this.k0.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.k0.get(i2).a(cache);
        }
    }

    public void b(int i2, int i3) {
        super.O = i2;
        super.P = i3;
        int size = this.k0.size();
        for (int i4 = 0; i4 < size; i4++) {
            this.k0.get(i4).b(super.I + super.O, super.J + super.P);
        }
    }

    public void k() {
        this.k0.clear();
        super.k();
    }

    public void n() {
        int i2 = super.I;
        int i3 = super.J;
        super.M = i2;
        super.N = i3;
        ArrayList<d> arrayList = this.k0;
        if (arrayList != null) {
            int size = arrayList.size();
            for (int i4 = 0; i4 < size; i4++) {
                ConstraintWidget constraintWidget = this.k0.get(i4);
                super.b(super.M + super.O, super.N + super.P);
                if (!(constraintWidget instanceof ConstraintWidgetContainer)) {
                    super.n();
                }
            }
        }
    }

    public void o() {
        n();
        ArrayList<d> arrayList = this.k0;
        if (arrayList != null) {
            int size = arrayList.size();
            for (int i2 = 0; i2 < size; i2++) {
                ConstraintWidget constraintWidget = this.k0.get(i2);
                if (constraintWidget instanceof WidgetContainer) {
                    ((WidgetContainer) constraintWidget).o();
                }
            }
        }
    }
}
