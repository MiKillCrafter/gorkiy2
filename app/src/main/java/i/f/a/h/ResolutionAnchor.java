package i.f.a.h;

import i.f.a.LinearSystem;
import i.f.a.SolverVariable;
import i.f.a.h.ConstraintAnchor;
import j.a.a.a.outline;

public class ResolutionAnchor extends ResolutionNode {
    public ConstraintAnchor c;
    public ResolutionAnchor d;

    /* renamed from: e  reason: collision with root package name */
    public float f1128e;

    /* renamed from: f  reason: collision with root package name */
    public ResolutionAnchor f1129f;
    public float g;
    public int h = 0;

    /* renamed from: i  reason: collision with root package name */
    public ResolutionAnchor f1130i;

    /* renamed from: j  reason: collision with root package name */
    public ResolutionDimension f1131j = null;

    /* renamed from: k  reason: collision with root package name */
    public int f1132k = 1;

    /* renamed from: l  reason: collision with root package name */
    public ResolutionDimension f1133l = null;

    /* renamed from: m  reason: collision with root package name */
    public int f1134m = 1;

    public ResolutionAnchor(ConstraintAnchor constraintAnchor) {
        this.c = constraintAnchor;
    }

    public String a(int i2) {
        return i2 == 1 ? "DIRECT" : i2 == 2 ? "CENTER" : i2 == 3 ? "MATCH" : i2 == 4 ? "CHAIN" : i2 == 5 ? "BARRIER" : "UNCONNECTED";
    }

    public void a(ResolutionAnchor resolutionAnchor, float f2) {
        if (super.b == 0 || !(this.f1129f == resolutionAnchor || this.g == f2)) {
            this.f1129f = resolutionAnchor;
            this.g = f2;
            if (super.b == 1) {
                b();
            }
            a();
        }
    }

    public void d() {
        ResolutionAnchor resolutionAnchor;
        ResolutionAnchor resolutionAnchor2;
        ResolutionAnchor resolutionAnchor3;
        ResolutionAnchor resolutionAnchor4;
        ResolutionAnchor resolutionAnchor5;
        ResolutionAnchor resolutionAnchor6;
        float f2;
        float f3;
        float f4;
        float f5;
        ResolutionAnchor resolutionAnchor7;
        boolean z = true;
        if (super.b != 1 && this.h != 4) {
            ResolutionDimension resolutionDimension = this.f1131j;
            if (resolutionDimension != null) {
                if (super.b == 1) {
                    this.f1128e = ((float) this.f1132k) * resolutionDimension.c;
                } else {
                    return;
                }
            }
            ResolutionDimension resolutionDimension2 = this.f1133l;
            if (resolutionDimension2 != null) {
                if (super.b == 1) {
                    float f6 = resolutionDimension2.c;
                } else {
                    return;
                }
            }
            if (this.h == 1 && ((resolutionAnchor7 = this.d) == null || super.b == 1)) {
                ResolutionAnchor resolutionAnchor8 = this.d;
                if (resolutionAnchor8 == null) {
                    this.f1129f = this;
                    this.g = this.f1128e;
                } else {
                    this.f1129f = resolutionAnchor8.f1129f;
                    this.g = resolutionAnchor8.g + this.f1128e;
                }
                a();
            } else if (this.h == 2 && (resolutionAnchor4 = this.d) != null && super.b == 1 && (resolutionAnchor5 = this.f1130i) != null && (resolutionAnchor6 = resolutionAnchor5.d) != null && super.b == 1) {
                this.f1129f = resolutionAnchor4.f1129f;
                resolutionAnchor5.f1129f = resolutionAnchor6.f1129f;
                ConstraintAnchor.c cVar = this.c.c;
                int i2 = 0;
                if (!(cVar == ConstraintAnchor.c.RIGHT || cVar == ConstraintAnchor.c.BOTTOM)) {
                    z = false;
                }
                if (z) {
                    f3 = this.d.g;
                    f2 = this.f1130i.d.g;
                } else {
                    f3 = this.f1130i.d.g;
                    f2 = this.d.g;
                }
                float f7 = f3 - f2;
                ConstraintAnchor constraintAnchor = this.c;
                ConstraintAnchor.c cVar2 = constraintAnchor.c;
                if (cVar2 == ConstraintAnchor.c.LEFT || cVar2 == ConstraintAnchor.c.RIGHT) {
                    f5 = f7 - ((float) this.c.b.i());
                    f4 = this.c.b.V;
                } else {
                    f5 = f7 - ((float) constraintAnchor.b.d());
                    f4 = this.c.b.W;
                }
                int a = this.c.a();
                int a2 = this.f1130i.c.a();
                if (this.c.d == this.f1130i.c.d) {
                    f4 = 0.5f;
                    a2 = 0;
                } else {
                    i2 = a;
                }
                float f8 = (float) i2;
                float f9 = (float) a2;
                float f10 = (f5 - f8) - f9;
                if (z) {
                    ResolutionAnchor resolutionAnchor9 = this.f1130i;
                    resolutionAnchor9.g = (f10 * f4) + resolutionAnchor9.d.g + f9;
                    this.g = (this.d.g - f8) - ((1.0f - f4) * f10);
                } else {
                    this.g = (f10 * f4) + this.d.g + f8;
                    ResolutionAnchor resolutionAnchor10 = this.f1130i;
                    resolutionAnchor10.g = (resolutionAnchor10.d.g - f9) - ((1.0f - f4) * f10);
                }
                a();
                this.f1130i.a();
            } else if (this.h == 3 && (resolutionAnchor = this.d) != null && super.b == 1 && (resolutionAnchor2 = this.f1130i) != null && (resolutionAnchor3 = resolutionAnchor2.d) != null && super.b == 1) {
                this.f1129f = resolutionAnchor.f1129f;
                resolutionAnchor2.f1129f = resolutionAnchor3.f1129f;
                this.g = resolutionAnchor.g + this.f1128e;
                resolutionAnchor2.g = resolutionAnchor3.g + resolutionAnchor2.f1128e;
                a();
                this.f1130i.a();
            } else if (this.h == 5) {
                this.c.b.m();
            }
        }
    }

    public void e() {
        super.b = 0;
        super.a.clear();
        this.d = null;
        this.f1128e = 0.0f;
        this.f1131j = null;
        this.f1132k = 1;
        this.f1133l = null;
        this.f1134m = 1;
        this.f1129f = null;
        this.g = 0.0f;
        this.f1130i = null;
        this.h = 0;
    }

    public String toString() {
        if (super.b != 1) {
            StringBuilder a = outline.a("{ ");
            a.append(this.c);
            a.append(" UNRESOLVED} type: ");
            a.append(a(this.h));
            return a.toString();
        } else if (this.f1129f == this) {
            StringBuilder a2 = outline.a("[");
            a2.append(this.c);
            a2.append(", RESOLVED: ");
            a2.append(this.g);
            a2.append("]  type: ");
            a2.append(a(this.h));
            return a2.toString();
        } else {
            StringBuilder a3 = outline.a("[");
            a3.append(this.c);
            a3.append(", RESOLVED: ");
            a3.append(this.f1129f);
            a3.append(":");
            a3.append(this.g);
            a3.append("] type: ");
            a3.append(a(this.h));
            return a3.toString();
        }
    }

    public void a(int i2, ResolutionAnchor resolutionAnchor, int i3) {
        this.h = i2;
        this.d = resolutionAnchor;
        this.f1128e = (float) i3;
        super.a.add(this);
    }

    public void a(ResolutionAnchor resolutionAnchor, int i2) {
        this.d = resolutionAnchor;
        this.f1128e = (float) i2;
        super.a.add(this);
    }

    public void a(ResolutionAnchor resolutionAnchor, int i2, ResolutionDimension resolutionDimension) {
        this.d = resolutionAnchor;
        super.a.add(this);
        this.f1131j = resolutionDimension;
        this.f1132k = i2;
        super.a.add(this);
    }

    public void a(LinearSystem linearSystem) {
        SolverVariable solverVariable = this.c.f1108i;
        ResolutionAnchor resolutionAnchor = this.f1129f;
        if (resolutionAnchor == null) {
            linearSystem.a(solverVariable, (int) (this.g + 0.5f));
        } else {
            linearSystem.a(solverVariable, linearSystem.a(resolutionAnchor.c), (int) (this.g + 0.5f), 6);
        }
    }
}
