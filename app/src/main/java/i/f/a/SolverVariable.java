package i.f.a;

import j.a.a.a.outline;
import java.util.Arrays;

public class SolverVariable {

    /* renamed from: k  reason: collision with root package name */
    public static int f1090k = 1;
    public String a;
    public int b = -1;
    public int c = -1;
    public int d = 0;

    /* renamed from: e  reason: collision with root package name */
    public float f1091e;

    /* renamed from: f  reason: collision with root package name */
    public float[] f1092f = new float[7];
    public a g;
    public ArrayRow[] h = new ArrayRow[8];

    /* renamed from: i  reason: collision with root package name */
    public int f1093i = 0;

    /* renamed from: j  reason: collision with root package name */
    public int f1094j = 0;

    public enum a {
        UNRESTRICTED,
        CONSTANT,
        SLACK,
        ERROR,
        UNKNOWN
    }

    public SolverVariable(a aVar) {
        this.g = aVar;
    }

    public final void a(ArrayRow arrayRow) {
        int i2 = 0;
        while (true) {
            int i3 = this.f1093i;
            if (i2 >= i3) {
                ArrayRow[] arrayRowArr = this.h;
                if (i3 >= arrayRowArr.length) {
                    this.h = (ArrayRow[]) Arrays.copyOf(arrayRowArr, arrayRowArr.length * 2);
                }
                ArrayRow[] arrayRowArr2 = this.h;
                int i4 = this.f1093i;
                arrayRowArr2[i4] = arrayRow;
                this.f1093i = i4 + 1;
                return;
            } else if (this.h[i2] != arrayRow) {
                i2++;
            } else {
                return;
            }
        }
    }

    public final void b(ArrayRow arrayRow) {
        int i2 = this.f1093i;
        for (int i3 = 0; i3 < i2; i3++) {
            if (this.h[i3] == arrayRow) {
                for (int i4 = 0; i4 < (i2 - i3) - 1; i4++) {
                    ArrayRow[] arrayRowArr = this.h;
                    int i5 = i3 + i4;
                    arrayRowArr[i5] = arrayRowArr[i5 + 1];
                }
                this.f1093i--;
                return;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.f.a.ArrayLinkedVariables.a(i.f.a.SolverVariable, boolean):float
     arg types: [i.f.a.SolverVariable, int]
     candidates:
      i.f.a.ArrayLinkedVariables.a(boolean[], i.f.a.SolverVariable):i.f.a.SolverVariable
      i.f.a.ArrayLinkedVariables.a(i.f.a.SolverVariable, float):void
      i.f.a.ArrayLinkedVariables.a(i.f.a.SolverVariable, boolean):float */
    public final void c(ArrayRow arrayRow) {
        int i2 = this.f1093i;
        for (int i3 = 0; i3 < i2; i3++) {
            ArrayRow[] arrayRowArr = this.h;
            ArrayLinkedVariables arrayLinkedVariables = arrayRowArr[i3].d;
            ArrayRow arrayRow2 = arrayRowArr[i3];
            int i4 = arrayLinkedVariables.f1076i;
            while (true) {
                int i5 = 0;
                while (i4 != -1 && i5 < arrayLinkedVariables.a) {
                    int i6 = arrayLinkedVariables.f1075f[i4];
                    SolverVariable solverVariable = arrayRow.a;
                    if (i6 == solverVariable.b) {
                        float f2 = arrayLinkedVariables.h[i4];
                        arrayLinkedVariables.a(solverVariable, false);
                        ArrayLinkedVariables arrayLinkedVariables2 = arrayRow.d;
                        int i7 = arrayLinkedVariables2.f1076i;
                        int i8 = 0;
                        while (i7 != -1 && i8 < arrayLinkedVariables2.a) {
                            arrayLinkedVariables.a(arrayLinkedVariables.c.c[arrayLinkedVariables2.f1075f[i7]], arrayLinkedVariables2.h[i7] * f2, false);
                            i7 = arrayLinkedVariables2.g[i7];
                            i8++;
                        }
                        arrayRow2.b = (arrayRow.b * f2) + arrayRow2.b;
                        i4 = arrayLinkedVariables.f1076i;
                    } else {
                        i4 = arrayLinkedVariables.g[i4];
                        i5++;
                    }
                }
            }
        }
        this.f1093i = 0;
    }

    public String toString() {
        StringBuilder a2 = outline.a("");
        a2.append(this.a);
        return a2.toString();
    }

    public void a() {
        this.a = null;
        this.g = a.UNKNOWN;
        this.d = 0;
        this.b = -1;
        this.c = -1;
        this.f1091e = 0.0f;
        this.f1093i = 0;
        this.f1094j = 0;
    }
}
