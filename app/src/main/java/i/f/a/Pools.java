package i.f.a;

public class Pools<T> {
    public final Object[] a;
    public int b;

    public Pools(int i2) {
        if (i2 > 0) {
            this.a = new Object[i2];
            return;
        }
        throw new IllegalArgumentException("The max pool size must be > 0");
    }

    public T a() {
        int i2 = this.b;
        if (i2 <= 0) {
            return null;
        }
        int i3 = i2 - 1;
        T[] tArr = this.a;
        T t2 = tArr[i3];
        tArr[i3] = null;
        this.b = i2 - 1;
        return t2;
    }

    public boolean a(T t2) {
        int i2 = this.b;
        Object[] objArr = this.a;
        if (i2 >= objArr.length) {
            return false;
        }
        objArr[i2] = t2;
        this.b = i2 + 1;
        return true;
    }
}
