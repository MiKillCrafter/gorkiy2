package i.b.q;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import androidx.appcompat.widget.ActionMenuView;
import androidx.appcompat.widget.Toolbar;
import i.b.h;
import i.b.j;
import i.b.l.a.AppCompatResources;
import i.b.p.i.MenuBuilder;
import i.b.p.i.MenuItemImpl;
import i.b.p.i.MenuPresenter;
import i.h.l.ViewCompat;
import i.h.l.ViewPropertyAnimatorCompat;
import i.h.l.ViewPropertyAnimatorListenerAdapter;

/* compiled from: ToolbarWidgetWrapper */
public class ToolbarWidgetWrapper0 implements DecorToolbar {
    public Toolbar a;
    public int b;
    public View c;
    public View d;

    /* renamed from: e  reason: collision with root package name */
    public Drawable f1030e;

    /* renamed from: f  reason: collision with root package name */
    public Drawable f1031f;
    public Drawable g;
    public boolean h;

    /* renamed from: i  reason: collision with root package name */
    public CharSequence f1032i;

    /* renamed from: j  reason: collision with root package name */
    public CharSequence f1033j;

    /* renamed from: k  reason: collision with root package name */
    public CharSequence f1034k;

    /* renamed from: l  reason: collision with root package name */
    public Window.Callback f1035l;

    /* renamed from: m  reason: collision with root package name */
    public boolean f1036m;

    /* renamed from: n  reason: collision with root package name */
    public ActionMenuPresenter f1037n;

    /* renamed from: o  reason: collision with root package name */
    public int f1038o = 0;

    /* renamed from: p  reason: collision with root package name */
    public int f1039p = 0;

    /* renamed from: q  reason: collision with root package name */
    public Drawable f1040q;

    /* compiled from: ToolbarWidgetWrapper */
    public class a extends ViewPropertyAnimatorListenerAdapter {
        public boolean a = false;
        public final /* synthetic */ int b;

        public a(int i2) {
            this.b = i2;
        }

        public void a(View view) {
            if (!this.a) {
                ToolbarWidgetWrapper0.this.a.setVisibility(this.b);
            }
        }

        public void b(View view) {
            ToolbarWidgetWrapper0.this.a.setVisibility(0);
        }

        public void c(View view) {
            this.a = true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, androidx.appcompat.widget.Toolbar, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public ToolbarWidgetWrapper0(Toolbar toolbar, boolean z) {
        int i2;
        Drawable drawable;
        int i3 = h.abc_action_bar_up_description;
        this.a = toolbar;
        this.f1032i = toolbar.getTitle();
        this.f1033j = toolbar.getSubtitle();
        this.h = this.f1032i != null;
        this.g = toolbar.getNavigationIcon();
        String str = null;
        TintTypedArray a2 = TintTypedArray.a(toolbar.getContext(), null, j.ActionBar, i.b.a.actionBarStyle, 0);
        this.f1040q = a2.b(j.ActionBar_homeAsUpIndicator);
        if (z) {
            CharSequence e2 = a2.e(j.ActionBar_title);
            if (!TextUtils.isEmpty(e2)) {
                a(e2);
            }
            CharSequence e3 = a2.e(j.ActionBar_subtitle);
            if (!TextUtils.isEmpty(e3)) {
                this.f1033j = e3;
                if ((this.b & 8) != 0) {
                    this.a.setSubtitle(e3);
                }
            }
            Drawable b2 = a2.b(j.ActionBar_logo);
            if (b2 != null) {
                this.f1031f = b2;
                r();
            }
            Drawable b3 = a2.b(j.ActionBar_icon);
            if (b3 != null) {
                this.f1030e = b3;
                r();
            }
            if (this.g == null && (drawable = this.f1040q) != null) {
                this.g = drawable;
                q();
            }
            c(a2.d(j.ActionBar_displayOptions, 0));
            int f2 = a2.f(j.ActionBar_customNavigationLayout, 0);
            if (f2 != 0) {
                View inflate = LayoutInflater.from(this.a.getContext()).inflate(f2, (ViewGroup) this.a, false);
                View view = this.d;
                if (!(view == null || (this.b & 16) == 0)) {
                    this.a.removeView(view);
                }
                this.d = inflate;
                if (!(inflate == null || (this.b & 16) == 0)) {
                    this.a.addView(inflate);
                }
                c(this.b | 16);
            }
            int e4 = a2.e(j.ActionBar_height, 0);
            if (e4 > 0) {
                ViewGroup.LayoutParams layoutParams = this.a.getLayoutParams();
                layoutParams.height = e4;
                this.a.setLayoutParams(layoutParams);
            }
            int b4 = a2.b(j.ActionBar_contentInsetStart, -1);
            int b5 = a2.b(j.ActionBar_contentInsetEnd, -1);
            if (b4 >= 0 || b5 >= 0) {
                Toolbar toolbar2 = this.a;
                int max = Math.max(b4, 0);
                int max2 = Math.max(b5, 0);
                toolbar2.b();
                toolbar2.u.a(max, max2);
            }
            int f3 = a2.f(j.ActionBar_titleTextStyle, 0);
            if (f3 != 0) {
                Toolbar toolbar3 = this.a;
                Context context = toolbar3.getContext();
                toolbar3.f133m = f3;
                TextView textView = toolbar3.c;
                if (textView != null) {
                    textView.setTextAppearance(context, f3);
                }
            }
            int f4 = a2.f(j.ActionBar_subtitleTextStyle, 0);
            if (f4 != 0) {
                Toolbar toolbar4 = this.a;
                Context context2 = toolbar4.getContext();
                toolbar4.f134n = f4;
                TextView textView2 = toolbar4.d;
                if (textView2 != null) {
                    textView2.setTextAppearance(context2, f4);
                }
            }
            int f5 = a2.f(j.ActionBar_popupTheme, 0);
            if (f5 != 0) {
                this.a.setPopupTheme(f5);
            }
        } else {
            if (this.a.getNavigationIcon() != null) {
                i2 = 15;
                this.f1040q = this.a.getNavigationIcon();
            } else {
                i2 = 11;
            }
            this.b = i2;
        }
        a2.b.recycle();
        if (i3 != this.f1039p) {
            this.f1039p = i3;
            if (TextUtils.isEmpty(this.a.getNavigationContentDescription())) {
                int i4 = this.f1039p;
                this.f1034k = i4 != 0 ? c().getString(i4) : str;
                p();
            }
        }
        this.f1034k = this.a.getNavigationContentDescription();
        this.a.setNavigationOnClickListener(new ToolbarWidgetWrapper(this));
    }

    public void a(CharSequence charSequence) {
        this.h = true;
        this.f1032i = charSequence;
        if ((this.b & 8) != 0) {
            this.a.setTitle(charSequence);
        }
    }

    public void a(boolean z) {
    }

    public void b(int i2) {
        this.f1031f = i2 != 0 ? AppCompatResources.c(c(), i2) : null;
        r();
    }

    public Context c() {
        return this.a.getContext();
    }

    public void collapseActionView() {
        Toolbar.d dVar = this.a.L;
        MenuItemImpl menuItemImpl = dVar == null ? null : dVar.c;
        if (menuItemImpl != null) {
            menuItemImpl.collapseActionView();
        }
    }

    public boolean d() {
        ActionMenuView actionMenuView = this.a.b;
        if (actionMenuView == null) {
            return false;
        }
        ActionMenuPresenter actionMenuPresenter = actionMenuView.u;
        if (actionMenuPresenter != null && actionMenuPresenter.b()) {
            return true;
        }
        return false;
    }

    public boolean e() {
        return this.a.g();
    }

    public void f() {
        this.f1036m = true;
    }

    public boolean g() {
        ActionMenuView actionMenuView;
        Toolbar toolbar = this.a;
        return toolbar.getVisibility() == 0 && (actionMenuView = toolbar.b) != null && actionMenuView.f89t;
    }

    public CharSequence getTitle() {
        return this.a.getTitle();
    }

    public void h() {
        ActionMenuPresenter actionMenuPresenter;
        ActionMenuView actionMenuView = this.a.b;
        if (actionMenuView != null && (actionMenuPresenter = actionMenuView.u) != null) {
            actionMenuPresenter.a();
        }
    }

    public int i() {
        return this.b;
    }

    public Menu j() {
        return this.a.getMenu();
    }

    public ViewGroup k() {
        return this.a;
    }

    public int l() {
        return this.f1038o;
    }

    public void m() {
        Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
    }

    public boolean n() {
        Toolbar.d dVar = this.a.L;
        return (dVar == null || dVar.c == null) ? false : true;
    }

    public void o() {
        Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
    }

    public final void p() {
        if ((this.b & 4) == 0) {
            return;
        }
        if (TextUtils.isEmpty(this.f1034k)) {
            this.a.setNavigationContentDescription(this.f1039p);
        } else {
            this.a.setNavigationContentDescription(this.f1034k);
        }
    }

    public final void q() {
        if ((this.b & 4) != 0) {
            Toolbar toolbar = this.a;
            Drawable drawable = this.g;
            if (drawable == null) {
                drawable = this.f1040q;
            }
            toolbar.setNavigationIcon(drawable);
            return;
        }
        this.a.setNavigationIcon((Drawable) null);
    }

    public final void r() {
        Drawable drawable;
        int i2 = this.b;
        if ((i2 & 2) == 0) {
            drawable = null;
        } else if ((i2 & 1) != 0) {
            drawable = this.f1031f;
            if (drawable == null) {
                drawable = this.f1030e;
            }
        } else {
            drawable = this.f1030e;
        }
        this.a.setLogo(drawable);
    }

    public void setIcon(int i2) {
        this.f1030e = i2 != 0 ? AppCompatResources.c(c(), i2) : null;
        r();
    }

    public void setWindowCallback(Window.Callback callback) {
        this.f1035l = callback;
    }

    public void setWindowTitle(CharSequence charSequence) {
        if (!this.h) {
            this.f1032i = charSequence;
            if ((this.b & 8) != 0) {
                this.a.setTitle(charSequence);
            }
        }
    }

    public void c(int i2) {
        View view;
        int i3 = this.b ^ i2;
        this.b = i2;
        if (i3 != 0) {
            if ((i3 & 4) != 0) {
                if ((i2 & 4) != 0) {
                    p();
                }
                q();
            }
            if ((i3 & 3) != 0) {
                r();
            }
            if ((i3 & 8) != 0) {
                if ((i2 & 8) != 0) {
                    this.a.setTitle(this.f1032i);
                    this.a.setSubtitle(this.f1033j);
                } else {
                    this.a.setTitle((CharSequence) null);
                    this.a.setSubtitle((CharSequence) null);
                }
            }
            if ((i3 & 16) != 0 && (view = this.d) != null) {
                if ((i2 & 16) != 0) {
                    this.a.addView(view);
                } else {
                    this.a.removeView(view);
                }
            }
        }
    }

    public boolean b() {
        return this.a.f();
    }

    public void setIcon(Drawable drawable) {
        this.f1030e = drawable;
        r();
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0021 A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:16:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a() {
        /*
            r4 = this;
            androidx.appcompat.widget.Toolbar r0 = r4.a
            androidx.appcompat.widget.ActionMenuView r0 = r0.b
            r1 = 0
            r2 = 1
            if (r0 == 0) goto L_0x0022
            i.b.q.ActionMenuPresenter r0 = r0.u
            if (r0 == 0) goto L_0x001e
            i.b.q.ActionMenuPresenter$c r3 = r0.w
            if (r3 != 0) goto L_0x0019
            boolean r0 = r0.e()
            if (r0 == 0) goto L_0x0017
            goto L_0x0019
        L_0x0017:
            r0 = 0
            goto L_0x001a
        L_0x0019:
            r0 = 1
        L_0x001a:
            if (r0 == 0) goto L_0x001e
            r0 = 1
            goto L_0x001f
        L_0x001e:
            r0 = 0
        L_0x001f:
            if (r0 == 0) goto L_0x0022
            r1 = 1
        L_0x0022:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: i.b.q.ToolbarWidgetWrapper0.a():boolean");
    }

    public void b(boolean z) {
        this.a.setCollapsible(z);
    }

    public void a(Menu menu, MenuPresenter.a aVar) {
        MenuItemImpl menuItemImpl;
        if (this.f1037n == null) {
            this.f1037n = new ActionMenuPresenter(this.a.getContext());
        }
        ActionMenuPresenter actionMenuPresenter = this.f1037n;
        actionMenuPresenter.f859f = aVar;
        Toolbar toolbar = this.a;
        MenuBuilder menuBuilder = (MenuBuilder) menu;
        if (menuBuilder != null || toolbar.b != null) {
            toolbar.d();
            MenuBuilder menuBuilder2 = toolbar.b.f86q;
            if (menuBuilder2 != menuBuilder) {
                if (menuBuilder2 != null) {
                    menuBuilder2.a(toolbar.K);
                    menuBuilder2.a(toolbar.L);
                }
                if (toolbar.L == null) {
                    toolbar.L = new Toolbar.d();
                }
                actionMenuPresenter.f943r = true;
                if (menuBuilder != null) {
                    menuBuilder.a(actionMenuPresenter, toolbar.f131k);
                    menuBuilder.a(toolbar.L, toolbar.f131k);
                } else {
                    actionMenuPresenter.a(toolbar.f131k, (MenuBuilder) null);
                    Toolbar.d dVar = toolbar.L;
                    MenuBuilder menuBuilder3 = dVar.b;
                    if (!(menuBuilder3 == null || (menuItemImpl = dVar.c) == null)) {
                        menuBuilder3.a(menuItemImpl);
                    }
                    dVar.b = null;
                    actionMenuPresenter.a(true);
                    toolbar.L.a(true);
                }
                toolbar.b.setPopupTheme(toolbar.f132l);
                toolbar.b.setPresenter(actionMenuPresenter);
                toolbar.K = actionMenuPresenter;
            }
        }
    }

    public void a(ScrollingTabContainerView scrollingTabContainerView) {
        Toolbar toolbar;
        View view = this.c;
        if (view != null && view.getParent() == (toolbar = this.a)) {
            toolbar.removeView(this.c);
        }
        this.c = scrollingTabContainerView;
        if (scrollingTabContainerView != null && this.f1038o == 2) {
            this.a.addView(scrollingTabContainerView, 0);
            Toolbar.e eVar = (Toolbar.e) this.c.getLayoutParams();
            eVar.width = -2;
            eVar.height = -2;
            eVar.a = 8388691;
            scrollingTabContainerView.setAllowCollapse(true);
        }
    }

    public ViewPropertyAnimatorCompat a(int i2, long j2) {
        ViewPropertyAnimatorCompat a2 = ViewCompat.a(this.a);
        a2.a(i2 == 0 ? 1.0f : 0.0f);
        a2.a(j2);
        a aVar = new a(i2);
        View view = a2.a.get();
        if (view != null) {
            a2.a(view, aVar);
        }
        return a2;
    }

    public void a(int i2) {
        this.a.setVisibility(i2);
    }

    public void a(MenuPresenter.a aVar, MenuBuilder.a aVar2) {
        Toolbar toolbar = this.a;
        toolbar.M = aVar;
        toolbar.N = aVar2;
        ActionMenuView actionMenuView = toolbar.b;
        if (actionMenuView != null) {
            actionMenuView.v = aVar;
            actionMenuView.w = aVar2;
        }
    }
}
