package i.b.q;

import androidx.recyclerview.widget.RecyclerView;

public class RtlSpacingHelper {
    public int a = 0;
    public int b = 0;
    public int c = RecyclerView.UNDEFINED_DURATION;
    public int d = RecyclerView.UNDEFINED_DURATION;

    /* renamed from: e  reason: collision with root package name */
    public int f1015e = 0;

    /* renamed from: f  reason: collision with root package name */
    public int f1016f = 0;
    public boolean g = false;
    public boolean h = false;

    public void a(int i2, int i3) {
        this.c = i2;
        this.d = i3;
        this.h = true;
        if (this.g) {
            if (i3 != Integer.MIN_VALUE) {
                this.a = i3;
            }
            if (i2 != Integer.MIN_VALUE) {
                this.b = i2;
                return;
            }
            return;
        }
        if (i2 != Integer.MIN_VALUE) {
            this.a = i2;
        }
        if (i3 != Integer.MIN_VALUE) {
            this.b = i3;
        }
    }
}
