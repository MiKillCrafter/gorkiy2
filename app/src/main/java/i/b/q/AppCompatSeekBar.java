package i.b.q;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.widget.SeekBar;

public class AppCompatSeekBar extends SeekBar {
    public final AppCompatSeekBarHelper b;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AppCompatSeekBar(android.content.Context r2, android.util.AttributeSet r3) {
        /*
            r1 = this;
            int r0 = i.b.a.seekBarStyle
            r1.<init>(r2, r3, r0)
            i.b.q.AppCompatSeekBarHelper r2 = new i.b.q.AppCompatSeekBarHelper
            r2.<init>(r1)
            r1.b = r2
            r2.a(r3, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: i.b.q.AppCompatSeekBar.<init>(android.content.Context, android.util.AttributeSet):void");
    }

    public void drawableStateChanged() {
        super.drawableStateChanged();
        AppCompatSeekBarHelper appCompatSeekBarHelper = this.b;
        Drawable drawable = appCompatSeekBarHelper.f955e;
        if (drawable != null && drawable.isStateful() && drawable.setState(appCompatSeekBarHelper.d.getDrawableState())) {
            appCompatSeekBarHelper.d.invalidateDrawable(drawable);
        }
    }

    public void jumpDrawablesToCurrentState() {
        super.jumpDrawablesToCurrentState();
        Drawable drawable = this.b.f955e;
        if (drawable != null) {
            drawable.jumpToCurrentState();
        }
    }

    public synchronized void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        this.b.a(canvas);
    }
}
