package i.b.q;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.view.menu.ActionMenuItemView;
import androidx.appcompat.widget.ActionMenuView;
import i.b.g;
import i.b.k.ResourcesFlusher;
import i.b.p.i.BaseMenuPresenter;
import i.b.p.i.MenuBuilder;
import i.b.p.i.MenuItemImpl;
import i.b.p.i.MenuPopup;
import i.b.p.i.MenuPopupHelper;
import i.b.p.i.MenuPresenter;
import i.b.p.i.MenuView;
import i.b.p.i.ShowableListMenu;
import i.b.p.i.SubMenuBuilder;
import i.b.p.i.i;
import i.h.l.ActionProvider;
import java.util.ArrayList;

public class ActionMenuPresenter extends BaseMenuPresenter {

    /* renamed from: j  reason: collision with root package name */
    public d f935j;

    /* renamed from: k  reason: collision with root package name */
    public Drawable f936k;

    /* renamed from: l  reason: collision with root package name */
    public boolean f937l;

    /* renamed from: m  reason: collision with root package name */
    public boolean f938m;

    /* renamed from: n  reason: collision with root package name */
    public boolean f939n;

    /* renamed from: o  reason: collision with root package name */
    public int f940o;

    /* renamed from: p  reason: collision with root package name */
    public int f941p;

    /* renamed from: q  reason: collision with root package name */
    public int f942q;

    /* renamed from: r  reason: collision with root package name */
    public boolean f943r;

    /* renamed from: s  reason: collision with root package name */
    public int f944s;

    /* renamed from: t  reason: collision with root package name */
    public final SparseBooleanArray f945t = new SparseBooleanArray();
    public e u;
    public a v;
    public c w;
    public b x;
    public final f y = new f();
    public int z;

    public class a extends MenuPopupHelper {
        public a(Context context, SubMenuBuilder subMenuBuilder, View view) {
            super(context, subMenuBuilder, view, false, i.b.a.actionOverflowMenuStyle, 0);
            if (!subMenuBuilder.B.d()) {
                View view2 = ActionMenuPresenter.this.f935j;
                super.f912f = view2 == null ? (View) ActionMenuPresenter.this.f860i : view2;
            }
            a(ActionMenuPresenter.this.y);
        }

        public void c() {
            ActionMenuPresenter actionMenuPresenter = ActionMenuPresenter.this;
            actionMenuPresenter.v = null;
            actionMenuPresenter.z = 0;
            super.c();
        }
    }

    public class b extends ActionMenuItemView.b {
        public b() {
        }
    }

    public class c implements Runnable {
        public e b;

        public c(e eVar) {
            this.b = eVar;
        }

        public void run() {
            MenuBuilder.a aVar;
            MenuBuilder menuBuilder = ActionMenuPresenter.this.d;
            if (!(menuBuilder == null || (aVar = menuBuilder.f882e) == null)) {
                aVar.a(menuBuilder);
            }
            View view = (View) ActionMenuPresenter.this.f860i;
            if (!(view == null || view.getWindowToken() == null || !this.b.d())) {
                ActionMenuPresenter.this.u = this.b;
            }
            ActionMenuPresenter.this.w = null;
        }
    }

    public class d extends AppCompatImageView implements ActionMenuView.a {

        public class a extends ForwardingListener {
            public a(View view, ActionMenuPresenter actionMenuPresenter) {
                super(view);
            }

            public ShowableListMenu b() {
                e eVar = ActionMenuPresenter.this.u;
                if (eVar == null) {
                    return null;
                }
                return eVar.a();
            }

            public boolean c() {
                ActionMenuPresenter.this.f();
                return true;
            }

            public boolean d() {
                ActionMenuPresenter actionMenuPresenter = ActionMenuPresenter.this;
                if (actionMenuPresenter.w != null) {
                    return false;
                }
                actionMenuPresenter.b();
                return true;
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
         arg types: [i.b.q.ActionMenuPresenter$d, java.lang.CharSequence]
         candidates:
          i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
          i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
          i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
          i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
          i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
          i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
          i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
          i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
          i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
          i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
          i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
          i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
          i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
          i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
          i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
          i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
          i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
          i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
          i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
          i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
          i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
          i.b.k.ResourcesFlusher.a(int, int):boolean
          i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
          i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
          i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
          i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
          i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void */
        public d(Context context) {
            super(context, null, i.b.a.actionOverflowButtonStyle);
            setClickable(true);
            setFocusable(true);
            setVisibility(0);
            setEnabled(true);
            ResourcesFlusher.a((View) this, getContentDescription());
            setOnTouchListener(new a(this, ActionMenuPresenter.this));
        }

        public boolean a() {
            return false;
        }

        public boolean b() {
            return false;
        }

        public boolean performClick() {
            if (super.performClick()) {
                return true;
            }
            playSoundEffect(0);
            ActionMenuPresenter.this.f();
            return true;
        }

        public boolean setFrame(int i2, int i3, int i4, int i5) {
            boolean frame = super.setFrame(i2, i3, i4, i5);
            Drawable drawable = getDrawable();
            Drawable background = getBackground();
            if (!(drawable == null || background == null)) {
                int width = getWidth();
                int height = getHeight();
                int max = Math.max(width, height) / 2;
                int paddingLeft = (width + (getPaddingLeft() - getPaddingRight())) / 2;
                int paddingTop = (height + (getPaddingTop() - getPaddingBottom())) / 2;
                background.setHotspotBounds(paddingLeft - max, paddingTop - max, paddingLeft + max, paddingTop + max);
            }
            return frame;
        }
    }

    public class e extends MenuPopupHelper {
        public e(Context context, MenuBuilder menuBuilder, View view, boolean z) {
            super(context, menuBuilder, view, z, i.b.a.actionOverflowMenuStyle, 0);
            super.g = 8388613;
            a(ActionMenuPresenter.this.y);
        }

        public void c() {
            MenuBuilder menuBuilder = ActionMenuPresenter.this.d;
            if (menuBuilder != null) {
                menuBuilder.a(true);
            }
            ActionMenuPresenter.this.u = null;
            super.c();
        }
    }

    public ActionMenuPresenter(Context context) {
        super(context, g.abc_action_menu_layout, g.abc_action_menu_item_layout);
    }

    public void a(Context context, MenuBuilder menuBuilder) {
        super.c = context;
        LayoutInflater.from(context);
        super.d = menuBuilder;
        Resources resources = context.getResources();
        if (!this.f939n) {
            this.f938m = true;
        }
        int i2 = 2;
        this.f940o = context.getResources().getDisplayMetrics().widthPixels / 2;
        Configuration configuration = context.getResources().getConfiguration();
        int i3 = configuration.screenWidthDp;
        int i4 = configuration.screenHeightDp;
        if (configuration.smallestScreenWidthDp > 600 || i3 > 600 || ((i3 > 960 && i4 > 720) || (i3 > 720 && i4 > 960))) {
            i2 = 5;
        } else if (i3 >= 500 || ((i3 > 640 && i4 > 480) || (i3 > 480 && i4 > 640))) {
            i2 = 4;
        } else if (i3 >= 360) {
            i2 = 3;
        }
        this.f942q = i2;
        int i5 = this.f940o;
        if (this.f938m) {
            if (this.f935j == null) {
                d dVar = new d(super.b);
                this.f935j = dVar;
                if (this.f937l) {
                    dVar.setImageDrawable(this.f936k);
                    this.f936k = null;
                    this.f937l = false;
                }
                int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
                this.f935j.measure(makeMeasureSpec, makeMeasureSpec);
            }
            i5 -= this.f935j.getMeasuredWidth();
        } else {
            this.f935j = null;
        }
        this.f941p = i5;
        this.f944s = (int) (resources.getDisplayMetrics().density * 56.0f);
    }

    public boolean b() {
        MenuView menuView;
        c cVar = this.w;
        if (cVar == null || (menuView = super.f860i) == null) {
            e eVar = this.u;
            if (eVar == null) {
                return false;
            }
            if (eVar.b()) {
                eVar.f914j.dismiss();
            }
            return true;
        }
        ((View) menuView).removeCallbacks(cVar);
        this.w = null;
        return true;
    }

    public boolean c() {
        int i2;
        ArrayList<i> arrayList;
        int i3;
        boolean z2;
        MenuBuilder menuBuilder = super.d;
        View view = null;
        if (menuBuilder != null) {
            arrayList = menuBuilder.d();
            i2 = arrayList.size();
        } else {
            arrayList = null;
            i2 = 0;
        }
        int i4 = this.f942q;
        int i5 = this.f941p;
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        ViewGroup viewGroup = (ViewGroup) super.f860i;
        int i6 = 0;
        boolean z3 = false;
        int i7 = 0;
        int i8 = 0;
        while (true) {
            i3 = 2;
            z2 = true;
            if (i6 >= i2) {
                break;
            }
            MenuItemImpl menuItemImpl = (MenuItemImpl) arrayList.get(i6);
            if ((menuItemImpl.y & 2) == 2) {
                i8++;
            } else {
                if ((menuItemImpl.y & 1) == 1) {
                    i7++;
                } else {
                    z3 = true;
                }
            }
            if (this.f943r && menuItemImpl.C) {
                i4 = 0;
            }
            i6++;
        }
        if (this.f938m && (z3 || i7 + i8 > i4)) {
            i4--;
        }
        int i9 = i4 - i8;
        SparseBooleanArray sparseBooleanArray = this.f945t;
        sparseBooleanArray.clear();
        int i10 = 0;
        int i11 = 0;
        while (i10 < i2) {
            MenuItemImpl menuItemImpl2 = (MenuItemImpl) arrayList.get(i10);
            if ((menuItemImpl2.y & i3) == i3) {
                View a2 = a(menuItemImpl2, view, viewGroup);
                a2.measure(makeMeasureSpec, makeMeasureSpec);
                int measuredWidth = a2.getMeasuredWidth();
                i5 -= measuredWidth;
                if (i11 == 0) {
                    i11 = measuredWidth;
                }
                int i12 = menuItemImpl2.b;
                if (i12 != 0) {
                    sparseBooleanArray.put(i12, z2);
                }
                menuItemImpl2.b(z2);
            } else {
                if ((menuItemImpl2.y & z2) == z2) {
                    int i13 = menuItemImpl2.b;
                    boolean z4 = sparseBooleanArray.get(i13);
                    boolean z5 = (i9 > 0 || z4) && i5 > 0;
                    if (z5) {
                        View a3 = a(menuItemImpl2, view, viewGroup);
                        a3.measure(makeMeasureSpec, makeMeasureSpec);
                        int measuredWidth2 = a3.getMeasuredWidth();
                        i5 -= measuredWidth2;
                        if (i11 == 0) {
                            i11 = measuredWidth2;
                        }
                        z5 &= i5 + i11 > 0;
                    }
                    boolean z6 = z5;
                    if (z6 && i13 != 0) {
                        sparseBooleanArray.put(i13, z2);
                    } else if (z4) {
                        sparseBooleanArray.put(i13, false);
                        for (int i14 = 0; i14 < i10; i14++) {
                            MenuItemImpl menuItemImpl3 = (MenuItemImpl) arrayList.get(i14);
                            if (menuItemImpl3.b == i13) {
                                if (menuItemImpl3.d()) {
                                    i9++;
                                }
                                menuItemImpl3.b(false);
                            }
                        }
                    }
                    if (z6) {
                        i9--;
                    }
                    menuItemImpl2.b(z6);
                } else {
                    menuItemImpl2.b(false);
                    i10++;
                    view = null;
                    i3 = 2;
                    z2 = true;
                }
            }
            i10++;
            view = null;
            i3 = 2;
            z2 = true;
        }
        return true;
    }

    public boolean d() {
        a aVar = this.v;
        if (aVar == null) {
            return false;
        }
        if (!aVar.b()) {
            return true;
        }
        aVar.f914j.dismiss();
        return true;
    }

    public boolean e() {
        e eVar = this.u;
        return eVar != null && eVar.b();
    }

    public boolean f() {
        MenuBuilder menuBuilder;
        if (!this.f938m || e() || (menuBuilder = super.d) == null || super.f860i == null || this.w != null) {
            return false;
        }
        menuBuilder.a();
        if (menuBuilder.f885j.isEmpty()) {
            return false;
        }
        c cVar = new c(new e(super.c, super.d, this.f935j, true));
        this.w = cVar;
        ((View) super.f860i).post(cVar);
        super.a((SubMenuBuilder) null);
        return true;
    }

    public class f implements MenuPresenter.a {
        public f() {
        }

        public boolean a(MenuBuilder menuBuilder) {
            if (menuBuilder == null) {
                return false;
            }
            ActionMenuPresenter.this.z = ((SubMenuBuilder) menuBuilder).B.getItemId();
            MenuPresenter.a aVar = ActionMenuPresenter.this.f859f;
            if (aVar != null) {
                return aVar.a(menuBuilder);
            }
            return false;
        }

        public void a(MenuBuilder menuBuilder, boolean z) {
            if (menuBuilder instanceof SubMenuBuilder) {
                menuBuilder.c().a(false);
            }
            MenuPresenter.a aVar = ActionMenuPresenter.this.f859f;
            if (aVar != null) {
                aVar.a(menuBuilder, z);
            }
        }
    }

    public View a(MenuItemImpl menuItemImpl, View view, ViewGroup viewGroup) {
        View actionView = menuItemImpl.getActionView();
        if (actionView == null || menuItemImpl.c()) {
            actionView = super.a(menuItemImpl, view, viewGroup);
        }
        actionView.setVisibility(menuItemImpl.C ? 8 : 0);
        ActionMenuView actionMenuView = (ActionMenuView) viewGroup;
        ViewGroup.LayoutParams layoutParams = actionView.getLayoutParams();
        if (actionMenuView != null) {
            if (!(layoutParams instanceof ActionMenuView.c)) {
                actionView.setLayoutParams(actionMenuView.generateLayoutParams(layoutParams));
            }
            return actionView;
        }
        throw null;
    }

    public void a(boolean z2) {
        ArrayList<i> arrayList;
        MenuView menuView;
        super.a(z2);
        ((View) super.f860i).requestLayout();
        MenuBuilder menuBuilder = super.d;
        boolean z3 = false;
        if (menuBuilder != null) {
            menuBuilder.a();
            ArrayList<i> arrayList2 = menuBuilder.f884i;
            int size = arrayList2.size();
            for (int i2 = 0; i2 < size; i2++) {
                ActionProvider actionProvider = arrayList2.get(i2).A;
            }
        }
        MenuBuilder menuBuilder2 = super.d;
        if (menuBuilder2 != null) {
            menuBuilder2.a();
            arrayList = menuBuilder2.f885j;
        } else {
            arrayList = null;
        }
        if (this.f938m && arrayList != null) {
            int size2 = arrayList.size();
            if (size2 == 1) {
                z3 = !((MenuItemImpl) arrayList.get(0)).C;
            } else if (size2 > 0) {
                z3 = true;
            }
        }
        if (z3) {
            if (this.f935j == null) {
                this.f935j = new d(super.b);
            }
            ViewGroup viewGroup = (ViewGroup) this.f935j.getParent();
            if (viewGroup != super.f860i) {
                if (viewGroup != null) {
                    viewGroup.removeView(this.f935j);
                }
                ActionMenuView actionMenuView = (ActionMenuView) super.f860i;
                d dVar = this.f935j;
                ActionMenuView.c generateDefaultLayoutParams = actionMenuView.generateDefaultLayoutParams();
                generateDefaultLayoutParams.c = true;
                actionMenuView.addView(dVar, generateDefaultLayoutParams);
            }
        } else {
            d dVar2 = this.f935j;
            if (dVar2 != null && dVar2.getParent() == (menuView = super.f860i)) {
                ((ViewGroup) menuView).removeView(this.f935j);
            }
        }
        ((ActionMenuView) super.f860i).setOverflowReserved(this.f938m);
    }

    public boolean a(SubMenuBuilder subMenuBuilder) {
        boolean z2 = false;
        if (!subMenuBuilder.hasVisibleItems()) {
            return false;
        }
        SubMenuBuilder subMenuBuilder2 = subMenuBuilder;
        while (true) {
            MenuBuilder menuBuilder = subMenuBuilder2.A;
            if (menuBuilder == super.d) {
                break;
            }
            subMenuBuilder2 = menuBuilder;
        }
        MenuItemImpl menuItemImpl = subMenuBuilder2.B;
        ViewGroup viewGroup = (ViewGroup) super.f860i;
        View view = null;
        if (viewGroup != null) {
            int childCount = viewGroup.getChildCount();
            int i2 = 0;
            while (true) {
                if (i2 >= childCount) {
                    break;
                }
                View childAt = viewGroup.getChildAt(i2);
                if ((childAt instanceof MenuView.a) && ((MenuView.a) childAt).getItemData() == menuItemImpl) {
                    view = childAt;
                    break;
                }
                i2++;
            }
        }
        if (view == null) {
            return false;
        }
        subMenuBuilder.B.getItemId();
        int size = subMenuBuilder.size();
        int i3 = 0;
        while (true) {
            if (i3 >= size) {
                break;
            }
            MenuItem item = subMenuBuilder.getItem(i3);
            if (item.isVisible() && item.getIcon() != null) {
                z2 = true;
                break;
            }
            i3++;
        }
        a aVar = new a(super.c, subMenuBuilder, view);
        this.v = aVar;
        aVar.h = z2;
        MenuPopup menuPopup = aVar.f914j;
        if (menuPopup != null) {
            menuPopup.b(z2);
        }
        if (this.v.d()) {
            MenuPresenter.a aVar2 = super.f859f;
            if (aVar2 != null) {
                aVar2.a(subMenuBuilder);
            }
            return true;
        }
        throw new IllegalStateException("MenuPopupHelper cannot be used without an anchor");
    }

    public boolean a() {
        return b() | d();
    }

    public void a(MenuBuilder menuBuilder, boolean z2) {
        a();
        MenuPresenter.a aVar = super.f859f;
        if (aVar != null) {
            aVar.a(menuBuilder, z2);
        }
    }
}
