package i.b.q;

import android.view.Menu;
import android.view.Window;
import i.b.p.i.MenuPresenter;

public interface DecorContentParent {
    void a(int i2);

    void a(Menu menu, MenuPresenter.a aVar);

    boolean a();

    boolean b();

    boolean d();

    boolean e();

    void f();

    boolean g();

    void h();

    void setWindowCallback(Window.Callback callback);

    void setWindowTitle(CharSequence charSequence);
}
