package i.b.q;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ToggleButton;

public class AppCompatToggleButton extends ToggleButton {
    public final AppCompatTextHelper b;

    public AppCompatToggleButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 16842827);
        AppCompatTextHelper appCompatTextHelper = new AppCompatTextHelper(this);
        this.b = appCompatTextHelper;
        appCompatTextHelper.a(attributeSet, 16842827);
    }
}
