package i.b.q;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;

public class TintInfo {
    public ColorStateList a;
    public PorterDuff.Mode b;
    public boolean c;
    public boolean d;
}
