package i.b.k;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import androidx.appcompat.widget.ActionBarContainer;
import androidx.appcompat.widget.ActionBarContextView;
import androidx.appcompat.widget.ActionBarOverlayLayout;
import androidx.appcompat.widget.Toolbar;
import i.b.f;
import i.b.j;
import i.b.k.a;
import i.b.p.ActionMode;
import i.b.p.SupportMenuInflater;
import i.b.p.ViewPropertyAnimatorCompatSet;
import i.b.p.i.MenuBuilder;
import i.b.q.ActionMenuPresenter;
import i.b.q.DecorToolbar;
import i.b.q.ScrollingTabContainerView;
import i.h.l.ViewCompat;
import i.h.l.ViewPropertyAnimatorCompat;
import i.h.l.ViewPropertyAnimatorListener;
import i.h.l.ViewPropertyAnimatorListenerAdapter;
import i.h.l.ViewPropertyAnimatorUpdateListener;
import j.a.a.a.outline;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class WindowDecorActionBar extends ActionBar implements ActionBarOverlayLayout.d {
    public static final Interpolator A = new AccelerateInterpolator();
    public static final Interpolator B = new DecelerateInterpolator();
    public Context a;
    public Context b;
    public ActionBarOverlayLayout c;
    public ActionBarContainer d;

    /* renamed from: e  reason: collision with root package name */
    public DecorToolbar f780e;

    /* renamed from: f  reason: collision with root package name */
    public ActionBarContextView f781f;
    public View g;
    public boolean h;

    /* renamed from: i  reason: collision with root package name */
    public d f782i;

    /* renamed from: j  reason: collision with root package name */
    public ActionMode f783j;

    /* renamed from: k  reason: collision with root package name */
    public ActionMode.a f784k;

    /* renamed from: l  reason: collision with root package name */
    public boolean f785l;

    /* renamed from: m  reason: collision with root package name */
    public ArrayList<a.b> f786m = new ArrayList<>();

    /* renamed from: n  reason: collision with root package name */
    public boolean f787n;

    /* renamed from: o  reason: collision with root package name */
    public int f788o = 0;

    /* renamed from: p  reason: collision with root package name */
    public boolean f789p = true;

    /* renamed from: q  reason: collision with root package name */
    public boolean f790q;

    /* renamed from: r  reason: collision with root package name */
    public boolean f791r;

    /* renamed from: s  reason: collision with root package name */
    public boolean f792s;

    /* renamed from: t  reason: collision with root package name */
    public boolean f793t = true;
    public ViewPropertyAnimatorCompatSet u;
    public boolean v;
    public boolean w;
    public final ViewPropertyAnimatorListener x = new a();
    public final ViewPropertyAnimatorListener y = new b();
    public final ViewPropertyAnimatorUpdateListener z = new c();

    public class a extends ViewPropertyAnimatorListenerAdapter {
        public a() {
        }

        public void a(View view) {
            View view2;
            WindowDecorActionBar windowDecorActionBar = WindowDecorActionBar.this;
            if (windowDecorActionBar.f789p && (view2 = windowDecorActionBar.g) != null) {
                view2.setTranslationY(0.0f);
                WindowDecorActionBar.this.d.setTranslationY(0.0f);
            }
            WindowDecorActionBar.this.d.setVisibility(8);
            WindowDecorActionBar.this.d.setTransitioning(false);
            WindowDecorActionBar windowDecorActionBar2 = WindowDecorActionBar.this;
            windowDecorActionBar2.u = null;
            ActionMode.a aVar = windowDecorActionBar2.f784k;
            if (aVar != null) {
                aVar.a(windowDecorActionBar2.f783j);
                windowDecorActionBar2.f783j = null;
                windowDecorActionBar2.f784k = null;
            }
            ActionBarOverlayLayout actionBarOverlayLayout = WindowDecorActionBar.this.c;
            if (actionBarOverlayLayout != null) {
                ViewCompat.B(actionBarOverlayLayout);
            }
        }
    }

    public class b extends ViewPropertyAnimatorListenerAdapter {
        public b() {
        }

        public void a(View view) {
            WindowDecorActionBar windowDecorActionBar = WindowDecorActionBar.this;
            windowDecorActionBar.u = null;
            windowDecorActionBar.d.requestLayout();
        }
    }

    public class c implements ViewPropertyAnimatorUpdateListener {
        public c() {
        }
    }

    public class d extends ActionMode implements MenuBuilder.a {
        public final Context d;

        /* renamed from: e  reason: collision with root package name */
        public final MenuBuilder f794e;

        /* renamed from: f  reason: collision with root package name */
        public ActionMode.a f795f;
        public WeakReference<View> g;

        public d(Context context, ActionMode.a aVar) {
            this.d = context;
            this.f795f = aVar;
            MenuBuilder menuBuilder = new MenuBuilder(context);
            menuBuilder.f887l = 1;
            this.f794e = menuBuilder;
            menuBuilder.f882e = this;
        }

        public void a(boolean z) {
            super.c = z;
            WindowDecorActionBar.this.f781f.setTitleOptional(z);
        }

        public void b(CharSequence charSequence) {
            WindowDecorActionBar.this.f781f.setTitle(charSequence);
        }

        public Menu c() {
            return this.f794e;
        }

        public MenuInflater d() {
            return new SupportMenuInflater(this.d);
        }

        public CharSequence e() {
            return WindowDecorActionBar.this.f781f.getSubtitle();
        }

        public CharSequence f() {
            return WindowDecorActionBar.this.f781f.getTitle();
        }

        public void g() {
            if (WindowDecorActionBar.this.f782i == this) {
                this.f794e.j();
                try {
                    this.f795f.a(super, this.f794e);
                } finally {
                    this.f794e.i();
                }
            }
        }

        public boolean h() {
            return WindowDecorActionBar.this.f781f.f70s;
        }

        public void b(int i2) {
            WindowDecorActionBar.this.f781f.setTitle(WindowDecorActionBar.this.a.getResources().getString(i2));
        }

        public void a() {
            WindowDecorActionBar windowDecorActionBar = WindowDecorActionBar.this;
            if (windowDecorActionBar.f782i == this) {
                boolean z = windowDecorActionBar.f790q;
                boolean z2 = windowDecorActionBar.f791r;
                boolean z3 = true;
                if (z || z2) {
                    z3 = false;
                }
                if (!z3) {
                    WindowDecorActionBar windowDecorActionBar2 = WindowDecorActionBar.this;
                    windowDecorActionBar2.f783j = super;
                    windowDecorActionBar2.f784k = this.f795f;
                } else {
                    this.f795f.a(super);
                }
                this.f795f = null;
                WindowDecorActionBar.this.e(false);
                ActionBarContextView actionBarContextView = WindowDecorActionBar.this.f781f;
                if (actionBarContextView.f63l == null) {
                    actionBarContextView.b();
                }
                WindowDecorActionBar.this.f780e.k().sendAccessibilityEvent(32);
                WindowDecorActionBar windowDecorActionBar3 = WindowDecorActionBar.this;
                windowDecorActionBar3.c.setHideOnContentScrollEnabled(windowDecorActionBar3.w);
                WindowDecorActionBar.this.f782i = null;
            }
        }

        public View b() {
            WeakReference<View> weakReference = this.g;
            if (weakReference != null) {
                return weakReference.get();
            }
            return null;
        }

        public void a(View view) {
            WindowDecorActionBar.this.f781f.setCustomView(view);
            this.g = new WeakReference<>(view);
        }

        public void a(CharSequence charSequence) {
            WindowDecorActionBar.this.f781f.setSubtitle(charSequence);
        }

        public void a(int i2) {
            WindowDecorActionBar.this.f781f.setSubtitle(WindowDecorActionBar.this.a.getResources().getString(i2));
        }

        public boolean a(MenuBuilder menuBuilder, MenuItem menuItem) {
            ActionMode.a aVar = this.f795f;
            if (aVar != null) {
                return aVar.a(super, menuItem);
            }
            return false;
        }

        public void a(MenuBuilder menuBuilder) {
            if (this.f795f != null) {
                g();
                ActionMenuPresenter actionMenuPresenter = WindowDecorActionBar.this.f781f.f932e;
                if (actionMenuPresenter != null) {
                    actionMenuPresenter.f();
                }
            }
        }
    }

    public WindowDecorActionBar(Activity activity, boolean z2) {
        new ArrayList();
        View decorView = activity.getWindow().getDecorView();
        a(decorView);
        if (!z2) {
            this.g = decorView.findViewById(16908290);
        }
    }

    public final void a(View view) {
        DecorToolbar decorToolbar;
        ActionBarOverlayLayout actionBarOverlayLayout = (ActionBarOverlayLayout) view.findViewById(f.decor_content_parent);
        this.c = actionBarOverlayLayout;
        if (actionBarOverlayLayout != null) {
            actionBarOverlayLayout.setActionBarVisibilityCallback(this);
        }
        View findViewById = view.findViewById(f.action_bar);
        if (findViewById instanceof DecorToolbar) {
            decorToolbar = (DecorToolbar) findViewById;
        } else if (findViewById instanceof Toolbar) {
            decorToolbar = ((Toolbar) findViewById).getWrapper();
        } else {
            StringBuilder a2 = outline.a("Can't make a decor toolbar out of ");
            a2.append(findViewById != null ? findViewById.getClass().getSimpleName() : "null");
            throw new IllegalStateException(a2.toString());
        }
        this.f780e = decorToolbar;
        this.f781f = (ActionBarContextView) view.findViewById(f.action_context_bar);
        ActionBarContainer actionBarContainer = (ActionBarContainer) view.findViewById(f.action_bar_container);
        this.d = actionBarContainer;
        DecorToolbar decorToolbar2 = this.f780e;
        if (decorToolbar2 == null || this.f781f == null || actionBarContainer == null) {
            throw new IllegalStateException(WindowDecorActionBar.class.getSimpleName() + " can only be used with a compatible window decor layout");
        }
        this.a = decorToolbar2.c();
        boolean z2 = (this.f780e.i() & 4) != 0;
        if (z2) {
            this.h = true;
        }
        Context context = this.a;
        this.f780e.a((context.getApplicationInfo().targetSdkVersion < 14) || z2);
        f(context.getResources().getBoolean(i.b.b.abc_action_bar_embed_tabs));
        TypedArray obtainStyledAttributes = this.a.obtainStyledAttributes(null, j.ActionBar, i.b.a.actionBarStyle, 0);
        if (obtainStyledAttributes.getBoolean(j.ActionBar_hideOnContentScroll, false)) {
            ActionBarOverlayLayout actionBarOverlayLayout2 = this.c;
            if (actionBarOverlayLayout2.f74i) {
                this.w = true;
                actionBarOverlayLayout2.setHideOnContentScrollEnabled(true);
            } else {
                throw new IllegalStateException("Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to enable hide on content scroll");
            }
        }
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(j.ActionBar_elevation, 0);
        if (dimensionPixelSize != 0) {
            ViewCompat.a(this.d, (float) dimensionPixelSize);
        }
        obtainStyledAttributes.recycle();
    }

    public boolean b() {
        DecorToolbar decorToolbar = this.f780e;
        if (decorToolbar == null || !decorToolbar.n()) {
            return false;
        }
        this.f780e.collapseActionView();
        return true;
    }

    public void c(boolean z2) {
        int i2 = z2 ? 4 : 0;
        int i3 = this.f780e.i();
        this.h = true;
        this.f780e.c((i2 & 4) | (-5 & i3));
    }

    public void d(boolean z2) {
        ViewPropertyAnimatorCompatSet viewPropertyAnimatorCompatSet;
        this.v = z2;
        if (!z2 && (viewPropertyAnimatorCompatSet = this.u) != null) {
            viewPropertyAnimatorCompatSet.a();
        }
    }

    public void e(boolean z2) {
        ViewPropertyAnimatorCompat viewPropertyAnimatorCompat;
        ViewPropertyAnimatorCompat viewPropertyAnimatorCompat2;
        if (z2) {
            if (!this.f792s) {
                this.f792s = true;
                ActionBarOverlayLayout actionBarOverlayLayout = this.c;
                if (actionBarOverlayLayout != null) {
                    actionBarOverlayLayout.setShowingForActionMode(true);
                }
                g(false);
            }
        } else if (this.f792s) {
            this.f792s = false;
            ActionBarOverlayLayout actionBarOverlayLayout2 = this.c;
            if (actionBarOverlayLayout2 != null) {
                actionBarOverlayLayout2.setShowingForActionMode(false);
            }
            g(false);
        }
        if (ViewCompat.w(this.d)) {
            if (z2) {
                viewPropertyAnimatorCompat = this.f780e.a(4, 100);
                viewPropertyAnimatorCompat2 = this.f781f.a(0, 200);
            } else {
                viewPropertyAnimatorCompat2 = this.f780e.a(0, 200);
                viewPropertyAnimatorCompat = this.f781f.a(8, 100);
            }
            ViewPropertyAnimatorCompatSet viewPropertyAnimatorCompatSet = new ViewPropertyAnimatorCompatSet();
            viewPropertyAnimatorCompatSet.a.add(viewPropertyAnimatorCompat);
            View view = viewPropertyAnimatorCompat.a.get();
            long duration = view != null ? view.animate().getDuration() : 0;
            View view2 = viewPropertyAnimatorCompat2.a.get();
            if (view2 != null) {
                view2.animate().setStartDelay(duration);
            }
            viewPropertyAnimatorCompatSet.a.add(viewPropertyAnimatorCompat2);
            viewPropertyAnimatorCompatSet.b();
        } else if (z2) {
            this.f780e.a(4);
            this.f781f.setVisibility(0);
        } else {
            this.f780e.a(0);
            this.f781f.setVisibility(8);
        }
    }

    public final void f(boolean z2) {
        this.f787n = z2;
        if (!z2) {
            this.f780e.a((ScrollingTabContainerView) null);
            this.d.setTabContainer(null);
        } else {
            this.d.setTabContainer(null);
            this.f780e.a((ScrollingTabContainerView) null);
        }
        boolean z3 = true;
        boolean z4 = this.f780e.l() == 2;
        this.f780e.b(!this.f787n && z4);
        ActionBarOverlayLayout actionBarOverlayLayout = this.c;
        if (this.f787n || !z4) {
            z3 = false;
        }
        actionBarOverlayLayout.setHasNonEmbeddedTabs(z3);
    }

    public final void g(boolean z2) {
        View view;
        View view2;
        View view3;
        if (this.f792s || !this.f791r) {
            if (!this.f793t) {
                this.f793t = true;
                ViewPropertyAnimatorCompatSet viewPropertyAnimatorCompatSet = this.u;
                if (viewPropertyAnimatorCompatSet != null) {
                    viewPropertyAnimatorCompatSet.a();
                }
                this.d.setVisibility(0);
                if (this.f788o != 0 || (!this.v && !z2)) {
                    this.d.setAlpha(1.0f);
                    this.d.setTranslationY(0.0f);
                    if (this.f789p && (view2 = this.g) != null) {
                        view2.setTranslationY(0.0f);
                    }
                    this.y.a(null);
                } else {
                    this.d.setTranslationY(0.0f);
                    float f2 = (float) (-this.d.getHeight());
                    if (z2) {
                        int[] iArr = {0, 0};
                        this.d.getLocationInWindow(iArr);
                        f2 -= (float) iArr[1];
                    }
                    this.d.setTranslationY(f2);
                    ViewPropertyAnimatorCompatSet viewPropertyAnimatorCompatSet2 = new ViewPropertyAnimatorCompatSet();
                    ViewPropertyAnimatorCompat a2 = ViewCompat.a(this.d);
                    a2.b(0.0f);
                    a2.a(this.z);
                    if (!viewPropertyAnimatorCompatSet2.f843e) {
                        viewPropertyAnimatorCompatSet2.a.add(a2);
                    }
                    if (this.f789p && (view3 = this.g) != null) {
                        view3.setTranslationY(f2);
                        ViewPropertyAnimatorCompat a3 = ViewCompat.a(this.g);
                        a3.b(0.0f);
                        if (!viewPropertyAnimatorCompatSet2.f843e) {
                            viewPropertyAnimatorCompatSet2.a.add(a3);
                        }
                    }
                    Interpolator interpolator = B;
                    if (!viewPropertyAnimatorCompatSet2.f843e) {
                        viewPropertyAnimatorCompatSet2.c = interpolator;
                    }
                    if (!viewPropertyAnimatorCompatSet2.f843e) {
                        viewPropertyAnimatorCompatSet2.b = 250;
                    }
                    ViewPropertyAnimatorListener viewPropertyAnimatorListener = this.y;
                    if (!viewPropertyAnimatorCompatSet2.f843e) {
                        viewPropertyAnimatorCompatSet2.d = viewPropertyAnimatorListener;
                    }
                    this.u = viewPropertyAnimatorCompatSet2;
                    viewPropertyAnimatorCompatSet2.b();
                }
                ActionBarOverlayLayout actionBarOverlayLayout = this.c;
                if (actionBarOverlayLayout != null) {
                    ViewCompat.B(actionBarOverlayLayout);
                }
            }
        } else if (this.f793t) {
            this.f793t = false;
            ViewPropertyAnimatorCompatSet viewPropertyAnimatorCompatSet3 = this.u;
            if (viewPropertyAnimatorCompatSet3 != null) {
                viewPropertyAnimatorCompatSet3.a();
            }
            if (this.f788o != 0 || (!this.v && !z2)) {
                this.x.a(null);
                return;
            }
            this.d.setAlpha(1.0f);
            this.d.setTransitioning(true);
            ViewPropertyAnimatorCompatSet viewPropertyAnimatorCompatSet4 = new ViewPropertyAnimatorCompatSet();
            float f3 = (float) (-this.d.getHeight());
            if (z2) {
                int[] iArr2 = {0, 0};
                this.d.getLocationInWindow(iArr2);
                f3 -= (float) iArr2[1];
            }
            ViewPropertyAnimatorCompat a4 = ViewCompat.a(this.d);
            a4.b(f3);
            a4.a(this.z);
            if (!viewPropertyAnimatorCompatSet4.f843e) {
                viewPropertyAnimatorCompatSet4.a.add(a4);
            }
            if (this.f789p && (view = this.g) != null) {
                ViewPropertyAnimatorCompat a5 = ViewCompat.a(view);
                a5.b(f3);
                if (!viewPropertyAnimatorCompatSet4.f843e) {
                    viewPropertyAnimatorCompatSet4.a.add(a5);
                }
            }
            Interpolator interpolator2 = A;
            if (!viewPropertyAnimatorCompatSet4.f843e) {
                viewPropertyAnimatorCompatSet4.c = interpolator2;
            }
            if (!viewPropertyAnimatorCompatSet4.f843e) {
                viewPropertyAnimatorCompatSet4.b = 250;
            }
            ViewPropertyAnimatorListener viewPropertyAnimatorListener2 = this.x;
            if (!viewPropertyAnimatorCompatSet4.f843e) {
                viewPropertyAnimatorCompatSet4.d = viewPropertyAnimatorListener2;
            }
            this.u = viewPropertyAnimatorCompatSet4;
            viewPropertyAnimatorCompatSet4.b();
        }
    }

    public void b(boolean z2) {
        if (!this.h) {
            c(z2);
        }
    }

    public int c() {
        return this.f780e.i();
    }

    public Context d() {
        if (this.b == null) {
            TypedValue typedValue = new TypedValue();
            this.a.getTheme().resolveAttribute(i.b.a.actionBarWidgetTheme, typedValue, true);
            int i2 = typedValue.resourceId;
            if (i2 != 0) {
                this.b = new ContextThemeWrapper(this.a, i2);
            } else {
                this.b = this.a;
            }
        }
        return this.b;
    }

    public WindowDecorActionBar(Dialog dialog) {
        new ArrayList();
        a(dialog.getWindow().getDecorView());
    }

    public void a(Configuration configuration) {
        f(this.a.getResources().getBoolean(i.b.b.abc_action_bar_embed_tabs));
    }

    public void a(boolean z2) {
        if (z2 != this.f785l) {
            this.f785l = z2;
            int size = this.f786m.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.f786m.get(i2).a(z2);
            }
        }
    }

    public void a(CharSequence charSequence) {
        this.f780e.setWindowTitle(charSequence);
    }

    public ActionMode a(ActionMode.a aVar) {
        d dVar = this.f782i;
        if (dVar != null) {
            dVar.a();
        }
        this.c.setHideOnContentScrollEnabled(false);
        this.f781f.b();
        d dVar2 = new d(this.f781f.getContext(), aVar);
        dVar2.f794e.j();
        try {
            if (!dVar2.f795f.b(dVar2, dVar2.f794e)) {
                return null;
            }
            this.f782i = dVar2;
            dVar2.g();
            this.f781f.a(dVar2);
            e(true);
            this.f781f.sendAccessibilityEvent(32);
            return dVar2;
        } finally {
            dVar2.f794e.i();
        }
    }

    public boolean a(int i2, KeyEvent keyEvent) {
        MenuBuilder menuBuilder;
        d dVar = this.f782i;
        if (dVar == null || (menuBuilder = dVar.f794e) == null) {
            return false;
        }
        boolean z2 = true;
        if (KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() == 1) {
            z2 = false;
        }
        menuBuilder.setQwertyMode(z2);
        return menuBuilder.performShortcut(i2, keyEvent, 0);
    }
}
