package i.b.p;

import android.content.Context;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import i.b.p.ActionMode;
import i.b.p.i.MenuItemWrapperICS;
import i.b.p.i.MenuWrapperICS;
import i.e.SimpleArrayMap;
import i.h.g.a.SupportMenu;
import i.h.g.a.SupportMenuItem;
import java.util.ArrayList;

public class SupportActionModeWrapper extends ActionMode {
    public final Context a;
    public final ActionMode b;

    public SupportActionModeWrapper(Context context, ActionMode actionMode) {
        this.a = context;
        this.b = actionMode;
    }

    public void finish() {
        this.b.a();
    }

    public View getCustomView() {
        return this.b.b();
    }

    public Menu getMenu() {
        return new MenuWrapperICS(this.a, (SupportMenu) this.b.c());
    }

    public MenuInflater getMenuInflater() {
        return this.b.d();
    }

    public CharSequence getSubtitle() {
        return this.b.e();
    }

    public Object getTag() {
        return this.b.b;
    }

    public CharSequence getTitle() {
        return this.b.f();
    }

    public boolean getTitleOptionalHint() {
        return this.b.c;
    }

    public void invalidate() {
        this.b.g();
    }

    public boolean isTitleOptional() {
        return this.b.h();
    }

    public void setCustomView(View view) {
        this.b.a(view);
    }

    public void setSubtitle(CharSequence charSequence) {
        this.b.a(charSequence);
    }

    public void setTag(Object obj) {
        this.b.b = obj;
    }

    public void setTitle(CharSequence charSequence) {
        this.b.b(charSequence);
    }

    public void setTitleOptionalHint(boolean z) {
        this.b.a(z);
    }

    public void setSubtitle(int i2) {
        this.b.a(i2);
    }

    public void setTitle(int i2) {
        this.b.b(i2);
    }

    public static class a implements ActionMode.a {
        public final ActionMode.Callback a;
        public final Context b;
        public final ArrayList<e> c = new ArrayList<>();
        public final SimpleArrayMap<Menu, Menu> d = new SimpleArrayMap<>();

        public a(Context context, ActionMode.Callback callback) {
            this.b = context;
            this.a = callback;
        }

        public boolean a(ActionMode actionMode, Menu menu) {
            return this.a.onPrepareActionMode(b(actionMode), a(menu));
        }

        public boolean b(ActionMode actionMode, Menu menu) {
            return this.a.onCreateActionMode(b(actionMode), a(menu));
        }

        public boolean a(ActionMode actionMode, MenuItem menuItem) {
            return this.a.onActionItemClicked(b(actionMode), new MenuItemWrapperICS(this.b, (SupportMenuItem) menuItem));
        }

        public android.view.ActionMode b(ActionMode actionMode) {
            int size = this.c.size();
            for (int i2 = 0; i2 < size; i2++) {
                SupportActionModeWrapper supportActionModeWrapper = this.c.get(i2);
                if (supportActionModeWrapper != null && supportActionModeWrapper.b == actionMode) {
                    return supportActionModeWrapper;
                }
            }
            SupportActionModeWrapper supportActionModeWrapper2 = new SupportActionModeWrapper(this.b, actionMode);
            this.c.add(supportActionModeWrapper2);
            return supportActionModeWrapper2;
        }

        public void a(ActionMode actionMode) {
            this.a.onDestroyActionMode(b(actionMode));
        }

        public final Menu a(Menu menu) {
            Menu orDefault = this.d.getOrDefault(menu, null);
            if (orDefault != null) {
                return orDefault;
            }
            MenuWrapperICS menuWrapperICS = new MenuWrapperICS(this.b, (SupportMenu) menu);
            this.d.put(menu, menuWrapperICS);
            return menuWrapperICS;
        }
    }
}
