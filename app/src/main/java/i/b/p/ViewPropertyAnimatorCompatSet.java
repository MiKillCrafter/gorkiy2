package i.b.p;

import android.view.View;
import android.view.animation.Interpolator;
import i.h.l.ViewPropertyAnimatorCompat;
import i.h.l.ViewPropertyAnimatorListener;
import i.h.l.ViewPropertyAnimatorListenerAdapter;
import i.h.l.s;
import java.util.ArrayList;
import java.util.Iterator;

public class ViewPropertyAnimatorCompatSet {
    public final ArrayList<s> a = new ArrayList<>();
    public long b = -1;
    public Interpolator c;
    public ViewPropertyAnimatorListener d;

    /* renamed from: e  reason: collision with root package name */
    public boolean f843e;

    /* renamed from: f  reason: collision with root package name */
    public final ViewPropertyAnimatorListenerAdapter f844f = new a();

    public class a extends ViewPropertyAnimatorListenerAdapter {
        public boolean a = false;
        public int b = 0;

        public a() {
        }

        public void a(View view) {
            int i2 = this.b + 1;
            this.b = i2;
            if (i2 == ViewPropertyAnimatorCompatSet.this.a.size()) {
                ViewPropertyAnimatorListener viewPropertyAnimatorListener = ViewPropertyAnimatorCompatSet.this.d;
                if (viewPropertyAnimatorListener != null) {
                    viewPropertyAnimatorListener.a(null);
                }
                this.b = 0;
                this.a = false;
                ViewPropertyAnimatorCompatSet.this.f843e = false;
            }
        }

        public void b(View view) {
            if (!this.a) {
                this.a = true;
                ViewPropertyAnimatorListener viewPropertyAnimatorListener = ViewPropertyAnimatorCompatSet.this.d;
                if (viewPropertyAnimatorListener != null) {
                    viewPropertyAnimatorListener.b(null);
                }
            }
        }
    }

    public void a() {
        if (this.f843e) {
            Iterator<s> it = this.a.iterator();
            while (it.hasNext()) {
                it.next().a();
            }
            this.f843e = false;
        }
    }

    public void b() {
        View view;
        if (!this.f843e) {
            Iterator<s> it = this.a.iterator();
            while (it.hasNext()) {
                ViewPropertyAnimatorCompat next = it.next();
                long j2 = this.b;
                if (j2 >= 0) {
                    next.a(j2);
                }
                Interpolator interpolator = this.c;
                if (!(interpolator == null || (view = next.a.get()) == null)) {
                    view.animate().setInterpolator(interpolator);
                }
                if (this.d != null) {
                    next.a(this.f844f);
                }
                View view2 = next.a.get();
                if (view2 != null) {
                    view2.animate().start();
                }
            }
            this.f843e = true;
        }
    }
}
