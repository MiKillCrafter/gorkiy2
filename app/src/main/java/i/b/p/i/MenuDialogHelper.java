package i.b.p.i;

import android.content.DialogInterface;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import i.b.k.AlertDialog;
import i.b.p.i.MenuPresenter;

public class MenuDialogHelper implements DialogInterface.OnKeyListener, DialogInterface.OnClickListener, DialogInterface.OnDismissListener, MenuPresenter.a {
    public MenuBuilder b;
    public AlertDialog c;
    public ListMenuPresenter d;

    public MenuDialogHelper(MenuBuilder menuBuilder) {
        this.b = menuBuilder;
    }

    public void a(MenuBuilder menuBuilder, boolean z) {
        AlertDialog alertDialog;
        if ((z || menuBuilder == this.b) && (alertDialog = this.c) != null) {
            alertDialog.dismiss();
        }
    }

    public boolean a(MenuBuilder menuBuilder) {
        return false;
    }

    public void onClick(DialogInterface dialogInterface, int i2) {
        this.b.a((MenuItemImpl) this.d.a().getItem(i2), 0);
    }

    public void onDismiss(DialogInterface dialogInterface) {
        ListMenuPresenter listMenuPresenter = this.d;
        MenuBuilder menuBuilder = this.b;
        MenuPresenter.a aVar = listMenuPresenter.f878i;
        if (aVar != null) {
            aVar.a(menuBuilder, true);
        }
    }

    public boolean onKey(DialogInterface dialogInterface, int i2, KeyEvent keyEvent) {
        Window window;
        View decorView;
        KeyEvent.DispatcherState keyDispatcherState;
        View decorView2;
        KeyEvent.DispatcherState keyDispatcherState2;
        if (i2 == 82 || i2 == 4) {
            if (keyEvent.getAction() == 0 && keyEvent.getRepeatCount() == 0) {
                Window window2 = this.c.getWindow();
                if (!(window2 == null || (decorView2 = window2.getDecorView()) == null || (keyDispatcherState2 = decorView2.getKeyDispatcherState()) == null)) {
                    keyDispatcherState2.startTracking(keyEvent, this);
                    return true;
                }
            } else if (keyEvent.getAction() == 1 && !keyEvent.isCanceled() && (window = this.c.getWindow()) != null && (decorView = window.getDecorView()) != null && (keyDispatcherState = decorView.getKeyDispatcherState()) != null && keyDispatcherState.isTracking(keyEvent)) {
                this.b.a(true);
                dialogInterface.dismiss();
                return true;
            }
        }
        return this.b.performShortcut(i2, keyEvent, 0);
    }
}
