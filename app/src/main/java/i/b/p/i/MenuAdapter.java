package i.b.p.i;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import androidx.appcompat.view.menu.ListMenuItemView;
import i.b.p.i.MenuView;
import java.util.ArrayList;

public class MenuAdapter extends BaseAdapter {
    public MenuBuilder b;
    public int c = -1;
    public boolean d;

    /* renamed from: e  reason: collision with root package name */
    public final boolean f880e;

    /* renamed from: f  reason: collision with root package name */
    public final LayoutInflater f881f;
    public final int g;

    public MenuAdapter(MenuBuilder menuBuilder, LayoutInflater layoutInflater, boolean z, int i2) {
        this.f880e = z;
        this.f881f = layoutInflater;
        this.b = menuBuilder;
        this.g = i2;
        a();
    }

    public void a() {
        MenuBuilder menuBuilder = this.b;
        MenuItemImpl menuItemImpl = menuBuilder.w;
        if (menuItemImpl != null) {
            menuBuilder.a();
            ArrayList<i> arrayList = menuBuilder.f885j;
            int size = arrayList.size();
            for (int i2 = 0; i2 < size; i2++) {
                if (((MenuItemImpl) arrayList.get(i2)) == menuItemImpl) {
                    this.c = i2;
                    return;
                }
            }
        }
        this.c = -1;
    }

    public int getCount() {
        ArrayList<i> arrayList;
        if (this.f880e) {
            MenuBuilder menuBuilder = this.b;
            menuBuilder.a();
            arrayList = menuBuilder.f885j;
        } else {
            arrayList = this.b.d();
        }
        if (this.c < 0) {
            return arrayList.size();
        }
        return arrayList.size() - 1;
    }

    public long getItemId(int i2) {
        return (long) i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i2, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = this.f881f.inflate(this.g, viewGroup, false);
        }
        int i3 = getItem(i2).b;
        int i4 = i2 - 1;
        ListMenuItemView listMenuItemView = (ListMenuItemView) view;
        listMenuItemView.setGroupDividerEnabled(this.b.e() && i3 != (i4 >= 0 ? getItem(i4).b : i3));
        MenuView.a aVar = (MenuView.a) view;
        if (this.d) {
            listMenuItemView.setForceShowIcon(true);
        }
        aVar.a(getItem(i2), 0);
        return view;
    }

    public void notifyDataSetChanged() {
        a();
        super.notifyDataSetChanged();
    }

    public MenuItemImpl getItem(int i2) {
        ArrayList<i> arrayList;
        if (this.f880e) {
            MenuBuilder menuBuilder = this.b;
            menuBuilder.a();
            arrayList = menuBuilder.f885j;
        } else {
            arrayList = this.b.d();
        }
        int i3 = this.c;
        if (i3 >= 0 && i2 >= i3) {
            i2++;
        }
        return (MenuItemImpl) arrayList.get(i2);
    }
}
