package i.b.p.i;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;

public class SubMenuBuilder extends MenuBuilder implements SubMenu {
    public MenuBuilder A;
    public MenuItemImpl B;

    public SubMenuBuilder(Context context, MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
        super(context);
        this.A = super;
        this.B = menuItemImpl;
    }

    public boolean a(MenuBuilder menuBuilder, MenuItem menuItem) {
        return super.a(super, menuItem) || this.A.a(super, menuItem);
    }

    public boolean b(MenuItemImpl menuItemImpl) {
        return this.A.b(menuItemImpl);
    }

    public MenuBuilder c() {
        return this.A.c();
    }

    public boolean e() {
        return this.A.e();
    }

    public boolean f() {
        return this.A.f();
    }

    public boolean g() {
        return this.A.g();
    }

    public MenuItem getItem() {
        return this.B;
    }

    public void setGroupDividerEnabled(boolean z) {
        this.A.setGroupDividerEnabled(z);
    }

    public SubMenu setHeaderIcon(Drawable drawable) {
        a(0, null, 0, drawable, null);
        return this;
    }

    public SubMenu setHeaderTitle(CharSequence charSequence) {
        a(0, charSequence, 0, null, null);
        return this;
    }

    public SubMenu setHeaderView(View view) {
        a(0, null, 0, null, view);
        return this;
    }

    public SubMenu setIcon(Drawable drawable) {
        this.B.setIcon(drawable);
        return this;
    }

    public void setQwertyMode(boolean z) {
        this.A.setQwertyMode(z);
    }

    public String b() {
        MenuItemImpl menuItemImpl = this.B;
        int i2 = menuItemImpl != null ? menuItemImpl.a : 0;
        if (i2 == 0) {
            return null;
        }
        return "android:menu:actionviewstates" + ":" + i2;
    }

    public SubMenu setHeaderIcon(int i2) {
        a(0, null, i2, null, null);
        return this;
    }

    public SubMenu setHeaderTitle(int i2) {
        a(i2, null, 0, null, null);
        return this;
    }

    public SubMenu setIcon(int i2) {
        this.B.setIcon(i2);
        return this;
    }

    public boolean a(MenuItemImpl menuItemImpl) {
        return this.A.a(menuItemImpl);
    }
}
