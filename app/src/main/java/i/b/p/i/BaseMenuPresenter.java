package i.b.p.i;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.view.menu.ActionMenuItemView;
import androidx.appcompat.widget.ActionMenuView;
import i.b.p.i.MenuPresenter;
import i.b.p.i.MenuView;
import i.b.q.ActionMenuPresenter;
import java.util.ArrayList;

public abstract class BaseMenuPresenter implements MenuPresenter {
    public Context b;
    public Context c;
    public MenuBuilder d;

    /* renamed from: e  reason: collision with root package name */
    public LayoutInflater f858e;

    /* renamed from: f  reason: collision with root package name */
    public MenuPresenter.a f859f;
    public int g;
    public int h;

    /* renamed from: i  reason: collision with root package name */
    public MenuView f860i;

    public BaseMenuPresenter(Context context, int i2, int i3) {
        this.b = context;
        this.f858e = LayoutInflater.from(context);
        this.g = i2;
        this.h = i3;
    }

    public void a(boolean z) {
        int i2;
        boolean z2;
        ViewGroup viewGroup = (ViewGroup) this.f860i;
        if (viewGroup != null) {
            MenuBuilder menuBuilder = this.d;
            if (menuBuilder != null) {
                menuBuilder.a();
                ArrayList<i> d2 = this.d.d();
                int size = d2.size();
                i2 = 0;
                for (int i3 = 0; i3 < size; i3++) {
                    MenuItemImpl menuItemImpl = d2.get(i3);
                    if (menuItemImpl.d()) {
                        View childAt = viewGroup.getChildAt(i2);
                        MenuItemImpl itemData = childAt instanceof MenuView.a ? ((MenuView.a) childAt).getItemData() : null;
                        View a = a(menuItemImpl, childAt, viewGroup);
                        if (menuItemImpl != itemData) {
                            a.setPressed(false);
                            a.jumpDrawablesToCurrentState();
                        }
                        if (a != childAt) {
                            ViewGroup viewGroup2 = (ViewGroup) a.getParent();
                            if (viewGroup2 != null) {
                                viewGroup2.removeView(a);
                            }
                            ((ViewGroup) this.f860i).addView(a, i2);
                        }
                        i2++;
                    }
                }
            } else {
                i2 = 0;
            }
            while (i2 < viewGroup.getChildCount()) {
                if (viewGroup.getChildAt(i2) == ((ActionMenuPresenter) this).f935j) {
                    z2 = false;
                } else {
                    viewGroup.removeViewAt(i2);
                    z2 = true;
                }
                if (!z2) {
                    i2++;
                }
            }
        }
    }

    public boolean a(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
        return false;
    }

    public boolean b(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
        return false;
    }

    public void a(MenuPresenter.a aVar) {
        this.f859f = aVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View a(MenuItemImpl menuItemImpl, View view, ViewGroup viewGroup) {
        MenuView.a aVar;
        if (view instanceof MenuView.a) {
            aVar = (MenuView.a) view;
        } else {
            aVar = (MenuView.a) this.f858e.inflate(this.h, viewGroup, false);
        }
        ActionMenuPresenter actionMenuPresenter = (ActionMenuPresenter) this;
        aVar.a(menuItemImpl, 0);
        ActionMenuItemView actionMenuItemView = (ActionMenuItemView) aVar;
        actionMenuItemView.setItemInvoker((ActionMenuView) actionMenuPresenter.f860i);
        if (actionMenuPresenter.x == null) {
            actionMenuPresenter.x = new ActionMenuPresenter.b();
        }
        actionMenuItemView.setPopupCallback(actionMenuPresenter.x);
        return (View) aVar;
    }

    public boolean a(SubMenuBuilder subMenuBuilder) {
        MenuPresenter.a aVar = this.f859f;
        if (aVar != null) {
            return aVar.a(subMenuBuilder);
        }
        return false;
    }
}
