package i.t;

import i.o.LifecycleOwner;

public interface SavedStateRegistryOwner extends LifecycleOwner {
    SavedStateRegistry d();
}
