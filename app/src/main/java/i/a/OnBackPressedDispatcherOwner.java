package i.a;

import androidx.activity.OnBackPressedDispatcher;
import i.o.LifecycleOwner;

public interface OnBackPressedDispatcherOwner extends LifecycleOwner {
    OnBackPressedDispatcher c();
}
