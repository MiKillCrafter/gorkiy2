package i.h.h;

import android.os.Build;
import android.os.Trace;
import android.util.Log;

public final class TraceCompat {
    static {
        Class<String> cls = String.class;
        if (Build.VERSION.SDK_INT < 29) {
            try {
                Trace.class.getField("TRACE_TAG_APP").getLong(null);
                Trace.class.getMethod("isTagEnabled", Long.TYPE);
                Trace.class.getMethod("asyncTraceBegin", Long.TYPE, cls, Integer.TYPE);
                Trace.class.getMethod("asyncTraceEnd", Long.TYPE, cls, Integer.TYPE);
                Trace.class.getMethod("traceCounter", Long.TYPE, cls, Integer.TYPE);
            } catch (Exception e2) {
                Log.i("TraceCompat", "Unable to initialize via reflection.", e2);
            }
        }
    }

    public static void a(String str) {
        Trace.beginSection(str);
    }

    public static void a() {
        Trace.endSection();
    }
}
