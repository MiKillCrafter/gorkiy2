package i.h.d;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import i.h.e.ContextCompat;

public class ActivityCompat extends ContextCompat {

    public static class a implements Runnable {
        public final /* synthetic */ String[] b;
        public final /* synthetic */ Activity c;
        public final /* synthetic */ int d;

        public a(String[] strArr, Activity activity, int i2) {
            this.b = strArr;
            this.c = activity;
            this.d = i2;
        }

        public void run() {
            int[] iArr = new int[this.b.length];
            PackageManager packageManager = this.c.getPackageManager();
            String packageName = this.c.getPackageName();
            int length = this.b.length;
            for (int i2 = 0; i2 < length; i2++) {
                iArr[i2] = packageManager.checkPermission(this.b[i2], packageName);
            }
            ((b) this.c).onRequestPermissionsResult(this.d, this.b, iArr);
        }
    }

    public interface b {
        void onRequestPermissionsResult(int i2, String[] strArr, int[] iArr);
    }

    public interface c {
    }

    public interface d {
        void a(int i2);
    }

    public static c a() {
        return null;
    }

    public static void a(Activity activity, Intent intent, int i2, Bundle bundle) {
        activity.startActivityForResult(intent, i2, bundle);
    }

    public static void b(Activity activity) {
        if (Build.VERSION.SDK_INT >= 28) {
            activity.recreate();
        } else if (!ActivityRecreator.a(activity)) {
            activity.recreate();
        }
    }

    public static void a(Activity activity) {
        activity.finishAffinity();
    }

    public static void a(Activity activity, String[] strArr, int i2) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (activity instanceof d) {
                ((d) activity).a(i2);
            }
            activity.requestPermissions(strArr, i2);
        } else if (activity instanceof b) {
            new Handler(Looper.getMainLooper()).post(new a(strArr, activity, i2));
        }
    }
}
