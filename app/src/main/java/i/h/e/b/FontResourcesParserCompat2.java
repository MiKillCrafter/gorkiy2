package i.h.e.b;

import i.h.i.FontRequest;

/* compiled from: FontResourcesParserCompat */
public final class FontResourcesParserCompat2 implements FontResourcesParserCompat {
    public final FontRequest a;
    public final int b;
    public final int c;

    public FontResourcesParserCompat2(FontRequest fontRequest, int i2, int i3) {
        this.a = fontRequest;
        this.c = i2;
        this.b = i3;
    }
}
