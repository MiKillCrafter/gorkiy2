package i.h.l;

import android.content.Context;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import i.b.p.i.MenuItemWrapperICS;
import j.a.a.a.outline;

public abstract class ActionProvider {
    public a a;

    public interface a {
    }

    public ActionProvider(Context context) {
    }

    public View a(MenuItem menuItem) {
        return ((MenuItemWrapperICS.a) this).b.onCreateActionView();
    }

    public boolean a() {
        return true;
    }

    public boolean b() {
        return false;
    }

    public void a(a aVar) {
        if (!(this.a == null || aVar == null)) {
            StringBuilder a2 = outline.a("setVisibilityListener: Setting a new ActionProvider.VisibilityListener when one is already set. Are you reusing this ");
            a2.append(getClass().getSimpleName());
            a2.append(" instance while it is still in use somewhere else?");
            Log.w("ActionProvider(support)", a2.toString());
        }
        this.a = aVar;
    }
}
