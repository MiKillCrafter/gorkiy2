package i.x.a.a;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.os.Build;
import android.util.AttributeSet;
import i.b.k.ResourcesFlusher;
import i.e.ArrayMap;
import i.h.e.b.ComplexColorCompat;
import i.h.f.PathParser;
import i.x.a.a.g;
import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParser;

public class VectorDrawableCompat extends VectorDrawableCommon {

    /* renamed from: k  reason: collision with root package name */
    public static final PorterDuff.Mode f1483k = PorterDuff.Mode.SRC_IN;
    public h c;
    public PorterDuffColorFilter d;

    /* renamed from: e  reason: collision with root package name */
    public ColorFilter f1484e;

    /* renamed from: f  reason: collision with root package name */
    public boolean f1485f;
    public boolean g;
    public final float[] h;

    /* renamed from: i  reason: collision with root package name */
    public final Matrix f1486i;

    /* renamed from: j  reason: collision with root package name */
    public final Rect f1487j;

    public static class b extends f {
        public b() {
        }

        public boolean b() {
            return true;
        }

        public b(b bVar) {
            super(super);
        }
    }

    public static class c extends f {

        /* renamed from: e  reason: collision with root package name */
        public int[] f1488e;

        /* renamed from: f  reason: collision with root package name */
        public ComplexColorCompat f1489f;
        public float g = 0.0f;
        public ComplexColorCompat h;

        /* renamed from: i  reason: collision with root package name */
        public float f1490i = 1.0f;

        /* renamed from: j  reason: collision with root package name */
        public float f1491j = 1.0f;

        /* renamed from: k  reason: collision with root package name */
        public float f1492k = 0.0f;

        /* renamed from: l  reason: collision with root package name */
        public float f1493l = 1.0f;

        /* renamed from: m  reason: collision with root package name */
        public float f1494m = 0.0f;

        /* renamed from: n  reason: collision with root package name */
        public Paint.Cap f1495n = Paint.Cap.BUTT;

        /* renamed from: o  reason: collision with root package name */
        public Paint.Join f1496o = Paint.Join.MITER;

        /* renamed from: p  reason: collision with root package name */
        public float f1497p = 4.0f;

        public c() {
        }

        public boolean a() {
            return this.h.b() || this.f1489f.b();
        }

        public float getFillAlpha() {
            return this.f1491j;
        }

        public int getFillColor() {
            return this.h.c;
        }

        public float getStrokeAlpha() {
            return this.f1490i;
        }

        public int getStrokeColor() {
            return this.f1489f.c;
        }

        public float getStrokeWidth() {
            return this.g;
        }

        public float getTrimPathEnd() {
            return this.f1493l;
        }

        public float getTrimPathOffset() {
            return this.f1494m;
        }

        public float getTrimPathStart() {
            return this.f1492k;
        }

        public void setFillAlpha(float f2) {
            this.f1491j = f2;
        }

        public void setFillColor(int i2) {
            this.h.c = i2;
        }

        public void setStrokeAlpha(float f2) {
            this.f1490i = f2;
        }

        public void setStrokeColor(int i2) {
            this.f1489f.c = i2;
        }

        public void setStrokeWidth(float f2) {
            this.g = f2;
        }

        public void setTrimPathEnd(float f2) {
            this.f1493l = f2;
        }

        public void setTrimPathOffset(float f2) {
            this.f1494m = f2;
        }

        public void setTrimPathStart(float f2) {
            this.f1492k = f2;
        }

        public boolean a(int[] iArr) {
            return this.f1489f.a(iArr) | this.h.a(iArr);
        }

        public c(c cVar) {
            super(super);
            this.f1488e = cVar.f1488e;
            this.f1489f = cVar.f1489f;
            this.g = cVar.g;
            this.f1490i = cVar.f1490i;
            this.h = cVar.h;
            super.c = super.c;
            this.f1491j = cVar.f1491j;
            this.f1492k = cVar.f1492k;
            this.f1493l = cVar.f1493l;
            this.f1494m = cVar.f1494m;
            this.f1495n = cVar.f1495n;
            this.f1496o = cVar.f1496o;
            this.f1497p = cVar.f1497p;
        }
    }

    public static abstract class e {
        public e() {
        }

        public boolean a() {
            return false;
        }

        public boolean a(int[] iArr) {
            return false;
        }

        public /* synthetic */ e(a aVar) {
        }
    }

    public static class h extends Drawable.ConstantState {
        public int a;
        public g b;
        public ColorStateList c;
        public PorterDuff.Mode d;

        /* renamed from: e  reason: collision with root package name */
        public boolean f1516e;

        /* renamed from: f  reason: collision with root package name */
        public Bitmap f1517f;
        public ColorStateList g;
        public PorterDuff.Mode h;

        /* renamed from: i  reason: collision with root package name */
        public int f1518i;

        /* renamed from: j  reason: collision with root package name */
        public boolean f1519j;

        /* renamed from: k  reason: collision with root package name */
        public boolean f1520k;

        /* renamed from: l  reason: collision with root package name */
        public Paint f1521l;

        public h(h hVar) {
            this.c = null;
            this.d = VectorDrawableCompat.f1483k;
            if (hVar != null) {
                this.a = hVar.a;
                g gVar = new g(hVar.b);
                this.b = gVar;
                if (hVar.b.f1506e != null) {
                    gVar.f1506e = new Paint(hVar.b.f1506e);
                }
                if (hVar.b.d != null) {
                    this.b.d = new Paint(hVar.b.d);
                }
                this.c = hVar.c;
                this.d = hVar.d;
                this.f1516e = hVar.f1516e;
            }
        }

        public void a(int i2, int i3) {
            this.f1517f.eraseColor(0);
            Canvas canvas = new Canvas(this.f1517f);
            g gVar = this.b;
            gVar.a(gVar.h, g.f1505q, canvas, i2, i3, null);
        }

        public int getChangingConfigurations() {
            return this.a;
        }

        public Drawable newDrawable() {
            return new VectorDrawableCompat(this);
        }

        public Drawable newDrawable(Resources resources) {
            return new VectorDrawableCompat(this);
        }

        public boolean a() {
            g gVar = this.b;
            if (gVar.f1514o == null) {
                gVar.f1514o = Boolean.valueOf(gVar.h.a());
            }
            return gVar.f1514o.booleanValue();
        }

        public h() {
            this.c = null;
            this.d = VectorDrawableCompat.f1483k;
            this.b = new g();
        }
    }

    public VectorDrawableCompat() {
        this.g = true;
        this.h = new float[9];
        this.f1486i = new Matrix();
        this.f1487j = new Rect();
        this.c = new h();
    }

    public static VectorDrawableCompat createFromXmlInner(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
        VectorDrawableCompat vectorDrawableCompat = new VectorDrawableCompat();
        vectorDrawableCompat.inflate(resources, xmlPullParser, attributeSet, theme);
        return vectorDrawableCompat;
    }

    public PorterDuffColorFilter a(ColorStateList colorStateList, PorterDuff.Mode mode) {
        if (colorStateList == null || mode == null) {
            return null;
        }
        return new PorterDuffColorFilter(colorStateList.getColorForState(getState(), 0), mode);
    }

    public boolean canApplyTheme() {
        Drawable drawable = super.b;
        if (drawable == null) {
            return false;
        }
        drawable.canApplyTheme();
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00d3, code lost:
        if ((r1 == r7.getWidth() && r3 == r6.f1517f.getHeight()) == false) goto L_0x00d5;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void draw(android.graphics.Canvas r11) {
        /*
            r10 = this;
            android.graphics.drawable.Drawable r0 = r10.b
            if (r0 == 0) goto L_0x0008
            r0.draw(r11)
            return
        L_0x0008:
            android.graphics.Rect r0 = r10.f1487j
            r10.copyBounds(r0)
            android.graphics.Rect r0 = r10.f1487j
            int r0 = r0.width()
            if (r0 <= 0) goto L_0x016b
            android.graphics.Rect r0 = r10.f1487j
            int r0 = r0.height()
            if (r0 > 0) goto L_0x001f
            goto L_0x016b
        L_0x001f:
            android.graphics.ColorFilter r0 = r10.f1484e
            if (r0 != 0) goto L_0x0025
            android.graphics.PorterDuffColorFilter r0 = r10.d
        L_0x0025:
            android.graphics.Matrix r1 = r10.f1486i
            r11.getMatrix(r1)
            android.graphics.Matrix r1 = r10.f1486i
            float[] r2 = r10.h
            r1.getValues(r2)
            float[] r1 = r10.h
            r2 = 0
            r1 = r1[r2]
            float r1 = java.lang.Math.abs(r1)
            float[] r3 = r10.h
            r4 = 4
            r3 = r3[r4]
            float r3 = java.lang.Math.abs(r3)
            float[] r4 = r10.h
            r5 = 1
            r4 = r4[r5]
            float r4 = java.lang.Math.abs(r4)
            float[] r6 = r10.h
            r7 = 3
            r6 = r6[r7]
            float r6 = java.lang.Math.abs(r6)
            r7 = 1065353216(0x3f800000, float:1.0)
            r8 = 0
            int r4 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1))
            if (r4 != 0) goto L_0x0060
            int r4 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r4 == 0) goto L_0x0064
        L_0x0060:
            r1 = 1065353216(0x3f800000, float:1.0)
            r3 = 1065353216(0x3f800000, float:1.0)
        L_0x0064:
            android.graphics.Rect r4 = r10.f1487j
            int r4 = r4.width()
            float r4 = (float) r4
            float r4 = r4 * r1
            int r1 = (int) r4
            android.graphics.Rect r4 = r10.f1487j
            int r4 = r4.height()
            float r4 = (float) r4
            float r4 = r4 * r3
            int r3 = (int) r4
            r4 = 2048(0x800, float:2.87E-42)
            int r1 = java.lang.Math.min(r4, r1)
            int r3 = java.lang.Math.min(r4, r3)
            if (r1 <= 0) goto L_0x016b
            if (r3 > 0) goto L_0x0088
            goto L_0x016b
        L_0x0088:
            int r4 = r11.save()
            android.graphics.Rect r6 = r10.f1487j
            int r9 = r6.left
            float r9 = (float) r9
            int r6 = r6.top
            float r6 = (float) r6
            r11.translate(r9, r6)
            boolean r6 = r10.isAutoMirrored()
            if (r6 == 0) goto L_0x00a5
            int r6 = i.b.k.ResourcesFlusher.b(r10)
            if (r6 != r5) goto L_0x00a5
            r6 = 1
            goto L_0x00a6
        L_0x00a5:
            r6 = 0
        L_0x00a6:
            if (r6 == 0) goto L_0x00b7
            android.graphics.Rect r6 = r10.f1487j
            int r6 = r6.width()
            float r6 = (float) r6
            r11.translate(r6, r8)
            r6 = -1082130432(0xffffffffbf800000, float:-1.0)
            r11.scale(r6, r7)
        L_0x00b7:
            android.graphics.Rect r6 = r10.f1487j
            r6.offsetTo(r2, r2)
            i.x.a.a.VectorDrawableCompat$h r6 = r10.c
            android.graphics.Bitmap r7 = r6.f1517f
            if (r7 == 0) goto L_0x00d5
            int r7 = r7.getWidth()
            if (r1 != r7) goto L_0x00d2
            android.graphics.Bitmap r7 = r6.f1517f
            int r7 = r7.getHeight()
            if (r3 != r7) goto L_0x00d2
            r7 = 1
            goto L_0x00d3
        L_0x00d2:
            r7 = 0
        L_0x00d3:
            if (r7 != 0) goto L_0x00df
        L_0x00d5:
            android.graphics.Bitmap$Config r7 = android.graphics.Bitmap.Config.ARGB_8888
            android.graphics.Bitmap r7 = android.graphics.Bitmap.createBitmap(r1, r3, r7)
            r6.f1517f = r7
            r6.f1520k = r5
        L_0x00df:
            boolean r6 = r10.g
            if (r6 != 0) goto L_0x00e9
            i.x.a.a.VectorDrawableCompat$h r6 = r10.c
            r6.a(r1, r3)
            goto L_0x012d
        L_0x00e9:
            i.x.a.a.VectorDrawableCompat$h r6 = r10.c
            boolean r7 = r6.f1520k
            if (r7 != 0) goto L_0x010d
            android.content.res.ColorStateList r7 = r6.g
            android.content.res.ColorStateList r8 = r6.c
            if (r7 != r8) goto L_0x010d
            android.graphics.PorterDuff$Mode r7 = r6.h
            android.graphics.PorterDuff$Mode r8 = r6.d
            if (r7 != r8) goto L_0x010d
            boolean r7 = r6.f1519j
            boolean r8 = r6.f1516e
            if (r7 != r8) goto L_0x010d
            int r7 = r6.f1518i
            i.x.a.a.VectorDrawableCompat$g r6 = r6.b
            int r6 = r6.getRootAlpha()
            if (r7 != r6) goto L_0x010d
            r6 = 1
            goto L_0x010e
        L_0x010d:
            r6 = 0
        L_0x010e:
            if (r6 != 0) goto L_0x012d
            i.x.a.a.VectorDrawableCompat$h r6 = r10.c
            r6.a(r1, r3)
            i.x.a.a.VectorDrawableCompat$h r1 = r10.c
            android.content.res.ColorStateList r3 = r1.c
            r1.g = r3
            android.graphics.PorterDuff$Mode r3 = r1.d
            r1.h = r3
            i.x.a.a.VectorDrawableCompat$g r3 = r1.b
            int r3 = r3.getRootAlpha()
            r1.f1518i = r3
            boolean r3 = r1.f1516e
            r1.f1519j = r3
            r1.f1520k = r2
        L_0x012d:
            i.x.a.a.VectorDrawableCompat$h r1 = r10.c
            android.graphics.Rect r3 = r10.f1487j
            i.x.a.a.VectorDrawableCompat$g r6 = r1.b
            int r6 = r6.getRootAlpha()
            r7 = 255(0xff, float:3.57E-43)
            if (r6 >= r7) goto L_0x013c
            r2 = 1
        L_0x013c:
            r6 = 0
            if (r2 != 0) goto L_0x0143
            if (r0 != 0) goto L_0x0143
            r0 = r6
            goto L_0x0163
        L_0x0143:
            android.graphics.Paint r2 = r1.f1521l
            if (r2 != 0) goto L_0x0151
            android.graphics.Paint r2 = new android.graphics.Paint
            r2.<init>()
            r1.f1521l = r2
            r2.setFilterBitmap(r5)
        L_0x0151:
            android.graphics.Paint r2 = r1.f1521l
            i.x.a.a.VectorDrawableCompat$g r5 = r1.b
            int r5 = r5.getRootAlpha()
            r2.setAlpha(r5)
            android.graphics.Paint r2 = r1.f1521l
            r2.setColorFilter(r0)
            android.graphics.Paint r0 = r1.f1521l
        L_0x0163:
            android.graphics.Bitmap r1 = r1.f1517f
            r11.drawBitmap(r1, r6, r3, r0)
            r11.restoreToCount(r4)
        L_0x016b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: i.x.a.a.VectorDrawableCompat.draw(android.graphics.Canvas):void");
    }

    public int getAlpha() {
        Drawable drawable = super.b;
        if (drawable != null) {
            return drawable.getAlpha();
        }
        return this.c.b.getRootAlpha();
    }

    public int getChangingConfigurations() {
        Drawable drawable = super.b;
        if (drawable != null) {
            return drawable.getChangingConfigurations();
        }
        return super.getChangingConfigurations() | this.c.getChangingConfigurations();
    }

    public ColorFilter getColorFilter() {
        Drawable drawable = super.b;
        if (drawable != null) {
            return drawable.getColorFilter();
        }
        return this.f1484e;
    }

    public Drawable.ConstantState getConstantState() {
        if (super.b != null && Build.VERSION.SDK_INT >= 24) {
            return new i(super.b.getConstantState());
        }
        this.c.a = getChangingConfigurations();
        return this.c;
    }

    public int getIntrinsicHeight() {
        Drawable drawable = super.b;
        if (drawable != null) {
            return drawable.getIntrinsicHeight();
        }
        return (int) this.c.b.f1509j;
    }

    public int getIntrinsicWidth() {
        Drawable drawable = super.b;
        if (drawable != null) {
            return drawable.getIntrinsicWidth();
        }
        return (int) this.c.b.f1508i;
    }

    public int getOpacity() {
        Drawable drawable = super.b;
        if (drawable != null) {
            return drawable.getOpacity();
        }
        return -3;
    }

    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet) {
        Drawable drawable = super.b;
        if (drawable != null) {
            drawable.inflate(resources, xmlPullParser, attributeSet);
        } else {
            inflate(resources, xmlPullParser, attributeSet, null);
        }
    }

    public void invalidateSelf() {
        Drawable drawable = super.b;
        if (drawable != null) {
            drawable.invalidateSelf();
        } else {
            super.invalidateSelf();
        }
    }

    public boolean isAutoMirrored() {
        Drawable drawable = super.b;
        if (drawable != null) {
            return drawable.isAutoMirrored();
        }
        return this.c.f1516e;
    }

    public boolean isStateful() {
        h hVar;
        ColorStateList colorStateList;
        Drawable drawable = super.b;
        if (drawable != null) {
            return drawable.isStateful();
        }
        return super.isStateful() || ((hVar = this.c) != null && (hVar.a() || ((colorStateList = this.c.c) != null && colorStateList.isStateful())));
    }

    public Drawable mutate() {
        Drawable drawable = super.b;
        if (drawable != null) {
            drawable.mutate();
            return this;
        }
        if (!this.f1485f && super.mutate() == this) {
            this.c = new h(this.c);
            this.f1485f = true;
        }
        return this;
    }

    public void onBoundsChange(Rect rect) {
        Drawable drawable = super.b;
        if (drawable != null) {
            drawable.setBounds(rect);
        }
    }

    public boolean onStateChange(int[] iArr) {
        PorterDuff.Mode mode;
        Drawable drawable = super.b;
        if (drawable != null) {
            return drawable.setState(iArr);
        }
        boolean z = false;
        h hVar = this.c;
        ColorStateList colorStateList = hVar.c;
        if (!(colorStateList == null || (mode = hVar.d) == null)) {
            this.d = a(colorStateList, mode);
            invalidateSelf();
            z = true;
        }
        if (hVar.a()) {
            boolean a2 = hVar.b.h.a(iArr);
            hVar.f1520k |= a2;
            if (a2) {
                invalidateSelf();
                return true;
            }
        }
        return z;
    }

    public void scheduleSelf(Runnable runnable, long j2) {
        Drawable drawable = super.b;
        if (drawable != null) {
            drawable.scheduleSelf(runnable, j2);
        } else {
            super.scheduleSelf(runnable, j2);
        }
    }

    public void setAlpha(int i2) {
        Drawable drawable = super.b;
        if (drawable != null) {
            drawable.setAlpha(i2);
        } else if (this.c.b.getRootAlpha() != i2) {
            this.c.b.setRootAlpha(i2);
            invalidateSelf();
        }
    }

    public void setAutoMirrored(boolean z) {
        Drawable drawable = super.b;
        if (drawable != null) {
            drawable.setAutoMirrored(z);
        } else {
            this.c.f1516e = z;
        }
    }

    public void setColorFilter(ColorFilter colorFilter) {
        Drawable drawable = super.b;
        if (drawable != null) {
            drawable.setColorFilter(colorFilter);
            return;
        }
        this.f1484e = colorFilter;
        invalidateSelf();
    }

    public void setTint(int i2) {
        Drawable drawable = super.b;
        if (drawable != null) {
            ResourcesFlusher.b(drawable, i2);
        } else {
            setTintList(ColorStateList.valueOf(i2));
        }
    }

    public void setTintList(ColorStateList colorStateList) {
        Drawable drawable = super.b;
        if (drawable != null) {
            ResourcesFlusher.a(drawable, colorStateList);
            return;
        }
        h hVar = this.c;
        if (hVar.c != colorStateList) {
            hVar.c = colorStateList;
            this.d = a(colorStateList, hVar.d);
            invalidateSelf();
        }
    }

    public void setTintMode(PorterDuff.Mode mode) {
        Drawable drawable = super.b;
        if (drawable != null) {
            ResourcesFlusher.a(drawable, mode);
            return;
        }
        h hVar = this.c;
        if (hVar.d != mode) {
            hVar.d = mode;
            this.d = a(hVar.c, mode);
            invalidateSelf();
        }
    }

    public boolean setVisible(boolean z, boolean z2) {
        Drawable drawable = super.b;
        if (drawable != null) {
            return drawable.setVisible(z, z2);
        }
        return super.setVisible(z, z2);
    }

    public void unscheduleSelf(Runnable runnable) {
        Drawable drawable = super.b;
        if (drawable != null) {
            drawable.unscheduleSelf(runnable);
        } else {
            super.unscheduleSelf(runnable);
        }
    }

    public static class d extends e {
        public final Matrix a = new Matrix();
        public final ArrayList<g.e> b = new ArrayList<>();
        public float c = 0.0f;
        public float d = 0.0f;

        /* renamed from: e  reason: collision with root package name */
        public float f1498e = 0.0f;

        /* renamed from: f  reason: collision with root package name */
        public float f1499f = 1.0f;
        public float g = 1.0f;
        public float h = 0.0f;

        /* renamed from: i  reason: collision with root package name */
        public float f1500i = 0.0f;

        /* renamed from: j  reason: collision with root package name */
        public final Matrix f1501j = new Matrix();

        /* renamed from: k  reason: collision with root package name */
        public int f1502k;

        /* renamed from: l  reason: collision with root package name */
        public int[] f1503l;

        /* renamed from: m  reason: collision with root package name */
        public String f1504m = null;

        public d(g.d dVar, ArrayMap<String, Object> arrayMap) {
            super(null);
            f fVar;
            this.c = dVar.c;
            this.d = dVar.d;
            this.f1498e = dVar.f1498e;
            this.f1499f = dVar.f1499f;
            this.g = dVar.g;
            this.h = dVar.h;
            this.f1500i = dVar.f1500i;
            this.f1503l = dVar.f1503l;
            String str = dVar.f1504m;
            this.f1504m = str;
            this.f1502k = dVar.f1502k;
            if (str != null) {
                arrayMap.put(str, this);
            }
            this.f1501j.set(dVar.f1501j);
            ArrayList<g.e> arrayList = dVar.b;
            for (int i2 = 0; i2 < arrayList.size(); i2++) {
                d dVar2 = arrayList.get(i2);
                if (dVar2 instanceof d) {
                    this.b.add(new d(dVar2, arrayMap));
                } else {
                    if (dVar2 instanceof c) {
                        fVar = new c((c) dVar2);
                    } else if (dVar2 instanceof b) {
                        fVar = new b((b) dVar2);
                    } else {
                        throw new IllegalStateException("Unknown object in the tree!");
                    }
                    this.b.add(fVar);
                    String str2 = fVar.b;
                    if (str2 != null) {
                        arrayMap.put(str2, fVar);
                    }
                }
            }
        }

        public boolean a() {
            for (int i2 = 0; i2 < this.b.size(); i2++) {
                if (this.b.get(i2).a()) {
                    return true;
                }
            }
            return false;
        }

        public final void b() {
            this.f1501j.reset();
            this.f1501j.postTranslate(-this.d, -this.f1498e);
            this.f1501j.postScale(this.f1499f, this.g);
            this.f1501j.postRotate(this.c, 0.0f, 0.0f);
            this.f1501j.postTranslate(this.h + this.d, this.f1500i + this.f1498e);
        }

        public String getGroupName() {
            return this.f1504m;
        }

        public Matrix getLocalMatrix() {
            return this.f1501j;
        }

        public float getPivotX() {
            return this.d;
        }

        public float getPivotY() {
            return this.f1498e;
        }

        public float getRotation() {
            return this.c;
        }

        public float getScaleX() {
            return this.f1499f;
        }

        public float getScaleY() {
            return this.g;
        }

        public float getTranslateX() {
            return this.h;
        }

        public float getTranslateY() {
            return this.f1500i;
        }

        public void setPivotX(float f2) {
            if (f2 != this.d) {
                this.d = f2;
                b();
            }
        }

        public void setPivotY(float f2) {
            if (f2 != this.f1498e) {
                this.f1498e = f2;
                b();
            }
        }

        public void setRotation(float f2) {
            if (f2 != this.c) {
                this.c = f2;
                b();
            }
        }

        public void setScaleX(float f2) {
            if (f2 != this.f1499f) {
                this.f1499f = f2;
                b();
            }
        }

        public void setScaleY(float f2) {
            if (f2 != this.g) {
                this.g = f2;
                b();
            }
        }

        public void setTranslateX(float f2) {
            if (f2 != this.h) {
                this.h = f2;
                b();
            }
        }

        public void setTranslateY(float f2) {
            if (f2 != this.f1500i) {
                this.f1500i = f2;
                b();
            }
        }

        public boolean a(int[] iArr) {
            boolean z = false;
            for (int i2 = 0; i2 < this.b.size(); i2++) {
                z |= this.b.get(i2).a(iArr);
            }
            return z;
        }

        public d() {
            super(null);
        }
    }

    public static class i extends Drawable.ConstantState {
        public final Drawable.ConstantState a;

        public i(Drawable.ConstantState constantState) {
            this.a = super;
        }

        public boolean canApplyTheme() {
            return this.a.canApplyTheme();
        }

        public int getChangingConfigurations() {
            return this.a.getChangingConfigurations();
        }

        public Drawable newDrawable() {
            VectorDrawableCompat vectorDrawableCompat = new VectorDrawableCompat();
            vectorDrawableCompat.b = (VectorDrawable) this.a.newDrawable();
            return vectorDrawableCompat;
        }

        public Drawable newDrawable(Resources resources) {
            VectorDrawableCompat vectorDrawableCompat = new VectorDrawableCompat();
            vectorDrawableCompat.b = (VectorDrawable) this.a.newDrawable(resources);
            return vectorDrawableCompat;
        }

        public Drawable newDrawable(Resources resources, Resources.Theme theme) {
            VectorDrawableCompat vectorDrawableCompat = new VectorDrawableCompat();
            vectorDrawableCompat.b = (VectorDrawable) this.a.newDrawable(resources, theme);
            return vectorDrawableCompat;
        }
    }

    public static abstract class f extends e {
        public PathParser[] a = null;
        public String b;
        public int c = 0;
        public int d;

        public f() {
            super(null);
        }

        public boolean b() {
            return false;
        }

        public PathParser[] getPathData() {
            return this.a;
        }

        public String getPathName() {
            return this.b;
        }

        public void setPathData(PathParser[] pathParserArr) {
            if (!ResourcesFlusher.a(this.a, pathParserArr)) {
                this.a = ResourcesFlusher.a(pathParserArr);
                return;
            }
            PathParser[] pathParserArr2 = this.a;
            for (int i2 = 0; i2 < pathParserArr.length; i2++) {
                pathParserArr2[i2].a = pathParserArr[i2].a;
                for (int i3 = 0; i3 < pathParserArr[i2].b.length; i3++) {
                    pathParserArr2[i2].b[i3] = pathParserArr[i2].b[i3];
                }
            }
        }

        public f(f fVar) {
            super(null);
            this.b = fVar.b;
            this.d = fVar.d;
            this.a = ResourcesFlusher.a(fVar.a);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.x.a.a.VectorDrawableCompat.createFromXmlInner(android.content.res.Resources, org.xmlpull.v1.XmlPullParser, android.util.AttributeSet, android.content.res.Resources$Theme):i.x.a.a.VectorDrawableCompat
     arg types: [android.content.res.Resources, android.content.res.XmlResourceParser, android.util.AttributeSet, android.content.res.Resources$Theme]
     candidates:
      ClspMth{android.graphics.drawable.Drawable.createFromXmlInner(android.content.res.Resources, org.xmlpull.v1.XmlPullParser, android.util.AttributeSet, android.content.res.Resources$Theme):android.graphics.drawable.Drawable throws java.io.IOException, org.xmlpull.v1.XmlPullParserException}
      i.x.a.a.VectorDrawableCompat.createFromXmlInner(android.content.res.Resources, org.xmlpull.v1.XmlPullParser, android.util.AttributeSet, android.content.res.Resources$Theme):i.x.a.a.VectorDrawableCompat */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036 A[Catch:{ XmlPullParserException -> 0x0048, IOException -> 0x0043 }] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x003b A[Catch:{ XmlPullParserException -> 0x0048, IOException -> 0x0043 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static i.x.a.a.VectorDrawableCompat a(android.content.res.Resources r6, int r7, android.content.res.Resources.Theme r8) {
        /*
            java.lang.String r0 = "parser error"
            java.lang.String r1 = "VectorDrawableCompat"
            int r2 = android.os.Build.VERSION.SDK_INT
            r3 = 24
            if (r2 < r3) goto L_0x0021
            i.x.a.a.VectorDrawableCompat r0 = new i.x.a.a.VectorDrawableCompat
            r0.<init>()
            android.graphics.drawable.Drawable r6 = r6.getDrawable(r7, r8)
            r0.b = r6
            i.x.a.a.VectorDrawableCompat$i r6 = new i.x.a.a.VectorDrawableCompat$i
            android.graphics.drawable.Drawable r7 = r0.b
            android.graphics.drawable.Drawable$ConstantState r7 = r7.getConstantState()
            r6.<init>(r7)
            return r0
        L_0x0021:
            android.content.res.XmlResourceParser r7 = r6.getXml(r7)     // Catch:{ XmlPullParserException -> 0x0048, IOException -> 0x0043 }
            android.util.AttributeSet r2 = android.util.Xml.asAttributeSet(r7)     // Catch:{ XmlPullParserException -> 0x0048, IOException -> 0x0043 }
        L_0x0029:
            int r3 = r7.next()     // Catch:{ XmlPullParserException -> 0x0048, IOException -> 0x0043 }
            r4 = 2
            if (r3 == r4) goto L_0x0034
            r5 = 1
            if (r3 == r5) goto L_0x0034
            goto L_0x0029
        L_0x0034:
            if (r3 != r4) goto L_0x003b
            i.x.a.a.VectorDrawableCompat r6 = createFromXmlInner(r6, r7, r2, r8)     // Catch:{ XmlPullParserException -> 0x0048, IOException -> 0x0043 }
            return r6
        L_0x003b:
            org.xmlpull.v1.XmlPullParserException r6 = new org.xmlpull.v1.XmlPullParserException     // Catch:{ XmlPullParserException -> 0x0048, IOException -> 0x0043 }
            java.lang.String r7 = "No start tag found"
            r6.<init>(r7)     // Catch:{ XmlPullParserException -> 0x0048, IOException -> 0x0043 }
            throw r6     // Catch:{ XmlPullParserException -> 0x0048, IOException -> 0x0043 }
        L_0x0043:
            r6 = move-exception
            android.util.Log.e(r1, r0, r6)
            goto L_0x004c
        L_0x0048:
            r6 = move-exception
            android.util.Log.e(r1, r0, r6)
        L_0x004c:
            r6 = 0
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: i.x.a.a.VectorDrawableCompat.a(android.content.res.Resources, int, android.content.res.Resources$Theme):i.x.a.a.VectorDrawableCompat");
    }

    /* JADX WARNING: Removed duplicated region for block: B:132:0x043e  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00bc  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00e1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void inflate(android.content.res.Resources r28, org.xmlpull.v1.XmlPullParser r29, android.util.AttributeSet r30, android.content.res.Resources.Theme r31) {
        /*
            r27 = this;
            r1 = r27
            r2 = r28
            r9 = r29
            r10 = r30
            r11 = r31
            android.graphics.drawable.Drawable r0 = r1.b
            if (r0 == 0) goto L_0x0012
            r0.inflate(r2, r9, r10, r11)
            return
        L_0x0012:
            i.x.a.a.VectorDrawableCompat$h r12 = r1.c
            i.x.a.a.VectorDrawableCompat$g r0 = new i.x.a.a.VectorDrawableCompat$g
            r0.<init>()
            r12.b = r0
            int[] r0 = i.x.a.a.AndroidResources.a
            android.content.res.TypedArray r3 = i.b.k.ResourcesFlusher.a(r2, r11, r10, r0)
            i.x.a.a.VectorDrawableCompat$h r4 = r1.c
            i.x.a.a.VectorDrawableCompat$g r5 = r4.b
            r13 = 6
            r14 = -1
            java.lang.String r0 = "tintMode"
            int r0 = i.b.k.ResourcesFlusher.b(r3, r9, r0, r13, r14)
            android.graphics.PorterDuff$Mode r6 = android.graphics.PorterDuff.Mode.SRC_IN
            r15 = 9
            r8 = 5
            r7 = 3
            if (r0 == r7) goto L_0x0049
            if (r0 == r8) goto L_0x004b
            if (r0 == r15) goto L_0x0046
            switch(r0) {
                case 14: goto L_0x0043;
                case 15: goto L_0x0040;
                case 16: goto L_0x003d;
                default: goto L_0x003c;
            }
        L_0x003c:
            goto L_0x004b
        L_0x003d:
            android.graphics.PorterDuff$Mode r6 = android.graphics.PorterDuff.Mode.ADD
            goto L_0x004b
        L_0x0040:
            android.graphics.PorterDuff$Mode r6 = android.graphics.PorterDuff.Mode.SCREEN
            goto L_0x004b
        L_0x0043:
            android.graphics.PorterDuff$Mode r6 = android.graphics.PorterDuff.Mode.MULTIPLY
            goto L_0x004b
        L_0x0046:
            android.graphics.PorterDuff$Mode r6 = android.graphics.PorterDuff.Mode.SRC_ATOP
            goto L_0x004b
        L_0x0049:
            android.graphics.PorterDuff$Mode r6 = android.graphics.PorterDuff.Mode.SRC_OVER
        L_0x004b:
            r4.d = r6
            java.lang.String r0 = "tint"
            boolean r0 = i.b.k.ResourcesFlusher.a(r9, r0)
            r13 = 0
            r15 = 1
            r14 = 2
            if (r0 == 0) goto L_0x00ac
            android.util.TypedValue r0 = new android.util.TypedValue
            r0.<init>()
            r3.getValue(r15, r0)
            int r6 = r0.type
            if (r6 == r14) goto L_0x008d
            r14 = 28
            if (r6 < r14) goto L_0x0073
            r14 = 31
            if (r6 > r14) goto L_0x0073
            int r0 = r0.data
            android.content.res.ColorStateList r0 = android.content.res.ColorStateList.valueOf(r0)
            goto L_0x00ad
        L_0x0073:
            android.content.res.Resources r0 = r3.getResources()
            int r6 = r3.getResourceId(r15, r13)
            android.content.res.XmlResourceParser r6 = r0.getXml(r6)     // Catch:{ Exception -> 0x0084 }
            android.content.res.ColorStateList r0 = i.b.k.ResourcesFlusher.a(r0, r6, r11)     // Catch:{ Exception -> 0x0084 }
            goto L_0x00ad
        L_0x0084:
            r0 = move-exception
            java.lang.String r6 = "CSLCompat"
            java.lang.String r14 = "Failed to inflate ColorStateList."
            android.util.Log.e(r6, r14, r0)
            goto L_0x00ac
        L_0x008d:
            java.lang.UnsupportedOperationException r2 = new java.lang.UnsupportedOperationException
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Failed to resolve attribute at index "
            r3.append(r4)
            r3.append(r15)
            java.lang.String r4 = ": "
            r3.append(r4)
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r2.<init>(r0)
            throw r2
        L_0x00ac:
            r0 = 0
        L_0x00ad:
            if (r0 == 0) goto L_0x00b1
            r4.c = r0
        L_0x00b1:
            boolean r0 = r4.f1516e
            java.lang.String r6 = "autoMirrored"
            boolean r6 = i.b.k.ResourcesFlusher.a(r9, r6)
            if (r6 != 0) goto L_0x00bc
            goto L_0x00c0
        L_0x00bc:
            boolean r0 = r3.getBoolean(r8, r0)
        L_0x00c0:
            r4.f1516e = r0
            float r0 = r5.f1510k
            r14 = 7
            java.lang.String r4 = "viewportWidth"
            float r0 = i.b.k.ResourcesFlusher.a(r3, r9, r4, r14, r0)
            r5.f1510k = r0
            float r0 = r5.f1511l
            r6 = 8
            java.lang.String r4 = "viewportHeight"
            float r0 = i.b.k.ResourcesFlusher.a(r3, r9, r4, r6, r0)
            r5.f1511l = r0
            float r4 = r5.f1510k
            r19 = 0
            int r4 = (r4 > r19 ? 1 : (r4 == r19 ? 0 : -1))
            if (r4 <= 0) goto L_0x043e
            int r0 = (r0 > r19 ? 1 : (r0 == r19 ? 0 : -1))
            if (r0 <= 0) goto L_0x0422
            float r0 = r5.f1508i
            float r0 = r3.getDimension(r7, r0)
            r5.f1508i = r0
            float r0 = r5.f1509j
            r4 = 2
            float r0 = r3.getDimension(r4, r0)
            r5.f1509j = r0
            float r4 = r5.f1508i
            int r4 = (r4 > r19 ? 1 : (r4 == r19 ? 0 : -1))
            if (r4 <= 0) goto L_0x0406
            int r0 = (r0 > r19 ? 1 : (r0 == r19 ? 0 : -1))
            if (r0 <= 0) goto L_0x03ea
            float r0 = r5.getAlpha()
            r4 = 4
            java.lang.String r6 = "alpha"
            float r0 = i.b.k.ResourcesFlusher.a(r3, r9, r6, r4, r0)
            r5.setAlpha(r0)
            java.lang.String r0 = r3.getString(r13)
            if (r0 == 0) goto L_0x011b
            r5.f1513n = r0
            i.e.ArrayMap<java.lang.String, java.lang.Object> r6 = r5.f1515p
            r6.put(r0, r5)
        L_0x011b:
            r3.recycle()
            int r0 = r27.getChangingConfigurations()
            r12.a = r0
            r12.f1520k = r15
            i.x.a.a.VectorDrawableCompat$h r0 = r1.c
            i.x.a.a.VectorDrawableCompat$g r6 = r0.b
            java.util.ArrayDeque r5 = new java.util.ArrayDeque
            r5.<init>()
            i.x.a.a.VectorDrawableCompat$d r3 = r6.h
            r5.push(r3)
            int r3 = r29.getEventType()
            int r20 = r29.getDepth()
            int r14 = r20 + 1
            r20 = 1
        L_0x0140:
            if (r3 == r15) goto L_0x03d1
            int r4 = r29.getDepth()
            if (r4 >= r14) goto L_0x014a
            if (r3 == r7) goto L_0x03d1
        L_0x014a:
            java.lang.String r4 = "group"
            r7 = 2
            if (r3 != r7) goto L_0x03a1
            java.lang.String r3 = r29.getName()
            java.lang.Object r7 = r5.peek()
            i.x.a.a.VectorDrawableCompat$d r7 = (i.x.a.a.VectorDrawableCompat.d) r7
            java.lang.String r8 = "path"
            boolean r8 = r8.equals(r3)
            java.lang.String r15 = "fillType"
            java.lang.String r13 = "pathData"
            if (r8 == 0) goto L_0x02a8
            i.x.a.a.VectorDrawableCompat$c r8 = new i.x.a.a.VectorDrawableCompat$c
            r8.<init>()
            int[] r3 = i.x.a.a.AndroidResources.c
            android.content.res.TypedArray r4 = i.b.k.ResourcesFlusher.a(r2, r11, r10, r3)
            r3 = 0
            r8.f1488e = r3
            boolean r13 = i.b.k.ResourcesFlusher.a(r9, r13)
            if (r13 != 0) goto L_0x018b
            r1 = r4
            r25 = r5
            r26 = r6
            r13 = r8
            r18 = r14
            r5 = 4
            r17 = -1
            r21 = 9
            r22 = 8
            r14 = r7
            goto L_0x027a
        L_0x018b:
            r13 = 0
            java.lang.String r3 = r4.getString(r13)
            if (r3 == 0) goto L_0x0194
            r8.b = r3
        L_0x0194:
            r3 = 2
            java.lang.String r13 = r4.getString(r3)
            if (r13 == 0) goto L_0x01a1
            i.h.f.PathParser[] r3 = i.b.k.ResourcesFlusher.d(r13)
            r8.a = r3
        L_0x01a1:
            r13 = 1
            r20 = 0
            java.lang.String r23 = "fillColor"
            r18 = 0
            r3 = r4
            r24 = r4
            r4 = r29
            r25 = r5
            r5 = r31
            r26 = r6
            r6 = r23
            r18 = r14
            r1 = 3
            r14 = r7
            r7 = r13
            r13 = r8
            r1 = 5
            r8 = r20
            i.h.e.b.ComplexColorCompat r3 = i.b.k.ResourcesFlusher.a(r3, r4, r5, r6, r7, r8)
            r13.h = r3
            r3 = 12
            float r4 = r13.f1491j
            java.lang.String r5 = "fillAlpha"
            r8 = r24
            float r3 = i.b.k.ResourcesFlusher.a(r8, r9, r5, r3, r4)
            r13.f1491j = r3
            java.lang.String r3 = "strokeLineCap"
            r4 = -1
            r7 = 8
            int r3 = i.b.k.ResourcesFlusher.b(r8, r9, r3, r7, r4)
            android.graphics.Paint$Cap r4 = r13.f1495n
            if (r3 == 0) goto L_0x01ec
            r5 = 1
            if (r3 == r5) goto L_0x01e9
            r5 = 2
            if (r3 == r5) goto L_0x01e6
            goto L_0x01ee
        L_0x01e6:
            android.graphics.Paint$Cap r4 = android.graphics.Paint.Cap.SQUARE
            goto L_0x01ee
        L_0x01e9:
            android.graphics.Paint$Cap r4 = android.graphics.Paint.Cap.ROUND
            goto L_0x01ee
        L_0x01ec:
            android.graphics.Paint$Cap r4 = android.graphics.Paint.Cap.BUTT
        L_0x01ee:
            r13.f1495n = r4
            java.lang.String r3 = "strokeLineJoin"
            r5 = -1
            r6 = 9
            int r3 = i.b.k.ResourcesFlusher.b(r8, r9, r3, r6, r5)
            android.graphics.Paint$Join r4 = r13.f1496o
            if (r3 == 0) goto L_0x020a
            r5 = 1
            if (r3 == r5) goto L_0x0207
            r5 = 2
            if (r3 == r5) goto L_0x0204
            goto L_0x020c
        L_0x0204:
            android.graphics.Paint$Join r4 = android.graphics.Paint.Join.BEVEL
            goto L_0x020c
        L_0x0207:
            android.graphics.Paint$Join r4 = android.graphics.Paint.Join.ROUND
            goto L_0x020c
        L_0x020a:
            android.graphics.Paint$Join r4 = android.graphics.Paint.Join.MITER
        L_0x020c:
            r13.f1496o = r4
            r3 = 10
            float r4 = r13.f1497p
            java.lang.String r5 = "strokeMiterLimit"
            float r3 = i.b.k.ResourcesFlusher.a(r8, r9, r5, r3, r4)
            r13.f1497p = r3
            r16 = 3
            r19 = 0
            java.lang.String r20 = "strokeColor"
            r3 = r8
            r4 = r29
            r17 = -1
            r5 = r31
            r21 = 9
            r6 = r20
            r22 = 8
            r7 = r16
            r1 = r8
            r8 = r19
            i.h.e.b.ComplexColorCompat r3 = i.b.k.ResourcesFlusher.a(r3, r4, r5, r6, r7, r8)
            r13.f1489f = r3
            r3 = 11
            float r4 = r13.f1490i
            java.lang.String r5 = "strokeAlpha"
            float r3 = i.b.k.ResourcesFlusher.a(r1, r9, r5, r3, r4)
            r13.f1490i = r3
            float r3 = r13.g
            java.lang.String r4 = "strokeWidth"
            r5 = 4
            float r3 = i.b.k.ResourcesFlusher.a(r1, r9, r4, r5, r3)
            r13.g = r3
            float r3 = r13.f1493l
            java.lang.String r4 = "trimPathEnd"
            r6 = 6
            float r3 = i.b.k.ResourcesFlusher.a(r1, r9, r4, r6, r3)
            r13.f1493l = r3
            float r3 = r13.f1494m
            java.lang.String r4 = "trimPathOffset"
            r6 = 7
            float r3 = i.b.k.ResourcesFlusher.a(r1, r9, r4, r6, r3)
            r13.f1494m = r3
            float r3 = r13.f1492k
            java.lang.String r4 = "trimPathStart"
            r6 = 5
            float r3 = i.b.k.ResourcesFlusher.a(r1, r9, r4, r6, r3)
            r13.f1492k = r3
            r3 = 13
            int r4 = r13.c
            int r3 = i.b.k.ResourcesFlusher.b(r1, r9, r15, r3, r4)
            r13.c = r3
        L_0x027a:
            r1.recycle()
            java.util.ArrayList<i.x.a.a.g$e> r1 = r14.b
            r1.add(r13)
            java.lang.String r1 = r13.getPathName()
            if (r1 == 0) goto L_0x0294
            r1 = r26
            i.e.ArrayMap<java.lang.String, java.lang.Object> r3 = r1.f1515p
            java.lang.String r4 = r13.getPathName()
            r3.put(r4, r13)
            goto L_0x0296
        L_0x0294:
            r1 = r26
        L_0x0296:
            int r3 = r0.a
            int r4 = r13.d
            r3 = r3 | r4
            r0.a = r3
            r7 = r25
            r5 = 7
            r6 = 0
            r8 = 6
            r13 = 5
            r15 = 2
            r20 = 0
            goto L_0x039f
        L_0x02a8:
            r25 = r5
            r1 = r6
            r18 = r14
            r5 = 4
            r17 = -1
            r21 = 9
            r22 = 8
            r14 = r7
            java.lang.String r6 = "clip-path"
            boolean r6 = r6.equals(r3)
            if (r6 == 0) goto L_0x030c
            i.x.a.a.VectorDrawableCompat$b r3 = new i.x.a.a.VectorDrawableCompat$b
            r3.<init>()
            boolean r4 = i.b.k.ResourcesFlusher.a(r9, r13)
            if (r4 != 0) goto L_0x02c9
            goto L_0x02ef
        L_0x02c9:
            int[] r4 = i.x.a.a.AndroidResources.d
            android.content.res.TypedArray r4 = i.b.k.ResourcesFlusher.a(r2, r11, r10, r4)
            r6 = 0
            java.lang.String r7 = r4.getString(r6)
            if (r7 == 0) goto L_0x02d8
            r3.b = r7
        L_0x02d8:
            r7 = 1
            java.lang.String r8 = r4.getString(r7)
            if (r8 == 0) goto L_0x02e5
            i.h.f.PathParser[] r7 = i.b.k.ResourcesFlusher.d(r8)
            r3.a = r7
        L_0x02e5:
            r7 = 2
            int r8 = i.b.k.ResourcesFlusher.b(r4, r9, r15, r7, r6)
            r3.c = r8
            r4.recycle()
        L_0x02ef:
            java.util.ArrayList<i.x.a.a.g$e> r4 = r14.b
            r4.add(r3)
            java.lang.String r4 = r3.getPathName()
            if (r4 == 0) goto L_0x0303
            i.e.ArrayMap<java.lang.String, java.lang.Object> r4 = r1.f1515p
            java.lang.String r6 = r3.getPathName()
            r4.put(r6, r3)
        L_0x0303:
            int r4 = r0.a
            int r3 = r3.d
            r3 = r3 | r4
            r0.a = r3
            goto L_0x0398
        L_0x030c:
            boolean r3 = r4.equals(r3)
            if (r3 == 0) goto L_0x0398
            i.x.a.a.VectorDrawableCompat$d r3 = new i.x.a.a.VectorDrawableCompat$d
            r3.<init>()
            int[] r4 = i.x.a.a.AndroidResources.b
            android.content.res.TypedArray r4 = i.b.k.ResourcesFlusher.a(r2, r11, r10, r4)
            r6 = 0
            r3.f1503l = r6
            float r7 = r3.c
            java.lang.String r8 = "rotation"
            r13 = 5
            float r7 = i.b.k.ResourcesFlusher.a(r4, r9, r8, r13, r7)
            r3.c = r7
            float r7 = r3.d
            r8 = 1
            float r7 = r4.getFloat(r8, r7)
            r3.d = r7
            float r7 = r3.f1498e
            r15 = 2
            float r7 = r4.getFloat(r15, r7)
            r3.f1498e = r7
            float r7 = r3.f1499f
            java.lang.String r6 = "scaleX"
            r8 = 3
            float r6 = i.b.k.ResourcesFlusher.a(r4, r9, r6, r8, r7)
            r3.f1499f = r6
            float r6 = r3.g
            java.lang.String r7 = "scaleY"
            float r6 = i.b.k.ResourcesFlusher.a(r4, r9, r7, r5, r6)
            r3.g = r6
            float r6 = r3.h
            java.lang.String r7 = "translateX"
            r8 = 6
            float r6 = i.b.k.ResourcesFlusher.a(r4, r9, r7, r8, r6)
            r3.h = r6
            float r6 = r3.f1500i
            java.lang.String r7 = "translateY"
            r5 = 7
            float r6 = i.b.k.ResourcesFlusher.a(r4, r9, r7, r5, r6)
            r3.f1500i = r6
            r6 = 0
            java.lang.String r7 = r4.getString(r6)
            if (r7 == 0) goto L_0x0371
            r3.f1504m = r7
        L_0x0371:
            r3.b()
            r4.recycle()
            java.util.ArrayList<i.x.a.a.g$e> r4 = r14.b
            r4.add(r3)
            r7 = r25
            r7.push(r3)
            java.lang.String r4 = r3.getGroupName()
            if (r4 == 0) goto L_0x0390
            i.e.ArrayMap<java.lang.String, java.lang.Object> r4 = r1.f1515p
            java.lang.String r14 = r3.getGroupName()
            r4.put(r14, r3)
        L_0x0390:
            int r4 = r0.a
            int r3 = r3.f1502k
            r3 = r3 | r4
            r0.a = r3
            goto L_0x039f
        L_0x0398:
            r7 = r25
            r5 = 7
            r6 = 0
            r8 = 6
            r13 = 5
            r15 = 2
        L_0x039f:
            r14 = 3
            goto L_0x03c0
        L_0x03a1:
            r7 = r5
            r1 = r6
            r18 = r14
            r5 = 7
            r6 = 0
            r8 = 6
            r13 = 5
            r14 = 3
            r15 = 2
            r17 = -1
            r21 = 9
            r22 = 8
            if (r3 != r14) goto L_0x03c0
            java.lang.String r3 = r29.getName()
            boolean r3 = r4.equals(r3)
            if (r3 == 0) goto L_0x03c0
            r7.pop()
        L_0x03c0:
            int r3 = r29.next()
            r6 = r1
            r5 = r7
            r14 = r18
            r4 = 4
            r7 = 3
            r8 = 5
            r13 = 0
            r15 = 1
            r1 = r27
            goto L_0x0140
        L_0x03d1:
            if (r20 != 0) goto L_0x03e0
            android.content.res.ColorStateList r0 = r12.c
            android.graphics.PorterDuff$Mode r1 = r12.d
            r2 = r27
            android.graphics.PorterDuffColorFilter r0 = r2.a(r0, r1)
            r2.d = r0
            return
        L_0x03e0:
            r2 = r27
            org.xmlpull.v1.XmlPullParserException r0 = new org.xmlpull.v1.XmlPullParserException
            java.lang.String r1 = "no path defined"
            r0.<init>(r1)
            throw r0
        L_0x03ea:
            r2 = r1
            org.xmlpull.v1.XmlPullParserException r0 = new org.xmlpull.v1.XmlPullParserException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = r3.getPositionDescription()
            r1.append(r3)
            java.lang.String r3 = "<vector> tag requires height > 0"
            r1.append(r3)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0406:
            r2 = r1
            org.xmlpull.v1.XmlPullParserException r0 = new org.xmlpull.v1.XmlPullParserException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = r3.getPositionDescription()
            r1.append(r3)
            java.lang.String r3 = "<vector> tag requires width > 0"
            r1.append(r3)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0422:
            r2 = r1
            org.xmlpull.v1.XmlPullParserException r0 = new org.xmlpull.v1.XmlPullParserException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = r3.getPositionDescription()
            r1.append(r3)
            java.lang.String r3 = "<vector> tag requires viewportHeight > 0"
            r1.append(r3)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x043e:
            r2 = r1
            org.xmlpull.v1.XmlPullParserException r0 = new org.xmlpull.v1.XmlPullParserException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = r3.getPositionDescription()
            r1.append(r3)
            java.lang.String r3 = "<vector> tag requires viewportWidth > 0"
            r1.append(r3)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: i.x.a.a.VectorDrawableCompat.inflate(android.content.res.Resources, org.xmlpull.v1.XmlPullParser, android.util.AttributeSet, android.content.res.Resources$Theme):void");
    }

    public VectorDrawableCompat(h hVar) {
        this.g = true;
        this.h = new float[9];
        this.f1486i = new Matrix();
        this.f1487j = new Rect();
        this.c = hVar;
        this.d = a(hVar.c, hVar.d);
    }

    public static class g {

        /* renamed from: q  reason: collision with root package name */
        public static final Matrix f1505q = new Matrix();
        public final Path a;
        public final Path b;
        public final Matrix c;
        public Paint d;

        /* renamed from: e  reason: collision with root package name */
        public Paint f1506e;

        /* renamed from: f  reason: collision with root package name */
        public PathMeasure f1507f;
        public int g;
        public final d h;

        /* renamed from: i  reason: collision with root package name */
        public float f1508i;

        /* renamed from: j  reason: collision with root package name */
        public float f1509j;

        /* renamed from: k  reason: collision with root package name */
        public float f1510k;

        /* renamed from: l  reason: collision with root package name */
        public float f1511l;

        /* renamed from: m  reason: collision with root package name */
        public int f1512m;

        /* renamed from: n  reason: collision with root package name */
        public String f1513n;

        /* renamed from: o  reason: collision with root package name */
        public Boolean f1514o;

        /* renamed from: p  reason: collision with root package name */
        public final ArrayMap<String, Object> f1515p;

        public g() {
            this.c = new Matrix();
            this.f1508i = 0.0f;
            this.f1509j = 0.0f;
            this.f1510k = 0.0f;
            this.f1511l = 0.0f;
            this.f1512m = 255;
            this.f1513n = null;
            this.f1514o = null;
            this.f1515p = new ArrayMap<>();
            this.h = new d();
            this.a = new Path();
            this.b = new Path();
        }

        /* JADX WARN: Type inference failed for: r11v0 */
        /* JADX WARN: Type inference failed for: r11v1, types: [boolean] */
        /* JADX WARN: Type inference failed for: r11v2 */
        public final void a(d dVar, Matrix matrix, Canvas canvas, int i2, int i3, ColorFilter colorFilter) {
            g gVar;
            g gVar2 = this;
            d dVar2 = dVar;
            Canvas canvas2 = canvas;
            ColorFilter colorFilter2 = colorFilter;
            dVar2.a.set(matrix);
            dVar2.a.preConcat(dVar2.f1501j);
            canvas.save();
            ? r11 = 0;
            int i4 = 0;
            while (i4 < dVar2.b.size()) {
                e eVar = dVar2.b.get(i4);
                if (eVar instanceof d) {
                    a((d) eVar, dVar2.a, canvas, i2, i3, colorFilter);
                } else if (eVar instanceof f) {
                    f fVar = (f) eVar;
                    float f2 = ((float) i2) / gVar2.f1510k;
                    float f3 = ((float) i3) / gVar2.f1511l;
                    float min = Math.min(f2, f3);
                    Matrix matrix2 = dVar2.a;
                    gVar2.c.set(matrix2);
                    gVar2.c.postScale(f2, f3);
                    float[] fArr = {0.0f, 1.0f, 1.0f, 0.0f};
                    matrix2.mapVectors(fArr);
                    float f4 = min;
                    float f5 = (fArr[r11] * fArr[3]) - (fArr[1] * fArr[2]);
                    float max = Math.max((float) Math.hypot((double) fArr[r11], (double) fArr[1]), (float) Math.hypot((double) fArr[2], (double) fArr[3]));
                    float abs = max > 0.0f ? Math.abs(f5) / max : 0.0f;
                    if (abs == 0.0f) {
                        gVar = this;
                    } else {
                        gVar = this;
                        Path path = gVar.a;
                        if (fVar != null) {
                            path.reset();
                            PathParser[] pathParserArr = fVar.a;
                            if (pathParserArr != null) {
                                PathParser.a(pathParserArr, path);
                            }
                            Path path2 = gVar.a;
                            gVar.b.reset();
                            if (fVar.b()) {
                                gVar.b.setFillType(fVar.c == 0 ? Path.FillType.WINDING : Path.FillType.EVEN_ODD);
                                gVar.b.addPath(path2, gVar.c);
                                canvas2.clipPath(gVar.b);
                            } else {
                                c cVar = (c) fVar;
                                if (!(cVar.f1492k == 0.0f && cVar.f1493l == 1.0f)) {
                                    float f6 = cVar.f1492k;
                                    float f7 = cVar.f1494m;
                                    float f8 = (f6 + f7) % 1.0f;
                                    float f9 = (cVar.f1493l + f7) % 1.0f;
                                    if (gVar.f1507f == null) {
                                        gVar.f1507f = new PathMeasure();
                                    }
                                    gVar.f1507f.setPath(gVar.a, r11);
                                    float length = gVar.f1507f.getLength();
                                    float f10 = f8 * length;
                                    float f11 = f9 * length;
                                    path2.reset();
                                    if (f10 > f11) {
                                        gVar.f1507f.getSegment(f10, length, path2, true);
                                        gVar.f1507f.getSegment(0.0f, f11, path2, true);
                                    } else {
                                        gVar.f1507f.getSegment(f10, f11, path2, true);
                                    }
                                    path2.rLineTo(0.0f, 0.0f);
                                }
                                gVar.b.addPath(path2, gVar.c);
                                ComplexColorCompat complexColorCompat = cVar.h;
                                if (complexColorCompat.a() || complexColorCompat.c != 0) {
                                    ComplexColorCompat complexColorCompat2 = cVar.h;
                                    if (gVar.f1506e == null) {
                                        Paint paint = new Paint(1);
                                        gVar.f1506e = paint;
                                        paint.setStyle(Paint.Style.FILL);
                                    }
                                    Paint paint2 = gVar.f1506e;
                                    if (complexColorCompat2.a()) {
                                        Shader shader = complexColorCompat2.a;
                                        shader.setLocalMatrix(gVar.c);
                                        paint2.setShader(shader);
                                        paint2.setAlpha(Math.round(cVar.f1491j * 255.0f));
                                    } else {
                                        paint2.setShader(null);
                                        paint2.setAlpha(255);
                                        paint2.setColor(VectorDrawableCompat.a(complexColorCompat2.c, cVar.f1491j));
                                    }
                                    paint2.setColorFilter(colorFilter2);
                                    gVar.b.setFillType(cVar.c == 0 ? Path.FillType.WINDING : Path.FillType.EVEN_ODD);
                                    canvas2.drawPath(gVar.b, paint2);
                                }
                                ComplexColorCompat complexColorCompat3 = cVar.f1489f;
                                if (complexColorCompat3.a() || complexColorCompat3.c != 0) {
                                    ComplexColorCompat complexColorCompat4 = cVar.f1489f;
                                    if (gVar.d == null) {
                                        Paint paint3 = new Paint(1);
                                        gVar.d = paint3;
                                        paint3.setStyle(Paint.Style.STROKE);
                                    }
                                    Paint paint4 = gVar.d;
                                    Paint.Join join = cVar.f1496o;
                                    if (join != null) {
                                        paint4.setStrokeJoin(join);
                                    }
                                    Paint.Cap cap = cVar.f1495n;
                                    if (cap != null) {
                                        paint4.setStrokeCap(cap);
                                    }
                                    paint4.setStrokeMiter(cVar.f1497p);
                                    if (complexColorCompat4.a()) {
                                        Shader shader2 = complexColorCompat4.a;
                                        shader2.setLocalMatrix(gVar.c);
                                        paint4.setShader(shader2);
                                        paint4.setAlpha(Math.round(cVar.f1490i * 255.0f));
                                    } else {
                                        paint4.setShader(null);
                                        paint4.setAlpha(255);
                                        paint4.setColor(VectorDrawableCompat.a(complexColorCompat4.c, cVar.f1490i));
                                    }
                                    paint4.setColorFilter(colorFilter2);
                                    paint4.setStrokeWidth(cVar.g * abs * f4);
                                    canvas2.drawPath(gVar.b, paint4);
                                }
                            }
                        } else {
                            throw null;
                        }
                    }
                    i4++;
                    gVar2 = gVar;
                    r11 = 0;
                }
                gVar = gVar2;
                i4++;
                gVar2 = gVar;
                r11 = 0;
            }
            canvas.restore();
        }

        public float getAlpha() {
            return ((float) getRootAlpha()) / 255.0f;
        }

        public int getRootAlpha() {
            return this.f1512m;
        }

        public void setAlpha(float f2) {
            setRootAlpha((int) (f2 * 255.0f));
        }

        public void setRootAlpha(int i2) {
            this.f1512m = i2;
        }

        public g(g gVar) {
            this.c = new Matrix();
            this.f1508i = 0.0f;
            this.f1509j = 0.0f;
            this.f1510k = 0.0f;
            this.f1511l = 0.0f;
            this.f1512m = 255;
            this.f1513n = null;
            this.f1514o = null;
            ArrayMap<String, Object> arrayMap = new ArrayMap<>();
            this.f1515p = arrayMap;
            this.h = new d(gVar.h, arrayMap);
            this.a = new Path(gVar.a);
            this.b = new Path(gVar.b);
            this.f1508i = gVar.f1508i;
            this.f1509j = gVar.f1509j;
            this.f1510k = gVar.f1510k;
            this.f1511l = gVar.f1511l;
            this.g = gVar.g;
            this.f1512m = gVar.f1512m;
            this.f1513n = gVar.f1513n;
            String str = gVar.f1513n;
            if (str != null) {
                this.f1515p.put(str, this);
            }
            this.f1514o = gVar.f1514o;
        }
    }

    public static int a(int i2, float f2) {
        return (i2 & 16777215) | (((int) (((float) Color.alpha(i2)) * f2)) << 24);
    }
}
