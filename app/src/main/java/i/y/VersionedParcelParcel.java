package i.y;

import android.os.Parcel;
import android.util.SparseIntArray;
import androidx.versionedparcelable.VersionedParcel;
import i.e.ArrayMap;
import j.a.a.a.outline;
import java.lang.reflect.Method;

public class VersionedParcelParcel extends VersionedParcel {
    public final SparseIntArray d;

    /* renamed from: e  reason: collision with root package name */
    public final Parcel f1522e;

    /* renamed from: f  reason: collision with root package name */
    public final int f1523f;
    public final int g;
    public final String h;

    /* renamed from: i  reason: collision with root package name */
    public int f1524i;

    /* renamed from: j  reason: collision with root package name */
    public int f1525j;

    /* renamed from: k  reason: collision with root package name */
    public int f1526k;

    public VersionedParcelParcel(Parcel parcel) {
        this(parcel, parcel.dataPosition(), parcel.dataSize(), "", new ArrayMap(), new ArrayMap(), new ArrayMap());
    }

    public boolean a(int i2) {
        while (this.f1525j < this.g) {
            int i3 = this.f1526k;
            if (i3 == i2) {
                return true;
            }
            if (String.valueOf(i3).compareTo(String.valueOf(i2)) > 0) {
                return false;
            }
            this.f1522e.setDataPosition(this.f1525j);
            int readInt = this.f1522e.readInt();
            this.f1526k = this.f1522e.readInt();
            this.f1525j += readInt;
        }
        if (this.f1526k == i2) {
            return true;
        }
        return false;
    }

    public void b(int i2) {
        a();
        this.f1524i = i2;
        this.d.put(i2, this.f1522e.dataPosition());
        this.f1522e.writeInt(0);
        this.f1522e.writeInt(i2);
    }

    public VersionedParcelParcel(Parcel parcel, int i2, int i3, String str, ArrayMap<String, Method> arrayMap, ArrayMap<String, Method> arrayMap2, ArrayMap<String, Class> arrayMap3) {
        super(arrayMap, arrayMap2, arrayMap3);
        this.d = new SparseIntArray();
        this.f1524i = -1;
        this.f1525j = 0;
        this.f1526k = -1;
        this.f1522e = parcel;
        this.f1523f = i2;
        this.g = i3;
        this.f1525j = i2;
        this.h = str;
    }

    public VersionedParcel b() {
        Parcel parcel = this.f1522e;
        int dataPosition = parcel.dataPosition();
        int i2 = this.f1525j;
        if (i2 == this.f1523f) {
            i2 = this.g;
        }
        return new VersionedParcelParcel(parcel, dataPosition, i2, outline.a(new StringBuilder(), this.h, "  "), super.a, super.b, super.c);
    }

    public void a() {
        int i2 = this.f1524i;
        if (i2 >= 0) {
            int i3 = this.d.get(i2);
            int dataPosition = this.f1522e.dataPosition();
            this.f1522e.setDataPosition(i3);
            this.f1522e.writeInt(dataPosition - i3);
            this.f1522e.setDataPosition(dataPosition);
        }
    }
}
