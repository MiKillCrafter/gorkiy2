package i.e;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

public abstract class MapCollections<K, V> {
    public MapCollections<K, V>.defpackage.b a;
    public MapCollections<K, V>.defpackage.c b;
    public MapCollections<K, V>.e c;

    public final class a<T> implements Iterator<T> {
        public final int b;
        public int c;
        public int d;

        /* renamed from: e  reason: collision with root package name */
        public boolean f1067e = false;

        public a(int i2) {
            this.b = i2;
            this.c = MapCollections.this.c();
        }

        public boolean hasNext() {
            return this.d < this.c;
        }

        public T next() {
            if (hasNext()) {
                T a = MapCollections.this.a(this.d, this.b);
                this.d++;
                this.f1067e = true;
                return a;
            }
            throw new NoSuchElementException();
        }

        public void remove() {
            if (this.f1067e) {
                int i2 = this.d - 1;
                this.d = i2;
                this.c--;
                this.f1067e = false;
                MapCollections.this.a(i2);
                return;
            }
            throw new IllegalStateException();
        }
    }

    public final class b implements Set<Map.Entry<K, V>> {
        public b() {
        }

        public boolean add(Object obj) {
            Map.Entry entry = (Map.Entry) obj;
            throw new UnsupportedOperationException();
        }

        public boolean addAll(Collection<? extends Map.Entry<K, V>> collection) {
            int c = MapCollections.this.c();
            for (Map.Entry entry : collection) {
                MapCollections.this.a(entry.getKey(), entry.getValue());
            }
            return c != MapCollections.this.c();
        }

        public void clear() {
            MapCollections.this.a();
        }

        public boolean contains(Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            int a = MapCollections.this.a(entry.getKey());
            if (a < 0) {
                return false;
            }
            return ContainerHelpers.a(MapCollections.this.a(a, 1), entry.getValue());
        }

        public boolean containsAll(Collection<?> collection) {
            for (Object contains : collection) {
                if (!contains(contains)) {
                    return false;
                }
            }
            return true;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: i.e.MapCollections.a(java.util.Set, java.lang.Object):boolean
         arg types: [i.e.MapCollections$b, java.lang.Object]
         candidates:
          i.e.MapCollections.a(java.util.Map, java.util.Collection<?>):boolean
          i.e.MapCollections.a(int, int):java.lang.Object
          i.e.MapCollections.a(int, java.lang.Object):V
          i.e.MapCollections.a(java.lang.Object, java.lang.Object):void
          i.e.MapCollections.a(java.lang.Object[], int):T[]
          i.e.MapCollections.a(java.util.Set, java.lang.Object):boolean */
        public boolean equals(Object obj) {
            return MapCollections.a((Set) this, obj);
        }

        public int hashCode() {
            int i2;
            int i3;
            int i4 = 0;
            for (int c = MapCollections.this.c() - 1; c >= 0; c--) {
                Object a = MapCollections.this.a(c, 0);
                Object a2 = MapCollections.this.a(c, 1);
                if (a == null) {
                    i2 = 0;
                } else {
                    i2 = a.hashCode();
                }
                if (a2 == null) {
                    i3 = 0;
                } else {
                    i3 = a2.hashCode();
                }
                i4 += i2 ^ i3;
            }
            return i4;
        }

        public boolean isEmpty() {
            return MapCollections.this.c() == 0;
        }

        public Iterator<Map.Entry<K, V>> iterator() {
            return new d();
        }

        public boolean remove(Object obj) {
            throw new UnsupportedOperationException();
        }

        public boolean removeAll(Collection<?> collection) {
            throw new UnsupportedOperationException();
        }

        public boolean retainAll(Collection<?> collection) {
            throw new UnsupportedOperationException();
        }

        public int size() {
            return MapCollections.this.c();
        }

        public Object[] toArray() {
            throw new UnsupportedOperationException();
        }

        public <T> T[] toArray(T[] tArr) {
            throw new UnsupportedOperationException();
        }
    }

    public final class c implements Set<K> {
        public c() {
        }

        public boolean add(K k2) {
            throw new UnsupportedOperationException();
        }

        public boolean addAll(Collection<? extends K> collection) {
            throw new UnsupportedOperationException();
        }

        public void clear() {
            MapCollections.this.a();
        }

        public boolean contains(Object obj) {
            return MapCollections.this.a(obj) >= 0;
        }

        public boolean containsAll(Collection<?> collection) {
            Map b2 = MapCollections.this.b();
            for (Object containsKey : collection) {
                if (!b2.containsKey(containsKey)) {
                    return false;
                }
            }
            return true;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: i.e.MapCollections.a(java.util.Set, java.lang.Object):boolean
         arg types: [i.e.MapCollections$c, java.lang.Object]
         candidates:
          i.e.MapCollections.a(java.util.Map, java.util.Collection<?>):boolean
          i.e.MapCollections.a(int, int):java.lang.Object
          i.e.MapCollections.a(int, java.lang.Object):V
          i.e.MapCollections.a(java.lang.Object, java.lang.Object):void
          i.e.MapCollections.a(java.lang.Object[], int):T[]
          i.e.MapCollections.a(java.util.Set, java.lang.Object):boolean */
        public boolean equals(Object obj) {
            return MapCollections.a((Set) this, obj);
        }

        public int hashCode() {
            int i2;
            int i3 = 0;
            for (int c = MapCollections.this.c() - 1; c >= 0; c--) {
                Object a = MapCollections.this.a(c, 0);
                if (a == null) {
                    i2 = 0;
                } else {
                    i2 = a.hashCode();
                }
                i3 += i2;
            }
            return i3;
        }

        public boolean isEmpty() {
            return MapCollections.this.c() == 0;
        }

        public Iterator<K> iterator() {
            return new a(0);
        }

        public boolean remove(Object obj) {
            int a = MapCollections.this.a(obj);
            if (a < 0) {
                return false;
            }
            MapCollections.this.a(a);
            return true;
        }

        public boolean removeAll(Collection<?> collection) {
            Map b2 = MapCollections.this.b();
            int size = b2.size();
            for (Object remove : collection) {
                b2.remove(remove);
            }
            return size != b2.size();
        }

        public boolean retainAll(Collection<?> collection) {
            return MapCollections.a(MapCollections.this.b(), collection);
        }

        public int size() {
            return MapCollections.this.c();
        }

        public Object[] toArray() {
            return MapCollections.this.b(0);
        }

        public <T> T[] toArray(T[] tArr) {
            return MapCollections.this.a(tArr, 0);
        }
    }

    public final class d implements Iterator<Map.Entry<K, V>>, Map.Entry<K, V> {
        public int b;
        public int c;
        public boolean d = false;

        public d() {
            this.b = MapCollections.this.c() - 1;
            this.c = -1;
        }

        public boolean equals(Object obj) {
            if (!this.d) {
                throw new IllegalStateException("This container does not support retaining Map.Entry objects");
            } else if (!(obj instanceof Map.Entry)) {
                return false;
            } else {
                Map.Entry entry = (Map.Entry) obj;
                if (!ContainerHelpers.a(entry.getKey(), MapCollections.this.a(this.c, 0)) || !ContainerHelpers.a(entry.getValue(), MapCollections.this.a(this.c, 1))) {
                    return false;
                }
                return true;
            }
        }

        public K getKey() {
            if (this.d) {
                return MapCollections.this.a(this.c, 0);
            }
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }

        public V getValue() {
            if (this.d) {
                return MapCollections.this.a(this.c, 1);
            }
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }

        public boolean hasNext() {
            return this.c < this.b;
        }

        public int hashCode() {
            int i2;
            if (this.d) {
                int i3 = 0;
                Object a = MapCollections.this.a(this.c, 0);
                Object a2 = MapCollections.this.a(this.c, 1);
                if (a == null) {
                    i2 = 0;
                } else {
                    i2 = a.hashCode();
                }
                if (a2 != null) {
                    i3 = a2.hashCode();
                }
                return i2 ^ i3;
            }
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }

        public Object next() {
            if (hasNext()) {
                this.c++;
                this.d = true;
                return this;
            }
            throw new NoSuchElementException();
        }

        public void remove() {
            if (this.d) {
                MapCollections.this.a(this.c);
                this.c--;
                this.b--;
                this.d = false;
                return;
            }
            throw new IllegalStateException();
        }

        public V setValue(V v) {
            if (this.d) {
                return MapCollections.this.a(this.c, v);
            }
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }

        public String toString() {
            return getKey() + "=" + getValue();
        }
    }

    public final class e implements Collection<V> {
        public e() {
        }

        public boolean add(V v) {
            throw new UnsupportedOperationException();
        }

        public boolean addAll(Collection<? extends V> collection) {
            throw new UnsupportedOperationException();
        }

        public void clear() {
            MapCollections.this.a();
        }

        public boolean contains(Object obj) {
            return MapCollections.this.b(obj) >= 0;
        }

        public boolean containsAll(Collection<?> collection) {
            for (Object contains : collection) {
                if (!contains(contains)) {
                    return false;
                }
            }
            return true;
        }

        public boolean isEmpty() {
            return MapCollections.this.c() == 0;
        }

        public Iterator<V> iterator() {
            return new a(1);
        }

        public boolean remove(Object obj) {
            int b2 = MapCollections.this.b(obj);
            if (b2 < 0) {
                return false;
            }
            MapCollections.this.a(b2);
            return true;
        }

        public boolean removeAll(Collection<?> collection) {
            int c = MapCollections.this.c();
            int i2 = 0;
            boolean z = false;
            while (i2 < c) {
                if (collection.contains(MapCollections.this.a(i2, 1))) {
                    MapCollections.this.a(i2);
                    i2--;
                    c--;
                    z = true;
                }
                i2++;
            }
            return z;
        }

        public boolean retainAll(Collection<?> collection) {
            int c = MapCollections.this.c();
            int i2 = 0;
            boolean z = false;
            while (i2 < c) {
                if (!collection.contains(MapCollections.this.a(i2, 1))) {
                    MapCollections.this.a(i2);
                    i2--;
                    c--;
                    z = true;
                }
                i2++;
            }
            return z;
        }

        public int size() {
            return MapCollections.this.c();
        }

        public Object[] toArray() {
            return MapCollections.this.b(1);
        }

        public <T> T[] toArray(T[] tArr) {
            return MapCollections.this.a(tArr, 1);
        }
    }

    public static <K, V> boolean a(Map<K, V> map, Collection<?> collection) {
        int size = map.size();
        Iterator<K> it = map.keySet().iterator();
        while (it.hasNext()) {
            if (!collection.contains(it.next())) {
                it.remove();
            }
        }
        return size != map.size();
    }

    public abstract int a(Object obj);

    public abstract Object a(int i2, int i3);

    public abstract V a(int i2, V v);

    public abstract void a();

    public abstract void a(int i2);

    public abstract void a(K k2, V v);

    public abstract int b(Object obj);

    public abstract Map<K, V> b();

    public Object[] b(int i2) {
        int c2 = c();
        Object[] objArr = new Object[c2];
        for (int i3 = 0; i3 < c2; i3++) {
            objArr[i3] = a(i3, i2);
        }
        return objArr;
    }

    public abstract int c();

    public <T> T[] a(T[] tArr, int i2) {
        int c2 = c();
        if (tArr.length < c2) {
            tArr = (Object[]) Array.newInstance(tArr.getClass().getComponentType(), c2);
        }
        for (int i3 = 0; i3 < c2; i3++) {
            tArr[i3] = a(i3, i2);
        }
        if (tArr.length > c2) {
            tArr[c2] = null;
        }
        return tArr;
    }

    public static <T> boolean a(Set<T> set, Object obj) {
        if (set == obj) {
            return true;
        }
        if (obj instanceof Set) {
            Set set2 = (Set) obj;
            try {
                if (set.size() != set2.size() || !set.containsAll(set2)) {
                    return false;
                }
                return true;
            } catch (ClassCastException | NullPointerException unused) {
            }
        }
        return false;
    }
}
