package i.e;

import java.util.Map;

/* compiled from: ArraySet */
public class ArraySet0 extends MapCollections<E, E> {
    public final /* synthetic */ ArraySet d;

    public ArraySet0(ArraySet arraySet) {
        this.d = arraySet;
    }

    public Object a(int i2, int i3) {
        return this.d.c[i2];
    }

    public int b(Object obj) {
        return this.d.indexOf(obj);
    }

    public int c() {
        return this.d.d;
    }

    public int a(Object obj) {
        return this.d.indexOf(obj);
    }

    public Map<E, E> b() {
        throw new UnsupportedOperationException("not a map");
    }

    public void a(E e2, E e3) {
        this.d.add(e2);
    }

    public E a(int i2, E e2) {
        throw new UnsupportedOperationException("not a map");
    }

    public void a(int i2) {
        this.d.d(i2);
    }

    public void a() {
        this.d.clear();
    }
}
