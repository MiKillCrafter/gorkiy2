package i.e;

public class LongSparseArray<E> implements Cloneable {

    /* renamed from: f  reason: collision with root package name */
    public static final Object f1063f = new Object();
    public boolean b = false;
    public long[] c;
    public Object[] d;

    /* renamed from: e  reason: collision with root package name */
    public int f1064e;

    public LongSparseArray() {
        int c2 = ContainerHelpers.c(10);
        this.c = new long[c2];
        this.d = new Object[c2];
    }

    public E a(long j2) {
        return b(j2, null);
    }

    public E b(long j2, E e2) {
        int a = ContainerHelpers.a(this.c, this.f1064e, j2);
        if (a >= 0) {
            E[] eArr = this.d;
            if (eArr[a] != f1063f) {
                return eArr[a];
            }
        }
        return e2;
    }

    public final void c() {
        int i2 = this.f1064e;
        long[] jArr = this.c;
        Object[] objArr = this.d;
        int i3 = 0;
        for (int i4 = 0; i4 < i2; i4++) {
            Object obj = objArr[i4];
            if (obj != f1063f) {
                if (i4 != i3) {
                    jArr[i3] = jArr[i4];
                    objArr[i3] = obj;
                    objArr[i4] = null;
                }
                i3++;
            }
        }
        this.b = false;
        this.f1064e = i3;
    }

    public int d() {
        if (this.b) {
            c();
        }
        return this.f1064e;
    }

    public String toString() {
        if (d() <= 0) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(this.f1064e * 28);
        sb.append('{');
        for (int i2 = 0; i2 < this.f1064e; i2++) {
            if (i2 > 0) {
                sb.append(", ");
            }
            if (this.b) {
                c();
            }
            sb.append(this.c[i2]);
            sb.append('=');
            Object a = a(i2);
            if (a != this) {
                sb.append(a);
            } else {
                sb.append("(this Map)");
            }
        }
        sb.append('}');
        return sb.toString();
    }

    public E a(int i2) {
        if (this.b) {
            c();
        }
        return this.d[i2];
    }

    public LongSparseArray<E> clone() {
        try {
            LongSparseArray<E> longSparseArray = (LongSparseArray) super.clone();
            longSparseArray.c = (long[]) this.c.clone();
            longSparseArray.d = (Object[]) this.d.clone();
            return longSparseArray;
        } catch (CloneNotSupportedException e2) {
            throw new AssertionError(e2);
        }
    }

    public void b() {
        int i2 = this.f1064e;
        Object[] objArr = this.d;
        for (int i3 = 0; i3 < i2; i3++) {
            objArr[i3] = null;
        }
        this.f1064e = 0;
        this.b = false;
    }

    public void a(long j2, E e2) {
        int i2 = this.f1064e;
        if (i2 == 0 || j2 > this.c[i2 - 1]) {
            if (this.b && this.f1064e >= this.c.length) {
                c();
            }
            int i3 = this.f1064e;
            if (i3 >= this.c.length) {
                int c2 = ContainerHelpers.c(i3 + 1);
                long[] jArr = new long[c2];
                Object[] objArr = new Object[c2];
                long[] jArr2 = this.c;
                System.arraycopy(jArr2, 0, jArr, 0, jArr2.length);
                Object[] objArr2 = this.d;
                System.arraycopy(objArr2, 0, objArr, 0, objArr2.length);
                this.c = jArr;
                this.d = objArr;
            }
            this.c[i3] = j2;
            this.d[i3] = e2;
            this.f1064e = i3 + 1;
            return;
        }
        c(j2, e2);
    }

    public void c(long j2, E e2) {
        int a = ContainerHelpers.a(this.c, this.f1064e, j2);
        if (a >= 0) {
            this.d[a] = e2;
            return;
        }
        int i2 = ~a;
        if (i2 < this.f1064e) {
            Object[] objArr = this.d;
            if (objArr[i2] == f1063f) {
                this.c[i2] = j2;
                objArr[i2] = e2;
                return;
            }
        }
        if (this.b && this.f1064e >= this.c.length) {
            c();
            i2 = ~ContainerHelpers.a(this.c, this.f1064e, j2);
        }
        int i3 = this.f1064e;
        if (i3 >= this.c.length) {
            int c2 = ContainerHelpers.c(i3 + 1);
            long[] jArr = new long[c2];
            Object[] objArr2 = new Object[c2];
            long[] jArr2 = this.c;
            System.arraycopy(jArr2, 0, jArr, 0, jArr2.length);
            Object[] objArr3 = this.d;
            System.arraycopy(objArr3, 0, objArr2, 0, objArr3.length);
            this.c = jArr;
            this.d = objArr2;
        }
        int i4 = this.f1064e;
        if (i4 - i2 != 0) {
            long[] jArr3 = this.c;
            int i5 = i2 + 1;
            System.arraycopy(jArr3, i2, jArr3, i5, i4 - i2);
            Object[] objArr4 = this.d;
            System.arraycopy(objArr4, i2, objArr4, i5, this.f1064e - i2);
        }
        this.c[i2] = j2;
        this.d[i2] = e2;
        this.f1064e++;
    }
}
