package i.v.a;

import i.v.a.e.FrameworkSQLiteProgram;

public final class SimpleSQLiteQuery {
    public final String a;
    public final Object[] b = null;

    public SimpleSQLiteQuery(String str) {
        this.a = str;
    }

    public void a(SupportSQLiteProgram supportSQLiteProgram) {
        Object[] objArr = this.b;
        if (objArr != null) {
            int length = objArr.length;
            int i2 = 0;
            while (i2 < length) {
                Object obj = objArr[i2];
                i2++;
                if (obj == null) {
                    ((FrameworkSQLiteProgram) supportSQLiteProgram).b.bindNull(i2);
                } else if (obj instanceof byte[]) {
                    ((FrameworkSQLiteProgram) supportSQLiteProgram).b.bindBlob(i2, (byte[]) obj);
                } else if (obj instanceof Float) {
                    ((FrameworkSQLiteProgram) supportSQLiteProgram).b.bindDouble(i2, (double) ((Float) obj).floatValue());
                } else if (obj instanceof Double) {
                    ((FrameworkSQLiteProgram) supportSQLiteProgram).b.bindDouble(i2, ((Double) obj).doubleValue());
                } else if (obj instanceof Long) {
                    ((FrameworkSQLiteProgram) supportSQLiteProgram).b.bindLong(i2, ((Long) obj).longValue());
                } else if (obj instanceof Integer) {
                    ((FrameworkSQLiteProgram) supportSQLiteProgram).b.bindLong(i2, (long) ((Integer) obj).intValue());
                } else if (obj instanceof Short) {
                    ((FrameworkSQLiteProgram) supportSQLiteProgram).b.bindLong(i2, (long) ((Short) obj).shortValue());
                } else if (obj instanceof Byte) {
                    ((FrameworkSQLiteProgram) supportSQLiteProgram).b.bindLong(i2, (long) ((Byte) obj).byteValue());
                } else if (obj instanceof String) {
                    ((FrameworkSQLiteProgram) supportSQLiteProgram).b.bindString(i2, (String) obj);
                } else if (obj instanceof Boolean) {
                    ((FrameworkSQLiteProgram) supportSQLiteProgram).b.bindLong(i2, ((Boolean) obj).booleanValue() ? 1 : 0);
                } else {
                    throw new IllegalArgumentException("Cannot bind " + obj + " at index " + i2 + " Supported types: null, byte[], float, double, long, int, short, byte," + " string");
                }
            }
        }
    }
}
