package i.v.a.e;

import android.database.sqlite.SQLiteProgram;
import i.v.a.SupportSQLiteProgram;

public class FrameworkSQLiteProgram implements SupportSQLiteProgram {
    public final SQLiteProgram b;

    public FrameworkSQLiteProgram(SQLiteProgram sQLiteProgram) {
        this.b = sQLiteProgram;
    }

    public void close() {
        this.b.close();
    }
}
