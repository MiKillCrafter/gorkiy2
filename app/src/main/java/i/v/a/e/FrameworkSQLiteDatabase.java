package i.v.a.e;

import android.database.Cursor;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;
import i.v.a.SimpleSQLiteQuery;

public class FrameworkSQLiteDatabase implements SQLiteDatabase.CursorFactory {
    public final /* synthetic */ SimpleSQLiteQuery a;

    public FrameworkSQLiteDatabase(FrameworkSQLiteDatabase0 frameworkSQLiteDatabase0, SimpleSQLiteQuery simpleSQLiteQuery) {
        this.a = simpleSQLiteQuery;
    }

    public Cursor newCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery) {
        this.a.a(new FrameworkSQLiteProgram(sQLiteQuery));
        return new SQLiteCursor(sQLiteCursorDriver, str, sQLiteQuery);
    }
}
