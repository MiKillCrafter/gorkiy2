package i.v.a;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import java.io.File;

public interface SupportSQLiteOpenHelper {

    public static abstract class a {
        public final int a;

        public a(int i2) {
            this.a = i2;
        }

        public abstract void a(SupportSQLiteDatabase supportSQLiteDatabase, int i2, int i3);

        public final void a(String str) {
            if (!str.equalsIgnoreCase(":memory:") && str.trim().length() != 0) {
                Log.w("SupportSQLite", "deleting the database file: " + str);
                try {
                    SQLiteDatabase.deleteDatabase(new File(str));
                } catch (Exception e2) {
                    Log.w("SupportSQLite", "delete failed: ", e2);
                }
            }
        }
    }

    public static class b {
        public final Context a;
        public final String b;
        public final a c;

        public b(Context context, String str, a aVar) {
            this.a = context;
            this.b = str;
            this.c = aVar;
        }
    }

    public interface c {
        SupportSQLiteOpenHelper a(b bVar);
    }

    SupportSQLiteDatabase a();

    void a(boolean z);

    String b();
}
