package i.l.a;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import androidx.fragment.app.Fragment;
import i.l.a.FragmentManagerImpl;
import i.l.a.FragmentTransaction;
import j.a.a.a.outline;

public class DialogFragment extends Fragment implements DialogInterface.OnCancelListener, DialogInterface.OnDismissListener {
    public Handler X;
    public Runnable Y = new a();
    public int Z = 0;
    public int a0 = 0;
    public boolean b0 = true;
    public boolean c0 = true;
    public int d0 = -1;
    public Dialog e0;
    public boolean f0;
    public boolean g0;
    public boolean h0;

    public class a implements Runnable {
        public a() {
        }

        public void run() {
            DialogFragment dialogFragment = DialogFragment.this;
            Dialog dialog = dialogFragment.e0;
            if (dialog != null) {
                dialogFragment.onDismiss(dialog);
            }
        }
    }

    public void C() {
        super.F = true;
        Dialog dialog = this.e0;
        if (dialog != null) {
            this.f0 = true;
            dialog.setOnDismissListener(null);
            this.e0.dismiss();
            if (!this.g0) {
                onDismiss(this.e0);
            }
            this.e0 = null;
        }
    }

    public void D() {
        super.F = true;
        if (!this.h0 && !this.g0) {
            this.g0 = true;
        }
    }

    public void G() {
        super.F = true;
        Dialog dialog = this.e0;
        if (dialog != null) {
            this.f0 = false;
            dialog.show();
        }
    }

    public void H() {
        super.F = true;
        Dialog dialog = this.e0;
        if (dialog != null) {
            dialog.hide();
        }
    }

    public void a(Context context) {
        super.a(context);
        if (!this.h0) {
            this.g0 = false;
        }
    }

    public void b(Bundle bundle) {
        super.b(bundle);
        this.X = new Handler();
        this.c0 = super.x == 0;
        if (bundle != null) {
            this.Z = bundle.getInt("android:style", 0);
            this.a0 = bundle.getInt("android:theme", 0);
            this.b0 = bundle.getBoolean("android:cancelable", true);
            this.c0 = bundle.getBoolean("android:showsDialog", this.c0);
            this.d0 = bundle.getInt("android:backStackId", -1);
        }
    }

    public LayoutInflater c(Bundle bundle) {
        if (!this.c0) {
            return super.c(bundle);
        }
        Dialog g = g(bundle);
        this.e0 = g;
        if (g == null) {
            return (LayoutInflater) super.f233t.c.getSystemService("layout_inflater");
        }
        a(g, this.Z);
        return (LayoutInflater) this.e0.getContext().getSystemService("layout_inflater");
    }

    public void d(Bundle bundle) {
        Bundle onSaveInstanceState;
        Dialog dialog = this.e0;
        if (!(dialog == null || (onSaveInstanceState = dialog.onSaveInstanceState()) == null)) {
            bundle.putBundle("android:savedDialogState", onSaveInstanceState);
        }
        int i2 = this.Z;
        if (i2 != 0) {
            bundle.putInt("android:style", i2);
        }
        int i3 = this.a0;
        if (i3 != 0) {
            bundle.putInt("android:theme", i3);
        }
        boolean z = this.b0;
        if (!z) {
            bundle.putBoolean("android:cancelable", z);
        }
        boolean z2 = this.c0;
        if (!z2) {
            bundle.putBoolean("android:showsDialog", z2);
        }
        int i4 = this.d0;
        if (i4 != -1) {
            bundle.putInt("android:backStackId", i4);
        }
    }

    public Dialog g(Bundle bundle) {
        return new Dialog(J(), this.a0);
    }

    public void onCancel(DialogInterface dialogInterface) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentManagerImpl.a(i.l.a.FragmentManagerImpl$h, boolean):void
     arg types: [i.l.a.FragmentManagerImpl$i, int]
     candidates:
      i.l.a.FragmentManagerImpl.a(int, i.l.a.BackStackRecord):void
      i.l.a.FragmentManagerImpl.a(int, boolean):void
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, i.o.Lifecycle$b):void
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, boolean):void
      i.l.a.FragmentManagerImpl.a(java.util.ArrayList<i.l.a.a>, java.util.ArrayList<java.lang.Boolean>):void
      i.l.a.FragmentManagerImpl.a(android.view.Menu, android.view.MenuInflater):boolean
      i.l.a.FragmentManagerImpl.a(i.l.a.FragmentManagerImpl$h, boolean):void */
    public void onDismiss(DialogInterface dialogInterface) {
        if (!this.f0 && !this.g0) {
            this.g0 = true;
            this.h0 = false;
            Dialog dialog = this.e0;
            if (dialog != null) {
                dialog.setOnDismissListener(null);
                this.e0.dismiss();
            }
            this.f0 = true;
            if (this.d0 >= 0) {
                FragmentManager K = K();
                int i2 = this.d0;
                FragmentManagerImpl fragmentManagerImpl = (FragmentManagerImpl) K;
                if (fragmentManagerImpl == null) {
                    throw null;
                } else if (i2 >= 0) {
                    fragmentManagerImpl.a((FragmentManagerImpl.h) new FragmentManagerImpl.i(null, i2, 1), false);
                    this.d0 = -1;
                } else {
                    throw new IllegalArgumentException(outline.b("Bad id: ", i2));
                }
            } else {
                FragmentManagerImpl fragmentManagerImpl2 = (FragmentManagerImpl) K();
                if (fragmentManagerImpl2 != null) {
                    BackStackRecord backStackRecord = new BackStackRecord(fragmentManagerImpl2);
                    FragmentManagerImpl fragmentManagerImpl3 = super.f232s;
                    if (fragmentManagerImpl3 == null || fragmentManagerImpl3 == backStackRecord.f1275s) {
                        backStackRecord.a(new FragmentTransaction.a(3, super));
                        backStackRecord.a(true);
                        return;
                    }
                    StringBuilder a2 = outline.a("Cannot remove Fragment attached to a different FragmentManager. Fragment ");
                    a2.append(toString());
                    a2.append(" is already attached to a FragmentManager.");
                    throw new IllegalStateException(a2.toString());
                }
                throw null;
            }
        }
    }

    public void a(Dialog dialog, int i2) {
        if (!(i2 == 1 || i2 == 2)) {
            if (i2 == 3) {
                dialog.getWindow().addFlags(24);
            } else {
                return;
            }
        }
        dialog.requestWindowFeature(1);
    }

    public void a(Bundle bundle) {
        Bundle bundle2;
        super.F = true;
        if (this.c0) {
            View view = super.H;
            if (view != null) {
                if (view.getParent() == null) {
                    this.e0.setContentView(view);
                } else {
                    throw new IllegalStateException("DialogFragment can not be attached to a container view");
                }
            }
            FragmentActivity l2 = l();
            if (l2 != null) {
                this.e0.setOwnerActivity(l2);
            }
            this.e0.setCancelable(this.b0);
            this.e0.setOnCancelListener(this);
            this.e0.setOnDismissListener(this);
            if (bundle != null && (bundle2 = bundle.getBundle("android:savedDialogState")) != null) {
                this.e0.onRestoreInstanceState(bundle2);
            }
        }
    }
}
