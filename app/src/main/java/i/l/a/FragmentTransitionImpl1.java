package i.l.a;

import android.view.View;
import i.h.l.ViewCompat;
import java.util.ArrayList;

/* compiled from: FragmentTransitionImpl */
public class FragmentTransitionImpl1 implements Runnable {
    public final /* synthetic */ int b;
    public final /* synthetic */ ArrayList c;
    public final /* synthetic */ ArrayList d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ ArrayList f1354e;

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ ArrayList f1355f;

    public FragmentTransitionImpl1(FragmentTransitionImpl0 fragmentTransitionImpl0, int i2, ArrayList arrayList, ArrayList arrayList2, ArrayList arrayList3, ArrayList arrayList4) {
        this.b = i2;
        this.c = arrayList;
        this.d = arrayList2;
        this.f1354e = arrayList3;
        this.f1355f = arrayList4;
    }

    public void run() {
        for (int i2 = 0; i2 < this.b; i2++) {
            ViewCompat.a((View) this.c.get(i2), (String) this.d.get(i2));
            ((View) this.f1354e.get(i2)).setTransitionName((String) this.f1355f.get(i2));
        }
    }
}
