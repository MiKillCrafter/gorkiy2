package i.l.a;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.fragment.app.Fragment;

@SuppressLint({"BanParcelableUsage"})
public final class FragmentState implements Parcelable {
    public static final Parcelable.Creator<q> CREATOR = new a();
    public final String b;
    public final String c;
    public final boolean d;

    /* renamed from: e  reason: collision with root package name */
    public final int f1315e;

    /* renamed from: f  reason: collision with root package name */
    public final int f1316f;
    public final String g;
    public final boolean h;

    /* renamed from: i  reason: collision with root package name */
    public final boolean f1317i;

    /* renamed from: j  reason: collision with root package name */
    public final boolean f1318j;

    /* renamed from: k  reason: collision with root package name */
    public final Bundle f1319k;

    /* renamed from: l  reason: collision with root package name */
    public final boolean f1320l;

    /* renamed from: m  reason: collision with root package name */
    public final int f1321m;

    /* renamed from: n  reason: collision with root package name */
    public Bundle f1322n;

    /* renamed from: o  reason: collision with root package name */
    public Fragment f1323o;

    public static class a implements Parcelable.Creator<q> {
        public Object createFromParcel(Parcel parcel) {
            return new FragmentState(parcel);
        }

        public Object[] newArray(int i2) {
            return new FragmentState[i2];
        }
    }

    public FragmentState(Fragment fragment) {
        this.b = fragment.getClass().getName();
        this.c = fragment.f221f;
        this.d = fragment.f227n;
        this.f1315e = fragment.w;
        this.f1316f = fragment.x;
        this.g = fragment.y;
        this.h = fragment.B;
        this.f1317i = fragment.f226m;
        this.f1318j = fragment.A;
        this.f1319k = fragment.g;
        this.f1320l = fragment.z;
        this.f1321m = fragment.R.ordinal();
    }

    public int describeContents() {
        return 0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("FragmentState{");
        sb.append(this.b);
        sb.append(" (");
        sb.append(this.c);
        sb.append(")}:");
        if (this.d) {
            sb.append(" fromLayout");
        }
        if (this.f1316f != 0) {
            sb.append(" id=0x");
            sb.append(Integer.toHexString(this.f1316f));
        }
        String str = this.g;
        if (str != null && !str.isEmpty()) {
            sb.append(" tag=");
            sb.append(this.g);
        }
        if (this.h) {
            sb.append(" retainInstance");
        }
        if (this.f1317i) {
            sb.append(" removing");
        }
        if (this.f1318j) {
            sb.append(" detached");
        }
        if (this.f1320l) {
            sb.append(" hidden");
        }
        return sb.toString();
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeInt(this.d ? 1 : 0);
        parcel.writeInt(this.f1315e);
        parcel.writeInt(this.f1316f);
        parcel.writeString(this.g);
        parcel.writeInt(this.h ? 1 : 0);
        parcel.writeInt(this.f1317i ? 1 : 0);
        parcel.writeInt(this.f1318j ? 1 : 0);
        parcel.writeBundle(this.f1319k);
        parcel.writeInt(this.f1320l ? 1 : 0);
        parcel.writeBundle(this.f1322n);
        parcel.writeInt(this.f1321m);
    }

    public FragmentState(Parcel parcel) {
        this.b = parcel.readString();
        this.c = parcel.readString();
        boolean z = true;
        this.d = parcel.readInt() != 0;
        this.f1315e = parcel.readInt();
        this.f1316f = parcel.readInt();
        this.g = parcel.readString();
        this.h = parcel.readInt() != 0;
        this.f1317i = parcel.readInt() != 0;
        this.f1318j = parcel.readInt() != 0;
        this.f1319k = parcel.readBundle();
        this.f1320l = parcel.readInt() == 0 ? false : z;
        this.f1322n = parcel.readBundle();
        this.f1321m = parcel.readInt();
    }
}
