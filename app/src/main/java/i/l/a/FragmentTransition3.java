package i.l.a;

import android.view.View;
import androidx.fragment.app.Fragment;
import i.e.ArrayMap;
import i.e.MapCollections;
import i.h.l.ViewCompat;
import i.l.a.w;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/* compiled from: FragmentTransition */
public class FragmentTransition3 {
    public static final int[] a = {0, 3, 0, 1, 5, 4, 7, 6, 9, 8, 10};
    public static final FragmentTransitionImpl0 b = new FragmentTransitionCompat21();
    public static final FragmentTransitionImpl0 c;

    /* compiled from: FragmentTransition */
    public static class a {
        public Fragment a;
        public boolean b;
        public BackStackRecord c;
        public Fragment d;

        /* renamed from: e  reason: collision with root package name */
        public boolean f1350e;

        /* renamed from: f  reason: collision with root package name */
        public BackStackRecord f1351f;
    }

    static {
        FragmentTransitionImpl0 fragmentTransitionImpl0;
        try {
            fragmentTransitionImpl0 = (FragmentTransitionImpl0) Class.forName("i.w.d").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            fragmentTransitionImpl0 = null;
        }
        c = fragmentTransitionImpl0;
    }

    /* JADX WARN: Type inference failed for: r3v8, types: [android.view.View] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentTransition3.a(i.l.a.a, i.l.a.r$a, android.util.SparseArray<i.l.a.w$a>, boolean, boolean):void
     arg types: [i.l.a.BackStackRecord, i.l.a.FragmentTransaction$a, android.util.SparseArray, int, boolean]
     candidates:
      i.l.a.FragmentTransition3.a(i.l.a.b0, java.lang.Object, androidx.fragment.app.Fragment, java.util.ArrayList<android.view.View>, android.view.View):java.util.ArrayList<android.view.View>
      i.l.a.FragmentTransition3.a(androidx.fragment.app.Fragment, androidx.fragment.app.Fragment, boolean, i.e.ArrayMap<java.lang.String, android.view.View>, boolean):void
      i.l.a.FragmentTransition3.a(i.l.a.a, i.l.a.r$a, android.util.SparseArray<i.l.a.w$a>, boolean, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentTransition3.a(i.l.a.b0, i.e.ArrayMap<java.lang.String, java.lang.String>, java.lang.Object, i.l.a.w$a):i.e.ArrayMap<java.lang.String, android.view.View>
     arg types: [i.l.a.FragmentTransitionImpl0, i.e.ArrayMap, java.lang.Object, i.l.a.FragmentTransition3$a]
     candidates:
      i.l.a.FragmentTransition3.a(i.e.ArrayMap<java.lang.String, android.view.View>, i.l.a.w$a, java.lang.Object, boolean):android.view.View
      i.l.a.FragmentTransition3.a(i.l.a.FragmentTransitionImpl0, androidx.fragment.app.Fragment, androidx.fragment.app.Fragment, boolean):java.lang.Object
      i.l.a.FragmentTransition3.a(i.l.a.b0, i.e.ArrayMap<java.lang.String, java.lang.String>, java.lang.Object, i.l.a.w$a):i.e.ArrayMap<java.lang.String, android.view.View> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentTransition3.a(androidx.fragment.app.Fragment, androidx.fragment.app.Fragment, boolean, i.e.ArrayMap<java.lang.String, android.view.View>, boolean):void
     arg types: [androidx.fragment.app.Fragment, androidx.fragment.app.Fragment, boolean, i.e.ArrayMap<java.lang.String, android.view.View>, int]
     candidates:
      i.l.a.FragmentTransition3.a(i.l.a.b0, java.lang.Object, androidx.fragment.app.Fragment, java.util.ArrayList<android.view.View>, android.view.View):java.util.ArrayList<android.view.View>
      i.l.a.FragmentTransition3.a(i.l.a.a, i.l.a.r$a, android.util.SparseArray<i.l.a.w$a>, boolean, boolean):void
      i.l.a.FragmentTransition3.a(androidx.fragment.app.Fragment, androidx.fragment.app.Fragment, boolean, i.e.ArrayMap<java.lang.String, android.view.View>, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentTransition3.a(i.l.a.b0, java.lang.Object, java.lang.Object, i.e.ArrayMap<java.lang.String, android.view.View>, boolean, i.l.a.a):void
     arg types: [i.l.a.FragmentTransitionImpl0, java.lang.Object, java.lang.Object, i.e.ArrayMap<java.lang.String, android.view.View>, boolean, i.l.a.BackStackRecord]
     candidates:
      i.l.a.FragmentTransition3.a(i.l.a.FragmentTransitionImpl0, java.lang.Object, java.lang.Object, java.lang.Object, androidx.fragment.app.Fragment, boolean):java.lang.Object
      i.l.a.FragmentTransition3.a(i.l.a.k, java.util.ArrayList<i.l.a.a>, java.util.ArrayList<java.lang.Boolean>, int, int, boolean):void
      i.l.a.FragmentTransition3.a(i.l.a.b0, java.lang.Object, java.lang.Object, i.e.ArrayMap<java.lang.String, android.view.View>, boolean, i.l.a.a):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentTransition3.a(i.e.ArrayMap<java.lang.String, android.view.View>, i.l.a.w$a, java.lang.Object, boolean):android.view.View
     arg types: [i.e.ArrayMap<java.lang.String, android.view.View>, i.l.a.FragmentTransition3$a, java.lang.Object, boolean]
     candidates:
      i.l.a.FragmentTransition3.a(i.l.a.b0, i.e.ArrayMap<java.lang.String, java.lang.String>, java.lang.Object, i.l.a.w$a):i.e.ArrayMap<java.lang.String, android.view.View>
      i.l.a.FragmentTransition3.a(i.l.a.FragmentTransitionImpl0, androidx.fragment.app.Fragment, androidx.fragment.app.Fragment, boolean):java.lang.Object
      i.l.a.FragmentTransition3.a(i.e.ArrayMap<java.lang.String, android.view.View>, i.l.a.w$a, java.lang.Object, boolean):android.view.View */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentTransition3.a(i.l.a.b0, java.lang.Object, androidx.fragment.app.Fragment, java.util.ArrayList<android.view.View>, android.view.View):java.util.ArrayList<android.view.View>
     arg types: [i.l.a.FragmentTransitionImpl0, java.lang.Object, androidx.fragment.app.Fragment, java.util.ArrayList, android.view.View]
     candidates:
      i.l.a.FragmentTransition3.a(androidx.fragment.app.Fragment, androidx.fragment.app.Fragment, boolean, i.e.ArrayMap<java.lang.String, android.view.View>, boolean):void
      i.l.a.FragmentTransition3.a(i.l.a.a, i.l.a.r$a, android.util.SparseArray<i.l.a.w$a>, boolean, boolean):void
      i.l.a.FragmentTransition3.a(i.l.a.b0, java.lang.Object, androidx.fragment.app.Fragment, java.util.ArrayList<android.view.View>, android.view.View):java.util.ArrayList<android.view.View> */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:151:0x040f  */
    /* JADX WARNING: Removed duplicated region for block: B:160:0x044d A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x0222 A[ADDED_TO_REGION] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(i.l.a.k r39, java.util.ArrayList<i.l.a.a> r40, java.util.ArrayList<java.lang.Boolean> r41, int r42, int r43, boolean r44) {
        /*
            r0 = r39
            r1 = r40
            r2 = r41
            r3 = r43
            r4 = r44
            int r5 = r0.f1304p
            r6 = 1
            if (r5 >= r6) goto L_0x0010
            return
        L_0x0010:
            android.util.SparseArray r5 = new android.util.SparseArray
            r5.<init>()
            r7 = r42
        L_0x0017:
            r8 = 0
            if (r7 >= r3) goto L_0x0068
            java.lang.Object r9 = r1.get(r7)
            i.l.a.BackStackRecord r9 = (i.l.a.BackStackRecord) r9
            java.lang.Object r10 = r2.get(r7)
            java.lang.Boolean r10 = (java.lang.Boolean) r10
            boolean r10 = r10.booleanValue()
            if (r10 == 0) goto L_0x004e
            i.l.a.FragmentManagerImpl r8 = r9.f1275s
            i.l.a.FragmentContainer r8 = r8.f1306r
            boolean r8 = r8.b()
            if (r8 != 0) goto L_0x0037
            goto L_0x0065
        L_0x0037:
            java.util.ArrayList<i.l.a.r$a> r8 = r9.a
            int r8 = r8.size()
            int r8 = r8 - r6
        L_0x003e:
            if (r8 < 0) goto L_0x0065
            java.util.ArrayList<i.l.a.r$a> r10 = r9.a
            java.lang.Object r10 = r10.get(r8)
            i.l.a.FragmentTransaction$a r10 = (i.l.a.FragmentTransaction.a) r10
            a(r9, r10, r5, r6, r4)
            int r8 = r8 + -1
            goto L_0x003e
        L_0x004e:
            java.util.ArrayList<i.l.a.r$a> r10 = r9.a
            int r10 = r10.size()
            r11 = 0
        L_0x0055:
            if (r11 >= r10) goto L_0x0065
            java.util.ArrayList<i.l.a.r$a> r12 = r9.a
            java.lang.Object r12 = r12.get(r11)
            i.l.a.FragmentTransaction$a r12 = (i.l.a.FragmentTransaction.a) r12
            a(r9, r12, r5, r8, r4)
            int r11 = r11 + 1
            goto L_0x0055
        L_0x0065:
            int r7 = r7 + 1
            goto L_0x0017
        L_0x0068:
            int r7 = r5.size()
            if (r7 == 0) goto L_0x0461
            android.view.View r7 = new android.view.View
            i.l.a.FragmentHostCallback r9 = r0.f1305q
            android.content.Context r9 = r9.c
            r7.<init>(r9)
            int r15 = r5.size()
            r14 = 0
        L_0x007c:
            if (r14 >= r15) goto L_0x0461
            int r9 = r5.keyAt(r14)
            i.e.ArrayMap r13 = new i.e.ArrayMap
            r13.<init>()
            int r10 = r3 + -1
            r12 = r42
        L_0x008b:
            if (r10 < r12) goto L_0x00f6
            java.lang.Object r11 = r1.get(r10)
            i.l.a.BackStackRecord r11 = (i.l.a.BackStackRecord) r11
            boolean r16 = r11.b(r9)
            if (r16 != 0) goto L_0x009a
            goto L_0x00eb
        L_0x009a:
            java.lang.Object r16 = r2.get(r10)
            java.lang.Boolean r16 = (java.lang.Boolean) r16
            boolean r16 = r16.booleanValue()
            java.util.ArrayList<java.lang.String> r6 = r11.f1332o
            if (r6 == 0) goto L_0x00eb
            int r6 = r6.size()
            if (r16 == 0) goto L_0x00b3
            java.util.ArrayList<java.lang.String> r8 = r11.f1332o
            java.util.ArrayList<java.lang.String> r11 = r11.f1333p
            goto L_0x00bc
        L_0x00b3:
            java.util.ArrayList<java.lang.String> r8 = r11.f1332o
            java.util.ArrayList<java.lang.String> r11 = r11.f1333p
            r38 = r11
            r11 = r8
            r8 = r38
        L_0x00bc:
            r1 = 0
        L_0x00bd:
            if (r1 >= r6) goto L_0x00eb
            java.lang.Object r16 = r11.get(r1)
            r2 = r16
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r16 = r8.get(r1)
            r3 = r16
            java.lang.String r3 = (java.lang.String) r3
            java.lang.Object r16 = r13.remove(r3)
            r17 = r6
            r6 = r16
            java.lang.String r6 = (java.lang.String) r6
            if (r6 == 0) goto L_0x00df
            r13.put(r2, r6)
            goto L_0x00e2
        L_0x00df:
            r13.put(r2, r3)
        L_0x00e2:
            int r1 = r1 + 1
            r2 = r41
            r3 = r43
            r6 = r17
            goto L_0x00bd
        L_0x00eb:
            int r10 = r10 + -1
            r1 = r40
            r2 = r41
            r3 = r43
            r6 = 1
            r8 = 0
            goto L_0x008b
        L_0x00f6:
            java.lang.Object r1 = r5.valueAt(r14)
            i.l.a.FragmentTransition3$a r1 = (i.l.a.FragmentTransition3.a) r1
            if (r4 == 0) goto L_0x02ea
            i.l.a.FragmentContainer r3 = r0.f1306r
            boolean r3 = r3.b()
            if (r3 == 0) goto L_0x010f
            i.l.a.FragmentContainer r3 = r0.f1306r
            android.view.View r3 = r3.a(r9)
            android.view.ViewGroup r3 = (android.view.ViewGroup) r3
            goto L_0x0110
        L_0x010f:
            r3 = 0
        L_0x0110:
            if (r3 != 0) goto L_0x011a
        L_0x0112:
            r31 = r5
            r32 = r14
            r33 = r15
            goto L_0x02e3
        L_0x011a:
            androidx.fragment.app.Fragment r6 = r1.a
            androidx.fragment.app.Fragment r8 = r1.d
            i.l.a.FragmentTransitionImpl0 r9 = a(r8, r6)
            if (r9 != 0) goto L_0x0125
            goto L_0x0112
        L_0x0125:
            boolean r10 = r1.b
            boolean r11 = r1.f1350e
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            r31 = r5
            java.lang.Object r5 = a(r9, r6, r10)
            java.lang.Object r11 = b(r9, r8, r11)
            androidx.fragment.app.Fragment r12 = r1.a
            r32 = r14
            androidx.fragment.app.Fragment r14 = r1.d
            r33 = r15
            if (r12 == 0) goto L_0x014f
            android.view.View r15 = r12.L()
            r0 = 0
            r15.setVisibility(r0)
        L_0x014f:
            if (r12 == 0) goto L_0x01ef
            if (r14 != 0) goto L_0x0155
            goto L_0x01ef
        L_0x0155:
            boolean r0 = r1.b
            boolean r15 = r13.isEmpty()
            if (r15 == 0) goto L_0x0161
            r34 = r10
            r15 = 0
            goto L_0x0167
        L_0x0161:
            java.lang.Object r15 = a(r9, r12, r14, r0)
            r34 = r10
        L_0x0167:
            i.e.ArrayMap r10 = b(r9, r13, r15, r1)
            r35 = r6
            i.e.ArrayMap r6 = a(r9, r13, r15, r1)
            boolean r16 = r13.isEmpty()
            if (r16 == 0) goto L_0x0183
            if (r10 == 0) goto L_0x017c
            r10.clear()
        L_0x017c:
            if (r6 == 0) goto L_0x0181
            r6.clear()
        L_0x0181:
            r15 = 0
            goto L_0x0195
        L_0x0183:
            r16 = r15
            java.util.Set r15 = r13.keySet()
            a(r4, r10, r15)
            java.util.Collection r15 = r13.values()
            a(r2, r6, r15)
            r15 = r16
        L_0x0195:
            if (r5 != 0) goto L_0x019e
            if (r11 != 0) goto L_0x019e
            if (r15 != 0) goto L_0x019e
            r37 = r2
            goto L_0x01f5
        L_0x019e:
            r36 = r13
            r13 = 1
            a(r12, r14, r0, r10, r13)
            if (r15 == 0) goto L_0x01d4
            r2.add(r7)
            r9.b(r15, r7, r4)
            boolean r13 = r1.f1350e
            r37 = r2
            i.l.a.BackStackRecord r2 = r1.f1351f
            r16 = r9
            r17 = r15
            r18 = r11
            r19 = r10
            r20 = r13
            r21 = r2
            a(r16, r17, r18, r19, r20, r21)
            android.graphics.Rect r2 = new android.graphics.Rect
            r2.<init>()
            android.view.View r1 = a(r6, r1, r5, r0)
            if (r1 == 0) goto L_0x01cf
            r9.a(r5, r2)
        L_0x01cf:
            r27 = r1
            r29 = r2
            goto L_0x01da
        L_0x01d4:
            r37 = r2
            r27 = 0
            r29 = 0
        L_0x01da:
            i.l.a.FragmentTransition1 r1 = new i.l.a.FragmentTransition1
            r22 = r1
            r23 = r12
            r24 = r14
            r25 = r0
            r26 = r6
            r28 = r9
            r22.<init>(r23, r24, r25, r26, r27, r28, r29)
            i.h.l.OneShotPreDrawListener.a(r3, r1)
            goto L_0x01f8
        L_0x01ef:
            r37 = r2
            r35 = r6
            r34 = r10
        L_0x01f5:
            r36 = r13
            r15 = 0
        L_0x01f8:
            if (r5 != 0) goto L_0x0200
            if (r15 != 0) goto L_0x0200
            if (r11 != 0) goto L_0x0200
            goto L_0x02e3
        L_0x0200:
            java.util.ArrayList r0 = a(r9, r11, r8, r4, r7)
            r1 = r35
            r2 = r37
            java.util.ArrayList r6 = a(r9, r5, r1, r2, r7)
            r10 = 4
            a(r6, r10)
            r16 = r9
            r17 = r5
            r18 = r11
            r19 = r15
            r20 = r1
            r21 = r34
            java.lang.Object r1 = a(r16, r17, r18, r19, r20, r21)
            if (r1 == 0) goto L_0x02e3
            if (r8 == 0) goto L_0x0245
            if (r11 == 0) goto L_0x0245
            boolean r10 = r8.f225l
            if (r10 == 0) goto L_0x0245
            boolean r10 = r8.z
            if (r10 == 0) goto L_0x0245
            boolean r10 = r8.N
            if (r10 == 0) goto L_0x0245
            r10 = 1
            r8.a(r10)
            android.view.View r10 = r8.H
            r9.a(r11, r10, r0)
            android.view.ViewGroup r8 = r8.G
            i.l.a.FragmentTransition r10 = new i.l.a.FragmentTransition
            r10.<init>(r0)
            i.h.l.OneShotPreDrawListener.a(r8, r10)
        L_0x0245:
            java.util.ArrayList r8 = new java.util.ArrayList
            r8.<init>()
            int r10 = r2.size()
            r12 = 0
        L_0x024f:
            if (r12 >= r10) goto L_0x0265
            java.lang.Object r13 = r2.get(r12)
            android.view.View r13 = (android.view.View) r13
            java.lang.String r14 = i.h.l.ViewCompat.p(r13)
            r8.add(r14)
            r14 = 0
            r13.setTransitionName(r14)
            int r12 = r12 + 1
            goto L_0x024f
        L_0x0265:
            r22 = r9
            r23 = r1
            r24 = r5
            r25 = r6
            r26 = r11
            r27 = r0
            r28 = r15
            r29 = r2
            r22.a(r23, r24, r25, r26, r27, r28, r29)
            r9.a(r3, r1)
            int r0 = r2.size()
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r5 = 0
        L_0x0285:
            if (r5 >= r0) goto L_0x02c5
            java.lang.Object r10 = r4.get(r5)
            android.view.View r10 = (android.view.View) r10
            java.lang.String r11 = i.h.l.ViewCompat.p(r10)
            r1.add(r11)
            if (r11 != 0) goto L_0x0299
            r13 = r36
            goto L_0x02c0
        L_0x0299:
            r14 = 0
            r10.setTransitionName(r14)
            r13 = r36
            java.lang.Object r10 = r13.get(r11)
            java.lang.String r10 = (java.lang.String) r10
            r12 = 0
        L_0x02a6:
            if (r12 >= r0) goto L_0x02c0
            java.lang.Object r14 = r8.get(r12)
            boolean r14 = r10.equals(r14)
            if (r14 == 0) goto L_0x02bc
            java.lang.Object r10 = r2.get(r12)
            android.view.View r10 = (android.view.View) r10
            r10.setTransitionName(r11)
            goto L_0x02c0
        L_0x02bc:
            int r12 = r12 + 1
            r14 = 0
            goto L_0x02a6
        L_0x02c0:
            int r5 = r5 + 1
            r36 = r13
            goto L_0x0285
        L_0x02c5:
            i.l.a.FragmentTransitionImpl1 r5 = new i.l.a.FragmentTransitionImpl1
            r22 = r5
            r23 = r9
            r24 = r0
            r25 = r2
            r26 = r8
            r27 = r4
            r28 = r1
            r22.<init>(r23, r24, r25, r26, r27, r28)
            i.h.l.OneShotPreDrawListener.a(r3, r5)
            r0 = 0
            a(r6, r0)
            r9.b(r15, r4, r2)
            goto L_0x02e4
        L_0x02e3:
            r0 = 0
        L_0x02e4:
            r27 = r32
            r30 = r33
            goto L_0x044d
        L_0x02ea:
            r31 = r5
            r32 = r14
            r33 = r15
            r0 = 0
            r2 = r39
            i.l.a.FragmentContainer r3 = r2.f1306r
            boolean r3 = r3.b()
            if (r3 == 0) goto L_0x0306
            i.l.a.FragmentContainer r3 = r2.f1306r
            android.view.View r3 = r3.a(r9)
            r14 = r3
            android.view.ViewGroup r14 = (android.view.ViewGroup) r14
            r3 = r14
            goto L_0x0307
        L_0x0306:
            r3 = 0
        L_0x0307:
            if (r3 != 0) goto L_0x030a
            goto L_0x02e4
        L_0x030a:
            androidx.fragment.app.Fragment r4 = r1.a
            androidx.fragment.app.Fragment r5 = r1.d
            i.l.a.FragmentTransitionImpl0 r6 = a(r5, r4)
            if (r6 != 0) goto L_0x0315
            goto L_0x02e4
        L_0x0315:
            boolean r8 = r1.b
            boolean r9 = r1.f1350e
            java.lang.Object r8 = a(r6, r4, r8)
            java.lang.Object r12 = b(r6, r5, r9)
            java.util.ArrayList r11 = new java.util.ArrayList
            r11.<init>()
            java.util.ArrayList r10 = new java.util.ArrayList
            r10.<init>()
            androidx.fragment.app.Fragment r9 = r1.a
            androidx.fragment.app.Fragment r15 = r1.d
            if (r9 == 0) goto L_0x03cd
            if (r15 != 0) goto L_0x0335
            goto L_0x03cd
        L_0x0335:
            boolean r14 = r1.b
            boolean r16 = r13.isEmpty()
            if (r16 == 0) goto L_0x033f
            r0 = 0
            goto L_0x0345
        L_0x033f:
            java.lang.Object r16 = a(r6, r9, r15, r14)
            r0 = r16
        L_0x0345:
            i.e.ArrayMap r2 = b(r6, r13, r0, r1)
            boolean r16 = r13.isEmpty()
            if (r16 == 0) goto L_0x0351
            r0 = 0
            goto L_0x035c
        L_0x0351:
            r16 = r0
            java.util.Collection r0 = r2.values()
            r11.addAll(r0)
            r0 = r16
        L_0x035c:
            if (r8 != 0) goto L_0x0364
            if (r12 != 0) goto L_0x0364
            if (r0 != 0) goto L_0x0364
            goto L_0x03cd
        L_0x0364:
            r22 = r4
            r4 = 1
            a(r9, r15, r14, r2, r4)
            if (r0 == 0) goto L_0x0393
            android.graphics.Rect r4 = new android.graphics.Rect
            r4.<init>()
            r6.b(r0, r7, r11)
            r20 = r9
            boolean r9 = r1.f1350e
            r21 = r10
            i.l.a.BackStackRecord r10 = r1.f1351f
            r23 = r14
            r14 = r6
            r24 = r15
            r15 = r0
            r16 = r12
            r17 = r2
            r18 = r9
            r19 = r10
            a(r14, r15, r16, r17, r18, r19)
            if (r8 == 0) goto L_0x039c
            r6.a(r8, r4)
            goto L_0x039c
        L_0x0393:
            r20 = r9
            r21 = r10
            r23 = r14
            r24 = r15
            r4 = 0
        L_0x039c:
            i.l.a.FragmentTransition2 r2 = new i.l.a.FragmentTransition2
            r16 = r20
            r9 = r2
            r15 = r21
            r10 = r6
            r14 = r11
            r11 = r13
            r25 = r5
            r5 = r12
            r12 = r0
            r26 = r0
            r0 = r13
            r13 = r1
            r28 = r14
            r27 = r32
            r29 = 0
            r14 = r15
            r32 = r15
            r30 = r33
            r15 = r7
            r17 = r24
            r18 = r23
            r19 = r28
            r20 = r8
            r21 = r4
            r9.<init>(r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21)
            i.h.l.OneShotPreDrawListener.a(r3, r2)
            r20 = r26
            goto L_0x03df
        L_0x03cd:
            r22 = r4
            r25 = r5
            r28 = r11
            r5 = r12
            r0 = r13
            r27 = r32
            r30 = r33
            r29 = 0
            r32 = r10
            r20 = r29
        L_0x03df:
            if (r8 != 0) goto L_0x03e7
            if (r20 != 0) goto L_0x03e7
            if (r5 != 0) goto L_0x03e7
            goto L_0x044d
        L_0x03e7:
            r2 = r25
            r4 = r28
            java.util.ArrayList r2 = a(r6, r5, r2, r4, r7)
            if (r2 == 0) goto L_0x03fa
            boolean r4 = r2.isEmpty()
            if (r4 == 0) goto L_0x03f8
            goto L_0x03fa
        L_0x03f8:
            r29 = r5
        L_0x03fa:
            r6.a(r8, r7)
            boolean r1 = r1.b
            r14 = r6
            r15 = r8
            r16 = r29
            r17 = r20
            r18 = r22
            r19 = r1
            java.lang.Object r1 = a(r14, r15, r16, r17, r18, r19)
            if (r1 == 0) goto L_0x044d
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            r14 = r6
            r15 = r1
            r16 = r8
            r17 = r4
            r18 = r29
            r19 = r2
            r21 = r32
            r14.a(r15, r16, r17, r18, r19, r20, r21)
            i.l.a.FragmentTransition0 r5 = new i.l.a.FragmentTransition0
            r9 = r5
            r10 = r8
            r11 = r6
            r12 = r7
            r13 = r22
            r14 = r32
            r15 = r4
            r16 = r2
            r17 = r29
            r9.<init>(r10, r11, r12, r13, r14, r15, r16, r17)
            i.h.l.OneShotPreDrawListener.a(r3, r5)
            i.l.a.FragmentTransitionImpl2 r2 = new i.l.a.FragmentTransitionImpl2
            r4 = r32
            r2.<init>(r6, r4, r0)
            i.h.l.OneShotPreDrawListener.a(r3, r2)
            r6.a(r3, r1)
            i.l.a.FragmentTransitionImpl r1 = new i.l.a.FragmentTransitionImpl
            r1.<init>(r6, r4, r0)
            i.h.l.OneShotPreDrawListener.a(r3, r1)
        L_0x044d:
            int r14 = r27 + 1
            r1 = r40
            r2 = r41
            r3 = r43
            r4 = r44
            r15 = r30
            r5 = r31
            r6 = 1
            r8 = 0
            r0 = r39
            goto L_0x007c
        L_0x0461:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: i.l.a.FragmentTransition3.a(i.l.a.FragmentManagerImpl, java.util.ArrayList, java.util.ArrayList, int, int, boolean):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.e.MapCollections.a(java.util.Map, java.util.Collection<?>):boolean
     arg types: [i.e.ArrayMap<java.lang.String, android.view.View>, java.util.ArrayList<java.lang.String>]
     candidates:
      i.e.MapCollections.a(java.util.Set, java.lang.Object):boolean
      i.e.MapCollections.a(int, int):java.lang.Object
      i.e.MapCollections.a(int, java.lang.Object):V
      i.e.MapCollections.a(java.lang.Object, java.lang.Object):void
      i.e.MapCollections.a(java.lang.Object[], int):T[]
      i.e.MapCollections.a(java.util.Map, java.util.Collection<?>):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.e.MapCollections.a(java.util.Map, java.util.Collection<?>):boolean
     arg types: [i.e.ArrayMap<java.lang.String, java.lang.String>, java.util.Set<java.lang.String>]
     candidates:
      i.e.MapCollections.a(java.util.Set, java.lang.Object):boolean
      i.e.MapCollections.a(int, int):java.lang.Object
      i.e.MapCollections.a(int, java.lang.Object):V
      i.e.MapCollections.a(java.lang.Object, java.lang.Object):void
      i.e.MapCollections.a(java.lang.Object[], int):T[]
      i.e.MapCollections.a(java.util.Map, java.util.Collection<?>):boolean */
    public static ArrayMap<String, View> b(b0 b0Var, ArrayMap<String, String> arrayMap, Object obj, w.a aVar) {
        ArrayList<String> arrayList;
        if (arrayMap.isEmpty() || obj == null) {
            arrayMap.clear();
            return null;
        }
        Fragment fragment = aVar.d;
        ArrayMap<String, View> arrayMap2 = new ArrayMap<>();
        b0Var.a(arrayMap2, fragment.L());
        BackStackRecord backStackRecord = aVar.f1351f;
        if (aVar.f1350e) {
            arrayList = backStackRecord.f1333p;
        } else {
            arrayList = backStackRecord.f1332o;
        }
        MapCollections.a((Map) arrayMap2, (Collection<?>) arrayList);
        MapCollections.a((Map) arrayMap, (Collection<?>) arrayMap2.keySet());
        return arrayMap2;
    }

    public static Object b(FragmentTransitionImpl0 fragmentTransitionImpl0, Fragment fragment, boolean z) {
        Object obj = null;
        if (fragment == null) {
            return null;
        }
        if (z) {
            Fragment.b bVar = fragment.L;
            if (bVar != null && (obj = bVar.h) == Fragment.W) {
                obj = fragment.q();
            }
        } else {
            obj = fragment.r();
        }
        return fragmentTransitionImpl0.b(obj);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentTransition3.a(i.l.a.b0, java.util.List<java.lang.Object>):boolean
     arg types: [i.l.a.FragmentTransitionImpl0, java.util.ArrayList]
     candidates:
      i.l.a.FragmentTransition3.a(androidx.fragment.app.Fragment, androidx.fragment.app.Fragment):i.l.a.FragmentTransitionImpl0
      i.l.a.FragmentTransition3.a(java.util.ArrayList<android.view.View>, int):void
      i.l.a.FragmentTransition3.a(i.l.a.b0, java.util.List<java.lang.Object>):boolean */
    public static FragmentTransitionImpl0 a(Fragment fragment, Fragment fragment2) {
        Object obj;
        Object obj2;
        Object obj3;
        ArrayList arrayList = new ArrayList();
        if (fragment != null) {
            Object r2 = fragment.r();
            if (r2 != null) {
                arrayList.add(r2);
            }
            Fragment.b bVar = fragment.L;
            if (bVar == null) {
                obj2 = null;
            } else {
                obj2 = bVar.h;
                if (obj2 == Fragment.W) {
                    obj2 = fragment.q();
                }
            }
            if (obj2 != null) {
                arrayList.add(obj2);
            }
            Fragment.b bVar2 = fragment.L;
            if (bVar2 == null) {
                obj3 = null;
            } else {
                obj3 = bVar2.f239l;
                if (obj3 == Fragment.W) {
                    obj3 = fragment.w();
                }
            }
            if (obj3 != null) {
                arrayList.add(obj3);
            }
        }
        if (fragment2 != null) {
            Object q2 = fragment2.q();
            if (q2 != null) {
                arrayList.add(q2);
            }
            Fragment.b bVar3 = fragment2.L;
            if (bVar3 == null) {
                obj = null;
            } else {
                obj = bVar3.f237j;
                if (obj == Fragment.W) {
                    obj = fragment2.r();
                }
            }
            if (obj != null) {
                arrayList.add(obj);
            }
            Object w = fragment2.w();
            if (w != null) {
                arrayList.add(w);
            }
        }
        if (arrayList.isEmpty()) {
            return null;
        }
        FragmentTransitionImpl0 fragmentTransitionImpl0 = b;
        if (fragmentTransitionImpl0 != null && a((b0) fragmentTransitionImpl0, (List<Object>) arrayList)) {
            return b;
        }
        FragmentTransitionImpl0 fragmentTransitionImpl02 = c;
        if (fragmentTransitionImpl02 != null && a((b0) fragmentTransitionImpl02, (List<Object>) arrayList)) {
            return c;
        }
        if (b == null && c == null) {
            return null;
        }
        throw new IllegalArgumentException("Invalid Transition types");
    }

    public static boolean a(b0 b0Var, List<Object> list) {
        int size = list.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (!b0Var.a(list.get(i2))) {
                return false;
            }
        }
        return true;
    }

    public static void a(ArrayList<View> arrayList, ArrayMap<String, View> arrayMap, Collection<String> collection) {
        for (int i2 = arrayMap.d - 1; i2 >= 0; i2--) {
            View e2 = arrayMap.e(i2);
            if (collection.contains(ViewCompat.p(e2))) {
                arrayList.add(e2);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.e.MapCollections.a(java.util.Map, java.util.Collection<?>):boolean
     arg types: [i.e.ArrayMap<java.lang.String, android.view.View>, java.util.ArrayList<java.lang.String>]
     candidates:
      i.e.MapCollections.a(java.util.Set, java.lang.Object):boolean
      i.e.MapCollections.a(int, int):java.lang.Object
      i.e.MapCollections.a(int, java.lang.Object):V
      i.e.MapCollections.a(java.lang.Object, java.lang.Object):void
      i.e.MapCollections.a(java.lang.Object[], int):T[]
      i.e.MapCollections.a(java.util.Map, java.util.Collection<?>):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.e.MapCollections.a(java.util.Map, java.util.Collection<?>):boolean
     arg types: [i.e.ArrayMap<java.lang.String, android.view.View>, java.util.Collection<java.lang.String>]
     candidates:
      i.e.MapCollections.a(java.util.Set, java.lang.Object):boolean
      i.e.MapCollections.a(int, int):java.lang.Object
      i.e.MapCollections.a(int, java.lang.Object):V
      i.e.MapCollections.a(java.lang.Object, java.lang.Object):void
      i.e.MapCollections.a(java.lang.Object[], int):T[]
      i.e.MapCollections.a(java.util.Map, java.util.Collection<?>):boolean */
    public static ArrayMap<String, View> a(b0 b0Var, ArrayMap<String, String> arrayMap, Object obj, w.a aVar) {
        ArrayList<String> arrayList;
        View view = aVar.a.H;
        if (arrayMap.isEmpty() || obj == null || view == null) {
            arrayMap.clear();
            return null;
        }
        ArrayMap<String, View> arrayMap2 = new ArrayMap<>();
        b0Var.a(arrayMap2, view);
        BackStackRecord backStackRecord = aVar.c;
        if (aVar.b) {
            arrayList = backStackRecord.f1332o;
        } else {
            arrayList = backStackRecord.f1333p;
        }
        if (arrayList != null) {
            MapCollections.a((Map) arrayMap2, (Collection<?>) arrayList);
            MapCollections.a((Map) arrayMap2, (Collection<?>) arrayMap.values());
        }
        int i2 = arrayMap.d;
        while (true) {
            i2--;
            if (i2 < 0) {
                return arrayMap2;
            }
            if (!arrayMap2.containsKey(arrayMap.e(i2))) {
                arrayMap.d(i2);
            }
        }
    }

    public static View a(ArrayMap<String, View> arrayMap, w.a aVar, Object obj, boolean z) {
        ArrayList<String> arrayList;
        String str;
        BackStackRecord backStackRecord = aVar.c;
        if (obj == null || arrayMap == null || (arrayList = backStackRecord.f1332o) == null || arrayList.isEmpty()) {
            return null;
        }
        if (z) {
            str = backStackRecord.f1332o.get(0);
        } else {
            str = backStackRecord.f1333p.get(0);
        }
        return arrayMap.get(str);
    }

    public static void a(b0 b0Var, Object obj, Object obj2, ArrayMap<String, View> arrayMap, boolean z, a aVar) {
        String str;
        ArrayList<String> arrayList = aVar.f1332o;
        if (arrayList != null && !arrayList.isEmpty()) {
            if (z) {
                str = aVar.f1333p.get(0);
            } else {
                str = aVar.f1332o.get(0);
            }
            View view = arrayMap.get(str);
            b0Var.c(obj, view);
            if (obj2 != null) {
                b0Var.c(obj2, view);
            }
        }
    }

    public static ArrayList<View> a(b0 b0Var, Object obj, Fragment fragment, ArrayList<View> arrayList, View view) {
        if (obj == null) {
            return null;
        }
        ArrayList<View> arrayList2 = new ArrayList<>();
        View view2 = fragment.H;
        if (view2 != null) {
            b0Var.a(arrayList2, view2);
        }
        if (arrayList != null) {
            arrayList2.removeAll(arrayList);
        }
        if (arrayList2.isEmpty()) {
            return arrayList2;
        }
        arrayList2.add(view);
        b0Var.a(obj, arrayList2);
        return arrayList2;
    }

    public static void a(ArrayList<View> arrayList, int i2) {
        if (arrayList != null) {
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                arrayList.get(size).setVisibility(i2);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, int, int, int, boolean):void
     arg types: [androidx.fragment.app.Fragment, int, int, int, int]
     candidates:
      i.l.a.FragmentManagerImpl.a(java.util.ArrayList<i.l.a.a>, java.util.ArrayList<java.lang.Boolean>, java.lang.String, int, int):boolean
      i.l.a.FragmentManagerImpl.a(androidx.fragment.app.Fragment, int, int, int, boolean):void */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0038, code lost:
        if (r6.f225l != false) goto L_0x008a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0076, code lost:
        r12 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0088, code lost:
        if (r6.z == false) goto L_0x008a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x008a, code lost:
        r12 = true;
     */
    /* JADX WARNING: Removed duplicated region for block: B:100:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x00dc  */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x00ed A[ADDED_TO_REGION] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(i.l.a.a r11, i.l.a.r.a r12, android.util.SparseArray<i.l.a.w.a> r13, boolean r14, boolean r15) {
        /*
            androidx.fragment.app.Fragment r6 = r12.b
            if (r6 != 0) goto L_0x0005
            return
        L_0x0005:
            int r7 = r6.x
            if (r7 != 0) goto L_0x000a
            return
        L_0x000a:
            if (r14 == 0) goto L_0x0013
            int[] r0 = i.l.a.FragmentTransition3.a
            int r12 = r12.a
            r12 = r0[r12]
            goto L_0x0015
        L_0x0013:
            int r12 = r12.a
        L_0x0015:
            r0 = 1
            r1 = 0
            if (r12 == r0) goto L_0x007d
            r2 = 3
            if (r12 == r2) goto L_0x0056
            r2 = 4
            if (r12 == r2) goto L_0x003e
            r2 = 5
            if (r12 == r2) goto L_0x002c
            r2 = 6
            if (r12 == r2) goto L_0x0056
            r2 = 7
            if (r12 == r2) goto L_0x007d
            r12 = 0
            r2 = 0
            goto L_0x008e
        L_0x002c:
            if (r15 == 0) goto L_0x003b
            boolean r12 = r6.N
            if (r12 == 0) goto L_0x008c
            boolean r12 = r6.z
            if (r12 != 0) goto L_0x008c
            boolean r12 = r6.f225l
            if (r12 == 0) goto L_0x008c
            goto L_0x008a
        L_0x003b:
            boolean r12 = r6.z
            goto L_0x008d
        L_0x003e:
            if (r15 == 0) goto L_0x004d
            boolean r12 = r6.N
            if (r12 == 0) goto L_0x0078
            boolean r12 = r6.f225l
            if (r12 == 0) goto L_0x0078
            boolean r12 = r6.z
            if (r12 == 0) goto L_0x0078
            goto L_0x0076
        L_0x004d:
            boolean r12 = r6.f225l
            if (r12 == 0) goto L_0x0078
            boolean r12 = r6.z
            if (r12 != 0) goto L_0x0078
            goto L_0x0076
        L_0x0056:
            if (r15 == 0) goto L_0x006e
            boolean r12 = r6.f225l
            if (r12 != 0) goto L_0x0078
            android.view.View r12 = r6.H
            if (r12 == 0) goto L_0x0078
            int r12 = r12.getVisibility()
            if (r12 != 0) goto L_0x0078
            float r12 = r6.O
            r2 = 0
            int r12 = (r12 > r2 ? 1 : (r12 == r2 ? 0 : -1))
            if (r12 < 0) goto L_0x0078
            goto L_0x0076
        L_0x006e:
            boolean r12 = r6.f225l
            if (r12 == 0) goto L_0x0078
            boolean r12 = r6.z
            if (r12 != 0) goto L_0x0078
        L_0x0076:
            r12 = 1
            goto L_0x0079
        L_0x0078:
            r12 = 0
        L_0x0079:
            r8 = r12
            r12 = 1
            r2 = 0
            goto L_0x0091
        L_0x007d:
            if (r15 == 0) goto L_0x0082
            boolean r12 = r6.M
            goto L_0x008d
        L_0x0082:
            boolean r12 = r6.f225l
            if (r12 != 0) goto L_0x008c
            boolean r12 = r6.z
            if (r12 != 0) goto L_0x008c
        L_0x008a:
            r12 = 1
            goto L_0x008d
        L_0x008c:
            r12 = 0
        L_0x008d:
            r2 = 1
        L_0x008e:
            r1 = r12
            r12 = 0
            r8 = 0
        L_0x0091:
            java.lang.Object r3 = r13.get(r7)
            i.l.a.FragmentTransition3$a r3 = (i.l.a.FragmentTransition3.a) r3
            if (r1 == 0) goto L_0x00aa
            if (r3 != 0) goto L_0x00a4
            i.l.a.FragmentTransition3$a r1 = new i.l.a.FragmentTransition3$a
            r1.<init>()
            r13.put(r7, r1)
            r3 = r1
        L_0x00a4:
            r3.a = r6
            r3.b = r14
            r3.c = r11
        L_0x00aa:
            r9 = r3
            r10 = 0
            if (r15 != 0) goto L_0x00d2
            if (r2 == 0) goto L_0x00d2
            if (r9 == 0) goto L_0x00b8
            androidx.fragment.app.Fragment r1 = r9.d
            if (r1 != r6) goto L_0x00b8
            r9.d = r10
        L_0x00b8:
            i.l.a.FragmentManagerImpl r1 = r11.f1275s
            int r2 = r6.b
            if (r2 >= r0) goto L_0x00d2
            int r2 = r1.f1304p
            if (r2 < r0) goto L_0x00d2
            boolean r0 = r11.f1334q
            if (r0 != 0) goto L_0x00d2
            r1.f(r6)
            r2 = 1
            r3 = 0
            r4 = 0
            r5 = 0
            r0 = r1
            r1 = r6
            r0.a(r1, r2, r3, r4, r5)
        L_0x00d2:
            if (r8 == 0) goto L_0x00eb
            if (r9 == 0) goto L_0x00da
            androidx.fragment.app.Fragment r0 = r9.d
            if (r0 != 0) goto L_0x00eb
        L_0x00da:
            if (r9 != 0) goto L_0x00e5
            i.l.a.FragmentTransition3$a r0 = new i.l.a.FragmentTransition3$a
            r0.<init>()
            r13.put(r7, r0)
            r9 = r0
        L_0x00e5:
            r9.d = r6
            r9.f1350e = r14
            r9.f1351f = r11
        L_0x00eb:
            if (r15 != 0) goto L_0x00f7
            if (r12 == 0) goto L_0x00f7
            if (r9 == 0) goto L_0x00f7
            androidx.fragment.app.Fragment r11 = r9.a
            if (r11 != r6) goto L_0x00f7
            r9.a = r10
        L_0x00f7:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: i.l.a.FragmentTransition3.a(i.l.a.BackStackRecord, i.l.a.FragmentTransaction$a, android.util.SparseArray, boolean, boolean):void");
    }

    public static Object a(FragmentTransitionImpl0 fragmentTransitionImpl0, Fragment fragment, boolean z) {
        Object obj = null;
        if (fragment == null) {
            return null;
        }
        if (z) {
            Fragment.b bVar = fragment.L;
            if (bVar != null && (obj = bVar.f237j) == Fragment.W) {
                obj = fragment.r();
            }
        } else {
            obj = fragment.q();
        }
        return fragmentTransitionImpl0.b(obj);
    }

    public static Object a(FragmentTransitionImpl0 fragmentTransitionImpl0, Fragment fragment, Fragment fragment2, boolean z) {
        Object obj = null;
        if (fragment == null || fragment2 == null) {
            return null;
        }
        if (z) {
            Fragment.b bVar = fragment2.L;
            if (bVar != null) {
                Object obj2 = bVar.f239l;
                if (obj2 == Fragment.W) {
                    obj2 = fragment2.w();
                }
                obj = obj2;
            }
        } else {
            obj = fragment.w();
        }
        return fragmentTransitionImpl0.c(fragmentTransitionImpl0.b(obj));
    }

    public static Object a(FragmentTransitionImpl0 fragmentTransitionImpl0, Object obj, Object obj2, Object obj3, Fragment fragment, boolean z) {
        Boolean bool;
        Boolean bool2;
        boolean z2 = true;
        if (!(obj == null || obj2 == null || fragment == null)) {
            if (z) {
                Fragment.b bVar = fragment.L;
                if (!(bVar == null || (bool2 = bVar.f240m) == null)) {
                    z2 = bool2.booleanValue();
                }
            } else {
                Fragment.b bVar2 = fragment.L;
                if (!(bVar2 == null || (bool = bVar2.f241n) == null)) {
                    z2 = bool.booleanValue();
                }
            }
        }
        if (z2) {
            return fragmentTransitionImpl0.b(obj2, obj, obj3);
        }
        return fragmentTransitionImpl0.a(obj2, obj, obj3);
    }

    public static void a(Fragment fragment, Fragment fragment2, boolean z, ArrayMap<String, View> arrayMap, boolean z2) {
        if (z) {
            Fragment.b bVar = fragment2.L;
        } else {
            Fragment.b bVar2 = fragment.L;
        }
    }
}
