package i.l.a;

import android.util.AndroidRuntimeException;

public final class SuperNotCalledException extends AndroidRuntimeException {
    public SuperNotCalledException(String str) {
        super(str);
    }
}
