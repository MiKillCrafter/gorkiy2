package e.a.a.g.a;

import e.a.a.g.a.g.IApiConfig;
import e.a.a.g.a.g.IApiConfig0;
import e.c.b.j.IApiConfigRepository;
import java.util.HashSet;
import java.util.Map;
import n.n.c.Intrinsics;
import o.HttpUrl;

/* compiled from: IAuthenticateRequestChecker.kt */
public final class IAuthenticateRequestChecker0 implements IAuthenticateRequestChecker {
    public HashSet<String> a;
    public HashSet<String> b;
    public HashSet<String> c;

    public IAuthenticateRequestChecker0(IApiConfig iApiConfig) {
        if (iApiConfig != null) {
            this.a = new HashSet<>();
            this.b = new HashSet<>();
            this.c = new HashSet<>();
            for (Map.Entry next : iApiConfig.a().entrySet()) {
                IApiConfigRepository iApiConfigRepository = (IApiConfigRepository) next.getKey();
                if (iApiConfigRepository instanceof IApiConfig0) {
                    IApiConfig0 iApiConfig0 = (IApiConfig0) iApiConfigRepository;
                    if (iApiConfig0.a()) {
                        this.a.add(next.getValue());
                    }
                    if (iApiConfig0.b()) {
                        this.b.add(next.getValue());
                    }
                    if (iApiConfig0.c()) {
                        this.c.add(next.getValue());
                    }
                }
            }
            return;
        }
        Intrinsics.a("apiConfig");
        throw null;
    }

    public boolean a(HttpUrl httpUrl) {
        if (httpUrl != null) {
            HashSet<String> hashSet = this.c;
            return hashSet.contains(httpUrl.b + "://" + httpUrl.f2851e + '/');
        }
        Intrinsics.a("url");
        throw null;
    }

    public boolean b(HttpUrl httpUrl) {
        if (httpUrl != null) {
            HashSet<String> hashSet = this.a;
            return hashSet.contains(httpUrl.b + "://" + httpUrl.f2851e + '/');
        }
        Intrinsics.a("url");
        throw null;
    }

    public boolean c(HttpUrl httpUrl) {
        if (httpUrl != null) {
            HashSet<String> hashSet = this.b;
            return hashSet.contains(httpUrl.b + "://" + httpUrl.f2851e + '/');
        }
        Intrinsics.a("url");
        throw null;
    }
}
