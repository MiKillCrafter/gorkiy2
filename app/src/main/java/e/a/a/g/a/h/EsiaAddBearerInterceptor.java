package e.a.a.g.a.h;

import e.a.a.g.a.IAuthenticateRequestChecker;
import e.a.a.g.a.Session;
import j.a.a.a.outline;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import n.n.c.Intrinsics;
import n.r.Indent;
import o.Headers;
import o.HttpUrl;
import o.Interceptor;
import o.Request;
import o.RequestBody;
import o.Response;
import o.m0.Util;
import t.a.Timber;

/* compiled from: EsiaAddBearerInterceptor.kt */
public final class EsiaAddBearerInterceptor implements Interceptor {
    public final Session b;
    public final IAuthenticateRequestChecker c;

    public EsiaAddBearerInterceptor(Session session, IAuthenticateRequestChecker iAuthenticateRequestChecker) {
        if (session == null) {
            Intrinsics.a("session");
            throw null;
        } else if (iAuthenticateRequestChecker != null) {
            this.b = session;
            this.c = iAuthenticateRequestChecker;
        } else {
            Intrinsics.a("authenticateRequestChecker");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Response a(Interceptor.a aVar) {
        LinkedHashMap linkedHashMap;
        if (aVar == null) {
            Intrinsics.a("chain");
            throw null;
        } else if (!this.c.b(aVar.f().b)) {
            return aVar.a(aVar.f());
        } else {
            Request f2 = aVar.f();
            if (f2 == null) {
                throw null;
            } else if (f2 != null) {
                new LinkedHashMap();
                HttpUrl httpUrl = f2.b;
                String str = f2.c;
                RequestBody requestBody = f2.f2896e;
                if (f2.f2897f.isEmpty()) {
                    linkedHashMap = new LinkedHashMap();
                } else {
                    Map<Class<?>, Object> map = f2.f2897f;
                    if (map != null) {
                        linkedHashMap = new LinkedHashMap(map);
                    } else {
                        Intrinsics.a("$this$toMutableMap");
                        throw null;
                    }
                }
                Headers.a c2 = f2.d.c();
                if (!Indent.b(this.b.a)) {
                    StringBuilder a = outline.a("Bearer ");
                    a.append(this.b.a);
                    String sb = a.toString();
                    if (sb != null) {
                        c2.a("Authorization", sb);
                        String format = String.format("Adding Header: Authorization %1$s", Arrays.copyOf(new Object[]{sb}, 1));
                        Intrinsics.a((Object) format, "java.lang.String.format(format, *args)");
                        Timber.d.b(format, new Object[0]);
                    } else {
                        Intrinsics.a("value");
                        throw null;
                    }
                }
                if (httpUrl != null) {
                    return aVar.a(new Request(httpUrl, str, c2.a(), requestBody, Util.a(linkedHashMap)));
                }
                throw new IllegalStateException("url == null".toString());
            } else {
                Intrinsics.a("request");
                throw null;
            }
        }
    }
}
