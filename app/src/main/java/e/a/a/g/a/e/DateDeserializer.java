package e.a.a.g.a.e;

import com.google.gson.JsonParseException;
import j.a.a.a.outline;
import j.c.d.JsonDeserializationContext;
import j.c.d.JsonDeserializer;
import j.c.d.JsonElement;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import n.i.Collections;
import n.n.c.Intrinsics;
import t.a.Timber;

/* compiled from: DateDeserializer.kt */
public final class DateDeserializer implements JsonDeserializer<Date> {
    public final List<String> a = Collections.a((Object[]) new String[]{"dd.MM.yyyy HH:mm:ss", "dd.MM.yyyy"});

    public Object a(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) {
        if (jsonElement == null) {
            Intrinsics.a("element");
            throw null;
        } else if (type == null) {
            Intrinsics.a("arg1");
            throw null;
        } else if (jsonDeserializationContext != null) {
            for (String simpleDateFormat : this.a) {
                try {
                    return new SimpleDateFormat(simpleDateFormat, Locale.getDefault()).parse(jsonElement.d());
                } catch (ParseException unused) {
                }
            }
            StringBuilder a2 = outline.a("Unable to parse date ");
            a2.append(jsonElement.d());
            Timber.d.a(a2.toString(), new Object[0]);
            StringBuilder a3 = outline.a("Unable to parse date ");
            a3.append(jsonElement.d());
            throw new JsonParseException(a3.toString());
        } else {
            Intrinsics.a("arg2");
            throw null;
        }
    }
}
