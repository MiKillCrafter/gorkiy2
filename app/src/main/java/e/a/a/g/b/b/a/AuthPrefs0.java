package e.a.a.g.b.b.a;

import android.content.Context;
import android.content.SharedPreferences;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import e.a.a.g.a.Session;
import e.a.a.g.b.b.a.AuthPrefs;
import e.c.a.c.a.a.IPrefsStorage;
import n.Lazy;
import n.Unit;
import n.g;
import n.i.Collections;
import n.n.b.Functions;
import n.n.c.Intrinsics;
import n.n.c.PropertyReference1Impl;
import n.n.c.Reflection;
import n.n.c.j;
import n.p.KProperty;
import t.a.Timber;

/* compiled from: AuthPrefs.kt */
public final class AuthPrefs0 implements IAuthPrefs {

    /* renamed from: e  reason: collision with root package name */
    public static final /* synthetic */ KProperty[] f623e;
    public final Lazy a;
    public final Context b;
    public final IPrefsStorage c;
    public final Session d;

    /* compiled from: AuthPrefs.kt */
    public static final class a extends j implements Functions<g> {
        public final /* synthetic */ AuthPrefs0 c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(AuthPrefs0 authPrefs0) {
            super(0);
            this.c = authPrefs0;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [com.google.android.gms.ads.identifier.AdvertisingIdClient$Info, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        public Object b() {
            AuthPrefs0 authPrefs0 = this.c;
            AdvertisingIdClient.Info advertisingIdInfo = AdvertisingIdClient.getAdvertisingIdInfo(authPrefs0.b);
            Intrinsics.a((Object) advertisingIdInfo, "AdvertisingIdClient.getAdvertisingIdInfo(context)");
            String id = advertisingIdInfo.getId();
            Intrinsics.a((Object) id, "AdvertisingIdClient.getA…rtisingIdInfo(context).id");
            Collections.a(authPrefs0.g(), "PincodeAddition_key", id);
            return Unit.a;
        }
    }

    /* compiled from: AuthPrefs.kt */
    public static final class b extends j implements Functions<SharedPreferences> {
        public final /* synthetic */ AuthPrefs0 c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(AuthPrefs0 authPrefs0) {
            super(0);
            this.c = authPrefs0;
        }

        public Object b() {
            return this.c.c.a("AUTH_PREFS");
        }
    }

    /* JADX WARN: Type inference failed for: r2v1, types: [n.p.KClass, n.p.KDeclarationContainer] */
    static {
        PropertyReference1Impl propertyReference1Impl = new PropertyReference1Impl(Reflection.a(AuthPrefs0.class), "prefs", "getPrefs()Landroid/content/SharedPreferences;");
        Reflection.a(propertyReference1Impl);
        f623e = new KProperty[]{propertyReference1Impl};
    }

    /* JADX WARN: Type inference failed for: r13v15, types: [java.lang.Boolean] */
    /* JADX WARN: Type inference failed for: r13v17, types: [java.lang.Long] */
    /* JADX WARN: Type inference failed for: r13v19, types: [java.lang.Float] */
    /* JADX WARN: Type inference failed for: r13v21, types: [java.lang.Integer] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AuthPrefs0(android.content.Context r13, e.c.a.c.a.a.IPrefsStorage r14, e.a.a.g.a.Session r15) {
        /*
            r12 = this;
            java.lang.Class<java.lang.String> r0 = java.lang.String.class
            r1 = 0
            if (r13 == 0) goto L_0x024b
            if (r14 == 0) goto L_0x0245
            if (r15 == 0) goto L_0x023f
            r12.<init>()
            r12.b = r13
            r12.c = r14
            r12.d = r15
            e.a.a.g.b.b.a.AuthPrefs0$b r13 = new e.a.a.g.b.b.a.AuthPrefs0$b
            r13.<init>(r12)
            n.Lazy r13 = j.c.a.a.c.n.c.a(r13)
            r12.a = r13
            android.content.SharedPreferences r13 = r12.g()
            java.lang.String r14 = "PincodeAddition_key"
            boolean r15 = r13.contains(r14)
            r2 = -1
            java.lang.String r4 = "Not acceptable type"
            r5 = -1082130432(0xffffffffbf800000, float:-1.0)
            r6 = -1
            java.lang.String r7 = ""
            r8 = 0
            java.lang.String r9 = "null cannot be cast to non-null type kotlin.String"
            if (r15 != 0) goto L_0x0038
            r13 = r1
            goto L_0x011c
        L_0x0038:
            n.p.KClass r15 = n.n.c.Reflection.a(r0)
            n.p.KClass r10 = n.n.c.Reflection.a(r0)
            boolean r10 = n.n.c.Intrinsics.a(r15, r10)
            if (r10 == 0) goto L_0x0054
            java.lang.String r13 = r13.getString(r14, r7)
            if (r13 == 0) goto L_0x004e
            goto L_0x011c
        L_0x004e:
            kotlin.TypeCastException r13 = new kotlin.TypeCastException
            r13.<init>(r9)
            throw r13
        L_0x0054:
            java.lang.Class r10 = java.lang.Integer.TYPE
            n.p.KClass r10 = n.n.c.Reflection.a(r10)
            boolean r10 = n.n.c.Intrinsics.a(r15, r10)
            if (r10 == 0) goto L_0x007d
            boolean r15 = r7 instanceof java.lang.Integer
            if (r15 != 0) goto L_0x0066
            r15 = r1
            goto L_0x0067
        L_0x0066:
            r15 = r7
        L_0x0067:
            java.lang.Integer r15 = (java.lang.Integer) r15
            if (r15 == 0) goto L_0x0070
            int r15 = r15.intValue()
            goto L_0x0071
        L_0x0070:
            r15 = -1
        L_0x0071:
            int r13 = r13.getInt(r14, r15)
            java.lang.Integer r13 = java.lang.Integer.valueOf(r13)
            java.lang.String r13 = (java.lang.String) r13
            goto L_0x011c
        L_0x007d:
            java.lang.Class r10 = java.lang.Float.TYPE
            n.p.KClass r10 = n.n.c.Reflection.a(r10)
            boolean r10 = n.n.c.Intrinsics.a(r15, r10)
            if (r10 == 0) goto L_0x00a7
            boolean r15 = r7 instanceof java.lang.Float
            if (r15 != 0) goto L_0x008f
            r15 = r1
            goto L_0x0090
        L_0x008f:
            r15 = r7
        L_0x0090:
            java.lang.Float r15 = (java.lang.Float) r15
            if (r15 == 0) goto L_0x0099
            float r15 = r15.floatValue()
            goto L_0x009b
        L_0x0099:
            r15 = -1082130432(0xffffffffbf800000, float:-1.0)
        L_0x009b:
            float r13 = r13.getFloat(r14, r15)
            java.lang.Float r13 = java.lang.Float.valueOf(r13)
            java.lang.String r13 = (java.lang.String) r13
            goto L_0x011c
        L_0x00a7:
            java.lang.Class r10 = java.lang.Long.TYPE
            n.p.KClass r10 = n.n.c.Reflection.a(r10)
            boolean r10 = n.n.c.Intrinsics.a(r15, r10)
            if (r10 == 0) goto L_0x00cf
            boolean r15 = r7 instanceof java.lang.Long
            if (r15 != 0) goto L_0x00b9
            r15 = r1
            goto L_0x00ba
        L_0x00b9:
            r15 = r7
        L_0x00ba:
            java.lang.Long r15 = (java.lang.Long) r15
            if (r15 == 0) goto L_0x00c3
            long r10 = r15.longValue()
            goto L_0x00c4
        L_0x00c3:
            r10 = r2
        L_0x00c4:
            long r13 = r13.getLong(r14, r10)
            java.lang.Long r13 = java.lang.Long.valueOf(r13)
            java.lang.String r13 = (java.lang.String) r13
            goto L_0x011c
        L_0x00cf:
            java.lang.Class r10 = java.lang.Boolean.TYPE
            n.p.KClass r10 = n.n.c.Reflection.a(r10)
            boolean r10 = n.n.c.Intrinsics.a(r15, r10)
            if (r10 == 0) goto L_0x00f7
            boolean r15 = r7 instanceof java.lang.Boolean
            if (r15 != 0) goto L_0x00e1
            r15 = r1
            goto L_0x00e2
        L_0x00e1:
            r15 = r7
        L_0x00e2:
            java.lang.Boolean r15 = (java.lang.Boolean) r15
            if (r15 == 0) goto L_0x00eb
            boolean r15 = r15.booleanValue()
            goto L_0x00ec
        L_0x00eb:
            r15 = 0
        L_0x00ec:
            boolean r13 = r13.getBoolean(r14, r15)
            java.lang.Boolean r13 = java.lang.Boolean.valueOf(r13)
            java.lang.String r13 = (java.lang.String) r13
            goto L_0x011c
        L_0x00f7:
            java.lang.Class<java.util.Set> r10 = java.util.Set.class
            n.p.KClass r10 = n.n.c.Reflection.a(r10)
            boolean r15 = n.n.c.Intrinsics.a(r15, r10)
            if (r15 == 0) goto L_0x0239
            boolean r15 = r7 instanceof java.util.Set
            if (r15 != 0) goto L_0x0109
            r15 = r1
            goto L_0x010a
        L_0x0109:
            r15 = r7
        L_0x010a:
            java.util.Set r15 = (java.util.Set) r15
            if (r15 == 0) goto L_0x010f
            goto L_0x0114
        L_0x010f:
            java.util.LinkedHashSet r15 = new java.util.LinkedHashSet
            r15.<init>()
        L_0x0114:
            java.util.Set r13 = r13.getStringSet(r14, r15)
            if (r13 == 0) goto L_0x0233
            java.lang.String r13 = (java.lang.String) r13
        L_0x011c:
            if (r13 == 0) goto L_0x0127
            boolean r13 = n.r.Indent.b(r13)
            if (r13 == 0) goto L_0x0125
            goto L_0x0127
        L_0x0125:
            r13 = 0
            goto L_0x0128
        L_0x0127:
            r13 = 1
        L_0x0128:
            if (r13 == 0) goto L_0x0137
            e.a.a.g.b.b.a.AuthPrefs0$a r13 = new e.a.a.g.b.b.a.AuthPrefs0$a
            r13.<init>(r12)
            n.k.Thread r14 = new n.k.Thread
            r14.<init>(r13)
            r14.start()
        L_0x0137:
            android.content.SharedPreferences r13 = r12.g()
            java.lang.String r14 = "ACCESS_TOKEN"
            boolean r15 = r13.contains(r14)
            if (r15 != 0) goto L_0x0145
            goto L_0x0220
        L_0x0145:
            n.p.KClass r15 = n.n.c.Reflection.a(r0)
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r15, r0)
            if (r0 == 0) goto L_0x0161
            java.lang.String r1 = r13.getString(r14, r7)
            if (r1 == 0) goto L_0x015b
            goto L_0x0220
        L_0x015b:
            kotlin.TypeCastException r13 = new kotlin.TypeCastException
            r13.<init>(r9)
            throw r13
        L_0x0161:
            java.lang.Class r0 = java.lang.Integer.TYPE
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r15, r0)
            if (r0 == 0) goto L_0x0188
            boolean r15 = r7 instanceof java.lang.Integer
            if (r15 != 0) goto L_0x0172
            goto L_0x0173
        L_0x0172:
            r1 = r7
        L_0x0173:
            java.lang.Integer r1 = (java.lang.Integer) r1
            if (r1 == 0) goto L_0x017b
            int r6 = r1.intValue()
        L_0x017b:
            int r13 = r13.getInt(r14, r6)
            java.lang.Integer r13 = java.lang.Integer.valueOf(r13)
            r1 = r13
            java.lang.String r1 = (java.lang.String) r1
            goto L_0x0220
        L_0x0188:
            java.lang.Class r0 = java.lang.Float.TYPE
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r15, r0)
            if (r0 == 0) goto L_0x01af
            boolean r15 = r7 instanceof java.lang.Float
            if (r15 != 0) goto L_0x0199
            goto L_0x019a
        L_0x0199:
            r1 = r7
        L_0x019a:
            java.lang.Float r1 = (java.lang.Float) r1
            if (r1 == 0) goto L_0x01a2
            float r5 = r1.floatValue()
        L_0x01a2:
            float r13 = r13.getFloat(r14, r5)
            java.lang.Float r13 = java.lang.Float.valueOf(r13)
            r1 = r13
            java.lang.String r1 = (java.lang.String) r1
            goto L_0x0220
        L_0x01af:
            java.lang.Class r0 = java.lang.Long.TYPE
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r15, r0)
            if (r0 == 0) goto L_0x01d5
            boolean r15 = r7 instanceof java.lang.Long
            if (r15 != 0) goto L_0x01c0
            goto L_0x01c1
        L_0x01c0:
            r1 = r7
        L_0x01c1:
            java.lang.Long r1 = (java.lang.Long) r1
            if (r1 == 0) goto L_0x01c9
            long r2 = r1.longValue()
        L_0x01c9:
            long r13 = r13.getLong(r14, r2)
            java.lang.Long r13 = java.lang.Long.valueOf(r13)
            r1 = r13
            java.lang.String r1 = (java.lang.String) r1
            goto L_0x0220
        L_0x01d5:
            java.lang.Class r0 = java.lang.Boolean.TYPE
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r15, r0)
            if (r0 == 0) goto L_0x01fb
            boolean r15 = r7 instanceof java.lang.Boolean
            if (r15 != 0) goto L_0x01e6
            goto L_0x01e7
        L_0x01e6:
            r1 = r7
        L_0x01e7:
            java.lang.Boolean r1 = (java.lang.Boolean) r1
            if (r1 == 0) goto L_0x01ef
            boolean r8 = r1.booleanValue()
        L_0x01ef:
            boolean r13 = r13.getBoolean(r14, r8)
            java.lang.Boolean r13 = java.lang.Boolean.valueOf(r13)
            r1 = r13
            java.lang.String r1 = (java.lang.String) r1
            goto L_0x0220
        L_0x01fb:
            java.lang.Class<java.util.Set> r0 = java.util.Set.class
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r15 = n.n.c.Intrinsics.a(r15, r0)
            if (r15 == 0) goto L_0x022d
            boolean r15 = r7 instanceof java.util.Set
            if (r15 != 0) goto L_0x020c
            goto L_0x020d
        L_0x020c:
            r1 = r7
        L_0x020d:
            java.util.Set r1 = (java.util.Set) r1
            if (r1 == 0) goto L_0x0212
            goto L_0x0217
        L_0x0212:
            java.util.LinkedHashSet r1 = new java.util.LinkedHashSet
            r1.<init>()
        L_0x0217:
            java.util.Set r13 = r13.getStringSet(r14, r1)
            if (r13 == 0) goto L_0x0227
            r1 = r13
            java.lang.String r1 = (java.lang.String) r1
        L_0x0220:
            if (r1 == 0) goto L_0x0226
            boolean r13 = n.r.Indent.b(r1)
        L_0x0226:
            return
        L_0x0227:
            kotlin.TypeCastException r13 = new kotlin.TypeCastException
            r13.<init>(r9)
            throw r13
        L_0x022d:
            java.lang.UnsupportedOperationException r13 = new java.lang.UnsupportedOperationException
            r13.<init>(r4)
            throw r13
        L_0x0233:
            kotlin.TypeCastException r13 = new kotlin.TypeCastException
            r13.<init>(r9)
            throw r13
        L_0x0239:
            java.lang.UnsupportedOperationException r13 = new java.lang.UnsupportedOperationException
            r13.<init>(r4)
            throw r13
        L_0x023f:
            java.lang.String r13 = "session"
            n.n.c.Intrinsics.a(r13)
            throw r1
        L_0x0245:
            java.lang.String r13 = "prefsStorage"
            n.n.c.Intrinsics.a(r13)
            throw r1
        L_0x024b:
            java.lang.String r13 = "context"
            n.n.c.Intrinsics.a(r13)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: e.a.a.g.b.b.a.AuthPrefs0.<init>(android.content.Context, e.c.a.c.a.a.IPrefsStorage, e.a.a.g.a.Session):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(android.content.SharedPreferences, java.lang.String, java.lang.Object):void
     arg types: [android.content.SharedPreferences, java.lang.String, boolean]
     candidates:
      n.i.Collections.a(java.lang.String, java.lang.String, int):java.lang.Integer
      n.i.Collections.a(int, java.lang.String, int):java.lang.String
      n.i.Collections.a(java.lang.String, int, int):java.net.InetAddress
      n.i.Collections.a(java.io.File, boolean, int):p.Sink
      n.i.Collections.a(int, java.lang.String, java.lang.Throwable):void
      n.i.Collections.a(int, byte[], int):void
      n.i.Collections.a(long, long, long):void
      n.i.Collections.a(java.lang.Appendable, java.lang.Object, n.n.b.Functions0):void
      n.i.Collections.a(char, char, boolean):boolean
      n.i.Collections.a(android.content.SharedPreferences, java.lang.String, java.lang.Object):void */
    public void a(String str) {
        if (str != null) {
            this.d.a = str;
            try {
                Collections.a(g(), "ACCESS_TOKEN", AuthPrefs.b(str, "0000", f()));
                Collections.a(g(), "DefaultPincode_Key", (Object) true);
            } catch (Throwable th) {
                Timber.d.a(th);
                throw new AuthPrefs.a("Access token encryption error");
            }
        } else {
            Intrinsics.a("acct");
            throw null;
        }
    }

    /* JADX WARN: Type inference failed for: r0v25, types: [java.lang.Boolean] */
    /* JADX WARN: Type inference failed for: r0v30, types: [java.lang.Long] */
    /* JADX WARN: Type inference failed for: r0v34, types: [java.lang.Float] */
    /* JADX WARN: Type inference failed for: r0v40, types: [java.lang.Integer] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String b() {
        /*
            r7 = this;
            java.lang.Class<java.lang.String> r0 = java.lang.String.class
            android.content.SharedPreferences r1 = r7.g()
            java.lang.String r2 = "EsiaOid_key"
            boolean r3 = r1.contains(r2)
            java.lang.String r4 = ""
            r5 = 0
            if (r3 != 0) goto L_0x0013
            goto L_0x00fa
        L_0x0013:
            n.p.KClass r3 = n.n.c.Reflection.a(r0)
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r3, r0)
            java.lang.String r6 = "null cannot be cast to non-null type kotlin.String"
            if (r0 == 0) goto L_0x0031
            java.lang.String r5 = r1.getString(r2, r4)
            if (r5 == 0) goto L_0x002b
            goto L_0x00fa
        L_0x002b:
            kotlin.TypeCastException r0 = new kotlin.TypeCastException
            r0.<init>(r6)
            throw r0
        L_0x0031:
            java.lang.Class r0 = java.lang.Integer.TYPE
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r3, r0)
            if (r0 == 0) goto L_0x005a
            boolean r0 = r4 instanceof java.lang.Integer
            if (r0 != 0) goto L_0x0042
            goto L_0x0043
        L_0x0042:
            r5 = r4
        L_0x0043:
            java.lang.Integer r5 = (java.lang.Integer) r5
            if (r5 == 0) goto L_0x004c
            int r0 = r5.intValue()
            goto L_0x004d
        L_0x004c:
            r0 = -1
        L_0x004d:
            int r0 = r1.getInt(r2, r0)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r5 = r0
            java.lang.String r5 = (java.lang.String) r5
            goto L_0x00fa
        L_0x005a:
            java.lang.Class r0 = java.lang.Float.TYPE
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r3, r0)
            if (r0 == 0) goto L_0x0084
            boolean r0 = r4 instanceof java.lang.Float
            if (r0 != 0) goto L_0x006b
            goto L_0x006c
        L_0x006b:
            r5 = r4
        L_0x006c:
            java.lang.Float r5 = (java.lang.Float) r5
            if (r5 == 0) goto L_0x0075
            float r0 = r5.floatValue()
            goto L_0x0077
        L_0x0075:
            r0 = -1082130432(0xffffffffbf800000, float:-1.0)
        L_0x0077:
            float r0 = r1.getFloat(r2, r0)
            java.lang.Float r0 = java.lang.Float.valueOf(r0)
            r5 = r0
            java.lang.String r5 = (java.lang.String) r5
            goto L_0x00fa
        L_0x0084:
            java.lang.Class r0 = java.lang.Long.TYPE
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r3, r0)
            if (r0 == 0) goto L_0x00ad
            boolean r0 = r4 instanceof java.lang.Long
            if (r0 != 0) goto L_0x0095
            goto L_0x0096
        L_0x0095:
            r5 = r4
        L_0x0096:
            java.lang.Long r5 = (java.lang.Long) r5
            if (r5 == 0) goto L_0x009f
            long r5 = r5.longValue()
            goto L_0x00a1
        L_0x009f:
            r5 = -1
        L_0x00a1:
            long r0 = r1.getLong(r2, r5)
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            r5 = r0
            java.lang.String r5 = (java.lang.String) r5
            goto L_0x00fa
        L_0x00ad:
            java.lang.Class r0 = java.lang.Boolean.TYPE
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r3, r0)
            if (r0 == 0) goto L_0x00d5
            boolean r0 = r4 instanceof java.lang.Boolean
            if (r0 != 0) goto L_0x00be
            goto L_0x00bf
        L_0x00be:
            r5 = r4
        L_0x00bf:
            java.lang.Boolean r5 = (java.lang.Boolean) r5
            if (r5 == 0) goto L_0x00c8
            boolean r0 = r5.booleanValue()
            goto L_0x00c9
        L_0x00c8:
            r0 = 0
        L_0x00c9:
            boolean r0 = r1.getBoolean(r2, r0)
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r5 = r0
            java.lang.String r5 = (java.lang.String) r5
            goto L_0x00fa
        L_0x00d5:
            java.lang.Class<java.util.Set> r0 = java.util.Set.class
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r3, r0)
            if (r0 == 0) goto L_0x0104
            boolean r0 = r4 instanceof java.util.Set
            if (r0 != 0) goto L_0x00e6
            goto L_0x00e7
        L_0x00e6:
            r5 = r4
        L_0x00e7:
            java.util.Set r5 = (java.util.Set) r5
            if (r5 == 0) goto L_0x00ec
            goto L_0x00f1
        L_0x00ec:
            java.util.LinkedHashSet r5 = new java.util.LinkedHashSet
            r5.<init>()
        L_0x00f1:
            java.util.Set r0 = r1.getStringSet(r2, r5)
            if (r0 == 0) goto L_0x00fe
            r5 = r0
            java.lang.String r5 = (java.lang.String) r5
        L_0x00fa:
            if (r5 == 0) goto L_0x00fd
            r4 = r5
        L_0x00fd:
            return r4
        L_0x00fe:
            kotlin.TypeCastException r0 = new kotlin.TypeCastException
            r0.<init>(r6)
            throw r0
        L_0x0104:
            java.lang.UnsupportedOperationException r0 = new java.lang.UnsupportedOperationException
            java.lang.String r1 = "Not acceptable type"
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: e.a.a.g.b.b.a.AuthPrefs0.b():java.lang.String");
    }

    /* JADX WARN: Type inference failed for: r0v31, types: [java.lang.Boolean] */
    /* JADX WARN: Type inference failed for: r0v36, types: [java.lang.Long] */
    /* JADX WARN: Type inference failed for: r0v40, types: [java.lang.Float] */
    /* JADX WARN: Type inference failed for: r0v46, types: [java.lang.Integer] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [byte[], java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String c() {
        /*
            r8 = this;
            java.lang.Class<java.lang.String> r0 = java.lang.String.class
            android.content.SharedPreferences r1 = r8.g()
            java.lang.String r2 = "EntToken_key"
            boolean r3 = r1.contains(r2)
            r4 = 0
            java.lang.String r5 = ""
            r6 = 0
            if (r3 != 0) goto L_0x0014
            goto L_0x00fb
        L_0x0014:
            n.p.KClass r3 = n.n.c.Reflection.a(r0)
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r3, r0)
            java.lang.String r7 = "null cannot be cast to non-null type kotlin.String"
            if (r0 == 0) goto L_0x0032
            java.lang.String r6 = r1.getString(r2, r5)
            if (r6 == 0) goto L_0x002c
            goto L_0x00fb
        L_0x002c:
            kotlin.TypeCastException r0 = new kotlin.TypeCastException
            r0.<init>(r7)
            throw r0
        L_0x0032:
            java.lang.Class r0 = java.lang.Integer.TYPE
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r3, r0)
            if (r0 == 0) goto L_0x005b
            boolean r0 = r5 instanceof java.lang.Integer
            if (r0 != 0) goto L_0x0043
            goto L_0x0044
        L_0x0043:
            r6 = r5
        L_0x0044:
            java.lang.Integer r6 = (java.lang.Integer) r6
            if (r6 == 0) goto L_0x004d
            int r0 = r6.intValue()
            goto L_0x004e
        L_0x004d:
            r0 = -1
        L_0x004e:
            int r0 = r1.getInt(r2, r0)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r6 = r0
            java.lang.String r6 = (java.lang.String) r6
            goto L_0x00fb
        L_0x005b:
            java.lang.Class r0 = java.lang.Float.TYPE
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r3, r0)
            if (r0 == 0) goto L_0x0085
            boolean r0 = r5 instanceof java.lang.Float
            if (r0 != 0) goto L_0x006c
            goto L_0x006d
        L_0x006c:
            r6 = r5
        L_0x006d:
            java.lang.Float r6 = (java.lang.Float) r6
            if (r6 == 0) goto L_0x0076
            float r0 = r6.floatValue()
            goto L_0x0078
        L_0x0076:
            r0 = -1082130432(0xffffffffbf800000, float:-1.0)
        L_0x0078:
            float r0 = r1.getFloat(r2, r0)
            java.lang.Float r0 = java.lang.Float.valueOf(r0)
            r6 = r0
            java.lang.String r6 = (java.lang.String) r6
            goto L_0x00fb
        L_0x0085:
            java.lang.Class r0 = java.lang.Long.TYPE
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r3, r0)
            if (r0 == 0) goto L_0x00ae
            boolean r0 = r5 instanceof java.lang.Long
            if (r0 != 0) goto L_0x0096
            goto L_0x0097
        L_0x0096:
            r6 = r5
        L_0x0097:
            java.lang.Long r6 = (java.lang.Long) r6
            if (r6 == 0) goto L_0x00a0
            long r6 = r6.longValue()
            goto L_0x00a2
        L_0x00a0:
            r6 = -1
        L_0x00a2:
            long r0 = r1.getLong(r2, r6)
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            r6 = r0
            java.lang.String r6 = (java.lang.String) r6
            goto L_0x00fb
        L_0x00ae:
            java.lang.Class r0 = java.lang.Boolean.TYPE
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r3, r0)
            if (r0 == 0) goto L_0x00d6
            boolean r0 = r5 instanceof java.lang.Boolean
            if (r0 != 0) goto L_0x00bf
            goto L_0x00c0
        L_0x00bf:
            r6 = r5
        L_0x00c0:
            java.lang.Boolean r6 = (java.lang.Boolean) r6
            if (r6 == 0) goto L_0x00c9
            boolean r0 = r6.booleanValue()
            goto L_0x00ca
        L_0x00c9:
            r0 = 0
        L_0x00ca:
            boolean r0 = r1.getBoolean(r2, r0)
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r6 = r0
            java.lang.String r6 = (java.lang.String) r6
            goto L_0x00fb
        L_0x00d6:
            java.lang.Class<java.util.Set> r0 = java.util.Set.class
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r3, r0)
            if (r0 == 0) goto L_0x0173
            boolean r0 = r5 instanceof java.util.Set
            if (r0 != 0) goto L_0x00e7
            goto L_0x00e8
        L_0x00e7:
            r6 = r5
        L_0x00e8:
            java.util.Set r6 = (java.util.Set) r6
            if (r6 == 0) goto L_0x00ed
            goto L_0x00f2
        L_0x00ed:
            java.util.LinkedHashSet r6 = new java.util.LinkedHashSet
            r6.<init>()
        L_0x00f2:
            java.util.Set r0 = r1.getStringSet(r2, r6)
            if (r0 == 0) goto L_0x016d
            r6 = r0
            java.lang.String r6 = (java.lang.String) r6
        L_0x00fb:
            if (r6 == 0) goto L_0x00fe
            r5 = r6
        L_0x00fe:
            int r0 = r5.length()
            if (r0 != 0) goto L_0x0105
            r4 = 1
        L_0x0105:
            if (r4 == 0) goto L_0x016c
            j.c.d.JsonObject r0 = new j.c.d.JsonObject
            r0.<init>()
            java.util.UUID r1 = java.util.UUID.randomUUID()
            java.lang.String r1 = r1.toString()
            java.lang.String r3 = "rnd_id"
            r0.a(r3, r1)
            j.c.d.JsonObject r1 = new j.c.d.JsonObject
            r1.<init>()
            java.lang.String r3 = r8.d()
            java.lang.String r4 = "device_id"
            r1.a(r4, r3)
            java.lang.String r3 = "persistent"
            r0.a(r3, r1)
            java.lang.String r1 = "app_name"
            java.lang.String r3 = "gosuslugi_mob"
            r0.a(r1, r3)
            java.lang.String r1 = android.os.Build.MODEL
            java.lang.String r3 = "phone_model"
            r0.a(r3, r1)
            java.lang.String r1 = "os_type"
            java.lang.String r3 = "android"
            r0.a(r1, r3)
            java.lang.String r1 = android.os.Build.VERSION.RELEASE
            java.lang.String r3 = "os_version"
            r0.a(r3, r1)
            java.lang.String r0 = r0.toString()
            java.lang.String r1 = "obj.toString()"
            n.n.c.Intrinsics.a(r0, r1)
            java.nio.charset.Charset r1 = n.r.Charsets.a
            byte[] r0 = r0.getBytes(r1)
            java.lang.String r1 = "(this as java.lang.String).getBytes(charset)"
            n.n.c.Intrinsics.a(r0, r1)
            java.lang.String r5 = q.a.b.c.a.a(r0)
            java.lang.String r0 = "Base64.toBase64String(byteArray)"
            n.n.c.Intrinsics.a(r5, r0)
            android.content.SharedPreferences r0 = r8.g()
            n.i.Collections.a(r0, r2, r5)
        L_0x016c:
            return r5
        L_0x016d:
            kotlin.TypeCastException r0 = new kotlin.TypeCastException
            r0.<init>(r7)
            throw r0
        L_0x0173:
            java.lang.UnsupportedOperationException r0 = new java.lang.UnsupportedOperationException
            java.lang.String r1 = "Not acceptable type"
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: e.a.a.g.b.b.a.AuthPrefs0.c():java.lang.String");
    }

    /* JADX WARN: Type inference failed for: r0v30, types: [java.lang.Boolean] */
    /* JADX WARN: Type inference failed for: r0v35, types: [java.lang.Long] */
    /* JADX WARN: Type inference failed for: r0v39, types: [java.lang.Float] */
    /* JADX WARN: Type inference failed for: r0v45, types: [java.lang.Integer] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Multi-variable type inference failed */
    @android.annotation.SuppressLint({"HardwareIds"})
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String d() {
        /*
            r8 = this;
            java.lang.Class<java.lang.String> r0 = java.lang.String.class
            android.content.SharedPreferences r1 = r8.g()
            java.lang.String r2 = "DeviceAuthId_key"
            boolean r3 = r1.contains(r2)
            r4 = 0
            java.lang.String r5 = ""
            r6 = 0
            if (r3 != 0) goto L_0x0014
            goto L_0x00fb
        L_0x0014:
            n.p.KClass r3 = n.n.c.Reflection.a(r0)
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r3, r0)
            java.lang.String r7 = "null cannot be cast to non-null type kotlin.String"
            if (r0 == 0) goto L_0x0032
            java.lang.String r6 = r1.getString(r2, r5)
            if (r6 == 0) goto L_0x002c
            goto L_0x00fb
        L_0x002c:
            kotlin.TypeCastException r0 = new kotlin.TypeCastException
            r0.<init>(r7)
            throw r0
        L_0x0032:
            java.lang.Class r0 = java.lang.Integer.TYPE
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r3, r0)
            if (r0 == 0) goto L_0x005b
            boolean r0 = r5 instanceof java.lang.Integer
            if (r0 != 0) goto L_0x0043
            goto L_0x0044
        L_0x0043:
            r6 = r5
        L_0x0044:
            java.lang.Integer r6 = (java.lang.Integer) r6
            if (r6 == 0) goto L_0x004d
            int r0 = r6.intValue()
            goto L_0x004e
        L_0x004d:
            r0 = -1
        L_0x004e:
            int r0 = r1.getInt(r2, r0)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r6 = r0
            java.lang.String r6 = (java.lang.String) r6
            goto L_0x00fb
        L_0x005b:
            java.lang.Class r0 = java.lang.Float.TYPE
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r3, r0)
            if (r0 == 0) goto L_0x0085
            boolean r0 = r5 instanceof java.lang.Float
            if (r0 != 0) goto L_0x006c
            goto L_0x006d
        L_0x006c:
            r6 = r5
        L_0x006d:
            java.lang.Float r6 = (java.lang.Float) r6
            if (r6 == 0) goto L_0x0076
            float r0 = r6.floatValue()
            goto L_0x0078
        L_0x0076:
            r0 = -1082130432(0xffffffffbf800000, float:-1.0)
        L_0x0078:
            float r0 = r1.getFloat(r2, r0)
            java.lang.Float r0 = java.lang.Float.valueOf(r0)
            r6 = r0
            java.lang.String r6 = (java.lang.String) r6
            goto L_0x00fb
        L_0x0085:
            java.lang.Class r0 = java.lang.Long.TYPE
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r3, r0)
            if (r0 == 0) goto L_0x00ae
            boolean r0 = r5 instanceof java.lang.Long
            if (r0 != 0) goto L_0x0096
            goto L_0x0097
        L_0x0096:
            r6 = r5
        L_0x0097:
            java.lang.Long r6 = (java.lang.Long) r6
            if (r6 == 0) goto L_0x00a0
            long r6 = r6.longValue()
            goto L_0x00a2
        L_0x00a0:
            r6 = -1
        L_0x00a2:
            long r0 = r1.getLong(r2, r6)
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            r6 = r0
            java.lang.String r6 = (java.lang.String) r6
            goto L_0x00fb
        L_0x00ae:
            java.lang.Class r0 = java.lang.Boolean.TYPE
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r3, r0)
            if (r0 == 0) goto L_0x00d6
            boolean r0 = r5 instanceof java.lang.Boolean
            if (r0 != 0) goto L_0x00bf
            goto L_0x00c0
        L_0x00bf:
            r6 = r5
        L_0x00c0:
            java.lang.Boolean r6 = (java.lang.Boolean) r6
            if (r6 == 0) goto L_0x00c9
            boolean r0 = r6.booleanValue()
            goto L_0x00ca
        L_0x00c9:
            r0 = 0
        L_0x00ca:
            boolean r0 = r1.getBoolean(r2, r0)
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r6 = r0
            java.lang.String r6 = (java.lang.String) r6
            goto L_0x00fb
        L_0x00d6:
            java.lang.Class<java.util.Set> r0 = java.util.Set.class
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r3, r0)
            if (r0 == 0) goto L_0x0126
            boolean r0 = r5 instanceof java.util.Set
            if (r0 != 0) goto L_0x00e7
            goto L_0x00e8
        L_0x00e7:
            r6 = r5
        L_0x00e8:
            java.util.Set r6 = (java.util.Set) r6
            if (r6 == 0) goto L_0x00ed
            goto L_0x00f2
        L_0x00ed:
            java.util.LinkedHashSet r6 = new java.util.LinkedHashSet
            r6.<init>()
        L_0x00f2:
            java.util.Set r0 = r1.getStringSet(r2, r6)
            if (r0 == 0) goto L_0x0120
            r6 = r0
            java.lang.String r6 = (java.lang.String) r6
        L_0x00fb:
            if (r6 == 0) goto L_0x00fe
            r5 = r6
        L_0x00fe:
            int r0 = r5.length()
            if (r0 != 0) goto L_0x0105
            r4 = 1
        L_0x0105:
            if (r4 == 0) goto L_0x011f
            android.content.Context r0 = r8.b
            android.content.ContentResolver r0 = r0.getContentResolver()
            java.lang.String r1 = "android_id"
            java.lang.String r5 = android.provider.Settings.Secure.getString(r0, r1)
            java.lang.String r0 = "Settings.Secure.getStrin…ROID_ID\n                )"
            n.n.c.Intrinsics.a(r5, r0)
            android.content.SharedPreferences r0 = r8.g()
            n.i.Collections.a(r0, r2, r5)
        L_0x011f:
            return r5
        L_0x0120:
            kotlin.TypeCastException r0 = new kotlin.TypeCastException
            r0.<init>(r7)
            throw r0
        L_0x0126:
            java.lang.UnsupportedOperationException r0 = new java.lang.UnsupportedOperationException
            java.lang.String r1 = "Not acceptable type"
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: e.a.a.g.b.b.a.AuthPrefs0.d():java.lang.String");
    }

    public Session e() {
        return this.d;
    }

    /* JADX WARN: Type inference failed for: r0v24, types: [java.lang.Boolean] */
    /* JADX WARN: Type inference failed for: r0v27, types: [java.lang.Long] */
    /* JADX WARN: Type inference failed for: r0v30, types: [java.lang.Float] */
    /* JADX WARN: Type inference failed for: r0v33, types: [java.lang.Integer] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String f() {
        /*
            r15 = this;
            java.lang.Class<java.lang.String> r0 = java.lang.String.class
            android.content.SharedPreferences r1 = r15.g()
            java.lang.String r2 = "Salt_key"
            boolean r3 = r1.contains(r2)
            r4 = -1
            java.lang.String r6 = "Not acceptable type"
            r7 = -1082130432(0xffffffffbf800000, float:-1.0)
            r8 = -1
            r9 = 0
            java.lang.String r10 = "null cannot be cast to non-null type kotlin.String"
            java.lang.String r11 = ""
            r12 = 0
            if (r3 != 0) goto L_0x001e
            r1 = r12
            goto L_0x0102
        L_0x001e:
            n.p.KClass r3 = n.n.c.Reflection.a(r0)
            n.p.KClass r13 = n.n.c.Reflection.a(r0)
            boolean r13 = n.n.c.Intrinsics.a(r3, r13)
            if (r13 == 0) goto L_0x003a
            java.lang.String r1 = r1.getString(r2, r11)
            if (r1 == 0) goto L_0x0034
            goto L_0x0102
        L_0x0034:
            kotlin.TypeCastException r0 = new kotlin.TypeCastException
            r0.<init>(r10)
            throw r0
        L_0x003a:
            java.lang.Class r13 = java.lang.Integer.TYPE
            n.p.KClass r13 = n.n.c.Reflection.a(r13)
            boolean r13 = n.n.c.Intrinsics.a(r3, r13)
            if (r13 == 0) goto L_0x0063
            boolean r3 = r11 instanceof java.lang.Integer
            if (r3 != 0) goto L_0x004c
            r3 = r12
            goto L_0x004d
        L_0x004c:
            r3 = r11
        L_0x004d:
            java.lang.Integer r3 = (java.lang.Integer) r3
            if (r3 == 0) goto L_0x0056
            int r3 = r3.intValue()
            goto L_0x0057
        L_0x0056:
            r3 = -1
        L_0x0057:
            int r1 = r1.getInt(r2, r3)
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            java.lang.String r1 = (java.lang.String) r1
            goto L_0x0102
        L_0x0063:
            java.lang.Class r13 = java.lang.Float.TYPE
            n.p.KClass r13 = n.n.c.Reflection.a(r13)
            boolean r13 = n.n.c.Intrinsics.a(r3, r13)
            if (r13 == 0) goto L_0x008d
            boolean r3 = r11 instanceof java.lang.Float
            if (r3 != 0) goto L_0x0075
            r3 = r12
            goto L_0x0076
        L_0x0075:
            r3 = r11
        L_0x0076:
            java.lang.Float r3 = (java.lang.Float) r3
            if (r3 == 0) goto L_0x007f
            float r3 = r3.floatValue()
            goto L_0x0081
        L_0x007f:
            r3 = -1082130432(0xffffffffbf800000, float:-1.0)
        L_0x0081:
            float r1 = r1.getFloat(r2, r3)
            java.lang.Float r1 = java.lang.Float.valueOf(r1)
            java.lang.String r1 = (java.lang.String) r1
            goto L_0x0102
        L_0x008d:
            java.lang.Class r13 = java.lang.Long.TYPE
            n.p.KClass r13 = n.n.c.Reflection.a(r13)
            boolean r13 = n.n.c.Intrinsics.a(r3, r13)
            if (r13 == 0) goto L_0x00b5
            boolean r3 = r11 instanceof java.lang.Long
            if (r3 != 0) goto L_0x009f
            r3 = r12
            goto L_0x00a0
        L_0x009f:
            r3 = r11
        L_0x00a0:
            java.lang.Long r3 = (java.lang.Long) r3
            if (r3 == 0) goto L_0x00a9
            long r13 = r3.longValue()
            goto L_0x00aa
        L_0x00a9:
            r13 = r4
        L_0x00aa:
            long r13 = r1.getLong(r2, r13)
            java.lang.Long r1 = java.lang.Long.valueOf(r13)
            java.lang.String r1 = (java.lang.String) r1
            goto L_0x0102
        L_0x00b5:
            java.lang.Class r13 = java.lang.Boolean.TYPE
            n.p.KClass r13 = n.n.c.Reflection.a(r13)
            boolean r13 = n.n.c.Intrinsics.a(r3, r13)
            if (r13 == 0) goto L_0x00dd
            boolean r3 = r11 instanceof java.lang.Boolean
            if (r3 != 0) goto L_0x00c7
            r3 = r12
            goto L_0x00c8
        L_0x00c7:
            r3 = r11
        L_0x00c8:
            java.lang.Boolean r3 = (java.lang.Boolean) r3
            if (r3 == 0) goto L_0x00d1
            boolean r3 = r3.booleanValue()
            goto L_0x00d2
        L_0x00d1:
            r3 = 0
        L_0x00d2:
            boolean r1 = r1.getBoolean(r2, r3)
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)
            java.lang.String r1 = (java.lang.String) r1
            goto L_0x0102
        L_0x00dd:
            java.lang.Class<java.util.Set> r13 = java.util.Set.class
            n.p.KClass r13 = n.n.c.Reflection.a(r13)
            boolean r3 = n.n.c.Intrinsics.a(r3, r13)
            if (r3 == 0) goto L_0x0229
            boolean r3 = r11 instanceof java.util.Set
            if (r3 != 0) goto L_0x00ef
            r3 = r12
            goto L_0x00f0
        L_0x00ef:
            r3 = r11
        L_0x00f0:
            java.util.Set r3 = (java.util.Set) r3
            if (r3 == 0) goto L_0x00f5
            goto L_0x00fa
        L_0x00f5:
            java.util.LinkedHashSet r3 = new java.util.LinkedHashSet
            r3.<init>()
        L_0x00fa:
            java.util.Set r1 = r1.getStringSet(r2, r3)
            if (r1 == 0) goto L_0x0223
            java.lang.String r1 = (java.lang.String) r1
        L_0x0102:
            if (r1 == 0) goto L_0x0105
            goto L_0x0106
        L_0x0105:
            r1 = r11
        L_0x0106:
            int r1 = r1.length()
            if (r1 != 0) goto L_0x010e
            r1 = 1
            goto L_0x010f
        L_0x010e:
            r1 = 0
        L_0x010f:
            if (r1 == 0) goto L_0x012c
            android.content.SharedPreferences r1 = r15.g()
            r3 = 8
            java.security.SecureRandom r13 = new java.security.SecureRandom
            r13.<init>()
            byte[] r3 = r13.generateSeed(r3)
            java.lang.String r3 = q.a.b.c.a.a(r3)
            java.lang.String r13 = "Base64.toBase64String(seed)"
            n.n.c.Intrinsics.a(r3, r13)
            n.i.Collections.a(r1, r2, r3)
        L_0x012c:
            android.content.SharedPreferences r1 = r15.g()
            boolean r3 = r1.contains(r2)
            if (r3 != 0) goto L_0x0138
            goto L_0x0213
        L_0x0138:
            n.p.KClass r3 = n.n.c.Reflection.a(r0)
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r3, r0)
            if (r0 == 0) goto L_0x0154
            java.lang.String r12 = r1.getString(r2, r11)
            if (r12 == 0) goto L_0x014e
            goto L_0x0213
        L_0x014e:
            kotlin.TypeCastException r0 = new kotlin.TypeCastException
            r0.<init>(r10)
            throw r0
        L_0x0154:
            java.lang.Class r0 = java.lang.Integer.TYPE
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r3, r0)
            if (r0 == 0) goto L_0x017b
            boolean r0 = r11 instanceof java.lang.Integer
            if (r0 != 0) goto L_0x0165
            goto L_0x0166
        L_0x0165:
            r12 = r11
        L_0x0166:
            java.lang.Integer r12 = (java.lang.Integer) r12
            if (r12 == 0) goto L_0x016e
            int r8 = r12.intValue()
        L_0x016e:
            int r0 = r1.getInt(r2, r8)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r12 = r0
            java.lang.String r12 = (java.lang.String) r12
            goto L_0x0213
        L_0x017b:
            java.lang.Class r0 = java.lang.Float.TYPE
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r3, r0)
            if (r0 == 0) goto L_0x01a2
            boolean r0 = r11 instanceof java.lang.Float
            if (r0 != 0) goto L_0x018c
            goto L_0x018d
        L_0x018c:
            r12 = r11
        L_0x018d:
            java.lang.Float r12 = (java.lang.Float) r12
            if (r12 == 0) goto L_0x0195
            float r7 = r12.floatValue()
        L_0x0195:
            float r0 = r1.getFloat(r2, r7)
            java.lang.Float r0 = java.lang.Float.valueOf(r0)
            r12 = r0
            java.lang.String r12 = (java.lang.String) r12
            goto L_0x0213
        L_0x01a2:
            java.lang.Class r0 = java.lang.Long.TYPE
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r3, r0)
            if (r0 == 0) goto L_0x01c8
            boolean r0 = r11 instanceof java.lang.Long
            if (r0 != 0) goto L_0x01b3
            goto L_0x01b4
        L_0x01b3:
            r12 = r11
        L_0x01b4:
            java.lang.Long r12 = (java.lang.Long) r12
            if (r12 == 0) goto L_0x01bc
            long r4 = r12.longValue()
        L_0x01bc:
            long r0 = r1.getLong(r2, r4)
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            r12 = r0
            java.lang.String r12 = (java.lang.String) r12
            goto L_0x0213
        L_0x01c8:
            java.lang.Class r0 = java.lang.Boolean.TYPE
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r3, r0)
            if (r0 == 0) goto L_0x01ee
            boolean r0 = r11 instanceof java.lang.Boolean
            if (r0 != 0) goto L_0x01d9
            goto L_0x01da
        L_0x01d9:
            r12 = r11
        L_0x01da:
            java.lang.Boolean r12 = (java.lang.Boolean) r12
            if (r12 == 0) goto L_0x01e2
            boolean r9 = r12.booleanValue()
        L_0x01e2:
            boolean r0 = r1.getBoolean(r2, r9)
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r12 = r0
            java.lang.String r12 = (java.lang.String) r12
            goto L_0x0213
        L_0x01ee:
            java.lang.Class<java.util.Set> r0 = java.util.Set.class
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r3, r0)
            if (r0 == 0) goto L_0x021d
            boolean r0 = r11 instanceof java.util.Set
            if (r0 != 0) goto L_0x01ff
            goto L_0x0200
        L_0x01ff:
            r12 = r11
        L_0x0200:
            java.util.Set r12 = (java.util.Set) r12
            if (r12 == 0) goto L_0x0205
            goto L_0x020a
        L_0x0205:
            java.util.LinkedHashSet r12 = new java.util.LinkedHashSet
            r12.<init>()
        L_0x020a:
            java.util.Set r0 = r1.getStringSet(r2, r12)
            if (r0 == 0) goto L_0x0217
            r12 = r0
            java.lang.String r12 = (java.lang.String) r12
        L_0x0213:
            if (r12 == 0) goto L_0x0216
            r11 = r12
        L_0x0216:
            return r11
        L_0x0217:
            kotlin.TypeCastException r0 = new kotlin.TypeCastException
            r0.<init>(r10)
            throw r0
        L_0x021d:
            java.lang.UnsupportedOperationException r0 = new java.lang.UnsupportedOperationException
            r0.<init>(r6)
            throw r0
        L_0x0223:
            kotlin.TypeCastException r0 = new kotlin.TypeCastException
            r0.<init>(r10)
            throw r0
        L_0x0229:
            java.lang.UnsupportedOperationException r0 = new java.lang.UnsupportedOperationException
            r0.<init>(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: e.a.a.g.b.b.a.AuthPrefs0.f():java.lang.String");
    }

    public final SharedPreferences g() {
        Lazy lazy = this.a;
        KProperty kProperty = f623e[0];
        return (SharedPreferences) lazy.getValue();
    }

    /* JADX WARN: Type inference failed for: r0v5, types: [java.lang.Long] */
    /* JADX WARN: Type inference failed for: r0v7, types: [java.lang.Float] */
    /* JADX WARN: Type inference failed for: r0v9, types: [java.lang.Integer] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a() {
        /*
            r8 = this;
            android.content.SharedPreferences r0 = r8.g()
            r1 = 0
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r1)
            java.lang.String r3 = "DefaultPincode_Key"
            boolean r4 = r0.contains(r3)
            r5 = 0
            if (r4 != 0) goto L_0x0014
            goto L_0x00fa
        L_0x0014:
            java.lang.Class<java.lang.Boolean> r4 = java.lang.Boolean.class
            n.p.KClass r4 = n.n.c.Reflection.a(r4)
            java.lang.Class<java.lang.String> r6 = java.lang.String.class
            n.p.KClass r6 = n.n.c.Reflection.a(r6)
            boolean r6 = n.n.c.Intrinsics.a(r4, r6)
            java.lang.String r7 = "null cannot be cast to non-null type kotlin.Boolean"
            if (r6 == 0) goto L_0x0040
            boolean r4 = r2 instanceof java.lang.String
            if (r4 != 0) goto L_0x002d
            r2 = r5
        L_0x002d:
            java.lang.String r2 = (java.lang.String) r2
            java.lang.String r0 = r0.getString(r3, r2)
            if (r0 == 0) goto L_0x003a
            r5 = r0
            java.lang.Boolean r5 = (java.lang.Boolean) r5
            goto L_0x00fa
        L_0x003a:
            kotlin.TypeCastException r0 = new kotlin.TypeCastException
            r0.<init>(r7)
            throw r0
        L_0x0040:
            java.lang.Class r6 = java.lang.Integer.TYPE
            n.p.KClass r6 = n.n.c.Reflection.a(r6)
            boolean r6 = n.n.c.Intrinsics.a(r4, r6)
            if (r6 == 0) goto L_0x0068
            boolean r4 = r2 instanceof java.lang.Integer
            if (r4 != 0) goto L_0x0051
            r2 = r5
        L_0x0051:
            java.lang.Integer r2 = (java.lang.Integer) r2
            if (r2 == 0) goto L_0x005a
            int r2 = r2.intValue()
            goto L_0x005b
        L_0x005a:
            r2 = -1
        L_0x005b:
            int r0 = r0.getInt(r3, r2)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r5 = r0
            java.lang.Boolean r5 = (java.lang.Boolean) r5
            goto L_0x00fa
        L_0x0068:
            java.lang.Class r6 = java.lang.Float.TYPE
            n.p.KClass r6 = n.n.c.Reflection.a(r6)
            boolean r6 = n.n.c.Intrinsics.a(r4, r6)
            if (r6 == 0) goto L_0x0091
            boolean r4 = r2 instanceof java.lang.Float
            if (r4 != 0) goto L_0x0079
            r2 = r5
        L_0x0079:
            java.lang.Float r2 = (java.lang.Float) r2
            if (r2 == 0) goto L_0x0082
            float r2 = r2.floatValue()
            goto L_0x0084
        L_0x0082:
            r2 = -1082130432(0xffffffffbf800000, float:-1.0)
        L_0x0084:
            float r0 = r0.getFloat(r3, r2)
            java.lang.Float r0 = java.lang.Float.valueOf(r0)
            r5 = r0
            java.lang.Boolean r5 = (java.lang.Boolean) r5
            goto L_0x00fa
        L_0x0091:
            java.lang.Class r6 = java.lang.Long.TYPE
            n.p.KClass r6 = n.n.c.Reflection.a(r6)
            boolean r6 = n.n.c.Intrinsics.a(r4, r6)
            if (r6 == 0) goto L_0x00b9
            boolean r4 = r2 instanceof java.lang.Long
            if (r4 != 0) goto L_0x00a2
            r2 = r5
        L_0x00a2:
            java.lang.Long r2 = (java.lang.Long) r2
            if (r2 == 0) goto L_0x00ab
            long r4 = r2.longValue()
            goto L_0x00ad
        L_0x00ab:
            r4 = -1
        L_0x00ad:
            long r2 = r0.getLong(r3, r4)
            java.lang.Long r0 = java.lang.Long.valueOf(r2)
            r5 = r0
            java.lang.Boolean r5 = (java.lang.Boolean) r5
            goto L_0x00fa
        L_0x00b9:
            java.lang.Class r6 = java.lang.Boolean.TYPE
            n.p.KClass r6 = n.n.c.Reflection.a(r6)
            boolean r6 = n.n.c.Intrinsics.a(r4, r6)
            if (r6 == 0) goto L_0x00d6
            if (r2 == 0) goto L_0x00cc
            boolean r2 = r2.booleanValue()
            goto L_0x00cd
        L_0x00cc:
            r2 = 0
        L_0x00cd:
            boolean r0 = r0.getBoolean(r3, r2)
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r0)
            goto L_0x00fa
        L_0x00d6:
            java.lang.Class<java.util.Set> r6 = java.util.Set.class
            n.p.KClass r6 = n.n.c.Reflection.a(r6)
            boolean r4 = n.n.c.Intrinsics.a(r4, r6)
            if (r4 == 0) goto L_0x0107
            boolean r4 = r2 instanceof java.util.Set
            if (r4 != 0) goto L_0x00e7
            r2 = r5
        L_0x00e7:
            java.util.Set r2 = (java.util.Set) r2
            if (r2 == 0) goto L_0x00ec
            goto L_0x00f1
        L_0x00ec:
            java.util.LinkedHashSet r2 = new java.util.LinkedHashSet
            r2.<init>()
        L_0x00f1:
            java.util.Set r0 = r0.getStringSet(r3, r2)
            if (r0 == 0) goto L_0x0101
            r5 = r0
            java.lang.Boolean r5 = (java.lang.Boolean) r5
        L_0x00fa:
            if (r5 == 0) goto L_0x0100
            boolean r1 = r5.booleanValue()
        L_0x0100:
            return r1
        L_0x0101:
            kotlin.TypeCastException r0 = new kotlin.TypeCastException
            r0.<init>(r7)
            throw r0
        L_0x0107:
            java.lang.UnsupportedOperationException r0 = new java.lang.UnsupportedOperationException
            java.lang.String r1 = "Not acceptable type"
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: e.a.a.g.b.b.a.AuthPrefs0.a():boolean");
    }

    /* JADX WARN: Type inference failed for: r0v28, types: [java.lang.Boolean] */
    /* JADX WARN: Type inference failed for: r0v34, types: [java.lang.Long] */
    /* JADX WARN: Type inference failed for: r0v40, types: [java.lang.Float] */
    /* JADX WARN: Type inference failed for: r0v46, types: [java.lang.Integer] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String b(java.lang.String r9) {
        /*
            r8 = this;
            java.lang.Class<java.lang.String> r0 = java.lang.String.class
            r1 = 0
            if (r9 == 0) goto L_0x012f
            android.content.SharedPreferences r2 = r8.g()
            java.lang.String r3 = "ACCESS_TOKEN"
            boolean r4 = r2.contains(r3)
            java.lang.String r5 = ""
            r6 = 0
            if (r4 != 0) goto L_0x0016
            goto L_0x00fd
        L_0x0016:
            n.p.KClass r4 = n.n.c.Reflection.a(r0)
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r4, r0)
            java.lang.String r7 = "null cannot be cast to non-null type kotlin.String"
            if (r0 == 0) goto L_0x0034
            java.lang.String r1 = r2.getString(r3, r5)
            if (r1 == 0) goto L_0x002e
            goto L_0x00fd
        L_0x002e:
            kotlin.TypeCastException r9 = new kotlin.TypeCastException
            r9.<init>(r7)
            throw r9
        L_0x0034:
            java.lang.Class r0 = java.lang.Integer.TYPE
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r4, r0)
            if (r0 == 0) goto L_0x005d
            boolean r0 = r5 instanceof java.lang.Integer
            if (r0 != 0) goto L_0x0045
            goto L_0x0046
        L_0x0045:
            r1 = r5
        L_0x0046:
            java.lang.Integer r1 = (java.lang.Integer) r1
            if (r1 == 0) goto L_0x004f
            int r0 = r1.intValue()
            goto L_0x0050
        L_0x004f:
            r0 = -1
        L_0x0050:
            int r0 = r2.getInt(r3, r0)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r1 = r0
            java.lang.String r1 = (java.lang.String) r1
            goto L_0x00fd
        L_0x005d:
            java.lang.Class r0 = java.lang.Float.TYPE
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r4, r0)
            if (r0 == 0) goto L_0x0087
            boolean r0 = r5 instanceof java.lang.Float
            if (r0 != 0) goto L_0x006e
            goto L_0x006f
        L_0x006e:
            r1 = r5
        L_0x006f:
            java.lang.Float r1 = (java.lang.Float) r1
            if (r1 == 0) goto L_0x0078
            float r0 = r1.floatValue()
            goto L_0x007a
        L_0x0078:
            r0 = -1082130432(0xffffffffbf800000, float:-1.0)
        L_0x007a:
            float r0 = r2.getFloat(r3, r0)
            java.lang.Float r0 = java.lang.Float.valueOf(r0)
            r1 = r0
            java.lang.String r1 = (java.lang.String) r1
            goto L_0x00fd
        L_0x0087:
            java.lang.Class r0 = java.lang.Long.TYPE
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r4, r0)
            if (r0 == 0) goto L_0x00b0
            boolean r0 = r5 instanceof java.lang.Long
            if (r0 != 0) goto L_0x0098
            goto L_0x0099
        L_0x0098:
            r1 = r5
        L_0x0099:
            java.lang.Long r1 = (java.lang.Long) r1
            if (r1 == 0) goto L_0x00a2
            long r0 = r1.longValue()
            goto L_0x00a4
        L_0x00a2:
            r0 = -1
        L_0x00a4:
            long r0 = r2.getLong(r3, r0)
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            r1 = r0
            java.lang.String r1 = (java.lang.String) r1
            goto L_0x00fd
        L_0x00b0:
            java.lang.Class r0 = java.lang.Boolean.TYPE
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r4, r0)
            if (r0 == 0) goto L_0x00d8
            boolean r0 = r5 instanceof java.lang.Boolean
            if (r0 != 0) goto L_0x00c1
            goto L_0x00c2
        L_0x00c1:
            r1 = r5
        L_0x00c2:
            java.lang.Boolean r1 = (java.lang.Boolean) r1
            if (r1 == 0) goto L_0x00cb
            boolean r0 = r1.booleanValue()
            goto L_0x00cc
        L_0x00cb:
            r0 = 0
        L_0x00cc:
            boolean r0 = r2.getBoolean(r3, r0)
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r1 = r0
            java.lang.String r1 = (java.lang.String) r1
            goto L_0x00fd
        L_0x00d8:
            java.lang.Class<java.util.Set> r0 = java.util.Set.class
            n.p.KClass r0 = n.n.c.Reflection.a(r0)
            boolean r0 = n.n.c.Intrinsics.a(r4, r0)
            if (r0 == 0) goto L_0x0127
            boolean r0 = r5 instanceof java.util.Set
            if (r0 != 0) goto L_0x00e9
            goto L_0x00ea
        L_0x00e9:
            r1 = r5
        L_0x00ea:
            java.util.Set r1 = (java.util.Set) r1
            if (r1 == 0) goto L_0x00ef
            goto L_0x00f4
        L_0x00ef:
            java.util.LinkedHashSet r1 = new java.util.LinkedHashSet
            r1.<init>()
        L_0x00f4:
            java.util.Set r0 = r2.getStringSet(r3, r1)
            if (r0 == 0) goto L_0x0121
            r1 = r0
            java.lang.String r1 = (java.lang.String) r1
        L_0x00fd:
            if (r1 == 0) goto L_0x0100
            goto L_0x0101
        L_0x0100:
            r1 = r5
        L_0x0101:
            int r0 = r1.length()     // Catch:{ all -> 0x0113 }
            if (r0 <= 0) goto L_0x0108
            r6 = 1
        L_0x0108:
            if (r6 == 0) goto L_0x0112
            java.lang.String r0 = r8.f()     // Catch:{ all -> 0x0113 }
            java.lang.String r5 = e.a.a.g.b.b.a.AuthPrefs.a(r1, r9, r0)     // Catch:{ all -> 0x0113 }
        L_0x0112:
            return r5
        L_0x0113:
            r9 = move-exception
            t.a.Timber$b r0 = t.a.Timber.d
            r0.a(r9)
            e.a.a.g.b.b.a.AuthPrefs$a r9 = new e.a.a.g.b.b.a.AuthPrefs$a
            java.lang.String r0 = "Access token decryption error"
            r9.<init>(r0)
            throw r9
        L_0x0121:
            kotlin.TypeCastException r9 = new kotlin.TypeCastException
            r9.<init>(r7)
            throw r9
        L_0x0127:
            java.lang.UnsupportedOperationException r9 = new java.lang.UnsupportedOperationException
            java.lang.String r0 = "Not acceptable type"
            r9.<init>(r0)
            throw r9
        L_0x012f:
            java.lang.String r9 = "password"
            n.n.c.Intrinsics.a(r9)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: e.a.a.g.b.b.a.AuthPrefs0.b(java.lang.String):java.lang.String");
    }

    public void c(String str) {
        if (str != null) {
            Collections.a(g(), "EsiaOid_key", str);
        } else {
            Intrinsics.a("value");
            throw null;
        }
    }
}
