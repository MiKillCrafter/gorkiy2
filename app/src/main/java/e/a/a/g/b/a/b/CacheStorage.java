package e.a.a.g.b.a.b;

import e.c.b.i.a.ICacheStorage;
import n.n.c.Intrinsics;

/* compiled from: CacheStorage.kt */
public final class CacheStorage implements ICacheStorage {
    public final CacheDao a;

    public CacheStorage(CacheDao cacheDao) {
        if (cacheDao != null) {
            this.a = cacheDao;
        } else {
            Intrinsics.a("cacheDao");
            throw null;
        }
    }

    public void a() {
        this.a.a();
    }
}
