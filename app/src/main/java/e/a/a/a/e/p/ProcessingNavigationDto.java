package e.a.a.a.e.p;

import android.os.Parcel;
import android.os.Parcelable;
import n.n.c.Intrinsics;

/* compiled from: ProcessingNavigationDto.kt */
public final class ProcessingNavigationDto implements Parcelable {
    public static final Parcelable.Creator CREATOR = new a();
    public final String b;
    public final String c;
    public final boolean d;

    public static class a implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            if (parcel != null) {
                return new ProcessingNavigationDto(parcel.readString(), parcel.readString(), parcel.readInt() != 0);
            }
            Intrinsics.a("in");
            throw null;
        }

        public final Object[] newArray(int i2) {
            return new ProcessingNavigationDto[i2];
        }
    }

    public ProcessingNavigationDto() {
        if ("" != 0) {
            this.b = "";
            this.c = "";
            this.d = false;
            return;
        }
        Intrinsics.a("parent");
        throw null;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeString(this.b);
            parcel.writeString(this.c);
            parcel.writeInt(this.d ? 1 : 0);
            return;
        }
        Intrinsics.a("parcel");
        throw null;
    }

    public ProcessingNavigationDto(String str, String str2, boolean z) {
        if (str != null) {
            this.b = str;
            this.c = str2;
            this.d = z;
            return;
        }
        Intrinsics.a("parent");
        throw null;
    }
}
