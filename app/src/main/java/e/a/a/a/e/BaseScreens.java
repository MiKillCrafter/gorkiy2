package e.a.a.a.e;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.fragment.app.Fragment;
import e.a.a.a.c.c.BottomSheetSelectorDialog0;
import e.a.a.a.e.p.BottomSheetSelectorNavigationDto0;
import j.a.a.a.outline;
import n.n.c.Intrinsics;

/* compiled from: BaseScreens.kt */
public final class BaseScreens extends BaseScreens2 implements Parcelable {
    public static final Parcelable.Creator CREATOR = new a();
    public final BottomSheetSelectorNavigationDto0 d;

    public static class a implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            if (parcel != null) {
                return new BaseScreens((BottomSheetSelectorNavigationDto0) BottomSheetSelectorNavigationDto0.CREATOR.createFromParcel(parcel));
            }
            Intrinsics.a("in");
            throw null;
        }

        public final Object[] newArray(int i2) {
            return new BaseScreens[i2];
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BaseScreens(BottomSheetSelectorNavigationDto0 bottomSheetSelectorNavigationDto0) {
        super(bottomSheetSelectorNavigationDto0);
        if (bottomSheetSelectorNavigationDto0 != null) {
            this.d = bottomSheetSelectorNavigationDto0;
            return;
        }
        Intrinsics.a("dto");
        throw null;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [androidx.fragment.app.Fragment, e.a.a.a.c.c.BottomSheetSelectorDialog0] */
    public Fragment a() {
        return new BottomSheetSelectorDialog0();
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof BaseScreens) && Intrinsics.a(this.d, ((BaseScreens) obj).d);
        }
        return true;
    }

    public int hashCode() {
        BottomSheetSelectorNavigationDto0 bottomSheetSelectorNavigationDto0 = this.d;
        if (bottomSheetSelectorNavigationDto0 != null) {
            return bottomSheetSelectorNavigationDto0.hashCode();
        }
        return 0;
    }

    public String toString() {
        StringBuilder a2 = outline.a("BottomSheetSelectorScreen(dto=");
        a2.append(this.d);
        a2.append(")");
        return a2.toString();
    }

    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            this.d.writeToParcel(parcel, 0);
        } else {
            Intrinsics.a("parcel");
            throw null;
        }
    }
}
