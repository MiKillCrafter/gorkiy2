package e.a.a.a.a;

import l.b.t.Consumer;
import t.a.Timber;

/* compiled from: BaseAuthFragmentVm.kt */
public final class BaseAuthFragmentVm7<T> implements Consumer<Throwable> {
    public final /* synthetic */ BaseAuthFragmentVm8 b;

    public BaseAuthFragmentVm7(BaseAuthFragmentVm8 baseAuthFragmentVm8) {
        this.b = baseAuthFragmentVm8;
    }

    public void a(Object obj) {
        Timber.d.a((Throwable) obj, "Restore session error: ", new Object[0]);
        this.b.f577r.a((Boolean) false);
    }
}
