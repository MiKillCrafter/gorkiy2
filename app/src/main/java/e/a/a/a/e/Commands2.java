package e.a.a.a.e;

import android.net.Uri;
import e.b.a.h.Command;
import n.n.c.Intrinsics;

/* compiled from: Commands.kt */
public final class Commands2 implements Command {
    public final Uri a;

    public Commands2(Uri uri) {
        if (uri != null) {
            this.a = uri;
        } else {
            Intrinsics.a("link");
            throw null;
        }
    }
}
