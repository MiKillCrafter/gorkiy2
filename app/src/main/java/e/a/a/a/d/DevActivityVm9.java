package e.a.a.a.d;

import e.a.a.j.b.IDevMenuRepository;
import l.b.t.Function;
import n.n.c.Intrinsics;

/* compiled from: DevActivityVm.kt */
public final class DevActivityVm9<T, R> implements Function<T, R> {
    public static final DevActivityVm9 a = new DevActivityVm9();

    public Object a(Object obj) {
        IDevMenuRepository iDevMenuRepository = (IDevMenuRepository) obj;
        if (iDevMenuRepository != null) {
            return iDevMenuRepository.name();
        }
        Intrinsics.a("it");
        throw null;
    }
}
