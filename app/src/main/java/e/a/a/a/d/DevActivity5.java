package e.a.a.a.d;

import n.Unit;
import n.g;
import n.n.b.Functions;
import n.n.c.Reflection;
import n.n.c.h;
import n.p.KDeclarationContainer;
import ru.covid19.core.presentation.dev.DevActivity;

/* compiled from: DevActivity.kt */
public final /* synthetic */ class DevActivity5 extends h implements Functions<g> {
    public DevActivity5(DevActivity devActivity) {
        super(0, devActivity);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [n.n.c.CallableReference, e.a.a.a.d.DevActivity5] */
    public Object b() {
        DevActivity.a((DevActivity) this.c);
        return Unit.a;
    }

    public final String f() {
        return "finishActivity";
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [n.p.KClass, n.p.KDeclarationContainer] */
    public final KDeclarationContainer g() {
        return Reflection.a(DevActivity.class);
    }

    public final String i() {
        return "finishActivity()V";
    }
}
