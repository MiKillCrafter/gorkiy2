package e.a.a.a.c.b;

import j.a.a.a.outline;
import n.n.c.Intrinsics;

/* compiled from: ProcessingFragmentViewState.kt */
public final class ProcessingFragmentViewState {
    public final String a;
    public final boolean b;

    public ProcessingFragmentViewState() {
        this.a = null;
        this.b = false;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ProcessingFragmentViewState)) {
            return false;
        }
        ProcessingFragmentViewState processingFragmentViewState = (ProcessingFragmentViewState) obj;
        return Intrinsics.a(this.a, processingFragmentViewState.a) && this.b == processingFragmentViewState.b;
    }

    public int hashCode() {
        String str = this.a;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        boolean z = this.b;
        if (z) {
            z = true;
        }
        return hashCode + (z ? 1 : 0);
    }

    public String toString() {
        StringBuilder a2 = outline.a("ProcessingFragmentViewState(message=");
        a2.append(this.a);
        a2.append(", hideAppOnBack=");
        return outline.a(a2, this.b, ")");
    }

    public /* synthetic */ ProcessingFragmentViewState(String str, boolean z, int i2) {
        str = (i2 & 1) != 0 ? null : str;
        z = (i2 & 2) != 0 ? false : z;
        this.a = str;
        this.b = z;
    }
}
