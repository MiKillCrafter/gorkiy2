package e.a.a.a.d;

import e.a.a.a.e.o.c;
import e.c.b.i.a.a;
import k.a.Factory;
import m.a.Provider;

public final class DevActivityVm_Factory implements Factory<m> {
    public final Provider<c> a;
    public final Provider<e.a.a.j.b.c> b;
    public final Provider<e.a.a.j.a.c> c;
    public final Provider<a> d;

    public DevActivityVm_Factory(Provider<c> provider, Provider<e.a.a.j.b.c> provider2, Provider<e.a.a.j.a.c> provider3, Provider<a> provider4) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
    }

    public Object get() {
        return new DevActivityVm2(this.a.get(), this.b.get(), this.c.get(), this.d.get());
    }
}
