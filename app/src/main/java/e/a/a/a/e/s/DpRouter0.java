package e.a.a.a.e.s;

import android.util.SparseArray;
import e.b.a.Router;

/* compiled from: DpRouter.kt */
public class DpRouter0 extends Router {
    public final SparseArray<e> b = new SparseArray<>();
    public final SparseArray<Object> c = new SparseArray<>();

    public final boolean a(int i2, Object obj) {
        DpRouter dpRouter = this.b.get(i2);
        if (dpRouter != null) {
            dpRouter.a(obj);
            return true;
        }
        this.c.put(i2, obj);
        return false;
    }
}
