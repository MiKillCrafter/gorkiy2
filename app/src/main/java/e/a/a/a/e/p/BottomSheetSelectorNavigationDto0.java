package e.a.a.a.e.p;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import n.i.Collections2;
import n.n.c.Intrinsics;

/* compiled from: BottomSheetSelectorNavigationDto.kt */
public final class BottomSheetSelectorNavigationDto0 implements Parcelable {
    public static final Parcelable.Creator CREATOR = new a();
    public final String b;
    public final List<a> c;

    public static class a implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            if (parcel != null) {
                String readString = parcel.readString();
                int readInt = parcel.readInt();
                ArrayList arrayList = new ArrayList(readInt);
                while (readInt != 0) {
                    arrayList.add((BottomSheetSelectorNavigationDto) parcel.readParcelable(BottomSheetSelectorNavigationDto0.class.getClassLoader()));
                    readInt--;
                }
                return new BottomSheetSelectorNavigationDto0(readString, arrayList);
            }
            Intrinsics.a("in");
            throw null;
        }

        public final Object[] newArray(int i2) {
            return new BottomSheetSelectorNavigationDto0[i2];
        }
    }

    public BottomSheetSelectorNavigationDto0(String str, List<? extends a> list) {
        if (str == null) {
            Intrinsics.a("title");
            throw null;
        } else if (list != null) {
            this.b = str;
            this.c = list;
        } else {
            Intrinsics.a("items");
            throw null;
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeString(this.b);
            List<a> list = this.c;
            parcel.writeInt(list.size());
            Iterator<a> it = list.iterator();
            while (it.hasNext()) {
                parcel.writeParcelable(it.next(), i2);
            }
            return;
        }
        Intrinsics.a("parcel");
        throw null;
    }

    public BottomSheetSelectorNavigationDto0() {
        this("", Collections2.b);
    }
}
