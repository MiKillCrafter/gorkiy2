package e.a.a.a.e.q;

import e.a.a.a.e.p.BottomSheetSelectorNavigationDto;
import e.a.a.a.e.p.BottomSheetSelectorNavigationDto0;
import e.a.a.a.e.p.ErrorNavigationDto;
import e.a.a.a.e.p.ErrorResultDto;
import e.a.a.a.e.p.ProcessingNavigationDto0;
import e.a.a.a.e.p.a;
import e.a.a.a.e.p.f;
import l.b.Observable;

/* compiled from: IFragmentCoordinator.kt */
public interface IFragmentCoordinator {
    void a(BottomSheetSelectorNavigationDto0 bottomSheetSelectorNavigationDto0);

    void a(BottomSheetSelectorNavigationDto bottomSheetSelectorNavigationDto);

    void a(ErrorNavigationDto errorNavigationDto);

    void a(ErrorResultDto errorResultDto);

    void a(ProcessingNavigationDto0 processingNavigationDto0);

    void a(Integer num, Object obj);

    void a(String str);

    Observable<f> b(String str);

    Observable<a> f();
}
