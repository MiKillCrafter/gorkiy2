package e.a.a.a.b;

import android.os.Parcelable;
import com.crashlytics.android.core.CrashlyticsController;
import e.a.a.a.e.p.ErrorNavigationDto;
import e.a.a.a.e.q.IFragmentCoordinator;
import e.a.a.f;
import e.c.c.BaseMviVm0;
import e.c.c.k;
import j.c.a.a.c.n.c;
import l.b.t.Function;
import n.n.b.Functions0;
import n.n.c.Intrinsics;
import ru.waveaccess.waandroidnetwork.Exceptions0;
import t.a.Timber;

/* compiled from: BaseDpFragmentVm.kt */
public final class BaseDpFragmentVm0<T, R> implements Function<Throwable, BaseMviVm0<? extends k>> {
    public final /* synthetic */ BaseDpFragmentVm1 a;
    public final /* synthetic */ Functions0 b;
    public final /* synthetic */ ErrorNavigationDto c;
    public final /* synthetic */ Class d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ Parcelable f584e;

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ Parcelable f585f;

    public BaseDpFragmentVm0(BaseDpFragmentVm1 baseDpFragmentVm1, Functions0 functions0, ErrorNavigationDto errorNavigationDto, Class cls, Parcelable parcelable, Parcelable parcelable2) {
        this.a = baseDpFragmentVm1;
        this.b = functions0;
        this.c = errorNavigationDto;
        this.d = cls;
        this.f584e = parcelable;
        this.f585f = parcelable2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Object a(Object obj) {
        String str;
        String str2;
        int i2;
        int i3;
        int i4;
        Parcelable parcelable;
        Parcelable parcelable2;
        BaseMviVm0.b bVar;
        int i5;
        String str3;
        String str4;
        String str5;
        int i6;
        int i7;
        int i8;
        Parcelable parcelable3;
        Parcelable parcelable4;
        String str6;
        Throwable th = (Throwable) obj;
        Integer num = null;
        if (th != null) {
            Timber.d.a(th);
            BaseMviVm0 baseMviVm0 = (BaseMviVm0) this.b.a(th);
            if (baseMviVm0 != null) {
                return baseMviVm0;
            }
            if (th instanceof Exceptions0) {
                IFragmentCoordinator iFragmentCoordinator = this.a.f586i;
                ErrorNavigationDto errorNavigationDto = this.c;
                if (errorNavigationDto == null || (str4 = errorNavigationDto.b) == null) {
                    str4 = this.d.getName();
                    Intrinsics.a((Object) str4, "resultClass.name");
                }
                String str7 = str4;
                ErrorNavigationDto errorNavigationDto2 = this.c;
                if (errorNavigationDto2 == null || (str6 = errorNavigationDto2.c) == null) {
                    str5 = "";
                } else {
                    str5 = str6;
                }
                ErrorNavigationDto errorNavigationDto3 = this.c;
                if ((errorNavigationDto3 != null ? Integer.valueOf(errorNavigationDto3.h) : null) == null || (i6 = this.c.h) == 0) {
                    i6 = f.frag_error_no_internet_connection_title;
                }
                int i9 = i6;
                ErrorNavigationDto errorNavigationDto4 = this.c;
                if ((errorNavigationDto4 != null ? Integer.valueOf(errorNavigationDto4.f609i) : null) == null || (i7 = this.c.f609i) == 0) {
                    i7 = f.frag_error_no_internet_connection_description;
                }
                int i10 = i7;
                ErrorNavigationDto errorNavigationDto5 = this.c;
                if (errorNavigationDto5 != null) {
                    num = Integer.valueOf(errorNavigationDto5.f610j);
                }
                if (num == null || (i8 = this.c.f610j) == 0) {
                    i8 = f.frag_error_no_internet_connection_btn;
                }
                ErrorNavigationDto errorNavigationDto6 = this.c;
                boolean z = errorNavigationDto6 != null ? errorNavigationDto6.f614n : false;
                ErrorNavigationDto errorNavigationDto7 = this.c;
                boolean z2 = errorNavigationDto7 != null ? errorNavigationDto7.f616p : false;
                ErrorNavigationDto errorNavigationDto8 = this.c;
                boolean z3 = errorNavigationDto8 != null ? errorNavigationDto8.f617q : false;
                ErrorNavigationDto errorNavigationDto9 = this.c;
                if (errorNavigationDto9 == null || (parcelable3 = errorNavigationDto9.f618r) == null) {
                    parcelable3 = this.f584e;
                }
                Parcelable parcelable5 = parcelable3;
                ErrorNavigationDto errorNavigationDto10 = this.c;
                if (errorNavigationDto10 == null || (parcelable4 = errorNavigationDto10.f619s) == null) {
                    parcelable4 = this.f585f;
                }
                Parcelable parcelable6 = parcelable4;
                ErrorNavigationDto errorNavigationDto11 = r6;
                ErrorNavigationDto errorNavigationDto12 = new ErrorNavigationDto(str7, str5, null, null, null, null, i9, i10, i8, 0, true, false, z, false, z2, z3, parcelable5, parcelable6, 572);
                iFragmentCoordinator.a(errorNavigationDto11);
                bVar = new BaseMviVm0.b(this.d, c.c(BaseEvents2.a));
            } else {
                IFragmentCoordinator iFragmentCoordinator2 = this.a.f586i;
                ErrorNavigationDto errorNavigationDto13 = this.c;
                if (errorNavigationDto13 == null || (str = errorNavigationDto13.b) == null) {
                    str = this.d.getName();
                    Intrinsics.a((Object) str, "resultClass.name");
                }
                String str8 = str;
                ErrorNavigationDto errorNavigationDto14 = this.c;
                if (errorNavigationDto14 == null || (str3 = errorNavigationDto14.c) == null) {
                    str2 = "";
                } else {
                    str2 = str3;
                }
                ErrorNavigationDto errorNavigationDto15 = this.c;
                String str9 = errorNavigationDto15 != null ? errorNavigationDto15.d : null;
                ErrorNavigationDto errorNavigationDto16 = this.c;
                String str10 = errorNavigationDto16 != null ? errorNavigationDto16.f607e : null;
                ErrorNavigationDto errorNavigationDto17 = this.c;
                String str11 = errorNavigationDto17 != null ? errorNavigationDto17.f608f : null;
                ErrorNavigationDto errorNavigationDto18 = this.c;
                String str12 = errorNavigationDto18 != null ? errorNavigationDto18.g : null;
                ErrorNavigationDto errorNavigationDto19 = this.c;
                if ((errorNavigationDto19 != null ? Integer.valueOf(errorNavigationDto19.h) : null) == null || (i2 = this.c.h) == 0) {
                    i2 = f.frag_error_unexpected_error_title;
                }
                int i11 = i2;
                ErrorNavigationDto errorNavigationDto20 = this.c;
                if ((errorNavigationDto20 != null ? Integer.valueOf(errorNavigationDto20.f609i) : null) == null || (i3 = this.c.f609i) == 0) {
                    i3 = f.frag_error_unexpected_error_description;
                }
                int i12 = i3;
                ErrorNavigationDto errorNavigationDto21 = this.c;
                if ((errorNavigationDto21 != null ? Integer.valueOf(errorNavigationDto21.f610j) : null) == null || (i4 = this.c.f610j) == 0) {
                    i4 = f.frag_error_no_internet_connection_btn;
                }
                ErrorNavigationDto errorNavigationDto22 = this.c;
                if (errorNavigationDto22 != null) {
                    num = Integer.valueOf(errorNavigationDto22.f611k);
                }
                int i13 = (num == null || (i5 = this.c.f611k) == 0) ? 0 : i5;
                ErrorNavigationDto errorNavigationDto23 = this.c;
                boolean z4 = errorNavigationDto23 != null ? errorNavigationDto23.f612l : false;
                ErrorNavigationDto errorNavigationDto24 = this.c;
                boolean z5 = errorNavigationDto24 != null ? errorNavigationDto24.f613m : false;
                ErrorNavigationDto errorNavigationDto25 = this.c;
                boolean z6 = errorNavigationDto25 != null ? errorNavigationDto25.f614n : false;
                ErrorNavigationDto errorNavigationDto26 = this.c;
                boolean z7 = errorNavigationDto26 != null ? errorNavigationDto26.f615o : false;
                ErrorNavigationDto errorNavigationDto27 = this.c;
                boolean z8 = errorNavigationDto27 != null ? errorNavigationDto27.f616p : false;
                ErrorNavigationDto errorNavigationDto28 = this.c;
                boolean z9 = errorNavigationDto28 != null ? errorNavigationDto28.f617q : false;
                ErrorNavigationDto errorNavigationDto29 = this.c;
                if (errorNavigationDto29 == null || (parcelable = errorNavigationDto29.f618r) == null) {
                    parcelable = this.f584e;
                }
                Parcelable parcelable7 = parcelable;
                ErrorNavigationDto errorNavigationDto30 = this.c;
                if (errorNavigationDto30 == null || (parcelable2 = errorNavigationDto30.f619s) == null) {
                    parcelable2 = this.f585f;
                }
                Parcelable parcelable8 = parcelable2;
                ErrorNavigationDto errorNavigationDto31 = r6;
                ErrorNavigationDto errorNavigationDto32 = new ErrorNavigationDto(str8, str2, str9, str10, str11, str12, i11, i12, i4, i13, z4, z5, z6, z7, z8, z9, parcelable7, parcelable8);
                iFragmentCoordinator2.a(errorNavigationDto31);
                bVar = new BaseMviVm0.b(this.d, c.c(BaseEvents0.a));
            }
            return bVar;
        }
        Intrinsics.a(CrashlyticsController.EVENT_TYPE_LOGGED);
        throw null;
    }
}
