package e.a.a.a.e;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import e.a.a.a.e.p.DeeplinkDto;
import e.b.a.g.a.SupportAppScreen;

/* compiled from: BaseScreens.kt */
public abstract class BaseScreens1 extends SupportAppScreen {
    public final Parcelable c;
    public final DeeplinkDto d;

    public BaseScreens1() {
        this(null, null, 3);
    }

    public /* synthetic */ BaseScreens1(Parcelable parcelable, DeeplinkDto deeplinkDto, int i2) {
        parcelable = (i2 & 1) != 0 ? null : parcelable;
        deeplinkDto = (i2 & 2) != 0 ? null : deeplinkDto;
        this.c = parcelable;
        this.d = deeplinkDto;
    }

    public Intent a(Context context) {
        Intent intent = new Intent(context, a());
        Parcelable parcelable = this.c;
        if (parcelable != null) {
            intent.putExtra("EXTRA_SCREEN_DATA", parcelable);
        }
        DeeplinkDto deeplinkDto = this.d;
        if (deeplinkDto != null) {
            intent.putExtra("EXTRA_SCREEN_DEEPLINK", deeplinkDto);
        }
        return intent;
    }

    public abstract Class<?> a();
}
