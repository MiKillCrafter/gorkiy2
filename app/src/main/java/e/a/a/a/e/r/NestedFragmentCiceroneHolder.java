package e.a.a.a.e.r;

import e.a.a.a.e.s.NestedFragmentRouter;
import e.a.a.a.e.s.d;
import e.b.a.Router;

/* compiled from: NestedFragmentCiceroneHolder.kt */
public final class NestedFragmentCiceroneHolder extends BaseFragmentCiceroneHolder<d> {
    public final NestedFragmentRouter b = new NestedFragmentRouter();

    public Router a() {
        return this.b;
    }
}
