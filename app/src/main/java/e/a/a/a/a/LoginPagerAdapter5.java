package e.a.a.a.a;

import android.view.View;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import e.a.a.d;
import e.a.a.f;
import n.Unit;
import n.g;
import n.n.b.Functions0;
import n.n.c.Intrinsics;
import n.n.c.j;

/* compiled from: LoginPagerAdapter.kt */
public final class LoginPagerAdapter5 extends j implements Functions0<Boolean, g> {
    public final /* synthetic */ TextInputEditText c;
    public final /* synthetic */ View d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LoginPagerAdapter5(TextInputEditText textInputEditText, LoginPagerAdapter1 loginPagerAdapter1, View view) {
        super(1);
        this.c = textInputEditText;
        this.d = view;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [com.google.android.material.textfield.TextInputLayout, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Object a(Object obj) {
        boolean booleanValue = ((Boolean) obj).booleanValue();
        View view = this.d;
        Intrinsics.a((Object) view, "layout");
        TextInputLayout textInputLayout = (TextInputLayout) view.findViewById(d.frag_auth_login_til_login);
        Intrinsics.a((Object) textInputLayout, "layout.frag_auth_login_til_login");
        textInputLayout.setError(!booleanValue ? this.c.getContext().getString(f.frag_auth_login_failed) : null);
        return Unit.a;
    }
}
