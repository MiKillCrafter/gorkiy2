package e.a.a.a.c.c;

import e.a.a.a.c.c.BottomSheetSelectorDialogViewState0;
import e.a.a.a.c.c.BottomSheetSelectorDialogViewState1;
import e.a.a.a.c.c.c;
import e.a.a.a.e.p.BottomSheetSelectorNavigationDto;
import e.a.a.a.e.q.IFragmentCoordinator;
import e.c.c.BaseMviVm;
import e.c.c.BaseMviVm0;
import e.c.c.BaseMviVm1;
import e.c.c.BaseViewState1;
import e.c.c.k;
import java.util.List;
import l.b.Observable;
import l.b.t.Function;
import n.Unit;
import n.g;
import n.i.Collections2;
import n.n.b.Functions0;
import n.n.c.Intrinsics;
import n.n.c.j;

/* compiled from: BottomSheetSelectorDialogVm.kt */
public final class BottomSheetSelectorDialogVm extends BaseMviVm1<e> {

    /* renamed from: i  reason: collision with root package name */
    public final IFragmentCoordinator f599i;

    /* compiled from: BottomSheetSelectorDialogVm.kt */
    public static final class a<T, R> implements Function<T, R> {
        public static final a a = new a();

        public Object a(Object obj) {
            BottomSheetSelectorDialogViewState0.b bVar = (BottomSheetSelectorDialogViewState0.b) obj;
            if (bVar != null) {
                return new BaseMviVm0.a(new BottomSheetSelectorDialogViewState1.a(bVar.a, bVar.b));
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: BottomSheetSelectorDialogVm.kt */
    public static final class b extends j implements Functions0<c.a, g> {
        public final /* synthetic */ BottomSheetSelectorDialogVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(BottomSheetSelectorDialogVm bottomSheetSelectorDialogVm) {
            super(1);
            this.c = bottomSheetSelectorDialogVm;
        }

        public Object a(Object obj) {
            if (((BottomSheetSelectorDialogViewState0.a) obj) != null) {
                this.c.f599i.a((BottomSheetSelectorNavigationDto) null);
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: BottomSheetSelectorDialogVm.kt */
    public static final class c extends j implements Functions0<c.c, g> {
        public final /* synthetic */ BottomSheetSelectorDialogVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(BottomSheetSelectorDialogVm bottomSheetSelectorDialogVm) {
            super(1);
            this.c = bottomSheetSelectorDialogVm;
        }

        public Object a(Object obj) {
            BottomSheetSelectorDialogViewState0.c cVar = (BottomSheetSelectorDialogViewState0.c) obj;
            if (cVar != null) {
                this.c.f599i.a(cVar.a);
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    public BottomSheetSelectorDialogVm(IFragmentCoordinator iFragmentCoordinator) {
        if (iFragmentCoordinator != null) {
            this.f599i = iFragmentCoordinator;
        } else {
            Intrinsics.a("fragmentCoordinator");
            throw null;
        }
    }

    public Object a(Object obj, BaseViewState1 baseViewState1) {
        BottomSheetSelectorDialogViewState bottomSheetSelectorDialogViewState = (BottomSheetSelectorDialogViewState) obj;
        if (bottomSheetSelectorDialogViewState == null) {
            Intrinsics.a("vs");
            throw null;
        } else if (baseViewState1 == null) {
            Intrinsics.a("result");
            throw null;
        } else if (baseViewState1 instanceof BottomSheetSelectorDialogViewState1.a) {
            BottomSheetSelectorDialogViewState1.a aVar = (BottomSheetSelectorDialogViewState1.a) baseViewState1;
            String str = aVar.a;
            List<e.a.a.a.e.p.a> list = aVar.b;
            if (str == null) {
                Intrinsics.a("title");
                throw null;
            } else if (list != null) {
                return new BottomSheetSelectorDialogViewState(str, list);
            } else {
                Intrinsics.a("items");
                throw null;
            }
        } else {
            super.a(bottomSheetSelectorDialogViewState, baseViewState1);
            return bottomSheetSelectorDialogViewState;
        }
    }

    public Object d() {
        return new BottomSheetSelectorDialogViewState("", Collections2.b);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<U>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<e.c.c.BaseMviVm0<? extends e.c.c.k>>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Observable<BaseMviVm0<? extends k>> a(Observable<? extends e.c.c.b> observable) {
        if (observable != null) {
            Observable<U> a2 = observable.a(BottomSheetSelectorDialogViewState0.b.class);
            Intrinsics.a((Object) a2, "ofType(R::class.java)");
            Observable<U> a3 = observable.a(BottomSheetSelectorDialogViewState0.a.class);
            Intrinsics.a((Object) a3, "ofType(R::class.java)");
            Observable<U> a4 = observable.a(BottomSheetSelectorDialogViewState0.c.class);
            Intrinsics.a((Object) a4, "ofType(R::class.java)");
            Observable<BaseMviVm0<? extends k>> a5 = Observable.a(super.a(observable), a2.c((Function) a.a), BaseMviVm.a(a3, new b(this)), BaseMviVm.a(a4, new c(this)));
            Intrinsics.a((Object) a5, "Observable.mergeArray(\n …)\n            }\n        )");
            return a5;
        }
        Intrinsics.a("o");
        throw null;
    }
}
