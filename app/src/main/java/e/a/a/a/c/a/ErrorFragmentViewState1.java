package e.a.a.a.c.a;

import com.crashlytics.android.core.CrashlyticsController;
import e.c.c.BaseViewState0;
import j.a.a.a.outline;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;

/* compiled from: ErrorFragmentViewState.kt */
public abstract class ErrorFragmentViewState1 extends BaseViewState0 {

    /* compiled from: ErrorFragmentViewState.kt */
    public static final class a extends ErrorFragmentViewState1 {
        public static final a a = new a();

        public a() {
            super(null);
        }
    }

    /* compiled from: ErrorFragmentViewState.kt */
    public static final class b extends ErrorFragmentViewState1 {
        public final ErrorFragmentViewState a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ErrorFragmentViewState errorFragmentViewState) {
            super(null);
            if (errorFragmentViewState != null) {
                this.a = errorFragmentViewState;
                return;
            }
            Intrinsics.a(CrashlyticsController.EVENT_TYPE_LOGGED);
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof b) && Intrinsics.a(this.a, ((b) obj).a);
            }
            return true;
        }

        public int hashCode() {
            ErrorFragmentViewState errorFragmentViewState = this.a;
            if (errorFragmentViewState != null) {
                return errorFragmentViewState.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder a2 = outline.a("InitEvent(error=");
            a2.append(this.a);
            a2.append(")");
            return a2.toString();
        }
    }

    /* compiled from: ErrorFragmentViewState.kt */
    public static final class c extends ErrorFragmentViewState1 {
        public static final c a = new c();

        public c() {
            super(null);
        }
    }

    public /* synthetic */ ErrorFragmentViewState1(DefaultConstructorMarker defaultConstructorMarker) {
    }
}
