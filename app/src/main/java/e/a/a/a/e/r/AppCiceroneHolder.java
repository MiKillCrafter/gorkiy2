package e.a.a.a.e.r;

import e.a.a.a.e.s.ActivityRouter;
import e.a.a.a.e.s.a;
import e.b.a.Cicerone;
import e.b.a.CommandBuffer;
import n.n.c.Intrinsics;

/* compiled from: AppCiceroneHolder.kt */
public final class AppCiceroneHolder {
    public final Cicerone<a> a;
    public final ActivityRouter b;

    public AppCiceroneHolder(ActivityRouter activityRouter) {
        if (activityRouter != null) {
            this.b = activityRouter;
            this.a = new Cicerone<>(activityRouter);
            return;
        }
        Intrinsics.a("router");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [e.b.a.Cicerone<e.a.a.a.e.s.a>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [e.b.a.CommandBuffer, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public final CommandBuffer a() {
        Cicerone<a> cicerone = this.a;
        Intrinsics.a((Object) cicerone, "cicerone");
        CommandBuffer commandBuffer = cicerone.a.a;
        Intrinsics.a((Object) commandBuffer, "cicerone.navigatorHolder");
        return commandBuffer;
    }
}
