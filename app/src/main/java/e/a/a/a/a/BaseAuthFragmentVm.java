package e.a.a.a.a;

import l.b.d;
import l.b.t.Function;
import n.n.c.Intrinsics;
import ru.covid19.core.data.network.model.SessionResponse;

/* compiled from: BaseAuthFragmentVm.kt */
public final class BaseAuthFragmentVm<T, R> implements Function<SessionResponse, d> {
    public final /* synthetic */ BaseAuthFragmentVm8 a;

    public BaseAuthFragmentVm(BaseAuthFragmentVm8 baseAuthFragmentVm8) {
        this.a = baseAuthFragmentVm8;
    }

    public Object a(Object obj) {
        if (((SessionResponse) obj) != null) {
            return this.a.d();
        }
        Intrinsics.a("it");
        throw null;
    }
}
