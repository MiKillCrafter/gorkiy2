package e.a.a.a.e.o;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.core.content.FileProvider;
import e.a.a.CoreApp;
import e.a.a.a.e.BaseScreens1;
import e.a.a.a.e.Commands;
import e.a.a.a.e.Commands0;
import e.a.a.a.e.Commands1;
import e.a.a.a.e.Commands2;
import e.a.a.a.e.Commands3;
import e.a.a.a.e.Commands4;
import e.a.a.a.e.Commands6;
import e.a.a.a.e.Commands8;
import e.a.a.f;
import e.b.a.Navigator;
import e.b.a.Screen;
import e.b.a.h.Back;
import e.b.a.h.Command;
import e.b.a.h.Forward;
import e.b.a.h.Replace;
import j.a.a.a.outline;
import java.io.File;
import java.util.Locale;
import kotlin.TypeCastException;
import n.g;
import n.n.b.Functions;
import n.n.b.Functions0;
import n.n.b.Functions1;
import n.n.c.Intrinsics;
import q.b.a.LocalDateTime;
import q.b.a.ZoneId;
import q.b.a.ZoneOffset;
import q.b.a.t.DateTimeFormatter;
import t.a.Timber;

/* compiled from: ActivityNavigator.kt */
public final class ActivityNavigator implements Navigator {
    public Uri a;
    public final Context b;
    public final Functions0<Intent, g> c;
    public final Functions1<Intent, Integer, g> d;

    /* renamed from: e  reason: collision with root package name */
    public final Functions<g> f605e;

    /* renamed from: f  reason: collision with root package name */
    public final Functions<g> f606f;
    public final Functions1<String, Object, g> g;

    /* JADX WARN: Type inference failed for: r3v0, types: [n.n.b.Functions0<android.content.Intent, n.g>, n.n.b.Functions0<? super android.content.Intent, n.g>] */
    /* JADX WARN: Type inference failed for: r4v0, types: [n.n.b.Functions1<? super android.content.Intent, ? super java.lang.Integer, n.g>, n.n.b.Functions1<android.content.Intent, java.lang.Integer, n.g>] */
    /* JADX WARN: Type inference failed for: r7v0, types: [n.n.b.Functions1<? super java.lang.String, java.lang.Object, n.g>, n.n.b.Functions1<java.lang.String, java.lang.Object, n.g>] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.net.Uri, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Unknown variable types count: 3 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ActivityNavigator(android.content.Context r2, n.n.b.Functions0<? super android.content.Intent, n.g> r3, n.n.b.Functions1<? super android.content.Intent, ? super java.lang.Integer, n.g> r4, n.n.b.Functions<n.g> r5, n.n.b.Functions<n.g> r6, n.n.b.Functions1<? super java.lang.String, java.lang.Object, n.g> r7) {
        /*
            r1 = this;
            r0 = 0
            if (r2 == 0) goto L_0x0044
            if (r3 == 0) goto L_0x003e
            if (r4 == 0) goto L_0x0038
            if (r5 == 0) goto L_0x0032
            if (r6 == 0) goto L_0x002c
            if (r7 == 0) goto L_0x0026
            r1.<init>()
            r1.b = r2
            r1.c = r3
            r1.d = r4
            r1.f605e = r5
            r1.f606f = r6
            r1.g = r7
            android.net.Uri r2 = android.net.Uri.EMPTY
            java.lang.String r3 = "Uri.EMPTY"
            n.n.c.Intrinsics.a(r2, r3)
            r1.a = r2
            return
        L_0x0026:
            java.lang.String r2 = "showMessageAction"
            n.n.c.Intrinsics.a(r2)
            throw r0
        L_0x002c:
            java.lang.String r2 = "hideAppAction"
            n.n.c.Intrinsics.a(r2)
            throw r0
        L_0x0032:
            java.lang.String r2 = "finishActivityAction"
            n.n.c.Intrinsics.a(r2)
            throw r0
        L_0x0038:
            java.lang.String r2 = "openActivityForResultAction"
            n.n.c.Intrinsics.a(r2)
            throw r0
        L_0x003e:
            java.lang.String r2 = "openActivityAction"
            n.n.c.Intrinsics.a(r2)
            throw r0
        L_0x0044:
            java.lang.String r2 = "context"
            n.n.c.Intrinsics.a(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: e.a.a.a.e.o.ActivityNavigator.<init>(android.content.Context, n.n.b.Functions0, n.n.b.Functions1, n.n.b.Functions, n.n.b.Functions, n.n.b.Functions1):void");
    }

    /* JADX WARN: Type inference failed for: r8v0, types: [q.b.a.ZoneId, q.b.a.ZoneOffset] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [q.b.a.LocalDateTime, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Locale, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.net.Uri, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void a(Command[] commandArr) {
        if (commandArr != null) {
            for (Command command : commandArr) {
                if (command instanceof Forward) {
                    Screen screen = ((Forward) command).a;
                    if (screen != null) {
                        BaseScreens1 baseScreens1 = (BaseScreens1) screen;
                        a(baseScreens1.a(this.b), baseScreens1);
                    }
                } else if (command instanceof Replace) {
                    Screen screen2 = ((Replace) command).a;
                    if (screen2 != null) {
                        BaseScreens1 baseScreens12 = (BaseScreens1) screen2;
                        a(baseScreens12.a(this.b), baseScreens12);
                        this.f605e.b();
                    }
                } else if (command instanceof Back) {
                    this.f605e.b();
                } else if (command instanceof Commands6) {
                    BaseScreens1 baseScreens13 = ((Commands6) command).a;
                    Intent a2 = baseScreens13.a(this.b);
                    a2.setFlags(268468224);
                    a(a2, baseScreens13);
                    Context applicationContext = this.b.getApplicationContext();
                    if (applicationContext != null) {
                        ((CoreApp) applicationContext).a();
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type ru.covid19.core.CoreApp");
                    }
                } else if (command instanceof Commands) {
                    this.f606f.b();
                } else if (command instanceof Commands8) {
                    Commands8 commands8 = (Commands8) command;
                    this.g.a(commands8.a, commands8.b);
                } else if (command instanceof Commands4) {
                    if (((Commands4) command) != null) {
                        throw null;
                    }
                    throw null;
                } else if (command instanceof Commands1) {
                    if (((Commands1) command) != null) {
                        Intent intent = new Intent((String) null);
                        if (intent.resolveActivity(this.b.getPackageManager()) != null) {
                            this.d.a(intent, 0);
                        } else {
                            throw new ActivityNotFoundException("Couldn't find activity for activity null");
                        }
                    } else {
                        throw null;
                    }
                } else if (command instanceof Commands2) {
                    try {
                        this.c.a(new Intent("android.intent.action.VIEW", ((Commands2) command).a));
                    } catch (ActivityNotFoundException e2) {
                        Timber.d.a(e2);
                    }
                } else if (command instanceof Commands3) {
                    try {
                        this.c.a(new Intent("android.settings.NFC_SETTINGS"));
                    } catch (ActivityNotFoundException e3) {
                        Timber.d.a(e3);
                    }
                } else if (command instanceof Commands0) {
                    boolean z = ((Commands0) command).a;
                    Intent intent2 = new Intent("android.media.action.IMAGE_CAPTURE");
                    Context context = this.b;
                    if (context != null) {
                        File cacheDir = context.getCacheDir();
                        StringBuilder sb = new StringBuilder();
                        LocalDateTime b2 = LocalDateTime.b((ZoneId) ZoneOffset.g);
                        Intrinsics.a((Object) b2, "LocalDateTime.now(ZoneOffset.UTC)");
                        Locale locale = Locale.getDefault();
                        Intrinsics.a((Object) locale, "Locale.getDefault()");
                        String a3 = b2.a(DateTimeFormatter.a("yyyy-MM-dd-HH-mm-ss-SSS", locale));
                        Intrinsics.a((Object) a3, "this.format(formatter)");
                        sb.append(a3);
                        sb.append(".jpg");
                        File file = new File(cacheDir, sb.toString());
                        Context context2 = this.b;
                        Uri a4 = FileProvider.a(context2, context2.getString(f.authority_file_provider)).a(file);
                        Intrinsics.a((Object) a4, "FileProvider.getUriForFi…   file\n                )");
                        this.a = a4;
                        if (z) {
                            intent2.putExtra("android.intent.extras.CAMERA_FACING", 1);
                        }
                        intent2.putExtra("output", this.a);
                        intent2.addFlags(1);
                        try {
                            this.d.a(intent2, 1);
                        } catch (ActivityNotFoundException e4) {
                            Timber.d.a(e4, "Error open camera app", new Object[0]);
                        }
                    } else {
                        Intrinsics.a("context");
                        throw null;
                    }
                } else {
                    continue;
                }
            }
        }
    }

    public final void a(Intent intent, BaseScreens1 baseScreens1) {
        if (intent.resolveActivity(this.b.getPackageManager()) != null) {
            this.c.a(intent);
            return;
        }
        StringBuilder a2 = outline.a("Couldn't find activity for tag ");
        a2.append(baseScreens1.b);
        throw new ActivityNotFoundException(a2.toString());
    }
}
