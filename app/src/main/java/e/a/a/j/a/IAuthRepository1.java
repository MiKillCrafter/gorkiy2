package e.a.a.j.a;

import l.b.t.Function;
import n.n.c.Intrinsics;
import ru.covid19.core.data.network.model.SessionResponse;

/* compiled from: IAuthRepository.kt */
public final class IAuthRepository1<T, R> implements Function<T, R> {
    public final /* synthetic */ IAuthRepository a;
    public final /* synthetic */ boolean b;

    public IAuthRepository1(IAuthRepository iAuthRepository, boolean z) {
        this.a = iAuthRepository;
        this.b = z;
    }

    public Object a(Object obj) {
        SessionResponse sessionResponse = (SessionResponse) obj;
        if (sessionResponse != null) {
            IAuthRepository.a(this.a, sessionResponse, this.b);
            return sessionResponse;
        }
        Intrinsics.a("sessionResp");
        throw null;
    }
}
