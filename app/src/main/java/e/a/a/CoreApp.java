package e.a.a;

import android.app.Application;
import e.a.a.i.CoreComponentsHolder;
import e.a.a.i.b.AppComponent;
import e.a.a.i.b.CoreComponent;
import e.a.a.i.b.CoreComponent0;
import e.a.a.i.b.CoreComponent1;
import e.a.a.i.b.CoreComponent2;
import e.a.a.i.b.DaggerAppComponent;
import e.a.a.i.b.DaggerCoreComponent;
import e.a.a.i.c.AppModule;
import e.a.a.i.c.AuthModule;
import e.a.a.i.c.CipherModule;
import e.a.a.i.c.CoreNavigationModule;
import e.a.a.i.c.CoreStorageModule;
import e.a.a.i.c.NetworkModule;
import e.a.a.i.c.m0.DevRepositoryModule;
import e.a.a.l.IAppLifecycle;
import e.a.b.f.a.DaggerAppDatabaseComponent;
import e.a.b.f.a.DaggerAppDeeplinkConfigsComponent;
import e.a.b.f.a.DaggerAppNavigationComponent;
import e.a.b.f.c.AppDatabaseModule;
import e.a.b.f.c.AppDeeplinkConfigsModule;
import e.a.b.f.c.AppNavigationModule;
import j.c.a.a.c.n.c;
import j.e.c.AndroidThreeTen;
import j.e.c.AssetsZoneRulesInitializer;
import n.n.c.Intrinsics;
import q.b.a.w.ZoneRulesInitializer;

/* compiled from: CoreApp.kt */
public abstract class CoreApp extends Application {
    public IAppLifecycle b;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [e.a.b.f.a.DaggerAppNavigationComponent, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [e.a.b.f.a.DaggerAppDatabaseComponent, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [e.a.b.f.a.DaggerAppDeeplinkConfigsComponent, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [e.a.a.i.b.DaggerCoreComponent, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public final void a() {
        Class<AppComponent> cls = AppComponent.class;
        c.a(this, Application.class);
        CoreComponentsHolder.a = new DaggerAppComponent(new CipherModule(), new AppModule(), super, null);
        DaggerAppNavigationComponent daggerAppNavigationComponent = new DaggerAppNavigationComponent(new AppNavigationModule(), null);
        Intrinsics.a((Object) daggerAppNavigationComponent, "DaggerAppNavigationCompo…er()\n            .build()");
        CoreComponentsHolder.c = daggerAppNavigationComponent;
        AppComponent appComponent = CoreComponentsHolder.a;
        if (appComponent != null) {
            AppDatabaseModule appDatabaseModule = new AppDatabaseModule();
            c.a(appComponent, cls);
            DaggerAppDatabaseComponent daggerAppDatabaseComponent = new DaggerAppDatabaseComponent(appDatabaseModule, appComponent, null);
            Intrinsics.a((Object) daggerAppDatabaseComponent, "DaggerAppDatabaseCompone…ent)\n            .build()");
            CoreComponentsHolder.d = daggerAppDatabaseComponent;
            DaggerAppDeeplinkConfigsComponent daggerAppDeeplinkConfigsComponent = new DaggerAppDeeplinkConfigsComponent(new AppDeeplinkConfigsModule(), null);
            Intrinsics.a((Object) daggerAppDeeplinkConfigsComponent, "DaggerAppDeeplinkConfigs…er()\n            .build()");
            CoreComponentsHolder.f624e = daggerAppDeeplinkConfigsComponent;
            AppComponent appComponent2 = CoreComponentsHolder.a;
            if (appComponent2 != null) {
                CoreComponent2 coreComponent2 = CoreComponentsHolder.c;
                if (coreComponent2 != null) {
                    CoreComponent0 coreComponent0 = CoreComponentsHolder.d;
                    if (coreComponent0 != null) {
                        CoreComponent1 coreComponent1 = CoreComponentsHolder.f624e;
                        if (coreComponent1 != null) {
                            NetworkModule networkModule = new NetworkModule();
                            CoreNavigationModule coreNavigationModule = new CoreNavigationModule();
                            CoreStorageModule coreStorageModule = new CoreStorageModule();
                            AuthModule authModule = new AuthModule();
                            DevRepositoryModule devRepositoryModule = new DevRepositoryModule();
                            c.a(appComponent2, cls);
                            c.a(coreComponent2, CoreComponent2.class);
                            c.a(coreComponent0, CoreComponent0.class);
                            c.a(coreComponent1, CoreComponent1.class);
                            DaggerCoreComponent daggerCoreComponent = new DaggerCoreComponent(networkModule, coreNavigationModule, coreStorageModule, authModule, devRepositoryModule, appComponent2, coreComponent2, coreComponent0, coreComponent1, null);
                            Intrinsics.a((Object) daggerCoreComponent, "DaggerCoreComponent.buil…ent)\n            .build()");
                            CoreComponentsHolder.b = daggerCoreComponent;
                            return;
                        }
                        Intrinsics.b("appDeeplinkConfigsComponent");
                        throw null;
                    }
                    Intrinsics.b("appDatabaseComponent");
                    throw null;
                }
                Intrinsics.b("appNavigationComponent");
                throw null;
            }
            Intrinsics.b("appComponent");
            throw null;
        }
        Intrinsics.b("appComponent");
        throw null;
    }

    public void onCreate() {
        super.onCreate();
        if (!AndroidThreeTen.a.getAndSet(true)) {
            AssetsZoneRulesInitializer assetsZoneRulesInitializer = new AssetsZoneRulesInitializer(this, "org/threeten/bp/TZDB.dat");
            if (ZoneRulesInitializer.a.get()) {
                throw new IllegalStateException("Already initialized");
            } else if (!ZoneRulesInitializer.b.compareAndSet(null, assetsZoneRulesInitializer)) {
                throw new IllegalStateException("Initializer was already set, possibly with a default during initialization");
            }
        }
        a();
        CoreComponent coreComponent = CoreComponentsHolder.b;
        if (coreComponent != null) {
            coreComponent.a(this);
            IAppLifecycle iAppLifecycle = this.b;
            if (iAppLifecycle != null) {
                registerActivityLifecycleCallbacks(iAppLifecycle);
            } else {
                Intrinsics.b("appLifecycle");
                throw null;
            }
        } else {
            Intrinsics.b("coreComponent");
            throw null;
        }
    }

    public void onTerminate() {
        IAppLifecycle iAppLifecycle = this.b;
        if (iAppLifecycle != null) {
            unregisterActivityLifecycleCallbacks(iAppLifecycle);
            super.onTerminate();
            return;
        }
        Intrinsics.b("appLifecycle");
        throw null;
    }
}
