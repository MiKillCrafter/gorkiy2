package e.a.a.i.b;

import android.app.Application;
import android.content.Context;
import e.a.a.i.c.AppModule;
import e.a.a.i.c.AppModule_ProvideActivityCounterFactory;
import e.a.a.i.c.AppModule_ProvideCipherPreferencesWrapperFactory;
import e.a.a.i.c.AppModule_ProvideNfcAdapterFactory;
import e.a.a.i.c.CipherModule;
import e.a.a.i.c.d;
import e.a.a.i.c.f;
import e.a.a.i.c.g;
import e.a.a.i.c.n;
import e.a.a.i.c.o;
import e.a.a.l.IAppLifecycle;
import e.a.a.l.b;
import e.c.a.a.c;
import e.c.a.c.a.b.ISecuredPreferencesWrapper;
import k.a.DoubleCheck;
import k.a.InstanceFactory;
import m.a.Provider;

public final class DaggerAppComponent implements AppComponent {
    public Provider<Application> a;
    public Provider<Context> b;
    public Provider<b> c;
    public Provider<c> d;

    /* renamed from: e  reason: collision with root package name */
    public Provider<e.c.a.a.a> f626e;

    /* renamed from: f  reason: collision with root package name */
    public Provider<e.c.a.c.a.b.b> f627f;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
     arg types: [android.app.Application, java.lang.String]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(android.view.View, int):int
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
    public /* synthetic */ DaggerAppComponent(CipherModule cipherModule, AppModule appModule, Application application, a aVar) {
        j.c.a.a.c.n.c.a((Object) application, "instance cannot be null");
        InstanceFactory instanceFactory = new InstanceFactory(application);
        this.a = instanceFactory;
        this.b = DoubleCheck.a(new d(appModule, instanceFactory));
        this.c = DoubleCheck.a(new AppModule_ProvideActivityCounterFactory(appModule));
        DoubleCheck.a(new g(appModule));
        DoubleCheck.a(new f(appModule, this.b));
        DoubleCheck.a(new AppModule_ProvideNfcAdapterFactory(appModule, this.b));
        Provider<c> a2 = DoubleCheck.a(new o(cipherModule, this.b));
        this.d = a2;
        Provider<e.c.a.a.a> a3 = DoubleCheck.a(new n(cipherModule, a2));
        this.f626e = a3;
        this.f627f = DoubleCheck.a(new AppModule_ProvideCipherPreferencesWrapperFactory(appModule, this.b, a3));
    }

    public Context b() {
        return this.b.get();
    }

    public ISecuredPreferencesWrapper c() {
        return this.f627f.get();
    }

    public IAppLifecycle d() {
        return this.c.get();
    }
}
