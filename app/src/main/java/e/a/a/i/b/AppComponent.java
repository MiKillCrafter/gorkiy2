package e.a.a.i.b;

import android.content.Context;
import e.a.a.l.IAppLifecycle;
import e.c.a.c.a.b.ISecuredPreferencesWrapper;

/* compiled from: AppComponent.kt */
public interface AppComponent {
    Context b();

    ISecuredPreferencesWrapper c();

    IAppLifecycle d();
}
