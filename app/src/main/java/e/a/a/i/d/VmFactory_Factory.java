package e.a.a.i.d;

import i.o.v;
import java.util.Map;
import k.a.Factory;
import m.a.Provider;

public final class VmFactory_Factory implements Factory<a> {
    public final Provider<Map<Class<? extends v>, Provider<v>>> a;

    public VmFactory_Factory(Provider<Map<Class<? extends v>, Provider<v>>> provider) {
        this.a = provider;
    }

    public Object get() {
        return new VmFactory(this.a.get());
    }
}
