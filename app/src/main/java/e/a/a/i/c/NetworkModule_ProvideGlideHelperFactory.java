package e.a.a.i.c;

import e.a.a.g.b.b.a.IAuthPrefs;
import e.a.a.m.a.GlideHelper;
import e.a.a.m.a.b;
import e.c.b.j.IApiConfigRepository0;
import e.c.b.j.c;
import k.a.Factory;
import m.a.Provider;
import n.n.c.Intrinsics;

public final class NetworkModule_ProvideGlideHelperFactory implements Factory<b> {
    public final NetworkModule a;
    public final Provider<c> b;
    public final Provider<e.a.a.g.b.b.a.b> c;

    public NetworkModule_ProvideGlideHelperFactory(y yVar, Provider<c> provider, Provider<e.a.a.g.b.b.a.b> provider2) {
        this.a = yVar;
        this.b = provider;
        this.c = provider2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.a.m.a.GlideHelper, java.lang.String]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(android.view.View, int):int
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
    public Object get() {
        NetworkModule networkModule = this.a;
        IApiConfigRepository0 iApiConfigRepository0 = this.b.get();
        IAuthPrefs iAuthPrefs = this.c.get();
        if (networkModule == null) {
            throw null;
        } else if (iApiConfigRepository0 == null) {
            Intrinsics.a("apiConfigRepository");
            throw null;
        } else if (iAuthPrefs != null) {
            GlideHelper glideHelper = new GlideHelper(iApiConfigRepository0, iAuthPrefs);
            j.c.a.a.c.n.c.a((Object) glideHelper, "Cannot return null from a non-@Nullable @Provides method");
            return glideHelper;
        } else {
            Intrinsics.a("authPrefs");
            throw null;
        }
    }
}
