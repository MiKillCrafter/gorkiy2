package e.a.a.k;

import e.a.a.g.a.ClientErrorResponseDto;
import j.a.a.a.outline;
import n.n.c.Intrinsics;

/* compiled from: RxExtensions.kt */
public final class RxExtensions extends Throwable {
    public final ClientErrorResponseDto b;

    public RxExtensions(ClientErrorResponseDto clientErrorResponseDto) {
        if (clientErrorResponseDto != null) {
            this.b = clientErrorResponseDto;
        } else {
            Intrinsics.a("errorDto");
            throw null;
        }
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof RxExtensions) && Intrinsics.a(this.b, ((RxExtensions) obj).b);
        }
        return true;
    }

    public int hashCode() {
        ClientErrorResponseDto clientErrorResponseDto = this.b;
        if (clientErrorResponseDto != null) {
            return clientErrorResponseDto.hashCode();
        }
        return 0;
    }

    public String toString() {
        StringBuilder a = outline.a("ClientErrorResponseException(errorDto=");
        a.append(this.b);
        a.append(")");
        return a.toString();
    }
}
