package e.a.b.h.b;

import n.Unit;
import n.g;
import n.n.b.Functions;
import n.n.c.Reflection;
import n.n.c.h;
import n.p.KDeclarationContainer;

/* compiled from: MainActivity.kt */
public final /* synthetic */ class MainActivity extends h implements Functions<g> {
    public MainActivity(MainActivityVm mainActivityVm) {
        super(0, mainActivityVm);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [n.n.c.CallableReference, e.a.b.h.b.MainActivity] */
    public Object b() {
        ((MainActivityVm) this.c).d.b();
        return Unit.a;
    }

    public final String f() {
        return "backActivityAction";
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [n.p.KClass, n.p.KDeclarationContainer] */
    public final KDeclarationContainer g() {
        return Reflection.a(MainActivityVm.class);
    }

    public final String i() {
        return "backActivityAction()V";
    }
}
