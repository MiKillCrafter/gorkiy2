package e.a.b.h.b.h.l;

import e.a.a.a.e.p.BottomSheetSelectorNavigationDto0;
import e.c.c.BaseViewState0;
import j.a.a.a.outline;
import java.util.List;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;
import ru.covid19.core.data.network.model.CitizenshipResponse;
import ru.covid19.droid.data.model.profileData.Document0;
import ru.covid19.droid.data.model.profileData.ProfileData0;

/* compiled from: ProfileFillingAddChildFragmentViewState.kt */
public abstract class ProfileFillingAddChildFragmentViewState0 extends BaseViewState0 {

    /* compiled from: ProfileFillingAddChildFragmentViewState.kt */
    public static final class a extends ProfileFillingAddChildFragmentViewState0 {
        public final CitizenshipResponse a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(CitizenshipResponse citizenshipResponse) {
            super(null);
            if (citizenshipResponse != null) {
                this.a = citizenshipResponse;
                return;
            }
            Intrinsics.a("citizenship");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof a) && Intrinsics.a(this.a, ((a) obj).a);
            }
            return true;
        }

        public int hashCode() {
            CitizenshipResponse citizenshipResponse = this.a;
            if (citizenshipResponse != null) {
                return citizenshipResponse.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder a2 = outline.a("CitizenshipSelectedEvent(citizenship=");
            a2.append(this.a);
            a2.append(")");
            return a2.toString();
        }
    }

    /* compiled from: ProfileFillingAddChildFragmentViewState.kt */
    public static final class b extends ProfileFillingAddChildFragmentViewState0 {
        public final Document0 a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(Document0 document0) {
            super(null);
            if (document0 != null) {
                this.a = document0;
                return;
            }
            Intrinsics.a("documentType");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof b) && Intrinsics.a(this.a, ((b) obj).a);
            }
            return true;
        }

        public int hashCode() {
            Document0 document0 = this.a;
            if (document0 != null) {
                return document0.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder a2 = outline.a("DocumentTypeSelected(documentType=");
            a2.append(this.a);
            a2.append(")");
            return a2.toString();
        }
    }

    /* compiled from: ProfileFillingAddChildFragmentViewState.kt */
    public static final class c extends ProfileFillingAddChildFragmentViewState0 {
        public final ProfileData0 a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ProfileData0 profileData0) {
            super(null);
            if (profileData0 != null) {
                this.a = profileData0;
                return;
            }
            Intrinsics.a("gender");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof c) && Intrinsics.a(this.a, ((c) obj).a);
            }
            return true;
        }

        public int hashCode() {
            ProfileData0 profileData0 = this.a;
            if (profileData0 != null) {
                return profileData0.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder a2 = outline.a("GenderSelectedEvent(gender=");
            a2.append(this.a);
            a2.append(")");
            return a2.toString();
        }
    }

    /* compiled from: ProfileFillingAddChildFragmentViewState.kt */
    public static final class d extends ProfileFillingAddChildFragmentViewState0 {
        public static final d a = new d();

        public d() {
            super(null);
        }
    }

    /* compiled from: ProfileFillingAddChildFragmentViewState.kt */
    public static final class e extends ProfileFillingAddChildFragmentViewState0 {
        public final Boolean a;

        public e(Boolean bool) {
            super(null);
            this.a = bool;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof e) && Intrinsics.a(this.a, ((e) obj).a);
            }
            return true;
        }

        public int hashCode() {
            Boolean bool = this.a;
            if (bool != null) {
                return bool.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder a2 = outline.a("OnAddressMatchesChangedEvent(macthes=");
            a2.append(this.a);
            a2.append(")");
            return a2.toString();
        }
    }

    /* compiled from: ProfileFillingAddChildFragmentViewState.kt */
    public static final class f extends ProfileFillingAddChildFragmentViewState0 {
        public final String a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(String str) {
            super(null);
            if (str != null) {
                this.a = str;
                return;
            }
            Intrinsics.a("date");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof f) && Intrinsics.a(this.a, ((f) obj).a);
            }
            return true;
        }

        public int hashCode() {
            String str = this.a;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return outline.a(outline.a("OnBirthDateChangedEvent(date="), this.a, ")");
        }
    }

    /* compiled from: ProfileFillingAddChildFragmentViewState.kt */
    public static final class g extends ProfileFillingAddChildFragmentViewState0 {
        public final Boolean a;

        public g(Boolean bool) {
            super(null);
            this.a = bool;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof g) && Intrinsics.a(this.a, ((g) obj).a);
            }
            return true;
        }

        public int hashCode() {
            Boolean bool = this.a;
            if (bool != null) {
                return bool.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder a2 = outline.a("OnChildSymptomsChangedEvent(hasSymptoms=");
            a2.append(this.a);
            a2.append(")");
            return a2.toString();
        }
    }

    /* compiled from: ProfileFillingAddChildFragmentViewState.kt */
    public static final class h extends ProfileFillingAddChildFragmentViewState0 {
        public final String a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(String str) {
            super(null);
            if (str != null) {
                this.a = str;
                return;
            }
            Intrinsics.a("number");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof h) && Intrinsics.a(this.a, ((h) obj).a);
            }
            return true;
        }

        public int hashCode() {
            String str = this.a;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return outline.a(outline.a("OnDocumentNumberChangedEvent(number="), this.a, ")");
        }
    }

    /* compiled from: ProfileFillingAddChildFragmentViewState.kt */
    public static final class i extends ProfileFillingAddChildFragmentViewState0 {
        public final String a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(String str) {
            super(null);
            if (str != null) {
                this.a = str;
                return;
            }
            Intrinsics.a("series");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof i) && Intrinsics.a(this.a, ((i) obj).a);
            }
            return true;
        }

        public int hashCode() {
            String str = this.a;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return outline.a(outline.a("OnDocumentSeriesChangedEvent(series="), this.a, ")");
        }
    }

    /* compiled from: ProfileFillingAddChildFragmentViewState.kt */
    public static final class j extends ProfileFillingAddChildFragmentViewState0 {
        public final String a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(String str) {
            super(null);
            if (str != null) {
                this.a = str;
                return;
            }
            Intrinsics.a("address");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof j) && Intrinsics.a(this.a, ((j) obj).a);
            }
            return true;
        }

        public int hashCode() {
            String str = this.a;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return outline.a(outline.a("OnLivingPlaceChangedEvent(address="), this.a, ")");
        }
    }

    /* compiled from: ProfileFillingAddChildFragmentViewState.kt */
    public static final class k extends ProfileFillingAddChildFragmentViewState0 {
        public final String a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(String str) {
            super(null);
            if (str != null) {
                this.a = str;
                return;
            }
            Intrinsics.a("text");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof k) && Intrinsics.a(this.a, ((k) obj).a);
            }
            return true;
        }

        public int hashCode() {
            String str = this.a;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return outline.a(outline.a("OnNameChangedEvent(text="), this.a, ")");
        }
    }

    /* compiled from: ProfileFillingAddChildFragmentViewState.kt */
    public static final class l extends ProfileFillingAddChildFragmentViewState0 {
        public final String a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(String str) {
            super(null);
            if (str != null) {
                this.a = str;
                return;
            }
            Intrinsics.a("text");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof l) && Intrinsics.a(this.a, ((l) obj).a);
            }
            return true;
        }

        public int hashCode() {
            String str = this.a;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return outline.a(outline.a("OnPatronymicChangedEvent(text="), this.a, ")");
        }
    }

    /* compiled from: ProfileFillingAddChildFragmentViewState.kt */
    public static final class m extends ProfileFillingAddChildFragmentViewState0 {
        public final String a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public m(String str) {
            super(null);
            if (str != null) {
                this.a = str;
                return;
            }
            Intrinsics.a("address");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof m) && Intrinsics.a(this.a, ((m) obj).a);
            }
            return true;
        }

        public int hashCode() {
            String str = this.a;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return outline.a(outline.a("OnRegistrationAddressChangedEvent(address="), this.a, ")");
        }
    }

    /* compiled from: ProfileFillingAddChildFragmentViewState.kt */
    public static final class n extends ProfileFillingAddChildFragmentViewState0 {
        public final String a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public n(String str) {
            super(null);
            if (str != null) {
                this.a = str;
                return;
            }
            Intrinsics.a("text");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof n) && Intrinsics.a(this.a, ((n) obj).a);
            }
            return true;
        }

        public int hashCode() {
            String str = this.a;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return outline.a(outline.a("OnSurnameChangedEvent(text="), this.a, ")");
        }
    }

    /* compiled from: ProfileFillingAddChildFragmentViewState.kt */
    public static final class o extends ProfileFillingAddChildFragmentViewState0 {
        public final BottomSheetSelectorNavigationDto0 a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public o(BottomSheetSelectorNavigationDto0 bottomSheetSelectorNavigationDto0) {
            super(null);
            if (bottomSheetSelectorNavigationDto0 != null) {
                this.a = bottomSheetSelectorNavigationDto0;
                return;
            }
            Intrinsics.a("dto");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof o) && Intrinsics.a(this.a, ((o) obj).a);
            }
            return true;
        }

        public int hashCode() {
            BottomSheetSelectorNavigationDto0 bottomSheetSelectorNavigationDto0 = this.a;
            if (bottomSheetSelectorNavigationDto0 != null) {
                return bottomSheetSelectorNavigationDto0.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder a2 = outline.a("SelectCitizenshipEvent(dto=");
            a2.append(this.a);
            a2.append(")");
            return a2.toString();
        }
    }

    /* compiled from: ProfileFillingAddChildFragmentViewState.kt */
    public static final class p extends ProfileFillingAddChildFragmentViewState0 {
        public final String a;
        public final List<e.a.a.a.e.p.a> b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public p(String str, List<? extends e.a.a.a.e.p.a> list) {
            super(null);
            if (str == null) {
                Intrinsics.a("title");
                throw null;
            } else if (list != null) {
                this.a = str;
                this.b = list;
            } else {
                Intrinsics.a("items");
                throw null;
            }
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof p)) {
                return false;
            }
            p pVar = (p) obj;
            return Intrinsics.a(this.a, pVar.a) && Intrinsics.a(this.b, pVar.b);
        }

        public int hashCode() {
            String str = this.a;
            int i2 = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            List<e.a.a.a.e.p.a> list = this.b;
            if (list != null) {
                i2 = list.hashCode();
            }
            return hashCode + i2;
        }

        public String toString() {
            StringBuilder a2 = outline.a("SelectDocumentTypeEvent(title=");
            a2.append(this.a);
            a2.append(", items=");
            a2.append(this.b);
            a2.append(")");
            return a2.toString();
        }
    }

    /* compiled from: ProfileFillingAddChildFragmentViewState.kt */
    public static final class q extends ProfileFillingAddChildFragmentViewState0 {
        public static final q a = new q();

        public q() {
            super(null);
        }
    }

    /* compiled from: ProfileFillingAddChildFragmentViewState.kt */
    public static final class r extends ProfileFillingAddChildFragmentViewState0 {
        public static final r a = new r();

        public r() {
            super(null);
        }
    }

    /* compiled from: ProfileFillingAddChildFragmentViewState.kt */
    public static final class s extends ProfileFillingAddChildFragmentViewState0 {
        public final String a;
        public final List<e.a.b.h.b.h.o.c> b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public s(String str, List<e.a.b.h.b.h.o.c> list) {
            super(null);
            if (str == null) {
                Intrinsics.a("title");
                throw null;
            } else if (list != null) {
                this.a = str;
                this.b = list;
            } else {
                Intrinsics.a("items");
                throw null;
            }
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof s)) {
                return false;
            }
            s sVar = (s) obj;
            return Intrinsics.a(this.a, sVar.a) && Intrinsics.a(this.b, sVar.b);
        }

        public int hashCode() {
            String str = this.a;
            int i2 = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            List<e.a.b.h.b.h.o.c> list = this.b;
            if (list != null) {
                i2 = list.hashCode();
            }
            return hashCode + i2;
        }

        public String toString() {
            StringBuilder a2 = outline.a("SymptomsAnswerClicked(title=");
            a2.append(this.a);
            a2.append(", items=");
            a2.append(this.b);
            a2.append(")");
            return a2.toString();
        }
    }

    public /* synthetic */ ProfileFillingAddChildFragmentViewState0(DefaultConstructorMarker defaultConstructorMarker) {
    }
}
