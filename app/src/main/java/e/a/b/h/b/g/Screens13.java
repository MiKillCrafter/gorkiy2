package e.a.b.h.b.g;

import androidx.fragment.app.Fragment;
import e.a.a.a.e.BaseScreens2;
import ru.covid19.droid.presentation.main.questionnaire.QuestionnaireContainerFragment;

/* compiled from: Screens.kt */
public final class Screens13 extends BaseScreens2 {
    public static final Screens13 d = new Screens13();

    public Screens13() {
        super(null, 1);
    }

    public Fragment a() {
        return new QuestionnaireContainerFragment();
    }
}
