package e.a.b.h.b.i.c.m;

import j.a.a.a.outline;

/* compiled from: InformedConfirmationFragmentViewState.kt */
public final class InformedConfirmationFragmentViewState {
    public final boolean a;
    public final boolean b;
    public final boolean c;

    public InformedConfirmationFragmentViewState() {
        this(false, false, false, 7);
    }

    public InformedConfirmationFragmentViewState(boolean z, boolean z2, boolean z3) {
        this.a = z;
        this.b = z2;
        this.c = z3;
    }

    public static /* synthetic */ InformedConfirmationFragmentViewState a(InformedConfirmationFragmentViewState informedConfirmationFragmentViewState, boolean z, boolean z2, boolean z3, int i2) {
        if ((i2 & 1) != 0) {
            z = informedConfirmationFragmentViewState.a;
        }
        if ((i2 & 2) != 0) {
            z2 = informedConfirmationFragmentViewState.b;
        }
        if ((i2 & 4) != 0) {
            z3 = informedConfirmationFragmentViewState.c;
        }
        if (informedConfirmationFragmentViewState != null) {
            return new InformedConfirmationFragmentViewState(z, z2, z3);
        }
        throw null;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof InformedConfirmationFragmentViewState)) {
            return false;
        }
        InformedConfirmationFragmentViewState informedConfirmationFragmentViewState = (InformedConfirmationFragmentViewState) obj;
        return this.a == informedConfirmationFragmentViewState.a && this.b == informedConfirmationFragmentViewState.b && this.c == informedConfirmationFragmentViewState.c;
    }

    public int hashCode() {
        boolean z = this.a;
        boolean z2 = true;
        if (z) {
            z = true;
        }
        int i2 = (z ? 1 : 0) * true;
        boolean z3 = this.b;
        if (z3) {
            z3 = true;
        }
        int i3 = (i2 + (z3 ? 1 : 0)) * 31;
        boolean z4 = this.c;
        if (!z4) {
            z2 = z4;
        }
        return i3 + (z2 ? 1 : 0);
    }

    public String toString() {
        StringBuilder a2 = outline.a("InformedConfirmationFragmentViewState(isSelfIsolationInformed=");
        a2.append(this.a);
        a2.append(", isQuarantineTerminationResponsibilityInformed=");
        a2.append(this.b);
        a2.append(", isAllInfoIsCorrectConfirmed=");
        return outline.a(a2, this.c, ")");
    }

    public /* synthetic */ InformedConfirmationFragmentViewState(boolean z, boolean z2, boolean z3, int i2) {
        z = (i2 & 1) != 0 ? false : z;
        z2 = (i2 & 2) != 0 ? false : z2;
        z3 = (i2 & 4) != 0 ? false : z3;
        this.a = z;
        this.b = z2;
        this.c = z3;
    }
}
