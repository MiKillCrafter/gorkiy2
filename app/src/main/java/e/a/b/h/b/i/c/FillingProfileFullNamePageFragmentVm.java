package e.a.b.h.b.i.c;

import android.net.Uri;
import e.a.a.a.b.BaseDpFragmentVm1;
import e.a.a.a.e.o.ITopLevelCoordinator;
import e.a.a.a.e.p.BottomSheetSelectorNavigationDto0;
import e.a.a.a.e.q.IFragmentCoordinator;
import e.a.a.g.b.b.a.IAuthPrefs;
import e.a.b.d.b.IProfileRepository;
import e.a.b.h.b.h.BaseStepValidatedFragmentVm;
import e.a.b.h.b.i.c.FillingProfileFullNamePageFragmentViewState1;
import e.a.b.h.b.i.c.FillingProfileFullNamePageFragmentViewState2;
import e.a.b.h.b.i.c.a;
import e.c.c.BaseMviVm;
import e.c.c.BaseMviVm0;
import e.c.c.BaseViewState1;
import j.e.b.BehaviorRelay;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import l.b.Observable;
import l.b.Single;
import l.b.s.Disposable;
import l.b.t.Action;
import l.b.t.Consumer;
import l.b.t.Function;
import l.b.u.b.Functions;
import l.b.u.b.ObjectHelper;
import l.b.u.e.c.ObservableDoOnLifecycle;
import n.Unit;
import n.i.Collections;
import n.n.b.Functions0;
import n.n.c.Intrinsics;
import n.n.c.Reflection;
import n.p.KDeclarationContainer;
import n.r.Indent;
import o.ResponseBody;
import q.b.a.ZoneId;
import ru.covid19.core.data.network.model.CitizenshipResponse;
import ru.covid19.core.data.network.model.CitizenshipResponse1;
import ru.covid19.core.data.network.model.CitizenshipsResponse;
import ru.covid19.droid.data.model.profileData.Passenger;
import ru.covid19.droid.data.model.profileData.ProfileData0;

/* compiled from: FillingProfileFullNamePageFragmentVm.kt */
public final class FillingProfileFullNamePageFragmentVm extends BaseStepValidatedFragmentVm<d> {

    /* renamed from: n  reason: collision with root package name */
    public final IFragmentCoordinator f703n;

    /* renamed from: o  reason: collision with root package name */
    public final ITopLevelCoordinator f704o;

    /* renamed from: p  reason: collision with root package name */
    public final IProfileRepository f705p;

    /* renamed from: q  reason: collision with root package name */
    public final IAuthPrefs f706q;

    /* compiled from: FillingProfileFullNamePageFragmentVm.kt */
    public static final class a<T> implements Consumer<CitizenshipsResponse> {
        public final /* synthetic */ String b;
        public final /* synthetic */ FillingProfileFullNamePageFragmentVm c;

        public a(String str, FillingProfileFullNamePageFragmentVm fillingProfileFullNamePageFragmentVm) {
            this.b = str;
            this.c = fillingProfileFullNamePageFragmentVm;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
         arg types: [java.lang.String, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
        public void a(Object obj) {
            T t2;
            Iterator<T> it = ((CitizenshipResponse1) obj).getCitizenshipsList().iterator();
            while (true) {
                if (!it.hasNext()) {
                    t2 = null;
                    break;
                }
                t2 = it.next();
                if (Intrinsics.a((Object) ((CitizenshipResponse) t2).getCode(), (Object) this.b)) {
                    break;
                }
            }
            CitizenshipResponse citizenshipResponse = (CitizenshipResponse) t2;
            if (citizenshipResponse != null) {
                this.c.f705p.f().getPassenger().setCitizenship(citizenshipResponse.getCode());
                this.c.f705p.f().getPassenger().setCitizenshipName(citizenshipResponse.getName());
            }
        }
    }

    /* compiled from: FillingProfileFullNamePageFragmentVm.kt */
    public static final class b<T> implements Consumer<Throwable> {
        public static final b b = new b();

        public void a(Object obj) {
            Throwable th = (Throwable) obj;
        }
    }

    /* compiled from: FillingProfileFullNamePageFragmentVm.kt */
    public static final class c extends n.n.c.j implements Functions0<a.l, n.g> {
        public final /* synthetic */ FillingProfileFullNamePageFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(FillingProfileFullNamePageFragmentVm fillingProfileFullNamePageFragmentVm) {
            super(1);
            this.c = fillingProfileFullNamePageFragmentVm;
        }

        public Object a(Object obj) {
            FillingProfileFullNamePageFragmentViewState1.l lVar = (FillingProfileFullNamePageFragmentViewState1.l) obj;
            if (lVar != null) {
                this.c.f705p.b(lVar.a);
                return Unit.a;
            }
            Intrinsics.a("event");
            throw null;
        }
    }

    /* compiled from: FillingProfileFullNamePageFragmentVm.kt */
    public static final class d extends n.n.c.j implements Functions0<a.k, n.g> {
        public final /* synthetic */ FillingProfileFullNamePageFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(FillingProfileFullNamePageFragmentVm fillingProfileFullNamePageFragmentVm) {
            super(1);
            this.c = fillingProfileFullNamePageFragmentVm;
        }

        public Object a(Object obj) {
            FillingProfileFullNamePageFragmentViewState1.k kVar = (FillingProfileFullNamePageFragmentViewState1.k) obj;
            if (kVar != null) {
                this.c.f705p.f().getPassenger().setLastName(kVar.a);
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: FillingProfileFullNamePageFragmentVm.kt */
    public static final class e extends n.n.c.j implements Functions0<a.h, n.g> {
        public final /* synthetic */ FillingProfileFullNamePageFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(FillingProfileFullNamePageFragmentVm fillingProfileFullNamePageFragmentVm) {
            super(1);
            this.c = fillingProfileFullNamePageFragmentVm;
        }

        public Object a(Object obj) {
            FillingProfileFullNamePageFragmentViewState1.h hVar = (FillingProfileFullNamePageFragmentViewState1.h) obj;
            if (hVar != null) {
                this.c.f705p.f().getPassenger().setFirstName(hVar.a);
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: FillingProfileFullNamePageFragmentVm.kt */
    public static final class f extends n.n.c.j implements Functions0<a.i, n.g> {
        public final /* synthetic */ FillingProfileFullNamePageFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(FillingProfileFullNamePageFragmentVm fillingProfileFullNamePageFragmentVm) {
            super(1);
            this.c = fillingProfileFullNamePageFragmentVm;
        }

        public Object a(Object obj) {
            FillingProfileFullNamePageFragmentViewState1.i iVar = (FillingProfileFullNamePageFragmentViewState1.i) obj;
            if (iVar != null) {
                this.c.f705p.f().getPassenger().setPatronymic(iVar.a);
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: FillingProfileFullNamePageFragmentVm.kt */
    public static final class g extends n.n.c.j implements Functions0<a.e, n.g> {
        public final /* synthetic */ FillingProfileFullNamePageFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(FillingProfileFullNamePageFragmentVm fillingProfileFullNamePageFragmentVm) {
            super(1);
            this.c = fillingProfileFullNamePageFragmentVm;
        }

        public Object a(Object obj) {
            FillingProfileFullNamePageFragmentViewState1.e eVar = (FillingProfileFullNamePageFragmentViewState1.e) obj;
            if (eVar != null) {
                if (eVar.a) {
                    this.c.f705p.f().getPassenger().setPlacementAddress(this.c.f705p.f().getPassenger().getRegistrationAddress());
                } else {
                    this.c.f705p.f().getPassenger().setPlacementAddress(FillingProfileFullNamePageFragmentVm.b(this.c).d);
                }
                this.c.f705p.f().getPassenger().setRegistrationAddressMatchesPlacement(Boolean.valueOf(eVar.a));
                FillingProfileFullNamePageFragmentVm fillingProfileFullNamePageFragmentVm = this.c;
                fillingProfileFullNamePageFragmentVm.d.a(FillingProfileFullNamePageFragmentVm.b(fillingProfileFullNamePageFragmentVm));
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: FillingProfileFullNamePageFragmentVm.kt */
    public static final class h extends n.n.c.j implements Functions0<a.f, n.g> {
        public final /* synthetic */ FillingProfileFullNamePageFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(FillingProfileFullNamePageFragmentVm fillingProfileFullNamePageFragmentVm) {
            super(1);
            this.c = fillingProfileFullNamePageFragmentVm;
        }

        public Object a(Object obj) {
            FillingProfileFullNamePageFragmentViewState1.f fVar = (FillingProfileFullNamePageFragmentViewState1.f) obj;
            if (fVar != null) {
                this.c.f705p.f().getPassenger().setBirthDate(Integer.valueOf(Integer.parseInt(Collections.a(fVar.a, "yyyyMMdd", (Locale) null, (ZoneId) null, 6))));
                return Unit.a;
            }
            Intrinsics.a("event");
            throw null;
        }
    }

    /* compiled from: FillingProfileFullNamePageFragmentVm.kt */
    public static final class i extends n.n.c.j implements Functions0<a.j, n.g> {
        public final /* synthetic */ FillingProfileFullNamePageFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(FillingProfileFullNamePageFragmentVm fillingProfileFullNamePageFragmentVm) {
            super(1);
            this.c = fillingProfileFullNamePageFragmentVm;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
         arg types: [java.lang.Boolean, boolean]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
        public Object a(Object obj) {
            FillingProfileFullNamePageFragmentViewState1.j jVar = (FillingProfileFullNamePageFragmentViewState1.j) obj;
            if (jVar != null) {
                this.c.f705p.f().getPassenger().setRegistrationAddress(jVar.a);
                if (Intrinsics.a((Object) this.c.f705p.f().getPassenger().getRegistrationAddressMatchesPlacement(), (Object) true)) {
                    this.c.f705p.f().getPassenger().setPlacementAddress(jVar.a);
                    this.c.f721e.a((e.c.c.l) new FillingProfileFullNamePageFragmentViewState3(jVar.a));
                }
                return Unit.a;
            }
            Intrinsics.a("event");
            throw null;
        }
    }

    /* compiled from: FillingProfileFullNamePageFragmentVm.kt */
    public static final class j extends n.n.c.j implements Functions0<a.g, n.g> {
        public final /* synthetic */ FillingProfileFullNamePageFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(FillingProfileFullNamePageFragmentVm fillingProfileFullNamePageFragmentVm) {
            super(1);
            this.c = fillingProfileFullNamePageFragmentVm;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
         arg types: [java.lang.Boolean, boolean]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
        public Object a(Object obj) {
            FillingProfileFullNamePageFragmentViewState1.g gVar = (FillingProfileFullNamePageFragmentViewState1.g) obj;
            if (gVar != null) {
                Passenger passenger = this.c.f705p.f().getPassenger();
                passenger.setPlacementAddress(gVar.a);
                if (Intrinsics.a((Object) passenger.getQuarantineAddressMatchesPlacement(), (Object) true)) {
                    passenger.setQuarantineAddress(passenger.getPlacementAddress());
                }
                if (Intrinsics.a((Object) passenger.getRegistrationAddressMatchesPlacement(), (Object) false)) {
                    FillingProfileFullNamePageFragmentViewState b = FillingProfileFullNamePageFragmentVm.b(this.c);
                    String str = gVar.a;
                    if (str != null) {
                        b.d = str;
                    } else {
                        Intrinsics.a("<set-?>");
                        throw null;
                    }
                }
                return Unit.a;
            }
            Intrinsics.a("event");
            throw null;
        }
    }

    /* compiled from: FillingProfileFullNamePageFragmentVm.kt */
    public static final class k extends n.n.c.j implements Functions0<a.c, n.g> {
        public final /* synthetic */ FillingProfileFullNamePageFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(FillingProfileFullNamePageFragmentVm fillingProfileFullNamePageFragmentVm) {
            super(1);
            this.c = fillingProfileFullNamePageFragmentVm;
        }

        public Object a(Object obj) {
            FillingProfileFullNamePageFragmentViewState1.c cVar = (FillingProfileFullNamePageFragmentViewState1.c) obj;
            if (cVar != null) {
                this.c.f705p.f().getPassenger().setGender(cVar.a);
                FillingProfileFullNamePageFragmentVm fillingProfileFullNamePageFragmentVm = this.c;
                fillingProfileFullNamePageFragmentVm.d.a(FillingProfileFullNamePageFragmentVm.b(fillingProfileFullNamePageFragmentVm));
                return Unit.a;
            }
            Intrinsics.a("event");
            throw null;
        }
    }

    /* compiled from: FillingProfileFullNamePageFragmentVm.kt */
    public static final class l extends n.n.c.j implements Functions0<a.b, n.g> {
        public final /* synthetic */ FillingProfileFullNamePageFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(FillingProfileFullNamePageFragmentVm fillingProfileFullNamePageFragmentVm) {
            super(1);
            this.c = fillingProfileFullNamePageFragmentVm;
        }

        public Object a(Object obj) {
            FillingProfileFullNamePageFragmentViewState1.b bVar = (FillingProfileFullNamePageFragmentViewState1.b) obj;
            if (bVar != null) {
                this.c.f705p.f().getPassenger().setCitizenship(bVar.a.getCode());
                this.c.f705p.f().getPassenger().setCitizenshipName(bVar.a.getName());
                FillingProfileFullNamePageFragmentVm fillingProfileFullNamePageFragmentVm = this.c;
                fillingProfileFullNamePageFragmentVm.d.a(FillingProfileFullNamePageFragmentVm.b(fillingProfileFullNamePageFragmentVm));
                return Unit.a;
            }
            Intrinsics.a("event");
            throw null;
        }
    }

    /* compiled from: FillingProfileFullNamePageFragmentVm.kt */
    public static final class m extends n.n.c.j implements Functions0<a.n, n.g> {
        public final /* synthetic */ FillingProfileFullNamePageFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public m(FillingProfileFullNamePageFragmentVm fillingProfileFullNamePageFragmentVm) {
            super(1);
            this.c = fillingProfileFullNamePageFragmentVm;
        }

        public Object a(Object obj) {
            if (((FillingProfileFullNamePageFragmentViewState1.n) obj) != null) {
                IFragmentCoordinator iFragmentCoordinator = this.c.f703n;
                String genderBottomSheetTitle = FillingProfileFullNamePageFragmentVm.b(this.c).a.getGenderBottomSheetTitle();
                FillingProfileFullNamePageFragmentViewState0[] fillingProfileFullNamePageFragmentViewState0Arr = new FillingProfileFullNamePageFragmentViewState0[2];
                boolean z = false;
                fillingProfileFullNamePageFragmentViewState0Arr[0] = new FillingProfileFullNamePageFragmentViewState0(ProfileData0.male, FillingProfileFullNamePageFragmentVm.b(this.c).a.getGenderMaleLabel(), this.c.f705p.f().getPassenger().getGender() == ProfileData0.male);
                ProfileData0 profileData0 = ProfileData0.female;
                String genderFemaleLabel = FillingProfileFullNamePageFragmentVm.b(this.c).a.getGenderFemaleLabel();
                if (this.c.f705p.f().getPassenger().getGender() == ProfileData0.female) {
                    z = true;
                }
                fillingProfileFullNamePageFragmentViewState0Arr[1] = new FillingProfileFullNamePageFragmentViewState0(profileData0, genderFemaleLabel, z);
                iFragmentCoordinator.a(new BottomSheetSelectorNavigationDto0(genderBottomSheetTitle, Collections.a((Object[]) fillingProfileFullNamePageFragmentViewState0Arr)));
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: FillingProfileFullNamePageFragmentVm.kt */
    public static final class n extends n.n.c.j implements Functions0<a.m, n.g> {
        public final /* synthetic */ FillingProfileFullNamePageFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public n(FillingProfileFullNamePageFragmentVm fillingProfileFullNamePageFragmentVm) {
            super(1);
            this.c = fillingProfileFullNamePageFragmentVm;
        }

        public Object a(Object obj) {
            FillingProfileFullNamePageFragmentViewState1.m mVar = (FillingProfileFullNamePageFragmentViewState1.m) obj;
            if (mVar != null) {
                this.c.f703n.a(mVar.a);
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: FillingProfileFullNamePageFragmentVm.kt */
    public static final class o extends n.n.c.j implements Functions0<a.a, n.g> {
        public final /* synthetic */ FillingProfileFullNamePageFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public o(FillingProfileFullNamePageFragmentVm fillingProfileFullNamePageFragmentVm) {
            super(1);
            this.c = fillingProfileFullNamePageFragmentVm;
        }

        public Object a(Object obj) {
            if (((FillingProfileFullNamePageFragmentViewState1.a) obj) != null) {
                this.c.f704o.a(true);
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: FillingProfileFullNamePageFragmentVm.kt */
    public static final /* synthetic */ class p extends n.n.c.h implements Functions0<Observable<a.d>, Observable<BaseMviVm0<? extends e.c.c.k>>> {
        public p(FillingProfileFullNamePageFragmentVm fillingProfileFullNamePageFragmentVm) {
            super(1, fillingProfileFullNamePageFragmentVm);
        }

        /* JADX WARN: Type inference failed for: r1v0, types: [n.n.c.CallableReference, e.a.b.h.b.i.c.FillingProfileFullNamePageFragmentVm$p] */
        public Object a(Object obj) {
            Observable observable = (Observable) obj;
            if (observable != null) {
                return FillingProfileFullNamePageFragmentVm.a((FillingProfileFullNamePageFragmentVm) this.c, observable);
            }
            Intrinsics.a("p1");
            throw null;
        }

        public final String f() {
            return "selectCitizenshipEventProcessor";
        }

        /* JADX WARN: Type inference failed for: r0v1, types: [n.p.KClass, n.p.KDeclarationContainer] */
        public final KDeclarationContainer g() {
            return Reflection.a(FillingProfileFullNamePageFragmentVm.class);
        }

        public final String i() {
            return "selectCitizenshipEventProcessor(Lio/reactivex/Observable;)Lio/reactivex/Observable;";
        }
    }

    /* compiled from: FillingProfileFullNamePageFragmentVm.kt */
    public static final class q<T, R> implements Function<T, R> {
        public static final q a = new q();

        public Object a(Object obj) {
            if (((ResponseBody) obj) != null) {
                return new BaseMviVm0.a(FillingProfileFullNamePageFragmentViewState2.a.a);
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: FillingProfileFullNamePageFragmentVm.kt */
    public static final class r<T> implements Consumer<l.b.s.b> {
        public final /* synthetic */ FillingProfileFullNamePageFragmentVm b;

        public r(FillingProfileFullNamePageFragmentVm fillingProfileFullNamePageFragmentVm) {
            this.b = fillingProfileFullNamePageFragmentVm;
        }

        public void a(Object obj) {
            Disposable disposable = (Disposable) obj;
            this.b.f682m.a((Boolean) true);
        }
    }

    /* compiled from: FillingProfileFullNamePageFragmentVm.kt */
    public static final class s<T, R> implements Function<T, R> {
        public final /* synthetic */ FillingProfileFullNamePageFragmentVm a;

        public s(FillingProfileFullNamePageFragmentVm fillingProfileFullNamePageFragmentVm) {
            this.a = fillingProfileFullNamePageFragmentVm;
        }

        public Object a(Object obj) {
            BaseMviVm0 baseMviVm0 = (BaseMviVm0) obj;
            if (baseMviVm0 != null) {
                this.a.f682m.a((Boolean) false);
                return Boolean.valueOf(!(baseMviVm0 instanceof BaseMviVm0.b));
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: FillingProfileFullNamePageFragmentVm.kt */
    public static final class t<T, R> implements Function<T, R> {
        public final /* synthetic */ FillingProfileFullNamePageFragmentVm a;

        public t(FillingProfileFullNamePageFragmentVm fillingProfileFullNamePageFragmentVm) {
            this.a = fillingProfileFullNamePageFragmentVm;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
         arg types: [java.lang.Boolean, boolean]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
        public Object a(Object obj) {
            String str;
            if (((BaseMviVm0) obj) != null) {
                Passenger passenger = this.a.f705p.f().getPassenger();
                boolean a2 = Intrinsics.a((Object) passenger.getRegistrationAddressMatchesPlacement(), (Object) true);
                FillingProfileFullNamePageFragmentVm fillingProfileFullNamePageFragmentVm = this.a;
                BehaviorRelay<VS> behaviorRelay = fillingProfileFullNamePageFragmentVm.d;
                FillingProfileFullNamePageFragmentViewState b = FillingProfileFullNamePageFragmentVm.b(fillingProfileFullNamePageFragmentVm);
                if (a2) {
                    str = "";
                } else {
                    str = passenger.getPlacementAddress();
                }
                behaviorRelay.a(FillingProfileFullNamePageFragmentViewState.a(b, null, null, null, str, 7));
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: FillingProfileFullNamePageFragmentVm.kt */
    public static final class u<T, R> implements Function<T, R> {
        public static final u a = new u();

        public Object a(Object obj) {
            if (((Unit) obj) != null) {
                return BaseMviVm.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.b.h.b.i.c.FillingProfileFullNamePageFragmentVm$a, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.b.h.b.i.c.FillingProfileFullNamePageFragmentVm$b, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00a3  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00ae  */
    /* JADX WARNING: Removed duplicated region for block: B:40:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public FillingProfileFullNamePageFragmentVm(e.a.a.a.e.q.IFragmentCoordinator r2, e.a.b.h.b.g.IMainCoordinator r3, e.a.a.a.e.o.ITopLevelCoordinator r4, e.a.b.d.b.IProfileRepository r5, e.a.a.g.b.b.a.IAuthPrefs r6) {
        /*
            r1 = this;
            r0 = 0
            if (r2 == 0) goto L_0x00ea
            if (r3 == 0) goto L_0x00e4
            if (r4 == 0) goto L_0x00de
            if (r5 == 0) goto L_0x00d8
            if (r6 == 0) goto L_0x00d2
            r1.<init>(r3)
            r1.f703n = r2
            r1.f704o = r4
            r1.f705p = r5
            r1.f706q = r6
            e.a.a.g.a.Session r2 = r6.e()
            java.lang.String r2 = r2.b
            ru.covid19.core.data.network.model.PersonalResponse r2 = r5.a(r2)
            if (r2 == 0) goto L_0x00d1
            e.a.b.d.b.IProfileRepository r3 = r1.f705p
            ru.covid19.droid.data.model.profileData.ProfileData r3 = r3.f()
            ru.covid19.droid.data.model.profileData.Passenger r3 = r3.getPassenger()
            java.lang.String r4 = r2.getFirstName()
            r3.setFirstName(r4)
            e.a.b.d.b.IProfileRepository r3 = r1.f705p
            ru.covid19.droid.data.model.profileData.ProfileData r3 = r3.f()
            ru.covid19.droid.data.model.profileData.Passenger r3 = r3.getPassenger()
            java.lang.String r4 = r2.getLastName()
            r3.setLastName(r4)
            e.a.b.d.b.IProfileRepository r3 = r1.f705p
            ru.covid19.droid.data.model.profileData.ProfileData r3 = r3.f()
            ru.covid19.droid.data.model.profileData.Passenger r3 = r3.getPassenger()
            java.lang.String r4 = r2.getMiddleName()
            r3.setPatronymic(r4)
            e.a.b.d.b.IProfileRepository r3 = r1.f705p
            ru.covid19.droid.data.model.profileData.ProfileData r3 = r3.f()
            ru.covid19.droid.data.model.profileData.Passenger r3 = r3.getPassenger()
            java.lang.String r4 = r2.getBirthDate()
            r5 = 1
            if (r4 == 0) goto L_0x006b
            java.lang.Integer r4 = n.i.Collections.a(r4, r0, r5)
            goto L_0x006c
        L_0x006b:
            r4 = r0
        L_0x006c:
            r3.setBirthDate(r4)
            e.a.b.d.b.IProfileRepository r3 = r1.f705p
            ru.covid19.droid.data.model.profileData.ProfileData r3 = r3.f()
            ru.covid19.droid.data.model.profileData.Passenger r3 = r3.getPassenger()
            ru.covid19.core.data.network.model.PersonalResponse4 r4 = r2.getGender()
            if (r4 != 0) goto L_0x0080
            goto L_0x0088
        L_0x0080:
            int r4 = r4.ordinal()
            if (r4 == 0) goto L_0x008d
            if (r4 == r5) goto L_0x008a
        L_0x0088:
            r4 = r0
            goto L_0x008f
        L_0x008a:
            ru.covid19.droid.data.model.profileData.ProfileData0 r4 = ru.covid19.droid.data.model.profileData.ProfileData0.female
            goto L_0x008f
        L_0x008d:
            ru.covid19.droid.data.model.profileData.ProfileData0 r4 = ru.covid19.droid.data.model.profileData.ProfileData0.male
        L_0x008f:
            r3.setGender(r4)
            e.a.b.d.b.IProfileRepository r3 = r1.f705p
            ru.covid19.droid.data.model.profileData.ProfileData r3 = r3.f()
            ru.covid19.droid.data.model.profileData.Passenger r3 = r3.getPassenger()
            java.lang.String r4 = r2.getCitizenship()
            if (r4 == 0) goto L_0x00a3
            goto L_0x00a5
        L_0x00a3:
            java.lang.String r4 = ""
        L_0x00a5:
            r3.setCitizenship(r4)
            java.lang.String r2 = r2.getCitizenship()
            if (r2 == 0) goto L_0x00d1
            e.a.b.d.b.IProfileRepository r3 = r1.f705p
            l.b.Single r3 = r3.b()
            e.a.b.h.b.i.c.FillingProfileFullNamePageFragmentVm$a r4 = new e.a.b.h.b.i.c.FillingProfileFullNamePageFragmentVm$a
            r4.<init>(r2, r1)
            e.a.b.h.b.i.c.FillingProfileFullNamePageFragmentVm$b r2 = e.a.b.h.b.i.c.FillingProfileFullNamePageFragmentVm.b.b
            if (r3 == 0) goto L_0x00d0
            java.lang.String r5 = "onSuccess is null"
            l.b.u.b.ObjectHelper.a(r4, r5)
            java.lang.String r5 = "onError is null"
            l.b.u.b.ObjectHelper.a(r2, r5)
            l.b.u.d.ConsumerSingleObserver r5 = new l.b.u.d.ConsumerSingleObserver
            r5.<init>(r4, r2)
            r3.a(r5)
            goto L_0x00d1
        L_0x00d0:
            throw r0
        L_0x00d1:
            return
        L_0x00d2:
            java.lang.String r2 = "authPrefs"
            n.n.c.Intrinsics.a(r2)
            throw r0
        L_0x00d8:
            java.lang.String r2 = "profileRepository"
            n.n.c.Intrinsics.a(r2)
            throw r0
        L_0x00de:
            java.lang.String r2 = "topLevelCoordinator"
            n.n.c.Intrinsics.a(r2)
            throw r0
        L_0x00e4:
            java.lang.String r2 = "mainCoordinator"
            n.n.c.Intrinsics.a(r2)
            throw r0
        L_0x00ea:
            java.lang.String r2 = "fragmentCoordinator"
            n.n.c.Intrinsics.a(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: e.a.b.h.b.i.c.FillingProfileFullNamePageFragmentVm.<init>(e.a.a.a.e.q.IFragmentCoordinator, e.a.b.h.b.g.IMainCoordinator, e.a.a.a.e.o.ITopLevelCoordinator, e.a.b.d.b.IProfileRepository, e.a.a.g.b.b.a.IAuthPrefs):void");
    }

    public static final /* synthetic */ FillingProfileFullNamePageFragmentViewState b(FillingProfileFullNamePageFragmentVm fillingProfileFullNamePageFragmentVm) {
        return (FillingProfileFullNamePageFragmentViewState) fillingProfileFullNamePageFragmentVm.e();
    }

    public void a(Object obj) {
        FillingProfileFullNamePageFragmentViewState fillingProfileFullNamePageFragmentViewState = (FillingProfileFullNamePageFragmentViewState) obj;
        if (fillingProfileFullNamePageFragmentViewState != null) {
            Passenger passenger = this.f705p.f().getPassenger();
            if (passenger != null) {
                fillingProfileFullNamePageFragmentViewState.c = passenger;
                fillingProfileFullNamePageFragmentViewState.b = this.f705p.g();
                return;
            }
            Intrinsics.a("<set-?>");
            throw null;
        }
        Intrinsics.a("vs");
        throw null;
    }

    public Object d() {
        return new FillingProfileFullNamePageFragmentViewState(null, null, null, null, 15);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.b.h.b.i.c.FillingProfileFullNamePageFragmentVm$r, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.t.Action, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.u.e.c.ObservableDoOnLifecycle, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Single<java.lang.Boolean>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Single<Boolean> g() {
        Passenger passenger = this.f705p.f().getPassenger();
        StringBuilder sb = new StringBuilder();
        sb.append(this.f705p.f().getPassenger().getLastName());
        sb.append(' ' + this.f705p.f().getPassenger().getFirstName());
        String patronymic = this.f705p.f().getPassenger().getPatronymic();
        if (patronymic != null) {
            sb.append(' ' + patronymic);
        }
        String sb2 = sb.toString();
        Intrinsics.a((Object) sb2, "StringBuilder().apply(builderAction).toString()");
        passenger.setFullName(sb2);
        IProfileRepository iProfileRepository = this.f705p;
        Uri g2 = iProfileRepository.g();
        if (g2 != null) {
            Observable<R> c2 = iProfileRepository.a(g2, this.f706q.b()).c().c((Function) q.a);
            r rVar = new r(this);
            Action action = Functions.c;
            ObjectHelper.a((Object) rVar, "onSubscribe is null");
            ObjectHelper.a((Object) action, "onDispose is null");
            ObservableDoOnLifecycle observableDoOnLifecycle = new ObservableDoOnLifecycle(c2, rVar, action);
            Intrinsics.a((Object) observableDoOnLifecycle, "profileRepository\n      …ccept(true)\n            }");
            Single<Boolean> b2 = BaseDpFragmentVm1.a(this, observableDoOnLifecycle, FillingProfileFullNamePageFragmentViewState2.a.class, null, null, null, null, 30, null).c((Function) new s(this)).b();
            Intrinsics.a((Object) b2, "profileRepository\n      …          .firstOrError()");
            return b2;
        }
        Intrinsics.a();
        throw null;
    }

    public boolean h() {
        Passenger passenger = this.f705p.f().getPassenger();
        Integer birthDate = passenger.getBirthDate();
        if ((birthDate == null || birthDate.intValue() != 0) && !Indent.b(passenger.getCitizenship()) && passenger.getGender() != null && !Indent.b(passenger.getPlacementAddress()) && !Indent.b(passenger.getRegistrationAddress()) && this.f705p.g() != null) {
            String firstName = this.f705p.f().getPassenger().getFirstName();
            if (!(firstName == null || Indent.b(firstName))) {
                String lastName = this.f705p.f().getPassenger().getLastName();
                if (!(lastName == null || Indent.b(lastName))) {
                    return true;
                }
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<R>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Observable<BaseMviVm0<? extends e.c.c.k>> b(Observable<e.c.c.p> observable) {
        if (observable != null) {
            Observable<R> c2 = super.b(observable).c((Function) new t(this)).c((Function) u.a);
            Intrinsics.a((Object) c2, "super.initEventProcessor…      }.map { IgnoreLce }");
            return c2;
        }
        Intrinsics.a("observable");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.c.c.BaseMviVm1.a(java.lang.Object, e.c.c.k):VS
     arg types: [e.a.b.h.b.i.c.FillingProfileFullNamePageFragmentViewState, e.c.c.BaseViewState1]
     candidates:
      e.a.b.h.b.i.c.FillingProfileFullNamePageFragmentVm.a(e.a.b.h.b.i.c.FillingProfileFullNamePageFragmentVm, l.b.Observable):l.b.Observable
      e.c.c.BaseMviVm1.a(java.lang.Object, e.c.c.k):VS */
    public Object a(Object obj, BaseViewState1 baseViewState1) {
        FillingProfileFullNamePageFragmentViewState fillingProfileFullNamePageFragmentViewState = (FillingProfileFullNamePageFragmentViewState) obj;
        if (fillingProfileFullNamePageFragmentViewState == null) {
            Intrinsics.a("vs");
            throw null;
        } else if (baseViewState1 == null) {
            Intrinsics.a("result");
            throw null;
        } else if (baseViewState1 instanceof FillingProfileFullNamePageFragmentViewState2.b) {
            FillingProfileFullNamePageFragmentViewState2.b bVar = (FillingProfileFullNamePageFragmentViewState2.b) baseViewState1;
            return FillingProfileFullNamePageFragmentViewState.a(fillingProfileFullNamePageFragmentViewState, null, null, null, null, 13);
        } else {
            super.a((Object) fillingProfileFullNamePageFragmentViewState, (e.c.c.k) baseViewState1);
            return fillingProfileFullNamePageFragmentViewState;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.s.Disposable, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void a(List<? extends Observable<? extends e.c.c.b>> list) {
        if (list != null) {
            super.a(list);
            Disposable a2 = this.f703n.f().a(new FillingProfileFullNamePageFragmentVm3(this));
            Intrinsics.a((Object) a2, "fragmentCoordinator.addB…          }\n            }");
            j.c.a.a.c.n.c.a(a2, this.f722f);
            Disposable a3 = this.f704o.c().a(new FillingProfileFullNamePageFragmentVm4(this));
            Intrinsics.a((Object) a3, "topLevelCoordinator.addC…(photoUri))\n            }");
            j.c.a.a.c.n.c.a(a3, this.f722f);
            return;
        }
        Intrinsics.a("es");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<U>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<e.c.c.BaseMviVm0<? extends e.c.c.k>>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Observable<BaseMviVm0<? extends e.c.c.k>> a(Observable<? extends e.c.c.b> observable) {
        if (observable != null) {
            Observable<U> a2 = observable.a(FillingProfileFullNamePageFragmentViewState1.f.class);
            Intrinsics.a((Object) a2, "ofType(R::class.java)");
            Observable<U> a3 = observable.a(FillingProfileFullNamePageFragmentViewState1.j.class);
            Intrinsics.a((Object) a3, "ofType(R::class.java)");
            Observable<U> a4 = observable.a(FillingProfileFullNamePageFragmentViewState1.g.class);
            Intrinsics.a((Object) a4, "ofType(R::class.java)");
            Observable<U> a5 = observable.a(FillingProfileFullNamePageFragmentViewState1.c.class);
            Intrinsics.a((Object) a5, "ofType(R::class.java)");
            Observable<U> a6 = observable.a(FillingProfileFullNamePageFragmentViewState1.b.class);
            Intrinsics.a((Object) a6, "ofType(R::class.java)");
            Observable<U> a7 = observable.a(FillingProfileFullNamePageFragmentViewState1.n.class);
            Intrinsics.a((Object) a7, "ofType(R::class.java)");
            Observable<U> a8 = observable.a(FillingProfileFullNamePageFragmentViewState1.m.class);
            Intrinsics.a((Object) a8, "ofType(R::class.java)");
            Observable<U> a9 = observable.a(FillingProfileFullNamePageFragmentViewState1.a.class);
            Intrinsics.a((Object) a9, "ofType(R::class.java)");
            Observable<U> a10 = observable.a(FillingProfileFullNamePageFragmentViewState1.d.class);
            Intrinsics.a((Object) a10, "ofType(R::class.java)");
            Observable<U> a11 = observable.a(FillingProfileFullNamePageFragmentViewState1.l.class);
            Intrinsics.a((Object) a11, "ofType(R::class.java)");
            Observable<U> a12 = observable.a(FillingProfileFullNamePageFragmentViewState1.k.class);
            Intrinsics.a((Object) a12, "ofType(R::class.java)");
            Observable<U> a13 = observable.a(FillingProfileFullNamePageFragmentViewState1.h.class);
            Intrinsics.a((Object) a13, "ofType(R::class.java)");
            Observable<U> a14 = observable.a(FillingProfileFullNamePageFragmentViewState1.i.class);
            Intrinsics.a((Object) a14, "ofType(R::class.java)");
            Observable<U> a15 = observable.a(FillingProfileFullNamePageFragmentViewState1.e.class);
            Intrinsics.a((Object) a15, "ofType(R::class.java)");
            Observable<BaseMviVm0<? extends e.c.c.k>> a16 = Observable.a(super.a(observable), BaseMviVm.a(a2, new h(this)), BaseMviVm.a(a3, new i(this)), BaseMviVm.a(a4, new j(this)), BaseMviVm.a(a5, new k(this)), BaseMviVm.a(a6, new l(this)), BaseMviVm.a(a7, new m(this)), BaseMviVm.a(a8, new n(this)), BaseMviVm.a(a9, new o(this)), BaseMviVm.b(a10, new p(this)), BaseMviVm.a(a11, new c(this)), BaseMviVm.a(a12, new d(this)), BaseMviVm.a(a13, new e(this)), BaseMviVm.a(a14, new f(this)), BaseMviVm.a(a15, new g(this)));
            Intrinsics.a((Object) a16, "Observable.mergeArray(\n …)\n            }\n        )");
            return a16;
        }
        Intrinsics.a("o");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static final /* synthetic */ Observable a(FillingProfileFullNamePageFragmentVm fillingProfileFullNamePageFragmentVm, Observable observable) {
        if (fillingProfileFullNamePageFragmentVm != null) {
            Observable c2 = observable.b(new FillingProfileFullNamePageFragmentVm0(fillingProfileFullNamePageFragmentVm)).c((Function) new FillingProfileFullNamePageFragmentVm1(fillingProfileFullNamePageFragmentVm)).c((Function) FillingProfileFullNamePageFragmentVm2.a);
            Intrinsics.a((Object) c2, "observable\n            .…       .map { IgnoreLce }");
            return c2;
        }
        throw null;
    }
}
