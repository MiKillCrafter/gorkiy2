package e.a.b.h.b.h.m;

import e.a.a.a.e.o.c;
import e.a.a.g.b.b.a.b;
import e.a.b.d.b.a;
import e.a.b.h.b.g.g;
import k.a.Factory;
import m.a.Provider;

public final class DocumentsStepFragmentVm_Factory implements Factory<d> {
    public final Provider<g> a;
    public final Provider<a> b;
    public final Provider<c> c;
    public final Provider<b> d;

    public DocumentsStepFragmentVm_Factory(Provider<g> provider, Provider<a> provider2, Provider<c> provider3, Provider<b> provider4) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
    }

    public Object get() {
        return new DocumentsStepFragmentVm(this.a.get(), this.b.get(), this.c.get(), this.d.get());
    }
}
