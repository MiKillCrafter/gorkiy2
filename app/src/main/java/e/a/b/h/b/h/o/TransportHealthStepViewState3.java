package e.a.b.h.b.h.o;

import android.os.Parcel;
import android.os.Parcelable;
import e.a.a.a.e.p.BottomSheetSelectorNavigationDto;
import j.a.a.a.outline;
import n.n.c.Intrinsics;

/* compiled from: TransportHealthStepViewState.kt */
public final class TransportHealthStepViewState3 implements BottomSheetSelectorNavigationDto {
    public static final Parcelable.Creator CREATOR = new a();
    public final TransportHealthStepViewState2 b;
    public final String c;
    public final boolean d;

    public static class a implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            if (parcel != null) {
                return new TransportHealthStepViewState3((TransportHealthStepViewState2) Enum.valueOf(TransportHealthStepViewState2.class, parcel.readString()), parcel.readString(), parcel.readInt() != 0);
            }
            Intrinsics.a("in");
            throw null;
        }

        public final Object[] newArray(int i2) {
            return new TransportHealthStepViewState3[i2];
        }
    }

    public TransportHealthStepViewState3(TransportHealthStepViewState2 transportHealthStepViewState2, String str, boolean z) {
        if (transportHealthStepViewState2 == null) {
            Intrinsics.a("transportType");
            throw null;
        } else if (str != null) {
            this.b = transportHealthStepViewState2;
            this.c = str;
            this.d = z;
        } else {
            Intrinsics.a("label");
            throw null;
        }
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TransportHealthStepViewState3)) {
            return false;
        }
        TransportHealthStepViewState3 transportHealthStepViewState3 = (TransportHealthStepViewState3) obj;
        return Intrinsics.a(this.b, transportHealthStepViewState3.b) && Intrinsics.a(this.c, transportHealthStepViewState3.c) && this.d == transportHealthStepViewState3.d;
    }

    public String getLabel() {
        return this.c;
    }

    public int hashCode() {
        TransportHealthStepViewState2 transportHealthStepViewState2 = this.b;
        int i2 = 0;
        int hashCode = (transportHealthStepViewState2 != null ? transportHealthStepViewState2.hashCode() : 0) * 31;
        String str = this.c;
        if (str != null) {
            i2 = str.hashCode();
        }
        int i3 = (hashCode + i2) * 31;
        boolean z = this.d;
        if (z) {
            z = true;
        }
        return i3 + (z ? 1 : 0);
    }

    public boolean isSelected() {
        return this.d;
    }

    public String toString() {
        StringBuilder a2 = outline.a("TransportTypeSelectorItem(transportType=");
        a2.append(this.b);
        a2.append(", label=");
        a2.append(this.c);
        a2.append(", isSelected=");
        return outline.a(a2, this.d, ")");
    }

    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeString(this.b.name());
            parcel.writeString(this.c);
            parcel.writeInt(this.d ? 1 : 0);
            return;
        }
        Intrinsics.a("parcel");
        throw null;
    }
}
