package e.a.b.h.b.h.m;

import e.a.b.h.b.h.m.DocumentsStepFragmentViewState1;
import l.b.t.Function;
import n.Tuples;
import n.n.c.Intrinsics;
import ru.covid19.core.data.network.model.CountriesResponse;

/* compiled from: DocumentsStepFragmentVm.kt */
public final class DocumentsStepFragmentVm0<T, R> implements Function<T, R> {
    public final /* synthetic */ DocumentsStepFragmentViewState1.j a;

    public DocumentsStepFragmentVm0(DocumentsStepFragmentViewState1.j jVar) {
        this.a = jVar;
    }

    public Object a(Object obj) {
        CountriesResponse countriesResponse = (CountriesResponse) obj;
        if (countriesResponse != null) {
            return new Tuples(this.a, countriesResponse);
        }
        Intrinsics.a("it");
        throw null;
    }
}
