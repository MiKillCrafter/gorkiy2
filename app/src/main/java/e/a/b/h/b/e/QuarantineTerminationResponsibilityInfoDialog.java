package e.a.b.h.b.e;

import android.view.View;
import com.minsvyaz.gosuslugi.stopcorona.R;
import java.util.HashMap;

/* compiled from: QuarantineTerminationResponsibilityInfoDialog.kt */
public final class QuarantineTerminationResponsibilityInfoDialog extends BaseFaqBottomDialog {
    public final int o0 = R.string.dlg_quarantine_termination_responsibility_info_title;
    public final int p0 = R.string.dlg_quarantine_termination_responsibility_info_content;
    public HashMap q0;

    public /* synthetic */ void C() {
        super.C();
        N();
    }

    public void N() {
        HashMap hashMap = this.q0;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public int Q() {
        return this.p0;
    }

    public int R() {
        return this.o0;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [e.a.b.h.b.e.QuarantineTerminationResponsibilityInfoDialog, androidx.fragment.app.Fragment] */
    public View c(int i2) {
        if (this.q0 == null) {
            this.q0 = new HashMap();
        }
        View view = (View) this.q0.get(Integer.valueOf(i2));
        if (view != null) {
            return view;
        }
        View view2 = this.H;
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i2);
        this.q0.put(Integer.valueOf(i2), findViewById);
        return findViewById;
    }
}
