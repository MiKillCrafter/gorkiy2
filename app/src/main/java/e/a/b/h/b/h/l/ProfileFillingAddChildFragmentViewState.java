package e.a.b.h.b.h.l;

import j.a.a.a.outline;
import n.n.c.Intrinsics;
import ru.covid19.droid.data.model.ProfileFillingBottomSheetDto;
import ru.covid19.droid.data.model.profileData.Children;

/* compiled from: ProfileFillingAddChildFragmentViewState.kt */
public final class ProfileFillingAddChildFragmentViewState {
    public final ProfileFillingBottomSheetDto a;
    public final boolean b;
    public final Children c;
    public final String d;

    /* renamed from: e  reason: collision with root package name */
    public final String f688e;

    /* renamed from: f  reason: collision with root package name */
    public final String f689f;

    public ProfileFillingAddChildFragmentViewState() {
        this(null, false, null, null, null, null, 63);
    }

    public ProfileFillingAddChildFragmentViewState(ProfileFillingBottomSheetDto profileFillingBottomSheetDto, boolean z, Children children, String str, String str2, String str3) {
        if (profileFillingBottomSheetDto == null) {
            Intrinsics.a("bottomSheetDto");
            throw null;
        } else if (children != null) {
            this.a = profileFillingBottomSheetDto;
            this.b = z;
            this.c = children;
            this.d = str;
            this.f688e = str2;
            this.f689f = str3;
        } else {
            Intrinsics.a("child");
            throw null;
        }
    }

    public static /* synthetic */ ProfileFillingAddChildFragmentViewState a(ProfileFillingAddChildFragmentViewState profileFillingAddChildFragmentViewState, ProfileFillingBottomSheetDto profileFillingBottomSheetDto, boolean z, Children children, String str, String str2, String str3, int i2) {
        if ((i2 & 1) != 0) {
            profileFillingBottomSheetDto = profileFillingAddChildFragmentViewState.a;
        }
        ProfileFillingBottomSheetDto profileFillingBottomSheetDto2 = profileFillingBottomSheetDto;
        if ((i2 & 2) != 0) {
            z = profileFillingAddChildFragmentViewState.b;
        }
        boolean z2 = z;
        if ((i2 & 4) != 0) {
            children = profileFillingAddChildFragmentViewState.c;
        }
        Children children2 = children;
        if ((i2 & 8) != 0) {
            str = profileFillingAddChildFragmentViewState.d;
        }
        String str4 = str;
        if ((i2 & 16) != 0) {
            str2 = profileFillingAddChildFragmentViewState.f688e;
        }
        String str5 = str2;
        if ((i2 & 32) != 0) {
            str3 = profileFillingAddChildFragmentViewState.f689f;
        }
        String str6 = str3;
        if (profileFillingAddChildFragmentViewState == null) {
            throw null;
        } else if (profileFillingBottomSheetDto2 == null) {
            Intrinsics.a("bottomSheetDto");
            throw null;
        } else if (children2 != null) {
            return new ProfileFillingAddChildFragmentViewState(profileFillingBottomSheetDto2, z2, children2, str4, str5, str6);
        } else {
            Intrinsics.a("child");
            throw null;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ProfileFillingAddChildFragmentViewState)) {
            return false;
        }
        ProfileFillingAddChildFragmentViewState profileFillingAddChildFragmentViewState = (ProfileFillingAddChildFragmentViewState) obj;
        return Intrinsics.a(this.a, profileFillingAddChildFragmentViewState.a) && this.b == profileFillingAddChildFragmentViewState.b && Intrinsics.a(this.c, profileFillingAddChildFragmentViewState.c) && Intrinsics.a(this.d, profileFillingAddChildFragmentViewState.d) && Intrinsics.a(this.f688e, profileFillingAddChildFragmentViewState.f688e) && Intrinsics.a(this.f689f, profileFillingAddChildFragmentViewState.f689f);
    }

    public int hashCode() {
        ProfileFillingBottomSheetDto profileFillingBottomSheetDto = this.a;
        int i2 = 0;
        int hashCode = (profileFillingBottomSheetDto != null ? profileFillingBottomSheetDto.hashCode() : 0) * 31;
        boolean z = this.b;
        if (z) {
            z = true;
        }
        int i3 = (hashCode + (z ? 1 : 0)) * 31;
        Children children = this.c;
        int hashCode2 = (i3 + (children != null ? children.hashCode() : 0)) * 31;
        String str = this.d;
        int hashCode3 = (hashCode2 + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.f688e;
        int hashCode4 = (hashCode3 + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.f689f;
        if (str3 != null) {
            i2 = str3.hashCode();
        }
        return hashCode4 + i2;
    }

    public String toString() {
        StringBuilder a2 = outline.a("ProfileFillingAddChildFragmentViewState(bottomSheetDto=");
        a2.append(this.a);
        a2.append(", isPredefinedValue=");
        a2.append(this.b);
        a2.append(", child=");
        a2.append(this.c);
        a2.append(", firstName=");
        a2.append(this.d);
        a2.append(", lastName=");
        a2.append(this.f688e);
        a2.append(", patronymic=");
        return outline.a(a2, this.f689f, ")");
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ProfileFillingAddChildFragmentViewState(ProfileFillingBottomSheetDto profileFillingBottomSheetDto, boolean z, Children children, String str, String str2, String str3, int i2) {
        this((i2 & 1) != 0 ? new ProfileFillingBottomSheetDto(null, null, null, null, 15, null) : profileFillingBottomSheetDto, (i2 & 2) != 0 ? false : z, (i2 & 4) != 0 ? new Children(null, 0, null, null, null, null, null, null, null, null, 1023, null) : children, (i2 & 8) != 0 ? null : str, (i2 & 16) != 0 ? null : str2, (i2 & 32) == 0 ? str3 : null);
    }
}
