package e.a.b.h.b.g;

import androidx.fragment.app.Fragment;
import e.a.a.a.e.BaseScreens2;
import e.a.b.h.b.e.WasContactBottomDialog;

/* compiled from: Screens.kt */
public final class Screens17 extends BaseScreens2 {
    public static final Screens17 d = new Screens17();

    public Screens17() {
        super(null, 1);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [androidx.fragment.app.Fragment, e.a.b.h.b.e.WasContactBottomDialog] */
    public Fragment a() {
        return new WasContactBottomDialog();
    }
}
