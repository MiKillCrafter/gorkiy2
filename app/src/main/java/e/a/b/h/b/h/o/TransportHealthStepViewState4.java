package e.a.b.h.b.h.o;

import e.c.c.BaseViewState0;
import j.a.a.a.outline;
import java.util.List;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;
import ru.covid19.droid.data.model.profileData.Children;

/* compiled from: TransportHealthStepViewState.kt */
public abstract class TransportHealthStepViewState4 extends BaseViewState0 {

    /* compiled from: TransportHealthStepViewState.kt */
    public static final class a extends TransportHealthStepViewState4 {
        public static final a a = new a();

        public a() {
            super(null);
        }
    }

    /* compiled from: TransportHealthStepViewState.kt */
    public static final class b extends TransportHealthStepViewState4 {
        public final String a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(String str) {
            super(null);
            if (str != null) {
                this.a = str;
                return;
            }
            Intrinsics.a("info");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof b) && Intrinsics.a(this.a, ((b) obj).a);
            }
            return true;
        }

        public int hashCode() {
            String str = this.a;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return outline.a(outline.a("AdditionalInfoChanged(info="), this.a, ")");
        }
    }

    /* compiled from: TransportHealthStepViewState.kt */
    public static final class c extends TransportHealthStepViewState4 {
        public final Children a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(Children children) {
            super(null);
            if (children != null) {
                this.a = children;
                return;
            }
            Intrinsics.a("item");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof c) && Intrinsics.a(this.a, ((c) obj).a);
            }
            return true;
        }

        public int hashCode() {
            Children children = this.a;
            if (children != null) {
                return children.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder a2 = outline.a("ChildrenItemDeleteClicked(item=");
            a2.append(this.a);
            a2.append(")");
            return a2.toString();
        }
    }

    /* compiled from: TransportHealthStepViewState.kt */
    public static final class d extends TransportHealthStepViewState4 {
        public final boolean a;

        public d(boolean z) {
            super(null);
            this.a = z;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof d) && this.a == ((d) obj).a;
            }
            return true;
        }

        public int hashCode() {
            boolean z = this.a;
            if (z) {
                return 1;
            }
            return z ? 1 : 0;
        }

        public String toString() {
            return outline.a(outline.a("OnMatchesFactAddressChanged(matches="), this.a, ")");
        }
    }

    /* compiled from: TransportHealthStepViewState.kt */
    public static final class e extends TransportHealthStepViewState4 {
        public final String a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(String str) {
            super(null);
            if (str != null) {
                this.a = str;
                return;
            }
            Intrinsics.a("address");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof e) && Intrinsics.a(this.a, ((e) obj).a);
            }
            return true;
        }

        public int hashCode() {
            String str = this.a;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return outline.a(outline.a("OnQuarantineAddressChanged(address="), this.a, ")");
        }
    }

    /* compiled from: TransportHealthStepViewState.kt */
    public static final class f extends TransportHealthStepViewState4 {
        public final Boolean a;

        public f(Boolean bool) {
            super(null);
            this.a = bool;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof f) && Intrinsics.a(this.a, ((f) obj).a);
            }
            return true;
        }

        public int hashCode() {
            Boolean bool = this.a;
            if (bool != null) {
                return bool.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder a2 = outline.a("SymptomsAnswerChanged(hasSymptoms=");
            a2.append(this.a);
            a2.append(")");
            return a2.toString();
        }
    }

    /* compiled from: TransportHealthStepViewState.kt */
    public static final class g extends TransportHealthStepViewState4 {
        public final String a;
        public final List<c> b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(String str, List<c> list) {
            super(null);
            if (str == null) {
                Intrinsics.a("title");
                throw null;
            } else if (list != null) {
                this.a = str;
                this.b = list;
            } else {
                Intrinsics.a("items");
                throw null;
            }
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof g)) {
                return false;
            }
            g gVar = (g) obj;
            return Intrinsics.a(this.a, gVar.a) && Intrinsics.a(this.b, gVar.b);
        }

        public int hashCode() {
            String str = this.a;
            int i2 = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            List<c> list = this.b;
            if (list != null) {
                i2 = list.hashCode();
            }
            return hashCode + i2;
        }

        public String toString() {
            StringBuilder a2 = outline.a("SymptomsAnswerClicked(title=");
            a2.append(this.a);
            a2.append(", items=");
            a2.append(this.b);
            a2.append(")");
            return a2.toString();
        }
    }

    /* compiled from: TransportHealthStepViewState.kt */
    public static final class h extends TransportHealthStepViewState4 {
        public final TransportHealthStepViewState0 a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(TransportHealthStepViewState0 transportHealthStepViewState0) {
            super(null);
            if (transportHealthStepViewState0 != null) {
                this.a = transportHealthStepViewState0;
                return;
            }
            Intrinsics.a("bottomSheetDto");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof h) && Intrinsics.a(this.a, ((h) obj).a);
            }
            return true;
        }

        public int hashCode() {
            TransportHealthStepViewState0 transportHealthStepViewState0 = this.a;
            if (transportHealthStepViewState0 != null) {
                return transportHealthStepViewState0.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder a2 = outline.a("TransportTypeSelect(bottomSheetDto=");
            a2.append(this.a);
            a2.append(")");
            return a2.toString();
        }
    }

    /* compiled from: TransportHealthStepViewState.kt */
    public static final class i extends TransportHealthStepViewState4 {
        public final TransportHealthStepViewState2 a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(TransportHealthStepViewState2 transportHealthStepViewState2) {
            super(null);
            if (transportHealthStepViewState2 != null) {
                this.a = transportHealthStepViewState2;
                return;
            }
            Intrinsics.a("choice");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof i) && Intrinsics.a(this.a, ((i) obj).a);
            }
            return true;
        }

        public int hashCode() {
            TransportHealthStepViewState2 transportHealthStepViewState2 = this.a;
            if (transportHealthStepViewState2 != null) {
                return transportHealthStepViewState2.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder a2 = outline.a("TransportTypeSelectionChanged(choice=");
            a2.append(this.a);
            a2.append(")");
            return a2.toString();
        }
    }

    public /* synthetic */ TransportHealthStepViewState4(DefaultConstructorMarker defaultConstructorMarker) {
    }
}
