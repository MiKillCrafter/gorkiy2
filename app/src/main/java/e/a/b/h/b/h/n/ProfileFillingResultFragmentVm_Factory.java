package e.a.b.h.b.h.n;

import e.a.a.m.a.b;
import e.a.b.d.b.a;
import e.a.b.h.b.g.g;
import k.a.Factory;
import m.a.Provider;

public final class ProfileFillingResultFragmentVm_Factory implements Factory<g> {
    public final Provider<a> a;
    public final Provider<b> b;
    public final Provider<e.a.a.g.b.b.a.b> c;
    public final Provider<g> d;

    public ProfileFillingResultFragmentVm_Factory(Provider<a> provider, Provider<b> provider2, Provider<e.a.a.g.b.b.a.b> provider3, Provider<g> provider4) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
    }

    public Object get() {
        return new ProfileFillingResultFragmentVm(this.a.get(), this.b.get(), this.c.get(), this.d.get());
    }
}
