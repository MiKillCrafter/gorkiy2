package e.a.b.h.b.h.m;

import android.net.Uri;
import j.a.a.a.outline;
import n.n.c.Intrinsics;
import ru.covid19.droid.data.model.profileData.Document;
import ru.covid19.droid.data.model.profileData.Travel;

/* compiled from: DocumentsStepFragmentViewState.kt */
public final class DocumentsStepFragmentViewState0 {
    public Document a;
    public Travel b;
    public Uri c;

    public DocumentsStepFragmentViewState0() {
        this(null, null, null, 7);
    }

    public /* synthetic */ DocumentsStepFragmentViewState0(Document document, Travel travel, Uri uri, int i2) {
        document = (i2 & 1) != 0 ? new Document(null, null, null, 7, null) : document;
        travel = (i2 & 2) != 0 ? new Travel(0, null, null, null, null, 31, null) : travel;
        uri = (i2 & 4) != 0 ? null : uri;
        if (document == null) {
            Intrinsics.a("document");
            throw null;
        } else if (travel != null) {
            this.a = document;
            this.b = travel;
            this.c = uri;
        } else {
            Intrinsics.a("travel");
            throw null;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DocumentsStepFragmentViewState0)) {
            return false;
        }
        DocumentsStepFragmentViewState0 documentsStepFragmentViewState0 = (DocumentsStepFragmentViewState0) obj;
        return Intrinsics.a(this.a, documentsStepFragmentViewState0.a) && Intrinsics.a(this.b, documentsStepFragmentViewState0.b) && Intrinsics.a(this.c, documentsStepFragmentViewState0.c);
    }

    public int hashCode() {
        Document document = this.a;
        int i2 = 0;
        int hashCode = (document != null ? document.hashCode() : 0) * 31;
        Travel travel = this.b;
        int hashCode2 = (hashCode + (travel != null ? travel.hashCode() : 0)) * 31;
        Uri uri = this.c;
        if (uri != null) {
            i2 = uri.hashCode();
        }
        return hashCode2 + i2;
    }

    public String toString() {
        StringBuilder a2 = outline.a("DocumentsStepFragmentViewState(document=");
        a2.append(this.a);
        a2.append(", travel=");
        a2.append(this.b);
        a2.append(", documentUri=");
        a2.append(this.c);
        a2.append(")");
        return a2.toString();
    }
}
