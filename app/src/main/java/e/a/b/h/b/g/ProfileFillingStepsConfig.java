package e.a.b.h.b.g;

import e.a.a.a.e.d;
import e.a.b.h.c.IStepScreensConfig;
import e.a.b.h.c.a;
import java.util.ArrayList;
import java.util.List;
import n.Tuples;
import n.i.Collections;
import n.n.c.Intrinsics;

/* compiled from: ProfileFillingStepsConfig.kt */
public final class ProfileFillingStepsConfig implements IStepScreensConfig {
    public final List<Tuples<d, String>> a = Collections.a((Object[]) new Tuples[]{new Tuples(Screens3.d, "FULL_NAME_PAGE"), new Tuples(Screens8.d, "DOCUMENTS_STEP"), new Tuples(Screens11.d, "TRANSPORT_HEALTH_STEP"), new Tuples(Screens4.d, "INFORMED_CONFIRM_STEP")});
    public final List<Tuples<d, String>> b = new ArrayList();

    public List<Tuples<d, String>> a() {
        return this.a;
    }

    public List<Tuples<d, String>> b() {
        return this.b;
    }

    public int c() {
        List<Tuples<d, String>> a2 = Collections.a((a) this);
        if (a2 != null) {
            return a2.size() - 1;
        }
        Intrinsics.a("$this$lastIndex");
        throw null;
    }

    public Tuples<d, String> a(int i2) {
        return Collections.a((a) this).get(i2);
    }
}
