package e.a.b.h.b.h.m;

import e.a.b.h.b.h.m.DocumentsStepFragmentViewState1;
import l.b.SingleSource;
import l.b.t.Function;
import n.n.c.Intrinsics;

/* compiled from: DocumentsStepFragmentVm.kt */
public final class DocumentsStepFragmentVm1<T, R> implements Function<T, SingleSource<? extends R>> {
    public final /* synthetic */ DocumentsStepFragmentVm a;

    public DocumentsStepFragmentVm1(DocumentsStepFragmentVm documentsStepFragmentVm) {
        this.a = documentsStepFragmentVm;
    }

    public Object a(Object obj) {
        DocumentsStepFragmentViewState1.j jVar = (DocumentsStepFragmentViewState1.j) obj;
        if (jVar != null) {
            return this.a.f693o.c().a((Function) new DocumentsStepFragmentVm0(jVar));
        }
        Intrinsics.a("event");
        throw null;
    }
}
