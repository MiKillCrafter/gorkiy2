package e.a.b.h.c;

import l.b.t.Consumer;

/* compiled from: StepsNavigator.kt */
public final class StepsNavigator<T> implements Consumer<Boolean> {
    public final /* synthetic */ StepsNavigator0 b;

    public StepsNavigator(StepsNavigator0 stepsNavigator0) {
        this.b = stepsNavigator0;
    }

    public void a(Object obj) {
        if (!((Boolean) obj).booleanValue()) {
            this.b.f711f.b();
        }
    }
}
