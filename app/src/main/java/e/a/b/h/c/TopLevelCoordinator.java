package e.a.b.h.c;

import e.a.a.a.e.BaseScreens1;
import e.a.a.a.e.o.ITopLevelCoordinator;
import e.a.a.a.e.o.TopLevelBaseCoordinator1;
import e.a.a.a.e.r.AppCiceroneHolder;
import e.a.b.f.b.AppComponentsHolder;
import e.a.b.h.b.g.Screens15;
import e.a.b.h.b.g.Screens5;
import n.n.c.Intrinsics;

/* compiled from: TopLevelCoordinator.kt */
public final class TopLevelCoordinator extends TopLevelBaseCoordinator1 implements ITopLevelCoordinator {
    public final AppCiceroneHolder b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TopLevelCoordinator(AppCiceroneHolder appCiceroneHolder) {
        super(appCiceroneHolder);
        if (appCiceroneHolder != null) {
            this.b = appCiceroneHolder;
            return;
        }
        Intrinsics.a("appCiceroneHolder");
        throw null;
    }

    public void a() {
        this.b.b.b(new Screens5(null, 1));
    }

    public void b(boolean z) {
        AppComponentsHolder.b = null;
        if (z) {
            this.b.b.a((BaseScreens1) Screens15.f676f);
        } else {
            this.b.b.a((BaseScreens1) new Screens5(null, 1));
        }
    }
}
