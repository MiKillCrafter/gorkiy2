package e.a.b.h.b.e;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.minsvyaz.gosuslugi.stopcorona.R;
import e.a.a.a.b.BaseBottomSheetMviDialog;
import e.a.a.i.CoreComponentsHolder;
import e.a.a.i.b.CoreComponent;
import e.a.b.b;
import e.a.b.f.a.DaggerMainComponent;
import e.a.b.f.a.MainComponent;
import e.a.b.f.b.AppComponentsHolder;
import e.a.b.f.c.AddProfileStepsNavigationModule;
import e.a.b.f.c.MainModule;
import e.c.c.o;
import j.c.a.a.c.n.c;
import n.n.c.Intrinsics;

/* compiled from: BaseFaqBottomDialog.kt */
public abstract class BaseFaqBottomDialog extends BaseBottomSheetMviDialog<o, c> {
    public /* synthetic */ void C() {
        super.C();
        N();
    }

    public Class<c> O() {
        return BaseFaqBottomSheetDialogVm.class;
    }

    public abstract int Q();

    public abstract int R();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        if (layoutInflater != null) {
            return layoutInflater.inflate((int) R.layout.dlg_faq_base, viewGroup, false);
        }
        Intrinsics.a("inflater");
        throw null;
    }

    public void b(Bundle bundle) {
        if (AppComponentsHolder.b == null) {
            CoreComponent coreComponent = CoreComponentsHolder.b;
            if (coreComponent != null) {
                MainModule mainModule = new MainModule();
                AddProfileStepsNavigationModule addProfileStepsNavigationModule = new AddProfileStepsNavigationModule();
                c.a(coreComponent, CoreComponent.class);
                AppComponentsHolder.b = new DaggerMainComponent(mainModule, addProfileStepsNavigationModule, coreComponent, null);
            } else {
                Intrinsics.b("coreComponent");
                throw null;
            }
        }
        MainComponent mainComponent = AppComponentsHolder.b;
        if (mainComponent != null) {
            mainComponent.a(super.m0);
            super.b(bundle);
            return;
        }
        Intrinsics.a();
        throw null;
    }

    public abstract View c(int i2);

    public void a(View view, Bundle bundle) {
        if (view != null) {
            ((TextView) c(b.dlg_faq_tv_title)).setText(R());
            ((TextView) c(b.dlg_faq_tv_content)).setText(Q());
            return;
        }
        Intrinsics.a("view");
        throw null;
    }
}
