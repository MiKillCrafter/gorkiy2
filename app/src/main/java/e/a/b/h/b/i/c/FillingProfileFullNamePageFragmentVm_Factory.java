package e.a.b.h.b.i.c;

import e.a.a.a.e.o.c;
import e.a.a.a.e.q.b;
import e.a.b.d.b.a;
import e.a.b.h.b.g.g;
import k.a.Factory;
import m.a.Provider;

public final class FillingProfileFullNamePageFragmentVm_Factory implements Factory<e> {
    public final Provider<b> a;
    public final Provider<g> b;
    public final Provider<c> c;
    public final Provider<a> d;

    /* renamed from: e  reason: collision with root package name */
    public final Provider<e.a.a.g.b.b.a.b> f707e;

    public FillingProfileFullNamePageFragmentVm_Factory(Provider<b> provider, Provider<g> provider2, Provider<c> provider3, Provider<a> provider4, Provider<e.a.a.g.b.b.a.b> provider5) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.f707e = provider5;
    }

    public Object get() {
        return new FillingProfileFullNamePageFragmentVm(this.a.get(), this.b.get(), this.c.get(), this.d.get(), this.f707e.get());
    }
}
