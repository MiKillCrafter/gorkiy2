package e.a.b.h.b.g;

import androidx.fragment.app.Fragment;
import e.a.a.a.e.BaseScreens2;
import ru.covid19.droid.presentation.main.profileFilling.result.ProfileFillingResultFragment;

/* compiled from: Screens.kt */
public final class Screens10 extends BaseScreens2 {
    public static final Screens10 d = new Screens10();

    public Screens10() {
        super(null, 1);
    }

    public Fragment a() {
        return new ProfileFillingResultFragment();
    }
}
