package e.a.b.h.b.h.l;

import e.a.b.h.b.h.l.ProfileFillingAddChildFragmentViewState0;
import l.b.SingleSource;
import l.b.t.Function;
import n.n.c.Intrinsics;

/* compiled from: ProfileFillingAddChildFragmentVm.kt */
public final class ProfileFillingAddChildFragmentVm0<T, R> implements Function<T, SingleSource<? extends R>> {
    public final /* synthetic */ ProfileFillingAddChildFragmentVm a;

    public ProfileFillingAddChildFragmentVm0(ProfileFillingAddChildFragmentVm profileFillingAddChildFragmentVm) {
        this.a = profileFillingAddChildFragmentVm;
    }

    public Object a(Object obj) {
        if (((ProfileFillingAddChildFragmentViewState0.d) obj) != null) {
            return this.a.f691k.b();
        }
        Intrinsics.a("it");
        throw null;
    }
}
