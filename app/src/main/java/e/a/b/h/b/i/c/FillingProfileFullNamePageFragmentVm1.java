package e.a.b.h.b.i.c;

import e.a.a.a.e.p.BottomSheetSelectorNavigationDto0;
import e.a.b.h.b.i.c.FillingProfileFullNamePageFragmentViewState1;
import e.c.c.b;
import j.e.b.PublishRelay;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import l.b.t.Function;
import n.Unit;
import n.i.Collections;
import n.n.c.Intrinsics;
import ru.covid19.core.data.network.model.Citizenship;
import ru.covid19.core.data.network.model.CitizenshipResponse;
import ru.covid19.core.data.network.model.CitizenshipResponse0;
import ru.covid19.core.data.network.model.CitizenshipResponse1;

/* compiled from: FillingProfileFullNamePageFragmentVm.kt */
public final class FillingProfileFullNamePageFragmentVm1<T, R> implements Function<T, R> {
    public final /* synthetic */ FillingProfileFullNamePageFragmentVm a;

    public FillingProfileFullNamePageFragmentVm1(FillingProfileFullNamePageFragmentVm fillingProfileFullNamePageFragmentVm) {
        this.a = fillingProfileFullNamePageFragmentVm;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
    public Object a(Object obj) {
        CitizenshipResponse1 citizenshipResponse1 = (CitizenshipResponse1) obj;
        if (citizenshipResponse1 != null) {
            PublishRelay a2 = this.a.g;
            String citizenshipBottomSheetTitle = FillingProfileFullNamePageFragmentVm.b(this.a).a.getCitizenshipBottomSheetTitle();
            List<Citizenship> a3 = Collections.a(citizenshipResponse1.getCitizenshipsList());
            ArrayList arrayList = new ArrayList(Collections.a(a3, 10));
            Iterator<T> it = a3.iterator();
            while (it.hasNext()) {
                CitizenshipResponse citizenshipResponse = (CitizenshipResponse) it.next();
                arrayList.add(new CitizenshipResponse0(citizenshipResponse, citizenshipResponse.getName(), Intrinsics.a((Object) citizenshipResponse.getCode(), (Object) this.a.f705p.f().getPassenger().getCitizenship())));
            }
            a2.a((b) new FillingProfileFullNamePageFragmentViewState1.m(new BottomSheetSelectorNavigationDto0(citizenshipBottomSheetTitle, arrayList)));
            return Unit.a;
        }
        Intrinsics.a("it");
        throw null;
    }
}
