package e.a.b.h.b.f;

import e.c.c.BaseViewState1;
import j.a.a.a.outline;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;
import ru.covid19.droid.data.model.Statistic;

/* compiled from: MainFragmentViewState.kt */
public abstract class MainFragmentViewState1 extends BaseViewState1 {

    /* compiled from: MainFragmentViewState.kt */
    public static final class a extends MainFragmentViewState1 {
        public final Statistic a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Statistic statistic) {
            super(null);
            if (statistic != null) {
                this.a = statistic;
                return;
            }
            Intrinsics.a("statistic");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof a) && Intrinsics.a(this.a, ((a) obj).a);
            }
            return true;
        }

        public int hashCode() {
            Statistic statistic = this.a;
            if (statistic != null) {
                return statistic.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder a2 = outline.a("InitResult(statistic=");
            a2.append(this.a);
            a2.append(")");
            return a2.toString();
        }
    }

    public /* synthetic */ MainFragmentViewState1(DefaultConstructorMarker defaultConstructorMarker) {
    }
}
