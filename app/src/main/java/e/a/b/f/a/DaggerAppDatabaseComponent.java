package e.a.b.f.a;

import android.content.Context;
import e.a.a.i.b.AppComponent;
import e.a.b.f.c.AppDatabaseModule;
import e.a.b.f.c.AppDatabaseModule_ProvideDatabaseFactory;
import e.a.b.f.c.AppDatabaseModule_ProvidePrefsStorageFactory;
import e.c.a.c.a.a.IPrefsStorage;
import e.c.a.c.a.b.ISecuredPreferencesWrapper;
import i.s.RoomDatabase;
import i.s.h;
import k.a.DoubleCheck;
import m.a.Provider;

public final class DaggerAppDatabaseComponent implements AppDatabaseComponent {
    public Provider<Context> a;
    public Provider<h> b;
    public Provider<e.c.a.c.a.b.b> c;
    public Provider<e.c.a.c.a.a.a> d;

    public static class b implements Provider<e.c.a.c.a.b.b> {
        public final AppComponent a;

        public b(AppComponent appComponent) {
            this.a = appComponent;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
         arg types: [e.c.a.c.a.b.ISecuredPreferencesWrapper, java.lang.String]
         candidates:
          j.c.a.a.c.n.c.a(android.content.Context, int):float
          j.c.a.a.c.n.c.a(android.view.View, int):int
          j.c.a.a.c.n.c.a(byte[], int):long
          j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
          j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
          j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
          j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
          j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
          j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
          j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
          j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
          j.c.a.a.c.n.c.a(android.view.View, float):void
          j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
          j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
          j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
          j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
          j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
        public Object get() {
            ISecuredPreferencesWrapper c = this.a.c();
            j.c.a.a.c.n.c.a((Object) c, "Cannot return null from a non-@Nullable component method");
            return c;
        }
    }

    public static class c implements Provider<Context> {
        public final AppComponent a;

        public c(AppComponent appComponent) {
            this.a = appComponent;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
         arg types: [android.content.Context, java.lang.String]
         candidates:
          j.c.a.a.c.n.c.a(android.content.Context, int):float
          j.c.a.a.c.n.c.a(android.view.View, int):int
          j.c.a.a.c.n.c.a(byte[], int):long
          j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
          j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
          j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
          j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
          j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
          j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
          j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
          j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
          j.c.a.a.c.n.c.a(android.view.View, float):void
          j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
          j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
          j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
          j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
          j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
        public Object get() {
            Context b = this.a.b();
            j.c.a.a.c.n.c.a((Object) b, "Cannot return null from a non-@Nullable component method");
            return b;
        }
    }

    public /* synthetic */ DaggerAppDatabaseComponent(AppDatabaseModule appDatabaseModule, AppComponent appComponent, a aVar) {
        c cVar = new c(appComponent);
        this.a = cVar;
        this.b = DoubleCheck.a(new AppDatabaseModule_ProvideDatabaseFactory(appDatabaseModule, cVar));
        b bVar = new b(appComponent);
        this.c = bVar;
        this.d = DoubleCheck.a(new AppDatabaseModule_ProvidePrefsStorageFactory(appDatabaseModule, this.a, bVar));
    }

    public RoomDatabase a() {
        return this.b.get();
    }

    public IPrefsStorage b() {
        return this.d.get();
    }
}
