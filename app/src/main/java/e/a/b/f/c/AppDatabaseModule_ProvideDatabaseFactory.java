package e.a.b.f.c;

import android.app.ActivityManager;
import android.content.Context;
import com.crashlytics.android.answers.SessionEvent;
import e.a.b.d.c.a.DpDatabase_Impl;
import i.c.a.a.ArchTaskExecutor;
import i.s.DatabaseConfiguration;
import i.s.InvalidationTracker;
import i.s.MultiInstanceInvalidationClient;
import i.s.RoomDatabase;
import i.s.RoomOpenHelper;
import i.s.SQLiteCopyOpenHelper;
import i.s.h;
import i.v.a.SupportSQLiteOpenHelper;
import i.v.a.e.FrameworkSQLiteOpenHelperFactory;
import j.a.a.a.outline;
import j.c.a.a.c.n.c;
import java.util.ArrayDeque;
import java.util.concurrent.Executor;
import k.a.Factory;
import m.a.Provider;
import n.n.c.Intrinsics;
import ru.covid19.droid.data.storage.db.CovidDatabase;

public final class AppDatabaseModule_ProvideDatabaseFactory implements Factory<h> {
    public final AppDatabaseModule a;
    public final Provider<Context> b;

    public AppDatabaseModule_ProvideDatabaseFactory(d dVar, Provider<Context> provider) {
        this.a = dVar;
        this.b = provider;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [i.s.RoomDatabase, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
     arg types: [i.s.RoomDatabase, java.lang.String]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(android.view.View, int):int
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
    public Object get() {
        String str;
        AppDatabaseModule appDatabaseModule = this.a;
        Context context = this.b.get();
        if (appDatabaseModule == null) {
            throw null;
        } else if (context != null) {
            Class<CovidDatabase> cls = CovidDatabase.class;
            if ("CovidDatabase".trim().length() != 0) {
                RoomDatabase.b bVar = RoomDatabase.b.AUTOMATIC;
                RoomDatabase.c cVar = new RoomDatabase.c();
                Executor executor = ArchTaskExecutor.d;
                FrameworkSQLiteOpenHelperFactory frameworkSQLiteOpenHelperFactory = new FrameworkSQLiteOpenHelperFactory();
                if (bVar != null) {
                    if (bVar == RoomDatabase.b.AUTOMATIC) {
                        ActivityManager activityManager = (ActivityManager) context.getSystemService(SessionEvent.ACTIVITY_KEY);
                        if (activityManager == null || activityManager.isLowRamDevice()) {
                            bVar = RoomDatabase.b.TRUNCATE;
                        } else {
                            bVar = RoomDatabase.b.WRITE_AHEAD_LOGGING;
                        }
                    }
                    DatabaseConfiguration databaseConfiguration = r3;
                    DatabaseConfiguration databaseConfiguration2 = new DatabaseConfiguration(context, "CovidDatabase", frameworkSQLiteOpenHelperFactory, cVar, null, false, bVar, executor, executor, false, false, true, null, null, null);
                    String name = cls.getPackage().getName();
                    String canonicalName = cls.getCanonicalName();
                    boolean z = true;
                    if (!name.isEmpty()) {
                        canonicalName = canonicalName.substring(name.length() + 1);
                    }
                    String str2 = canonicalName.replace('.', '_') + "_Impl";
                    try {
                        if (name.isEmpty()) {
                            str = str2;
                        } else {
                            str = name + "." + str2;
                        }
                        RoomDatabase roomDatabase = (RoomDatabase) Class.forName(str).newInstance();
                        if (roomDatabase != null) {
                            DatabaseConfiguration databaseConfiguration3 = databaseConfiguration;
                            RoomOpenHelper roomOpenHelper = new RoomOpenHelper(databaseConfiguration3, new DpDatabase_Impl((ru.covid19.droid.data.storage.db.DpDatabase_Impl) roomDatabase, 1), "d0ad62d7a3cc5fec9606eeaa4ea045da", "769d38ca647fffdd76314d814cac4f83");
                            Context context2 = databaseConfiguration3.b;
                            String str3 = databaseConfiguration3.c;
                            if (context2 != null) {
                                SupportSQLiteOpenHelper a2 = databaseConfiguration3.a.a(new SupportSQLiteOpenHelper.b(context2, str3, roomOpenHelper));
                                roomDatabase.c = a2;
                                if (a2 instanceof SQLiteCopyOpenHelper) {
                                    ((SQLiteCopyOpenHelper) a2).f1439f = databaseConfiguration3;
                                }
                                if (databaseConfiguration3.g != RoomDatabase.b.WRITE_AHEAD_LOGGING) {
                                    z = false;
                                }
                                roomDatabase.c.a(z);
                                roomDatabase.g = databaseConfiguration3.f1415e;
                                roomDatabase.b = databaseConfiguration3.h;
                                new ArrayDeque();
                                roomDatabase.f1434e = databaseConfiguration3.f1416f;
                                roomDatabase.f1435f = z;
                                if (databaseConfiguration3.f1418j) {
                                    InvalidationTracker invalidationTracker = roomDatabase.d;
                                    new MultiInstanceInvalidationClient(databaseConfiguration3.b, databaseConfiguration3.c, invalidationTracker, invalidationTracker.d.b);
                                }
                                Intrinsics.a((Object) roomDatabase, "Room.databaseBuilder(con…on()\n            .build()");
                                c.a((Object) roomDatabase, "Cannot return null from a non-@Nullable @Provides method");
                                return roomDatabase;
                            }
                            throw new IllegalArgumentException("Must set a non-null context to create the configuration.");
                        }
                        throw null;
                    } catch (ClassNotFoundException unused) {
                        StringBuilder a3 = outline.a("cannot find implementation for ");
                        a3.append(cls.getCanonicalName());
                        a3.append(". ");
                        a3.append(str2);
                        a3.append(" does not exist");
                        throw new RuntimeException(a3.toString());
                    } catch (IllegalAccessException unused2) {
                        StringBuilder a4 = outline.a("Cannot access the constructor");
                        a4.append(cls.getCanonicalName());
                        throw new RuntimeException(a4.toString());
                    } catch (InstantiationException unused3) {
                        StringBuilder a5 = outline.a("Failed to create an instance of ");
                        a5.append(cls.getCanonicalName());
                        throw new RuntimeException(a5.toString());
                    }
                } else {
                    throw null;
                }
            } else {
                throw new IllegalArgumentException("Cannot build a database with null or empty name. If you are trying to create an in memory database, use Room.inMemoryDatabaseBuilder");
            }
        } else {
            Intrinsics.a("context");
            throw null;
        }
    }
}
