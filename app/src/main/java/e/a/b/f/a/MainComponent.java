package e.a.b.f.a;

import e.a.a.i.e.DeeplinkManagerWrapper;
import e.a.a.i.e.NavigationWrapper;
import e.a.a.i.e.NavigationWrapper0;
import e.c.d.b.c.VmFactoryWrapper;

/* compiled from: MainComponent.kt */
public interface MainComponent {
    void a(DeeplinkManagerWrapper deeplinkManagerWrapper);

    void a(NavigationWrapper0 navigationWrapper0);

    void a(NavigationWrapper navigationWrapper);

    void a(VmFactoryWrapper vmFactoryWrapper);
}
