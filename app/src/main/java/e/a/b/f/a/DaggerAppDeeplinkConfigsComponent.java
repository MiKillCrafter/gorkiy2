package e.a.b.f.a;

import e.a.a.h.IDeeplinkConfig;
import e.a.a.h.d;
import e.a.b.f.c.AppDeeplinkConfigsModule;
import e.a.b.f.c.h;
import k.a.DoubleCheck;
import m.a.Provider;

public final class DaggerAppDeeplinkConfigsComponent implements AppDeeplinkConfigsComponent {
    public Provider<d> a;

    public /* synthetic */ DaggerAppDeeplinkConfigsComponent(AppDeeplinkConfigsModule appDeeplinkConfigsModule, a aVar) {
        this.a = DoubleCheck.a(new h(appDeeplinkConfigsModule));
    }

    public IDeeplinkConfig a() {
        return this.a.get();
    }
}
