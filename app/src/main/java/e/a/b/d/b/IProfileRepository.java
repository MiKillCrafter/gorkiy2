package e.a.b.d.b;

import android.net.Uri;
import l.b.Completable;
import l.b.Single;
import o.i0;
import ru.covid19.core.data.network.model.CitizenshipsResponse;
import ru.covid19.core.data.network.model.CountriesResponse;
import ru.covid19.core.data.network.model.PersonalResponse;
import ru.covid19.droid.data.model.profileData.ProfileData;

/* compiled from: IProfileRepository.kt */
public interface IProfileRepository {
    Single<ProfileData> a();

    Single<i0> a(Uri uri, String str);

    Single<PersonalResponse> a(String str, String str2);

    PersonalResponse a(String str);

    void a(Uri uri);

    void a(ProfileData profileData);

    Single<CitizenshipsResponse> b();

    Single<i0> b(Uri uri, String str);

    void b(Uri uri);

    Single<CountriesResponse> c();

    Completable d();

    Uri e();

    ProfileData f();

    Uri g();
}
