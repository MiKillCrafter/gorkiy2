package e.a.b.d.a.a;

import l.b.Completable;
import l.b.Single;
import o.a0;
import o.g0;
import o.i0;
import r.l0.Body;
import r.l0.GET;
import r.l0.Multipart;
import r.l0.POST;
import r.l0.Part;
import ru.covid19.core.data.network.model.CountriesResponse;
import ru.covid19.core.data.network.model.GetCountriesRequestBody;
import ru.covid19.droid.data.model.profileData.ProfileData;

/* compiled from: ProfileDataApiService.kt */
public interface ProfileDataApiService {
    @POST("api/lk/v1/coronavirus/questionnaire/passenger")
    Completable a(@Body ProfileData profileData);

    @GET("api/lk/v1/coronavirus/questionnaire/passenger")
    Single<ProfileData> a();

    @Multipart
    @POST("api/storage/v1/files/upload")
    Single<i0> a(@Part("objectType") g0 g0Var, @Part("objectId") g0 g0Var2, @Part("mnemonic") g0 g0Var3, @Part("chunk") g0 g0Var4, @Part("chunks") g0 g0Var5, @Part("name") g0 g0Var6, @Part a0.c cVar);

    @POST("api/nsi/v1/dictionary/COUNTRIES_NO_RUSSIA")
    Single<CountriesResponse> a(@Body GetCountriesRequestBody getCountriesRequestBody);
}
