package e.a.b.g;

import android.app.DatePickerDialog;
import android.widget.DatePicker;
import android.widget.EditText;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import n.Unit;
import n.n.b.Functions0;
import n.n.c.Intrinsics;

/* compiled from: EditTextExtensions.kt */
public final class EditTextExtensions0 implements DatePickerDialog.OnDateSetListener {
    public final Calendar a;
    public final /* synthetic */ EditText b;
    public final /* synthetic */ String c;
    public final /* synthetic */ Functions0 d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ Date f669e;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Calendar, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public EditTextExtensions0(EditText editText, String str, Functions0 functions0, Date date) {
        this.b = editText;
        this.c = str;
        this.d = functions0;
        this.f669e = date;
        Calendar instance = Calendar.getInstance();
        Date date2 = this.f669e;
        if (date2 != null) {
            instance.setTime(date2);
        }
        Intrinsics.a((Object) instance, "Calendar.getInstance().a…e = initialDate\n        }");
        this.a = instance;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Date, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void onDateSet(DatePicker datePicker, int i2, int i3, int i4) {
        this.a.set(1, i2);
        this.a.set(2, i3);
        this.a.set(5, i4);
        EditText editText = this.b;
        Calendar calendar = this.a;
        String str = this.c;
        if (calendar == null) {
            Intrinsics.a("$this$formatToSting");
            throw null;
        } else if (str != null) {
            String format = new SimpleDateFormat(str, Locale.US).format(calendar.getTime());
            Intrinsics.a((Object) format, "sdf.format(this.time)");
            editText.setText(format);
            Functions0 functions0 = this.d;
            if (functions0 != null) {
                Date time = this.a.getTime();
                Intrinsics.a((Object) time, "calendar.time");
                Unit unit = (Unit) functions0.a(time);
            }
        } else {
            Intrinsics.a("format");
            throw null;
        }
    }
}
