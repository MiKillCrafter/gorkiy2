package e.a.b.c;

import com.crashlytics.android.Crashlytics;
import io.reactivex.exceptions.CompositeException;
import n.n.c.Intrinsics;
import t.a.Timber;

/* compiled from: CrashlyticsTree.kt */
public final class CrashlyticsTree extends Timber.b {
    public final String b = "priority";
    public final String c = "tag";
    public final String d = "message";

    public void a(int i2, String str, String str2, Throwable th) {
        if (str2 == null) {
            Intrinsics.a("message");
            throw null;
        } else if (i2 != 2 && i2 != 3 && i2 != 4) {
            Crashlytics.setInt(this.b, i2);
            Crashlytics.setString(this.c, str);
            Crashlytics.setString(this.d, str2);
            if (th == null) {
                Crashlytics.logException(new Exception(str2));
            } else if (th instanceof CompositeException) {
                Crashlytics.logException(((CompositeException) th).b.get(0));
            } else {
                Crashlytics.logException(th);
            }
        }
    }
}
