package e.c.d.a;

import j.e.b.BehaviorRelay;
import j.e.b.Relay;
import l.b.Observable;
import l.b.u.e.c.ObservableHide;
import n.n.c.Intrinsics;

/* compiled from: ViewState.kt */
public final class ViewState<T> extends Bind<T> {
    public final BehaviorRelay<T> a;
    public final Observable<T> b;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [j.e.b.BehaviorRelay<T>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.u.e.c.ObservableHide, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public ViewState(T t2) {
        BehaviorRelay<T> behaviorRelay;
        if (t2 == null) {
            behaviorRelay = new BehaviorRelay<>();
        } else {
            BehaviorRelay<T> behaviorRelay2 = new BehaviorRelay<>();
            behaviorRelay2.b.lazySet(t2);
            behaviorRelay = behaviorRelay2;
        }
        Intrinsics.a((Object) behaviorRelay, "if (initValue == null)\n …eateDefault<T>(initValue)");
        this.a = behaviorRelay;
        if (behaviorRelay != null) {
            ObservableHide observableHide = new ObservableHide(behaviorRelay);
            Intrinsics.a((Object) observableHide, "relay.hide()");
            this.b = observableHide;
            return;
        }
        throw null;
    }

    public Relay a() {
        return this.a;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ViewState(Object obj, int i2) {
        this((i2 & 1) != 0 ? null : obj);
    }
}
