package e.c.d.a;

import l.b.t.Consumer;
import n.n.b.Functions0;
import n.n.c.Intrinsics;

/* compiled from: Bind.kt */
public final class Bind0 implements Consumer {
    public final /* synthetic */ Functions0 b;

    public Bind0(Functions0 functions0) {
        this.b = functions0;
    }

    public final /* synthetic */ void a(Object obj) {
        Intrinsics.a(this.b.a(obj), "invoke(...)");
    }
}
