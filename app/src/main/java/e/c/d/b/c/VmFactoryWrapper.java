package e.c.d.b.c;

import i.o.ViewModelProvider;
import n.n.c.Intrinsics;

/* compiled from: VmFactoryWrapper.kt */
public final class VmFactoryWrapper {
    public ViewModelProvider.b a;

    public final ViewModelProvider.b a() {
        ViewModelProvider.b bVar = this.a;
        if (bVar != null) {
            return bVar;
        }
        Intrinsics.b("factory");
        throw null;
    }
}
