package e.c.a.a;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.security.KeyPairGeneratorSpec;
import android.security.keystore.KeyGenParameterSpec;
import android.util.Base64;
import java.math.BigInteger;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.util.Calendar;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.security.auth.x500.X500Principal;
import kotlin.TypeCastException;
import n.i.Collections;
import n.n.c.Intrinsics;
import n.n.c.Reflection;
import n.p.KClass;

/* compiled from: KeyStoreManager.kt */
public final class KeyStoreManager {
    public final SharedPreferences a;
    public final KeyStore b;
    public SecretKey c;
    public final Context d;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.security.KeyStore, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.SharedPreferences, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [byte[], java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [javax.crypto.SecretKey, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public KeyStoreManager(Context context) {
        String str;
        SecretKey secretKey = null;
        if (context != null) {
            this.d = context;
            this.a = context.getSharedPreferences("EISAO_KEYS_PREFERENCES", 0);
            KeyStore instance = KeyStore.getInstance("AndroidKeyStore");
            Intrinsics.a((Object) instance, "KeyStore.getInstance(ANDROID_KEY_STORE_TYPE_NAME)");
            this.b = instance;
            instance.load(null);
            Class<String> cls = String.class;
            SharedPreferences sharedPreferences = this.a;
            Intrinsics.a((Object) sharedPreferences, "sharedPreferences");
            if (!sharedPreferences.contains("EISAO_SYMMETRIC_KEY")) {
                str = null;
            } else {
                KClass a2 = Reflection.a(cls);
                if (Intrinsics.a(a2, Reflection.a(cls))) {
                    str = sharedPreferences.getString("EISAO_SYMMETRIC_KEY", "");
                    if (str == null) {
                        throw new TypeCastException("null cannot be cast to non-null type kotlin.String");
                    }
                } else if (Intrinsics.a(a2, Reflection.a(Integer.TYPE))) {
                    str = (String) Integer.valueOf(sharedPreferences.getInt("EISAO_SYMMETRIC_KEY", -1));
                } else if (Intrinsics.a(a2, Reflection.a(Float.TYPE))) {
                    str = (String) Float.valueOf(sharedPreferences.getFloat("EISAO_SYMMETRIC_KEY", -1.0f));
                } else if (Intrinsics.a(a2, Reflection.a(Long.TYPE))) {
                    str = (String) Long.valueOf(sharedPreferences.getLong("EISAO_SYMMETRIC_KEY", -1));
                } else if (Intrinsics.a(a2, Reflection.a(Boolean.TYPE))) {
                    str = (String) Boolean.valueOf(sharedPreferences.getBoolean("EISAO_SYMMETRIC_KEY", false));
                } else {
                    throw new UnsupportedOperationException("Not acceptable type");
                }
            }
            if (str != null) {
                byte[] decode = Base64.decode(str, 0);
                Intrinsics.a((Object) decode, "encodedSymmetricKey");
                Cipher instance2 = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                instance2.init(2, a().getPrivate());
                byte[] doFinal = instance2.doFinal(decode);
                Intrinsics.a((Object) doFinal, "cipher.doFinal(secretEncrypted)");
                secretKey = new SecretKeySpec(doFinal, "AES");
            }
            if (secretKey == null) {
                KeyGenerator instance3 = KeyGenerator.getInstance("AES");
                instance3.init(128);
                secretKey = instance3.generateKey();
                Intrinsics.a((Object) secretKey, "this");
                SharedPreferences sharedPreferences2 = this.a;
                Intrinsics.a((Object) sharedPreferences2, "sharedPreferences");
                byte[] encoded = secretKey.getEncoded();
                Intrinsics.a((Object) encoded, "secretKey.encoded");
                Cipher instance4 = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                instance4.init(1, a().getPublic());
                byte[] doFinal2 = instance4.doFinal(encoded);
                Intrinsics.a((Object) doFinal2, "cipher.doFinal(secret)");
                Collections.a(sharedPreferences2, "EISAO_SYMMETRIC_KEY", Base64.encodeToString(doFinal2, 0));
                Intrinsics.a((Object) secretKey, "generator.generateKey().…metricKey(this)\n        }");
            }
            this.c = secretKey;
            return;
        }
        Intrinsics.a("context");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.security.KeyPairGenerator, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.security.keystore.KeyGenParameterSpec$Builder, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Calendar, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.security.KeyPairGeneratorSpec$Builder, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.security.KeyPair, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public final KeyPair a() {
        Certificate certificate;
        PublicKey publicKey;
        KeyPair keyPair = null;
        Key key = this.b.getKey("EISAO_ASYMMETRIC_KEY_ALIAS", null);
        if (!(key instanceof PrivateKey)) {
            key = null;
        }
        PrivateKey privateKey = (PrivateKey) key;
        if (!(privateKey == null || (certificate = this.b.getCertificate("EISAO_ASYMMETRIC_KEY_ALIAS")) == null || (publicKey = certificate.getPublicKey()) == null)) {
            keyPair = new KeyPair(publicKey, privateKey);
        }
        if (keyPair != null) {
            return keyPair;
        }
        KeyPairGenerator instance = KeyPairGenerator.getInstance("RSA", "AndroidKeyStore");
        if (Build.VERSION.SDK_INT >= 23) {
            Intrinsics.a((Object) instance, "generator");
            KeyGenParameterSpec.Builder encryptionPaddings = new KeyGenParameterSpec.Builder("EISAO_ASYMMETRIC_KEY_ALIAS", 3).setBlockModes("ECB").setEncryptionPaddings("PKCS1Padding");
            Intrinsics.a((Object) encryptionPaddings, "KeyGenParameterSpec\n    …YPTION_PADDING_RSA_PKCS1)");
            instance.initialize(encryptionPaddings.build());
        } else {
            Intrinsics.a((Object) instance, "generator");
            Calendar instance2 = Calendar.getInstance();
            Calendar instance3 = Calendar.getInstance();
            instance3.add(1, 20);
            KeyPairGeneratorSpec.Builder subject = new KeyPairGeneratorSpec.Builder(this.d).setAlias("EISAO_ASYMMETRIC_KEY_ALIAS").setSerialNumber(BigInteger.ONE).setSubject(new X500Principal("CN=EISAO_ASYMMETRIC_KEY_ALIAS CA Certificate"));
            Intrinsics.a((Object) instance2, "startDate");
            KeyPairGeneratorSpec.Builder startDate = subject.setStartDate(instance2.getTime());
            Intrinsics.a((Object) instance3, "endDate");
            KeyPairGeneratorSpec.Builder endDate = startDate.setEndDate(instance3.getTime());
            Intrinsics.a((Object) endDate, "KeyPairGeneratorSpec.Bui….setEndDate(endDate.time)");
            instance.initialize(endDate.build());
        }
        KeyPair generateKeyPair = instance.generateKeyPair();
        Intrinsics.a((Object) generateKeyPair, "generator.generateKeyPair()");
        return generateKeyPair;
    }
}
