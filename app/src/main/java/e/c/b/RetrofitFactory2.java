package e.c.b;

import l.b.t.Predicate;
import n.n.c.Intrinsics;

/* compiled from: RetrofitFactory.kt */
public final class RetrofitFactory2<T> implements Predicate<Boolean> {
    public static final RetrofitFactory2 b = new RetrofitFactory2();

    public boolean a(Object obj) {
        Boolean bool = (Boolean) obj;
        if (bool != null) {
            return bool.booleanValue();
        }
        Intrinsics.a("it");
        throw null;
    }
}
