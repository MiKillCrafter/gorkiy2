package e.c.c;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import j.e.b.BehaviorRelay;
import j.e.b.PublishRelay;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import kotlin.TypeCastException;
import l.b.Observable;
import l.b.ObservableSource;
import l.b.s.CompositeDisposable;
import l.b.s.Disposable;
import l.b.t.Consumer;
import l.b.t.Function;
import l.b.u.b.ObjectHelper;
import l.b.u.e.c.ObservableDistinctUntilChanged;
import l.b.u.e.c.ObservablePublishSelector;
import n.Lazy;
import n.n.b.Functions;
import n.n.b.Functions0;
import n.n.c.Intrinsics;
import n.n.c.PropertyReference0Impl;
import n.n.c.Reflection;
import n.n.c.h;
import n.n.c.j;
import n.p.KDeclarationContainer;
import n.p.KProperty;

/* compiled from: BaseMviVm.kt */
public abstract class BaseMviVm1<VS> extends e.c.d.c.a {
    public static final /* synthetic */ KProperty[] h;
    public final BehaviorRelay<VS> d;

    /* renamed from: e  reason: collision with root package name */
    public final CacheSubject<l> f721e;

    /* renamed from: f  reason: collision with root package name */
    public final CompositeDisposable f722f;
    public final PublishRelay<b> g;

    /* compiled from: BaseMviVm.kt */
    public static final class a<T> implements Consumer<VS> {
        public final /* synthetic */ BaseMviVm1 b;

        public a(BaseMviVm1 baseMviVm1) {
            this.b = baseMviVm1;
        }

        public final void a(VS vs) {
            this.b.d.a((Object) vs);
        }
    }

    /* compiled from: BaseMviVm.kt */
    public static final class b<T> implements Consumer<Throwable> {
        public static final b b = new b();

        public void a(Object obj) {
            Log.d("BaseMviVm", "Event emitter error: " + ((Throwable) obj));
        }
    }

    /* compiled from: BaseMviVm.kt */
    public static final /* synthetic */ class c extends h implements Functions0<Observable<p>, Observable<BaseMviVm0<? extends k>>> {
        public c(BaseMviVm1 baseMviVm1) {
            super(1, baseMviVm1);
        }

        /* JADX WARN: Type inference failed for: r1v0, types: [e.c.c.BaseMviVm1$c, n.n.c.CallableReference] */
        public Object a(Object obj) {
            Observable observable = (Observable) obj;
            if (observable != null) {
                return ((BaseMviVm1) this.c).b((Observable<p>) observable);
            }
            Intrinsics.a("p1");
            throw null;
        }

        public final String f() {
            return "initEventProcessor";
        }

        /* JADX WARN: Type inference failed for: r0v1, types: [n.p.KClass, n.p.KDeclarationContainer] */
        public final KDeclarationContainer g() {
            return Reflection.a(BaseMviVm1.class);
        }

        public final String i() {
            return "initEventProcessor(Lio/reactivex/Observable;)Lio/reactivex/Observable;";
        }
    }

    /* compiled from: BaseMviVm.kt */
    public static final class d<T, R> implements Function<T, R> {
        public static final d a = new d();

        public Object a(Object obj) {
            if (((BaseViewState4) obj) != null) {
                return BaseMviVm.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: BaseMviVm.kt */
    public static final class e extends j implements Functions<VS> {
        public final /* synthetic */ BaseMviVm1 c;
        public final /* synthetic */ Object d;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(BaseMviVm1 baseMviVm1, Object obj) {
            super(0);
            this.c = baseMviVm1;
            this.d = obj;
        }

        public final VS b() {
            BaseMviVm1 baseMviVm1 = this.c;
            VS vs = this.d;
            if (vs == null) {
                Intrinsics.a();
                throw null;
            } else if (baseMviVm1 != null) {
                return vs;
            } else {
                throw null;
            }
        }
    }

    /* JADX WARN: Type inference failed for: r2v1, types: [n.p.KClass, n.p.KDeclarationContainer] */
    static {
        KProperty[] kPropertyArr = new KProperty[1];
        PropertyReference0Impl propertyReference0Impl = new PropertyReference0Impl(Reflection.a(BaseMviVm1.class), "modifiedRestoredVs", "<v#0>");
        if (Reflection.a != null) {
            kPropertyArr[0] = propertyReference0Impl;
            h = kPropertyArr;
            return;
        }
        throw null;
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [e.c.d.c.BaseVm, e.c.c.BaseMviVm1] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [j.e.b.BehaviorRelay<VS>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [j.e.b.PublishRelay<e.c.c.b>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [e.c.c.BaseMviVm2, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.u.e.c.ObservablePublishSelector, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.t.Function<java.lang.Object, java.lang.Object>, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.s.Disposable, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public BaseMviVm1() {
        Object d2 = d();
        BehaviorRelay<VS> behaviorRelay = new BehaviorRelay<>();
        if (d2 != null) {
            behaviorRelay.b.lazySet(d2);
            Intrinsics.a((Object) behaviorRelay, "BehaviorRelay.createDefault(viewStateScheme)");
            this.d = behaviorRelay;
            this.f721e = new CacheSubject<>();
            this.f722f = new CompositeDisposable();
            PublishRelay<b> publishRelay = new PublishRelay<>();
            Intrinsics.a((Object) publishRelay, "PublishRelay.create<BaseEvent>()");
            this.g = publishRelay;
            BaseMviVm2 baseMviVm2 = new BaseMviVm2(this);
            ObjectHelper.a((Object) baseMviVm2, "selector is null");
            ObservablePublishSelector observablePublishSelector = new ObservablePublishSelector(publishRelay, baseMviVm2);
            Intrinsics.a((Object) observablePublishSelector, "this.publish { bindEventsWithProcessors(it) }");
            Observable c2 = observablePublishSelector.c((Function) new BaseMviVm3(this));
            Intrinsics.a((Object) c2, "this.map { applyResultsT…(viewState.value!!, it) }");
            Function<Object, Object> function = l.b.u.b.Functions.a;
            ObjectHelper.a((Object) function, "keySelector is null");
            Disposable a2 = new ObservableDistinctUntilChanged(c2, function, ObjectHelper.a).a(new a(this), b.b);
            Intrinsics.a((Object) a2, "eventEmitter\n           …t emitter error: $it\") })");
            j.c.a.a.c.n.c.a(a2, this.b);
            return;
        }
        throw new NullPointerException("defaultValue == null");
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [e.c.d.c.BaseVm, e.c.c.BaseMviVm1] */
    public final void a(Bundle bundle) {
        if (!this.c) {
            c();
            Object obj = null;
            Parcelable parcelable = bundle != null ? bundle.getParcelable("VS") : null;
            if (!(parcelable instanceof Object)) {
                parcelable = null;
            }
            Lazy a2 = j.c.a.a.c.n.c.a((Functions) new e(this, parcelable));
            KProperty kProperty = h[0];
            if (parcelable == null || a2.getValue() == null) {
                this.g.a((b) BaseViewState4.b);
            } else {
                BehaviorRelay<VS> behaviorRelay = this.d;
                Object value = a2.getValue();
                if (value != null) {
                    a(value);
                    obj = value;
                }
                behaviorRelay.a(obj);
            }
            this.c = true;
        }
    }

    public void a(Object obj) {
    }

    public VS b(VS vs) {
        return vs;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<R>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Observable<BaseMviVm0<? extends k>> b(Observable<p> observable) {
        if (observable != null) {
            Observable<R> c2 = observable.c((Function) d.a);
            Intrinsics.a((Object) c2, "observable.map {\n            IgnoreLce\n        }");
            return c2;
        }
        Intrinsics.a("observable");
        throw null;
    }

    public void c() {
    }

    public abstract VS d();

    public final VS e() {
        VS c2 = this.d.c();
        if (c2 != null) {
            return c2;
        }
        Intrinsics.a();
        throw null;
    }

    public final void b(Bundle bundle) {
        VS vs = null;
        if (bundle != null) {
            VS c2 = this.d.c();
            if (c2 instanceof Parcelable) {
                vs = c2;
            }
            Parcelable parcelable = (Parcelable) vs;
            if (parcelable != null) {
                bundle.putParcelable("VS", parcelable);
                return;
            }
            return;
        }
        Intrinsics.a("bundle");
        throw null;
    }

    @SuppressLint({"CheckResult"})
    public void a(List<? extends Observable<? extends b>> list) {
        if (list == null) {
            Intrinsics.a("es");
            throw null;
        } else if (!list.isEmpty()) {
            CompositeDisposable compositeDisposable = this.f722f;
            ArrayList arrayList = new ArrayList();
            for (T next : list) {
                if (next != null) {
                    arrayList.add(next);
                }
            }
            Object[] array = arrayList.toArray(new Observable[0]);
            if (array != null) {
                ObservableSource[] observableSourceArr = (ObservableSource[]) array;
                compositeDisposable.c(Observable.a((ObservableSource[]) Arrays.copyOf(observableSourceArr, observableSourceArr.length)).a(this.g));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    public VS a(VS vs, k kVar) {
        if (kVar != null) {
            return vs;
        }
        Intrinsics.a("result");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<U>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<e.c.c.BaseMviVm0<? extends e.c.c.k>>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Observable<BaseMviVm0<? extends k>> a(Observable<? extends b> observable) {
        if (observable != null) {
            Observable<U> a2 = observable.a(BaseViewState4.class);
            Intrinsics.a((Object) a2, "ofType(R::class.java)");
            Observable<BaseMviVm0<? extends k>> a3 = Observable.a(BaseMviVm.b(a2, new c(this)));
            Intrinsics.a((Object) a3, "Observable.mergeArray(\n …EventProcessor)\n        )");
            return a3;
        }
        Intrinsics.a("o");
        throw null;
    }
}
