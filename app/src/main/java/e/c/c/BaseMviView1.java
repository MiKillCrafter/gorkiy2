package e.c.c;

import e.c.c.BaseMviVm1;
import e.c.d.b.BaveMvvmView;
import j.e.b.PublishRelay;
import java.util.List;
import l.b.Observable;

/* compiled from: BaseMviView.kt */
public interface BaseMviView1<VS, VM extends BaseMviVm1<VS>> extends BaveMvvmView<VM> {
    void a(BaseViewState2 baseViewState2);

    void a(VS vs);

    PublishRelay<b> b();

    List<Observable<? extends b>> g();
}
