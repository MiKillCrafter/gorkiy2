package e.c.c;

import j.a.a.a.outline;
import java.util.List;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;

/* compiled from: BaseMviVm.kt */
public abstract class BaseMviVm0<T> {

    /* compiled from: BaseMviVm.kt */
    public static final class a<T> extends BaseMviVm0<T> {
        public final T a;

        public a(T t2) {
            super(null);
            this.a = t2;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof a) && Intrinsics.a(this.a, ((a) obj).a);
            }
            return true;
        }

        public int hashCode() {
            T t2 = this.a;
            if (t2 != null) {
                return t2.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder a2 = outline.a("Content(packet=");
            a2.append((Object) this.a);
            a2.append(")");
            return a2.toString();
        }
    }

    /* compiled from: BaseMviVm.kt */
    public static final class b<T> extends BaseMviVm0<T> {
        public final Class<T> a;
        public final List<a> b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(Class<T> cls, List<? extends a> list) {
            super(null);
            if (cls != null) {
                this.a = cls;
                this.b = list;
                return;
            }
            Intrinsics.a("packetClass");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return Intrinsics.a(this.a, bVar.a) && Intrinsics.a(this.b, bVar.b);
        }

        public int hashCode() {
            Class<T> cls = this.a;
            int i2 = 0;
            int hashCode = (cls != null ? cls.hashCode() : 0) * 31;
            List<a> list = this.b;
            if (list != null) {
                i2 = list.hashCode();
            }
            return hashCode + i2;
        }

        public String toString() {
            StringBuilder a2 = outline.a("Error(packetClass=");
            a2.append(this.a);
            a2.append(", errors=");
            a2.append(this.b);
            a2.append(")");
            return a2.toString();
        }
    }

    /* compiled from: BaseMviVm.kt */
    public static final class c<T> extends BaseMviVm0<T> {
        public c() {
            super(null);
        }
    }

    public BaseMviVm0() {
    }

    public /* synthetic */ BaseMviVm0(DefaultConstructorMarker defaultConstructorMarker) {
    }
}
