package e.c.c;

import e.c.c.BaseMviVm0;
import kotlin.NoWhenBranchMatchedException;
import l.b.t.Function;
import n.n.c.Intrinsics;

/* compiled from: BaseMviVm.kt */
public final class BaseMviVm3<T, R> implements Function<T, R> {
    public final /* synthetic */ BaseMviVm1 a;

    public BaseMviVm3(BaseMviVm1 baseMviVm1) {
        this.a = baseMviVm1;
    }

    public Object a(Object obj) {
        BaseMviVm0 baseMviVm0 = (BaseMviVm0) obj;
        if (baseMviVm0 != null) {
            BaseMviVm1 baseMviVm1 = this.a;
            VS c = baseMviVm1.d.c();
            if (c != null) {
                if (baseMviVm0 instanceof BaseMviVm0.a) {
                    c = baseMviVm1.a(c, (BaseViewState1) ((BaseMviVm0.a) baseMviVm0).a);
                } else if (baseMviVm0 instanceof BaseMviVm0.c) {
                    c = baseMviVm1.b((Object) c);
                } else if (!(baseMviVm0 instanceof BaseMviVm0.b)) {
                    throw new NoWhenBranchMatchedException();
                } else if (((BaseMviVm0.b) baseMviVm0).a == null) {
                    Intrinsics.a("resultClass");
                    throw null;
                }
                baseMviVm1.a((Object) c);
                return c;
            }
            Intrinsics.a();
            throw null;
        }
        Intrinsics.a("it");
        throw null;
    }
}
