package e.c.c;

import android.os.Bundle;
import e.c.c.BaseMviVm1;
import e.c.d.b.b.BaseMvvmActivity;
import j.c.a.a.c.n.c;
import j.e.b.PublishRelay;
import java.util.List;
import l.b.Observable;
import n.i.Collections;
import n.n.c.Intrinsics;

/* compiled from: BaseMviActivity.kt */
public abstract class BaseMviActivity<VS, VM extends BaseMviVm1<VS>> extends BaseMvvmActivity<VM> implements BaseMviView1<VS, VM> {
    public final PublishRelay<b> u;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [j.e.b.PublishRelay<e.c.c.b>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public BaseMviActivity() {
        PublishRelay<b> publishRelay = new PublishRelay<>();
        Intrinsics.a((Object) publishRelay, "PublishRelay.create()");
        this.u = publishRelay;
    }

    public void a(BaseViewState2 baseViewState2) {
        if (baseViewState2 == null) {
            Intrinsics.a("ve");
            throw null;
        }
    }

    public void a(VS vs) {
    }

    public PublishRelay<b> b() {
        return this.u;
    }

    public List<Observable<? extends b>> g() {
        return c.c(b());
    }

    public void o() {
        Collections.a((BaseMviView1) this);
    }

    public void onCreate(Bundle bundle) {
        ((BaseMviVm1) h()).a(bundle);
        super.onCreate(bundle);
    }

    public void onPause() {
        super.onPause();
        ((BaseMviVm1) h()).f722f.a();
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [e.c.d.b.b.BaseMvvmActivity, i.b.k.AppCompatActivity, e.c.c.BaseMviActivity] */
    public void onSaveInstanceState(Bundle bundle) {
        if (bundle != null) {
            BaseMviActivity.super.onSaveInstanceState(bundle);
            ((BaseMviVm1) h()).b(bundle);
            return;
        }
        Intrinsics.a("outState");
        throw null;
    }
}
