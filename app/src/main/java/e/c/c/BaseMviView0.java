package e.c.c;

import l.b.t.Consumer;
import n.n.c.Intrinsics;

/* compiled from: BaseMviView.kt */
public final class BaseMviView0<T> implements Consumer<l> {
    public final /* synthetic */ BaseMviView1 b;

    public BaseMviView0(BaseMviView1 baseMviView1) {
        this.b = baseMviView1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [e.c.c.BaseViewState2, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void a(Object obj) {
        BaseViewState2 baseViewState2 = (BaseViewState2) obj;
        BaseMviView1 baseMviView1 = this.b;
        Intrinsics.a((Object) baseViewState2, "it");
        baseMviView1.a(baseViewState2);
    }
}
