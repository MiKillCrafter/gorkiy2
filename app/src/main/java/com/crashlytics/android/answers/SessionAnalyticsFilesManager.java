package com.crashlytics.android.answers;

import android.content.Context;
import j.a.a.a.outline;
import java.util.UUID;
import l.a.a.a.o.b.CurrentTimeProvider;
import l.a.a.a.o.b.SystemCurrentTimeProvider;
import l.a.a.a.o.d.EventsFilesManager;
import l.a.a.a.o.d.EventsStorage;
import l.a.a.a.o.g.AnalyticsSettingsData;

public class SessionAnalyticsFilesManager extends EventsFilesManager<SessionEvent> {
    public static final String SESSION_ANALYTICS_TO_SEND_FILE_EXTENSION = ".tap";
    public static final String SESSION_ANALYTICS_TO_SEND_FILE_PREFIX = "sa";
    public AnalyticsSettingsData analyticsSettingsData;

    public SessionAnalyticsFilesManager(Context context, SessionEventTransform sessionEventTransform, CurrentTimeProvider currentTimeProvider, EventsStorage eventsStorage) {
        super(context, sessionEventTransform, currentTimeProvider, eventsStorage, 100);
    }

    public String generateUniqueRollOverFileName() {
        UUID randomUUID = UUID.randomUUID();
        StringBuilder b = outline.b(SESSION_ANALYTICS_TO_SEND_FILE_PREFIX, EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
        b.append(randomUUID.toString());
        b.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
        if (((SystemCurrentTimeProvider) super.currentTimeProvider) != null) {
            b.append(System.currentTimeMillis());
            b.append(SESSION_ANALYTICS_TO_SEND_FILE_EXTENSION);
            return b.toString();
        }
        throw null;
    }

    public int getMaxByteSizePerFile() {
        AnalyticsSettingsData analyticsSettingsData2 = this.analyticsSettingsData;
        return analyticsSettingsData2 == null ? super.getMaxByteSizePerFile() : analyticsSettingsData2.c;
    }

    public int getMaxFilesToKeep() {
        AnalyticsSettingsData analyticsSettingsData2 = this.analyticsSettingsData;
        return analyticsSettingsData2 == null ? super.getMaxFilesToKeep() : analyticsSettingsData2.d;
    }

    public void setAnalyticsSettingsData(AnalyticsSettingsData analyticsSettingsData2) {
        this.analyticsSettingsData = analyticsSettingsData2;
    }
}
