package com.crashlytics.android.answers;

import android.content.Context;
import android.os.Looper;
import l.a.a.a.o.b.SystemCurrentTimeProvider;
import l.a.a.a.o.d.GZIPQueueFileEventStorage;
import l.a.a.a.o.f.FileStore;
import l.a.a.a.o.f.FileStoreImpl;

public class AnswersFilesManagerProvider {
    public static final String SESSION_ANALYTICS_FILE_NAME = "session_analytics.tap";
    public static final String SESSION_ANALYTICS_TO_SEND_DIR = "session_analytics_to_send";
    public final Context context;
    public final FileStore fileStore;

    public AnswersFilesManagerProvider(Context context2, FileStore fileStore2) {
        this.context = context2;
        this.fileStore = fileStore2;
    }

    public SessionAnalyticsFilesManager getAnalyticsFilesManager() {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            return new SessionAnalyticsFilesManager(this.context, new SessionEventTransform(), new SystemCurrentTimeProvider(), new GZIPQueueFileEventStorage(this.context, ((FileStoreImpl) this.fileStore).a(), SESSION_ANALYTICS_FILE_NAME, SESSION_ANALYTICS_TO_SEND_DIR));
        }
        throw new IllegalStateException("AnswersFilesManagerProvider cannot be called on the main thread");
    }
}
