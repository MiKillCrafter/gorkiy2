package com.crashlytics.android.answers;

import l.a.a.a.o.c.m.RetryState;

public class RetryManager {
    public static final long NANOSECONDS_IN_MS = 1000000;
    public long lastRetry;
    public RetryState retryState;

    public RetryManager(RetryState retryState2) {
        if (retryState2 != null) {
            this.retryState = retryState2;
            return;
        }
        throw new NullPointerException("retryState must not be null");
    }

    public boolean canRetry(long j2) {
        RetryState retryState2 = this.retryState;
        return j2 - this.lastRetry >= retryState2.b.getDelayMillis(retryState2.a) * NANOSECONDS_IN_MS;
    }

    public void recordRetry(long j2) {
        this.lastRetry = j2;
        RetryState retryState2 = this.retryState;
        this.retryState = new RetryState(retryState2.a + 1, retryState2.b, retryState2.c);
    }

    public void reset() {
        this.lastRetry = 0;
        RetryState retryState2 = this.retryState;
        this.retryState = new RetryState(retryState2.b, retryState2.c);
    }
}
