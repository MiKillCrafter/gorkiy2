package com.crashlytics.android.answers;

import android.content.Context;
import android.util.Log;
import com.crashlytics.android.answers.SessionEvent;
import java.util.concurrent.ScheduledExecutorService;
import l.a.a.a.Fabric;
import l.a.a.a.Kit;
import l.a.a.a.o.d.EventsStorageListener;
import l.a.a.a.o.e.HttpRequestFactory;
import l.a.a.a.o.g.AnalyticsSettingsData;

public class AnswersEventsHandler implements EventsStorageListener {
    public final Context context;
    public final ScheduledExecutorService executor;
    public final AnswersFilesManagerProvider filesManagerProvider;
    public final FirebaseAnalyticsApiAdapter firebaseAnalyticsApiAdapter;
    public final Kit kit;
    public final SessionMetadataCollector metadataCollector;
    public final HttpRequestFactory requestFactory;
    public SessionAnalyticsManagerStrategy strategy = new DisabledSessionAnalyticsManagerStrategy();

    public AnswersEventsHandler(Kit kit2, Context context2, AnswersFilesManagerProvider answersFilesManagerProvider, SessionMetadataCollector sessionMetadataCollector, HttpRequestFactory httpRequestFactory, ScheduledExecutorService scheduledExecutorService, FirebaseAnalyticsApiAdapter firebaseAnalyticsApiAdapter2) {
        this.kit = kit2;
        this.context = context2;
        this.filesManagerProvider = answersFilesManagerProvider;
        this.metadataCollector = sessionMetadataCollector;
        this.requestFactory = httpRequestFactory;
        this.executor = scheduledExecutorService;
        this.firebaseAnalyticsApiAdapter = firebaseAnalyticsApiAdapter2;
    }

    private void executeAsync(Runnable runnable) {
        try {
            this.executor.submit(runnable);
        } catch (Exception e2) {
            if (Fabric.a().a(Answers.TAG, 6)) {
                Log.e(Answers.TAG, "Failed to submit events task", e2);
            }
        }
    }

    private void executeSync(Runnable runnable) {
        try {
            this.executor.submit(runnable).get();
        } catch (Exception e2) {
            if (Fabric.a().a(Answers.TAG, 6)) {
                Log.e(Answers.TAG, "Failed to run events task", e2);
            }
        }
    }

    public void disable() {
        executeAsync(new Runnable() {
            /* class com.crashlytics.android.answers.AnswersEventsHandler.AnonymousClass2 */

            public void run() {
                try {
                    SessionAnalyticsManagerStrategy sessionAnalyticsManagerStrategy = AnswersEventsHandler.this.strategy;
                    AnswersEventsHandler.this.strategy = new DisabledSessionAnalyticsManagerStrategy();
                    sessionAnalyticsManagerStrategy.deleteAllEvents();
                } catch (Exception e2) {
                    if (Fabric.a().a(Answers.TAG, 6)) {
                        Log.e(Answers.TAG, "Failed to disable events", e2);
                    }
                }
            }
        });
    }

    public void enable() {
        executeAsync(new Runnable() {
            /* class com.crashlytics.android.answers.AnswersEventsHandler.AnonymousClass4 */

            public void run() {
                try {
                    SessionEventMetadata metadata = AnswersEventsHandler.this.metadataCollector.getMetadata();
                    SessionAnalyticsFilesManager analyticsFilesManager = AnswersEventsHandler.this.filesManagerProvider.getAnalyticsFilesManager();
                    analyticsFilesManager.registerRollOverListener(AnswersEventsHandler.this);
                    AnswersEventsHandler.this.strategy = new EnabledSessionAnalyticsManagerStrategy(AnswersEventsHandler.this.kit, AnswersEventsHandler.this.context, AnswersEventsHandler.this.executor, analyticsFilesManager, AnswersEventsHandler.this.requestFactory, metadata, AnswersEventsHandler.this.firebaseAnalyticsApiAdapter);
                } catch (Exception e2) {
                    if (Fabric.a().a(Answers.TAG, 6)) {
                        Log.e(Answers.TAG, "Failed to enable events", e2);
                    }
                }
            }
        });
    }

    public void flushEvents() {
        executeAsync(new Runnable() {
            /* class com.crashlytics.android.answers.AnswersEventsHandler.AnonymousClass5 */

            public void run() {
                try {
                    AnswersEventsHandler.this.strategy.rollFileOver();
                } catch (Exception e2) {
                    if (Fabric.a().a(Answers.TAG, 6)) {
                        Log.e(Answers.TAG, "Failed to flush events", e2);
                    }
                }
            }
        });
    }

    public void onRollOver(String str) {
        executeAsync(new Runnable() {
            /* class com.crashlytics.android.answers.AnswersEventsHandler.AnonymousClass3 */

            public void run() {
                try {
                    AnswersEventsHandler.this.strategy.sendEvents();
                } catch (Exception e2) {
                    if (Fabric.a().a(Answers.TAG, 6)) {
                        Log.e(Answers.TAG, "Failed to send events files", e2);
                    }
                }
            }
        });
    }

    public void processEvent(final SessionEvent.Builder builder, boolean z, final boolean z2) {
        AnonymousClass6 r0 = new Runnable() {
            /* class com.crashlytics.android.answers.AnswersEventsHandler.AnonymousClass6 */

            public void run() {
                try {
                    AnswersEventsHandler.this.strategy.processEvent(builder);
                    if (z2) {
                        AnswersEventsHandler.this.strategy.rollFileOver();
                    }
                } catch (Exception e2) {
                    if (Fabric.a().a(Answers.TAG, 6)) {
                        Log.e(Answers.TAG, "Failed to process event", e2);
                    }
                }
            }
        };
        if (z) {
            executeSync(r0);
        } else {
            executeAsync(r0);
        }
    }

    public void processEventAsync(SessionEvent.Builder builder) {
        processEvent(builder, false, false);
    }

    public void processEventAsyncAndFlush(SessionEvent.Builder builder) {
        processEvent(builder, false, true);
    }

    public void processEventSync(SessionEvent.Builder builder) {
        processEvent(builder, true, false);
    }

    public void setAnalyticsSettingsData(final AnalyticsSettingsData analyticsSettingsData, final String str) {
        executeAsync(new Runnable() {
            /* class com.crashlytics.android.answers.AnswersEventsHandler.AnonymousClass1 */

            public void run() {
                try {
                    AnswersEventsHandler.this.strategy.setAnalyticsSettingsData(analyticsSettingsData, str);
                } catch (Exception e2) {
                    if (Fabric.a().a(Answers.TAG, 6)) {
                        Log.e(Answers.TAG, "Failed to set analytics settings data", e2);
                    }
                }
            }
        });
    }
}
