package com.bumptech.glide;

import com.bumptech.glide.load.ImageHeaderParser;
import i.h.k.Pools;
import j.b.a.m.l.DataRewinderRegistry;
import j.b.a.m.n.ModelLoader;
import j.b.a.m.n.ModelLoaderRegistry;
import j.b.a.m.o.h.TranscoderRegistry;
import j.b.a.p.EncoderRegistry;
import j.b.a.p.ImageHeaderParserRegistry;
import j.b.a.p.LoadPathCache;
import j.b.a.p.ModelToResourceClassCache;
import j.b.a.p.ResourceDecoderRegistry;
import j.b.a.p.ResourceEncoderRegistry;
import j.b.a.s.k.FactoryPools;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Registry {
    public final ModelLoaderRegistry a;
    public final EncoderRegistry b;
    public final ResourceDecoderRegistry c;
    public final ResourceEncoderRegistry d;

    /* renamed from: e  reason: collision with root package name */
    public final DataRewinderRegistry f368e;

    /* renamed from: f  reason: collision with root package name */
    public final TranscoderRegistry f369f;
    public final ImageHeaderParserRegistry g;
    public final ModelToResourceClassCache h = new ModelToResourceClassCache();

    /* renamed from: i  reason: collision with root package name */
    public final LoadPathCache f370i = new LoadPathCache();

    /* renamed from: j  reason: collision with root package name */
    public final Pools<List<Throwable>> f371j;

    public static class MissingComponentException extends RuntimeException {
        public MissingComponentException(String str) {
            super(str);
        }
    }

    public static final class NoImageHeaderParserException extends MissingComponentException {
        public NoImageHeaderParserException() {
            super("Failed to find image header parser.");
        }
    }

    public static class NoModelLoaderAvailableException extends MissingComponentException {
        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public NoModelLoaderAvailableException(java.lang.Object r2) {
            /*
                r1 = this;
                java.lang.String r0 = "Failed to find any ModelLoaders registered for model class: "
                java.lang.StringBuilder r0 = j.a.a.a.outline.a(r0)
                java.lang.Class r2 = r2.getClass()
                r0.append(r2)
                java.lang.String r2 = r0.toString()
                r1.<init>(r2)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.Registry.NoModelLoaderAvailableException.<init>(java.lang.Object):void");
        }

        public <M> NoModelLoaderAvailableException(M m2, List<ModelLoader<M, ?>> list) {
            super("Found ModelLoaders for model class: " + list + ", but none that handle this specific model instance: " + ((Object) m2));
        }

        public NoModelLoaderAvailableException(Class<?> cls, Class<?> cls2) {
            super("Failed to find any ModelLoaders for model: " + cls + " and data: " + cls2);
        }
    }

    public static class NoResultEncoderAvailableException extends MissingComponentException {
        public NoResultEncoderAvailableException(Class<?> cls) {
            super("Failed to find result encoder for resource class: " + cls + ", you may need to consider registering a new Encoder for the requested type or DiskCacheStrategy.DATA/DiskCacheStrategy.NONE if caching your transformed resource is unnecessary.");
        }
    }

    public static class NoSourceEncoderAvailableException extends MissingComponentException {
        public NoSourceEncoderAvailableException(Class<?> cls) {
            super("Failed to find source encoder for data class: " + cls);
        }
    }

    public Registry() {
        Pools<List<Throwable>> a2 = FactoryPools.a();
        this.f371j = a2;
        this.a = new ModelLoaderRegistry(a2);
        this.b = new EncoderRegistry();
        this.c = new ResourceDecoderRegistry();
        this.d = new ResourceEncoderRegistry();
        this.f368e = new DataRewinderRegistry();
        this.f369f = new TranscoderRegistry();
        this.g = new ImageHeaderParserRegistry();
        List asList = Arrays.asList("Gif", "Bitmap", "BitmapDrawable");
        ArrayList arrayList = new ArrayList(asList.size());
        arrayList.addAll(asList);
        arrayList.add(0, "legacy_prepend_all");
        arrayList.add("legacy_append");
        this.c.a(arrayList);
    }

    public <Model> List<ModelLoader<Model, ?>> a(Model model) {
        ModelLoaderRegistry modelLoaderRegistry = this.a;
        if (modelLoaderRegistry != null) {
            List b2 = modelLoaderRegistry.b(model.getClass());
            if (!b2.isEmpty()) {
                int size = b2.size();
                List<ModelLoader<Model, ?>> emptyList = Collections.emptyList();
                boolean z = true;
                for (int i2 = 0; i2 < size; i2++) {
                    ModelLoader modelLoader = (ModelLoader) b2.get(i2);
                    if (modelLoader.a(model)) {
                        if (z) {
                            emptyList = new ArrayList<>(size - i2);
                            z = false;
                        }
                        emptyList.add(modelLoader);
                    }
                }
                if (!emptyList.isEmpty()) {
                    return emptyList;
                }
                throw new NoModelLoaderAvailableException(model, b2);
            }
            throw new NoModelLoaderAvailableException(model);
        }
        throw null;
    }

    public List<ImageHeaderParser> a() {
        List<ImageHeaderParser> a2 = this.g.a();
        if (!a2.isEmpty()) {
            return a2;
        }
        throw new NoImageHeaderParserException();
    }
}
