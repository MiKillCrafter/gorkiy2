package com.google.android.gms.measurement.internal;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.common.util.DynamiteApi;
import i.b.k.ResourcesFlusher;
import i.e.ArrayMap;
import j.c.a.a.f.e.db;
import j.c.a.a.f.e.fb;
import j.c.a.a.f.e.gb;
import j.c.a.a.f.e.lb;
import j.c.a.a.f.e.nb;
import j.c.a.a.g.a.a9;
import j.c.a.a.g.a.b6;
import j.c.a.a.g.a.b7;
import j.c.a.a.g.a.b8;
import j.c.a.a.g.a.b9;
import j.c.a.a.g.a.c6;
import j.c.a.a.g.a.e6;
import j.c.a.a.g.a.h;
import j.c.a.a.g.a.i;
import j.c.a.a.g.a.i6;
import j.c.a.a.g.a.j6;
import j.c.a.a.g.a.k6;
import j.c.a.a.g.a.l4;
import j.c.a.a.g.a.l6;
import j.c.a.a.g.a.m6;
import j.c.a.a.g.a.n6;
import j.c.a.a.g.a.p4;
import j.c.a.a.g.a.p6;
import j.c.a.a.g.a.q6;
import j.c.a.a.g.a.r4;
import j.c.a.a.g.a.s5;
import j.c.a.a.g.a.v5;
import j.c.a.a.g.a.w6;
import j.c.a.a.g.a.x5;
import j.c.a.a.g.a.y6;
import j.c.a.a.g.a.y8;
import j.c.a.a.g.a.z5;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

@DynamiteApi
/* compiled from: com.google.android.gms:play-services-measurement-sdk@@17.0.1 */
public class AppMeasurementDynamiteService extends db {
    public r4 a = null;
    public Map<Integer, v5> b = new ArrayMap();

    /* compiled from: com.google.android.gms:play-services-measurement-sdk@@17.0.1 */
    public class a implements v5 {
        public gb a;

        public a(gb gbVar) {
            this.a = gbVar;
        }

        public final void onEvent(String str, String str2, Bundle bundle, long j2) {
            try {
                this.a.a(str, str2, bundle, j2);
            } catch (RemoteException e2) {
                AppMeasurementDynamiteService.this.a.a().f2047i.a("Event listener threw exception", e2);
            }
        }
    }

    /* compiled from: com.google.android.gms:play-services-measurement-sdk@@17.0.1 */
    public class b implements s5 {
        public gb a;

        public b(gb gbVar) {
            this.a = gbVar;
        }
    }

    public final void a() {
        if (this.a == null) {
            throw new IllegalStateException("Attempting to perform action before initialize.");
        }
    }

    public void beginAdUnitExposure(String str, long j2) {
        a();
        this.a.u().a(str, j2);
    }

    public void clearConditionalUserProperty(String str, String str2, Bundle bundle) {
        a();
        x5 m2 = this.a.m();
        m2.b();
        m2.a((String) null, str, str2, bundle);
    }

    public void endAdUnitExposure(String str, long j2) {
        a();
        this.a.u().b(str, j2);
    }

    public void generateEventId(fb fbVar) {
        a();
        this.a.n().a(fbVar, this.a.n().t());
    }

    public void getAppInstanceId(fb fbVar) {
        a();
        l4 i2 = this.a.i();
        c6 c6Var = new c6(this, fbVar);
        i2.o();
        ResourcesFlusher.b(c6Var);
        i2.a((p4<?>) new p4(i2, c6Var, "Task exception on worker thread"));
    }

    public void getCachedAppInstanceId(fb fbVar) {
        a();
        x5 m2 = this.a.m();
        m2.b();
        this.a.n().a(fbVar, m2.g.get());
    }

    public void getConditionalUserProperties(String str, String str2, fb fbVar) {
        a();
        l4 i2 = this.a.i();
        b9 b9Var = new b9(this, fbVar, str, str2);
        i2.o();
        ResourcesFlusher.b(b9Var);
        i2.a((p4<?>) new p4(i2, b9Var, "Task exception on worker thread"));
    }

    public void getCurrentScreenClass(fb fbVar) {
        a();
        y6 q2 = this.a.m().a.q();
        q2.b();
        w6 w6Var = q2.d;
        this.a.n().a(fbVar, w6Var != null ? w6Var.b : null);
    }

    public void getCurrentScreenName(fb fbVar) {
        a();
        y6 q2 = this.a.m().a.q();
        q2.b();
        w6 w6Var = q2.d;
        this.a.n().a(fbVar, w6Var != null ? w6Var.a : null);
    }

    public void getGmpAppId(fb fbVar) {
        a();
        this.a.n().a(fbVar, this.a.m().B());
    }

    public void getMaxUserProperties(String str, fb fbVar) {
        a();
        this.a.m();
        ResourcesFlusher.b(str);
        this.a.n().a(fbVar, 25);
    }

    public void getTestFlag(fb fbVar, int i2) {
        a();
        if (i2 == 0) {
            y8 n2 = this.a.n();
            x5 m2 = this.a.m();
            if (m2 != null) {
                AtomicReference atomicReference = new AtomicReference();
                n2.a(fbVar, (String) m2.i().a(atomicReference, "String test flag value", new i6(m2, atomicReference)));
                return;
            }
            throw null;
        } else if (i2 == 1) {
            y8 n3 = this.a.n();
            x5 m3 = this.a.m();
            if (m3 != null) {
                AtomicReference atomicReference2 = new AtomicReference();
                n3.a(fbVar, ((Long) m3.i().a(atomicReference2, "long test flag value", new k6(m3, atomicReference2))).longValue());
                return;
            }
            throw null;
        } else if (i2 == 2) {
            y8 n4 = this.a.n();
            x5 m4 = this.a.m();
            if (m4 != null) {
                AtomicReference atomicReference3 = new AtomicReference();
                double doubleValue = ((Double) m4.i().a(atomicReference3, "double test flag value", new m6(m4, atomicReference3))).doubleValue();
                Bundle bundle = new Bundle();
                bundle.putDouble("r", doubleValue);
                try {
                    fbVar.a(bundle);
                } catch (RemoteException e2) {
                    n4.a.a().f2047i.a("Error returning double value to wrapper", e2);
                }
            } else {
                throw null;
            }
        } else if (i2 == 3) {
            y8 n5 = this.a.n();
            x5 m5 = this.a.m();
            if (m5 != null) {
                AtomicReference atomicReference4 = new AtomicReference();
                n5.a(fbVar, ((Integer) m5.i().a(atomicReference4, "int test flag value", new j6(m5, atomicReference4))).intValue());
                return;
            }
            throw null;
        } else if (i2 == 4) {
            y8 n6 = this.a.n();
            x5 m6 = this.a.m();
            if (m6 != null) {
                AtomicReference atomicReference5 = new AtomicReference();
                n6.a(fbVar, ((Boolean) m6.i().a(atomicReference5, "boolean test flag value", new z5(m6, atomicReference5))).booleanValue());
                return;
            }
            throw null;
        }
    }

    public void getUserProperties(String str, String str2, boolean z, fb fbVar) {
        a();
        l4 i2 = this.a.i();
        b7 b7Var = new b7(this, fbVar, str, str2, z);
        i2.o();
        ResourcesFlusher.b(b7Var);
        i2.a((p4<?>) new p4(i2, b7Var, "Task exception on worker thread"));
    }

    public void initForTests(Map map) {
        a();
    }

    public void initialize(j.c.a.a.d.a aVar, nb nbVar, long j2) {
        Context context = (Context) j.c.a.a.d.b.a(aVar);
        r4 r4Var = this.a;
        if (r4Var == null) {
            this.a = r4.a(context, nbVar);
        } else {
            r4Var.a().f2047i.a("Attempting to initialize multiple times");
        }
    }

    public void isDataCollectionEnabled(fb fbVar) {
        a();
        l4 i2 = this.a.i();
        a9 a9Var = new a9(this, fbVar);
        i2.o();
        ResourcesFlusher.b(a9Var);
        i2.a((p4<?>) new p4(i2, a9Var, "Task exception on worker thread"));
    }

    public void logEvent(String str, String str2, Bundle bundle, boolean z, boolean z2, long j2) {
        a();
        this.a.m().a(str, str2, bundle, z, z2, j2);
    }

    public void logEventAndBundle(String str, String str2, Bundle bundle, fb fbVar, long j2) {
        Bundle bundle2;
        a();
        ResourcesFlusher.b(str2);
        if (bundle == null) {
            bundle2 = new Bundle();
        }
        bundle2.putString("_o", "app");
        i iVar = new i(str2, new h(bundle), "app", j2);
        l4 i2 = this.a.i();
        b8 b8Var = new b8(this, fbVar, iVar, str);
        i2.o();
        ResourcesFlusher.b(b8Var);
        i2.a((p4<?>) new p4(i2, b8Var, "Task exception on worker thread"));
    }

    public void logHealthData(int i2, String str, j.c.a.a.d.a aVar, j.c.a.a.d.a aVar2, j.c.a.a.d.a aVar3) {
        Object obj;
        Object obj2;
        a();
        Object obj3 = null;
        if (aVar == null) {
            obj = null;
        } else {
            obj = j.c.a.a.d.b.a(aVar);
        }
        if (aVar2 == null) {
            obj2 = null;
        } else {
            obj2 = j.c.a.a.d.b.a(aVar2);
        }
        if (aVar3 != null) {
            obj3 = j.c.a.a.d.b.a(aVar3);
        }
        this.a.a().a(i2, true, false, str, obj, obj2, obj3);
    }

    public void onActivityCreated(j.c.a.a.d.a aVar, Bundle bundle, long j2) {
        a();
        p6 p6Var = this.a.m().c;
        if (p6Var != null) {
            this.a.m().z();
            p6Var.onActivityCreated((Activity) j.c.a.a.d.b.a(aVar), bundle);
        }
    }

    public void onActivityDestroyed(j.c.a.a.d.a aVar, long j2) {
        a();
        p6 p6Var = this.a.m().c;
        if (p6Var != null) {
            this.a.m().z();
            p6Var.onActivityDestroyed((Activity) j.c.a.a.d.b.a(aVar));
        }
    }

    public void onActivityPaused(j.c.a.a.d.a aVar, long j2) {
        a();
        p6 p6Var = this.a.m().c;
        if (p6Var != null) {
            this.a.m().z();
            p6Var.onActivityPaused((Activity) j.c.a.a.d.b.a(aVar));
        }
    }

    public void onActivityResumed(j.c.a.a.d.a aVar, long j2) {
        a();
        p6 p6Var = this.a.m().c;
        if (p6Var != null) {
            this.a.m().z();
            p6Var.onActivityResumed((Activity) j.c.a.a.d.b.a(aVar));
        }
    }

    public void onActivitySaveInstanceState(j.c.a.a.d.a aVar, fb fbVar, long j2) {
        a();
        p6 p6Var = this.a.m().c;
        Bundle bundle = new Bundle();
        if (p6Var != null) {
            this.a.m().z();
            p6Var.onActivitySaveInstanceState((Activity) j.c.a.a.d.b.a(aVar), bundle);
        }
        try {
            fbVar.a(bundle);
        } catch (RemoteException e2) {
            this.a.a().f2047i.a("Error returning bundle value to wrapper", e2);
        }
    }

    public void onActivityStarted(j.c.a.a.d.a aVar, long j2) {
        a();
        p6 p6Var = this.a.m().c;
        if (p6Var != null) {
            this.a.m().z();
            p6Var.onActivityStarted((Activity) j.c.a.a.d.b.a(aVar));
        }
    }

    public void onActivityStopped(j.c.a.a.d.a aVar, long j2) {
        a();
        p6 p6Var = this.a.m().c;
        if (p6Var != null) {
            this.a.m().z();
            p6Var.onActivityStopped((Activity) j.c.a.a.d.b.a(aVar));
        }
    }

    public void performAction(Bundle bundle, fb fbVar, long j2) {
        a();
        fbVar.a(null);
    }

    public void registerOnMeasurementEventListener(gb gbVar) {
        a();
        v5 v5Var = this.b.get(Integer.valueOf(gbVar.a()));
        if (v5Var == null) {
            v5Var = new a(gbVar);
            this.b.put(Integer.valueOf(gbVar.a()), v5Var);
        }
        this.a.m().a(v5Var);
    }

    public void resetAnalyticsData(long j2) {
        a();
        x5 m2 = this.a.m();
        m2.g.set(null);
        l4 i2 = m2.i();
        b6 b6Var = new b6(m2, j2);
        i2.o();
        ResourcesFlusher.b(b6Var);
        i2.a((p4<?>) new p4(i2, b6Var, "Task exception on worker thread"));
    }

    public void setConditionalUserProperty(Bundle bundle, long j2) {
        a();
        if (bundle == null) {
            this.a.a().f2046f.a("Conditional user property must not be null");
        } else {
            this.a.m().a(bundle, j2);
        }
    }

    public void setCurrentScreen(j.c.a.a.d.a aVar, String str, String str2, long j2) {
        a();
        this.a.q().a((Activity) j.c.a.a.d.b.a(aVar), str, str2);
    }

    public void setDataCollectionEnabled(boolean z) {
        a();
        this.a.m().a(z);
    }

    public void setEventInterceptor(gb gbVar) {
        a();
        x5 m2 = this.a.m();
        b bVar = new b(gbVar);
        m2.b();
        m2.w();
        l4 i2 = m2.i();
        e6 e6Var = new e6(m2, bVar);
        i2.o();
        ResourcesFlusher.b(e6Var);
        i2.a((p4<?>) new p4(i2, e6Var, "Task exception on worker thread"));
    }

    public void setInstanceIdProvider(lb lbVar) {
        a();
    }

    public void setMeasurementEnabled(boolean z, long j2) {
        a();
        x5 m2 = this.a.m();
        m2.w();
        m2.b();
        l4 i2 = m2.i();
        l6 l6Var = new l6(m2, z);
        i2.o();
        ResourcesFlusher.b(l6Var);
        i2.a((p4<?>) new p4(i2, l6Var, "Task exception on worker thread"));
    }

    public void setMinimumSessionDuration(long j2) {
        a();
        x5 m2 = this.a.m();
        m2.b();
        l4 i2 = m2.i();
        n6 n6Var = new n6(m2, j2);
        i2.o();
        ResourcesFlusher.b(n6Var);
        i2.a((p4<?>) new p4(i2, n6Var, "Task exception on worker thread"));
    }

    public void setSessionTimeoutDuration(long j2) {
        a();
        x5 m2 = this.a.m();
        m2.b();
        l4 i2 = m2.i();
        q6 q6Var = new q6(m2, j2);
        i2.o();
        ResourcesFlusher.b(q6Var);
        i2.a((p4<?>) new p4(i2, q6Var, "Task exception on worker thread"));
    }

    public void setUserId(String str, long j2) {
        a();
        this.a.m().a(null, "_id", str, true, j2);
    }

    public void setUserProperty(String str, String str2, j.c.a.a.d.a aVar, boolean z, long j2) {
        a();
        this.a.m().a(str, str2, j.c.a.a.d.b.a(aVar), z, j2);
    }

    public void unregisterOnMeasurementEventListener(gb gbVar) {
        a();
        Object remove = this.b.remove(Integer.valueOf(gbVar.a()));
        if (remove == null) {
            remove = new a(gbVar);
        }
        x5 m2 = this.a.m();
        m2.b();
        m2.w();
        ResourcesFlusher.b(remove);
        if (!m2.f2125e.remove(remove)) {
            m2.a().f2047i.a("OnEventListener had not been registered");
        }
    }
}
