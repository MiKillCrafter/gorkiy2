package com.google.android.gms.measurement;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import j.c.a.a.g.a.i4;
import j.c.a.a.g.a.j4;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class AppMeasurementInstallReferrerReceiver extends BroadcastReceiver implements j4 {
    public i4 a;

    public final BroadcastReceiver.PendingResult a() {
        return goAsync();
    }

    public final void a(Context context, Intent intent) {
    }

    public final void onReceive(Context context, Intent intent) {
        if (this.a == null) {
            this.a = new i4(this);
        }
        this.a.a(context, intent);
    }
}
