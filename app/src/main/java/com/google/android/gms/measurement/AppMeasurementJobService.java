package com.google.android.gms.measurement;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import j.c.a.a.f.e.nb;
import j.c.a.a.g.a.c8;
import j.c.a.a.g.a.n3;
import j.c.a.a.g.a.r4;
import j.c.a.a.g.a.x7;
import j.c.a.a.g.a.z7;

@TargetApi(24)
/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class AppMeasurementJobService extends JobService implements c8 {
    public x7<AppMeasurementJobService> b;

    public final x7<AppMeasurementJobService> a() {
        if (this.b == null) {
            this.b = new x7<>(this);
        }
        return this.b;
    }

    public final void a(Intent intent) {
    }

    public final void onCreate() {
        super.onCreate();
        r4.a(a().a, (nb) null).a().f2052n.a("Local AppMeasurementService is starting up");
    }

    public final void onDestroy() {
        r4.a(a().a, (nb) null).a().f2052n.a("Local AppMeasurementService is shutting down");
        super.onDestroy();
    }

    public final void onRebind(Intent intent) {
        a().b(intent);
    }

    public final boolean onStartJob(JobParameters jobParameters) {
        x7<AppMeasurementJobService> a = a();
        n3 a2 = r4.a(a.a, (nb) null).a();
        String string = jobParameters.getExtras().getString("action");
        a2.f2052n.a("Local AppMeasurementJobService called. action", string);
        if (!"com.google.android.gms.measurement.UPLOAD".equals(string)) {
            return true;
        }
        a.a(new z7(a, a2, jobParameters));
        return true;
    }

    public final boolean onStopJob(JobParameters jobParameters) {
        return false;
    }

    public final boolean onUnbind(Intent intent) {
        a().a(intent);
        return true;
    }

    public final boolean a(int i2) {
        throw new UnsupportedOperationException();
    }

    @TargetApi(24)
    public final void a(JobParameters jobParameters, boolean z) {
        jobFinished(jobParameters, false);
    }
}
