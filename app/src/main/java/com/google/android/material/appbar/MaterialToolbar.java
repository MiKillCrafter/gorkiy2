package com.google.android.material.appbar;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import androidx.appcompat.widget.Toolbar;
import i.h.l.ViewCompat;
import j.c.a.a.c.n.c;
import j.c.a.b.b;
import j.c.a.b.g0.MaterialShapeDrawable;
import j.c.a.b.k;
import j.c.a.b.m0.a.MaterialThemeOverlay;
import j.c.a.b.y.ElevationOverlayProvider;

public class MaterialToolbar extends Toolbar {
    public static final int Q = k.Widget_MaterialComponents_Toolbar;

    public MaterialToolbar(Context context) {
        this(context, null);
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Drawable background = getBackground();
        if (background instanceof MaterialShapeDrawable) {
            c.a(this, (MaterialShapeDrawable) background);
        }
    }

    public void setElevation(float f2) {
        super.setElevation(f2);
        c.a(this, f2);
    }

    public MaterialToolbar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, b.toolbarStyle);
    }

    public MaterialToolbar(Context context, AttributeSet attributeSet, int i2) {
        super(MaterialThemeOverlay.a(context, attributeSet, i2, Q), attributeSet, i2);
        Context context2 = getContext();
        Drawable background = getBackground();
        if (background == null || (background instanceof ColorDrawable)) {
            MaterialShapeDrawable materialShapeDrawable = new MaterialShapeDrawable();
            materialShapeDrawable.a(ColorStateList.valueOf(background != null ? ((ColorDrawable) background).getColor() : 0));
            materialShapeDrawable.b.b = new ElevationOverlayProvider(context2);
            materialShapeDrawable.j();
            materialShapeDrawable.a(ViewCompat.g(this));
            setBackground(materialShapeDrawable);
        }
    }
}
