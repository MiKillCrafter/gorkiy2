package com.google.android.material.floatingactionbutton;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Property;
import android.view.View;
import android.view.ViewGroup;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.button.MaterialButton;
import i.h.l.ViewCompat;
import j.c.a.b.a0.MotionStrategy;
import j.c.a.b.b0.DescendantOffsetUtils;
import j.c.a.b.l;
import j.c.a.b.m.MotionSpec;
import java.util.List;

public class ExtendedFloatingActionButton extends MaterialButton implements CoordinatorLayout.b {

    /* renamed from: s  reason: collision with root package name */
    public final MotionStrategy f471s;

    /* renamed from: t  reason: collision with root package name */
    public final MotionStrategy f472t;
    public final MotionStrategy u;
    public final MotionStrategy v;
    public boolean w;

    public static class ExtendedFloatingActionButtonBehavior<T extends ExtendedFloatingActionButton> extends CoordinatorLayout.c<T> {
        public Rect a;
        public boolean b;
        public boolean c;

        public ExtendedFloatingActionButtonBehavior() {
            this.b = false;
            this.c = true;
        }

        public boolean a(CoordinatorLayout coordinatorLayout, View view, Rect rect) {
            ExtendedFloatingActionButton extendedFloatingActionButton = (ExtendedFloatingActionButton) view;
            return false;
        }

        public boolean b(CoordinatorLayout coordinatorLayout, View view, View view2) {
            ExtendedFloatingActionButton extendedFloatingActionButton = (ExtendedFloatingActionButton) view;
            if (view2 instanceof AppBarLayout) {
                a(coordinatorLayout, (AppBarLayout) view2, extendedFloatingActionButton);
            } else {
                ViewGroup.LayoutParams layoutParams = view2.getLayoutParams();
                if (layoutParams instanceof CoordinatorLayout.f ? ((CoordinatorLayout.f) layoutParams).a instanceof BottomSheetBehavior : false) {
                    b(view2, extendedFloatingActionButton);
                }
            }
            return false;
        }

        public boolean a(CoordinatorLayout coordinatorLayout, View view, int i2) {
            ExtendedFloatingActionButton extendedFloatingActionButton = (ExtendedFloatingActionButton) view;
            List<View> a2 = coordinatorLayout.a(extendedFloatingActionButton);
            int size = a2.size();
            for (int i3 = 0; i3 < size; i3++) {
                View view2 = a2.get(i3);
                if (!(view2 instanceof AppBarLayout)) {
                    ViewGroup.LayoutParams layoutParams = view2.getLayoutParams();
                    if ((layoutParams instanceof CoordinatorLayout.f ? ((CoordinatorLayout.f) layoutParams).a instanceof BottomSheetBehavior : false) && b(view2, extendedFloatingActionButton)) {
                        break;
                    }
                } else if (a(coordinatorLayout, (AppBarLayout) view2, extendedFloatingActionButton)) {
                    break;
                }
            }
            coordinatorLayout.b(extendedFloatingActionButton, i2);
            return true;
        }

        public ExtendedFloatingActionButtonBehavior(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, l.ExtendedFloatingActionButton_Behavior_Layout);
            this.b = obtainStyledAttributes.getBoolean(l.ExtendedFloatingActionButton_Behavior_Layout_behavior_autoHide, false);
            this.c = obtainStyledAttributes.getBoolean(l.ExtendedFloatingActionButton_Behavior_Layout_behavior_autoShrink, true);
            obtainStyledAttributes.recycle();
        }

        public final boolean b(View view, ExtendedFloatingActionButton extendedFloatingActionButton) {
            MotionStrategy motionStrategy;
            MotionStrategy motionStrategy2;
            if (!a(view, extendedFloatingActionButton)) {
                return false;
            }
            if (view.getTop() < (extendedFloatingActionButton.getHeight() / 2) + ((CoordinatorLayout.f) extendedFloatingActionButton.getLayoutParams()).topMargin) {
                if (this.c) {
                    motionStrategy2 = extendedFloatingActionButton.f471s;
                } else {
                    motionStrategy2 = extendedFloatingActionButton.v;
                }
                ExtendedFloatingActionButton.a(extendedFloatingActionButton, motionStrategy2);
                return true;
            }
            if (this.c) {
                motionStrategy = extendedFloatingActionButton.f472t;
            } else {
                motionStrategy = extendedFloatingActionButton.u;
            }
            ExtendedFloatingActionButton.a(extendedFloatingActionButton, motionStrategy);
            return true;
        }

        public void a(CoordinatorLayout.f fVar) {
            if (fVar.h == 0) {
                fVar.h = 80;
            }
        }

        public final boolean a(View view, ExtendedFloatingActionButton extendedFloatingActionButton) {
            CoordinatorLayout.f fVar = (CoordinatorLayout.f) extendedFloatingActionButton.getLayoutParams();
            if ((this.b || this.c) && fVar.f189f == view.getId()) {
                return true;
            }
            return false;
        }

        public final boolean a(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, ExtendedFloatingActionButton extendedFloatingActionButton) {
            MotionStrategy motionStrategy;
            MotionStrategy motionStrategy2;
            if (!a(appBarLayout, extendedFloatingActionButton)) {
                return false;
            }
            if (this.a == null) {
                this.a = new Rect();
            }
            Rect rect = this.a;
            DescendantOffsetUtils.a(coordinatorLayout, appBarLayout, rect);
            if (rect.bottom <= appBarLayout.getMinimumHeightForVisibleOverlappingContent()) {
                if (this.c) {
                    motionStrategy2 = extendedFloatingActionButton.f471s;
                } else {
                    motionStrategy2 = extendedFloatingActionButton.v;
                }
                ExtendedFloatingActionButton.a(extendedFloatingActionButton, motionStrategy2);
                return true;
            }
            if (this.c) {
                motionStrategy = extendedFloatingActionButton.f472t;
            } else {
                motionStrategy = extendedFloatingActionButton.u;
            }
            ExtendedFloatingActionButton.a(extendedFloatingActionButton, motionStrategy);
            return true;
        }
    }

    public static class a extends Property<View, Float> {
        public a(Class cls, String str) {
            super(cls, str);
        }

        public Object get(Object obj) {
            return Float.valueOf((float) ((View) obj).getLayoutParams().width);
        }

        public void set(Object obj, Object obj2) {
            View view = (View) obj;
            view.getLayoutParams().width = ((Float) obj2).intValue();
            view.requestLayout();
        }
    }

    public static class b extends Property<View, Float> {
        public b(Class cls, String str) {
            super(cls, str);
        }

        public Object get(Object obj) {
            return Float.valueOf((float) ((View) obj).getLayoutParams().height);
        }

        public void set(Object obj, Object obj2) {
            View view = (View) obj;
            view.getLayoutParams().height = ((Float) obj2).intValue();
            view.requestLayout();
        }
    }

    public static abstract class c {
    }

    static {
        Class<Float> cls = Float.class;
        new a(cls, "width");
        new b(cls, "height");
    }

    public static /* synthetic */ void a(ExtendedFloatingActionButton extendedFloatingActionButton, MotionStrategy motionStrategy) {
        if (extendedFloatingActionButton == null) {
            throw null;
        } else if (!motionStrategy.e()) {
            if (!(ViewCompat.w(extendedFloatingActionButton) && !extendedFloatingActionButton.isInEditMode())) {
                motionStrategy.f();
                motionStrategy.a(null);
                return;
            }
            extendedFloatingActionButton.measure(0, 0);
            AnimatorSet a2 = motionStrategy.a();
            a2.addListener(new j.c.a.b.a0.ExtendedFloatingActionButton(extendedFloatingActionButton, motionStrategy));
            for (Animator.AnimatorListener addListener : motionStrategy.d()) {
                a2.addListener(addListener);
            }
            a2.start();
        }
    }

    public CoordinatorLayout.c<ExtendedFloatingActionButton> getBehavior() {
        return null;
    }

    public int getCollapsedSize() {
        return getIconSize() + (Math.min(ViewCompat.o(this), getPaddingEnd()) * 2);
    }

    public MotionSpec getExtendMotionSpec() {
        throw null;
    }

    public MotionSpec getHideMotionSpec() {
        throw null;
    }

    public MotionSpec getShowMotionSpec() {
        throw null;
    }

    public MotionSpec getShrinkMotionSpec() {
        throw null;
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.w && TextUtils.isEmpty(getText()) && getIcon() != null) {
            this.w = false;
            throw null;
        }
    }

    public void setExtendMotionSpec(MotionSpec motionSpec) {
        throw null;
    }

    public void setExtendMotionSpecResource(int i2) {
        setExtendMotionSpec(MotionSpec.a(getContext(), i2));
    }

    public void setExtended(boolean z) {
        if (this.w != z) {
            MotionStrategy motionStrategy = null;
            if (!motionStrategy.e()) {
                motionStrategy.f();
            }
        }
    }

    public void setHideMotionSpec(MotionSpec motionSpec) {
        throw null;
    }

    public void setHideMotionSpecResource(int i2) {
        setHideMotionSpec(MotionSpec.a(getContext(), i2));
    }

    public void setShowMotionSpec(MotionSpec motionSpec) {
        throw null;
    }

    public void setShowMotionSpecResource(int i2) {
        setShowMotionSpec(MotionSpec.a(getContext(), i2));
    }

    public void setShrinkMotionSpec(MotionSpec motionSpec) {
        throw null;
    }

    public void setShrinkMotionSpecResource(int i2) {
        setShrinkMotionSpec(MotionSpec.a(getContext(), i2));
    }
}
