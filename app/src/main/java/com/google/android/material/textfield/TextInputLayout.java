package com.google.android.material.textfield;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStructure;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.android.material.internal.CheckableImageButton;
import i.b.k.ResourcesFlusher;
import i.b.l.a.AppCompatResources;
import i.b.q.AppCompatDrawableManager;
import i.b.q.AppCompatTextView;
import i.b.q.DrawableUtils;
import i.h.e.ContextCompat;
import i.h.l.AccessibilityDelegateCompat;
import i.h.l.ViewCompat;
import i.h.l.x.AccessibilityNodeInfoCompat;
import i.j.a.AbsSavedState;
import j.a.a.a.outline;
import j.c.a.b.b0.CollapsingTextHelper;
import j.c.a.b.b0.CollapsingTextHelper0;
import j.c.a.b.b0.DescendantOffsetUtils;
import j.c.a.b.d0.CancelableFontCallback;
import j.c.a.b.d0.TextAppearance;
import j.c.a.b.g0.MaterialShapeDrawable;
import j.c.a.b.g0.ShapeAppearanceModel;
import j.c.a.b.j;
import j.c.a.b.k;
import j.c.a.b.k0.CutoutDrawable;
import j.c.a.b.k0.EndIconDelegate;
import j.c.a.b.k0.IndicatorViewController;
import j.c.a.b.k0.n;
import j.c.a.b.m.AnimationUtils;
import java.util.Iterator;
import java.util.LinkedHashSet;

public class TextInputLayout extends LinearLayout {
    public static final int I0 = k.Widget_Design_TextInputLayout;
    public CharSequence A;
    public final int A0;
    public boolean B;
    public int B0;
    public MaterialShapeDrawable C;
    public boolean C0;
    public MaterialShapeDrawable D;
    public final CollapsingTextHelper D0;
    public ShapeAppearanceModel E;
    public boolean E0;
    public final int F;
    public ValueAnimator F0;
    public int G;
    public boolean G0;
    public final int H;
    public boolean H0;
    public int I;
    public final int J;
    public final int K;
    public int L;
    public int M;
    public final Rect N;
    public final Rect O;
    public final RectF P;
    public Typeface Q;
    public final CheckableImageButton R;
    public ColorStateList S;
    public boolean T;
    public PorterDuff.Mode U;
    public boolean V;
    public Drawable W;
    public int a0;
    public final FrameLayout b;
    public View.OnLongClickListener b0;
    public final LinearLayout c;
    public final LinkedHashSet<f> c0;
    public final LinearLayout d;
    public int d0;

    /* renamed from: e  reason: collision with root package name */
    public final FrameLayout f530e;
    public final SparseArray<n> e0;

    /* renamed from: f  reason: collision with root package name */
    public EditText f531f;
    public final CheckableImageButton f0;
    public CharSequence g;
    public final LinkedHashSet<g> g0;
    public final IndicatorViewController h;
    public ColorStateList h0;

    /* renamed from: i  reason: collision with root package name */
    public boolean f532i;
    public boolean i0;

    /* renamed from: j  reason: collision with root package name */
    public int f533j;
    public PorterDuff.Mode j0;

    /* renamed from: k  reason: collision with root package name */
    public boolean f534k;
    public boolean k0;

    /* renamed from: l  reason: collision with root package name */
    public TextView f535l;
    public Drawable l0;

    /* renamed from: m  reason: collision with root package name */
    public int f536m;
    public int m0;

    /* renamed from: n  reason: collision with root package name */
    public int f537n;
    public Drawable n0;

    /* renamed from: o  reason: collision with root package name */
    public CharSequence f538o;
    public View.OnLongClickListener o0;

    /* renamed from: p  reason: collision with root package name */
    public boolean f539p;
    public final CheckableImageButton p0;

    /* renamed from: q  reason: collision with root package name */
    public TextView f540q;
    public ColorStateList q0;

    /* renamed from: r  reason: collision with root package name */
    public ColorStateList f541r;
    public ColorStateList r0;

    /* renamed from: s  reason: collision with root package name */
    public int f542s;
    public ColorStateList s0;

    /* renamed from: t  reason: collision with root package name */
    public ColorStateList f543t;
    public int t0;
    public ColorStateList u;
    public int u0;
    public CharSequence v;
    public int v0;
    public final TextView w;
    public ColorStateList w0;
    public CharSequence x;
    public int x0;
    public final TextView y;
    public final int y0;
    public boolean z;
    public final int z0;

    public class a implements TextWatcher {
        public a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.material.textfield.TextInputLayout.a(boolean, boolean):void
         arg types: [boolean, int]
         candidates:
          com.google.android.material.textfield.TextInputLayout.a(android.view.ViewGroup, boolean):void
          com.google.android.material.textfield.TextInputLayout.a(com.google.android.material.internal.CheckableImageButton, android.view.View$OnLongClickListener):void
          com.google.android.material.textfield.TextInputLayout.a(int, boolean):int
          com.google.android.material.textfield.TextInputLayout.a(android.widget.TextView, int):void
          com.google.android.material.textfield.TextInputLayout.a(com.google.android.material.internal.CheckableImageButton, android.content.res.ColorStateList):void
          com.google.android.material.textfield.TextInputLayout.a(boolean, boolean):void */
        public void afterTextChanged(Editable editable) {
            TextInputLayout textInputLayout = TextInputLayout.this;
            textInputLayout.a(!textInputLayout.H0, false);
            TextInputLayout textInputLayout2 = TextInputLayout.this;
            if (textInputLayout2.f532i) {
                textInputLayout2.a(editable.length());
            }
            TextInputLayout textInputLayout3 = TextInputLayout.this;
            if (textInputLayout3.f539p) {
                textInputLayout3.b(editable.length());
            }
        }

        public void beforeTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
        }

        public void onTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
        }
    }

    public class b implements Runnable {
        public b() {
        }

        public void run() {
            TextInputLayout.this.f0.performClick();
            TextInputLayout.this.f0.jumpDrawablesToCurrentState();
        }
    }

    public class c implements Runnable {
        public c() {
        }

        public void run() {
            TextInputLayout.this.f531f.requestLayout();
        }
    }

    public class d implements ValueAnimator.AnimatorUpdateListener {
        public d() {
        }

        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            TextInputLayout.this.D0.c(((Float) valueAnimator.getAnimatedValue()).floatValue());
        }
    }

    public static class e extends AccessibilityDelegateCompat {
        public final TextInputLayout d;

        public e(TextInputLayout textInputLayout) {
            this.d = textInputLayout;
        }

        public void a(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            String str;
            String str2;
            super.a.onInitializeAccessibilityNodeInfo(view, accessibilityNodeInfoCompat.a);
            EditText editText = this.d.getEditText();
            Editable text = editText != null ? editText.getText() : null;
            CharSequence hint = this.d.getHint();
            CharSequence helperText = this.d.getHelperText();
            CharSequence error = this.d.getError();
            int counterMaxLength = this.d.getCounterMaxLength();
            CharSequence counterOverflowDescription = this.d.getCounterOverflowDescription();
            boolean z = !TextUtils.isEmpty(text);
            boolean z2 = !TextUtils.isEmpty(hint);
            boolean z3 = !TextUtils.isEmpty(helperText);
            boolean z4 = !TextUtils.isEmpty(error);
            boolean z5 = z4 || !TextUtils.isEmpty(counterOverflowDescription);
            if (z2) {
                str = hint.toString();
            } else {
                str = "";
            }
            StringBuilder a = outline.a(str);
            if ((z4 || z3) && !TextUtils.isEmpty(str)) {
                str2 = ", ";
            } else {
                str2 = "";
            }
            a.append(str2);
            StringBuilder a2 = outline.a(a.toString());
            if (z4) {
                helperText = error;
            } else if (!z3) {
                helperText = "";
            }
            a2.append((Object) helperText);
            String sb = a2.toString();
            if (z) {
                accessibilityNodeInfoCompat.a.setText(text);
            } else if (!TextUtils.isEmpty(sb)) {
                accessibilityNodeInfoCompat.a.setText(sb);
            }
            if (!TextUtils.isEmpty(sb)) {
                if (Build.VERSION.SDK_INT >= 26) {
                    accessibilityNodeInfoCompat.a((CharSequence) sb);
                } else {
                    if (z) {
                        sb = ((Object) text) + ", " + sb;
                    }
                    accessibilityNodeInfoCompat.a.setText(sb);
                }
                boolean z6 = !z;
                if (Build.VERSION.SDK_INT >= 26) {
                    accessibilityNodeInfoCompat.a.setShowingHintText(z6);
                } else {
                    accessibilityNodeInfoCompat.a(4, z6);
                }
            }
            if (text == null || text.length() != counterMaxLength) {
                counterMaxLength = -1;
            }
            accessibilityNodeInfoCompat.a.setMaxTextLength(counterMaxLength);
            if (z5) {
                if (!z4) {
                    error = counterOverflowDescription;
                }
                accessibilityNodeInfoCompat.a.setError(error);
            }
        }
    }

    public interface f {
        void a(TextInputLayout textInputLayout);
    }

    public interface g {
        void a(TextInputLayout textInputLayout, int i2);
    }

    public static class h extends AbsSavedState {
        public static final Parcelable.Creator<h> CREATOR = new a();
        public CharSequence d;

        /* renamed from: e  reason: collision with root package name */
        public boolean f544e;

        public static class a implements Parcelable.ClassLoaderCreator<h> {
            public Object createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new h(parcel, classLoader);
            }

            public Object[] newArray(int i2) {
                return new h[i2];
            }

            public Object createFromParcel(Parcel parcel) {
                return new h(parcel, null);
            }
        }

        public h(Parcelable parcelable) {
            super(parcelable);
        }

        public String toString() {
            StringBuilder a2 = outline.a("TextInputLayout.SavedState{");
            a2.append(Integer.toHexString(System.identityHashCode(this)));
            a2.append(" error=");
            a2.append((Object) this.d);
            a2.append("}");
            return a2.toString();
        }

        public void writeToParcel(Parcel parcel, int i2) {
            parcel.writeParcelable(super.b, i2);
            TextUtils.writeToParcel(this.d, parcel, i2);
            parcel.writeInt(this.f544e ? 1 : 0);
        }

        public h(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.d = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
            this.f544e = parcel.readInt() != 1 ? false : true;
        }
    }

    public TextInputLayout(Context context) {
        this(context, null);
    }

    private EndIconDelegate getEndIconDelegate() {
        EndIconDelegate endIconDelegate = this.e0.get(this.d0);
        return endIconDelegate != null ? endIconDelegate : this.e0.get(0);
    }

    private CheckableImageButton getEndIconToUpdateDummyDrawable() {
        if (this.p0.getVisibility() == 0) {
            return this.p0;
        }
        if (!f() || !g()) {
            return null;
        }
        return this.f0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.material.textfield.TextInputLayout.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.google.android.material.textfield.TextInputLayout.a(android.view.ViewGroup, boolean):void
      com.google.android.material.textfield.TextInputLayout.a(com.google.android.material.internal.CheckableImageButton, android.view.View$OnLongClickListener):void
      com.google.android.material.textfield.TextInputLayout.a(int, boolean):int
      com.google.android.material.textfield.TextInputLayout.a(android.widget.TextView, int):void
      com.google.android.material.textfield.TextInputLayout.a(com.google.android.material.internal.CheckableImageButton, android.content.res.ColorStateList):void
      com.google.android.material.textfield.TextInputLayout.a(boolean, boolean):void */
    private void setEditText(EditText editText) {
        if (this.f531f == null) {
            if (this.d0 != 3 && !(editText instanceof TextInputEditText)) {
                Log.i("TextInputLayout", "EditText added is not a TextInputEditText. Please switch to using that class instead.");
            }
            this.f531f = editText;
            h();
            setTextInputAccessibilityDelegate(new e(this));
            this.D0.a(this.f531f.getTypeface());
            CollapsingTextHelper collapsingTextHelper = this.D0;
            float textSize = this.f531f.getTextSize();
            if (collapsingTextHelper.f2168i != textSize) {
                collapsingTextHelper.f2168i = textSize;
                collapsingTextHelper.e();
            }
            int gravity = this.f531f.getGravity();
            this.D0.a((gravity & -113) | 48);
            CollapsingTextHelper collapsingTextHelper2 = this.D0;
            if (collapsingTextHelper2.g != gravity) {
                collapsingTextHelper2.g = gravity;
                collapsingTextHelper2.e();
            }
            this.f531f.addTextChangedListener(new a());
            if (this.r0 == null) {
                this.r0 = this.f531f.getHintTextColors();
            }
            if (this.z) {
                if (TextUtils.isEmpty(this.A)) {
                    CharSequence hint = this.f531f.getHint();
                    this.g = hint;
                    setHint(hint);
                    this.f531f.setHint((CharSequence) null);
                }
                this.B = true;
            }
            if (this.f535l != null) {
                a(this.f531f.getText().length());
            }
            n();
            this.h.a();
            this.c.bringToFront();
            this.d.bringToFront();
            this.f530e.bringToFront();
            this.p0.bringToFront();
            Iterator<f> it = this.c0.iterator();
            while (it.hasNext()) {
                it.next().a(this);
            }
            p();
            r();
            if (!isEnabled()) {
                editText.setEnabled(false);
            }
            a(false, true);
            return;
        }
        throw new IllegalArgumentException("We already have an EditText, can only have one");
    }

    private void setErrorIconVisible(boolean z2) {
        int i2 = 0;
        this.p0.setVisibility(z2 ? 0 : 8);
        FrameLayout frameLayout = this.f530e;
        if (z2) {
            i2 = 8;
        }
        frameLayout.setVisibility(i2);
        r();
        if (!f()) {
            m();
        }
    }

    private void setHintInternal(CharSequence charSequence) {
        if (!TextUtils.equals(charSequence, this.A)) {
            this.A = charSequence;
            CollapsingTextHelper collapsingTextHelper = this.D0;
            if (charSequence == null || !TextUtils.equals(collapsingTextHelper.w, charSequence)) {
                collapsingTextHelper.w = charSequence;
                collapsingTextHelper.x = null;
                Bitmap bitmap = collapsingTextHelper.A;
                if (bitmap != null) {
                    bitmap.recycle();
                    collapsingTextHelper.A = null;
                }
                collapsingTextHelper.e();
            }
            if (!this.C0) {
                i();
            }
        }
    }

    private void setPlaceholderTextEnabled(boolean z2) {
        if (this.f539p != z2) {
            if (z2) {
                AppCompatTextView appCompatTextView = new AppCompatTextView(getContext());
                this.f540q = appCompatTextView;
                appCompatTextView.setId(j.c.a.b.f.textinput_placeholder);
                ViewCompat.g(this.f540q, 1);
                setPlaceholderTextAppearance(this.f542s);
                setPlaceholderTextColor(this.f541r);
                TextView textView = this.f540q;
                if (textView != null) {
                    this.b.addView(textView);
                    this.f540q.setVisibility(0);
                }
            } else {
                TextView textView2 = this.f540q;
                if (textView2 != null) {
                    textView2.setVisibility(8);
                }
                this.f540q = null;
            }
            this.f539p = z2;
        }
    }

    public final void a(boolean z2, boolean z3) {
        ColorStateList colorStateList;
        TextView textView;
        boolean isEnabled = isEnabled();
        EditText editText = this.f531f;
        boolean z4 = editText != null && !TextUtils.isEmpty(editText.getText());
        EditText editText2 = this.f531f;
        boolean z5 = editText2 != null && editText2.hasFocus();
        boolean c2 = this.h.c();
        ColorStateList colorStateList2 = this.r0;
        if (colorStateList2 != null) {
            CollapsingTextHelper collapsingTextHelper = this.D0;
            if (collapsingTextHelper.f2171l != colorStateList2) {
                collapsingTextHelper.f2171l = colorStateList2;
                collapsingTextHelper.e();
            }
            CollapsingTextHelper collapsingTextHelper2 = this.D0;
            ColorStateList colorStateList3 = this.r0;
            if (collapsingTextHelper2.f2170k != colorStateList3) {
                collapsingTextHelper2.f2170k = colorStateList3;
                collapsingTextHelper2.e();
            }
        }
        if (!isEnabled) {
            ColorStateList colorStateList4 = this.r0;
            int colorForState = colorStateList4 != null ? colorStateList4.getColorForState(new int[]{-16842910}, this.B0) : this.B0;
            this.D0.b(ColorStateList.valueOf(colorForState));
            CollapsingTextHelper collapsingTextHelper3 = this.D0;
            ColorStateList valueOf = ColorStateList.valueOf(colorForState);
            if (collapsingTextHelper3.f2170k != valueOf) {
                collapsingTextHelper3.f2170k = valueOf;
                collapsingTextHelper3.e();
            }
        } else if (c2) {
            CollapsingTextHelper collapsingTextHelper4 = this.D0;
            TextView textView2 = this.h.f2281m;
            collapsingTextHelper4.b(textView2 != null ? textView2.getTextColors() : null);
        } else if (this.f534k && (textView = this.f535l) != null) {
            this.D0.b(textView.getTextColors());
        } else if (z5 && (colorStateList = this.s0) != null) {
            CollapsingTextHelper collapsingTextHelper5 = this.D0;
            if (collapsingTextHelper5.f2171l != colorStateList) {
                collapsingTextHelper5.f2171l = colorStateList;
                collapsingTextHelper5.e();
            }
        }
        if (z4 || (isEnabled() && (z5 || c2))) {
            if (z3 || this.C0) {
                ValueAnimator valueAnimator = this.F0;
                if (valueAnimator != null && valueAnimator.isRunning()) {
                    this.F0.cancel();
                }
                if (!z2 || !this.E0) {
                    this.D0.c(1.0f);
                } else {
                    a(1.0f);
                }
                this.C0 = false;
                if (e()) {
                    i();
                }
                j();
                q();
                s();
            }
        } else if (z3 || !this.C0) {
            ValueAnimator valueAnimator2 = this.F0;
            if (valueAnimator2 != null && valueAnimator2.isRunning()) {
                this.F0.cancel();
            }
            if (!z2 || !this.E0) {
                this.D0.c(0.0f);
            } else {
                a(0.0f);
            }
            if (e() && (!((CutoutDrawable) this.C).A.isEmpty()) && e()) {
                ((CutoutDrawable) this.C).a(0.0f, 0.0f, 0.0f, 0.0f);
            }
            this.C0 = true;
            TextView textView3 = this.f540q;
            if (textView3 != null && this.f539p) {
                textView3.setText((CharSequence) null);
                this.f540q.setVisibility(4);
            }
            q();
            s();
        }
    }

    public void addView(View view, int i2, ViewGroup.LayoutParams layoutParams) {
        if (view instanceof EditText) {
            FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(layoutParams);
            layoutParams2.gravity = (layoutParams2.gravity & -113) | 16;
            this.b.addView(view, layoutParams2);
            this.b.setLayoutParams(layoutParams);
            o();
            setEditText((EditText) view);
            return;
        }
        super.addView(view, i2, layoutParams);
    }

    public final void b(int i2) {
        if (i2 != 0 || this.C0) {
            TextView textView = this.f540q;
            if (textView != null && this.f539p) {
                textView.setText((CharSequence) null);
                this.f540q.setVisibility(4);
                return;
            }
            return;
        }
        j();
    }

    public final void c() {
        a(this.R, this.T, this.S, this.V, this.U);
    }

    public final int d() {
        float b2;
        if (!this.z) {
            return 0;
        }
        int i2 = this.G;
        if (i2 == 0 || i2 == 1) {
            b2 = this.D0.b();
        } else if (i2 != 2) {
            return 0;
        } else {
            b2 = this.D0.b() / 2.0f;
        }
        return (int) b2;
    }

    public void dispatchProvideAutofillStructure(ViewStructure viewStructure, int i2) {
        EditText editText;
        if (this.g == null || (editText = this.f531f) == null) {
            super.dispatchProvideAutofillStructure(viewStructure, i2);
            return;
        }
        boolean z2 = this.B;
        this.B = false;
        CharSequence hint = editText.getHint();
        this.f531f.setHint(this.g);
        try {
            super.dispatchProvideAutofillStructure(viewStructure, i2);
        } finally {
            this.f531f.setHint(hint);
            this.B = z2;
        }
    }

    public void dispatchRestoreInstanceState(SparseArray<Parcelable> sparseArray) {
        this.H0 = true;
        super.dispatchRestoreInstanceState(sparseArray);
        this.H0 = false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Canvas.drawText(java.lang.String, int, int, float, float, android.graphics.Paint):void}
     arg types: [java.lang.String, int, int, int, float, android.text.TextPaint]
     candidates:
      ClspMth{android.graphics.Canvas.drawText(java.lang.CharSequence, int, int, float, float, android.graphics.Paint):void}
      ClspMth{android.graphics.Canvas.drawText(char[], int, int, float, float, android.graphics.Paint):void}
      ClspMth{android.graphics.Canvas.drawText(java.lang.String, int, int, float, float, android.graphics.Paint):void} */
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (this.z) {
            CollapsingTextHelper collapsingTextHelper = this.D0;
            if (collapsingTextHelper != null) {
                int save = canvas.save();
                if (collapsingTextHelper.x != null && collapsingTextHelper.b) {
                    float lineLeft = (collapsingTextHelper.R.getLineLeft(0) + collapsingTextHelper.f2176q) - (collapsingTextHelper.U * 2.0f);
                    collapsingTextHelper.G.setTextSize(collapsingTextHelper.D);
                    float f2 = collapsingTextHelper.f2176q;
                    float f3 = collapsingTextHelper.f2177r;
                    boolean z2 = collapsingTextHelper.z && collapsingTextHelper.A != null;
                    float lineAscent = (float) collapsingTextHelper.R.getLineAscent(0);
                    float f4 = collapsingTextHelper.C;
                    if (f4 != 1.0f) {
                        canvas.scale(f4, f4, f2, f3);
                    }
                    if (z2) {
                        canvas.drawBitmap(collapsingTextHelper.A, f2, f3 + lineAscent, collapsingTextHelper.B);
                        canvas.restoreToCount(save);
                    } else {
                        if (collapsingTextHelper.f()) {
                            int alpha = collapsingTextHelper.G.getAlpha();
                            canvas.translate(lineLeft, f3);
                            float f5 = (float) alpha;
                            collapsingTextHelper.G.setAlpha((int) (collapsingTextHelper.T * f5));
                            collapsingTextHelper.R.draw(canvas);
                            canvas.translate(f2 - lineLeft, 0.0f);
                            collapsingTextHelper.G.setAlpha((int) (collapsingTextHelper.S * f5));
                            CharSequence charSequence = collapsingTextHelper.V;
                            float f6 = -lineAscent;
                            canvas.drawText(charSequence, 0, charSequence.length(), 0.0f, f6 / collapsingTextHelper.C, collapsingTextHelper.G);
                            String trim = collapsingTextHelper.V.toString().trim();
                            if (trim.endsWith("…")) {
                                trim = trim.substring(0, trim.length() - 1);
                            }
                            String str = trim;
                            collapsingTextHelper.G.setAlpha(alpha);
                            canvas.drawText(str, 0, Math.min(collapsingTextHelper.R.getLineEnd(0), str.length()), 0.0f, f6 / collapsingTextHelper.C, (Paint) collapsingTextHelper.G);
                        } else {
                            canvas.translate(f2, f3 + lineAscent);
                            collapsingTextHelper.R.draw(canvas);
                        }
                        canvas.restoreToCount(save);
                    }
                }
            } else {
                throw null;
            }
        }
        MaterialShapeDrawable materialShapeDrawable = this.D;
        if (materialShapeDrawable != null) {
            Rect bounds = materialShapeDrawable.getBounds();
            bounds.top = bounds.bottom - this.I;
            this.D.draw(canvas);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.material.textfield.TextInputLayout.a(boolean, boolean):void
     arg types: [boolean, int]
     candidates:
      com.google.android.material.textfield.TextInputLayout.a(android.view.ViewGroup, boolean):void
      com.google.android.material.textfield.TextInputLayout.a(com.google.android.material.internal.CheckableImageButton, android.view.View$OnLongClickListener):void
      com.google.android.material.textfield.TextInputLayout.a(int, boolean):int
      com.google.android.material.textfield.TextInputLayout.a(android.widget.TextView, int):void
      com.google.android.material.textfield.TextInputLayout.a(com.google.android.material.internal.CheckableImageButton, android.content.res.ColorStateList):void
      com.google.android.material.textfield.TextInputLayout.a(boolean, boolean):void */
    public void drawableStateChanged() {
        boolean z2;
        boolean z3;
        ColorStateList colorStateList;
        if (!this.G0) {
            boolean z4 = true;
            this.G0 = true;
            super.drawableStateChanged();
            int[] drawableState = getDrawableState();
            CollapsingTextHelper collapsingTextHelper = this.D0;
            if (collapsingTextHelper != null) {
                collapsingTextHelper.E = drawableState;
                ColorStateList colorStateList2 = collapsingTextHelper.f2171l;
                if ((colorStateList2 != null && colorStateList2.isStateful()) || ((colorStateList = collapsingTextHelper.f2170k) != null && colorStateList.isStateful())) {
                    collapsingTextHelper.e();
                    z3 = true;
                } else {
                    z3 = false;
                }
                z2 = z3 | false;
            } else {
                z2 = false;
            }
            if (this.f531f != null) {
                if (!ViewCompat.w(this) || !isEnabled()) {
                    z4 = false;
                }
                a(z4, false);
            }
            n();
            t();
            if (z2) {
                invalidate();
            }
            this.G0 = false;
        }
    }

    public final boolean e() {
        return this.z && !TextUtils.isEmpty(this.A) && (this.C instanceof CutoutDrawable);
    }

    public final boolean f() {
        return this.d0 != 0;
    }

    public boolean g() {
        return this.f530e.getVisibility() == 0 && this.f0.getVisibility() == 0;
    }

    public int getBaseline() {
        EditText editText = this.f531f;
        if (editText == null) {
            return super.getBaseline();
        }
        return d() + getPaddingTop() + editText.getBaseline();
    }

    public MaterialShapeDrawable getBoxBackground() {
        int i2 = this.G;
        if (i2 == 1 || i2 == 2) {
            return this.C;
        }
        throw new IllegalStateException();
    }

    public int getBoxBackgroundColor() {
        return this.M;
    }

    public int getBoxBackgroundMode() {
        return this.G;
    }

    public float getBoxCornerRadiusBottomEnd() {
        MaterialShapeDrawable materialShapeDrawable = this.C;
        return materialShapeDrawable.b.a.h.a(materialShapeDrawable.b());
    }

    public float getBoxCornerRadiusBottomStart() {
        MaterialShapeDrawable materialShapeDrawable = this.C;
        return materialShapeDrawable.b.a.g.a(materialShapeDrawable.b());
    }

    public float getBoxCornerRadiusTopEnd() {
        MaterialShapeDrawable materialShapeDrawable = this.C;
        return materialShapeDrawable.b.a.f2243f.a(materialShapeDrawable.b());
    }

    public float getBoxCornerRadiusTopStart() {
        return this.C.f();
    }

    public int getBoxStrokeColor() {
        return this.v0;
    }

    public ColorStateList getBoxStrokeErrorColor() {
        return this.w0;
    }

    public int getCounterMaxLength() {
        return this.f533j;
    }

    public CharSequence getCounterOverflowDescription() {
        TextView textView;
        if (!this.f532i || !this.f534k || (textView = this.f535l) == null) {
            return null;
        }
        return textView.getContentDescription();
    }

    public ColorStateList getCounterOverflowTextColor() {
        return this.f543t;
    }

    public ColorStateList getCounterTextColor() {
        return this.f543t;
    }

    public ColorStateList getDefaultHintTextColor() {
        return this.r0;
    }

    public EditText getEditText() {
        return this.f531f;
    }

    public CharSequence getEndIconContentDescription() {
        return this.f0.getContentDescription();
    }

    public Drawable getEndIconDrawable() {
        return this.f0.getDrawable();
    }

    public int getEndIconMode() {
        return this.d0;
    }

    public CheckableImageButton getEndIconView() {
        return this.f0;
    }

    public CharSequence getError() {
        IndicatorViewController indicatorViewController = this.h;
        if (indicatorViewController.f2280l) {
            return indicatorViewController.f2279k;
        }
        return null;
    }

    public CharSequence getErrorContentDescription() {
        return this.h.f2282n;
    }

    public int getErrorCurrentTextColors() {
        return this.h.d();
    }

    public Drawable getErrorIconDrawable() {
        return this.p0.getDrawable();
    }

    public final int getErrorTextCurrentColor() {
        return this.h.d();
    }

    public CharSequence getHelperText() {
        IndicatorViewController indicatorViewController = this.h;
        if (indicatorViewController.f2286r) {
            return indicatorViewController.f2285q;
        }
        return null;
    }

    public int getHelperTextCurrentTextColor() {
        TextView textView = this.h.f2287s;
        if (textView != null) {
            return textView.getCurrentTextColor();
        }
        return -1;
    }

    public CharSequence getHint() {
        if (this.z) {
            return this.A;
        }
        return null;
    }

    public final float getHintCollapsedTextHeight() {
        return this.D0.b();
    }

    public final int getHintCurrentCollapsedTextColor() {
        return this.D0.c();
    }

    public ColorStateList getHintTextColor() {
        return this.s0;
    }

    @Deprecated
    public CharSequence getPasswordVisibilityToggleContentDescription() {
        return this.f0.getContentDescription();
    }

    @Deprecated
    public Drawable getPasswordVisibilityToggleDrawable() {
        return this.f0.getDrawable();
    }

    public CharSequence getPlaceholderText() {
        if (this.f539p) {
            return this.f538o;
        }
        return null;
    }

    public int getPlaceholderTextAppearance() {
        return this.f542s;
    }

    public ColorStateList getPlaceholderTextColor() {
        return this.f541r;
    }

    public CharSequence getPrefixText() {
        return this.v;
    }

    public ColorStateList getPrefixTextColor() {
        return this.w.getTextColors();
    }

    public TextView getPrefixTextView() {
        return this.w;
    }

    public CharSequence getStartIconContentDescription() {
        return this.R.getContentDescription();
    }

    public Drawable getStartIconDrawable() {
        return this.R.getDrawable();
    }

    public CharSequence getSuffixText() {
        return this.x;
    }

    public ColorStateList getSuffixTextColor() {
        return this.y.getTextColors();
    }

    public TextView getSuffixTextView() {
        return this.y;
    }

    public Typeface getTypeface() {
        return this.Q;
    }

    public final void h() {
        int i2 = this.G;
        boolean z2 = true;
        if (i2 == 0) {
            this.C = null;
            this.D = null;
        } else if (i2 == 1) {
            this.C = new MaterialShapeDrawable(this.E);
            this.D = new MaterialShapeDrawable();
        } else if (i2 == 2) {
            if (!this.z || (this.C instanceof CutoutDrawable)) {
                this.C = new MaterialShapeDrawable(this.E);
            } else {
                this.C = new CutoutDrawable(this.E);
            }
            this.D = null;
        } else {
            throw new IllegalArgumentException(this.G + " is illegal; only @BoxBackgroundMode constants are supported.");
        }
        EditText editText = this.f531f;
        if (editText == null || this.C == null || editText.getBackground() != null || this.G == 0) {
            z2 = false;
        }
        if (z2) {
            ViewCompat.a(this.f531f, this.C);
        }
        t();
        if (this.G != 0) {
            o();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:42:0x00dc  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00e8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void i() {
        /*
            r12 = this;
            boolean r0 = r12.e()
            if (r0 != 0) goto L_0x0007
            return
        L_0x0007:
            android.graphics.RectF r0 = r12.P
            j.c.a.b.b0.CollapsingTextHelper r1 = r12.D0
            android.widget.EditText r2 = r12.f531f
            int r2 = r2.getWidth()
            android.widget.EditText r3 = r12.f531f
            int r3 = r3.getGravity()
            java.lang.CharSequence r4 = r1.w
            boolean r4 = r1.a(r4)
            r1.y = r4
            r5 = 8388613(0x800005, float:1.175495E-38)
            r6 = 1
            r7 = 1073741824(0x40000000, float:2.0)
            r8 = 17
            r9 = 5
            if (r3 == r8) goto L_0x005d
            r10 = r3 & 7
            if (r10 != r6) goto L_0x002f
            goto L_0x005d
        L_0x002f:
            r10 = r3 & r5
            if (r10 == r5) goto L_0x0049
            r10 = r3 & 5
            if (r10 != r9) goto L_0x0038
            goto L_0x0049
        L_0x0038:
            if (r4 == 0) goto L_0x0044
            android.graphics.Rect r4 = r1.f2166e
            int r4 = r4.right
            float r4 = (float) r4
            float r10 = r1.a()
            goto L_0x0064
        L_0x0044:
            android.graphics.Rect r4 = r1.f2166e
            int r4 = r4.left
            goto L_0x0051
        L_0x0049:
            boolean r4 = r1.y
            if (r4 == 0) goto L_0x0053
            android.graphics.Rect r4 = r1.f2166e
            int r4 = r4.left
        L_0x0051:
            float r4 = (float) r4
            goto L_0x0065
        L_0x0053:
            android.graphics.Rect r4 = r1.f2166e
            int r4 = r4.right
            float r4 = (float) r4
            float r10 = r1.a()
            goto L_0x0064
        L_0x005d:
            float r4 = (float) r2
            float r4 = r4 / r7
            float r10 = r1.a()
            float r10 = r10 / r7
        L_0x0064:
            float r4 = r4 - r10
        L_0x0065:
            r0.left = r4
            android.graphics.Rect r10 = r1.f2166e
            int r11 = r10.top
            float r11 = (float) r11
            r0.top = r11
            if (r3 == r8) goto L_0x009c
            r8 = r3 & 7
            if (r8 != r6) goto L_0x0075
            goto L_0x009c
        L_0x0075:
            r2 = r3 & r5
            if (r2 == r5) goto L_0x008b
            r2 = r3 & 5
            if (r2 != r9) goto L_0x007e
            goto L_0x008b
        L_0x007e:
            boolean r2 = r1.y
            if (r2 == 0) goto L_0x0085
            int r2 = r10.right
            goto L_0x009a
        L_0x0085:
            float r2 = r1.a()
            float r2 = r2 + r4
            goto L_0x00a4
        L_0x008b:
            boolean r2 = r1.y
            if (r2 == 0) goto L_0x0096
            float r2 = r0.left
            float r3 = r1.a()
            goto L_0x00a3
        L_0x0096:
            android.graphics.Rect r2 = r1.f2166e
            int r2 = r2.right
        L_0x009a:
            float r2 = (float) r2
            goto L_0x00a4
        L_0x009c:
            float r2 = (float) r2
            float r2 = r2 / r7
            float r3 = r1.a()
            float r3 = r3 / r7
        L_0x00a3:
            float r2 = r2 + r3
        L_0x00a4:
            r0.right = r2
            android.graphics.Rect r2 = r1.f2166e
            int r2 = r2.top
            float r2 = (float) r2
            float r1 = r1.b()
            float r1 = r1 + r2
            r0.bottom = r1
            float r2 = r0.left
            int r3 = r12.F
            float r3 = (float) r3
            float r2 = r2 - r3
            r0.left = r2
            float r2 = r0.top
            float r2 = r2 - r3
            r0.top = r2
            float r2 = r0.right
            float r2 = r2 + r3
            r0.right = r2
            float r1 = r1 + r3
            r0.bottom = r1
            int r1 = r12.getPaddingLeft()
            int r1 = -r1
            float r1 = (float) r1
            int r2 = r12.getPaddingTop()
            int r2 = -r2
            float r2 = (float) r2
            r0.offset(r1, r2)
            j.c.a.b.g0.MaterialShapeDrawable r1 = r12.C
            j.c.a.b.k0.CutoutDrawable r1 = (j.c.a.b.k0.CutoutDrawable) r1
            if (r1 == 0) goto L_0x00e8
            float r2 = r0.left
            float r3 = r0.top
            float r4 = r0.right
            float r0 = r0.bottom
            r1.a(r2, r3, r4, r0)
            return
        L_0x00e8:
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.textfield.TextInputLayout.i():void");
    }

    public final void j() {
        TextView textView = this.f540q;
        if (textView != null && this.f539p) {
            textView.setText(this.f538o);
            this.f540q.setVisibility(0);
            this.f540q.bringToFront();
        }
    }

    public final void k() {
        if (this.f535l != null) {
            EditText editText = this.f531f;
            a(editText == null ? 0 : editText.getText().length());
        }
    }

    public final void l() {
        ColorStateList colorStateList;
        ColorStateList colorStateList2;
        TextView textView = this.f535l;
        if (textView != null) {
            a(textView, this.f534k ? this.f536m : this.f537n);
            if (!this.f534k && (colorStateList2 = this.f543t) != null) {
                this.f535l.setTextColor(colorStateList2);
            }
            if (this.f534k && (colorStateList = this.u) != null) {
                this.f535l.setTextColor(colorStateList);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x007d  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0106  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean m() {
        /*
            r10 = this;
            android.widget.EditText r0 = r10.f531f
            r1 = 0
            if (r0 != 0) goto L_0x0006
            return r1
        L_0x0006:
            android.graphics.drawable.Drawable r0 = r10.getStartIconDrawable()
            r2 = 1
            if (r0 != 0) goto L_0x0011
            java.lang.CharSequence r0 = r10.v
            if (r0 == 0) goto L_0x001b
        L_0x0011:
            android.widget.LinearLayout r0 = r10.c
            int r0 = r0.getMeasuredWidth()
            if (r0 <= 0) goto L_0x001b
            r0 = 1
            goto L_0x001c
        L_0x001b:
            r0 = 0
        L_0x001c:
            r3 = 0
            r4 = 3
            r5 = 2
            if (r0 == 0) goto L_0x005d
            android.widget.LinearLayout r0 = r10.c
            int r0 = r0.getMeasuredWidth()
            android.widget.EditText r6 = r10.f531f
            int r6 = r6.getPaddingLeft()
            int r0 = r0 - r6
            android.graphics.drawable.Drawable r6 = r10.W
            if (r6 == 0) goto L_0x0036
            int r6 = r10.a0
            if (r6 == r0) goto L_0x0042
        L_0x0036:
            android.graphics.drawable.ColorDrawable r6 = new android.graphics.drawable.ColorDrawable
            r6.<init>()
            r10.W = r6
            r10.a0 = r0
            r6.setBounds(r1, r1, r0, r2)
        L_0x0042:
            android.widget.EditText r0 = r10.f531f
            android.graphics.drawable.Drawable[] r0 = r0.getCompoundDrawablesRelative()
            r6 = r0[r1]
            android.graphics.drawable.Drawable r7 = r10.W
            if (r6 == r7) goto L_0x005b
            android.widget.EditText r6 = r10.f531f
            r8 = r0[r2]
            r9 = r0[r5]
            r0 = r0[r4]
            r6.setCompoundDrawablesRelative(r7, r8, r9, r0)
        L_0x0059:
            r0 = 1
            goto L_0x0075
        L_0x005b:
            r0 = 0
            goto L_0x0075
        L_0x005d:
            android.graphics.drawable.Drawable r0 = r10.W
            if (r0 == 0) goto L_0x005b
            android.widget.EditText r0 = r10.f531f
            android.graphics.drawable.Drawable[] r0 = r0.getCompoundDrawablesRelative()
            android.widget.EditText r6 = r10.f531f
            r7 = r0[r2]
            r8 = r0[r5]
            r0 = r0[r4]
            r6.setCompoundDrawablesRelative(r3, r7, r8, r0)
            r10.W = r3
            goto L_0x0059
        L_0x0075:
            com.google.android.material.internal.CheckableImageButton r6 = r10.p0
            int r6 = r6.getVisibility()
            if (r6 == 0) goto L_0x008d
            boolean r6 = r10.f()
            if (r6 == 0) goto L_0x0089
            boolean r6 = r10.g()
            if (r6 != 0) goto L_0x008d
        L_0x0089:
            java.lang.CharSequence r6 = r10.x
            if (r6 == 0) goto L_0x0097
        L_0x008d:
            android.widget.LinearLayout r6 = r10.d
            int r6 = r6.getMeasuredWidth()
            if (r6 <= 0) goto L_0x0097
            r6 = 1
            goto L_0x0098
        L_0x0097:
            r6 = 0
        L_0x0098:
            if (r6 == 0) goto L_0x0106
            android.widget.TextView r3 = r10.y
            int r3 = r3.getMeasuredWidth()
            android.widget.EditText r6 = r10.f531f
            int r6 = r6.getPaddingRight()
            int r3 = r3 - r6
            com.google.android.material.internal.CheckableImageButton r6 = r10.getEndIconToUpdateDummyDrawable()
            if (r6 == 0) goto L_0x00bd
            int r7 = r6.getMeasuredWidth()
            int r7 = r7 + r3
            android.view.ViewGroup$LayoutParams r3 = r6.getLayoutParams()
            android.view.ViewGroup$MarginLayoutParams r3 = (android.view.ViewGroup.MarginLayoutParams) r3
            int r3 = r3.getMarginStart()
            int r3 = r3 + r7
        L_0x00bd:
            android.widget.EditText r6 = r10.f531f
            android.graphics.drawable.Drawable[] r6 = r6.getCompoundDrawablesRelative()
            android.graphics.drawable.Drawable r7 = r10.l0
            if (r7 == 0) goto L_0x00de
            int r8 = r10.m0
            if (r8 == r3) goto L_0x00de
            r10.m0 = r3
            r7.setBounds(r1, r1, r3, r2)
            android.widget.EditText r0 = r10.f531f
            r1 = r6[r1]
            r3 = r6[r2]
            android.graphics.drawable.Drawable r5 = r10.l0
            r4 = r6[r4]
            r0.setCompoundDrawablesRelative(r1, r3, r5, r4)
            goto L_0x0127
        L_0x00de:
            android.graphics.drawable.Drawable r7 = r10.l0
            if (r7 != 0) goto L_0x00ee
            android.graphics.drawable.ColorDrawable r7 = new android.graphics.drawable.ColorDrawable
            r7.<init>()
            r10.l0 = r7
            r10.m0 = r3
            r7.setBounds(r1, r1, r3, r2)
        L_0x00ee:
            r3 = r6[r5]
            android.graphics.drawable.Drawable r7 = r10.l0
            if (r3 == r7) goto L_0x0104
            r0 = r6[r5]
            r10.n0 = r0
            android.widget.EditText r0 = r10.f531f
            r1 = r6[r1]
            r3 = r6[r2]
            r4 = r6[r4]
            r0.setCompoundDrawablesRelative(r1, r3, r7, r4)
            goto L_0x0127
        L_0x0104:
            r2 = r0
            goto L_0x0127
        L_0x0106:
            android.graphics.drawable.Drawable r6 = r10.l0
            if (r6 == 0) goto L_0x0128
            android.widget.EditText r6 = r10.f531f
            android.graphics.drawable.Drawable[] r6 = r6.getCompoundDrawablesRelative()
            r5 = r6[r5]
            android.graphics.drawable.Drawable r7 = r10.l0
            if (r5 != r7) goto L_0x0124
            android.widget.EditText r0 = r10.f531f
            r1 = r6[r1]
            r5 = r6[r2]
            android.graphics.drawable.Drawable r7 = r10.n0
            r4 = r6[r4]
            r0.setCompoundDrawablesRelative(r1, r5, r7, r4)
            goto L_0x0125
        L_0x0124:
            r2 = r0
        L_0x0125:
            r10.l0 = r3
        L_0x0127:
            r0 = r2
        L_0x0128:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.textfield.TextInputLayout.m():boolean");
    }

    public void n() {
        Drawable background;
        TextView textView;
        EditText editText = this.f531f;
        if (editText != null && this.G == 0 && (background = editText.getBackground()) != null) {
            if (DrawableUtils.a(background)) {
                background = background.mutate();
            }
            if (this.h.c()) {
                background.setColorFilter(AppCompatDrawableManager.a(this.h.d(), PorterDuff.Mode.SRC_IN));
            } else if (!this.f534k || (textView = this.f535l) == null) {
                ResourcesFlusher.a(background);
                this.f531f.refreshDrawableState();
            } else {
                background.setColorFilter(AppCompatDrawableManager.a(textView.getCurrentTextColor(), PorterDuff.Mode.SRC_IN));
            }
        }
    }

    public final void o() {
        if (this.G != 1) {
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) this.b.getLayoutParams();
            int d2 = d();
            if (d2 != layoutParams.topMargin) {
                layoutParams.topMargin = d2;
                this.b.requestLayout();
            }
        }
    }

    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        int i6;
        int i7;
        super.onLayout(z2, i2, i3, i4, i5);
        EditText editText = this.f531f;
        if (editText != null) {
            Rect rect = this.N;
            DescendantOffsetUtils.a(this, editText, rect);
            MaterialShapeDrawable materialShapeDrawable = this.D;
            if (materialShapeDrawable != null) {
                int i8 = rect.bottom;
                materialShapeDrawable.setBounds(rect.left, i8 - this.K, rect.right, i8);
            }
            if (this.z) {
                CollapsingTextHelper collapsingTextHelper = this.D0;
                float textSize = this.f531f.getTextSize();
                if (collapsingTextHelper.f2168i != textSize) {
                    collapsingTextHelper.f2168i = textSize;
                    collapsingTextHelper.e();
                }
                int gravity = this.f531f.getGravity();
                this.D0.a((gravity & -113) | 48);
                CollapsingTextHelper collapsingTextHelper2 = this.D0;
                if (collapsingTextHelper2.g != gravity) {
                    collapsingTextHelper2.g = gravity;
                    collapsingTextHelper2.e();
                }
                CollapsingTextHelper collapsingTextHelper3 = this.D0;
                if (this.f531f != null) {
                    Rect rect2 = this.O;
                    boolean z3 = false;
                    boolean z4 = ViewCompat.k(this) == 1;
                    rect2.bottom = rect.bottom;
                    int i9 = this.G;
                    if (i9 == 1) {
                        rect2.left = a(rect.left, z4);
                        rect2.top = rect.top + this.H;
                        rect2.right = b(rect.right, z4);
                    } else if (i9 != 2) {
                        rect2.left = a(rect.left, z4);
                        rect2.top = getPaddingTop();
                        rect2.right = b(rect.right, z4);
                    } else {
                        rect2.left = this.f531f.getPaddingLeft() + rect.left;
                        rect2.top = rect.top - d();
                        rect2.right = rect.right - this.f531f.getPaddingRight();
                    }
                    if (collapsingTextHelper3 != null) {
                        int i10 = rect2.left;
                        int i11 = rect2.top;
                        int i12 = rect2.right;
                        int i13 = rect2.bottom;
                        if (!CollapsingTextHelper.a(collapsingTextHelper3.f2166e, i10, i11, i12, i13)) {
                            collapsingTextHelper3.f2166e.set(i10, i11, i12, i13);
                            collapsingTextHelper3.F = true;
                            collapsingTextHelper3.d();
                        }
                        CollapsingTextHelper collapsingTextHelper4 = this.D0;
                        if (this.f531f != null) {
                            Rect rect3 = this.O;
                            TextPaint textPaint = collapsingTextHelper4.H;
                            textPaint.setTextSize(collapsingTextHelper4.f2168i);
                            textPaint.setTypeface(collapsingTextHelper4.f2179t);
                            float f2 = -collapsingTextHelper4.H.ascent();
                            rect3.left = this.f531f.getCompoundPaddingLeft() + rect.left;
                            if (this.G == 1 && this.f531f.getMinLines() <= 1) {
                                i6 = (int) (((float) rect.centerY()) - (f2 / 2.0f));
                            } else {
                                i6 = rect.top + this.f531f.getCompoundPaddingTop();
                            }
                            rect3.top = i6;
                            rect3.right = rect.right - this.f531f.getCompoundPaddingRight();
                            if (this.G == 1 && this.f531f.getMinLines() <= 1) {
                                z3 = true;
                            }
                            if (z3) {
                                i7 = (int) (((float) rect3.top) + f2);
                            } else {
                                i7 = rect.bottom - this.f531f.getCompoundPaddingBottom();
                            }
                            rect3.bottom = i7;
                            int i14 = rect3.left;
                            int i15 = rect3.top;
                            int i16 = rect3.right;
                            if (!CollapsingTextHelper.a(collapsingTextHelper4.d, i14, i15, i16, i7)) {
                                collapsingTextHelper4.d.set(i14, i15, i16, i7);
                                collapsingTextHelper4.F = true;
                                collapsingTextHelper4.d();
                            }
                            this.D0.e();
                            if (e() && !this.C0) {
                                i();
                                return;
                            }
                            return;
                        }
                        throw new IllegalStateException();
                    }
                    throw null;
                }
                throw new IllegalStateException();
            }
        }
    }

    public void onMeasure(int i2, int i3) {
        EditText editText;
        int max;
        super.onMeasure(i2, i3);
        boolean z2 = false;
        if (this.f531f != null && this.f531f.getMeasuredHeight() < (max = Math.max(this.d.getMeasuredHeight(), this.c.getMeasuredHeight()))) {
            this.f531f.setMinimumHeight(max);
            z2 = true;
        }
        boolean m2 = m();
        if (z2 || m2) {
            this.f531f.post(new c());
        }
        if (this.f540q != null && (editText = this.f531f) != null) {
            this.f540q.setGravity((editText.getGravity() & -113) | 48);
            this.f540q.setPadding(this.f531f.getCompoundPaddingLeft(), this.f531f.getCompoundPaddingTop(), this.f531f.getCompoundPaddingRight(), this.f531f.getCompoundPaddingBottom());
        }
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof h)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        h hVar = (h) parcelable;
        super.onRestoreInstanceState(hVar.b);
        setError(hVar.d);
        if (hVar.f544e) {
            this.f0.post(new b());
        }
        requestLayout();
    }

    public Parcelable onSaveInstanceState() {
        h hVar = new h(super.onSaveInstanceState());
        if (this.h.c()) {
            hVar.d = getError();
        }
        hVar.f544e = f() && this.f0.d;
        return hVar;
    }

    public final void p() {
        if (this.f531f != null) {
            TextView textView = this.w;
            int i2 = 0;
            if (!(this.R.getVisibility() == 0)) {
                i2 = this.f531f.getPaddingLeft();
            }
            textView.setPadding(i2, this.f531f.getCompoundPaddingTop(), this.w.getCompoundPaddingRight(), this.f531f.getCompoundPaddingBottom());
        }
    }

    public final void q() {
        this.w.setVisibility((this.v == null || this.C0) ? 8 : 0);
        m();
    }

    public final void r() {
        if (this.f531f != null) {
            TextView textView = this.y;
            int paddingLeft = textView.getPaddingLeft();
            int paddingTop = this.f531f.getPaddingTop();
            int i2 = 0;
            if (!g()) {
                if (!(this.p0.getVisibility() == 0)) {
                    i2 = this.f531f.getPaddingRight();
                }
            }
            textView.setPadding(paddingLeft, paddingTop, i2, this.f531f.getPaddingBottom());
        }
    }

    public final void s() {
        int visibility = this.y.getVisibility();
        int i2 = 0;
        boolean z2 = this.x != null && !this.C0;
        TextView textView = this.y;
        if (!z2) {
            i2 = 8;
        }
        textView.setVisibility(i2);
        if (visibility != this.y.getVisibility()) {
            getEndIconDelegate().a(z2);
        }
        m();
    }

    public void setBoxBackgroundColor(int i2) {
        if (this.M != i2) {
            this.M = i2;
            this.x0 = i2;
            a();
        }
    }

    public void setBoxBackgroundColorResource(int i2) {
        setBoxBackgroundColor(ContextCompat.a(getContext(), i2));
    }

    public void setBoxBackgroundMode(int i2) {
        if (i2 != this.G) {
            this.G = i2;
            if (this.f531f != null) {
                h();
            }
        }
    }

    public void setBoxStrokeColor(int i2) {
        if (this.v0 != i2) {
            this.v0 = i2;
            t();
        }
    }

    public void setBoxStrokeColorStateList(ColorStateList colorStateList) {
        if (colorStateList.isStateful()) {
            this.t0 = colorStateList.getDefaultColor();
            this.B0 = colorStateList.getColorForState(new int[]{-16842910}, -1);
            this.u0 = colorStateList.getColorForState(new int[]{16843623, 16842910}, -1);
            this.v0 = colorStateList.getColorForState(new int[]{16842908, 16842910}, -1);
        } else if (this.v0 != colorStateList.getDefaultColor()) {
            this.v0 = colorStateList.getDefaultColor();
        }
        t();
    }

    public void setBoxStrokeErrorColor(ColorStateList colorStateList) {
        if (this.w0 != colorStateList) {
            this.w0 = colorStateList;
            t();
        }
    }

    public void setCounterEnabled(boolean z2) {
        if (this.f532i != z2) {
            if (z2) {
                AppCompatTextView appCompatTextView = new AppCompatTextView(getContext());
                this.f535l = appCompatTextView;
                appCompatTextView.setId(j.c.a.b.f.textinput_counter);
                Typeface typeface = this.Q;
                if (typeface != null) {
                    this.f535l.setTypeface(typeface);
                }
                this.f535l.setMaxLines(1);
                this.h.a(this.f535l, 2);
                ((ViewGroup.MarginLayoutParams) this.f535l.getLayoutParams()).setMarginStart(getResources().getDimensionPixelOffset(j.c.a.b.d.mtrl_textinput_counter_margin_start));
                l();
                k();
            } else {
                this.h.b(this.f535l, 2);
                this.f535l = null;
            }
            this.f532i = z2;
        }
    }

    public void setCounterMaxLength(int i2) {
        if (this.f533j != i2) {
            if (i2 > 0) {
                this.f533j = i2;
            } else {
                this.f533j = -1;
            }
            if (this.f532i) {
                k();
            }
        }
    }

    public void setCounterOverflowTextAppearance(int i2) {
        if (this.f536m != i2) {
            this.f536m = i2;
            l();
        }
    }

    public void setCounterOverflowTextColor(ColorStateList colorStateList) {
        if (this.u != colorStateList) {
            this.u = colorStateList;
            l();
        }
    }

    public void setCounterTextAppearance(int i2) {
        if (this.f537n != i2) {
            this.f537n = i2;
            l();
        }
    }

    public void setCounterTextColor(ColorStateList colorStateList) {
        if (this.f543t != colorStateList) {
            this.f543t = colorStateList;
            l();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.material.textfield.TextInputLayout.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.google.android.material.textfield.TextInputLayout.a(android.view.ViewGroup, boolean):void
      com.google.android.material.textfield.TextInputLayout.a(com.google.android.material.internal.CheckableImageButton, android.view.View$OnLongClickListener):void
      com.google.android.material.textfield.TextInputLayout.a(int, boolean):int
      com.google.android.material.textfield.TextInputLayout.a(android.widget.TextView, int):void
      com.google.android.material.textfield.TextInputLayout.a(com.google.android.material.internal.CheckableImageButton, android.content.res.ColorStateList):void
      com.google.android.material.textfield.TextInputLayout.a(boolean, boolean):void */
    public void setDefaultHintTextColor(ColorStateList colorStateList) {
        this.r0 = colorStateList;
        this.s0 = colorStateList;
        if (this.f531f != null) {
            a(false, false);
        }
    }

    public void setEnabled(boolean z2) {
        a(this, z2);
        super.setEnabled(z2);
    }

    public void setEndIconActivated(boolean z2) {
        this.f0.setActivated(z2);
    }

    public void setEndIconCheckable(boolean z2) {
        this.f0.setCheckable(z2);
    }

    public void setEndIconContentDescription(int i2) {
        setEndIconContentDescription(i2 != 0 ? getResources().getText(i2) : null);
    }

    public void setEndIconDrawable(int i2) {
        setEndIconDrawable(i2 != 0 ? AppCompatResources.c(getContext(), i2) : null);
    }

    public void setEndIconMode(int i2) {
        int i3 = this.d0;
        this.d0 = i2;
        setEndIconVisible(i2 != 0);
        if (getEndIconDelegate().a(this.G)) {
            getEndIconDelegate().a();
            b();
            Iterator<g> it = this.g0.iterator();
            while (it.hasNext()) {
                it.next().a(this, i3);
            }
            return;
        }
        StringBuilder a2 = outline.a("The current box background mode ");
        a2.append(this.G);
        a2.append(" is not supported by the end icon mode ");
        a2.append(i2);
        throw new IllegalStateException(a2.toString());
    }

    public void setEndIconOnClickListener(View.OnClickListener onClickListener) {
        CheckableImageButton checkableImageButton = this.f0;
        View.OnLongClickListener onLongClickListener = this.o0;
        checkableImageButton.setOnClickListener(onClickListener);
        a(checkableImageButton, onLongClickListener);
    }

    public void setEndIconOnLongClickListener(View.OnLongClickListener onLongClickListener) {
        this.o0 = onLongClickListener;
        CheckableImageButton checkableImageButton = this.f0;
        checkableImageButton.setOnLongClickListener(onLongClickListener);
        a(checkableImageButton, onLongClickListener);
    }

    public void setEndIconTintList(ColorStateList colorStateList) {
        if (this.h0 != colorStateList) {
            this.h0 = colorStateList;
            this.i0 = true;
            b();
        }
    }

    public void setEndIconTintMode(PorterDuff.Mode mode) {
        if (this.j0 != mode) {
            this.j0 = mode;
            this.k0 = true;
            b();
        }
    }

    public void setEndIconVisible(boolean z2) {
        if (g() != z2) {
            this.f0.setVisibility(z2 ? 0 : 8);
            r();
            m();
        }
    }

    public void setError(CharSequence charSequence) {
        if (!this.h.f2280l) {
            if (!TextUtils.isEmpty(charSequence)) {
                setErrorEnabled(true);
            } else {
                return;
            }
        }
        if (!TextUtils.isEmpty(charSequence)) {
            IndicatorViewController indicatorViewController = this.h;
            indicatorViewController.b();
            indicatorViewController.f2279k = charSequence;
            indicatorViewController.f2281m.setText(charSequence);
            if (indicatorViewController.f2277i != 1) {
                indicatorViewController.f2278j = 1;
            }
            indicatorViewController.a(indicatorViewController.f2277i, indicatorViewController.f2278j, indicatorViewController.a(indicatorViewController.f2281m, charSequence));
            return;
        }
        this.h.e();
    }

    public void setErrorContentDescription(CharSequence charSequence) {
        IndicatorViewController indicatorViewController = this.h;
        indicatorViewController.f2282n = charSequence;
        TextView textView = indicatorViewController.f2281m;
        if (textView != null) {
            textView.setContentDescription(charSequence);
        }
    }

    public void setErrorEnabled(boolean z2) {
        IndicatorViewController indicatorViewController = this.h;
        if (indicatorViewController.f2280l != z2) {
            indicatorViewController.b();
            if (z2) {
                AppCompatTextView appCompatTextView = new AppCompatTextView(indicatorViewController.a);
                indicatorViewController.f2281m = appCompatTextView;
                appCompatTextView.setId(j.c.a.b.f.textinput_error);
                indicatorViewController.f2281m.setTextAlignment(5);
                Typeface typeface = indicatorViewController.v;
                if (typeface != null) {
                    indicatorViewController.f2281m.setTypeface(typeface);
                }
                int i2 = indicatorViewController.f2283o;
                indicatorViewController.f2283o = i2;
                TextView textView = indicatorViewController.f2281m;
                if (textView != null) {
                    indicatorViewController.b.a(textView, i2);
                }
                ColorStateList colorStateList = indicatorViewController.f2284p;
                indicatorViewController.f2284p = colorStateList;
                TextView textView2 = indicatorViewController.f2281m;
                if (!(textView2 == null || colorStateList == null)) {
                    textView2.setTextColor(colorStateList);
                }
                CharSequence charSequence = indicatorViewController.f2282n;
                indicatorViewController.f2282n = charSequence;
                TextView textView3 = indicatorViewController.f2281m;
                if (textView3 != null) {
                    textView3.setContentDescription(charSequence);
                }
                indicatorViewController.f2281m.setVisibility(4);
                ViewCompat.g(indicatorViewController.f2281m, 1);
                indicatorViewController.a(indicatorViewController.f2281m, 0);
            } else {
                indicatorViewController.e();
                indicatorViewController.b(indicatorViewController.f2281m, 0);
                indicatorViewController.f2281m = null;
                indicatorViewController.b.n();
                indicatorViewController.b.t();
            }
            indicatorViewController.f2280l = z2;
        }
    }

    public void setErrorIconDrawable(int i2) {
        setErrorIconDrawable(i2 != 0 ? AppCompatResources.c(getContext(), i2) : null);
    }

    public void setErrorIconTintList(ColorStateList colorStateList) {
        this.q0 = colorStateList;
        Drawable drawable = this.p0.getDrawable();
        if (drawable != null) {
            drawable = ResourcesFlusher.d(drawable).mutate();
            drawable.setTintList(colorStateList);
        }
        if (this.p0.getDrawable() != drawable) {
            this.p0.setImageDrawable(drawable);
        }
    }

    public void setErrorIconTintMode(PorterDuff.Mode mode) {
        Drawable drawable = this.p0.getDrawable();
        if (drawable != null) {
            drawable = ResourcesFlusher.d(drawable).mutate();
            drawable.setTintMode(mode);
        }
        if (this.p0.getDrawable() != drawable) {
            this.p0.setImageDrawable(drawable);
        }
    }

    public void setErrorTextAppearance(int i2) {
        IndicatorViewController indicatorViewController = this.h;
        indicatorViewController.f2283o = i2;
        TextView textView = indicatorViewController.f2281m;
        if (textView != null) {
            indicatorViewController.b.a(textView, i2);
        }
    }

    public void setErrorTextColor(ColorStateList colorStateList) {
        IndicatorViewController indicatorViewController = this.h;
        indicatorViewController.f2284p = colorStateList;
        TextView textView = indicatorViewController.f2281m;
        if (textView != null && colorStateList != null) {
            textView.setTextColor(colorStateList);
        }
    }

    public void setHelperText(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            if (!this.h.f2286r) {
                setHelperTextEnabled(true);
            }
            IndicatorViewController indicatorViewController = this.h;
            indicatorViewController.b();
            indicatorViewController.f2285q = charSequence;
            indicatorViewController.f2287s.setText(charSequence);
            if (indicatorViewController.f2277i != 2) {
                indicatorViewController.f2278j = 2;
            }
            indicatorViewController.a(indicatorViewController.f2277i, indicatorViewController.f2278j, indicatorViewController.a(indicatorViewController.f2287s, charSequence));
        } else if (this.h.f2286r) {
            setHelperTextEnabled(false);
        }
    }

    public void setHelperTextColor(ColorStateList colorStateList) {
        IndicatorViewController indicatorViewController = this.h;
        indicatorViewController.u = colorStateList;
        TextView textView = indicatorViewController.f2287s;
        if (textView != null && colorStateList != null) {
            textView.setTextColor(colorStateList);
        }
    }

    public void setHelperTextEnabled(boolean z2) {
        IndicatorViewController indicatorViewController = this.h;
        if (indicatorViewController.f2286r != z2) {
            indicatorViewController.b();
            if (z2) {
                AppCompatTextView appCompatTextView = new AppCompatTextView(indicatorViewController.a);
                indicatorViewController.f2287s = appCompatTextView;
                appCompatTextView.setId(j.c.a.b.f.textinput_helper_text);
                indicatorViewController.f2287s.setTextAlignment(5);
                Typeface typeface = indicatorViewController.v;
                if (typeface != null) {
                    indicatorViewController.f2287s.setTypeface(typeface);
                }
                indicatorViewController.f2287s.setVisibility(4);
                ViewCompat.g(indicatorViewController.f2287s, 1);
                int i2 = indicatorViewController.f2288t;
                indicatorViewController.f2288t = i2;
                TextView textView = indicatorViewController.f2287s;
                if (textView != null) {
                    ResourcesFlusher.d(textView, i2);
                }
                ColorStateList colorStateList = indicatorViewController.u;
                indicatorViewController.u = colorStateList;
                TextView textView2 = indicatorViewController.f2287s;
                if (!(textView2 == null || colorStateList == null)) {
                    textView2.setTextColor(colorStateList);
                }
                indicatorViewController.a(indicatorViewController.f2287s, 1);
            } else {
                indicatorViewController.b();
                if (indicatorViewController.f2277i == 2) {
                    indicatorViewController.f2278j = 0;
                }
                indicatorViewController.a(indicatorViewController.f2277i, indicatorViewController.f2278j, indicatorViewController.a(indicatorViewController.f2287s, (CharSequence) null));
                indicatorViewController.b(indicatorViewController.f2287s, 1);
                indicatorViewController.f2287s = null;
                indicatorViewController.b.n();
                indicatorViewController.b.t();
            }
            indicatorViewController.f2286r = z2;
        }
    }

    public void setHelperTextTextAppearance(int i2) {
        IndicatorViewController indicatorViewController = this.h;
        indicatorViewController.f2288t = i2;
        TextView textView = indicatorViewController.f2287s;
        if (textView != null) {
            ResourcesFlusher.d(textView, i2);
        }
    }

    public void setHint(CharSequence charSequence) {
        if (this.z) {
            setHintInternal(charSequence);
            sendAccessibilityEvent(2048);
        }
    }

    public void setHintAnimationEnabled(boolean z2) {
        this.E0 = z2;
    }

    public void setHintEnabled(boolean z2) {
        if (z2 != this.z) {
            this.z = z2;
            if (!z2) {
                this.B = false;
                if (!TextUtils.isEmpty(this.A) && TextUtils.isEmpty(this.f531f.getHint())) {
                    this.f531f.setHint(this.A);
                }
                setHintInternal(null);
            } else {
                CharSequence hint = this.f531f.getHint();
                if (!TextUtils.isEmpty(hint)) {
                    if (TextUtils.isEmpty(this.A)) {
                        setHint(hint);
                    }
                    this.f531f.setHint((CharSequence) null);
                }
                this.B = true;
            }
            if (this.f531f != null) {
                o();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.material.textfield.TextInputLayout.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.google.android.material.textfield.TextInputLayout.a(android.view.ViewGroup, boolean):void
      com.google.android.material.textfield.TextInputLayout.a(com.google.android.material.internal.CheckableImageButton, android.view.View$OnLongClickListener):void
      com.google.android.material.textfield.TextInputLayout.a(int, boolean):int
      com.google.android.material.textfield.TextInputLayout.a(android.widget.TextView, int):void
      com.google.android.material.textfield.TextInputLayout.a(com.google.android.material.internal.CheckableImageButton, android.content.res.ColorStateList):void
      com.google.android.material.textfield.TextInputLayout.a(boolean, boolean):void */
    public void setHintTextAppearance(int i2) {
        CollapsingTextHelper collapsingTextHelper = this.D0;
        TextAppearance textAppearance = new TextAppearance(collapsingTextHelper.a.getContext(), i2);
        ColorStateList colorStateList = textAppearance.b;
        if (colorStateList != null) {
            collapsingTextHelper.f2171l = colorStateList;
        }
        float f2 = textAppearance.a;
        if (f2 != 0.0f) {
            collapsingTextHelper.f2169j = f2;
        }
        ColorStateList colorStateList2 = textAppearance.f2197f;
        if (colorStateList2 != null) {
            collapsingTextHelper.N = colorStateList2;
        }
        collapsingTextHelper.L = textAppearance.g;
        collapsingTextHelper.M = textAppearance.h;
        collapsingTextHelper.K = textAppearance.f2198i;
        CancelableFontCallback cancelableFontCallback = collapsingTextHelper.v;
        if (cancelableFontCallback != null) {
            cancelableFontCallback.c = true;
        }
        CollapsingTextHelper0 collapsingTextHelper0 = new CollapsingTextHelper0(collapsingTextHelper);
        textAppearance.a();
        collapsingTextHelper.v = new CancelableFontCallback(collapsingTextHelper0, textAppearance.f2201l);
        textAppearance.a(collapsingTextHelper.a.getContext(), collapsingTextHelper.v);
        collapsingTextHelper.e();
        this.s0 = this.D0.f2171l;
        if (this.f531f != null) {
            a(false, false);
            o();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.material.textfield.TextInputLayout.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.google.android.material.textfield.TextInputLayout.a(android.view.ViewGroup, boolean):void
      com.google.android.material.textfield.TextInputLayout.a(com.google.android.material.internal.CheckableImageButton, android.view.View$OnLongClickListener):void
      com.google.android.material.textfield.TextInputLayout.a(int, boolean):int
      com.google.android.material.textfield.TextInputLayout.a(android.widget.TextView, int):void
      com.google.android.material.textfield.TextInputLayout.a(com.google.android.material.internal.CheckableImageButton, android.content.res.ColorStateList):void
      com.google.android.material.textfield.TextInputLayout.a(boolean, boolean):void */
    public void setHintTextColor(ColorStateList colorStateList) {
        if (this.s0 != colorStateList) {
            if (this.r0 == null) {
                CollapsingTextHelper collapsingTextHelper = this.D0;
                if (collapsingTextHelper.f2171l != colorStateList) {
                    collapsingTextHelper.f2171l = colorStateList;
                    collapsingTextHelper.e();
                }
            }
            this.s0 = colorStateList;
            if (this.f531f != null) {
                a(false, false);
            }
        }
    }

    @Deprecated
    public void setPasswordVisibilityToggleContentDescription(int i2) {
        setPasswordVisibilityToggleContentDescription(i2 != 0 ? getResources().getText(i2) : null);
    }

    @Deprecated
    public void setPasswordVisibilityToggleDrawable(int i2) {
        setPasswordVisibilityToggleDrawable(i2 != 0 ? AppCompatResources.c(getContext(), i2) : null);
    }

    @Deprecated
    public void setPasswordVisibilityToggleEnabled(boolean z2) {
        if (z2 && this.d0 != 1) {
            setEndIconMode(1);
        } else if (!z2) {
            setEndIconMode(0);
        }
    }

    @Deprecated
    public void setPasswordVisibilityToggleTintList(ColorStateList colorStateList) {
        this.h0 = colorStateList;
        this.i0 = true;
        b();
    }

    @Deprecated
    public void setPasswordVisibilityToggleTintMode(PorterDuff.Mode mode) {
        this.j0 = mode;
        this.k0 = true;
        b();
    }

    public void setPlaceholderText(CharSequence charSequence) {
        int i2 = 0;
        if (!this.f539p || !TextUtils.isEmpty(charSequence)) {
            if (!this.f539p) {
                setPlaceholderTextEnabled(true);
            }
            this.f538o = charSequence;
        } else {
            setPlaceholderTextEnabled(false);
        }
        EditText editText = this.f531f;
        if (editText != null) {
            i2 = editText.getText().length();
        }
        b(i2);
    }

    public void setPlaceholderTextAppearance(int i2) {
        this.f542s = i2;
        TextView textView = this.f540q;
        if (textView != null) {
            ResourcesFlusher.d(textView, i2);
        }
    }

    public void setPlaceholderTextColor(ColorStateList colorStateList) {
        if (this.f541r != colorStateList) {
            this.f541r = colorStateList;
            TextView textView = this.f540q;
            if (textView != null && colorStateList != null) {
                textView.setTextColor(colorStateList);
            }
        }
    }

    public void setPrefixText(CharSequence charSequence) {
        this.v = TextUtils.isEmpty(charSequence) ? null : charSequence;
        this.w.setText(charSequence);
        q();
    }

    public void setPrefixTextAppearance(int i2) {
        ResourcesFlusher.d(this.w, i2);
    }

    public void setPrefixTextColor(ColorStateList colorStateList) {
        this.w.setTextColor(colorStateList);
    }

    public void setStartIconCheckable(boolean z2) {
        this.R.setCheckable(z2);
    }

    public void setStartIconContentDescription(int i2) {
        setStartIconContentDescription(i2 != 0 ? getResources().getText(i2) : null);
    }

    public void setStartIconDrawable(int i2) {
        setStartIconDrawable(i2 != 0 ? AppCompatResources.c(getContext(), i2) : null);
    }

    public void setStartIconOnClickListener(View.OnClickListener onClickListener) {
        CheckableImageButton checkableImageButton = this.R;
        View.OnLongClickListener onLongClickListener = this.b0;
        checkableImageButton.setOnClickListener(onClickListener);
        a(checkableImageButton, onLongClickListener);
    }

    public void setStartIconOnLongClickListener(View.OnLongClickListener onLongClickListener) {
        this.b0 = onLongClickListener;
        CheckableImageButton checkableImageButton = this.R;
        checkableImageButton.setOnLongClickListener(onLongClickListener);
        a(checkableImageButton, onLongClickListener);
    }

    public void setStartIconTintList(ColorStateList colorStateList) {
        if (this.S != colorStateList) {
            this.S = colorStateList;
            this.T = true;
            c();
        }
    }

    public void setStartIconTintMode(PorterDuff.Mode mode) {
        if (this.U != mode) {
            this.U = mode;
            this.V = true;
            c();
        }
    }

    public void setStartIconVisible(boolean z2) {
        int i2 = 0;
        if ((this.R.getVisibility() == 0) != z2) {
            CheckableImageButton checkableImageButton = this.R;
            if (!z2) {
                i2 = 8;
            }
            checkableImageButton.setVisibility(i2);
            p();
            m();
        }
    }

    public void setSuffixText(CharSequence charSequence) {
        this.x = TextUtils.isEmpty(charSequence) ? null : charSequence;
        this.y.setText(charSequence);
        s();
    }

    public void setSuffixTextAppearance(int i2) {
        ResourcesFlusher.d(this.y, i2);
    }

    public void setSuffixTextColor(ColorStateList colorStateList) {
        this.y.setTextColor(colorStateList);
    }

    public void setTextInputAccessibilityDelegate(e eVar) {
        EditText editText = this.f531f;
        if (editText != null) {
            ViewCompat.a(editText, eVar);
        }
    }

    public void setTypeface(Typeface typeface) {
        if (typeface != this.Q) {
            this.Q = typeface;
            this.D0.a(typeface);
            IndicatorViewController indicatorViewController = this.h;
            if (typeface != indicatorViewController.v) {
                indicatorViewController.v = typeface;
                TextView textView = indicatorViewController.f2281m;
                if (textView != null) {
                    textView.setTypeface(typeface);
                }
                TextView textView2 = indicatorViewController.f2287s;
                if (textView2 != null) {
                    textView2.setTypeface(typeface);
                }
            }
            TextView textView3 = this.f535l;
            if (textView3 != null) {
                textView3.setTypeface(typeface);
            }
        }
    }

    public void t() {
        TextView textView;
        EditText editText;
        EditText editText2;
        if (this.C != null && this.G != 0) {
            boolean z2 = false;
            boolean z3 = isFocused() || ((editText2 = this.f531f) != null && editText2.hasFocus());
            boolean z4 = isHovered() || ((editText = this.f531f) != null && editText.isHovered());
            if (!isEnabled()) {
                this.L = this.B0;
            } else if (this.h.c()) {
                if (this.w0 != null) {
                    b(z3, z4);
                } else {
                    this.L = this.h.d();
                }
            } else if (!this.f534k || (textView = this.f535l) == null) {
                if (z3) {
                    this.L = this.v0;
                } else if (z4) {
                    this.L = this.u0;
                } else {
                    this.L = this.t0;
                }
            } else if (this.w0 != null) {
                b(z3, z4);
            } else {
                this.L = textView.getCurrentTextColor();
            }
            if (getErrorIconDrawable() != null) {
                IndicatorViewController indicatorViewController = this.h;
                if (indicatorViewController.f2280l && indicatorViewController.c()) {
                    z2 = true;
                }
            }
            setErrorIconVisible(z2);
            a(this.p0, this.q0);
            a(this.R, this.S);
            a(this.f0, this.h0);
            if (getEndIconDelegate().b()) {
                if (!this.h.c() || getEndIconDrawable() == null) {
                    b();
                } else {
                    Drawable mutate = ResourcesFlusher.d(getEndIconDrawable()).mutate();
                    mutate.setTint(this.h.d());
                    this.f0.setImageDrawable(mutate);
                }
            }
            if (!z3 || !isEnabled()) {
                this.I = this.J;
            } else {
                this.I = this.K;
            }
            if (this.G == 1) {
                if (!isEnabled()) {
                    this.M = this.y0;
                } else if (z4 && !z3) {
                    this.M = this.A0;
                } else if (z3) {
                    this.M = this.z0;
                } else {
                    this.M = this.x0;
                }
            }
            a();
        }
    }

    public TextInputLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, j.c.a.b.b.textInputStyle);
    }

    public void setEndIconContentDescription(CharSequence charSequence) {
        if (getEndIconContentDescription() != charSequence) {
            this.f0.setContentDescription(charSequence);
        }
    }

    public void setEndIconDrawable(Drawable drawable) {
        this.f0.setImageDrawable(drawable);
    }

    public void setErrorIconDrawable(Drawable drawable) {
        this.p0.setImageDrawable(drawable);
        setErrorIconVisible(drawable != null && this.h.f2280l);
    }

    public void setStartIconContentDescription(CharSequence charSequence) {
        if (getStartIconContentDescription() != charSequence) {
            this.R.setContentDescription(charSequence);
        }
    }

    public void setStartIconDrawable(Drawable drawable) {
        this.R.setImageDrawable(drawable);
        if (drawable != null) {
            setStartIconVisible(true);
            c();
            return;
        }
        setStartIconVisible(false);
        setStartIconOnClickListener(null);
        setStartIconOnLongClickListener(null);
        setStartIconContentDescription((CharSequence) null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.q.TintTypedArray.a(int, boolean):boolean
     arg types: [int, int]
     candidates:
      i.b.q.TintTypedArray.a(int, float):float
      i.b.q.TintTypedArray.a(int, int):int
      i.b.q.TintTypedArray.a(int, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.q.TintTypedArray.a(int, float):float
     arg types: [int, int]
     candidates:
      i.b.q.TintTypedArray.a(int, int):int
      i.b.q.TintTypedArray.a(int, boolean):boolean
      i.b.q.TintTypedArray.a(int, float):float */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.widget.LinearLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.widget.FrameLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public TextInputLayout(android.content.Context r21, android.util.AttributeSet r22, int r23) {
        /*
            r20 = this;
            r0 = r20
            r7 = r22
            r8 = r23
            int r1 = com.google.android.material.textfield.TextInputLayout.I0
            r2 = r21
            android.content.Context r1 = j.c.a.b.m0.a.MaterialThemeOverlay.a(r2, r7, r8, r1)
            r0.<init>(r1, r7, r8)
            j.c.a.b.k0.IndicatorViewController r1 = new j.c.a.b.k0.IndicatorViewController
            r1.<init>(r0)
            r0.h = r1
            android.graphics.Rect r1 = new android.graphics.Rect
            r1.<init>()
            r0.N = r1
            android.graphics.Rect r1 = new android.graphics.Rect
            r1.<init>()
            r0.O = r1
            android.graphics.RectF r1 = new android.graphics.RectF
            r1.<init>()
            r0.P = r1
            java.util.LinkedHashSet r1 = new java.util.LinkedHashSet
            r1.<init>()
            r0.c0 = r1
            r9 = 0
            r0.d0 = r9
            android.util.SparseArray r1 = new android.util.SparseArray
            r1.<init>()
            r0.e0 = r1
            java.util.LinkedHashSet r1 = new java.util.LinkedHashSet
            r1.<init>()
            r0.g0 = r1
            j.c.a.b.b0.CollapsingTextHelper r1 = new j.c.a.b.b0.CollapsingTextHelper
            r1.<init>(r0)
            r0.D0 = r1
            android.content.Context r10 = r20.getContext()
            r11 = 1
            r0.setOrientation(r11)
            r0.setWillNotDraw(r9)
            r0.setAddStatesFromChildren(r11)
            android.widget.FrameLayout r1 = new android.widget.FrameLayout
            r1.<init>(r10)
            r0.b = r1
            r1.setAddStatesFromChildren(r11)
            android.widget.FrameLayout r1 = r0.b
            r0.addView(r1)
            android.widget.LinearLayout r1 = new android.widget.LinearLayout
            r1.<init>(r10)
            r0.c = r1
            r1.setOrientation(r9)
            android.widget.LinearLayout r1 = r0.c
            android.widget.FrameLayout$LayoutParams r2 = new android.widget.FrameLayout$LayoutParams
            r12 = -2
            r13 = -1
            r3 = 8388611(0x800003, float:1.1754948E-38)
            r2.<init>(r12, r13, r3)
            r1.setLayoutParams(r2)
            android.widget.FrameLayout r1 = r0.b
            android.widget.LinearLayout r2 = r0.c
            r1.addView(r2)
            android.widget.LinearLayout r1 = new android.widget.LinearLayout
            r1.<init>(r10)
            r0.d = r1
            r1.setOrientation(r9)
            android.widget.LinearLayout r1 = r0.d
            android.widget.FrameLayout$LayoutParams r2 = new android.widget.FrameLayout$LayoutParams
            r3 = 8388613(0x800005, float:1.175495E-38)
            r2.<init>(r12, r13, r3)
            r1.setLayoutParams(r2)
            android.widget.FrameLayout r1 = r0.b
            android.widget.LinearLayout r2 = r0.d
            r1.addView(r2)
            android.widget.FrameLayout r1 = new android.widget.FrameLayout
            r1.<init>(r10)
            r0.f530e = r1
            android.widget.FrameLayout$LayoutParams r2 = new android.widget.FrameLayout$LayoutParams
            r2.<init>(r12, r13)
            r1.setLayoutParams(r2)
            j.c.a.b.b0.CollapsingTextHelper r1 = r0.D0
            android.animation.TimeInterpolator r2 = j.c.a.b.m.AnimationUtils.a
            r1.J = r2
            r1.e()
            j.c.a.b.b0.CollapsingTextHelper r1 = r0.D0
            android.animation.TimeInterpolator r2 = j.c.a.b.m.AnimationUtils.a
            r1.I = r2
            r1.e()
            j.c.a.b.b0.CollapsingTextHelper r1 = r0.D0
            r2 = 8388659(0x800033, float:1.1755015E-38)
            r1.a(r2)
            int[] r14 = j.c.a.b.l.TextInputLayout
            int r15 = com.google.android.material.textfield.TextInputLayout.I0
            r1 = 5
            int[] r6 = new int[r1]
            int r1 = j.c.a.b.l.TextInputLayout_counterTextAppearance
            r6[r9] = r1
            int r1 = j.c.a.b.l.TextInputLayout_counterOverflowTextAppearance
            r6[r11] = r1
            int r1 = j.c.a.b.l.TextInputLayout_errorTextAppearance
            r5 = 2
            r6[r5] = r1
            int r1 = j.c.a.b.l.TextInputLayout_helperTextTextAppearance
            r4 = 3
            r6[r4] = r1
            r1 = 4
            int r2 = j.c.a.b.l.TextInputLayout_hintTextAppearance
            r6[r1] = r2
            j.c.a.b.b0.ThemeEnforcement.a(r10, r7, r8, r15)
            r1 = r10
            r2 = r22
            r3 = r14
            r12 = 3
            r4 = r23
            r12 = 2
            r5 = r15
            j.c.a.b.b0.ThemeEnforcement.a(r1, r2, r3, r4, r5, r6)
            i.b.q.TintTypedArray r1 = new i.b.q.TintTypedArray
            android.content.res.TypedArray r2 = r10.obtainStyledAttributes(r7, r14, r8, r15)
            r1.<init>(r10, r2)
            int r2 = j.c.a.b.l.TextInputLayout_hintEnabled
            boolean r2 = r1.a(r2, r11)
            r0.z = r2
            int r2 = j.c.a.b.l.TextInputLayout_android_hint
            java.lang.CharSequence r2 = r1.e(r2)
            r0.setHint(r2)
            int r2 = j.c.a.b.l.TextInputLayout_hintAnimationEnabled
            boolean r2 = r1.a(r2, r11)
            r0.E0 = r2
            int r2 = com.google.android.material.textfield.TextInputLayout.I0
            j.c.a.b.g0.ShapeAppearanceModel$b r2 = j.c.a.b.g0.ShapeAppearanceModel.a(r10, r7, r8, r2)
            j.c.a.b.g0.ShapeAppearanceModel r2 = r2.a()
            r0.E = r2
            android.content.res.Resources r2 = r10.getResources()
            int r3 = j.c.a.b.d.mtrl_textinput_box_label_cutout_padding
            int r2 = r2.getDimensionPixelOffset(r3)
            r0.F = r2
            int r2 = j.c.a.b.l.TextInputLayout_boxCollapsedPaddingTop
            int r2 = r1.b(r2, r9)
            r0.H = r2
            int r2 = j.c.a.b.l.TextInputLayout_boxStrokeWidth
            android.content.res.Resources r3 = r10.getResources()
            int r4 = j.c.a.b.d.mtrl_textinput_box_stroke_width_default
            int r3 = r3.getDimensionPixelSize(r4)
            int r2 = r1.c(r2, r3)
            r0.J = r2
            int r2 = j.c.a.b.l.TextInputLayout_boxStrokeWidthFocused
            android.content.res.Resources r3 = r10.getResources()
            int r4 = j.c.a.b.d.mtrl_textinput_box_stroke_width_focused
            int r3 = r3.getDimensionPixelSize(r4)
            int r2 = r1.c(r2, r3)
            r0.K = r2
            int r2 = r0.J
            r0.I = r2
            int r2 = j.c.a.b.l.TextInputLayout_boxCornerRadiusTopStart
            r3 = -1082130432(0xffffffffbf800000, float:-1.0)
            float r2 = r1.a(r2, r3)
            int r4 = j.c.a.b.l.TextInputLayout_boxCornerRadiusTopEnd
            float r4 = r1.a(r4, r3)
            int r5 = j.c.a.b.l.TextInputLayout_boxCornerRadiusBottomEnd
            float r5 = r1.a(r5, r3)
            int r6 = j.c.a.b.l.TextInputLayout_boxCornerRadiusBottomStart
            float r3 = r1.a(r6, r3)
            j.c.a.b.g0.ShapeAppearanceModel r6 = r0.E
            r7 = 0
            if (r6 == 0) goto L_0x061b
            j.c.a.b.g0.ShapeAppearanceModel$b r8 = new j.c.a.b.g0.ShapeAppearanceModel$b
            r8.<init>(r6)
            r6 = 0
            int r14 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
            if (r14 < 0) goto L_0x0193
            r8.c(r2)
        L_0x0193:
            int r2 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r2 < 0) goto L_0x019a
            r8.d(r4)
        L_0x019a:
            int r2 = (r5 > r6 ? 1 : (r5 == r6 ? 0 : -1))
            if (r2 < 0) goto L_0x01a1
            r8.b(r5)
        L_0x01a1:
            int r2 = (r3 > r6 ? 1 : (r3 == r6 ? 0 : -1))
            if (r2 < 0) goto L_0x01a8
            r8.a(r3)
        L_0x01a8:
            j.c.a.b.g0.ShapeAppearanceModel r2 = r8.a()
            r0.E = r2
            int r2 = j.c.a.b.l.TextInputLayout_boxBackgroundColor
            android.content.res.ColorStateList r2 = j.c.a.a.c.n.c.a(r10, r1, r2)
            if (r2 == 0) goto L_0x020a
            int r3 = r2.getDefaultColor()
            r0.x0 = r3
            r0.M = r3
            boolean r3 = r2.isStateful()
            r4 = -16842910(0xfffffffffefeff62, float:-1.6947497E38)
            if (r3 == 0) goto L_0x01e8
            int[] r3 = new int[r11]
            r3[r9] = r4
            int r3 = r2.getColorForState(r3, r13)
            r0.y0 = r3
            int[] r3 = new int[r12]
            r3 = {16842908, 16842910} // fill-array
            int r3 = r2.getColorForState(r3, r13)
            r0.z0 = r3
            int[] r3 = new int[r12]
            r3 = {16843623, 16842910} // fill-array
            int r2 = r2.getColorForState(r3, r13)
            r0.A0 = r2
            goto L_0x0214
        L_0x01e8:
            int r2 = r0.x0
            r0.z0 = r2
            int r2 = j.c.a.b.c.mtrl_filled_background_color
            android.content.res.ColorStateList r2 = i.b.l.a.AppCompatResources.b(r10, r2)
            int[] r3 = new int[r11]
            r3[r9] = r4
            int r3 = r2.getColorForState(r3, r13)
            r0.y0 = r3
            int[] r3 = new int[r11]
            r4 = 16843623(0x1010367, float:2.3696E-38)
            r3[r9] = r4
            int r2 = r2.getColorForState(r3, r13)
            r0.A0 = r2
            goto L_0x0214
        L_0x020a:
            r0.M = r9
            r0.x0 = r9
            r0.y0 = r9
            r0.z0 = r9
            r0.A0 = r9
        L_0x0214:
            int r2 = j.c.a.b.l.TextInputLayout_android_textColorHint
            boolean r2 = r1.f(r2)
            if (r2 == 0) goto L_0x0226
            int r2 = j.c.a.b.l.TextInputLayout_android_textColorHint
            android.content.res.ColorStateList r2 = r1.a(r2)
            r0.s0 = r2
            r0.r0 = r2
        L_0x0226:
            int r2 = j.c.a.b.l.TextInputLayout_boxStrokeColor
            android.content.res.ColorStateList r2 = j.c.a.a.c.n.c.a(r10, r1, r2)
            int r3 = j.c.a.b.l.TextInputLayout_boxStrokeColor
            int r3 = r1.a(r3, r9)
            r0.v0 = r3
            int r3 = j.c.a.b.c.mtrl_textinput_default_box_stroke_color
            int r3 = i.h.e.ContextCompat.a(r10, r3)
            r0.t0 = r3
            int r3 = j.c.a.b.c.mtrl_textinput_disabled_color
            int r3 = i.h.e.ContextCompat.a(r10, r3)
            r0.B0 = r3
            int r3 = j.c.a.b.c.mtrl_textinput_hovered_box_stroke_color
            int r3 = i.h.e.ContextCompat.a(r10, r3)
            r0.u0 = r3
            if (r2 == 0) goto L_0x0251
            r0.setBoxStrokeColorStateList(r2)
        L_0x0251:
            int r2 = j.c.a.b.l.TextInputLayout_boxStrokeErrorColor
            boolean r2 = r1.f(r2)
            if (r2 == 0) goto L_0x0262
            int r2 = j.c.a.b.l.TextInputLayout_boxStrokeErrorColor
            android.content.res.ColorStateList r2 = j.c.a.a.c.n.c.a(r10, r1, r2)
            r0.setBoxStrokeErrorColor(r2)
        L_0x0262:
            int r2 = j.c.a.b.l.TextInputLayout_hintTextAppearance
            int r2 = r1.f(r2, r13)
            if (r2 == r13) goto L_0x0273
            int r2 = j.c.a.b.l.TextInputLayout_hintTextAppearance
            int r2 = r1.f(r2, r9)
            r0.setHintTextAppearance(r2)
        L_0x0273:
            int r2 = j.c.a.b.l.TextInputLayout_errorTextAppearance
            int r2 = r1.f(r2, r9)
            int r3 = j.c.a.b.l.TextInputLayout_errorContentDescription
            java.lang.CharSequence r3 = r1.e(r3)
            int r4 = j.c.a.b.l.TextInputLayout_errorEnabled
            boolean r4 = r1.a(r4, r9)
            android.content.Context r5 = r20.getContext()
            android.view.LayoutInflater r5 = android.view.LayoutInflater.from(r5)
            int r6 = j.c.a.b.h.design_text_input_end_icon
            android.widget.LinearLayout r8 = r0.d
            android.view.View r5 = r5.inflate(r6, r8, r9)
            com.google.android.material.internal.CheckableImageButton r5 = (com.google.android.material.internal.CheckableImageButton) r5
            r0.p0 = r5
            r6 = 8
            r5.setVisibility(r6)
            int r5 = j.c.a.b.l.TextInputLayout_errorIconDrawable
            boolean r5 = r1.f(r5)
            if (r5 == 0) goto L_0x02af
            int r5 = j.c.a.b.l.TextInputLayout_errorIconDrawable
            android.graphics.drawable.Drawable r5 = r1.b(r5)
            r0.setErrorIconDrawable(r5)
        L_0x02af:
            int r5 = j.c.a.b.l.TextInputLayout_errorIconTint
            boolean r5 = r1.f(r5)
            if (r5 == 0) goto L_0x02c0
            int r5 = j.c.a.b.l.TextInputLayout_errorIconTint
            android.content.res.ColorStateList r5 = j.c.a.a.c.n.c.a(r10, r1, r5)
            r0.setErrorIconTintList(r5)
        L_0x02c0:
            int r5 = j.c.a.b.l.TextInputLayout_errorIconTintMode
            boolean r5 = r1.f(r5)
            if (r5 == 0) goto L_0x02d5
            int r5 = j.c.a.b.l.TextInputLayout_errorIconTintMode
            int r5 = r1.d(r5, r13)
            android.graphics.PorterDuff$Mode r5 = j.c.a.a.c.n.c.a(r5, r7)
            r0.setErrorIconTintMode(r5)
        L_0x02d5:
            com.google.android.material.internal.CheckableImageButton r5 = r0.p0
            android.content.res.Resources r8 = r20.getResources()
            int r14 = j.c.a.b.j.error_icon_content_description
            java.lang.CharSequence r8 = r8.getText(r14)
            r5.setContentDescription(r8)
            com.google.android.material.internal.CheckableImageButton r5 = r0.p0
            i.h.l.ViewCompat.h(r5, r12)
            com.google.android.material.internal.CheckableImageButton r5 = r0.p0
            r5.setClickable(r9)
            com.google.android.material.internal.CheckableImageButton r5 = r0.p0
            r5.setPressable(r9)
            com.google.android.material.internal.CheckableImageButton r5 = r0.p0
            r5.setFocusable(r9)
            int r5 = j.c.a.b.l.TextInputLayout_helperTextTextAppearance
            int r5 = r1.f(r5, r9)
            int r8 = j.c.a.b.l.TextInputLayout_helperTextEnabled
            boolean r8 = r1.a(r8, r9)
            int r14 = j.c.a.b.l.TextInputLayout_helperText
            java.lang.CharSequence r14 = r1.e(r14)
            int r15 = j.c.a.b.l.TextInputLayout_placeholderTextAppearance
            int r15 = r1.f(r15, r9)
            int r12 = j.c.a.b.l.TextInputLayout_placeholderText
            java.lang.CharSequence r12 = r1.e(r12)
            int r11 = j.c.a.b.l.TextInputLayout_prefixTextAppearance
            int r11 = r1.f(r11, r9)
            int r7 = j.c.a.b.l.TextInputLayout_prefixText
            java.lang.CharSequence r7 = r1.e(r7)
            int r6 = j.c.a.b.l.TextInputLayout_suffixTextAppearance
            int r6 = r1.f(r6, r9)
            int r13 = j.c.a.b.l.TextInputLayout_suffixText
            java.lang.CharSequence r13 = r1.e(r13)
            r16 = r6
            int r6 = j.c.a.b.l.TextInputLayout_counterEnabled
            boolean r6 = r1.a(r6, r9)
            int r9 = j.c.a.b.l.TextInputLayout_counterMaxLength
            r17 = r6
            r6 = -1
            int r9 = r1.d(r9, r6)
            r0.setCounterMaxLength(r9)
            int r6 = j.c.a.b.l.TextInputLayout_counterTextAppearance
            r9 = 0
            int r6 = r1.f(r6, r9)
            r0.f537n = r6
            int r6 = j.c.a.b.l.TextInputLayout_counterOverflowTextAppearance
            int r6 = r1.f(r6, r9)
            r0.f536m = r6
            android.content.Context r6 = r20.getContext()
            android.view.LayoutInflater r6 = android.view.LayoutInflater.from(r6)
            r18 = r13
            int r13 = j.c.a.b.h.design_text_input_start_icon
            r19 = r11
            android.widget.LinearLayout r11 = r0.c
            android.view.View r6 = r6.inflate(r13, r11, r9)
            com.google.android.material.internal.CheckableImageButton r6 = (com.google.android.material.internal.CheckableImageButton) r6
            r0.R = r6
            r9 = 8
            r6.setVisibility(r9)
            r6 = 0
            r0.setStartIconOnClickListener(r6)
            r0.setStartIconOnLongClickListener(r6)
            int r6 = j.c.a.b.l.TextInputLayout_startIconDrawable
            boolean r6 = r1.f(r6)
            if (r6 == 0) goto L_0x03a3
            int r6 = j.c.a.b.l.TextInputLayout_startIconDrawable
            android.graphics.drawable.Drawable r6 = r1.b(r6)
            r0.setStartIconDrawable(r6)
            int r6 = j.c.a.b.l.TextInputLayout_startIconContentDescription
            boolean r6 = r1.f(r6)
            if (r6 == 0) goto L_0x0399
            int r6 = j.c.a.b.l.TextInputLayout_startIconContentDescription
            java.lang.CharSequence r6 = r1.e(r6)
            r0.setStartIconContentDescription(r6)
        L_0x0399:
            int r6 = j.c.a.b.l.TextInputLayout_startIconCheckable
            r9 = 1
            boolean r6 = r1.a(r6, r9)
            r0.setStartIconCheckable(r6)
        L_0x03a3:
            int r6 = j.c.a.b.l.TextInputLayout_startIconTint
            boolean r6 = r1.f(r6)
            if (r6 == 0) goto L_0x03b4
            int r6 = j.c.a.b.l.TextInputLayout_startIconTint
            android.content.res.ColorStateList r6 = j.c.a.a.c.n.c.a(r10, r1, r6)
            r0.setStartIconTintList(r6)
        L_0x03b4:
            int r6 = j.c.a.b.l.TextInputLayout_startIconTintMode
            boolean r6 = r1.f(r6)
            if (r6 == 0) goto L_0x03cb
            int r6 = j.c.a.b.l.TextInputLayout_startIconTintMode
            r9 = -1
            int r6 = r1.d(r6, r9)
            r9 = 0
            android.graphics.PorterDuff$Mode r6 = j.c.a.a.c.n.c.a(r6, r9)
            r0.setStartIconTintMode(r6)
        L_0x03cb:
            int r6 = j.c.a.b.l.TextInputLayout_boxBackgroundMode
            r9 = 0
            int r6 = r1.d(r6, r9)
            r0.setBoxBackgroundMode(r6)
            android.content.Context r6 = r20.getContext()
            android.view.LayoutInflater r6 = android.view.LayoutInflater.from(r6)
            int r11 = j.c.a.b.h.design_text_input_end_icon
            android.widget.FrameLayout r13 = r0.f530e
            android.view.View r6 = r6.inflate(r11, r13, r9)
            com.google.android.material.internal.CheckableImageButton r6 = (com.google.android.material.internal.CheckableImageButton) r6
            r0.f0 = r6
            android.widget.FrameLayout r9 = r0.f530e
            r9.addView(r6)
            com.google.android.material.internal.CheckableImageButton r6 = r0.f0
            r9 = 8
            r6.setVisibility(r9)
            android.util.SparseArray<j.c.a.b.k0.n> r6 = r0.e0
            j.c.a.b.k0.CustomEndIconDelegate r9 = new j.c.a.b.k0.CustomEndIconDelegate
            r9.<init>(r0)
            r11 = -1
            r6.append(r11, r9)
            android.util.SparseArray<j.c.a.b.k0.n> r6 = r0.e0
            j.c.a.b.k0.NoEndIconDelegate r9 = new j.c.a.b.k0.NoEndIconDelegate
            r9.<init>(r0)
            r11 = 0
            r6.append(r11, r9)
            android.util.SparseArray<j.c.a.b.k0.n> r6 = r0.e0
            j.c.a.b.k0.PasswordToggleEndIconDelegate r9 = new j.c.a.b.k0.PasswordToggleEndIconDelegate
            r9.<init>(r0)
            r11 = 1
            r6.append(r11, r9)
            android.util.SparseArray<j.c.a.b.k0.n> r6 = r0.e0
            j.c.a.b.k0.ClearTextEndIconDelegate r9 = new j.c.a.b.k0.ClearTextEndIconDelegate
            r9.<init>(r0)
            r11 = 2
            r6.append(r11, r9)
            android.util.SparseArray<j.c.a.b.k0.n> r6 = r0.e0
            j.c.a.b.k0.DropdownMenuEndIconDelegate r9 = new j.c.a.b.k0.DropdownMenuEndIconDelegate
            r9.<init>(r0)
            r11 = 3
            r6.append(r11, r9)
            int r6 = j.c.a.b.l.TextInputLayout_endIconMode
            boolean r6 = r1.f(r6)
            if (r6 == 0) goto L_0x046b
            int r6 = j.c.a.b.l.TextInputLayout_endIconMode
            r9 = 0
            int r6 = r1.d(r6, r9)
            r0.setEndIconMode(r6)
            int r6 = j.c.a.b.l.TextInputLayout_endIconDrawable
            boolean r6 = r1.f(r6)
            if (r6 == 0) goto L_0x044f
            int r6 = j.c.a.b.l.TextInputLayout_endIconDrawable
            android.graphics.drawable.Drawable r6 = r1.b(r6)
            r0.setEndIconDrawable(r6)
        L_0x044f:
            int r6 = j.c.a.b.l.TextInputLayout_endIconContentDescription
            boolean r6 = r1.f(r6)
            if (r6 == 0) goto L_0x0460
            int r6 = j.c.a.b.l.TextInputLayout_endIconContentDescription
            java.lang.CharSequence r6 = r1.e(r6)
            r0.setEndIconContentDescription(r6)
        L_0x0460:
            int r6 = j.c.a.b.l.TextInputLayout_endIconCheckable
            r9 = 1
            boolean r6 = r1.a(r6, r9)
            r0.setEndIconCheckable(r6)
            goto L_0x04b7
        L_0x046b:
            int r6 = j.c.a.b.l.TextInputLayout_passwordToggleEnabled
            boolean r6 = r1.f(r6)
            if (r6 == 0) goto L_0x04b7
            int r6 = j.c.a.b.l.TextInputLayout_passwordToggleEnabled
            r9 = 0
            boolean r6 = r1.a(r6, r9)
            r0.setEndIconMode(r6)
            int r6 = j.c.a.b.l.TextInputLayout_passwordToggleDrawable
            android.graphics.drawable.Drawable r6 = r1.b(r6)
            r0.setEndIconDrawable(r6)
            int r6 = j.c.a.b.l.TextInputLayout_passwordToggleContentDescription
            java.lang.CharSequence r6 = r1.e(r6)
            r0.setEndIconContentDescription(r6)
            int r6 = j.c.a.b.l.TextInputLayout_passwordToggleTint
            boolean r6 = r1.f(r6)
            if (r6 == 0) goto L_0x04a0
            int r6 = j.c.a.b.l.TextInputLayout_passwordToggleTint
            android.content.res.ColorStateList r6 = j.c.a.a.c.n.c.a(r10, r1, r6)
            r0.setEndIconTintList(r6)
        L_0x04a0:
            int r6 = j.c.a.b.l.TextInputLayout_passwordToggleTintMode
            boolean r6 = r1.f(r6)
            if (r6 == 0) goto L_0x04b7
            int r6 = j.c.a.b.l.TextInputLayout_passwordToggleTintMode
            r9 = -1
            int r6 = r1.d(r6, r9)
            r9 = 0
            android.graphics.PorterDuff$Mode r6 = j.c.a.a.c.n.c.a(r6, r9)
            r0.setEndIconTintMode(r6)
        L_0x04b7:
            int r6 = j.c.a.b.l.TextInputLayout_passwordToggleEnabled
            boolean r6 = r1.f(r6)
            if (r6 != 0) goto L_0x04e7
            int r6 = j.c.a.b.l.TextInputLayout_endIconTint
            boolean r6 = r1.f(r6)
            if (r6 == 0) goto L_0x04d0
            int r6 = j.c.a.b.l.TextInputLayout_endIconTint
            android.content.res.ColorStateList r6 = j.c.a.a.c.n.c.a(r10, r1, r6)
            r0.setEndIconTintList(r6)
        L_0x04d0:
            int r6 = j.c.a.b.l.TextInputLayout_endIconTintMode
            boolean r6 = r1.f(r6)
            if (r6 == 0) goto L_0x04e7
            int r6 = j.c.a.b.l.TextInputLayout_endIconTintMode
            r9 = -1
            int r6 = r1.d(r6, r9)
            r9 = 0
            android.graphics.PorterDuff$Mode r6 = j.c.a.a.c.n.c.a(r6, r9)
            r0.setEndIconTintMode(r6)
        L_0x04e7:
            i.b.q.AppCompatTextView r6 = new i.b.q.AppCompatTextView
            r6.<init>(r10)
            r0.w = r6
            int r9 = j.c.a.b.f.textinput_prefix_text
            r6.setId(r9)
            android.widget.TextView r6 = r0.w
            android.widget.FrameLayout$LayoutParams r9 = new android.widget.FrameLayout$LayoutParams
            r11 = -2
            r9.<init>(r11, r11)
            r6.setLayoutParams(r9)
            android.widget.TextView r6 = r0.w
            r9 = 1
            r6.setAccessibilityLiveRegion(r9)
            android.widget.LinearLayout r6 = r0.c
            com.google.android.material.internal.CheckableImageButton r9 = r0.R
            r6.addView(r9)
            android.widget.LinearLayout r6 = r0.c
            android.widget.TextView r9 = r0.w
            r6.addView(r9)
            i.b.q.AppCompatTextView r6 = new i.b.q.AppCompatTextView
            r6.<init>(r10)
            r0.y = r6
            int r9 = j.c.a.b.f.textinput_suffix_text
            r6.setId(r9)
            android.widget.TextView r6 = r0.y
            android.widget.FrameLayout$LayoutParams r9 = new android.widget.FrameLayout$LayoutParams
            r10 = 80
            r11 = -2
            r9.<init>(r11, r11, r10)
            r6.setLayoutParams(r9)
            android.widget.TextView r6 = r0.y
            r9 = 1
            r6.setAccessibilityLiveRegion(r9)
            android.widget.LinearLayout r6 = r0.d
            android.widget.TextView r9 = r0.y
            r6.addView(r9)
            android.widget.LinearLayout r6 = r0.d
            com.google.android.material.internal.CheckableImageButton r9 = r0.p0
            r6.addView(r9)
            android.widget.LinearLayout r6 = r0.d
            android.widget.FrameLayout r9 = r0.f530e
            r6.addView(r9)
            r0.setHelperTextEnabled(r8)
            r0.setHelperText(r14)
            r0.setHelperTextTextAppearance(r5)
            r0.setErrorEnabled(r4)
            r0.setErrorTextAppearance(r2)
            r0.setErrorContentDescription(r3)
            int r2 = r0.f537n
            r0.setCounterTextAppearance(r2)
            int r2 = r0.f536m
            r0.setCounterOverflowTextAppearance(r2)
            r0.setPlaceholderText(r12)
            r0.setPlaceholderTextAppearance(r15)
            r0.setPrefixText(r7)
            r2 = r19
            r0.setPrefixTextAppearance(r2)
            r2 = r18
            r0.setSuffixText(r2)
            r2 = r16
            r0.setSuffixTextAppearance(r2)
            int r2 = j.c.a.b.l.TextInputLayout_errorTextColor
            boolean r2 = r1.f(r2)
            if (r2 == 0) goto L_0x058b
            int r2 = j.c.a.b.l.TextInputLayout_errorTextColor
            android.content.res.ColorStateList r2 = r1.a(r2)
            r0.setErrorTextColor(r2)
        L_0x058b:
            int r2 = j.c.a.b.l.TextInputLayout_helperTextTextColor
            boolean r2 = r1.f(r2)
            if (r2 == 0) goto L_0x059c
            int r2 = j.c.a.b.l.TextInputLayout_helperTextTextColor
            android.content.res.ColorStateList r2 = r1.a(r2)
            r0.setHelperTextColor(r2)
        L_0x059c:
            int r2 = j.c.a.b.l.TextInputLayout_hintTextColor
            boolean r2 = r1.f(r2)
            if (r2 == 0) goto L_0x05ad
            int r2 = j.c.a.b.l.TextInputLayout_hintTextColor
            android.content.res.ColorStateList r2 = r1.a(r2)
            r0.setHintTextColor(r2)
        L_0x05ad:
            int r2 = j.c.a.b.l.TextInputLayout_counterTextColor
            boolean r2 = r1.f(r2)
            if (r2 == 0) goto L_0x05be
            int r2 = j.c.a.b.l.TextInputLayout_counterTextColor
            android.content.res.ColorStateList r2 = r1.a(r2)
            r0.setCounterTextColor(r2)
        L_0x05be:
            int r2 = j.c.a.b.l.TextInputLayout_counterOverflowTextColor
            boolean r2 = r1.f(r2)
            if (r2 == 0) goto L_0x05cf
            int r2 = j.c.a.b.l.TextInputLayout_counterOverflowTextColor
            android.content.res.ColorStateList r2 = r1.a(r2)
            r0.setCounterOverflowTextColor(r2)
        L_0x05cf:
            int r2 = j.c.a.b.l.TextInputLayout_placeholderTextColor
            boolean r2 = r1.f(r2)
            if (r2 == 0) goto L_0x05e0
            int r2 = j.c.a.b.l.TextInputLayout_placeholderTextColor
            android.content.res.ColorStateList r2 = r1.a(r2)
            r0.setPlaceholderTextColor(r2)
        L_0x05e0:
            int r2 = j.c.a.b.l.TextInputLayout_prefixTextColor
            boolean r2 = r1.f(r2)
            if (r2 == 0) goto L_0x05f1
            int r2 = j.c.a.b.l.TextInputLayout_prefixTextColor
            android.content.res.ColorStateList r2 = r1.a(r2)
            r0.setPrefixTextColor(r2)
        L_0x05f1:
            int r2 = j.c.a.b.l.TextInputLayout_suffixTextColor
            boolean r2 = r1.f(r2)
            if (r2 == 0) goto L_0x0602
            int r2 = j.c.a.b.l.TextInputLayout_suffixTextColor
            android.content.res.ColorStateList r2 = r1.a(r2)
            r0.setSuffixTextColor(r2)
        L_0x0602:
            r2 = r17
            r0.setCounterEnabled(r2)
            int r2 = j.c.a.b.l.TextInputLayout_android_enabled
            r3 = 1
            boolean r2 = r1.a(r2, r3)
            r0.setEnabled(r2)
            android.content.res.TypedArray r1 = r1.b
            r1.recycle()
            r1 = 2
            r0.setImportantForAccessibility(r1)
            return
        L_0x061b:
            r1 = r7
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.textfield.TextInputLayout.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    @Deprecated
    public void setPasswordVisibilityToggleContentDescription(CharSequence charSequence) {
        this.f0.setContentDescription(charSequence);
    }

    @Deprecated
    public void setPasswordVisibilityToggleDrawable(Drawable drawable) {
        this.f0.setImageDrawable(drawable);
    }

    public final int b(int i2, boolean z2) {
        int compoundPaddingRight = i2 - this.f531f.getCompoundPaddingRight();
        if (this.v == null || !z2) {
            return compoundPaddingRight;
        }
        return this.w.getPaddingRight() + this.w.getMeasuredWidth() + compoundPaddingRight;
    }

    public final void b() {
        a(this.f0, this.i0, this.h0, this.k0, this.j0);
    }

    public final void b(boolean z2, boolean z3) {
        int defaultColor = this.w0.getDefaultColor();
        int colorForState = this.w0.getColorForState(new int[]{16843623, 16842910}, defaultColor);
        int colorForState2 = this.w0.getColorForState(new int[]{16843518, 16842910}, defaultColor);
        if (z2) {
            this.L = colorForState2;
        } else if (z3) {
            this.L = colorForState;
        } else {
            this.L = defaultColor;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.material.textfield.TextInputLayout.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.google.android.material.textfield.TextInputLayout.a(android.view.ViewGroup, boolean):void
      com.google.android.material.textfield.TextInputLayout.a(com.google.android.material.internal.CheckableImageButton, android.view.View$OnLongClickListener):void
      com.google.android.material.textfield.TextInputLayout.a(int, boolean):int
      com.google.android.material.textfield.TextInputLayout.a(android.widget.TextView, int):void
      com.google.android.material.textfield.TextInputLayout.a(com.google.android.material.internal.CheckableImageButton, android.content.res.ColorStateList):void
      com.google.android.material.textfield.TextInputLayout.a(boolean, boolean):void */
    public void a(int i2) {
        boolean z2 = this.f534k;
        int i3 = this.f533j;
        if (i3 == -1) {
            this.f535l.setText(String.valueOf(i2));
            this.f535l.setContentDescription(null);
            this.f534k = false;
        } else {
            this.f534k = i2 > i3;
            Context context = getContext();
            this.f535l.setContentDescription(context.getString(this.f534k ? j.character_counter_overflowed_content_description : j.character_counter_content_description, Integer.valueOf(i2), Integer.valueOf(this.f533j)));
            if (z2 != this.f534k) {
                l();
            }
            this.f535l.setText(getContext().getString(j.character_counter_pattern, Integer.valueOf(i2), Integer.valueOf(this.f533j)));
        }
        if (this.f531f != null && z2 != this.f534k) {
            a(false, false);
            t();
            n();
        }
    }

    public static void a(ViewGroup viewGroup, boolean z2) {
        int childCount = viewGroup.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = viewGroup.getChildAt(i2);
            childAt.setEnabled(z2);
            if (childAt instanceof ViewGroup) {
                a((ViewGroup) childAt, z2);
            }
        }
    }

    public void a(TextView textView, int i2) {
        boolean z2 = true;
        try {
            ResourcesFlusher.d(textView, i2);
            if (Build.VERSION.SDK_INT < 23 || textView.getTextColors().getDefaultColor() != -65281) {
                z2 = false;
            }
        } catch (Exception unused) {
        }
        if (z2) {
            ResourcesFlusher.d(textView, k.TextAppearance_AppCompat_Caption);
            textView.setTextColor(ContextCompat.a(getContext(), j.c.a.b.c.design_error));
        }
    }

    public final int a(int i2, boolean z2) {
        int compoundPaddingLeft = this.f531f.getCompoundPaddingLeft() + i2;
        return (this.v == null || z2) ? compoundPaddingLeft : (compoundPaddingLeft - this.w.getMeasuredWidth()) + this.w.getPaddingLeft();
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0024  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0062  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a() {
        /*
            r6 = this;
            j.c.a.b.g0.MaterialShapeDrawable r0 = r6.C
            if (r0 != 0) goto L_0x0005
            return
        L_0x0005:
            j.c.a.b.g0.ShapeAppearanceModel r1 = r6.E
            r0.setShapeAppearanceModel(r1)
            int r0 = r6.G
            r1 = 2
            r2 = -1
            r3 = 0
            r4 = 1
            if (r0 != r1) goto L_0x0021
            int r0 = r6.I
            if (r0 <= r2) goto L_0x001c
            int r0 = r6.L
            if (r0 == 0) goto L_0x001c
            r0 = 1
            goto L_0x001d
        L_0x001c:
            r0 = 0
        L_0x001d:
            if (r0 == 0) goto L_0x0021
            r0 = 1
            goto L_0x0022
        L_0x0021:
            r0 = 0
        L_0x0022:
            if (r0 == 0) goto L_0x002e
            j.c.a.b.g0.MaterialShapeDrawable r0 = r6.C
            int r1 = r6.I
            float r1 = (float) r1
            int r5 = r6.L
            r0.a(r1, r5)
        L_0x002e:
            int r0 = r6.M
            int r1 = r6.G
            if (r1 != r4) goto L_0x0044
            int r0 = j.c.a.b.b.colorSurface
            android.content.Context r1 = r6.getContext()
            int r0 = j.c.a.a.c.n.c.a(r1, r0, r3)
            int r1 = r6.M
            int r0 = i.h.f.ColorUtils.a(r1, r0)
        L_0x0044:
            r6.M = r0
            j.c.a.b.g0.MaterialShapeDrawable r1 = r6.C
            android.content.res.ColorStateList r0 = android.content.res.ColorStateList.valueOf(r0)
            r1.a(r0)
            int r0 = r6.d0
            r1 = 3
            if (r0 != r1) goto L_0x005d
            android.widget.EditText r0 = r6.f531f
            android.graphics.drawable.Drawable r0 = r0.getBackground()
            r0.invalidateSelf()
        L_0x005d:
            j.c.a.b.g0.MaterialShapeDrawable r0 = r6.D
            if (r0 != 0) goto L_0x0062
            goto L_0x007b
        L_0x0062:
            int r0 = r6.I
            if (r0 <= r2) goto L_0x006b
            int r0 = r6.L
            if (r0 == 0) goto L_0x006b
            r3 = 1
        L_0x006b:
            if (r3 == 0) goto L_0x0078
            j.c.a.b.g0.MaterialShapeDrawable r0 = r6.D
            int r1 = r6.L
            android.content.res.ColorStateList r1 = android.content.res.ColorStateList.valueOf(r1)
            r0.a(r1)
        L_0x0078:
            r6.invalidate()
        L_0x007b:
            r6.invalidate()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.textfield.TextInputLayout.a():void");
    }

    public void a(f fVar) {
        this.c0.add(fVar);
        if (this.f531f != null) {
            fVar.a(this);
        }
    }

    public final void a(CheckableImageButton checkableImageButton, boolean z2, ColorStateList colorStateList, boolean z3, PorterDuff.Mode mode) {
        Drawable drawable = checkableImageButton.getDrawable();
        if (drawable != null && (z2 || z3)) {
            drawable = ResourcesFlusher.d(drawable).mutate();
            if (z2) {
                drawable.setTintList(colorStateList);
            }
            if (z3) {
                drawable.setTintMode(mode);
            }
        }
        if (checkableImageButton.getDrawable() != drawable) {
            checkableImageButton.setImageDrawable(drawable);
        }
    }

    public static void a(CheckableImageButton checkableImageButton, View.OnLongClickListener onLongClickListener) {
        boolean s2 = ViewCompat.s(checkableImageButton);
        boolean z2 = false;
        int i2 = 1;
        boolean z3 = onLongClickListener != null;
        if (s2 || z3) {
            z2 = true;
        }
        checkableImageButton.setFocusable(z2);
        checkableImageButton.setClickable(s2);
        checkableImageButton.setPressable(s2);
        checkableImageButton.setLongClickable(z3);
        if (!z2) {
            i2 = 2;
        }
        checkableImageButton.setImportantForAccessibility(i2);
    }

    public final void a(CheckableImageButton checkableImageButton, ColorStateList colorStateList) {
        Drawable drawable = checkableImageButton.getDrawable();
        if (checkableImageButton.getDrawable() != null && colorStateList != null && colorStateList.isStateful()) {
            int colorForState = colorStateList.getColorForState(getDrawableState(), colorStateList.getDefaultColor());
            Drawable mutate = ResourcesFlusher.d(drawable).mutate();
            mutate.setTintList(ColorStateList.valueOf(colorForState));
            checkableImageButton.setImageDrawable(mutate);
        }
    }

    public void a(float f2) {
        if (this.D0.c != f2) {
            if (this.F0 == null) {
                ValueAnimator valueAnimator = new ValueAnimator();
                this.F0 = valueAnimator;
                valueAnimator.setInterpolator(AnimationUtils.b);
                this.F0.setDuration(167L);
                this.F0.addUpdateListener(new d());
            }
            this.F0.setFloatValues(this.D0.c, f2);
            this.F0.start();
        }
    }
}
