package com.google.android.material.theme;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import androidx.appcompat.app.AppCompatViewInflater;
import androidx.appcompat.widget.AppCompatCheckBox;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textview.MaterialTextView;
import i.b.q.AppCompatAutoCompleteTextView;
import i.b.q.AppCompatButton;
import i.b.q.AppCompatRadioButton;
import i.b.q.AppCompatTextView;
import j.c.a.b.c0.MaterialRadioButton;
import j.c.a.b.l0.MaterialAutoCompleteTextView0;
import j.c.a.b.u.MaterialCheckBox;
import l.a.a.a.o.b.AbstractSpiCall;

public class MaterialComponentsViewInflater extends AppCompatViewInflater {

    /* renamed from: f  reason: collision with root package name */
    public static int f545f = -1;

    public AppCompatAutoCompleteTextView a(Context context, AttributeSet attributeSet) {
        return new MaterialAutoCompleteTextView0(context, attributeSet);
    }

    public AppCompatButton b(Context context, AttributeSet attributeSet) {
        int i2 = Build.VERSION.SDK_INT;
        boolean z = false;
        if (i2 == 23 || i2 == 24 || i2 == 25) {
            if (f545f == -1) {
                f545f = context.getResources().getIdentifier("floatingToolbarItemBackgroundDrawable", "^attr-private", AbstractSpiCall.ANDROID_CLIENT_TYPE);
            }
            int i3 = f545f;
            if (i3 != 0 && i3 != -1) {
                int i4 = 0;
                while (true) {
                    if (i4 < attributeSet.getAttributeCount()) {
                        if (attributeSet.getAttributeNameResource(i4) == 16842964 && f545f == attributeSet.getAttributeListValue(i4, null, 0)) {
                            z = true;
                            break;
                        }
                        i4++;
                    } else {
                        break;
                    }
                }
            }
        }
        if (z) {
            return new AppCompatButton(context, attributeSet);
        }
        return new MaterialButton(context, attributeSet);
    }

    public AppCompatCheckBox c(Context context, AttributeSet attributeSet) {
        return new MaterialCheckBox(context, attributeSet);
    }

    public AppCompatRadioButton d(Context context, AttributeSet attributeSet) {
        return new MaterialRadioButton(context, attributeSet);
    }

    public AppCompatTextView e(Context context, AttributeSet attributeSet) {
        return new MaterialTextView(context, attributeSet);
    }
}
