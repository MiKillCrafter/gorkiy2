package com.google.android.material.card;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.Checkable;
import android.widget.FrameLayout;
import androidx.cardview.widget.CardView;
import i.b.l.a.AppCompatResources;
import i.h.l.ViewCompat;
import j.c.a.a.c.n.c;
import j.c.a.b.b;
import j.c.a.b.b0.ThemeEnforcement;
import j.c.a.b.g0.MaterialShapeDrawable;
import j.c.a.b.g0.ShapeAppearanceModel;
import j.c.a.b.g0.Shapeable;
import j.c.a.b.k;
import j.c.a.b.l;
import j.c.a.b.m0.a.MaterialThemeOverlay;
import j.c.a.b.t.MaterialCardViewHelper;

public class MaterialCardView extends CardView implements Checkable, Shapeable {

    /* renamed from: p  reason: collision with root package name */
    public static final int[] f439p = {16842911};

    /* renamed from: q  reason: collision with root package name */
    public static final int[] f440q = {16842912};

    /* renamed from: r  reason: collision with root package name */
    public static final int[] f441r = {b.state_dragged};

    /* renamed from: s  reason: collision with root package name */
    public static final int f442s = k.Widget_MaterialComponents_CardView;

    /* renamed from: k  reason: collision with root package name */
    public final MaterialCardViewHelper f443k;

    /* renamed from: l  reason: collision with root package name */
    public boolean f444l;

    /* renamed from: m  reason: collision with root package name */
    public boolean f445m;

    /* renamed from: n  reason: collision with root package name */
    public boolean f446n;

    /* renamed from: o  reason: collision with root package name */
    public a f447o;

    public interface a {
        void a(MaterialCardView materialCardView, boolean z);
    }

    public MaterialCardView(Context context) {
        this(context, null);
    }

    public final void c() {
        MaterialCardViewHelper materialCardViewHelper;
        Drawable drawable;
        if (Build.VERSION.SDK_INT > 26 && (drawable = (materialCardViewHelper = this.f443k).f2348n) != null) {
            Rect bounds = drawable.getBounds();
            int i2 = bounds.bottom;
            materialCardViewHelper.f2348n.setBounds(bounds.left, bounds.top, bounds.right, i2 - 1);
            materialCardViewHelper.f2348n.setBounds(bounds.left, bounds.top, bounds.right, i2);
        }
    }

    public boolean d() {
        MaterialCardViewHelper materialCardViewHelper = this.f443k;
        return materialCardViewHelper != null && materialCardViewHelper.f2353s;
    }

    public ColorStateList getCardBackgroundColor() {
        return this.f443k.c.b.d;
    }

    public ColorStateList getCardForegroundColor() {
        return this.f443k.d.b.d;
    }

    public float getCardViewRadius() {
        return super.getRadius();
    }

    public Drawable getCheckedIcon() {
        return this.f443k.f2343i;
    }

    public ColorStateList getCheckedIconTint() {
        return this.f443k.f2345k;
    }

    public int getContentPaddingBottom() {
        return this.f443k.b.bottom;
    }

    public int getContentPaddingLeft() {
        return this.f443k.b.left;
    }

    public int getContentPaddingRight() {
        return this.f443k.b.right;
    }

    public int getContentPaddingTop() {
        return this.f443k.b.top;
    }

    public float getProgress() {
        return this.f443k.c.b.f2232k;
    }

    public float getRadius() {
        return this.f443k.c.f();
    }

    public ColorStateList getRippleColor() {
        return this.f443k.f2344j;
    }

    public ShapeAppearanceModel getShapeAppearanceModel() {
        return this.f443k.f2346l;
    }

    @Deprecated
    public int getStrokeColor() {
        ColorStateList colorStateList = this.f443k.f2347m;
        if (colorStateList == null) {
            return -1;
        }
        return colorStateList.getDefaultColor();
    }

    public ColorStateList getStrokeColorStateList() {
        return this.f443k.f2347m;
    }

    public int getStrokeWidth() {
        return this.f443k.g;
    }

    public boolean isChecked() {
        return this.f445m;
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        c.a(this, this.f443k.c);
    }

    public int[] onCreateDrawableState(int i2) {
        int[] onCreateDrawableState = super.onCreateDrawableState(i2 + 3);
        if (d()) {
            FrameLayout.mergeDrawableStates(onCreateDrawableState, f439p);
        }
        if (this.f445m) {
            FrameLayout.mergeDrawableStates(onCreateDrawableState, f440q);
        }
        if (this.f446n) {
            FrameLayout.mergeDrawableStates(onCreateDrawableState, f441r);
        }
        return onCreateDrawableState;
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setClassName("androidx.cardview.widget.CardView");
        accessibilityEvent.setChecked(this.f445m);
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        accessibilityNodeInfo.setClassName("androidx.cardview.widget.CardView");
        accessibilityNodeInfo.setCheckable(d());
        accessibilityNodeInfo.setClickable(isClickable());
        accessibilityNodeInfo.setChecked(this.f445m);
    }

    public void onMeasure(int i2, int i3) {
        int i4;
        int i5;
        super.onMeasure(i2, i3);
        MaterialCardViewHelper materialCardViewHelper = this.f443k;
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        if (materialCardViewHelper.f2349o != null) {
            int i6 = materialCardViewHelper.f2341e;
            int i7 = materialCardViewHelper.f2342f;
            int i8 = (measuredWidth - i6) - i7;
            int i9 = (measuredHeight - i6) - i7;
            if (ViewCompat.k(materialCardViewHelper.a) == 1) {
                i4 = i8;
                i5 = i6;
            } else {
                i5 = i8;
                i4 = i6;
            }
            materialCardViewHelper.f2349o.setLayerInset(2, i5, materialCardViewHelper.f2341e, i4, i9);
        }
    }

    public void setBackground(Drawable drawable) {
        setBackgroundDrawable(drawable);
    }

    public void setBackgroundDrawable(Drawable drawable) {
        if (this.f444l) {
            if (!this.f443k.f2352r) {
                Log.i("MaterialCardView", "Setting a custom background is not supported.");
                this.f443k.f2352r = true;
            }
            super.setBackgroundDrawable(drawable);
        }
    }

    public void setBackgroundInternal(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
    }

    public void setCardBackgroundColor(int i2) {
        MaterialCardViewHelper materialCardViewHelper = this.f443k;
        materialCardViewHelper.c.a(ColorStateList.valueOf(i2));
    }

    public void setCardElevation(float f2) {
        super.setCardElevation(f2);
        MaterialCardViewHelper materialCardViewHelper = this.f443k;
        materialCardViewHelper.c.a(materialCardViewHelper.a.getCardElevation());
    }

    public void setCardForegroundColor(ColorStateList colorStateList) {
        MaterialShapeDrawable materialShapeDrawable = this.f443k.d;
        if (colorStateList == null) {
            colorStateList = ColorStateList.valueOf(0);
        }
        materialShapeDrawable.a(colorStateList);
    }

    public void setCheckable(boolean z) {
        this.f443k.f2353s = z;
    }

    public void setChecked(boolean z) {
        if (this.f445m != z) {
            toggle();
        }
    }

    public void setCheckedIcon(Drawable drawable) {
        this.f443k.b(drawable);
    }

    public void setCheckedIconResource(int i2) {
        this.f443k.b(AppCompatResources.c(getContext(), i2));
    }

    public void setCheckedIconTint(ColorStateList colorStateList) {
        MaterialCardViewHelper materialCardViewHelper = this.f443k;
        materialCardViewHelper.f2345k = colorStateList;
        Drawable drawable = materialCardViewHelper.f2343i;
        if (drawable != null) {
            drawable.setTintList(colorStateList);
        }
    }

    public void setClickable(boolean z) {
        super.setClickable(z);
        MaterialCardViewHelper materialCardViewHelper = this.f443k;
        Drawable drawable = materialCardViewHelper.h;
        Drawable b = materialCardViewHelper.a.isClickable() ? materialCardViewHelper.b() : materialCardViewHelper.d;
        materialCardViewHelper.h = b;
        if (drawable == b) {
            return;
        }
        if (Build.VERSION.SDK_INT < 23 || !(materialCardViewHelper.a.getForeground() instanceof InsetDrawable)) {
            materialCardViewHelper.a.setForeground(materialCardViewHelper.a(b));
        } else {
            ((InsetDrawable) materialCardViewHelper.a.getForeground()).setDrawable(b);
        }
    }

    public void setDragged(boolean z) {
        if (this.f446n != z) {
            this.f446n = z;
            refreshDrawableState();
            c();
            invalidate();
        }
    }

    public void setMaxCardElevation(float f2) {
        super.setMaxCardElevation(f2);
        this.f443k.f();
    }

    public void setOnCheckedChangeListener(a aVar) {
        this.f447o = aVar;
    }

    public void setPreventCornerOverlap(boolean z) {
        super.setPreventCornerOverlap(z);
        this.f443k.f();
        this.f443k.e();
    }

    public void setProgress(float f2) {
        MaterialCardViewHelper materialCardViewHelper = this.f443k;
        materialCardViewHelper.c.b(f2);
        MaterialShapeDrawable materialShapeDrawable = materialCardViewHelper.d;
        if (materialShapeDrawable != null) {
            materialShapeDrawable.b(f2);
        }
        MaterialShapeDrawable materialShapeDrawable2 = materialCardViewHelper.f2351q;
        if (materialShapeDrawable2 != null) {
            materialShapeDrawable2.b(f2);
        }
    }

    public void setRadius(float f2) {
        super.setRadius(f2);
        MaterialCardViewHelper materialCardViewHelper = this.f443k;
        materialCardViewHelper.a(materialCardViewHelper.f2346l.a(f2));
        materialCardViewHelper.h.invalidateSelf();
        if (materialCardViewHelper.d() || materialCardViewHelper.c()) {
            materialCardViewHelper.e();
        }
        if (materialCardViewHelper.d()) {
            materialCardViewHelper.f();
        }
    }

    public void setRippleColor(ColorStateList colorStateList) {
        MaterialCardViewHelper materialCardViewHelper = this.f443k;
        materialCardViewHelper.f2344j = colorStateList;
        materialCardViewHelper.g();
    }

    public void setRippleColorResource(int i2) {
        MaterialCardViewHelper materialCardViewHelper = this.f443k;
        materialCardViewHelper.f2344j = AppCompatResources.b(getContext(), i2);
        materialCardViewHelper.g();
    }

    public void setShapeAppearanceModel(ShapeAppearanceModel shapeAppearanceModel) {
        this.f443k.a(shapeAppearanceModel);
    }

    public void setStrokeColor(int i2) {
        MaterialCardViewHelper materialCardViewHelper = this.f443k;
        ColorStateList valueOf = ColorStateList.valueOf(i2);
        if (materialCardViewHelper.f2347m != valueOf) {
            materialCardViewHelper.f2347m = valueOf;
            materialCardViewHelper.h();
        }
    }

    public void setStrokeWidth(int i2) {
        MaterialCardViewHelper materialCardViewHelper = this.f443k;
        if (i2 != materialCardViewHelper.g) {
            materialCardViewHelper.g = i2;
            materialCardViewHelper.h();
        }
    }

    public void setUseCompatPadding(boolean z) {
        super.setUseCompatPadding(z);
        this.f443k.f();
        this.f443k.e();
    }

    public void toggle() {
        if (d() && isEnabled()) {
            this.f445m = !this.f445m;
            refreshDrawableState();
            c();
            a aVar = this.f447o;
            if (aVar != null) {
                aVar.a(this, this.f445m);
            }
        }
    }

    public MaterialCardView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, b.materialCardViewStyle);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(android.view.View, int):int
     arg types: [com.google.android.material.card.MaterialCardView, int]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(android.view.View, int):int */
    public MaterialCardView(Context context, AttributeSet attributeSet, int i2) {
        super(MaterialThemeOverlay.a(context, attributeSet, i2, f442s), attributeSet, i2);
        this.f445m = false;
        this.f446n = false;
        this.f444l = true;
        TypedArray b = ThemeEnforcement.b(getContext(), attributeSet, l.MaterialCardView, i2, f442s, new int[0]);
        MaterialCardViewHelper materialCardViewHelper = new MaterialCardViewHelper(this, attributeSet, i2, f442s);
        this.f443k = materialCardViewHelper;
        materialCardViewHelper.c.a(super.getCardBackgroundColor());
        MaterialCardViewHelper materialCardViewHelper2 = this.f443k;
        materialCardViewHelper2.b.set(super.getContentPaddingLeft(), super.getContentPaddingTop(), super.getContentPaddingRight(), super.getContentPaddingBottom());
        materialCardViewHelper2.e();
        MaterialCardViewHelper materialCardViewHelper3 = this.f443k;
        ColorStateList a2 = c.a(materialCardViewHelper3.a.getContext(), b, l.MaterialCardView_strokeColor);
        materialCardViewHelper3.f2347m = a2;
        if (a2 == null) {
            materialCardViewHelper3.f2347m = ColorStateList.valueOf(-1);
        }
        materialCardViewHelper3.g = b.getDimensionPixelSize(l.MaterialCardView_strokeWidth, 0);
        boolean z = b.getBoolean(l.MaterialCardView_android_checkable, false);
        materialCardViewHelper3.f2353s = z;
        materialCardViewHelper3.a.setLongClickable(z);
        materialCardViewHelper3.f2345k = c.a(materialCardViewHelper3.a.getContext(), b, l.MaterialCardView_checkedIconTint);
        materialCardViewHelper3.b(c.b(materialCardViewHelper3.a.getContext(), b, l.MaterialCardView_checkedIcon));
        ColorStateList a3 = c.a(materialCardViewHelper3.a.getContext(), b, l.MaterialCardView_rippleColor);
        materialCardViewHelper3.f2344j = a3;
        if (a3 == null) {
            materialCardViewHelper3.f2344j = ColorStateList.valueOf(c.a((View) materialCardViewHelper3.a, b.colorControlHighlight));
        }
        ColorStateList a4 = c.a(materialCardViewHelper3.a.getContext(), b, l.MaterialCardView_cardForegroundColor);
        materialCardViewHelper3.d.a(a4 == null ? ColorStateList.valueOf(0) : a4);
        materialCardViewHelper3.g();
        materialCardViewHelper3.c.a(materialCardViewHelper3.a.getCardElevation());
        materialCardViewHelper3.h();
        materialCardViewHelper3.a.setBackgroundInternal(materialCardViewHelper3.a(materialCardViewHelper3.c));
        Drawable b2 = materialCardViewHelper3.a.isClickable() ? materialCardViewHelper3.b() : materialCardViewHelper3.d;
        materialCardViewHelper3.h = b2;
        materialCardViewHelper3.a.setForeground(materialCardViewHelper3.a(b2));
        b.recycle();
    }

    public void setCardBackgroundColor(ColorStateList colorStateList) {
        this.f443k.c.a(colorStateList);
    }

    public void setStrokeColor(ColorStateList colorStateList) {
        MaterialCardViewHelper materialCardViewHelper = this.f443k;
        if (materialCardViewHelper.f2347m != colorStateList) {
            materialCardViewHelper.f2347m = colorStateList;
            materialCardViewHelper.h();
        }
    }
}
