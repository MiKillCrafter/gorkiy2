package com.google.firebase.iid;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.os.Looper;
import android.util.Base64;
import android.util.Log;
import androidx.annotation.Keep;
import com.google.firebase.FirebaseApp;
import j.c.a.a.c.n.c;
import j.c.a.a.i.e;
import j.c.a.a.i.l;
import j.c.c.f.d;
import j.c.c.g.b;
import j.c.c.g.l0;
import j.c.c.g.n0;
import j.c.c.g.o;
import j.c.c.g.p0;
import j.c.c.g.r;
import j.c.c.g.v;
import j.c.c.g.x;
import j.c.c.g.y;
import j.c.c.g.z;
import j.c.c.j.f;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import javax.annotation.concurrent.GuardedBy;

public class FirebaseInstanceId {

    /* renamed from: i  reason: collision with root package name */
    public static final long f555i = TimeUnit.HOURS.toSeconds(8);

    /* renamed from: j  reason: collision with root package name */
    public static v f556j;
    @GuardedBy("FirebaseInstanceId.class")

    /* renamed from: k  reason: collision with root package name */
    public static ScheduledExecutorService f557k;
    public final Executor a;
    public final FirebaseApp b;
    public final o c;
    public b d;

    /* renamed from: e  reason: collision with root package name */
    public final r f558e;

    /* renamed from: f  reason: collision with root package name */
    public final z f559f;
    @GuardedBy("this")
    public boolean g = false;
    public final a h;

    public class a {
        public final boolean a;
        @GuardedBy("this")
        public j.c.c.f.b<j.c.c.a> b;
        @GuardedBy("this")
        public Boolean c;

        public a(d dVar) {
            Boolean bool;
            ApplicationInfo applicationInfo;
            boolean z = true;
            try {
                Class.forName("com.google.firebase.messaging.FirebaseMessaging");
            } catch (ClassNotFoundException unused) {
                FirebaseApp firebaseApp = FirebaseInstanceId.this.b;
                firebaseApp.a();
                Context context = firebaseApp.a;
                Intent intent = new Intent("com.google.firebase.MESSAGING_EVENT");
                intent.setPackage(context.getPackageName());
                ResolveInfo resolveService = context.getPackageManager().resolveService(intent, 0);
                if (resolveService == null || resolveService.serviceInfo == null) {
                    z = false;
                }
            }
            this.a = z;
            FirebaseApp firebaseApp2 = FirebaseInstanceId.this.b;
            firebaseApp2.a();
            Context context2 = firebaseApp2.a;
            SharedPreferences sharedPreferences = context2.getSharedPreferences("com.google.firebase.messaging", 0);
            if (sharedPreferences.contains("auto_init")) {
                bool = Boolean.valueOf(sharedPreferences.getBoolean("auto_init", false));
            } else {
                try {
                    PackageManager packageManager = context2.getPackageManager();
                    if (!(packageManager == null || (applicationInfo = packageManager.getApplicationInfo(context2.getPackageName(), 128)) == null || applicationInfo.metaData == null || !applicationInfo.metaData.containsKey("firebase_messaging_auto_init_enabled"))) {
                        bool = Boolean.valueOf(applicationInfo.metaData.getBoolean("firebase_messaging_auto_init_enabled"));
                    }
                } catch (PackageManager.NameNotFoundException unused2) {
                }
                bool = null;
            }
            this.c = bool;
            if (bool == null && this.a) {
                n0 n0Var = new n0(this);
                this.b = n0Var;
                dVar.a(j.c.c.a.class, n0Var);
            }
        }

        public final synchronized boolean a() {
            if (this.c == null) {
                return this.a && FirebaseInstanceId.this.b.isDataCollectionDefaultEnabled();
            }
            return this.c.booleanValue();
        }
    }

    public FirebaseInstanceId(FirebaseApp firebaseApp, o oVar, Executor executor, Executor executor2, d dVar, f fVar) {
        if (o.a(firebaseApp) != null) {
            synchronized (FirebaseInstanceId.class) {
                if (f556j == null) {
                    firebaseApp.a();
                    f556j = new v(firebaseApp.a);
                }
            }
            this.b = firebaseApp;
            this.c = oVar;
            if (this.d == null) {
                b bVar = (b) firebaseApp.a(b.class);
                if (bVar == null || !bVar.a()) {
                    this.d = new p0(firebaseApp, oVar, executor, fVar);
                } else {
                    this.d = bVar;
                }
            }
            this.d = this.d;
            this.a = executor2;
            this.f559f = new z(f556j);
            this.h = new a(dVar);
            this.f558e = new r(executor);
            if (this.h.a()) {
                b();
                return;
            }
            return;
        }
        throw new IllegalStateException("FirebaseInstanceId failed to initialize, FirebaseApp is missing project ID");
    }

    public static FirebaseInstanceId f() {
        return getInstance(FirebaseApp.getInstance());
    }

    public static String g() {
        try {
            byte[] digest = MessageDigest.getInstance("SHA1").digest(f556j.b("").a.getPublic().getEncoded());
            digest[0] = (byte) ((digest[0] & 15) + 112);
            return Base64.encodeToString(digest, 0, 8, 11);
        } catch (NoSuchAlgorithmException unused) {
            Log.w("FirebaseInstanceId", "Unexpected error, device missing required algorithms");
            return null;
        }
    }

    @Keep
    public static FirebaseInstanceId getInstance(FirebaseApp firebaseApp) {
        firebaseApp.a();
        return (FirebaseInstanceId) firebaseApp.d.a(FirebaseInstanceId.class);
    }

    public static boolean h() {
        if (!Log.isLoggable("FirebaseInstanceId", 3)) {
            return Build.VERSION.SDK_INT == 23 && Log.isLoggable("FirebaseInstanceId", 3);
        }
        return true;
    }

    public final synchronized void a(boolean z) {
        this.g = z;
    }

    public final void b() {
        y c2 = c();
        if (this.d.b() || a(c2) || this.f559f.a()) {
            a();
        }
    }

    public final y c() {
        return f556j.a("", o.a(this.b), "*");
    }

    public final String d() {
        String a2 = o.a(this.b);
        if (Looper.getMainLooper() != Looper.myLooper()) {
            e b2 = c.b((Object) null);
            Executor executor = this.a;
            l0 l0Var = new l0(this, a2, "*");
            j.c.a.a.i.y yVar = (j.c.a.a.i.y) b2;
            j.c.a.a.i.y yVar2 = new j.c.a.a.i.y();
            yVar.b.a(new l(executor, l0Var, yVar2));
            yVar.f();
            return ((j.c.c.g.a) a(yVar2)).a();
        }
        throw new IOException("MAIN_THREAD");
    }

    public final synchronized void e() {
        f556j.c();
        if (this.h.a()) {
            a();
        }
    }

    public final synchronized void a() {
        if (!this.g) {
            a(0);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public final synchronized void a(long j2) {
        a(new x(this, this.f559f, Math.min(Math.max(30L, j2 << 1), f555i)), j2);
        this.g = true;
    }

    public static void a(Runnable runnable, long j2) {
        synchronized (FirebaseInstanceId.class) {
            if (f557k == null) {
                f557k = new ScheduledThreadPoolExecutor(1, new j.c.a.a.c.n.g.a("FirebaseInstanceId"));
            }
            f557k.schedule(runnable, j2, TimeUnit.SECONDS);
        }
    }

    public final <T> T a(e<T> eVar) {
        try {
            return c.a(eVar, 30000, TimeUnit.MILLISECONDS);
        } catch (ExecutionException e2) {
            Throwable cause = e2.getCause();
            if (cause instanceof IOException) {
                if ("INSTANCE_ID_RESET".equals(cause.getMessage())) {
                    e();
                }
                throw ((IOException) cause);
            } else if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else {
                throw new IOException(e2);
            }
        } catch (InterruptedException | TimeoutException unused) {
            throw new IOException("SERVICE_NOT_AVAILABLE");
        }
    }

    public final boolean a(y yVar) {
        if (yVar != null) {
            if (System.currentTimeMillis() > yVar.c + y.d || !this.c.b().equals(yVar.b)) {
                return true;
            }
            return false;
        }
        return true;
    }
}
