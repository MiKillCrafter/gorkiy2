package com.google.firebase.iid;

public final class zzam extends Exception {
    public final int b;

    public zzam(int i2, String str) {
        super(str);
        this.b = i2;
    }
}
