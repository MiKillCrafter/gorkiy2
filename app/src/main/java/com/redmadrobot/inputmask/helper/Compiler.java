package com.redmadrobot.inputmask.helper;

import j.f.a.c.Notation;
import j.f.a.c.State;
import j.f.a.c.c;
import j.f.a.c.e.EOLState;
import j.f.a.c.e.FixedState;
import j.f.a.c.e.FreeState;
import j.f.a.c.e.OptionalValueState;
import j.f.a.c.e.ValueState;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import n.i.Collections;
import n.n.c.Intrinsics;

/* compiled from: Compiler.kt */
public final class Compiler {
    public final List<c> a;

    /* compiled from: Compiler.kt */
    public static final class FormatError extends Exception {
    }

    public Compiler(List<c> list) {
        if (list != null) {
            this.a = list;
        } else {
            Intrinsics.a("customNotations");
            throw null;
        }
    }

    public final State a(String str, boolean z, boolean z2, Character ch) {
        ValueState.a aVar;
        if (str.length() == 0) {
            return new EOLState();
        }
        if (!(str.length() == 0)) {
            char charAt = str.charAt(0);
            if (charAt != '{') {
                if (charAt != '}') {
                    switch (charAt) {
                        case '[':
                            if (ch == null || '\\' != ch.charValue()) {
                                return a(Collections.a(str, 1), true, false, Character.valueOf(charAt));
                            }
                        case '\\':
                            if (ch == null || '\\' != ch.charValue()) {
                                return a(Collections.a(str, 1), z, z2, Character.valueOf(charAt));
                            }
                        case ']':
                            if (ch == null || '\\' != ch.charValue()) {
                                return a(Collections.a(str, 1), false, false, Character.valueOf(charAt));
                            }
                    }
                } else if (ch == null || '\\' != ch.charValue()) {
                    return a(Collections.a(str, 1), false, false, Character.valueOf(charAt));
                }
            } else if (ch == null || '\\' != ch.charValue()) {
                return a(Collections.a(str, 1), false, true, Character.valueOf(charAt));
            }
            if (z) {
                if (charAt == '-') {
                    return new OptionalValueState(a(Collections.a(str, 1), true, false, Character.valueOf(charAt)), new OptionalValueState.a.C0031a());
                }
                if (charAt == '0') {
                    return new ValueState(a(Collections.a(str, 1), true, false, Character.valueOf(charAt)), new ValueState.a.e());
                }
                if (charAt == '9') {
                    return new OptionalValueState(a(Collections.a(str, 1), true, false, Character.valueOf(charAt)), new OptionalValueState.a.d());
                }
                if (charAt == 'A') {
                    return new ValueState(a(Collections.a(str, 1), true, false, Character.valueOf(charAt)), new ValueState.a.d());
                }
                if (charAt == '_') {
                    return new ValueState(a(Collections.a(str, 1), true, false, Character.valueOf(charAt)), new ValueState.a.C0032a());
                }
                if (charAt == 'a') {
                    return new OptionalValueState(a(Collections.a(str, 1), true, false, Character.valueOf(charAt)), new OptionalValueState.a.c());
                }
                if (charAt != 8230) {
                    Iterator<c> it = this.a.iterator();
                    while (it.hasNext()) {
                        if (it.next() == null) {
                            throw null;
                        } else if (charAt == 0) {
                            a(Collections.a(str, 1), true, false, Character.valueOf(charAt));
                            throw null;
                        }
                    }
                    throw new FormatError();
                }
                if ((ch != null && ch.charValue() == '0') || (ch != null && ch.charValue() == '9')) {
                    aVar = new ValueState.a.e();
                } else if ((ch != null && ch.charValue() == 'A') || (ch != null && ch.charValue() == 'a')) {
                    aVar = new ValueState.a.d();
                } else if ((ch != null && ch.charValue() == '_') || (ch != null && ch.charValue() == '-')) {
                    aVar = new ValueState.a.C0032a();
                } else if (ch != null && ch.charValue() == 8230) {
                    aVar = new ValueState.a.C0032a();
                } else if (ch != null && ch.charValue() == '[') {
                    aVar = new ValueState.a.C0032a();
                } else {
                    Iterator<T> it2 = this.a.iterator();
                    while (it2.hasNext()) {
                        if (((Notation) it2.next()) == null) {
                            throw null;
                        } else if (ch != null && ch.charValue() == 0) {
                            ch.charValue();
                            throw null;
                        }
                    }
                    throw new FormatError();
                }
                return new ValueState(aVar);
            } else if (z2) {
                return new FixedState(a(Collections.a(str, 1), false, true, Character.valueOf(charAt)), charAt);
            } else {
                return new FreeState(a(Collections.a(str, 1), false, false, Character.valueOf(charAt)), charAt);
            }
        } else {
            throw new NoSuchElementException("Char sequence is empty.");
        }
    }
}
