package n;

import java.io.Serializable;
import n.n.b.Functions;
import n.n.c.Intrinsics;

/* compiled from: LazyJVM.kt */
public final class LazyJVM<T> implements Lazy<T>, Serializable {
    public Functions<? extends T> b;
    public volatile Object c;
    public final Object d;

    public /* synthetic */ LazyJVM(Functions functions, Object obj, int i2) {
        obj = (i2 & 2) != 0 ? null : obj;
        if (functions != null) {
            this.b = functions;
            this.c = Lazy0.a;
            this.d = obj == null ? this : obj;
            return;
        }
        Intrinsics.a("initializer");
        throw null;
    }

    public T getValue() {
        T t2;
        T t3 = this.c;
        if (t3 != Lazy0.a) {
            return t3;
        }
        synchronized (this.d) {
            t2 = this.c;
            if (t2 == Lazy0.a) {
                Functions functions = this.b;
                if (functions != null) {
                    t2 = functions.b();
                    this.c = t2;
                    this.b = null;
                } else {
                    Intrinsics.a();
                    throw null;
                }
            }
        }
        return t2;
    }

    public String toString() {
        return this.c != Lazy0.a ? String.valueOf(getValue()) : "Lazy value not initialized yet.";
    }
}
