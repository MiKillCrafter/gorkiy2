package n.n.c;

import n.i.Collections;
import n.p.KClass;

/* compiled from: ClassReference.kt */
public final class ClassReference implements KClass<Object>, c {
    public final Class<?> a;

    public ClassReference(Class<?> cls) {
        if (cls != null) {
            this.a = cls;
        } else {
            Intrinsics.a("jClass");
            throw null;
        }
    }

    public Class<?> a() {
        return this.a;
    }

    public boolean equals(Object obj) {
        return (obj instanceof ClassReference) && Intrinsics.a(Collections.a(this), Collections.a((KClass) obj));
    }

    public int hashCode() {
        return Collections.a((KClass) this).hashCode();
    }

    public String toString() {
        return this.a.toString() + " (Kotlin reflection is not available)";
    }
}
