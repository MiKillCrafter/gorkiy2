package n.n.c;

import n.p.KDeclarationContainer;
import n.p.KProperty1;

public class PropertyReference1Impl extends PropertyReference1 {

    /* renamed from: e  reason: collision with root package name */
    public final KDeclarationContainer f2787e;

    /* renamed from: f  reason: collision with root package name */
    public final String f2788f;
    public final String g;

    public PropertyReference1Impl(KDeclarationContainer kDeclarationContainer, String str, String str2) {
        this.f2787e = kDeclarationContainer;
        this.f2788f = str;
        this.g = str2;
    }

    public String f() {
        return this.f2788f;
    }

    public KDeclarationContainer g() {
        return this.f2787e;
    }

    /* JADX WARN: Type inference failed for: r0v2, types: [n.p.KProperty1$a, n.p.KCallable] */
    public Object get(Object obj) {
        return ((KProperty1) h()).a().a(obj);
    }

    public String i() {
        return this.g;
    }
}
