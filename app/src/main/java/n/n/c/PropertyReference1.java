package n.n.c;

import n.p.KCallable;
import n.p.KProperty1;

public abstract class PropertyReference1 extends PropertyReference implements KProperty1 {
    public Object a(Object obj) {
        return get(obj);
    }

    public KCallable e() {
        Reflection.a(this);
        return this;
    }

    public KProperty1.a a() {
        return ((KProperty1) h()).a();
    }
}
