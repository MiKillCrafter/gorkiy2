package n.n.c;

import java.io.Serializable;
import n.n.KotlinReflectionNotSupportedError;
import n.p.KCallable;
import n.p.KDeclarationContainer;

public abstract class CallableReference implements KCallable, Serializable {
    public static final Object d = a.b;
    public transient KCallable b;
    public final Object c;

    public static class a implements Serializable {
        public static final a b = new a();
    }

    public CallableReference() {
        this.c = d;
    }

    public Object a(Object... objArr) {
        return h().a(objArr);
    }

    public KCallable d() {
        KCallable kCallable = this.b;
        if (kCallable != null) {
            return kCallable;
        }
        KCallable e2 = e();
        this.b = e2;
        return e2;
    }

    public abstract KCallable e();

    public String f() {
        throw new AbstractMethodError();
    }

    public KDeclarationContainer g() {
        throw new AbstractMethodError();
    }

    public KCallable h() {
        KCallable d2 = d();
        if (d2 != this) {
            return d2;
        }
        throw new KotlinReflectionNotSupportedError();
    }

    public String i() {
        throw new AbstractMethodError();
    }

    public CallableReference(Object obj) {
        this.c = obj;
    }
}
