package n.r;

import java.util.List;
import n.n.c.Intrinsics;

/* compiled from: MatchResult.kt */
public interface MatchResult {

    /* compiled from: MatchResult.kt */
    public static final class a {
        public final MatchResult a;

        public a(MatchResult matchResult) {
            if (matchResult != null) {
                this.a = matchResult;
            } else {
                Intrinsics.a("match");
                throw null;
            }
        }
    }

    List<String> a();

    a b();

    MatchResult next();
}
