package n.r;

import java.util.List;
import java.util.regex.Matcher;
import n.i.AbstractList;
import n.n.c.Intrinsics;
import n.r.MatchResult;

/* compiled from: Regex.kt */
public final class Regex0 implements MatchResult {
    public List<String> a;
    public final Matcher b;
    public final CharSequence c;

    /* compiled from: Regex.kt */
    public static final class a extends AbstractList<String> {
        public final /* synthetic */ Regex0 b;

        public a(Regex0 regex0) {
            this.b = regex0;
        }

        public int c() {
            return this.b.b.groupCount() + 1;
        }

        public final boolean contains(Object obj) {
            if (obj instanceof String) {
                return super.contains((String) obj);
            }
            return false;
        }

        public Object get(int i2) {
            String group = this.b.b.group(i2);
            return group != null ? group : "";
        }

        public final int indexOf(Object obj) {
            if (obj instanceof String) {
                return super.indexOf((String) obj);
            }
            return -1;
        }

        public final int lastIndexOf(Object obj) {
            if (obj instanceof String) {
                return super.lastIndexOf((String) obj);
            }
            return -1;
        }
    }

    public Regex0(Matcher matcher, CharSequence charSequence) {
        if (matcher == null) {
            Intrinsics.a("matcher");
            throw null;
        } else if (charSequence != null) {
            this.b = matcher;
            this.c = charSequence;
        } else {
            Intrinsics.a("input");
            throw null;
        }
    }

    public List<String> a() {
        if (this.a == null) {
            this.a = new a(this);
        }
        List<String> list = this.a;
        if (list != null) {
            return list;
        }
        Intrinsics.a();
        throw null;
    }

    public MatchResult.a b() {
        return new MatchResult.a(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.regex.Matcher, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public MatchResult next() {
        int end = this.b.end() + (this.b.end() == this.b.start() ? 1 : 0);
        if (end > this.c.length()) {
            return null;
        }
        Matcher matcher = this.b.pattern().matcher(this.c);
        Intrinsics.a((Object) matcher, "matcher.pattern().matcher(input)");
        CharSequence charSequence = this.c;
        if (!matcher.find(end)) {
            return null;
        }
        return new Regex0(matcher, charSequence);
    }
}
