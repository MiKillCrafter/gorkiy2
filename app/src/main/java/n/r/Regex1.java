package n.r;

import java.util.regex.Matcher;
import n.n.b.Functions;
import n.n.c.Intrinsics;
import n.n.c.j;

/* compiled from: Regex.kt */
public final class Regex1 extends j implements Functions<c> {
    public final /* synthetic */ Regex c;
    public final /* synthetic */ CharSequence d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ int f2792e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Regex1(Regex regex, CharSequence charSequence, int i2) {
        super(0);
        this.c = regex;
        this.d = charSequence;
        this.f2792e = i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.regex.Matcher, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Object b() {
        Regex regex = this.c;
        CharSequence charSequence = this.d;
        int i2 = this.f2792e;
        if (charSequence != null) {
            Matcher matcher = regex.b.matcher(charSequence);
            Intrinsics.a((Object) matcher, "nativePattern.matcher(input)");
            if (!matcher.find(i2)) {
                return null;
            }
            return new Regex0(matcher, charSequence);
        }
        Intrinsics.a("input");
        throw null;
    }
}
