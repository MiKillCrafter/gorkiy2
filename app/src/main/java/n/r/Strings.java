package n.r;

import java.util.Iterator;
import java.util.NoSuchElementException;
import kotlin.TypeCastException;
import n.Tuples;
import n.n.b.Functions1;
import n.o.Ranges0;
import n.o.c;
import n.q.Sequence;

/* compiled from: Strings.kt */
public final class Strings implements Sequence<c> {
    public final CharSequence a;
    public final int b;
    public final int c;
    public final Functions1<CharSequence, Integer, Tuples<Integer, Integer>> d;

    /* compiled from: Strings.kt */
    public static final class a implements Iterator<c>, n.n.c.t.a {
        public int b = -1;
        public int c;
        public int d;

        /* renamed from: e  reason: collision with root package name */
        public Ranges0 f2794e;

        /* renamed from: f  reason: collision with root package name */
        public int f2795f;
        public final /* synthetic */ Strings g;

        public a(Strings strings) {
            this.g = strings;
            int i2 = strings.b;
            int length = strings.a.length();
            if (length >= 0) {
                if (i2 < 0) {
                    i2 = 0;
                } else if (i2 > length) {
                    i2 = length;
                }
                this.c = i2;
                this.d = i2;
                return;
            }
            throw new IllegalArgumentException("Cannot coerce value to an empty range: maximum " + length + " is less than minimum " + 0 + '.');
        }

        /* JADX WARNING: Code restructure failed: missing block: B:6:0x0019, code lost:
            if (r4 < r0) goto L_0x001b;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void b() {
            /*
                r6 = this;
                int r0 = r6.d
                r1 = 0
                if (r0 >= 0) goto L_0x000c
                r6.b = r1
                r0 = 0
                r6.f2794e = r0
                goto L_0x0086
            L_0x000c:
                n.r.Strings r0 = r6.g
                int r0 = r0.c
                r2 = -1
                r3 = 1
                if (r0 <= 0) goto L_0x001b
                int r4 = r6.f2795f
                int r4 = r4 + r3
                r6.f2795f = r4
                if (r4 >= r0) goto L_0x0027
            L_0x001b:
                int r0 = r6.d
                n.r.Strings r4 = r6.g
                java.lang.CharSequence r4 = r4.a
                int r4 = r4.length()
                if (r0 <= r4) goto L_0x003b
            L_0x0027:
                int r0 = r6.c
                n.o.Ranges0 r1 = new n.o.Ranges0
                n.r.Strings r4 = r6.g
                java.lang.CharSequence r4 = r4.a
                int r4 = n.r.Indent.a(r4)
                r1.<init>(r0, r4)
                r6.f2794e = r1
                r6.d = r2
                goto L_0x0084
            L_0x003b:
                n.r.Strings r0 = r6.g
                n.n.b.Functions1<java.lang.CharSequence, java.lang.Integer, n.Tuples<java.lang.Integer, java.lang.Integer>> r4 = r0.d
                java.lang.CharSequence r0 = r0.a
                int r5 = r6.d
                java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
                java.lang.Object r0 = r4.a(r0, r5)
                n.Tuples r0 = (n.Tuples) r0
                if (r0 != 0) goto L_0x0063
                int r0 = r6.c
                n.o.Ranges0 r1 = new n.o.Ranges0
                n.r.Strings r4 = r6.g
                java.lang.CharSequence r4 = r4.a
                int r4 = n.r.Indent.a(r4)
                r1.<init>(r0, r4)
                r6.f2794e = r1
                r6.d = r2
                goto L_0x0084
            L_0x0063:
                A r2 = r0.b
                java.lang.Number r2 = (java.lang.Number) r2
                int r2 = r2.intValue()
                B r0 = r0.c
                java.lang.Number r0 = (java.lang.Number) r0
                int r0 = r0.intValue()
                int r4 = r6.c
                n.o.Ranges0 r4 = n.o.d.b(r4, r2)
                r6.f2794e = r4
                int r2 = r2 + r0
                r6.c = r2
                if (r0 != 0) goto L_0x0081
                r1 = 1
            L_0x0081:
                int r2 = r2 + r1
                r6.d = r2
            L_0x0084:
                r6.b = r3
            L_0x0086:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: n.r.Strings.a.b():void");
        }

        public boolean hasNext() {
            if (this.b == -1) {
                b();
            }
            return this.b == 1;
        }

        public Object next() {
            if (this.b == -1) {
                b();
            }
            if (this.b != 0) {
                Ranges0 ranges0 = this.f2794e;
                if (ranges0 != null) {
                    this.f2794e = null;
                    this.b = -1;
                    return ranges0;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.ranges.IntRange");
            }
            throw new NoSuchElementException();
        }

        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    /* JADX WARN: Type inference failed for: r5v0, types: [n.n.b.Functions1<? super java.lang.CharSequence, ? super java.lang.Integer, n.Tuples<java.lang.Integer, java.lang.Integer>>, n.n.b.Functions1<java.lang.CharSequence, java.lang.Integer, n.Tuples<java.lang.Integer, java.lang.Integer>>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Strings(java.lang.CharSequence r2, int r3, int r4, n.n.b.Functions1<? super java.lang.CharSequence, ? super java.lang.Integer, n.Tuples<java.lang.Integer, java.lang.Integer>> r5) {
        /*
            r1 = this;
            r0 = 0
            if (r2 == 0) goto L_0x0017
            if (r5 == 0) goto L_0x0011
            r1.<init>()
            r1.a = r2
            r1.b = r3
            r1.c = r4
            r1.d = r5
            return
        L_0x0011:
            java.lang.String r2 = "getNextMatch"
            n.n.c.Intrinsics.a(r2)
            throw r0
        L_0x0017:
            java.lang.String r2 = "input"
            n.n.c.Intrinsics.a(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: n.r.Strings.<init>(java.lang.CharSequence, int, int, n.n.b.Functions1):void");
    }

    public Iterator<c> iterator() {
        return new a(this);
    }
}
