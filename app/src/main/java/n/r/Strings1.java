package n.r;

import java.util.List;
import n.Tuples;
import n.n.b.Functions1;
import n.n.c.j;

/* compiled from: Strings.kt */
public final class Strings1 extends j implements Functions1<CharSequence, Integer, Tuples<? extends Integer, ? extends Integer>> {
    public final /* synthetic */ List c;
    public final /* synthetic */ boolean d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Strings1(List list, boolean z) {
        super(2);
        this.c = list;
        this.d = z;
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [n.o.Ranges0, n.o.Progressions] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean, int):int
     arg types: [java.lang.CharSequence, java.lang.String, int, int, int]
     candidates:
      n.r.Indent.a(java.lang.CharSequence, char, int, boolean, int):int
      n.r.Indent.a(java.lang.String, java.lang.String, java.lang.String, boolean, int):java.lang.String
      n.r.Indent.a(java.lang.CharSequence, char[], boolean, int, int):java.util.List
      n.r.Indent.a(java.lang.CharSequence, java.lang.String[], boolean, int, int):n.q.Sequence
      n.r.Indent.a(java.lang.String, java.lang.String, int, boolean, int):boolean
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean, int):int */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00e0  */
    /* JADX WARNING: Removed duplicated region for block: B:66:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(java.lang.Object r14, java.lang.Object r15) {
        /*
            r13 = this;
            java.lang.CharSequence r14 = (java.lang.CharSequence) r14
            java.lang.Number r15 = (java.lang.Number) r15
            int r15 = r15.intValue()
            r6 = 0
            if (r14 == 0) goto L_0x00f4
            java.util.List r7 = r13.c
            boolean r8 = r13.d
            r0 = 0
            if (r8 != 0) goto L_0x004b
            int r1 = r7.size()
            r2 = 1
            if (r1 != r2) goto L_0x004b
            int r1 = r7.size()
            if (r1 == 0) goto L_0x0043
            if (r1 != r2) goto L_0x003b
            java.lang.Object r1 = r7.get(r0)
            java.lang.String r1 = (java.lang.String) r1
            r2 = 4
            int r14 = n.r.Indent.a(r14, r1, r15, r0, r2)
            if (r14 >= 0) goto L_0x0030
            goto L_0x00dd
        L_0x0030:
            java.lang.Integer r14 = java.lang.Integer.valueOf(r14)
            n.Tuples r15 = new n.Tuples
            r15.<init>(r14, r1)
            goto L_0x00de
        L_0x003b:
            java.lang.IllegalArgumentException r14 = new java.lang.IllegalArgumentException
            java.lang.String r15 = "List has more than one element."
            r14.<init>(r15)
            throw r14
        L_0x0043:
            java.util.NoSuchElementException r14 = new java.util.NoSuchElementException
            java.lang.String r15 = "List is empty."
            r14.<init>(r15)
            throw r14
        L_0x004b:
            if (r15 >= 0) goto L_0x004e
            r15 = 0
        L_0x004e:
            n.o.Ranges0 r0 = new n.o.Ranges0
            int r1 = r14.length()
            r0.<init>(r15, r1)
            boolean r15 = r14 instanceof java.lang.String
            if (r15 == 0) goto L_0x009d
            int r15 = r0.b
            int r9 = r0.c
            int r10 = r0.d
            if (r10 < 0) goto L_0x0066
            if (r15 > r9) goto L_0x00dd
            goto L_0x0068
        L_0x0066:
            if (r15 < r9) goto L_0x00dd
        L_0x0068:
            java.util.Iterator r11 = r7.iterator()
        L_0x006c:
            boolean r0 = r11.hasNext()
            if (r0 == 0) goto L_0x008a
            java.lang.Object r12 = r11.next()
            r0 = r12
            java.lang.String r0 = (java.lang.String) r0
            r1 = 0
            r2 = r14
            java.lang.String r2 = (java.lang.String) r2
            int r4 = r0.length()
            r3 = r15
            r5 = r8
            boolean r0 = n.r.Indent.a(r0, r1, r2, r3, r4, r5)
            if (r0 == 0) goto L_0x006c
            goto L_0x008b
        L_0x008a:
            r12 = r6
        L_0x008b:
            java.lang.String r12 = (java.lang.String) r12
            if (r12 == 0) goto L_0x0099
            java.lang.Integer r14 = java.lang.Integer.valueOf(r15)
            n.Tuples r15 = new n.Tuples
            r15.<init>(r14, r12)
            goto L_0x00de
        L_0x0099:
            if (r15 == r9) goto L_0x00dd
            int r15 = r15 + r10
            goto L_0x0068
        L_0x009d:
            int r15 = r0.b
            int r9 = r0.c
            int r10 = r0.d
            if (r10 < 0) goto L_0x00a8
            if (r15 > r9) goto L_0x00dd
            goto L_0x00aa
        L_0x00a8:
            if (r15 < r9) goto L_0x00dd
        L_0x00aa:
            java.util.Iterator r11 = r7.iterator()
        L_0x00ae:
            boolean r0 = r11.hasNext()
            if (r0 == 0) goto L_0x00ca
            java.lang.Object r12 = r11.next()
            r0 = r12
            java.lang.String r0 = (java.lang.String) r0
            r1 = 0
            int r4 = r0.length()
            r2 = r14
            r3 = r15
            r5 = r8
            boolean r0 = n.r.Indent.a(r0, r1, r2, r3, r4, r5)
            if (r0 == 0) goto L_0x00ae
            goto L_0x00cb
        L_0x00ca:
            r12 = r6
        L_0x00cb:
            java.lang.String r12 = (java.lang.String) r12
            if (r12 == 0) goto L_0x00d9
            java.lang.Integer r14 = java.lang.Integer.valueOf(r15)
            n.Tuples r15 = new n.Tuples
            r15.<init>(r14, r12)
            goto L_0x00de
        L_0x00d9:
            if (r15 == r9) goto L_0x00dd
            int r15 = r15 + r10
            goto L_0x00aa
        L_0x00dd:
            r15 = r6
        L_0x00de:
            if (r15 == 0) goto L_0x00f3
            A r14 = r15.b
            B r15 = r15.c
            java.lang.String r15 = (java.lang.String) r15
            int r15 = r15.length()
            java.lang.Integer r15 = java.lang.Integer.valueOf(r15)
            n.Tuples r6 = new n.Tuples
            r6.<init>(r14, r15)
        L_0x00f3:
            return r6
        L_0x00f4:
            java.lang.String r14 = "$receiver"
            n.n.c.Intrinsics.a(r14)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: n.r.Strings1.a(java.lang.Object, java.lang.Object):java.lang.Object");
    }
}
