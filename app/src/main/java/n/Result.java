package n;

import j.a.a.a.outline;
import java.io.Serializable;
import n.n.c.Intrinsics;

/* compiled from: Result.kt */
public final class Result<T> implements Serializable {

    /* compiled from: Result.kt */
    public static final class a implements Serializable {
        public final Throwable b;

        public a(Throwable th) {
            if (th != null) {
                this.b = th;
            } else {
                Intrinsics.a("exception");
                throw null;
            }
        }

        public boolean equals(Object obj) {
            return (obj instanceof a) && Intrinsics.a(this.b, ((a) obj).b);
        }

        public int hashCode() {
            return this.b.hashCode();
        }

        public String toString() {
            StringBuilder a = outline.a("Failure(");
            a.append(this.b);
            a.append(')');
            return a.toString();
        }
    }

    public static Object a(Object obj) {
        return obj;
    }
}
