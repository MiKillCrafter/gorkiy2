package n.q;

import java.util.Iterator;
import java.util.NoSuchElementException;
import kotlin.TypeCastException;
import n.n.b.Functions;
import n.n.b.Functions0;
import n.n.c.Intrinsics;

/* compiled from: Sequences.kt */
public final class Sequences1<T> implements Sequence<T> {
    public final Functions<T> a;
    public final Functions0<T, T> b;

    /* compiled from: Sequences.kt */
    public static final class a implements Iterator<T>, n.n.c.t.a {
        public T b;
        public int c = -2;
        public final /* synthetic */ Sequences1 d;

        public a(Sequences1 sequences1) {
            this.d = sequences1;
        }

        public final void b() {
            T t2;
            if (this.c == -2) {
                t2 = this.d.a.b();
            } else {
                Functions0<T, T> functions0 = this.d.b;
                T t3 = this.b;
                if (t3 != null) {
                    t2 = functions0.a(t3);
                } else {
                    Intrinsics.a();
                    throw null;
                }
            }
            this.b = t2;
            this.c = t2 == null ? 0 : 1;
        }

        public boolean hasNext() {
            if (this.c < 0) {
                b();
            }
            return this.c == 1;
        }

        public T next() {
            if (this.c < 0) {
                b();
            }
            if (this.c != 0) {
                T t2 = this.b;
                if (t2 != null) {
                    this.c = -1;
                    return t2;
                }
                throw new TypeCastException("null cannot be cast to non-null type T");
            }
            throw new NoSuchElementException();
        }

        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    public Sequences1(Functions<? extends T> functions, Functions0<? super T, ? extends T> functions0) {
        if (functions == null) {
            Intrinsics.a("getInitialValue");
            throw null;
        } else if (functions0 != null) {
            this.a = functions;
            this.b = functions0;
        } else {
            Intrinsics.a("getNextValue");
            throw null;
        }
    }

    public Iterator<T> iterator() {
        return new a(this);
    }
}
