package n.q;

import java.util.Iterator;
import n.n.c.t.a;

/* compiled from: Iterables.kt */
public final class Iterables implements Iterable<T>, a {
    public final /* synthetic */ Sequence b;

    public Iterables(Sequence sequence) {
        this.b = sequence;
    }

    public Iterator<T> iterator() {
        return this.b.iterator();
    }
}
