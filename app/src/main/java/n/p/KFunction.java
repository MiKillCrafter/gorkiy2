package n.p;

import n.Function;

/* compiled from: KFunction.kt */
public interface KFunction<R> extends KCallable<R>, Function<R> {
}
