package j.e.b;

import j.e.b.AppendOnlyLinkedArrayList;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import l.b.Observer;
import l.b.s.b;

public final class BehaviorRelay<T> extends Relay<T> {
    public static final Object[] g = new Object[0];
    public static final a[] h = new a[0];
    public final AtomicReference<T> b = new AtomicReference<>();
    public final AtomicReference<a<T>[]> c = new AtomicReference<>(h);
    public final Lock d;

    /* renamed from: e  reason: collision with root package name */
    public final Lock f2600e;

    /* renamed from: f  reason: collision with root package name */
    public long f2601f;

    public BehaviorRelay() {
        ReentrantReadWriteLock reentrantReadWriteLock = new ReentrantReadWriteLock();
        this.d = reentrantReadWriteLock.readLock();
        this.f2600e = reentrantReadWriteLock.writeLock();
    }

    public void a(a aVar) {
        a[] aVarArr;
        a[] aVarArr2;
        do {
            aVarArr = (a[]) this.c.get();
            int length = aVarArr.length;
            if (length != 0) {
                int i2 = -1;
                int i3 = 0;
                while (true) {
                    if (i3 >= length) {
                        break;
                    } else if (aVarArr[i3] == aVar) {
                        i2 = i3;
                        break;
                    } else {
                        i3++;
                    }
                }
                if (i2 >= 0) {
                    if (length == 1) {
                        aVarArr2 = h;
                    } else {
                        a[] aVarArr3 = new a[(length - 1)];
                        System.arraycopy(aVarArr, 0, aVarArr3, 0, i2);
                        System.arraycopy(aVarArr, i2 + 1, aVarArr3, i2, (length - i2) - 1);
                        aVarArr2 = aVarArr3;
                    }
                } else {
                    return;
                }
            } else {
                return;
            }
        } while (!this.c.compareAndSet(aVarArr, aVarArr2));
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v1, resolved type: j.e.b.BehaviorRelay$a[]} */
    /* JADX WARN: Type inference failed for: r0v0, types: [j.e.b.BehaviorRelay$a, l.b.s.Disposable] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b(l.b.Observer r5) {
        /*
            r4 = this;
            j.e.b.BehaviorRelay$a r0 = new j.e.b.BehaviorRelay$a
            r0.<init>(r5, r4)
            r5.a(r0)
        L_0x0008:
            java.util.concurrent.atomic.AtomicReference<j.e.b.BehaviorRelay$a<T>[]> r5 = r4.c
            java.lang.Object r5 = r5.get()
            j.e.b.BehaviorRelay$a[] r5 = (j.e.b.BehaviorRelay.a[]) r5
            int r1 = r5.length
            int r2 = r1 + 1
            j.e.b.BehaviorRelay$a[] r2 = new j.e.b.BehaviorRelay.a[r2]
            r3 = 0
            java.lang.System.arraycopy(r5, r3, r2, r3, r1)
            r2[r1] = r0
            java.util.concurrent.atomic.AtomicReference<j.e.b.BehaviorRelay$a<T>[]> r1 = r4.c
            boolean r5 = r1.compareAndSet(r5, r2)
            if (r5 == 0) goto L_0x0008
            boolean r5 = r0.h
            if (r5 == 0) goto L_0x002b
            r4.a(r0)
            goto L_0x002e
        L_0x002b:
            r0.a()
        L_0x002e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: j.e.b.BehaviorRelay.b(l.b.Observer):void");
    }

    public T c() {
        return this.b.get();
    }

    public void a(Object obj) {
        if (obj != null) {
            this.f2600e.lock();
            this.f2601f++;
            this.b.lazySet(obj);
            this.f2600e.unlock();
            for (a a2 : (a[]) this.c.get()) {
                a2.a(obj, this.f2601f);
            }
            return;
        }
        throw new NullPointerException("value == null");
    }

    public static final class a<T> implements b, AppendOnlyLinkedArrayList.a<T> {
        public final Observer<? super T> b;
        public final BehaviorRelay<T> c;
        public boolean d;

        /* renamed from: e  reason: collision with root package name */
        public boolean f2602e;

        /* renamed from: f  reason: collision with root package name */
        public AppendOnlyLinkedArrayList<T> f2603f;
        public boolean g;
        public volatile boolean h;

        /* renamed from: i  reason: collision with root package name */
        public long f2604i;

        public a(Observer<? super T> observer, BehaviorRelay<T> behaviorRelay) {
            this.b = observer;
            this.c = behaviorRelay;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:19:0x0031, code lost:
            if (r0 == null) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x0033, code lost:
            a(r0);
            b();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
            return;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void a() {
            /*
                r4 = this;
                boolean r0 = r4.h
                if (r0 == 0) goto L_0x0005
                return
            L_0x0005:
                monitor-enter(r4)
                boolean r0 = r4.h     // Catch:{ all -> 0x003a }
                if (r0 == 0) goto L_0x000c
                monitor-exit(r4)     // Catch:{ all -> 0x003a }
                return
            L_0x000c:
                boolean r0 = r4.d     // Catch:{ all -> 0x003a }
                if (r0 == 0) goto L_0x0012
                monitor-exit(r4)     // Catch:{ all -> 0x003a }
                return
            L_0x0012:
                j.e.b.BehaviorRelay<T> r0 = r4.c     // Catch:{ all -> 0x003a }
                java.util.concurrent.locks.Lock r1 = r0.d     // Catch:{ all -> 0x003a }
                r1.lock()     // Catch:{ all -> 0x003a }
                long r2 = r0.f2601f     // Catch:{ all -> 0x003a }
                r4.f2604i = r2     // Catch:{ all -> 0x003a }
                java.util.concurrent.atomic.AtomicReference<T> r0 = r0.b     // Catch:{ all -> 0x003a }
                java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x003a }
                r1.unlock()     // Catch:{ all -> 0x003a }
                r1 = 1
                if (r0 == 0) goto L_0x002b
                r2 = 1
                goto L_0x002c
            L_0x002b:
                r2 = 0
            L_0x002c:
                r4.f2602e = r2     // Catch:{ all -> 0x003a }
                r4.d = r1     // Catch:{ all -> 0x003a }
                monitor-exit(r4)     // Catch:{ all -> 0x003a }
                if (r0 == 0) goto L_0x0039
                r4.a(r0)
                r4.b()
            L_0x0039:
                return
            L_0x003a:
                r0 = move-exception
                monitor-exit(r4)     // Catch:{ all -> 0x003a }
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: j.e.b.BehaviorRelay.a.a():void");
        }

        /* JADX WARNING: Code restructure failed: missing block: B:12:0x0013, code lost:
            r2 = r0.b;
            r0 = r0.a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x0017, code lost:
            if (r2 == null) goto L_0x0000;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0019, code lost:
            r3 = 0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x001a, code lost:
            if (r3 >= r0) goto L_0x002b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x001c, code lost:
            r4 = r2[r3];
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x001e, code lost:
            if (r4 != null) goto L_0x0021;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x0025, code lost:
            if (a(r4) == false) goto L_0x0028;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x0028, code lost:
            r3 = r3 + 1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x002b, code lost:
            r2 = r2[r0];
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void b() {
            /*
                r5 = this;
            L_0x0000:
                boolean r0 = r5.h
                if (r0 == 0) goto L_0x0005
                return
            L_0x0005:
                monitor-enter(r5)
                j.e.b.AppendOnlyLinkedArrayList<T> r0 = r5.f2603f     // Catch:{ all -> 0x0030 }
                r1 = 0
                if (r0 != 0) goto L_0x000f
                r5.f2602e = r1     // Catch:{ all -> 0x0030 }
                monitor-exit(r5)     // Catch:{ all -> 0x0030 }
                return
            L_0x000f:
                r2 = 0
                r5.f2603f = r2     // Catch:{ all -> 0x0030 }
                monitor-exit(r5)     // Catch:{ all -> 0x0030 }
                java.lang.Object[] r2 = r0.b
                int r0 = r0.a
            L_0x0017:
                if (r2 == 0) goto L_0x0000
                r3 = 0
            L_0x001a:
                if (r3 >= r0) goto L_0x002b
                r4 = r2[r3]
                if (r4 != 0) goto L_0x0021
                goto L_0x002b
            L_0x0021:
                boolean r4 = r5.a(r4)
                if (r4 == 0) goto L_0x0028
                goto L_0x002b
            L_0x0028:
                int r3 = r3 + 1
                goto L_0x001a
            L_0x002b:
                r2 = r2[r0]
                java.lang.Object[] r2 = (java.lang.Object[]) r2
                goto L_0x0017
            L_0x0030:
                r0 = move-exception
                monitor-exit(r5)     // Catch:{ all -> 0x0030 }
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: j.e.b.BehaviorRelay.a.b():void");
        }

        public void f() {
            if (!this.h) {
                this.h = true;
                this.c.a(this);
            }
        }

        public boolean g() {
            return this.h;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:28:0x0046, code lost:
            r3.g = true;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void a(T r4, long r5) {
            /*
                r3 = this;
                boolean r0 = r3.h
                if (r0 == 0) goto L_0x0005
                return
            L_0x0005:
                boolean r0 = r3.g
                if (r0 != 0) goto L_0x004c
                monitor-enter(r3)
                boolean r0 = r3.h     // Catch:{ all -> 0x0049 }
                if (r0 == 0) goto L_0x0010
                monitor-exit(r3)     // Catch:{ all -> 0x0049 }
                return
            L_0x0010:
                long r0 = r3.f2604i     // Catch:{ all -> 0x0049 }
                int r2 = (r0 > r5 ? 1 : (r0 == r5 ? 0 : -1))
                if (r2 != 0) goto L_0x0018
                monitor-exit(r3)     // Catch:{ all -> 0x0049 }
                return
            L_0x0018:
                boolean r5 = r3.f2602e     // Catch:{ all -> 0x0049 }
                r6 = 1
                if (r5 == 0) goto L_0x0043
                j.e.b.AppendOnlyLinkedArrayList<T> r5 = r3.f2603f     // Catch:{ all -> 0x0049 }
                if (r5 != 0) goto L_0x0029
                j.e.b.AppendOnlyLinkedArrayList r5 = new j.e.b.AppendOnlyLinkedArrayList     // Catch:{ all -> 0x0049 }
                r0 = 4
                r5.<init>(r0)     // Catch:{ all -> 0x0049 }
                r3.f2603f = r5     // Catch:{ all -> 0x0049 }
            L_0x0029:
                int r0 = r5.a     // Catch:{ all -> 0x0049 }
                int r1 = r5.d     // Catch:{ all -> 0x0049 }
                if (r1 != r0) goto L_0x003a
                int r1 = r0 + 1
                java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x0049 }
                java.lang.Object[] r2 = r5.c     // Catch:{ all -> 0x0049 }
                r2[r0] = r1     // Catch:{ all -> 0x0049 }
                r5.c = r1     // Catch:{ all -> 0x0049 }
                r1 = 0
            L_0x003a:
                java.lang.Object[] r0 = r5.c     // Catch:{ all -> 0x0049 }
                r0[r1] = r4     // Catch:{ all -> 0x0049 }
                int r1 = r1 + r6
                r5.d = r1     // Catch:{ all -> 0x0049 }
                monitor-exit(r3)     // Catch:{ all -> 0x0049 }
                return
            L_0x0043:
                r3.d = r6     // Catch:{ all -> 0x0049 }
                monitor-exit(r3)     // Catch:{ all -> 0x0049 }
                r3.g = r6
                goto L_0x004c
            L_0x0049:
                r4 = move-exception
                monitor-exit(r3)     // Catch:{ all -> 0x0049 }
                throw r4
            L_0x004c:
                boolean r5 = r3.h
                if (r5 != 0) goto L_0x0055
                l.b.Observer<? super T> r5 = r3.b
                r5.b(r4)
            L_0x0055:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: j.e.b.BehaviorRelay.a.a(java.lang.Object, long):void");
        }

        public boolean a(T t2) {
            if (this.h) {
                return false;
            }
            this.b.b(t2);
            return false;
        }
    }
}
