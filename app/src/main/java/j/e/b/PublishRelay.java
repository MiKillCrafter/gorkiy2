package j.e.b;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import l.b.Observer;
import l.b.s.b;

public final class PublishRelay<T> extends Relay<T> {
    public static final a[] c = new a[0];
    public final AtomicReference<a<T>[]> b = new AtomicReference<>(c);

    public static final class a<T> extends AtomicBoolean implements b {
        public final Observer<? super T> b;
        public final PublishRelay<T> c;

        public a(Observer<? super T> observer, PublishRelay<T> publishRelay) {
            this.b = observer;
            this.c = publishRelay;
        }

        public void f() {
            if (compareAndSet(false, true)) {
                this.c.a((a<e.c.c.b>) this);
            }
        }

        public boolean g() {
            return get();
        }
    }

    public void a(a<e.c.c.b> aVar) {
        a<e.c.c.b>[] aVarArr;
        a[] aVarArr2;
        do {
            aVarArr = (a[]) this.b.get();
            if (aVarArr != c) {
                int length = aVarArr.length;
                int i2 = -1;
                int i3 = 0;
                while (true) {
                    if (i3 >= length) {
                        break;
                    } else if (aVarArr[i3] == aVar) {
                        i2 = i3;
                        break;
                    } else {
                        i3++;
                    }
                }
                if (i2 >= 0) {
                    if (length == 1) {
                        aVarArr2 = c;
                    } else {
                        a[] aVarArr3 = new a[(length - 1)];
                        System.arraycopy(aVarArr, 0, aVarArr3, 0, i2);
                        System.arraycopy(aVarArr, i2 + 1, aVarArr3, i2, (length - i2) - 1);
                        aVarArr2 = aVarArr3;
                    }
                } else {
                    return;
                }
            } else {
                return;
            }
        } while (!this.b.compareAndSet(aVarArr, aVarArr2));
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v1, resolved type: j.e.b.PublishRelay$a[]} */
    /* JADX WARN: Type inference failed for: r0v0, types: [l.b.s.Disposable, java.util.concurrent.atomic.AtomicBoolean, j.e.b.PublishRelay$a] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b(l.b.Observer<? super T> r5) {
        /*
            r4 = this;
            j.e.b.PublishRelay$a r0 = new j.e.b.PublishRelay$a
            r0.<init>(r5, r4)
            r5.a(r0)
        L_0x0008:
            java.util.concurrent.atomic.AtomicReference<j.e.b.PublishRelay$a<T>[]> r5 = r4.b
            java.lang.Object r5 = r5.get()
            j.e.b.PublishRelay$a[] r5 = (j.e.b.PublishRelay.a[]) r5
            int r1 = r5.length
            int r2 = r1 + 1
            j.e.b.PublishRelay$a[] r2 = new j.e.b.PublishRelay.a[r2]
            r3 = 0
            java.lang.System.arraycopy(r5, r3, r2, r3, r1)
            r2[r1] = r0
            java.util.concurrent.atomic.AtomicReference<j.e.b.PublishRelay$a<T>[]> r1 = r4.b
            boolean r5 = r1.compareAndSet(r5, r2)
            if (r5 == 0) goto L_0x0008
            boolean r5 = r0.get()
            if (r5 == 0) goto L_0x002c
            r4.a(r0)
        L_0x002c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: j.e.b.PublishRelay.b(l.b.Observer):void");
    }

    public void a(e.c.c.b bVar) {
        if (bVar != null) {
            for (a aVar : (a[]) this.b.get()) {
                if (!aVar.get()) {
                    aVar.b.b(bVar);
                }
            }
            return;
        }
        throw new NullPointerException("value == null");
    }
}
