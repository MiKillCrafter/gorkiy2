package j.b.a.k;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

public class StrictLineReader implements Closeable {
    public final InputStream b;
    public final Charset c;
    public byte[] d;

    /* renamed from: e  reason: collision with root package name */
    public int f1569e;

    /* renamed from: f  reason: collision with root package name */
    public int f1570f;

    public class a extends ByteArrayOutputStream {
        public a(int i2) {
            super(i2);
        }

        public String toString() {
            int i2 = super.count;
            try {
                return new String(super.buf, 0, (i2 <= 0 || super.buf[i2 + -1] != 13) ? super.count : i2 - 1, StrictLineReader.this.c.name());
            } catch (UnsupportedEncodingException e2) {
                throw new AssertionError(e2);
            }
        }
    }

    public StrictLineReader(InputStream inputStream, Charset charset) {
        if (inputStream == null || charset == null) {
            throw null;
        } else if (charset.equals(Util.a)) {
            this.b = inputStream;
            this.c = charset;
            this.d = new byte[8192];
        } else {
            throw new IllegalArgumentException("Unsupported encoding");
        }
    }

    public final void a() {
        InputStream inputStream = this.b;
        byte[] bArr = this.d;
        int read = inputStream.read(bArr, 0, bArr.length);
        if (read != -1) {
            this.f1569e = 0;
            this.f1570f = read;
            return;
        }
        throw new EOFException();
    }

    public void close() {
        synchronized (this.b) {
            if (this.d != null) {
                this.d = null;
                this.b.close();
            }
        }
    }

    public String f() {
        int i2;
        int i3;
        synchronized (this.b) {
            if (this.d != null) {
                if (this.f1569e >= this.f1570f) {
                    a();
                }
                for (int i4 = this.f1569e; i4 != this.f1570f; i4++) {
                    if (this.d[i4] == 10) {
                        if (i4 != this.f1569e) {
                            i3 = i4 - 1;
                            if (this.d[i3] == 13) {
                                String str = new String(this.d, this.f1569e, i3 - this.f1569e, this.c.name());
                                this.f1569e = i4 + 1;
                                return str;
                            }
                        }
                        i3 = i4;
                        String str2 = new String(this.d, this.f1569e, i3 - this.f1569e, this.c.name());
                        this.f1569e = i4 + 1;
                        return str2;
                    }
                }
                a aVar = new a((this.f1570f - this.f1569e) + 80);
                loop1:
                while (true) {
                    aVar.write(this.d, this.f1569e, this.f1570f - this.f1569e);
                    this.f1570f = -1;
                    a();
                    i2 = this.f1569e;
                    while (true) {
                        if (i2 != this.f1570f) {
                            if (this.d[i2] == 10) {
                                break loop1;
                            }
                            i2++;
                        }
                    }
                }
                if (i2 != this.f1569e) {
                    aVar.write(this.d, this.f1569e, i2 - this.f1569e);
                }
                this.f1569e = i2 + 1;
                String aVar2 = aVar.toString();
                return aVar2;
            }
            throw new IOException("LineReader is closed");
        }
    }
}
