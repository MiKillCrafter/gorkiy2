package j.b.a.n;

import j.a.a.a.outline;
import j.b.a.q.Request;
import j.b.a.q.b;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;

public class RequestTracker {
    public final Set<b> a = Collections.newSetFromMap(new WeakHashMap());
    public final List<b> b = new ArrayList();
    public boolean c;

    public boolean a(Request request) {
        boolean z = true;
        if (request == null) {
            return true;
        }
        boolean remove = this.a.remove(request);
        if (!this.b.remove(request) && !remove) {
            z = false;
        }
        if (z) {
            request.clear();
        }
        return z;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append("{numRequests=");
        sb.append(this.a.size());
        sb.append(", isPaused=");
        return outline.a(sb, this.c, "}");
    }
}
