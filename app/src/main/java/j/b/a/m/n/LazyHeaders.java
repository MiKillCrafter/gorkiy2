package j.b.a.m.n;

import android.text.TextUtils;
import j.a.a.a.outline;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import l.a.a.a.o.b.AbstractSpiCall;

public final class LazyHeaders implements Headers {
    public final Map<String, List<i>> b;
    public volatile Map<String, String> c;

    public static final class a {
        public static final String d;

        /* renamed from: e  reason: collision with root package name */
        public static final Map<String, List<i>> f1690e;
        public boolean a = true;
        public Map<String, List<i>> b = f1690e;
        public boolean c = true;

        static {
            String property = System.getProperty("http.agent");
            if (!TextUtils.isEmpty(property)) {
                int length = property.length();
                StringBuilder sb = new StringBuilder(property.length());
                for (int i2 = 0; i2 < length; i2++) {
                    char charAt = property.charAt(i2);
                    if ((charAt > 31 || charAt == 9) && charAt < 127) {
                        sb.append(charAt);
                    } else {
                        sb.append('?');
                    }
                }
                property = sb.toString();
            }
            d = property;
            HashMap hashMap = new HashMap(2);
            if (!TextUtils.isEmpty(d)) {
                hashMap.put(AbstractSpiCall.HEADER_USER_AGENT, Collections.singletonList(new b(d)));
            }
            f1690e = Collections.unmodifiableMap(hashMap);
        }

        public final void a() {
            if (this.a) {
                this.a = false;
                HashMap hashMap = new HashMap(this.b.size());
                for (Map.Entry next : this.b.entrySet()) {
                    hashMap.put(next.getKey(), new ArrayList((Collection) next.getValue()));
                }
                this.b = hashMap;
            }
        }
    }

    public static final class b implements LazyHeaderFactory {
        public final String a;

        public b(String str) {
            this.a = str;
        }

        public String a() {
            return this.a;
        }

        public boolean equals(Object obj) {
            if (obj instanceof b) {
                return this.a.equals(((b) obj).a);
            }
            return false;
        }

        public int hashCode() {
            return this.a.hashCode();
        }

        public String toString() {
            StringBuilder a2 = outline.a("StringHeaderFactory{value='");
            a2.append(this.a);
            a2.append('\'');
            a2.append('}');
            return a2.toString();
        }
    }

    public LazyHeaders(Map<String, List<i>> map) {
        this.b = Collections.unmodifiableMap(map);
    }

    public Map<String, String> a() {
        if (this.c == null) {
            synchronized (this) {
                if (this.c == null) {
                    this.c = Collections.unmodifiableMap(b());
                }
            }
        }
        return this.c;
    }

    public final Map<String, String> b() {
        HashMap hashMap = new HashMap();
        for (Map.Entry next : this.b.entrySet()) {
            List list = (List) next.getValue();
            StringBuilder sb = new StringBuilder();
            int size = list.size();
            for (int i2 = 0; i2 < size; i2++) {
                String a2 = ((LazyHeaderFactory) list.get(i2)).a();
                if (!TextUtils.isEmpty(a2)) {
                    sb.append(a2);
                    if (i2 != list.size() - 1) {
                        sb.append(',');
                    }
                }
            }
            String sb2 = sb.toString();
            if (!TextUtils.isEmpty(sb2)) {
                hashMap.put(next.getKey(), sb2);
            }
        }
        return hashMap;
    }

    public boolean equals(Object obj) {
        if (obj instanceof LazyHeaders) {
            return this.b.equals(((LazyHeaders) obj).b);
        }
        return false;
    }

    public int hashCode() {
        return this.b.hashCode();
    }

    public String toString() {
        StringBuilder a2 = outline.a("LazyHeaders{headers=");
        a2.append(this.b);
        a2.append('}');
        return a2.toString();
    }
}
