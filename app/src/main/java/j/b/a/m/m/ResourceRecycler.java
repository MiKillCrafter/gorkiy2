package j.b.a.m.m;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

public class ResourceRecycler {
    public boolean a;
    public final Handler b = new Handler(Looper.getMainLooper(), new a());

    public static final class a implements Handler.Callback {
        public boolean handleMessage(Message message) {
            if (message.what != 1) {
                return false;
            }
            ((Resource) message.obj).d();
            return true;
        }
    }

    public synchronized void a(Resource<?> resource, boolean z) {
        if (!this.a) {
            if (!z) {
                this.a = true;
                resource.d();
                this.a = false;
            }
        }
        this.b.obtainMessage(1, resource).sendToTarget();
    }
}
