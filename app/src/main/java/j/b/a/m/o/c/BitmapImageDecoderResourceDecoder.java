package j.b.a.m.o.c;

import android.graphics.Bitmap;
import j.b.a.m.m.b0.BitmapPool;
import j.b.a.m.m.b0.BitmapPoolAdapter;
import j.b.a.m.o.ImageDecoderResourceDecoder;

public final class BitmapImageDecoderResourceDecoder extends ImageDecoderResourceDecoder<Bitmap> {
    public final BitmapPool b = new BitmapPoolAdapter();
}
