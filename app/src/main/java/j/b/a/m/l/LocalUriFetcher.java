package j.b.a.m.l;

import android.content.ContentResolver;
import android.net.Uri;
import android.util.Log;
import j.b.a.f;
import j.b.a.m.DataSource;
import j.b.a.m.l.DataFetcher;
import java.io.FileNotFoundException;
import java.io.IOException;

public abstract class LocalUriFetcher<T> implements DataFetcher<T> {
    public final Uri b;
    public final ContentResolver c;
    public T d;

    public LocalUriFetcher(ContentResolver contentResolver, Uri uri) {
        this.c = contentResolver;
        this.b = uri;
    }

    public abstract T a(Uri uri, ContentResolver contentResolver);

    public final void a(f fVar, DataFetcher.a<? super T> aVar) {
        try {
            T a = a(this.b, this.c);
            this.d = a;
            aVar.a((Object) a);
        } catch (FileNotFoundException e2) {
            if (Log.isLoggable("LocalUriFetcher", 3)) {
                Log.d("LocalUriFetcher", "Failed to open Uri", e2);
            }
            aVar.a((Exception) e2);
        }
    }

    public abstract void a(T t2);

    public void b() {
        T t2 = this.d;
        if (t2 != null) {
            try {
                a(t2);
            } catch (IOException unused) {
            }
        }
    }

    public DataSource c() {
        return DataSource.LOCAL;
    }

    public void cancel() {
    }
}
