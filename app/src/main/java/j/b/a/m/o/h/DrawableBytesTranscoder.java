package j.b.a.m.o.h;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import j.b.a.m.g;
import j.b.a.m.m.Resource;
import j.b.a.m.m.b0.BitmapPool;
import j.b.a.m.m.b0.d;
import j.b.a.m.o.c.BitmapResource;
import j.b.a.m.o.g.GifDrawable;
import j.b.a.m.o.g.c;

public final class DrawableBytesTranscoder implements ResourceTranscoder<Drawable, byte[]> {
    public final BitmapPool a;
    public final ResourceTranscoder<Bitmap, byte[]> b;
    public final ResourceTranscoder<c, byte[]> c;

    public DrawableBytesTranscoder(d dVar, ResourceTranscoder<Bitmap, byte[]> resourceTranscoder, ResourceTranscoder<c, byte[]> resourceTranscoder2) {
        this.a = dVar;
        this.b = resourceTranscoder;
        this.c = resourceTranscoder2;
    }

    public Resource<byte[]> a(Resource<Drawable> resource, g gVar) {
        Drawable drawable = resource.get();
        if (drawable instanceof BitmapDrawable) {
            return this.b.a(BitmapResource.a(((BitmapDrawable) drawable).getBitmap(), this.a), gVar);
        }
        if (drawable instanceof GifDrawable) {
            return this.c.a(resource, gVar);
        }
        return null;
    }
}
