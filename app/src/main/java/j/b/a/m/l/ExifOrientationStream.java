package j.b.a.m.l;

import j.a.a.a.outline;
import java.io.FilterInputStream;
import java.io.InputStream;

public final class ExifOrientationStream extends FilterInputStream {
    public static final byte[] d;

    /* renamed from: e  reason: collision with root package name */
    public static final int f1599e;

    /* renamed from: f  reason: collision with root package name */
    public static final int f1600f;
    public final byte b;
    public int c;

    static {
        byte[] bArr = {-1, -31, 0, 28, 69, 120, 105, 102, 0, 0, 77, 77, 0, 0, 0, 0, 0, 8, 0, 1, 1, 18, 0, 2, 0, 0, 0, 1, 0};
        d = bArr;
        int length = bArr.length;
        f1599e = length;
        f1600f = length + 2;
    }

    public ExifOrientationStream(InputStream inputStream, int i2) {
        super(inputStream);
        if (i2 < -1 || i2 > 8) {
            throw new IllegalArgumentException(outline.b("Cannot add invalid orientation: ", i2));
        }
        this.b = (byte) i2;
    }

    public void mark(int i2) {
        throw new UnsupportedOperationException();
    }

    public boolean markSupported() {
        return false;
    }

    public int read() {
        int i2;
        int i3;
        int i4 = this.c;
        if (i4 < 2 || i4 > (i3 = f1600f)) {
            i2 = super.read();
        } else if (i4 == i3) {
            i2 = this.b;
        } else {
            i2 = d[i4 - 2] & 255;
        }
        if (i2 != -1) {
            this.c++;
        }
        return i2;
    }

    public void reset() {
        throw new UnsupportedOperationException();
    }

    public long skip(long j2) {
        long skip = super.skip(j2);
        if (skip > 0) {
            this.c = (int) (((long) this.c) + skip);
        }
        return skip;
    }

    public int read(byte[] bArr, int i2, int i3) {
        int i4;
        int i5 = this.c;
        int i6 = f1600f;
        if (i5 > i6) {
            i4 = super.read(bArr, i2, i3);
        } else if (i5 == i6) {
            bArr[i2] = this.b;
            i4 = 1;
        } else if (i5 < 2) {
            i4 = super.read(bArr, i2, 2 - i5);
        } else {
            int min = Math.min(i6 - i5, i3);
            System.arraycopy(d, this.c - 2, bArr, i2, min);
            i4 = min;
        }
        if (i4 > 0) {
            this.c += i4;
        }
        return i4;
    }
}
