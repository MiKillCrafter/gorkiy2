package j.b.a.m.l;

import j.b.a.m.l.DataRewinder;
import j.b.a.m.m.b0.ArrayPool;
import j.b.a.m.o.c.RecyclableBufferedInputStream;
import java.io.InputStream;

public final class InputStreamRewinder implements DataRewinder<InputStream> {
    public final RecyclableBufferedInputStream a;

    public InputStreamRewinder(InputStream inputStream, ArrayPool arrayPool) {
        RecyclableBufferedInputStream recyclableBufferedInputStream = new RecyclableBufferedInputStream(inputStream, arrayPool);
        this.a = recyclableBufferedInputStream;
        recyclableBufferedInputStream.mark(5242880);
    }

    public void b() {
        this.a.f();
    }

    public static final class a implements DataRewinder.a<InputStream> {
        public final ArrayPool a;

        public a(ArrayPool arrayPool) {
            this.a = arrayPool;
        }

        public DataRewinder a(Object obj) {
            return new InputStreamRewinder((InputStream) obj, this.a);
        }

        public Class<InputStream> a() {
            return InputStream.class;
        }
    }

    public InputStream a() {
        this.a.reset();
        return this.a;
    }
}
