package j.b.a.m.o.c;

import android.graphics.Bitmap;
import j.b.a.m.Options;
import j.b.a.m.ResourceDecoder;
import j.b.a.m.g;
import j.b.a.m.m.Resource;
import j.b.a.m.o.c.l;
import j.b.a.s.ByteBufferUtil;
import java.nio.ByteBuffer;

public class ByteBufferBitmapDecoder implements ResourceDecoder<ByteBuffer, Bitmap> {
    public final Downsampler a;

    public ByteBufferBitmapDecoder(Downsampler downsampler) {
        this.a = downsampler;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.b.a.m.o.c.Downsampler.a(java.io.InputStream, int, int, j.b.a.m.g, j.b.a.m.o.c.l$b):j.b.a.m.m.Resource<android.graphics.Bitmap>
     arg types: [java.io.InputStream, int, int, j.b.a.m.Options, j.b.a.m.o.c.Downsampler$b]
     candidates:
      j.b.a.m.o.c.Downsampler.a(java.lang.IllegalArgumentException, int, int, java.lang.String, android.graphics.BitmapFactory$Options):java.io.IOException
      j.b.a.m.o.c.Downsampler.a(j.b.a.m.o.c.r, int, int, j.b.a.m.g, j.b.a.m.o.c.l$b):j.b.a.m.m.Resource<android.graphics.Bitmap>
      j.b.a.m.o.c.Downsampler.a(java.io.InputStream, int, int, j.b.a.m.g, j.b.a.m.o.c.l$b):j.b.a.m.m.Resource<android.graphics.Bitmap> */
    public Resource a(Object obj, int i2, int i3, Options options) {
        return this.a.a(ByteBufferUtil.b((ByteBuffer) obj), i2, i3, (g) options, (l.b) Downsampler.f1708k);
    }

    public boolean a(Object obj, Options options) {
        ByteBuffer byteBuffer = (ByteBuffer) obj;
        if (this.a != null) {
            return true;
        }
        throw null;
    }
}
