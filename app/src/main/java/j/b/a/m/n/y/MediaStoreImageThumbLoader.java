package j.b.a.m.n.y;

import android.content.Context;
import android.net.Uri;
import i.b.k.ResourcesFlusher;
import j.b.a.m.Options;
import j.b.a.m.l.p.ThumbFetcher;
import j.b.a.m.n.ModelLoader;
import j.b.a.m.n.ModelLoaderFactory;
import j.b.a.m.n.r;
import j.b.a.r.ObjectKey;
import java.io.InputStream;

public class MediaStoreImageThumbLoader implements ModelLoader<Uri, InputStream> {
    public final Context a;

    public static class a implements ModelLoaderFactory<Uri, InputStream> {
        public final Context a;

        public a(Context context) {
            this.a = context;
        }

        public ModelLoader<Uri, InputStream> a(r rVar) {
            return new MediaStoreImageThumbLoader(this.a);
        }
    }

    public MediaStoreImageThumbLoader(Context context) {
        this.a = context.getApplicationContext();
    }

    public ModelLoader.a a(Object obj, int i2, int i3, Options options) {
        Uri uri = (Uri) obj;
        if (!ResourcesFlusher.a(i2, i3)) {
            return null;
        }
        ObjectKey objectKey = new ObjectKey(uri);
        Context context = this.a;
        return new ModelLoader.a(objectKey, ThumbFetcher.a(context, uri, new ThumbFetcher.a(context.getContentResolver())));
    }

    public boolean a(Object obj) {
        Uri uri = (Uri) obj;
        return ResourcesFlusher.a(uri) && !uri.getPathSegments().contains("video");
    }
}
