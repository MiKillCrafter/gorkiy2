package j.b.a.m.o.h;

import j.b.a.m.g;
import j.b.a.m.m.Resource;
import j.b.a.m.o.d.BytesResource;
import j.b.a.m.o.g.c;
import j.b.a.s.ByteBufferUtil;

public class GifDrawableBytesTranscoder implements ResourceTranscoder<c, byte[]> {
    public Resource<byte[]> a(Resource<c> resource, g gVar) {
        return new BytesResource(ByteBufferUtil.a(resource.get().b.a.a.g().asReadOnlyBuffer()));
    }
}
