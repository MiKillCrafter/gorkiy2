package j.b.a.m.n;

import android.util.Log;
import j.b.a.f;
import j.b.a.m.DataSource;
import j.b.a.m.Options;
import j.b.a.m.l.DataFetcher;
import j.b.a.m.n.ModelLoader;
import j.b.a.r.ObjectKey;
import j.b.a.s.ByteBufferUtil;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

public class ByteBufferFileLoader implements ModelLoader<File, ByteBuffer> {

    public static class b implements ModelLoaderFactory<File, ByteBuffer> {
        public ModelLoader<File, ByteBuffer> a(r rVar) {
            return new ByteBufferFileLoader();
        }
    }

    public ModelLoader.a a(Object obj, int i2, int i3, Options options) {
        File file = (File) obj;
        return new ModelLoader.a(new ObjectKey(file), new a(file));
    }

    public boolean a(Object obj) {
        File file = (File) obj;
        return true;
    }

    public static final class a implements DataFetcher<ByteBuffer> {
        public final File b;

        public a(File file) {
            this.b = file;
        }

        public void a(f fVar, DataFetcher.a<? super ByteBuffer> aVar) {
            try {
                aVar.a(ByteBufferUtil.a(this.b));
            } catch (IOException e2) {
                if (Log.isLoggable("ByteBufferFileLoader", 3)) {
                    Log.d("ByteBufferFileLoader", "Failed to obtain ByteBuffer for file", e2);
                }
                aVar.a((Exception) e2);
            }
        }

        public void b() {
        }

        public DataSource c() {
            return DataSource.LOCAL;
        }

        public void cancel() {
        }

        public Class<ByteBuffer> a() {
            return ByteBuffer.class;
        }
    }
}
