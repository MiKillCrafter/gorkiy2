package j.b.a.m.o.c;

import android.os.Build;
import android.util.Log;
import java.io.File;

public final class HardwareConfigState {

    /* renamed from: f  reason: collision with root package name */
    public static final File f1712f = new File("/proc/self/fd");
    public static volatile HardwareConfigState g;
    public final boolean a;
    public final int b;
    public final int c;
    public int d;

    /* renamed from: e  reason: collision with root package name */
    public boolean f1713e = true;

    public HardwareConfigState() {
        boolean z = true;
        String str = Build.MODEL;
        if (str != null && str.length() >= 7) {
            String substring = Build.MODEL.substring(0, 7);
            char c2 = 65535;
            switch (substring.hashCode()) {
                case -1398613787:
                    if (substring.equals("SM-A520")) {
                        c2 = 6;
                        break;
                    }
                    break;
                case -1398431166:
                    if (substring.equals("SM-G930")) {
                        c2 = 5;
                        break;
                    }
                    break;
                case -1398431161:
                    if (substring.equals("SM-G935")) {
                        c2 = 4;
                        break;
                    }
                    break;
                case -1398431073:
                    if (substring.equals("SM-G960")) {
                        c2 = 2;
                        break;
                    }
                    break;
                case -1398431068:
                    if (substring.equals("SM-G965")) {
                        c2 = 3;
                        break;
                    }
                    break;
                case -1398343746:
                    if (substring.equals("SM-J720")) {
                        c2 = 1;
                        break;
                    }
                    break;
                case -1398222624:
                    if (substring.equals("SM-N935")) {
                        c2 = 0;
                        break;
                    }
                    break;
            }
            switch (c2) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                    if (Build.VERSION.SDK_INT == 26) {
                        z = false;
                        break;
                    }
                    break;
            }
        }
        this.a = z;
        if (Build.VERSION.SDK_INT >= 28) {
            this.b = 20000;
            this.c = 0;
            return;
        }
        this.b = 700;
        this.c = 128;
    }

    public static HardwareConfigState b() {
        if (g == null) {
            synchronized (HardwareConfigState.class) {
                if (g == null) {
                    g = new HardwareConfigState();
                }
            }
        }
        return g;
    }

    public boolean a(int i2, int i3, boolean z, boolean z2) {
        int i4;
        if (!z || !this.a || Build.VERSION.SDK_INT < 26 || z2 || i2 < (i4 = this.c) || i3 < i4 || !a()) {
            return false;
        }
        return true;
    }

    public final synchronized boolean a() {
        boolean z = true;
        int i2 = this.d + 1;
        this.d = i2;
        if (i2 >= 50) {
            this.d = 0;
            int length = f1712f.list().length;
            if (length >= this.b) {
                z = false;
            }
            this.f1713e = z;
            if (!z && Log.isLoggable("Downsampler", 5)) {
                Log.w("Downsampler", "Excluding HARDWARE bitmap config because we're over the file descriptor limit, file descriptors " + length + ", limit " + this.b);
            }
        }
        return this.f1713e;
    }
}
