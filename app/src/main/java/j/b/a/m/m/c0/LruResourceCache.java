package j.b.a.m.m.c0;

import j.b.a.m.Key;
import j.b.a.m.e;
import j.b.a.m.m.Engine;
import j.b.a.m.m.Resource;
import j.b.a.m.m.c0.MemoryCache;
import j.b.a.s.LruCache;

public class LruResourceCache extends LruCache<e, Resource<?>> implements i {
    public MemoryCache.a d;

    public LruResourceCache(long j2) {
        super(j2);
    }

    public void a(Object obj, Object obj2) {
        Key key = (Key) obj;
        Resource resource = (Resource) obj2;
        MemoryCache.a aVar = this.d;
        if (aVar != null && resource != null) {
            ((Engine) aVar).f1638e.a(resource, true);
        }
    }

    public int b(Object obj) {
        Resource resource = (Resource) obj;
        if (resource == null) {
            return 1;
        }
        return resource.b();
    }

    public /* bridge */ /* synthetic */ Resource a(Key key, Resource resource) {
        return (Resource) super.b(key, resource);
    }

    public /* bridge */ /* synthetic */ Resource a(Key key) {
        return (Resource) super.c(key);
    }
}
