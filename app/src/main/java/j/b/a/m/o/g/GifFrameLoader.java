package j.b.a.m.o.g;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import i.b.k.ResourcesFlusher;
import j.b.a.Glide;
import j.b.a.RequestBuilder;
import j.b.a.RequestManager;
import j.b.a.l.GifDecoder;
import j.b.a.m.Transformation;
import j.b.a.m.e;
import j.b.a.m.m.DiskCacheStrategy;
import j.b.a.m.m.b0.BitmapPool;
import j.b.a.m.m.k;
import j.b.a.m.o.g.g;
import j.b.a.q.BaseRequestOptions;
import j.b.a.q.RequestOptions;
import j.b.a.q.h.CustomTarget;
import j.b.a.q.i.Transition;
import j.b.a.r.ObjectKey;
import j.b.a.s.Executors;
import j.b.a.s.Util;
import java.util.ArrayList;
import java.util.List;

public class GifFrameLoader {
    public final GifDecoder a;
    public final Handler b;
    public final List<g.b> c;
    public final RequestManager d;

    /* renamed from: e  reason: collision with root package name */
    public final BitmapPool f1725e;

    /* renamed from: f  reason: collision with root package name */
    public boolean f1726f;
    public boolean g;
    public boolean h;

    /* renamed from: i  reason: collision with root package name */
    public RequestBuilder<Bitmap> f1727i;

    /* renamed from: j  reason: collision with root package name */
    public a f1728j;

    /* renamed from: k  reason: collision with root package name */
    public boolean f1729k;

    /* renamed from: l  reason: collision with root package name */
    public a f1730l;

    /* renamed from: m  reason: collision with root package name */
    public Bitmap f1731m;

    /* renamed from: n  reason: collision with root package name */
    public a f1732n;

    /* renamed from: o  reason: collision with root package name */
    public int f1733o;

    /* renamed from: p  reason: collision with root package name */
    public int f1734p;

    /* renamed from: q  reason: collision with root package name */
    public int f1735q;

    public static class a extends CustomTarget<Bitmap> {

        /* renamed from: e  reason: collision with root package name */
        public final Handler f1736e;

        /* renamed from: f  reason: collision with root package name */
        public final int f1737f;
        public final long g;
        public Bitmap h;

        public a(Handler handler, int i2, long j2) {
            this.f1736e = handler;
            this.f1737f = i2;
            this.g = j2;
        }

        public void a(Object obj, Transition transition) {
            this.h = (Bitmap) obj;
            this.f1736e.sendMessageAtTime(this.f1736e.obtainMessage(1, this), this.g);
        }

        public void c(Drawable drawable) {
            this.h = null;
        }
    }

    public interface b {
        void a();
    }

    public class c implements Handler.Callback {
        public c() {
        }

        public boolean handleMessage(Message message) {
            int i2 = message.what;
            if (i2 == 1) {
                GifFrameLoader.this.a((a) message.obj);
                return true;
            } else if (i2 != 2) {
                return false;
            } else {
                GifFrameLoader.this.d.a((a) message.obj);
                return false;
            }
        }
    }

    public GifFrameLoader(j.b.a.b bVar, j.b.a.l.a aVar, int i2, int i3, Transformation<Bitmap> transformation, Bitmap bitmap) {
        BitmapPool bitmapPool = bVar.b;
        RequestManager b2 = Glide.b(bVar.d.getBaseContext());
        RequestManager b3 = Glide.b(bVar.d.getBaseContext());
        if (b3 != null) {
            RequestBuilder<Bitmap> a2 = new RequestBuilder(b3.b, b3, Bitmap.class, b3.c).a((BaseRequestOptions<?>) RequestManager.f1551m).a((BaseRequestOptions<?>) ((RequestOptions) ((RequestOptions) ((RequestOptions) new RequestOptions().a((k) DiskCacheStrategy.a)).b(true)).a(true)).a(i2, i3));
            this.c = new ArrayList();
            this.d = b2;
            Handler handler = new Handler(Looper.getMainLooper(), new c());
            this.f1725e = bitmapPool;
            this.b = handler;
            this.f1727i = a2;
            this.a = aVar;
            a(transformation, bitmap);
            return;
        }
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
     arg types: [j.b.a.m.Transformation<android.graphics.Bitmap>, java.lang.String]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
      i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
      i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
      i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
      i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
      i.b.k.ResourcesFlusher.a(int, int):boolean
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
      i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
      i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
     arg types: [android.graphics.Bitmap, java.lang.String]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
      i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
      i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
      i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
      i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
      i.b.k.ResourcesFlusher.a(int, int):boolean
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
      i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
      i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.b.a.q.BaseRequestOptions.a(j.b.a.m.Transformation<android.graphics.Bitmap>, boolean):T
     arg types: [j.b.a.m.Transformation<android.graphics.Bitmap>, int]
     candidates:
      j.b.a.q.BaseRequestOptions.a(int, int):T
      j.b.a.q.BaseRequestOptions.a(j.b.a.m.Option, java.lang.Object):T
      j.b.a.q.BaseRequestOptions.a(j.b.a.m.o.c.k, j.b.a.m.Transformation<android.graphics.Bitmap>):T
      j.b.a.q.BaseRequestOptions.a(j.b.a.m.Transformation<android.graphics.Bitmap>, boolean):T */
    public void a(Transformation<Bitmap> transformation, Bitmap bitmap) {
        ResourcesFlusher.a((Object) transformation, "Argument must not be null");
        ResourcesFlusher.a((Object) bitmap, "Argument must not be null");
        this.f1731m = bitmap;
        this.f1727i = this.f1727i.a((BaseRequestOptions<?>) new RequestOptions().a(transformation, true));
        this.f1733o = Util.a(bitmap);
        this.f1734p = bitmap.getWidth();
        this.f1735q = bitmap.getHeight();
    }

    public final void a() {
        if (this.f1726f && !this.g) {
            if (this.h) {
                ResourcesFlusher.a(this.f1732n == null, "Pending target must be null when starting from the first frame");
                this.a.h();
                this.h = false;
            }
            a aVar = this.f1732n;
            if (aVar != null) {
                this.f1732n = null;
                a(aVar);
                return;
            }
            this.g = true;
            long uptimeMillis = SystemClock.uptimeMillis() + ((long) this.a.f());
            this.a.d();
            this.f1730l = new a(this.b, this.a.a(), uptimeMillis);
            RequestBuilder<Bitmap> a2 = this.f1727i.a((BaseRequestOptions<?>) ((RequestOptions) new RequestOptions().a((e) new ObjectKey(Double.valueOf(Math.random())))));
            a2.G = this.a;
            a2.J = true;
            a2.a(this.f1730l, null, a2, Executors.a);
        }
    }

    public void a(a aVar) {
        this.g = false;
        if (this.f1729k) {
            this.b.obtainMessage(2, aVar).sendToTarget();
        } else if (!this.f1726f) {
            this.f1732n = aVar;
        } else {
            if (aVar.h != null) {
                Bitmap bitmap = this.f1731m;
                if (bitmap != null) {
                    this.f1725e.a(bitmap);
                    this.f1731m = null;
                }
                a aVar2 = this.f1728j;
                this.f1728j = aVar;
                int size = this.c.size();
                while (true) {
                    size--;
                    if (size < 0) {
                        break;
                    }
                    this.c.get(size).a();
                }
                if (aVar2 != null) {
                    this.b.obtainMessage(2, aVar2).sendToTarget();
                }
            }
            a();
        }
    }
}
