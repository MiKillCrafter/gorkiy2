package j.b.a.m.o.c;

import android.media.MediaDataSource;
import j.b.a.m.o.c.VideoDecoder;
import java.nio.ByteBuffer;

/* compiled from: VideoDecoder */
public class VideoDecoder0 extends MediaDataSource {
    public final /* synthetic */ ByteBuffer b;

    public VideoDecoder0(VideoDecoder.d dVar, ByteBuffer byteBuffer) {
        this.b = byteBuffer;
    }

    public void close() {
    }

    public long getSize() {
        return (long) this.b.limit();
    }

    public int readAt(long j2, byte[] bArr, int i2, int i3) {
        if (j2 >= ((long) this.b.limit())) {
            return -1;
        }
        this.b.position((int) j2);
        int min = Math.min(i3, this.b.remaining());
        this.b.get(bArr, i2, min);
        return min;
    }
}
