package j.b.a.m.m;

import j.b.a.m.Encoder;
import j.b.a.m.Options;
import j.b.a.m.g;
import j.b.a.m.m.c0.a;

public class DataCacheWriter<DataType> implements a.b {
    public final Encoder<DataType> a;
    public final DataType b;
    public final Options c;

    public DataCacheWriter(Encoder<DataType> encoder, DataType datatype, g gVar) {
        this.a = encoder;
        this.b = datatype;
        this.c = gVar;
    }
}
