package j.b.a.m.n;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import i.b.k.ResourcesFlusher;
import j.a.a.a.outline;
import j.b.a.f;
import j.b.a.m.DataSource;
import j.b.a.m.Options;
import j.b.a.m.l.DataFetcher;
import j.b.a.m.n.ModelLoader;
import j.b.a.r.ObjectKey;
import java.io.File;
import java.io.FileNotFoundException;

public final class MediaStoreFileLoader implements ModelLoader<Uri, File> {
    public final Context a;

    public static final class a implements ModelLoaderFactory<Uri, File> {
        public final Context a;

        public a(Context context) {
            this.a = context;
        }

        public ModelLoader<Uri, File> a(r rVar) {
            return new MediaStoreFileLoader(this.a);
        }
    }

    public MediaStoreFileLoader(Context context) {
        this.a = context;
    }

    public ModelLoader.a a(Object obj, int i2, int i3, Options options) {
        Uri uri = (Uri) obj;
        return new ModelLoader.a(new ObjectKey(uri), new b(this.a, uri));
    }

    public boolean a(Object obj) {
        return ResourcesFlusher.a((Uri) obj);
    }

    public static class b implements DataFetcher<File> {
        public static final String[] d = {"_data"};
        public final Context b;
        public final Uri c;

        public b(Context context, Uri uri) {
            this.b = context;
            this.c = uri;
        }

        public void a(f fVar, DataFetcher.a<? super File> aVar) {
            Cursor query = this.b.getContentResolver().query(this.c, d, null, null, null);
            String str = null;
            if (query != null) {
                try {
                    if (query.moveToFirst()) {
                        str = query.getString(query.getColumnIndexOrThrow("_data"));
                    }
                } finally {
                    query.close();
                }
            }
            if (TextUtils.isEmpty(str)) {
                StringBuilder a = outline.a("Failed to find file path for: ");
                a.append(this.c);
                aVar.a((Exception) new FileNotFoundException(a.toString()));
                return;
            }
            aVar.a(new File(str));
        }

        public void b() {
        }

        public DataSource c() {
            return DataSource.LOCAL;
        }

        public void cancel() {
        }

        public Class<File> a() {
            return File.class;
        }
    }
}
