package j.b.a.m.n;

import android.net.Uri;
import j.b.a.m.Options;
import j.b.a.m.n.ModelLoader;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class UrlUriLoader<Data> implements ModelLoader<Uri, Data> {
    public static final Set<String> b = Collections.unmodifiableSet(new HashSet(Arrays.asList("http", "https")));
    public final ModelLoader<g, Data> a;

    public static class a implements ModelLoaderFactory<Uri, InputStream> {
        public ModelLoader<Uri, InputStream> a(r rVar) {
            return new UrlUriLoader(rVar.a(GlideUrl.class, InputStream.class));
        }
    }

    public UrlUriLoader(ModelLoader<g, Data> modelLoader) {
        this.a = modelLoader;
    }

    public ModelLoader.a a(Object obj, int i2, int i3, Options options) {
        return this.a.a(new GlideUrl(((Uri) obj).toString(), Headers.a), i2, i3, options);
    }

    public boolean a(Object obj) {
        return b.contains(((Uri) obj).getScheme());
    }
}
