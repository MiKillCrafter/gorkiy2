package j.b.a.m.m;

import j.b.a.m.DataSource;
import j.b.a.m.EncodeStrategy;

public abstract class DiskCacheStrategy {
    public static final DiskCacheStrategy a = new a();
    public static final DiskCacheStrategy b = new b();
    public static final DiskCacheStrategy c = new c();

    public class a extends DiskCacheStrategy {
        public boolean a() {
            return false;
        }

        public boolean a(DataSource dataSource) {
            return false;
        }

        public boolean a(boolean z, DataSource dataSource, EncodeStrategy encodeStrategy) {
            return false;
        }

        public boolean b() {
            return false;
        }
    }

    public class b extends DiskCacheStrategy {
        public boolean a() {
            return true;
        }

        public boolean a(DataSource dataSource) {
            return (dataSource == DataSource.DATA_DISK_CACHE || dataSource == DataSource.MEMORY_CACHE) ? false : true;
        }

        public boolean a(boolean z, DataSource dataSource, EncodeStrategy encodeStrategy) {
            return false;
        }

        public boolean b() {
            return false;
        }
    }

    public class c extends DiskCacheStrategy {
        public boolean a() {
            return true;
        }

        public boolean a(DataSource dataSource) {
            return dataSource == DataSource.REMOTE;
        }

        public boolean b() {
            return true;
        }

        public boolean a(boolean z, DataSource dataSource, EncodeStrategy encodeStrategy) {
            return ((z && dataSource == DataSource.DATA_DISK_CACHE) || dataSource == DataSource.LOCAL) && encodeStrategy == EncodeStrategy.TRANSFORMED;
        }
    }

    public abstract boolean a();

    public abstract boolean a(DataSource dataSource);

    public abstract boolean a(boolean z, DataSource dataSource, EncodeStrategy encodeStrategy);

    public abstract boolean b();
}
