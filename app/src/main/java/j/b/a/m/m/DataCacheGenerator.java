package j.b.a.m.m;

import j.b.a.m.DataSource;
import j.b.a.m.Key;
import j.b.a.m.e;
import j.b.a.m.l.DataFetcher;
import j.b.a.m.m.DataFetcherGenerator;
import j.b.a.m.m.g;
import j.b.a.m.n.ModelLoader;
import java.io.File;
import java.util.List;

public class DataCacheGenerator implements g, DataFetcher.a<Object> {
    public final List<e> b;
    public final DecodeHelper<?> c;
    public final DataFetcherGenerator.a d;

    /* renamed from: e  reason: collision with root package name */
    public int f1606e = -1;

    /* renamed from: f  reason: collision with root package name */
    public Key f1607f;
    public List<ModelLoader<File, ?>> g;
    public int h;

    /* renamed from: i  reason: collision with root package name */
    public volatile ModelLoader.a<?> f1608i;

    /* renamed from: j  reason: collision with root package name */
    public File f1609j;

    public DataCacheGenerator(DecodeHelper<?> decodeHelper, g.a aVar) {
        List<e> a = decodeHelper.a();
        this.b = a;
        this.c = decodeHelper;
        this.d = aVar;
    }

    public boolean a() {
        while (true) {
            List<ModelLoader<File, ?>> list = this.g;
            if (list != null) {
                if (this.h < list.size()) {
                    this.f1608i = null;
                    boolean z = false;
                    while (!z) {
                        if (!(this.h < this.g.size())) {
                            break;
                        }
                        List<ModelLoader<File, ?>> list2 = this.g;
                        int i2 = this.h;
                        this.h = i2 + 1;
                        File file = this.f1609j;
                        DecodeHelper<?> decodeHelper = this.c;
                        this.f1608i = list2.get(i2).a(file, decodeHelper.f1610e, decodeHelper.f1611f, decodeHelper.f1612i);
                        if (this.f1608i != null && this.c.c(this.f1608i.c.a())) {
                            this.f1608i.c.a(this.c.f1618o, this);
                            z = true;
                        }
                    }
                    return z;
                }
            }
            int i3 = this.f1606e + 1;
            this.f1606e = i3;
            if (i3 >= this.b.size()) {
                return false;
            }
            Key key = this.b.get(this.f1606e);
            File a = this.c.b().a(new DataCacheKey(key, this.c.f1617n));
            this.f1609j = a;
            if (a != null) {
                this.f1607f = key;
                this.g = this.c.c.b.a(a);
                this.h = 0;
            }
        }
    }

    public void cancel() {
        ModelLoader.a<?> aVar = this.f1608i;
        if (aVar != null) {
            aVar.c.cancel();
        }
    }

    public DataCacheGenerator(List<e> list, DecodeHelper<?> decodeHelper, g.a aVar) {
        this.b = list;
        this.c = decodeHelper;
        this.d = aVar;
    }

    public void a(Object obj) {
        this.d.a(this.f1607f, obj, this.f1608i.c, DataSource.DATA_DISK_CACHE, this.f1607f);
    }

    public void a(Exception exc) {
        this.d.a(this.f1607f, exc, this.f1608i.c, DataSource.DATA_DISK_CACHE);
    }
}
