package j.b.a;

public enum Priority {
    IMMEDIATE,
    HIGH,
    NORMAL,
    LOW
}
