package j.b.a.r;

import i.b.k.ResourcesFlusher;
import j.a.a.a.outline;
import j.b.a.m.Key;
import java.security.MessageDigest;

public final class ObjectKey implements Key {
    public final Object b;

    public ObjectKey(Object obj) {
        ResourcesFlusher.a(obj, "Argument must not be null");
        this.b = obj;
    }

    public void a(MessageDigest messageDigest) {
        messageDigest.update(this.b.toString().getBytes(Key.a));
    }

    public boolean equals(Object obj) {
        if (obj instanceof ObjectKey) {
            return this.b.equals(((ObjectKey) obj).b);
        }
        return false;
    }

    public int hashCode() {
        return this.b.hashCode();
    }

    public String toString() {
        StringBuilder a = outline.a("ObjectKey{object=");
        a.append(this.b);
        a.append('}');
        return a.toString();
    }
}
