package j.b.a.l;

import android.graphics.Bitmap;
import java.nio.ByteBuffer;

public interface GifDecoder {

    public interface a {
    }

    int a();

    void a(Bitmap.Config config);

    int b();

    Bitmap c();

    void clear();

    void d();

    int e();

    int f();

    ByteBuffer g();

    void h();
}
