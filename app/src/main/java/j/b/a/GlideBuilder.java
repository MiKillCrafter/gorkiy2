package j.b.a;

import i.e.ArrayMap;
import j.b.a.Glide;
import j.b.a.m.m.Engine;
import j.b.a.m.m.b0.ArrayPool;
import j.b.a.m.m.b0.BitmapPool;
import j.b.a.m.m.c0.DiskCache;
import j.b.a.m.m.c0.MemoryCache;
import j.b.a.m.m.c0.MemorySizeCalculator;
import j.b.a.m.m.d0.GlideExecutor;
import j.b.a.n.ConnectivityMonitorFactory;
import j.b.a.n.RequestManagerRetriever;
import j.b.a.q.RequestListener;
import java.util.List;
import java.util.Map;

public final class GlideBuilder {
    public final Map<Class<?>, TransitionOptions<?, ?>> a = new ArrayMap();
    public Engine b;
    public BitmapPool c;
    public ArrayPool d;

    /* renamed from: e  reason: collision with root package name */
    public MemoryCache f1537e;

    /* renamed from: f  reason: collision with root package name */
    public GlideExecutor f1538f;
    public GlideExecutor g;
    public DiskCache.a h;

    /* renamed from: i  reason: collision with root package name */
    public MemorySizeCalculator f1539i;

    /* renamed from: j  reason: collision with root package name */
    public ConnectivityMonitorFactory f1540j;

    /* renamed from: k  reason: collision with root package name */
    public int f1541k = 4;

    /* renamed from: l  reason: collision with root package name */
    public Glide.a f1542l = new a(this);

    /* renamed from: m  reason: collision with root package name */
    public RequestManagerRetriever.b f1543m;

    /* renamed from: n  reason: collision with root package name */
    public GlideExecutor f1544n;

    /* renamed from: o  reason: collision with root package name */
    public List<RequestListener<Object>> f1545o;

    public class a implements Glide.a {
        public a(GlideBuilder glideBuilder) {
        }
    }
}
