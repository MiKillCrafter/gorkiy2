package j.b.a.p;

import i.e.ArrayMap;
import j.b.a.m.m.DecodePath;
import j.b.a.m.m.LoadPath;
import j.b.a.m.o.h.UnitTranscoder;
import j.b.a.s.MultiClassKey;
import j.b.a.s.i;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicReference;

public class LoadPathCache {
    public static final LoadPath<?, ?, ?> c = new LoadPath(Object.class, Object.class, Object.class, Collections.singletonList(new DecodePath(Object.class, Object.class, Object.class, Collections.emptyList(), new UnitTranscoder(), null)), null);
    public final ArrayMap<i, LoadPath<?, ?, ?>> a = new ArrayMap<>();
    public final AtomicReference<i> b = new AtomicReference<>();

    public void a(Class<?> cls, Class<?> cls2, Class<?> cls3, LoadPath<?, ?, ?> loadPath) {
        synchronized (this.a) {
            ArrayMap<i, LoadPath<?, ?, ?>> arrayMap = this.a;
            MultiClassKey multiClassKey = new MultiClassKey(cls, cls2, cls3);
            if (loadPath == null) {
                loadPath = c;
            }
            arrayMap.put(multiClassKey, loadPath);
        }
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [java.lang.Class<Data>, java.lang.Class<?>] */
    /* JADX WARN: Type inference failed for: r4v0, types: [java.lang.Class<TResource>, java.lang.Class<?>] */
    /* JADX WARN: Type inference failed for: r5v0, types: [java.lang.Class<Transcode>, java.lang.Class<?>] */
    /* JADX WARNING: Unknown variable types count: 3 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <Data, TResource, Transcode> j.b.a.m.m.LoadPath<Data, TResource, Transcode> a(java.lang.Class<Data> r3, java.lang.Class<TResource> r4, java.lang.Class<Transcode> r5) {
        /*
            r2 = this;
            java.util.concurrent.atomic.AtomicReference<j.b.a.s.i> r0 = r2.b
            r1 = 0
            java.lang.Object r0 = r0.getAndSet(r1)
            j.b.a.s.MultiClassKey r0 = (j.b.a.s.MultiClassKey) r0
            if (r0 != 0) goto L_0x0010
            j.b.a.s.MultiClassKey r0 = new j.b.a.s.MultiClassKey
            r0.<init>()
        L_0x0010:
            r0.a = r3
            r0.b = r4
            r0.c = r5
            i.e.ArrayMap<j.b.a.s.i, j.b.a.m.m.LoadPath<?, ?, ?>> r3 = r2.a
            monitor-enter(r3)
            i.e.ArrayMap<j.b.a.s.i, j.b.a.m.m.LoadPath<?, ?, ?>> r4 = r2.a     // Catch:{ all -> 0x0028 }
            java.lang.Object r4 = r4.getOrDefault(r0, r1)     // Catch:{ all -> 0x0028 }
            j.b.a.m.m.LoadPath r4 = (j.b.a.m.m.LoadPath) r4     // Catch:{ all -> 0x0028 }
            monitor-exit(r3)     // Catch:{ all -> 0x0028 }
            java.util.concurrent.atomic.AtomicReference<j.b.a.s.i> r3 = r2.b
            r3.set(r0)
            return r4
        L_0x0028:
            r4 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0028 }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: j.b.a.p.LoadPathCache.a(java.lang.Class, java.lang.Class, java.lang.Class):j.b.a.m.m.LoadPath");
    }
}
