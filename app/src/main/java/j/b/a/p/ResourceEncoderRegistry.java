package j.b.a.p;

import j.b.a.m.ResourceEncoder;
import java.util.ArrayList;
import java.util.List;

public class ResourceEncoderRegistry {
    public final List<a<?>> a = new ArrayList();

    public static final class a<T> {
        public final Class<T> a;
        public final ResourceEncoder<T> b;

        public a(Class<T> cls, ResourceEncoder<T> resourceEncoder) {
            this.a = cls;
            this.b = resourceEncoder;
        }
    }

    public synchronized <Z> void a(Class<Z> cls, ResourceEncoder<Z> resourceEncoder) {
        this.a.add(new a(cls, resourceEncoder));
    }

    public synchronized <Z> ResourceEncoder<Z> a(Class<Z> cls) {
        int size = this.a.size();
        for (int i2 = 0; i2 < size; i2++) {
            a aVar = this.a.get(i2);
            if (aVar.a.isAssignableFrom(cls)) {
                return aVar.b;
            }
        }
        return null;
    }
}
