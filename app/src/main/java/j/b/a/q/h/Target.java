package j.b.a.q.h;

import android.graphics.drawable.Drawable;
import j.b.a.n.i;
import j.b.a.q.Request;
import j.b.a.q.i.Transition;

public interface Target<R> extends i {
    void a(Drawable drawable);

    void a(Request request);

    void a(SizeReadyCallback sizeReadyCallback);

    void a(R r2, Transition<? super R> transition);

    Request b();

    void b(Drawable drawable);

    void b(SizeReadyCallback sizeReadyCallback);

    void c(Drawable drawable);
}
