package j.b.a.q.h;

import android.content.Context;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import i.b.k.ResourcesFlusher;
import j.a.a.a.outline;
import j.b.a.g;
import j.b.a.q.Request;
import j.b.a.q.h.i;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Deprecated
public abstract class ViewTarget<T extends View, Z> extends BaseTarget<Z> {
    public static int d = g.glide_custom_view_target_tag;
    public final T b;
    public final a c;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
     arg types: [T, java.lang.String]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
      i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
      i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
      i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
      i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
      i.b.k.ResourcesFlusher.a(int, int):boolean
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
      i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
      i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T */
    public ViewTarget(T t2) {
        ResourcesFlusher.a((Object) t2, "Argument must not be null");
        this.b = t2;
        this.c = new a(t2);
    }

    public void a(SizeReadyCallback sizeReadyCallback) {
        this.c.b.remove(sizeReadyCallback);
    }

    public void b(SizeReadyCallback sizeReadyCallback) {
        a aVar = this.c;
        int c2 = aVar.c();
        int b2 = aVar.b();
        if (aVar.a(c2, b2)) {
            sizeReadyCallback.a(c2, b2);
            return;
        }
        if (!aVar.b.contains(sizeReadyCallback)) {
            aVar.b.add(sizeReadyCallback);
        }
        if (aVar.c == null) {
            ViewTreeObserver viewTreeObserver = aVar.a.getViewTreeObserver();
            a.C0017a aVar2 = new a.C0017a(aVar);
            aVar.c = aVar2;
            viewTreeObserver.addOnPreDrawListener(aVar2);
        }
    }

    public String toString() {
        StringBuilder a2 = outline.a("Target for: ");
        a2.append((Object) this.b);
        return a2.toString();
    }

    public void a(Request request) {
        this.b.setTag(d, request);
    }

    public static final class a {
        public static Integer d;
        public final View a;
        public final List<g> b = new ArrayList();
        public C0017a c;

        /* renamed from: j.b.a.q.h.ViewTarget$a$a  reason: collision with other inner class name */
        public static final class C0017a implements ViewTreeObserver.OnPreDrawListener {
            public final WeakReference<i.a> b;

            public C0017a(a aVar) {
                this.b = new WeakReference<>(aVar);
            }

            public boolean onPreDraw() {
                if (Log.isLoggable("ViewTarget", 2)) {
                    Log.v("ViewTarget", "OnGlobalLayoutListener called attachStateListener=" + this);
                }
                a aVar = this.b.get();
                if (aVar == null || aVar.b.isEmpty()) {
                    return true;
                }
                int c = aVar.c();
                int b2 = aVar.b();
                if (!aVar.a(c, b2)) {
                    return true;
                }
                Iterator it = new ArrayList(aVar.b).iterator();
                while (it.hasNext()) {
                    ((SizeReadyCallback) it.next()).a(c, b2);
                }
                aVar.a();
                return true;
            }
        }

        public a(View view) {
            this.a = view;
        }

        public void a() {
            ViewTreeObserver viewTreeObserver = this.a.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.removeOnPreDrawListener(this.c);
            }
            this.c = null;
            this.b.clear();
        }

        public final boolean a(int i2, int i3) {
            if (i2 > 0 || i2 == Integer.MIN_VALUE) {
                if (i3 > 0 || i3 == Integer.MIN_VALUE) {
                    return true;
                }
            }
            return false;
        }

        public final int b() {
            int paddingBottom = this.a.getPaddingBottom() + this.a.getPaddingTop();
            ViewGroup.LayoutParams layoutParams = this.a.getLayoutParams();
            return a(this.a.getHeight(), layoutParams != null ? layoutParams.height : 0, paddingBottom);
        }

        public final int c() {
            int paddingRight = this.a.getPaddingRight() + this.a.getPaddingLeft();
            ViewGroup.LayoutParams layoutParams = this.a.getLayoutParams();
            return a(this.a.getWidth(), layoutParams != null ? layoutParams.width : 0, paddingRight);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
         arg types: [android.view.WindowManager, java.lang.String]
         candidates:
          i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
          i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
          i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
          i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
          i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
          i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
          i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
          i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
          i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
          i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
          i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
          i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
          i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
          i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
          i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
          i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
          i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
          i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
          i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
          i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
          i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
          i.b.k.ResourcesFlusher.a(int, int):boolean
          i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
          i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
          i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
          i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
          i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T */
        public final int a(int i2, int i3, int i4) {
            int i5 = i3 - i4;
            if (i5 > 0) {
                return i5;
            }
            int i6 = i2 - i4;
            if (i6 > 0) {
                return i6;
            }
            if (this.a.isLayoutRequested() || i3 != -2) {
                return 0;
            }
            if (Log.isLoggable("ViewTarget", 4)) {
                Log.i("ViewTarget", "Glide treats LayoutParams.WRAP_CONTENT as a request for an image the size of this device's screen dimensions. If you want to load the original image and are ok with the corresponding memory cost and OOMs (depending on the input size), use override(Target.SIZE_ORIGINAL). Otherwise, use LayoutParams.MATCH_PARENT, set layout_width and layout_height to fixed dimension, or use .override() with fixed dimensions.");
            }
            Context context = this.a.getContext();
            if (d == null) {
                WindowManager windowManager = (WindowManager) context.getSystemService("window");
                ResourcesFlusher.a((Object) windowManager, "Argument must not be null");
                Display defaultDisplay = windowManager.getDefaultDisplay();
                Point point = new Point();
                defaultDisplay.getSize(point);
                d = Integer.valueOf(Math.max(point.x, point.y));
            }
            return d.intValue();
        }
    }

    public Request b() {
        Object tag = this.b.getTag(d);
        if (tag == null) {
            return null;
        }
        if (tag instanceof Request) {
            return (Request) tag;
        }
        throw new IllegalArgumentException("You must not call setTag() on a view Glide is targeting");
    }
}
