package j.b.a.q;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.Log;
import com.bumptech.glide.load.engine.GlideException;
import j.a.a.a.outline;
import j.b.a.GlideContext;
import j.b.a.Priority;
import j.b.a.d;
import j.b.a.f;
import j.b.a.m.m.Engine;
import j.b.a.m.m.Resource;
import j.b.a.m.m.l;
import j.b.a.m.o.e.DrawableDecoderCompat;
import j.b.a.q.h.Target;
import j.b.a.q.h.g;
import j.b.a.q.i.TransitionFactory;
import j.b.a.s.LogTime;
import j.b.a.s.Util;
import j.b.a.s.k.StateVerifier;
import java.util.List;
import java.util.concurrent.Executor;

public final class SingleRequest<R> implements b, g, f {
    public static final boolean D = Log.isLoggable("Request", 2);
    public int A;
    public boolean B;
    public RuntimeException C;
    public final String a;
    public final StateVerifier b;
    public final Object c;
    public final RequestListener<R> d;

    /* renamed from: e  reason: collision with root package name */
    public final RequestCoordinator f1758e;

    /* renamed from: f  reason: collision with root package name */
    public final Context f1759f;
    public final GlideContext g;
    public final Object h;

    /* renamed from: i  reason: collision with root package name */
    public final Class<R> f1760i;

    /* renamed from: j  reason: collision with root package name */
    public final BaseRequestOptions<?> f1761j;

    /* renamed from: k  reason: collision with root package name */
    public final int f1762k;

    /* renamed from: l  reason: collision with root package name */
    public final int f1763l;

    /* renamed from: m  reason: collision with root package name */
    public final Priority f1764m;

    /* renamed from: n  reason: collision with root package name */
    public final Target<R> f1765n;

    /* renamed from: o  reason: collision with root package name */
    public final List<RequestListener<R>> f1766o;

    /* renamed from: p  reason: collision with root package name */
    public final TransitionFactory<? super R> f1767p;

    /* renamed from: q  reason: collision with root package name */
    public final Executor f1768q;

    /* renamed from: r  reason: collision with root package name */
    public Resource<R> f1769r;

    /* renamed from: s  reason: collision with root package name */
    public Engine.d f1770s;

    /* renamed from: t  reason: collision with root package name */
    public long f1771t;
    public volatile Engine u;
    public a v;
    public Drawable w;
    public Drawable x;
    public Drawable y;
    public int z;

    public enum a {
        PENDING,
        RUNNING,
        WAITING_FOR_SIZE,
        COMPLETE,
        FAILED,
        CLEARED
    }

    public SingleRequest(Context context, d dVar, Object obj, Object obj2, Class<R> cls, BaseRequestOptions<?> baseRequestOptions, int i2, int i3, f fVar, Target<R> target, RequestListener<R> requestListener, List<RequestListener<R>> list, c cVar, l lVar, TransitionFactory<? super R> transitionFactory, Executor executor) {
        d dVar2 = dVar;
        this.a = D ? String.valueOf(super.hashCode()) : null;
        this.b = new StateVerifier.b();
        this.c = obj;
        this.f1759f = context;
        this.g = dVar2;
        this.h = obj2;
        this.f1760i = cls;
        this.f1761j = baseRequestOptions;
        this.f1762k = i2;
        this.f1763l = i3;
        this.f1764m = fVar;
        this.f1765n = target;
        this.d = requestListener;
        this.f1766o = list;
        this.f1758e = cVar;
        this.u = lVar;
        this.f1767p = transitionFactory;
        this.f1768q = executor;
        this.v = a.PENDING;
        if (this.C == null && dVar2.h) {
            this.C = new RuntimeException("Glide request origin trace");
        }
    }

    public void a() {
        synchronized (this.c) {
            if (isRunning()) {
                clear();
            }
        }
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [j.b.a.q.Request, j.b.a.q.SingleRequest, j.b.a.q.h.SizeReadyCallback] */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00b0, code lost:
        return;
     */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0088  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b() {
        /*
            r4 = this;
            java.lang.Object r0 = r4.c
            monitor-enter(r0)
            r4.e()     // Catch:{ all -> 0x00b9 }
            j.b.a.s.k.StateVerifier r1 = r4.b     // Catch:{ all -> 0x00b9 }
            r1.a()     // Catch:{ all -> 0x00b9 }
            long r1 = j.b.a.s.LogTime.a()     // Catch:{ all -> 0x00b9 }
            r4.f1771t = r1     // Catch:{ all -> 0x00b9 }
            java.lang.Object r1 = r4.h     // Catch:{ all -> 0x00b9 }
            if (r1 != 0) goto L_0x003c
            int r1 = r4.f1762k     // Catch:{ all -> 0x00b9 }
            int r2 = r4.f1763l     // Catch:{ all -> 0x00b9 }
            boolean r1 = j.b.a.s.Util.a(r1, r2)     // Catch:{ all -> 0x00b9 }
            if (r1 == 0) goto L_0x0027
            int r1 = r4.f1762k     // Catch:{ all -> 0x00b9 }
            r4.z = r1     // Catch:{ all -> 0x00b9 }
            int r1 = r4.f1763l     // Catch:{ all -> 0x00b9 }
            r4.A = r1     // Catch:{ all -> 0x00b9 }
        L_0x0027:
            android.graphics.drawable.Drawable r1 = r4.f()     // Catch:{ all -> 0x00b9 }
            if (r1 != 0) goto L_0x002f
            r1 = 5
            goto L_0x0030
        L_0x002f:
            r1 = 3
        L_0x0030:
            com.bumptech.glide.load.engine.GlideException r2 = new com.bumptech.glide.load.engine.GlideException     // Catch:{ all -> 0x00b9 }
            java.lang.String r3 = "Received null model"
            r2.<init>(r3)     // Catch:{ all -> 0x00b9 }
            r4.a(r2, r1)     // Catch:{ all -> 0x00b9 }
            monitor-exit(r0)     // Catch:{ all -> 0x00b9 }
            return
        L_0x003c:
            j.b.a.q.SingleRequest$a r1 = r4.v     // Catch:{ all -> 0x00b9 }
            j.b.a.q.SingleRequest$a r2 = j.b.a.q.SingleRequest.a.RUNNING     // Catch:{ all -> 0x00b9 }
            if (r1 == r2) goto L_0x00b1
            j.b.a.q.SingleRequest$a r1 = r4.v     // Catch:{ all -> 0x00b9 }
            j.b.a.q.SingleRequest$a r2 = j.b.a.q.SingleRequest.a.COMPLETE     // Catch:{ all -> 0x00b9 }
            if (r1 != r2) goto L_0x0051
            j.b.a.m.m.Resource<R> r1 = r4.f1769r     // Catch:{ all -> 0x00b9 }
            j.b.a.m.DataSource r2 = j.b.a.m.DataSource.MEMORY_CACHE     // Catch:{ all -> 0x00b9 }
            r4.a(r1, r2)     // Catch:{ all -> 0x00b9 }
            monitor-exit(r0)     // Catch:{ all -> 0x00b9 }
            return
        L_0x0051:
            j.b.a.q.SingleRequest$a r1 = j.b.a.q.SingleRequest.a.WAITING_FOR_SIZE     // Catch:{ all -> 0x00b9 }
            r4.v = r1     // Catch:{ all -> 0x00b9 }
            int r1 = r4.f1762k     // Catch:{ all -> 0x00b9 }
            int r2 = r4.f1763l     // Catch:{ all -> 0x00b9 }
            boolean r1 = j.b.a.s.Util.a(r1, r2)     // Catch:{ all -> 0x00b9 }
            if (r1 == 0) goto L_0x0067
            int r1 = r4.f1762k     // Catch:{ all -> 0x00b9 }
            int r2 = r4.f1763l     // Catch:{ all -> 0x00b9 }
            r4.a(r1, r2)     // Catch:{ all -> 0x00b9 }
            goto L_0x006c
        L_0x0067:
            j.b.a.q.h.Target<R> r1 = r4.f1765n     // Catch:{ all -> 0x00b9 }
            r1.b(r4)     // Catch:{ all -> 0x00b9 }
        L_0x006c:
            j.b.a.q.SingleRequest$a r1 = r4.v     // Catch:{ all -> 0x00b9 }
            j.b.a.q.SingleRequest$a r2 = j.b.a.q.SingleRequest.a.RUNNING     // Catch:{ all -> 0x00b9 }
            if (r1 == r2) goto L_0x0078
            j.b.a.q.SingleRequest$a r1 = r4.v     // Catch:{ all -> 0x00b9 }
            j.b.a.q.SingleRequest$a r2 = j.b.a.q.SingleRequest.a.WAITING_FOR_SIZE     // Catch:{ all -> 0x00b9 }
            if (r1 != r2) goto L_0x0091
        L_0x0078:
            j.b.a.q.RequestCoordinator r1 = r4.f1758e     // Catch:{ all -> 0x00b9 }
            if (r1 == 0) goto L_0x0085
            boolean r1 = r1.d(r4)     // Catch:{ all -> 0x00b9 }
            if (r1 == 0) goto L_0x0083
            goto L_0x0085
        L_0x0083:
            r1 = 0
            goto L_0x0086
        L_0x0085:
            r1 = 1
        L_0x0086:
            if (r1 == 0) goto L_0x0091
            j.b.a.q.h.Target<R> r1 = r4.f1765n     // Catch:{ all -> 0x00b9 }
            android.graphics.drawable.Drawable r2 = r4.g()     // Catch:{ all -> 0x00b9 }
            r1.a(r2)     // Catch:{ all -> 0x00b9 }
        L_0x0091:
            boolean r1 = j.b.a.q.SingleRequest.D     // Catch:{ all -> 0x00b9 }
            if (r1 == 0) goto L_0x00af
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00b9 }
            r1.<init>()     // Catch:{ all -> 0x00b9 }
            java.lang.String r2 = "finished run method in "
            r1.append(r2)     // Catch:{ all -> 0x00b9 }
            long r2 = r4.f1771t     // Catch:{ all -> 0x00b9 }
            double r2 = j.b.a.s.LogTime.a(r2)     // Catch:{ all -> 0x00b9 }
            r1.append(r2)     // Catch:{ all -> 0x00b9 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00b9 }
            r4.a(r1)     // Catch:{ all -> 0x00b9 }
        L_0x00af:
            monitor-exit(r0)     // Catch:{ all -> 0x00b9 }
            return
        L_0x00b1:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x00b9 }
            java.lang.String r2 = "Cannot restart a running request"
            r1.<init>(r2)     // Catch:{ all -> 0x00b9 }
            throw r1     // Catch:{ all -> 0x00b9 }
        L_0x00b9:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x00b9 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: j.b.a.q.SingleRequest.b():void");
    }

    public boolean c() {
        boolean z2;
        synchronized (this.c) {
            z2 = this.v == a.COMPLETE;
        }
        return z2;
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [j.b.a.q.Request, j.b.a.q.SingleRequest, j.b.a.q.h.SizeReadyCallback] */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0051, code lost:
        if (r2 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0053, code lost:
        r4.u.a(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        return;
     */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0043  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void clear() {
        /*
            r4 = this;
            java.lang.Object r0 = r4.c
            monitor-enter(r0)
            r4.e()     // Catch:{ all -> 0x0059 }
            j.b.a.s.k.StateVerifier r1 = r4.b     // Catch:{ all -> 0x0059 }
            r1.a()     // Catch:{ all -> 0x0059 }
            j.b.a.q.SingleRequest$a r1 = r4.v     // Catch:{ all -> 0x0059 }
            j.b.a.q.SingleRequest$a r2 = j.b.a.q.SingleRequest.a.CLEARED     // Catch:{ all -> 0x0059 }
            if (r1 != r2) goto L_0x0013
            monitor-exit(r0)     // Catch:{ all -> 0x0059 }
            return
        L_0x0013:
            r4.e()     // Catch:{ all -> 0x0059 }
            j.b.a.s.k.StateVerifier r1 = r4.b     // Catch:{ all -> 0x0059 }
            r1.a()     // Catch:{ all -> 0x0059 }
            j.b.a.q.h.Target<R> r1 = r4.f1765n     // Catch:{ all -> 0x0059 }
            r1.a(r4)     // Catch:{ all -> 0x0059 }
            j.b.a.m.m.Engine$d r1 = r4.f1770s     // Catch:{ all -> 0x0059 }
            r2 = 0
            if (r1 == 0) goto L_0x002a
            r1.a()     // Catch:{ all -> 0x0059 }
            r4.f1770s = r2     // Catch:{ all -> 0x0059 }
        L_0x002a:
            j.b.a.m.m.Resource<R> r1 = r4.f1769r     // Catch:{ all -> 0x0059 }
            if (r1 == 0) goto L_0x0033
            j.b.a.m.m.Resource<R> r1 = r4.f1769r     // Catch:{ all -> 0x0059 }
            r4.f1769r = r2     // Catch:{ all -> 0x0059 }
            r2 = r1
        L_0x0033:
            j.b.a.q.RequestCoordinator r1 = r4.f1758e     // Catch:{ all -> 0x0059 }
            if (r1 == 0) goto L_0x0040
            boolean r1 = r1.e(r4)     // Catch:{ all -> 0x0059 }
            if (r1 == 0) goto L_0x003e
            goto L_0x0040
        L_0x003e:
            r1 = 0
            goto L_0x0041
        L_0x0040:
            r1 = 1
        L_0x0041:
            if (r1 == 0) goto L_0x004c
            j.b.a.q.h.Target<R> r1 = r4.f1765n     // Catch:{ all -> 0x0059 }
            android.graphics.drawable.Drawable r3 = r4.g()     // Catch:{ all -> 0x0059 }
            r1.c(r3)     // Catch:{ all -> 0x0059 }
        L_0x004c:
            j.b.a.q.SingleRequest$a r1 = j.b.a.q.SingleRequest.a.CLEARED     // Catch:{ all -> 0x0059 }
            r4.v = r1     // Catch:{ all -> 0x0059 }
            monitor-exit(r0)     // Catch:{ all -> 0x0059 }
            if (r2 == 0) goto L_0x0058
            j.b.a.m.m.Engine r0 = r4.u
            r0.a(r2)
        L_0x0058:
            return
        L_0x0059:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0059 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: j.b.a.q.SingleRequest.clear():void");
    }

    public boolean d() {
        boolean z2;
        synchronized (this.c) {
            z2 = this.v == a.CLEARED;
        }
        return z2;
    }

    public final void e() {
        if (this.B) {
            throw new IllegalStateException("You can't start or clear loads in RequestListener or Target callbacks. If you're trying to start a fallback request when a load fails, use RequestBuilder#error(RequestBuilder). Otherwise consider posting your into() or clear() calls to the main thread using a Handler instead.");
        }
    }

    public final Drawable f() {
        int i2;
        if (this.y == null) {
            BaseRequestOptions<?> baseRequestOptions = this.f1761j;
            Drawable drawable = baseRequestOptions.f1753p;
            this.y = drawable;
            if (drawable == null && (i2 = baseRequestOptions.f1754q) > 0) {
                this.y = a(i2);
            }
        }
        return this.y;
    }

    public final Drawable g() {
        int i2;
        if (this.x == null) {
            BaseRequestOptions<?> baseRequestOptions = this.f1761j;
            Drawable drawable = baseRequestOptions.h;
            this.x = drawable;
            if (drawable == null && (i2 = baseRequestOptions.f1746i) > 0) {
                this.x = a(i2);
            }
        }
        return this.x;
    }

    public final boolean h() {
        RequestCoordinator requestCoordinator = this.f1758e;
        return requestCoordinator == null || !requestCoordinator.a().b();
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [j.b.a.q.Request, j.b.a.q.SingleRequest] */
    public final void i() {
        int i2;
        RequestCoordinator requestCoordinator = this.f1758e;
        if (requestCoordinator == null || requestCoordinator.d(this)) {
            Drawable drawable = null;
            if (this.h == null) {
                drawable = f();
            }
            if (drawable == null) {
                if (this.w == null) {
                    BaseRequestOptions<?> baseRequestOptions = this.f1761j;
                    Drawable drawable2 = baseRequestOptions.f1745f;
                    this.w = drawable2;
                    if (drawable2 == null && (i2 = baseRequestOptions.g) > 0) {
                        this.w = a(i2);
                    }
                }
                drawable = this.w;
            }
            if (drawable == null) {
                drawable = g();
            }
            this.f1765n.b(drawable);
        }
    }

    public boolean isRunning() {
        boolean z2;
        synchronized (this.c) {
            if (this.v != a.RUNNING) {
                if (this.v != a.WAITING_FOR_SIZE) {
                    z2 = false;
                }
            }
            z2 = true;
        }
        return z2;
    }

    public final Drawable a(int i2) {
        Resources.Theme theme = this.f1761j.v;
        if (theme == null) {
            theme = this.f1759f.getTheme();
        }
        GlideContext glideContext = this.g;
        return DrawableDecoderCompat.a(glideContext, glideContext, i2, theme);
    }

    public void a(int i2, int i3) {
        Object obj;
        int i4;
        int i5 = i2;
        int i6 = i3;
        this.b.a();
        Object obj2 = this.c;
        synchronized (obj2) {
            try {
                if (D) {
                    a("Got onSizeReady in " + LogTime.a(this.f1771t));
                }
                if (this.v == a.WAITING_FOR_SIZE) {
                    this.v = a.RUNNING;
                    float f2 = this.f1761j.c;
                    if (i5 != Integer.MIN_VALUE) {
                        i5 = Math.round(((float) i5) * f2);
                    }
                    this.z = i5;
                    if (i6 == Integer.MIN_VALUE) {
                        i4 = i6;
                    } else {
                        i4 = Math.round(f2 * ((float) i6));
                    }
                    this.A = i4;
                    if (D) {
                        a("finished setup for calling load in " + LogTime.a(this.f1771t));
                    }
                    obj = obj2;
                    try {
                    } catch (Throwable th) {
                        th = th;
                        while (true) {
                            try {
                                break;
                            } catch (Throwable th2) {
                                th = th2;
                            }
                        }
                        throw th;
                    }
                    try {
                        this.f1770s = this.u.a(this.g, this.h, this.f1761j.f1750m, this.z, this.A, this.f1761j.f1757t, this.f1760i, this.f1764m, this.f1761j.d, this.f1761j.f1756s, this.f1761j.f1751n, this.f1761j.z, this.f1761j.f1755r, this.f1761j.f1747j, this.f1761j.x, this.f1761j.A, this.f1761j.y, this, this.f1768q);
                        if (this.v != a.RUNNING) {
                            this.f1770s = null;
                        }
                        if (D) {
                            a("finished onSizeReady in " + LogTime.a(this.f1771t));
                        }
                    } catch (Throwable th3) {
                        th = th3;
                        while (true) {
                            break;
                        }
                        throw th;
                    }
                }
            } catch (Throwable th4) {
                th = th4;
                obj = obj2;
                while (true) {
                    break;
                }
                throw th;
            }
        }
    }

    /* JADX WARN: Type inference failed for: r6v0, types: [j.b.a.q.Request, j.b.a.q.SingleRequest] */
    public void a(Resource<?> resource, j.b.a.m.a aVar) {
        this.b.a();
        Resource<?> resource2 = null;
        try {
            synchronized (this.c) {
                try {
                    this.f1770s = null;
                    if (resource == null) {
                        a(new GlideException("Expected to receive a Resource<R> with an object of " + this.f1760i + " inside, but instead got null."), 5);
                        return;
                    }
                    Object obj = resource.get();
                    if (obj != null) {
                        if (this.f1760i.isAssignableFrom(obj.getClass())) {
                            RequestCoordinator requestCoordinator = this.f1758e;
                            if (!(requestCoordinator == null || requestCoordinator.a(this))) {
                                try {
                                    this.f1769r = null;
                                    this.v = a.COMPLETE;
                                    this.u.a(resource);
                                    return;
                                } catch (Throwable th) {
                                    resource2 = resource;
                                    th = th;
                                    throw th;
                                }
                            } else {
                                a(resource, obj, aVar);
                                return;
                            }
                        }
                    }
                    this.f1769r = null;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Expected to receive an object of ");
                    sb.append(this.f1760i);
                    sb.append(" but instead got ");
                    sb.append(obj != null ? obj.getClass() : "");
                    sb.append("{");
                    sb.append(obj);
                    sb.append("} inside Resource{");
                    sb.append(resource);
                    sb.append("}.");
                    sb.append(obj != null ? "" : " To indicate failure return a null Resource object, rather than a Resource object containing null data.");
                    a(new GlideException(sb.toString()), 5);
                    this.u.a(resource);
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        } catch (Throwable th3) {
            if (resource2 != null) {
                this.u.a(resource2);
            }
            throw th3;
        }
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARN: Type inference failed for: r10v0, types: [j.b.a.q.Request, j.b.a.q.SingleRequest] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00a6 A[Catch:{ all -> 0x00c0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00bc  */
    /* JADX WARNING: Removed duplicated region for block: B:33:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(j.b.a.m.m.Resource<R> r11, R r12, j.b.a.m.a r13) {
        /*
            r10 = this;
            boolean r6 = r10.h()
            j.b.a.q.SingleRequest$a r0 = j.b.a.q.SingleRequest.a.COMPLETE
            r10.v = r0
            r10.f1769r = r11
            j.b.a.GlideContext r11 = r10.g
            int r11 = r11.f1549i
            r0 = 3
            if (r11 > r0) goto L_0x0064
            java.lang.String r11 = "Finished loading "
            java.lang.StringBuilder r11 = j.a.a.a.outline.a(r11)
            java.lang.Class r0 = r12.getClass()
            java.lang.String r0 = r0.getSimpleName()
            r11.append(r0)
            java.lang.String r0 = " from "
            r11.append(r0)
            r11.append(r13)
            java.lang.String r0 = " for "
            r11.append(r0)
            java.lang.Object r0 = r10.h
            r11.append(r0)
            java.lang.String r0 = " with size ["
            r11.append(r0)
            int r0 = r10.z
            r11.append(r0)
            java.lang.String r0 = "x"
            r11.append(r0)
            int r0 = r10.A
            r11.append(r0)
            java.lang.String r0 = "] in "
            r11.append(r0)
            long r0 = r10.f1771t
            double r0 = j.b.a.s.LogTime.a(r0)
            r11.append(r0)
            java.lang.String r0 = " ms"
            r11.append(r0)
            java.lang.String r11 = r11.toString()
            java.lang.String r0 = "Glide"
            android.util.Log.d(r0, r11)
        L_0x0064:
            r11 = 1
            r10.B = r11
            r7 = 0
            java.util.List<j.b.a.q.RequestListener<R>> r0 = r10.f1766o     // Catch:{ all -> 0x00c0 }
            if (r0 == 0) goto L_0x008d
            java.util.List<j.b.a.q.RequestListener<R>> r0 = r10.f1766o     // Catch:{ all -> 0x00c0 }
            java.util.Iterator r8 = r0.iterator()     // Catch:{ all -> 0x00c0 }
            r0 = 0
            r9 = 0
        L_0x0074:
            boolean r0 = r8.hasNext()     // Catch:{ all -> 0x00c0 }
            if (r0 == 0) goto L_0x008e
            java.lang.Object r0 = r8.next()     // Catch:{ all -> 0x00c0 }
            j.b.a.q.RequestListener r0 = (j.b.a.q.RequestListener) r0     // Catch:{ all -> 0x00c0 }
            java.lang.Object r2 = r10.h     // Catch:{ all -> 0x00c0 }
            j.b.a.q.h.Target<R> r3 = r10.f1765n     // Catch:{ all -> 0x00c0 }
            r1 = r12
            r4 = r13
            r5 = r6
            boolean r0 = r0.a(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x00c0 }
            r9 = r9 | r0
            goto L_0x0074
        L_0x008d:
            r9 = 0
        L_0x008e:
            j.b.a.q.RequestListener<R> r0 = r10.d     // Catch:{ all -> 0x00c0 }
            if (r0 == 0) goto L_0x00a2
            j.b.a.q.RequestListener<R> r0 = r10.d     // Catch:{ all -> 0x00c0 }
            java.lang.Object r2 = r10.h     // Catch:{ all -> 0x00c0 }
            j.b.a.q.h.Target<R> r3 = r10.f1765n     // Catch:{ all -> 0x00c0 }
            r1 = r12
            r4 = r13
            r5 = r6
            boolean r13 = r0.a(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x00c0 }
            if (r13 == 0) goto L_0x00a2
            goto L_0x00a3
        L_0x00a2:
            r11 = 0
        L_0x00a3:
            r11 = r11 | r9
            if (r11 != 0) goto L_0x00b6
            j.b.a.q.i.TransitionFactory<? super R> r11 = r10.f1767p     // Catch:{ all -> 0x00c0 }
            j.b.a.q.i.NoTransition$a r11 = (j.b.a.q.i.NoTransition.a) r11     // Catch:{ all -> 0x00c0 }
            if (r11 == 0) goto L_0x00b4
            j.b.a.q.i.NoTransition<?> r11 = j.b.a.q.i.NoTransition.a     // Catch:{ all -> 0x00c0 }
            j.b.a.q.h.Target<R> r13 = r10.f1765n     // Catch:{ all -> 0x00c0 }
            r13.a(r12, r11)     // Catch:{ all -> 0x00c0 }
            goto L_0x00b6
        L_0x00b4:
            r11 = 0
            throw r11     // Catch:{ all -> 0x00c0 }
        L_0x00b6:
            r10.B = r7
            j.b.a.q.RequestCoordinator r11 = r10.f1758e
            if (r11 == 0) goto L_0x00bf
            r11.c(r10)
        L_0x00bf:
            return
        L_0x00c0:
            r11 = move-exception
            r10.B = r7
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: j.b.a.q.SingleRequest.a(j.b.a.m.m.Resource, java.lang.Object, j.b.a.m.DataSource):void");
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARN: Type inference failed for: r8v0, types: [j.b.a.q.Request, j.b.a.q.SingleRequest] */
    public final void a(GlideException glideException, int i2) {
        boolean z2;
        this.b.a();
        synchronized (this.c) {
            if (glideException != null) {
                int i3 = this.g.f1549i;
                if (i3 <= i2) {
                    Log.w("Glide", "Load failed for " + this.h + " with size [" + this.z + "x" + this.A + "]", glideException);
                    if (i3 <= 4) {
                        glideException.a("Glide");
                    }
                }
                this.f1770s = null;
                this.v = a.FAILED;
                boolean z3 = true;
                this.B = true;
                try {
                    if (this.f1766o != null) {
                        z2 = false;
                        for (RequestListener<R> a2 : this.f1766o) {
                            z2 |= a2.a(glideException, this.h, this.f1765n, h());
                        }
                    } else {
                        z2 = false;
                    }
                    if (this.d == null || !this.d.a(glideException, this.h, this.f1765n, h())) {
                        z3 = false;
                    }
                    if (!z2 && !z3) {
                        i();
                    }
                    this.B = false;
                    RequestCoordinator requestCoordinator = this.f1758e;
                    if (requestCoordinator != null) {
                        requestCoordinator.b(this);
                    }
                } catch (Throwable th) {
                    this.B = false;
                    throw th;
                }
            } else {
                throw null;
            }
        }
    }

    public boolean a(Request request) {
        int i2;
        int i3;
        Object obj;
        Class<R> cls;
        BaseRequestOptions<?> baseRequestOptions;
        Priority priority;
        int size;
        int i4;
        int i5;
        Object obj2;
        Class<R> cls2;
        BaseRequestOptions<?> baseRequestOptions2;
        Priority priority2;
        int size2;
        Request request2 = request;
        if (!(request2 instanceof SingleRequest)) {
            return false;
        }
        synchronized (this.c) {
            i2 = this.f1762k;
            i3 = this.f1763l;
            obj = this.h;
            cls = this.f1760i;
            baseRequestOptions = this.f1761j;
            priority = this.f1764m;
            size = this.f1766o != null ? this.f1766o.size() : 0;
        }
        SingleRequest singleRequest = (SingleRequest) request2;
        synchronized (singleRequest.c) {
            i4 = singleRequest.f1762k;
            i5 = singleRequest.f1763l;
            obj2 = singleRequest.h;
            cls2 = singleRequest.f1760i;
            baseRequestOptions2 = singleRequest.f1761j;
            priority2 = singleRequest.f1764m;
            size2 = singleRequest.f1766o != null ? singleRequest.f1766o.size() : 0;
        }
        return i2 == i4 && i3 == i5 && Util.a(obj, obj2) && cls.equals(cls2) && baseRequestOptions.equals(baseRequestOptions2) && priority == priority2 && size == size2;
    }

    public final void a(String str) {
        StringBuilder b2 = outline.b(str, " this: ");
        b2.append(this.a);
        Log.v("Request", b2.toString());
    }
}
