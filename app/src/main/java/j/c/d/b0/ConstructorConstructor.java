package j.c.d.b0;

import j.c.d.InstanceCreator;
import j.c.d.b0.b0.ReflectionAccessor;
import j.c.d.c0.TypeToken;
import java.lang.reflect.Constructor;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.EnumSet;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentNavigableMap;

public final class ConstructorConstructor {
    public final Map<Type, InstanceCreator<?>> a;
    public final ReflectionAccessor b = ReflectionAccessor.a;

    public class a implements ObjectConstructor<T> {
        public final /* synthetic */ InstanceCreator a;
        public final /* synthetic */ Type b;

        public a(ConstructorConstructor constructorConstructor, InstanceCreator instanceCreator, Type type) {
            this.a = instanceCreator;
            this.b = type;
        }

        public T a() {
            return this.a.a(this.b);
        }
    }

    public class b implements ObjectConstructor<T> {
        public final /* synthetic */ InstanceCreator a;
        public final /* synthetic */ Type b;

        public b(ConstructorConstructor constructorConstructor, InstanceCreator instanceCreator, Type type) {
            this.a = instanceCreator;
            this.b = type;
        }

        public T a() {
            return this.a.a(this.b);
        }
    }

    public ConstructorConstructor(Map<Type, InstanceCreator<?>> map) {
        this.a = map;
    }

    public <T> ObjectConstructor<T> a(TypeToken<T> typeToken) {
        ConstructorConstructor5 constructorConstructor5;
        Type type = typeToken.b;
        Class<? super T> cls = typeToken.a;
        InstanceCreator instanceCreator = this.a.get(type);
        if (instanceCreator != null) {
            return new a(this, instanceCreator, type);
        }
        InstanceCreator instanceCreator2 = this.a.get(cls);
        if (instanceCreator2 != null) {
            return new b(this, instanceCreator2, type);
        }
        ObjectConstructor<T> objectConstructor = null;
        try {
            Constructor<? super T> declaredConstructor = cls.getDeclaredConstructor(new Class[0]);
            if (!declaredConstructor.isAccessible()) {
                this.b.a(declaredConstructor);
            }
            constructorConstructor5 = new ConstructorConstructor5(this, declaredConstructor);
        } catch (NoSuchMethodException unused) {
            constructorConstructor5 = null;
        }
        if (constructorConstructor5 != null) {
            return constructorConstructor5;
        }
        if (Collection.class.isAssignableFrom(cls)) {
            if (SortedSet.class.isAssignableFrom(cls)) {
                objectConstructor = new ConstructorConstructor6(this);
            } else if (EnumSet.class.isAssignableFrom(cls)) {
                objectConstructor = new ConstructorConstructor7(this, type);
            } else if (Set.class.isAssignableFrom(cls)) {
                objectConstructor = new ConstructorConstructor8(this);
            } else if (Queue.class.isAssignableFrom(cls)) {
                objectConstructor = new ConstructorConstructor9(this);
            } else {
                objectConstructor = new ConstructorConstructor10(this);
            }
        } else if (Map.class.isAssignableFrom(cls)) {
            if (ConcurrentNavigableMap.class.isAssignableFrom(cls)) {
                objectConstructor = new ConstructorConstructor11(this);
            } else if (ConcurrentMap.class.isAssignableFrom(cls)) {
                objectConstructor = new ConstructorConstructor0(this);
            } else if (SortedMap.class.isAssignableFrom(cls)) {
                objectConstructor = new ConstructorConstructor1(this);
            } else {
                if (type instanceof ParameterizedType) {
                    Class<String> cls2 = String.class;
                    Type type2 = ((ParameterizedType) type).getActualTypeArguments()[0];
                    if (type2 != null) {
                        Type a2 = a.a(type2);
                        Class<?> c = a.c(a2);
                        a2.hashCode();
                        if (!cls2.isAssignableFrom(c)) {
                            objectConstructor = new ConstructorConstructor2(this);
                        }
                    } else {
                        throw null;
                    }
                }
                objectConstructor = new ConstructorConstructor3(this);
            }
        }
        if (objectConstructor != null) {
            return objectConstructor;
        }
        return new ConstructorConstructor4(this, cls, type);
    }

    public String toString() {
        return this.a.toString();
    }
}
