package j.c.d.b0.a0;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import j.c.d.JsonArray;
import j.c.d.JsonElement;
import j.c.d.JsonNull;
import j.c.d.JsonObject;
import j.c.d.JsonPrimitive;
import j.c.d.TypeAdapter;
import j.c.d.TypeAdapterFactory;
import j.c.d.b0.ConstructorConstructor;
import j.c.d.b0.JsonReaderInternalAccess;
import j.c.d.b0.ObjectConstructor;
import j.c.d.c0.TypeToken;
import j.c.d.k;
import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Map;
import java.util.Properties;

public final class MapTypeAdapterFactory implements TypeAdapterFactory {
    public final ConstructorConstructor b;
    public final boolean c;

    public MapTypeAdapterFactory(ConstructorConstructor constructorConstructor, boolean z) {
        this.b = constructorConstructor;
        this.c = z;
    }

    public <T> TypeAdapter<T> a(k kVar, TypeToken<T> typeToken) {
        Type[] typeArr;
        TypeAdapter<Boolean> typeAdapter;
        Type type = typeToken.b;
        if (!Map.class.isAssignableFrom(typeToken.a)) {
            return null;
        }
        Class<?> c2 = j.c.d.b0.a.c(type);
        Class<Object> cls = Object.class;
        Class<String> cls2 = String.class;
        if (type == Properties.class) {
            typeArr = new Type[]{cls2, cls2};
        } else {
            Type b2 = j.c.d.b0.a.b(type, c2, Map.class);
            typeArr = b2 instanceof ParameterizedType ? ((ParameterizedType) b2).getActualTypeArguments() : new Type[]{cls, cls};
        }
        Type type2 = typeArr[0];
        if (type2 == Boolean.TYPE || type2 == Boolean.class) {
            typeAdapter = TypeAdapters.f2572f;
        } else {
            typeAdapter = kVar.a(new TypeToken(type2));
        }
        return new a(kVar, typeArr[0], typeAdapter, typeArr[1], kVar.a(new TypeToken(typeArr[1])), this.b.a(typeToken));
    }

    public final class a<K, V> extends TypeAdapter<Map<K, V>> {
        public final TypeAdapter<K> a;
        public final TypeAdapter<V> b;
        public final ObjectConstructor<? extends Map<K, V>> c;

        public a(k kVar, Type type, TypeAdapter<K> typeAdapter, Type type2, TypeAdapter<V> typeAdapter2, ObjectConstructor<? extends Map<K, V>> objectConstructor) {
            this.a = new TypeAdapterRuntimeTypeWrapper(kVar, super, type);
            this.b = new TypeAdapterRuntimeTypeWrapper(kVar, super, type2);
            this.c = objectConstructor;
        }

        public void a(JsonWriter jsonWriter, Object obj) {
            String str;
            Map map = (Map) obj;
            if (map == null) {
                jsonWriter.nullValue();
            } else if (!MapTypeAdapterFactory.this.c) {
                jsonWriter.beginObject();
                for (Map.Entry entry : map.entrySet()) {
                    jsonWriter.name(String.valueOf(entry.getKey()));
                    this.b.a(jsonWriter, entry.getValue());
                }
                jsonWriter.endObject();
            } else {
                ArrayList arrayList = new ArrayList(map.size());
                ArrayList arrayList2 = new ArrayList(map.size());
                int i2 = 0;
                boolean z = false;
                for (Map.Entry entry2 : map.entrySet()) {
                    TypeAdapter<K> typeAdapter = this.a;
                    Object key = entry2.getKey();
                    if (typeAdapter != null) {
                        try {
                            JsonTreeWriter jsonTreeWriter = new JsonTreeWriter();
                            super.a(jsonTreeWriter, key);
                            if (jsonTreeWriter.b.isEmpty()) {
                                JsonElement jsonElement = jsonTreeWriter.d;
                                arrayList.add(jsonElement);
                                arrayList2.add(entry2.getValue());
                                if (jsonElement != null) {
                                    z |= (jsonElement instanceof JsonArray) || (jsonElement instanceof JsonObject);
                                } else {
                                    throw null;
                                }
                            } else {
                                throw new IllegalStateException("Expected one JSON element but was " + jsonTreeWriter.b);
                            }
                        } catch (IOException e2) {
                            throw new JsonIOException(e2);
                        }
                    } else {
                        throw null;
                    }
                }
                if (z) {
                    jsonWriter.beginArray();
                    int size = arrayList.size();
                    while (i2 < size) {
                        jsonWriter.beginArray();
                        TypeAdapters.X.a(jsonWriter, (JsonElement) arrayList.get(i2));
                        this.b.a(jsonWriter, arrayList2.get(i2));
                        jsonWriter.endArray();
                        i2++;
                    }
                    jsonWriter.endArray();
                    return;
                }
                jsonWriter.beginObject();
                int size2 = arrayList.size();
                while (i2 < size2) {
                    JsonElement jsonElement2 = (JsonElement) arrayList.get(i2);
                    if (jsonElement2 != null) {
                        if (jsonElement2 instanceof JsonPrimitive) {
                            JsonPrimitive c2 = jsonElement2.c();
                            Object obj2 = c2.a;
                            if (obj2 instanceof Number) {
                                str = String.valueOf(c2.f());
                            } else if (obj2 instanceof Boolean) {
                                str = Boolean.toString(c2.e());
                            } else if (obj2 instanceof String) {
                                str = c2.d();
                            } else {
                                throw new AssertionError();
                            }
                        } else if (jsonElement2 instanceof JsonNull) {
                            str = "null";
                        } else {
                            throw new AssertionError();
                        }
                        jsonWriter.name(str);
                        this.b.a(jsonWriter, arrayList2.get(i2));
                        i2++;
                    } else {
                        throw null;
                    }
                }
                jsonWriter.endObject();
            }
        }

        public Object a(JsonReader jsonReader) {
            JsonToken peek = jsonReader.peek();
            if (peek == JsonToken.NULL) {
                jsonReader.nextNull();
                return null;
            }
            Map map = (Map) this.c.a();
            if (peek == JsonToken.BEGIN_ARRAY) {
                jsonReader.beginArray();
                while (jsonReader.hasNext()) {
                    jsonReader.beginArray();
                    K a2 = this.a.a(jsonReader);
                    if (map.put(a2, this.b.a(jsonReader)) == null) {
                        jsonReader.endArray();
                    } else {
                        throw new JsonSyntaxException("duplicate key: " + ((Object) a2));
                    }
                }
                jsonReader.endArray();
            } else {
                jsonReader.beginObject();
                while (jsonReader.hasNext()) {
                    JsonReaderInternalAccess.INSTANCE.promoteNameToValue(jsonReader);
                    K a3 = this.a.a(jsonReader);
                    if (map.put(a3, this.b.a(jsonReader)) != null) {
                        throw new JsonSyntaxException("duplicate key: " + ((Object) a3));
                    }
                }
                jsonReader.endObject();
            }
            return map;
        }
    }
}
