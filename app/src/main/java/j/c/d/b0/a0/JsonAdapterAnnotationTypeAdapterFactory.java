package j.c.d.b0.a0;

import j.c.d.TypeAdapterFactory;
import j.c.d.b0.ConstructorConstructor;

public final class JsonAdapterAnnotationTypeAdapterFactory implements TypeAdapterFactory {
    public final ConstructorConstructor b;

    public JsonAdapterAnnotationTypeAdapterFactory(ConstructorConstructor constructorConstructor) {
        this.b = constructorConstructor;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r9v13, types: [j.c.d.TypeAdapter] */
    /* JADX WARN: Type inference failed for: r9v14, types: [j.c.d.TypeAdapter] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public j.c.d.TypeAdapter<?> a(j.c.d.b0.g r9, j.c.d.k r10, j.c.d.c0.TypeToken<?> r11, j.c.d.a0.a r12) {
        /*
            r8 = this;
            java.lang.Class r0 = r12.value()
            j.c.d.c0.TypeToken r1 = new j.c.d.c0.TypeToken
            r1.<init>(r0)
            j.c.d.b0.ObjectConstructor r9 = r9.a(r1)
            java.lang.Object r9 = r9.a()
            boolean r0 = r9 instanceof j.c.d.TypeAdapter
            if (r0 == 0) goto L_0x0018
            j.c.d.TypeAdapter r9 = (j.c.d.TypeAdapter) r9
            goto L_0x0072
        L_0x0018:
            boolean r0 = r9 instanceof j.c.d.TypeAdapterFactory
            if (r0 == 0) goto L_0x0023
            j.c.d.TypeAdapterFactory r9 = (j.c.d.TypeAdapterFactory) r9
            j.c.d.TypeAdapter r9 = r9.a(r10, r11)
            goto L_0x0072
        L_0x0023:
            boolean r0 = r9 instanceof j.c.d.JsonSerializer
            if (r0 != 0) goto L_0x0058
            boolean r1 = r9 instanceof j.c.d.JsonDeserializer
            if (r1 == 0) goto L_0x002c
            goto L_0x0058
        L_0x002c:
            java.lang.IllegalArgumentException r10 = new java.lang.IllegalArgumentException
            java.lang.String r12 = "Invalid attempt to bind an instance of "
            java.lang.StringBuilder r12 = j.a.a.a.outline.a(r12)
            java.lang.Class r9 = r9.getClass()
            java.lang.String r9 = r9.getName()
            r12.append(r9)
            java.lang.String r9 = " as a @JsonAdapter for "
            r12.append(r9)
            java.lang.String r9 = r11.toString()
            r12.append(r9)
            java.lang.String r9 = ". @JsonAdapter value must be a TypeAdapter, TypeAdapterFactory, JsonSerializer or JsonDeserializer."
            r12.append(r9)
            java.lang.String r9 = r12.toString()
            r10.<init>(r9)
            throw r10
        L_0x0058:
            r1 = 0
            if (r0 == 0) goto L_0x0060
            r0 = r9
            j.c.d.JsonSerializer r0 = (j.c.d.JsonSerializer) r0
            r3 = r0
            goto L_0x0061
        L_0x0060:
            r3 = r1
        L_0x0061:
            boolean r0 = r9 instanceof j.c.d.JsonDeserializer
            if (r0 == 0) goto L_0x0068
            r1 = r9
            j.c.d.JsonDeserializer r1 = (j.c.d.JsonDeserializer) r1
        L_0x0068:
            r4 = r1
            j.c.d.b0.a0.TreeTypeAdapter r9 = new j.c.d.b0.a0.TreeTypeAdapter
            r7 = 0
            r2 = r9
            r5 = r10
            r6 = r11
            r2.<init>(r3, r4, r5, r6, r7)
        L_0x0072:
            if (r9 == 0) goto L_0x0080
            boolean r10 = r12.nullSafe()
            if (r10 == 0) goto L_0x0080
            j.c.d.TypeAdapter0 r10 = new j.c.d.TypeAdapter0
            r10.<init>(r9)
            r9 = r10
        L_0x0080:
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.d.b0.a0.JsonAdapterAnnotationTypeAdapterFactory.a(j.c.d.b0.ConstructorConstructor, j.c.d.Gson, j.c.d.c0.TypeToken, j.c.d.a0.JsonAdapter):j.c.d.TypeAdapter");
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [j.c.d.c0.TypeToken<T>, j.c.d.c0.TypeToken] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <T> j.c.d.TypeAdapter<T> a(j.c.d.k r3, j.c.d.c0.TypeToken<T> r4) {
        /*
            r2 = this;
            java.lang.Class<? super T> r0 = r4.a
            java.lang.Class<j.c.d.a0.JsonAdapter> r1 = j.c.d.a0.JsonAdapter.class
            java.lang.annotation.Annotation r0 = r0.getAnnotation(r1)
            j.c.d.a0.JsonAdapter r0 = (j.c.d.a0.JsonAdapter) r0
            if (r0 != 0) goto L_0x000e
            r3 = 0
            return r3
        L_0x000e:
            j.c.d.b0.ConstructorConstructor r1 = r2.b
            j.c.d.TypeAdapter r3 = r2.a(r1, r3, r4, r0)
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.d.b0.a0.JsonAdapterAnnotationTypeAdapterFactory.a(j.c.d.Gson, j.c.d.c0.TypeToken):j.c.d.TypeAdapter");
    }
}
