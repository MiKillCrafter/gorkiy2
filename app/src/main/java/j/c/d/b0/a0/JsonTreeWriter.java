package j.c.d.b0.a0;

import com.google.gson.stream.JsonWriter;
import j.c.d.JsonArray;
import j.c.d.JsonElement;
import j.c.d.JsonNull;
import j.c.d.JsonObject;
import j.c.d.JsonPrimitive;
import j.c.d.q;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

public final class JsonTreeWriter extends JsonWriter {

    /* renamed from: e  reason: collision with root package name */
    public static final Writer f2560e = new a();

    /* renamed from: f  reason: collision with root package name */
    public static final JsonPrimitive f2561f = new JsonPrimitive("closed");
    public final List<q> b = new ArrayList();
    public String c;
    public JsonElement d = JsonNull.a;

    public static class a extends Writer {
        public void close() {
            throw new AssertionError();
        }

        public void flush() {
            throw new AssertionError();
        }

        public void write(char[] cArr, int i2, int i3) {
            throw new AssertionError();
        }
    }

    public JsonTreeWriter() {
        super(f2560e);
    }

    public final void a(JsonElement jsonElement) {
        if (this.c != null) {
            if (jsonElement != null) {
                if (!(jsonElement instanceof JsonNull) || getSerializeNulls()) {
                    ((JsonObject) peek()).a(this.c, jsonElement);
                }
                this.c = null;
                return;
            }
            throw null;
        } else if (this.b.isEmpty()) {
            this.d = jsonElement;
        } else {
            JsonElement peek = peek();
            if (peek instanceof JsonArray) {
                JsonArray jsonArray = (JsonArray) peek;
                if (jsonArray != null) {
                    if (jsonElement == null) {
                        jsonElement = JsonNull.a;
                    }
                    jsonArray.b.add(jsonElement);
                    return;
                }
                throw null;
            }
            throw new IllegalStateException();
        }
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [j.c.d.JsonElement, java.lang.Object, j.c.d.JsonArray] */
    public JsonWriter beginArray() {
        ? jsonArray = new JsonArray();
        a(jsonArray);
        this.b.add(jsonArray);
        return super;
    }

    public JsonWriter beginObject() {
        JsonObject jsonObject = new JsonObject();
        a(jsonObject);
        this.b.add(jsonObject);
        return super;
    }

    public void close() {
        if (this.b.isEmpty()) {
            this.b.add(f2561f);
            return;
        }
        throw new IOException("Incomplete document");
    }

    public JsonWriter endArray() {
        if (this.b.isEmpty() || this.c != null) {
            throw new IllegalStateException();
        } else if (peek() instanceof JsonArray) {
            List<q> list = this.b;
            list.remove(list.size() - 1);
            return super;
        } else {
            throw new IllegalStateException();
        }
    }

    public JsonWriter endObject() {
        if (this.b.isEmpty() || this.c != null) {
            throw new IllegalStateException();
        } else if (peek() instanceof JsonObject) {
            List<q> list = this.b;
            list.remove(list.size() - 1);
            return super;
        } else {
            throw new IllegalStateException();
        }
    }

    public void flush() {
    }

    public JsonWriter name(String str) {
        if (this.b.isEmpty() || this.c != null) {
            throw new IllegalStateException();
        } else if (peek() instanceof JsonObject) {
            this.c = str;
            return super;
        } else {
            throw new IllegalStateException();
        }
    }

    public JsonWriter nullValue() {
        a(JsonNull.a);
        return super;
    }

    public final JsonElement peek() {
        List<q> list = this.b;
        return list.get(list.size() - 1);
    }

    public JsonWriter value(Boolean bool) {
        if (bool == null) {
            a(JsonNull.a);
            return super;
        }
        a(new JsonPrimitive(bool));
        return super;
    }

    public JsonWriter value(Number number) {
        if (number == null) {
            a(JsonNull.a);
            return super;
        }
        if (!isLenient()) {
            double doubleValue = number.doubleValue();
            if (Double.isNaN(doubleValue) || Double.isInfinite(doubleValue)) {
                throw new IllegalArgumentException("JSON forbids NaN and infinities: " + number);
            }
        }
        a(new JsonPrimitive(number));
        return super;
    }

    public JsonWriter value(String str) {
        if (str == null) {
            a(JsonNull.a);
            return super;
        }
        a(new JsonPrimitive(str));
        return super;
    }

    public JsonWriter value(boolean z) {
        a(new JsonPrimitive(Boolean.valueOf(z)));
        return super;
    }

    public JsonWriter value(double d2) {
        if (isLenient() || (!Double.isNaN(d2) && !Double.isInfinite(d2))) {
            a(new JsonPrimitive((Number) Double.valueOf(d2)));
            return super;
        }
        throw new IllegalArgumentException("JSON forbids NaN and infinities: " + d2);
    }

    public JsonWriter value(long j2) {
        a(new JsonPrimitive((Number) Long.valueOf(j2)));
        return super;
    }
}
