package j.c.d.b0.a0;

import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import j.a.a.a.outline;
import j.c.d.TypeAdapter;
import j.c.d.TypeAdapterFactory;
import j.c.d.c0.TypeToken;
import j.c.d.k;

/* compiled from: TypeAdapters */
public final class TypeAdapters2 implements TypeAdapterFactory {
    public final /* synthetic */ Class b;
    public final /* synthetic */ TypeAdapter c;

    /* compiled from: TypeAdapters */
    public class a extends TypeAdapter<T1> {
        public final /* synthetic */ Class a;

        public a(Class cls) {
            this.a = cls;
        }

        public void a(JsonWriter jsonWriter, T1 t1) {
            TypeAdapters2.this.c.a(jsonWriter, t1);
        }

        public T1 a(JsonReader jsonReader) {
            T1 a2 = TypeAdapters2.this.c.a(jsonReader);
            if (a2 == null || this.a.isInstance(a2)) {
                return a2;
            }
            StringBuilder a3 = outline.a("Expected a ");
            a3.append(this.a.getName());
            a3.append(" but was ");
            a3.append(a2.getClass().getName());
            throw new JsonSyntaxException(a3.toString());
        }
    }

    public TypeAdapters2(Class cls, TypeAdapter typeAdapter) {
        this.b = cls;
        this.c = typeAdapter;
    }

    public <T2> TypeAdapter<T2> a(k kVar, TypeToken<T2> typeToken) {
        Class<? super T> cls = typeToken.a;
        if (!this.b.isAssignableFrom(cls)) {
            return null;
        }
        return new a(cls);
    }

    public String toString() {
        StringBuilder a2 = outline.a("Factory[typeHierarchy=");
        a2.append(this.b.getName());
        a2.append(",adapter=");
        a2.append(this.c);
        a2.append("]");
        return a2.toString();
    }
}
