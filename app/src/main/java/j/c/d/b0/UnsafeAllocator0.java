package j.c.d.b0;

import java.lang.reflect.Method;

/* compiled from: UnsafeAllocator */
public final class UnsafeAllocator0 extends UnsafeAllocator {
    public final /* synthetic */ Method a;
    public final /* synthetic */ Object b;

    public UnsafeAllocator0(Method method, Object obj) {
        this.a = method;
        this.b = obj;
    }

    public <T> T a(Class<T> cls) {
        UnsafeAllocator.b(cls);
        return this.a.invoke(this.b, cls);
    }
}
