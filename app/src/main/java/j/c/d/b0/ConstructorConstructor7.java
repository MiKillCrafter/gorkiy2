package j.c.d.b0;

import com.google.gson.JsonIOException;
import j.a.a.a.outline;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.EnumSet;

/* compiled from: ConstructorConstructor */
public class ConstructorConstructor7 implements ObjectConstructor<T> {
    public final /* synthetic */ Type a;

    public ConstructorConstructor7(ConstructorConstructor constructorConstructor, Type type) {
        this.a = type;
    }

    public T a() {
        Type type = this.a;
        if (type instanceof ParameterizedType) {
            Type type2 = ((ParameterizedType) type).getActualTypeArguments()[0];
            if (type2 instanceof Class) {
                return EnumSet.noneOf((Class) type2);
            }
            StringBuilder a2 = outline.a("Invalid EnumSet type: ");
            a2.append(this.a.toString());
            throw new JsonIOException(a2.toString());
        }
        StringBuilder a3 = outline.a("Invalid EnumSet type: ");
        a3.append(this.a.toString());
        throw new JsonIOException(a3.toString());
    }
}
