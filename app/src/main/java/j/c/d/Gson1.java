package j.c.d;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

/* compiled from: Gson */
public class Gson1 extends TypeAdapter<Number> {
    public Gson1(Gson gson) {
    }

    public void a(JsonWriter jsonWriter, Object obj) {
        Number number = (Number) obj;
        if (number == null) {
            jsonWriter.nullValue();
            return;
        }
        Gson.a((double) number.floatValue());
        jsonWriter.value(number);
    }

    public Object a(JsonReader jsonReader) {
        if (jsonReader.peek() != JsonToken.NULL) {
            return Float.valueOf((float) jsonReader.nextDouble());
        }
        jsonReader.nextNull();
        return null;
    }
}
