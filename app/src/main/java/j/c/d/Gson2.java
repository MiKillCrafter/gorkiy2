package j.c.d;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

/* compiled from: Gson */
public final class Gson2 extends TypeAdapter<Number> {
    public void a(JsonWriter jsonWriter, Object obj) {
        Number number = (Number) obj;
        if (number == null) {
            jsonWriter.nullValue();
        } else {
            jsonWriter.value(number.toString());
        }
    }

    public Object a(JsonReader jsonReader) {
        if (jsonReader.peek() != JsonToken.NULL) {
            return Long.valueOf(jsonReader.nextLong());
        }
        jsonReader.nextNull();
        return null;
    }
}
