package j.c.e;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.UninitializedMessageException;
import j.c.e.o;

public abstract class AbstractParser<MessageType extends o> implements Parser<MessageType> {
    static {
        ExtensionRegistryLite.a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.e.Parser.a(j.c.e.g, j.c.e.i):MessageType
     arg types: [j.c.e.CodedInputStream, j.c.e.ExtensionRegistryLite]
     candidates:
      j.c.e.AbstractParser.a(j.c.e.ByteString, j.c.e.ExtensionRegistryLite):java.lang.Object
      j.c.e.Parser.a(j.c.e.f, j.c.e.i):MessageType
      j.c.e.Parser.a(j.c.e.g, j.c.e.i):MessageType */
    public Object a(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) {
        UninitializedMessageException uninitializedMessageException;
        try {
            CodedInputStream c = byteString.c();
            MessageLite messageLite = (MessageLite) a((g) c, (i) extensionRegistryLite);
            c.a(0);
            if (messageLite == null || messageLite.j()) {
                return messageLite;
            }
            if (messageLite instanceof AbstractMessageLite) {
                AbstractMessageLite abstractMessageLite = (AbstractMessageLite) messageLite;
                uninitializedMessageException = new UninitializedMessageException();
            } else {
                uninitializedMessageException = new UninitializedMessageException();
            }
            throw new InvalidProtocolBufferException(uninitializedMessageException.getMessage());
        } catch (InvalidProtocolBufferException e2) {
            throw e2;
        } catch (InvalidProtocolBufferException e3) {
            throw e3;
        }
    }
}
