package j.c.e;

import java.util.Collections;
import java.util.HashMap;

public class ExtensionRegistryLite {
    public static final ExtensionRegistryLite a = new ExtensionRegistryLite(true);

    static {
        try {
            Class.forName("com.google.protobuf.Extension");
        } catch (ClassNotFoundException unused) {
        }
    }

    public ExtensionRegistryLite() {
        new HashMap();
    }

    public static ExtensionRegistryLite a() {
        Class<?> cls = ExtensionRegistryFactory.a;
        if (cls != null) {
            try {
                return (ExtensionRegistryLite) cls.getMethod("getEmptyRegistry", new Class[0]).invoke(null, new Object[0]);
            } catch (Exception unused) {
            }
        }
        return a;
    }

    public ExtensionRegistryLite(boolean z) {
        Collections.emptyMap();
    }
}
