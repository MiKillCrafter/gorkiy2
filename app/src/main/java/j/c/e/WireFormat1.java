package j.c.e;

/* compiled from: WireFormat */
public enum WireFormat1 {
    INT(0),
    LONG(0L),
    FLOAT(Float.valueOf(0.0f)),
    DOUBLE(Double.valueOf(0.0d)),
    BOOLEAN(false),
    STRING(""),
    BYTE_STRING(ByteString.c),
    ENUM(null),
    MESSAGE(null);
    
    public final Object defaultDefault;

    /* access modifiers changed from: public */
    WireFormat1(Object obj) {
        this.defaultDefault = obj;
    }
}
