package j.c.e;

import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.InvalidProtocolBufferException;
import j.a.a.a.outline;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Iterator;

public abstract class ByteString implements Iterable<Byte>, Serializable {
    public static final ByteString c = new g(Internal.b);
    public static final c d;
    public int b = 0;

    public static final class a implements c {
        public /* synthetic */ a(ByteString0 byteString0) {
        }

        public byte[] a(byte[] bArr, int i2, int i3) {
            return Arrays.copyOfRange(bArr, i2, i3 + i2);
        }
    }

    public static final class b extends g {

        /* renamed from: f  reason: collision with root package name */
        public final int f2585f;
        public final int g;

        public b(byte[] bArr, int i2, int i3) {
            super(bArr);
            ByteString.a(i2, i2 + i3, bArr.length);
            this.f2585f = i2;
            this.g = i3;
        }

        public void a(byte[] bArr, int i2, int i3, int i4) {
            System.arraycopy(super.f2586e, this.f2585f + i2, bArr, i3, i4);
        }

        public byte c(int i2) {
            int i3 = this.g;
            if (((i3 - (i2 + 1)) | i2) >= 0) {
                return super.f2586e[this.f2585f + i2];
            }
            if (i2 < 0) {
                throw new ArrayIndexOutOfBoundsException(outline.b("Index < 0: ", i2));
            }
            throw new ArrayIndexOutOfBoundsException(outline.a("Index > length: ", i2, ", ", i3));
        }

        public int e() {
            return this.f2585f;
        }

        public int size() {
            return this.g;
        }
    }

    public interface c {
        byte[] a(byte[] bArr, int i2, int i3);
    }

    public interface d extends Iterator<Byte> {
    }

    public static final class e {
        public final CodedOutputStream a;
        public final byte[] b;

        public /* synthetic */ e(int i2, ByteString0 byteString0) {
            byte[] bArr = new byte[i2];
            this.b = bArr;
            this.a = CodedOutputStream.a(bArr);
        }
    }

    public static abstract class f extends ByteString {
        public Iterator iterator() {
            return new ByteString0(super);
        }
    }

    public static class g extends f {

        /* renamed from: e  reason: collision with root package name */
        public final byte[] f2586e;

        public g(byte[] bArr) {
            this.f2586e = bArr;
        }

        public void a(byte[] bArr, int i2, int i3, int i4) {
            System.arraycopy(this.f2586e, i2, bArr, i3, i4);
        }

        public byte c(int i2) {
            return this.f2586e[i2];
        }

        public int e() {
            return 0;
        }

        public final boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof ByteString) || size() != ((ByteString) obj).size()) {
                return false;
            }
            if (size() == 0) {
                return true;
            }
            if (!(obj instanceof g)) {
                return obj.equals(this);
            }
            g gVar = (g) obj;
            int i2 = this.b;
            int i3 = gVar.b;
            if (i2 != 0 && i3 != 0 && i2 != i3) {
                return false;
            }
            int size = size();
            if (size > gVar.size()) {
                throw new IllegalArgumentException("Length too large: " + size + size());
            } else if (0 + size <= gVar.size()) {
                byte[] bArr = this.f2586e;
                byte[] bArr2 = gVar.f2586e;
                int e2 = e() + size;
                int e3 = e();
                int e4 = gVar.e() + 0;
                while (e3 < e2) {
                    if (bArr[e3] != bArr2[e4]) {
                        return false;
                    }
                    e3++;
                    e4++;
                }
                return true;
            } else {
                throw new IllegalArgumentException("Ran off end of other: " + 0 + ", " + size + ", " + gVar.size());
            }
        }

        public int size() {
            return this.f2586e.length;
        }

        public final CodedInputStream c() {
            byte[] bArr = this.f2586e;
            int e2 = e();
            int size = size();
            CodedInputStream codedInputStream = new CodedInputStream(bArr, e2, size, true);
            try {
                codedInputStream.b(size);
                return codedInputStream;
            } catch (InvalidProtocolBufferException e3) {
                throw new IllegalArgumentException(e3);
            }
        }
    }

    public static final class h implements c {
        public /* synthetic */ h(ByteString0 byteString0) {
        }

        public byte[] a(byte[] bArr, int i2, int i3) {
            byte[] bArr2 = new byte[i3];
            System.arraycopy(bArr, i2, bArr2, 0, i3);
            return bArr2;
        }
    }

    static {
        Class<ByteString> cls = ByteString.class;
        boolean z = true;
        try {
            Class.forName("android.content.Context");
        } catch (ClassNotFoundException unused) {
            z = false;
        }
        d = z ? new h(null) : new a(null);
    }

    public static ByteString a(byte[] bArr) {
        return new g(d.a(bArr, 0, bArr.length));
    }

    public static ByteString b(byte[] bArr) {
        return new g(bArr);
    }

    public abstract void a(byte[] bArr, int i2, int i3, int i4);

    public abstract byte c(int i2);

    public abstract CodedInputStream c();

    public final byte[] d() {
        int size = size();
        if (size == 0) {
            return Internal.b;
        }
        byte[] bArr = new byte[size];
        a(bArr, 0, 0, size);
        return bArr;
    }

    public abstract boolean equals(Object obj);

    public final int hashCode() {
        int i2 = this.b;
        if (i2 == 0) {
            int size = size();
            g gVar = (g) this;
            i2 = Internal.a(size, gVar.f2586e, gVar.e() + 0, size);
            if (i2 == 0) {
                i2 = 1;
            }
            this.b = i2;
        }
        return i2;
    }

    public final boolean isEmpty() {
        return size() == 0;
    }

    public Iterator iterator() {
        return new ByteString0(this);
    }

    public abstract int size();

    public final String toString() {
        return String.format("<ByteString@%s size=%d>", Integer.toHexString(System.identityHashCode(this)), Integer.valueOf(size()));
    }

    public static ByteString a(byte[] bArr, int i2, int i3) {
        return new b(bArr, i2, i3);
    }

    public static ByteString a(String str) {
        return new g(str.getBytes(Internal.a));
    }

    public static int a(int i2, int i3, int i4) {
        int i5 = i3 - i2;
        if ((i2 | i3 | i5 | (i4 - i3)) >= 0) {
            return i5;
        }
        if (i2 < 0) {
            throw new IndexOutOfBoundsException(outline.b("Beginning index: ", i2, " < 0"));
        } else if (i3 < i2) {
            throw new IndexOutOfBoundsException(outline.a("Beginning index larger than ending index: ", i2, ", ", i3));
        } else {
            throw new IndexOutOfBoundsException(outline.a("End index: ", i3, " >= ", i4));
        }
    }

    public static e d(int i2) {
        return new e(i2, null);
    }
}
