package j.c.e;

import j.c.e.Internal;
import java.util.ArrayList;
import java.util.List;

public final class ProtobufArrayList<E> extends AbstractProtobufList<E> {
    public static final ProtobufArrayList<Object> d;
    public final List<E> c;

    static {
        ProtobufArrayList<Object> protobufArrayList = new ProtobufArrayList<>();
        d = protobufArrayList;
        super.b = false;
    }

    public ProtobufArrayList() {
        this.c = new ArrayList(10);
    }

    public void add(int i2, E e2) {
        c();
        this.c.add(i2, e2);
        this.modCount++;
    }

    public Internal.c c(int i2) {
        if (i2 >= size()) {
            ArrayList arrayList = new ArrayList(i2);
            arrayList.addAll(this.c);
            return new ProtobufArrayList(arrayList);
        }
        throw new IllegalArgumentException();
    }

    public E get(int i2) {
        return this.c.get(i2);
    }

    public E remove(int i2) {
        c();
        E remove = this.c.remove(i2);
        this.modCount++;
        return remove;
    }

    public E set(int i2, E e2) {
        c();
        E e3 = this.c.set(i2, e2);
        this.modCount++;
        return e3;
    }

    public int size() {
        return this.c.size();
    }

    public ProtobufArrayList(List<E> list) {
        this.c = list;
    }
}
