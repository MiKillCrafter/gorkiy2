package j.c.e;

import j.c.e.FieldSet;
import java.lang.Comparable;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

public class SmallSortedMap<K extends Comparable<K>, V> extends AbstractMap<K, V> {
    public final int b;
    public List<SmallSortedMap<K, V>.defpackage.c> c = Collections.emptyList();
    public Map<K, V> d = Collections.emptyMap();

    /* renamed from: e  reason: collision with root package name */
    public boolean f2596e;

    /* renamed from: f  reason: collision with root package name */
    public volatile SmallSortedMap<K, V>.e f2597f;

    public static class a extends SmallSortedMap<FieldDescriptorType, Object> {
        public a(int i2) {
            super(i2, null);
        }

        public void e() {
            if (!super.f2596e) {
                for (int i2 = 0; i2 < b(); i2++) {
                    Map.Entry a = a(i2);
                    if (((FieldSet.a) a.getKey()).k()) {
                        a.setValue(Collections.unmodifiableList((List) a.getValue()));
                    }
                }
                for (Map.Entry entry : c()) {
                    if (((FieldSet.a) entry.getKey()).k()) {
                        entry.setValue(Collections.unmodifiableList((List) entry.getValue()));
                    }
                }
            }
            SmallSortedMap.super.e();
        }

        public /* bridge */ /* synthetic */ Object put(Object obj, Object obj2) {
            return SmallSortedMap.super.put((FieldSet.a) obj, obj2);
        }
    }

    public static class b {
        public static final Iterator<Object> a = new a();
        public static final Iterable<Object> b = new C0030b();

        public static class a implements Iterator<Object> {
            public boolean hasNext() {
                return false;
            }

            public Object next() {
                throw new NoSuchElementException();
            }

            public void remove() {
                throw new UnsupportedOperationException();
            }
        }

        /* renamed from: j.c.e.SmallSortedMap$b$b  reason: collision with other inner class name */
        public static class C0030b implements Iterable<Object> {
            public Iterator<Object> iterator() {
                return b.a;
            }
        }
    }

    public class d implements Iterator<Map.Entry<K, V>> {
        public int b = -1;
        public boolean c;
        public Iterator<Map.Entry<K, V>> d;

        public /* synthetic */ d(a aVar) {
        }

        public final Iterator<Map.Entry<K, V>> b() {
            if (this.d == null) {
                this.d = SmallSortedMap.this.d.entrySet().iterator();
            }
            return this.d;
        }

        public boolean hasNext() {
            if (this.b + 1 < SmallSortedMap.this.c.size() || b().hasNext()) {
                return true;
            }
            return false;
        }

        public Object next() {
            this.c = true;
            int i2 = this.b + 1;
            this.b = i2;
            if (i2 < SmallSortedMap.this.c.size()) {
                return SmallSortedMap.this.c.get(this.b);
            }
            return (Map.Entry) b().next();
        }

        public void remove() {
            if (this.c) {
                this.c = false;
                SmallSortedMap.this.a();
                if (this.b < SmallSortedMap.this.c.size()) {
                    SmallSortedMap smallSortedMap = SmallSortedMap.this;
                    int i2 = this.b;
                    this.b = i2 - 1;
                    smallSortedMap.b(i2);
                    return;
                }
                b().remove();
                return;
            }
            throw new IllegalStateException("remove() was called before next()");
        }
    }

    public class e extends AbstractSet<Map.Entry<K, V>> {
        public /* synthetic */ e(a aVar) {
        }

        public boolean add(Object obj) {
            Map.Entry entry = (Map.Entry) obj;
            if (contains(entry)) {
                return false;
            }
            SmallSortedMap.this.put((Comparable) entry.getKey(), entry.getValue());
            return true;
        }

        public void clear() {
            SmallSortedMap.this.clear();
        }

        public boolean contains(Object obj) {
            Map.Entry entry = (Map.Entry) obj;
            Object obj2 = SmallSortedMap.this.get(entry.getKey());
            Object value = entry.getValue();
            return obj2 == value || (obj2 != null && obj2.equals(value));
        }

        public Iterator<Map.Entry<K, V>> iterator() {
            return new d(null);
        }

        public boolean remove(Object obj) {
            Map.Entry entry = (Map.Entry) obj;
            if (!contains(entry)) {
                return false;
            }
            SmallSortedMap.this.remove(entry.getKey());
            return true;
        }

        public int size() {
            return SmallSortedMap.this.size();
        }
    }

    public /* synthetic */ SmallSortedMap(int i2, a aVar) {
        this.b = i2;
    }

    public static <FieldDescriptorType extends FieldSet.a<FieldDescriptorType>> SmallSortedMap<FieldDescriptorType, Object> c(int i2) {
        return new a(i2);
    }

    public int b() {
        return this.c.size();
    }

    public void clear() {
        a();
        if (!this.c.isEmpty()) {
            this.c.clear();
        }
        if (!this.d.isEmpty()) {
            this.d.clear();
        }
    }

    public boolean containsKey(Object obj) {
        Comparable comparable = (Comparable) obj;
        return a(comparable) >= 0 || this.d.containsKey(comparable);
    }

    public final SortedMap<K, V> d() {
        a();
        if (this.d.isEmpty() && !(this.d instanceof TreeMap)) {
            this.d = new TreeMap();
        }
        return (SortedMap) this.d;
    }

    public void e() {
        Map<K, V> map;
        if (!this.f2596e) {
            if (this.d.isEmpty()) {
                map = Collections.emptyMap();
            } else {
                map = Collections.unmodifiableMap(this.d);
            }
            this.d = map;
            this.f2596e = true;
        }
    }

    public Set<Map.Entry<K, V>> entrySet() {
        if (this.f2597f == null) {
            this.f2597f = new e(null);
        }
        return this.f2597f;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SmallSortedMap)) {
            return super.equals(obj);
        }
        SmallSortedMap smallSortedMap = (SmallSortedMap) obj;
        int size = size();
        if (size != smallSortedMap.size()) {
            return false;
        }
        int b2 = b();
        if (b2 != smallSortedMap.b()) {
            return entrySet().equals(smallSortedMap.entrySet());
        }
        for (int i2 = 0; i2 < b2; i2++) {
            if (!a(i2).equals(smallSortedMap.a(i2))) {
                return false;
            }
        }
        if (b2 != size) {
            return this.d.equals(smallSortedMap.d);
        }
        return true;
    }

    public V get(Object obj) {
        Comparable comparable = (Comparable) obj;
        int a2 = a(comparable);
        if (a2 >= 0) {
            return this.c.get(a2).c;
        }
        return this.d.get(comparable);
    }

    public int hashCode() {
        int b2 = b();
        int i2 = 0;
        for (int i3 = 0; i3 < b2; i3++) {
            i2 += this.c.get(i3).hashCode();
        }
        return this.d.size() > 0 ? i2 + this.d.hashCode() : i2;
    }

    public V remove(Object obj) {
        a();
        Comparable comparable = (Comparable) obj;
        int a2 = a(comparable);
        if (a2 >= 0) {
            return b(a2);
        }
        if (this.d.isEmpty()) {
            return null;
        }
        return this.d.remove(comparable);
    }

    public int size() {
        return this.d.size() + this.c.size();
    }

    public Map.Entry<K, V> a(int i2) {
        return this.c.get(i2);
    }

    public final V b(int i2) {
        a();
        V v = this.c.remove(i2).c;
        if (!this.d.isEmpty()) {
            Iterator it = d().entrySet().iterator();
            this.c.add(new c(this, (Map.Entry) it.next()));
            it.remove();
        }
        return v;
    }

    public Iterable<Map.Entry<K, V>> c() {
        if (this.d.isEmpty()) {
            return b.b;
        }
        return this.d.entrySet();
    }

    /* renamed from: a */
    public V put(K k2, V v) {
        a();
        int a2 = a((Comparable) k2);
        if (a2 >= 0) {
            c cVar = this.c.get(a2);
            SmallSortedMap.this.a();
            V v2 = cVar.c;
            cVar.c = v;
            return v2;
        }
        a();
        if (this.c.isEmpty() && !(this.c instanceof ArrayList)) {
            this.c = new ArrayList(this.b);
        }
        int i2 = -(a2 + 1);
        if (i2 >= this.b) {
            return d().put(k2, v);
        }
        int size = this.c.size();
        int i3 = this.b;
        if (size == i3) {
            c remove = this.c.remove(i3 - 1);
            d().put(remove.b, remove.c);
        }
        this.c.add(i2, new c(k2, v));
        return null;
    }

    public class c implements Map.Entry<K, V>, Comparable<SmallSortedMap<K, V>.defpackage.c> {
        public final K b;
        public V c;

        public c(SmallSortedMap smallSortedMap, Map.Entry<K, V> entry) {
            V value = entry.getValue();
            SmallSortedMap.this = smallSortedMap;
            this.b = (Comparable) entry.getKey();
            this.c = value;
        }

        public int compareTo(Object obj) {
            return this.b.compareTo(((c) obj).b);
        }

        public boolean equals(Object obj) {
            boolean z;
            boolean z2;
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            K k2 = this.b;
            Object key = entry.getKey();
            if (k2 == null) {
                z = key == null;
            } else {
                z = k2.equals(key);
            }
            if (z) {
                V v = this.c;
                Object value = entry.getValue();
                if (v == null) {
                    z2 = value == null;
                } else {
                    z2 = v.equals(value);
                }
                if (z2) {
                    return true;
                }
            }
            return false;
        }

        public Object getKey() {
            return this.b;
        }

        public V getValue() {
            return this.c;
        }

        public int hashCode() {
            K k2 = this.b;
            int i2 = 0;
            int hashCode = k2 == null ? 0 : k2.hashCode();
            V v = this.c;
            if (v != null) {
                i2 = v.hashCode();
            }
            return hashCode ^ i2;
        }

        public V setValue(V v) {
            SmallSortedMap.this.a();
            V v2 = this.c;
            this.c = v;
            return v2;
        }

        public String toString() {
            return ((Object) this.b) + "=" + ((Object) this.c);
        }

        public c(K k2, V v) {
            this.b = k2;
            this.c = v;
        }
    }

    public final int a(Comparable comparable) {
        int size = this.c.size() - 1;
        if (size >= 0) {
            int compareTo = comparable.compareTo(this.c.get(size).b);
            if (compareTo > 0) {
                return -(size + 2);
            }
            if (compareTo == 0) {
                return size;
            }
        }
        int i2 = 0;
        while (i2 <= size) {
            int i3 = (i2 + size) / 2;
            int compareTo2 = comparable.compareTo(this.c.get(i3).b);
            if (compareTo2 < 0) {
                size = i3 - 1;
            } else if (compareTo2 <= 0) {
                return i3;
            } else {
                i2 = i3 + 1;
            }
        }
        return -(i2 + 1);
    }

    public final void a() {
        if (this.f2596e) {
            throw new UnsupportedOperationException();
        }
    }
}
