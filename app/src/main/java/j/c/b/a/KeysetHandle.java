package j.c.b.a;

import com.google.protobuf.InvalidProtocolBufferException;
import j.c.a.a.c.n.c;
import j.c.b.a.x.a.SharedPrefKeysetReader;
import j.c.b.a.x.a.SharedPrefKeysetWriter;
import j.c.b.a.z.EncryptedKeyset;
import j.c.b.a.z.Keyset;
import j.c.b.a.z.KeysetInfo;
import j.c.e.ByteString;
import j.c.e.GeneratedMessageLite;
import java.security.GeneralSecurityException;

public final class KeysetHandle {
    public Keyset a;

    public KeysetHandle(Keyset keyset) {
        this.a = keyset;
    }

    public static final KeysetHandle a(SharedPrefKeysetReader sharedPrefKeysetReader, Aead aead) {
        EncryptedKeyset encryptedKeyset = (EncryptedKeyset) GeneratedMessageLite.a(EncryptedKeyset.g, sharedPrefKeysetReader.a());
        if (encryptedKeyset == null || encryptedKeyset.f2468e.size() == 0) {
            throw new GeneralSecurityException("empty keyset");
        }
        try {
            Keyset a2 = Keyset.a(aead.b(encryptedKeyset.f2468e.d(), new byte[0]));
            if (a2 != null && a2.g.size() > 0) {
                return new KeysetHandle(a2);
            }
            throw new GeneralSecurityException("empty keyset");
        } catch (InvalidProtocolBufferException unused) {
            throw new GeneralSecurityException("invalid keyset, corrupted key material");
        }
    }

    public String toString() {
        return Util.a(this.a).toString();
    }

    public void a(KeysetWriter keysetWriter, Aead aead) {
        Keyset keyset = this.a;
        byte[] a2 = aead.a(keyset.b(), new byte[0]);
        try {
            if (Keyset.a(aead.b(a2, new byte[0])).equals(keyset)) {
                EncryptedKeyset.b bVar = (EncryptedKeyset.b) EncryptedKeyset.g.e();
                ByteString a3 = ByteString.a(a2);
                bVar.m();
                EncryptedKeyset.a((EncryptedKeyset) bVar.c, a3);
                KeysetInfo a4 = Util.a(keyset);
                bVar.m();
                EncryptedKeyset.a((EncryptedKeyset) bVar.c, a4);
                SharedPrefKeysetWriter sharedPrefKeysetWriter = (SharedPrefKeysetWriter) keysetWriter;
                sharedPrefKeysetWriter.a.putString(sharedPrefKeysetWriter.b, c.c(((EncryptedKeyset) bVar.k()).b())).apply();
                return;
            }
            throw new GeneralSecurityException("cannot encrypt keyset");
        } catch (InvalidProtocolBufferException unused) {
            throw new GeneralSecurityException("invalid keyset, corrupted key material");
        }
    }

    public static final KeysetHandle a(Keyset keyset) {
        if (keyset != null && keyset.g.size() > 0) {
            return new KeysetHandle(keyset);
        }
        throw new GeneralSecurityException("empty keyset");
    }
}
