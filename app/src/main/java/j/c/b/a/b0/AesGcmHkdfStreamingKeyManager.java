package j.c.b.a.b0;

import com.google.protobuf.InvalidProtocolBufferException;
import j.c.a.a.c.n.c;
import j.c.b.a.KeyManager;
import j.c.b.a.c0.AesGcmHkdfStreaming;
import j.c.b.a.c0.NonceBasedStreamingAead;
import j.c.b.a.c0.Random;
import j.c.b.a.c0.Validators;
import j.c.b.a.c0.f0;
import j.c.b.a.z.AesGcmHkdfStreamingKey;
import j.c.b.a.z.AesGcmHkdfStreamingKeyFormat;
import j.c.b.a.z.AesGcmHkdfStreamingParams;
import j.c.b.a.z.HashType;
import j.c.b.a.z.KeyData;
import j.c.e.ByteString;
import j.c.e.GeneratedMessageLite;
import j.c.e.MessageLite;
import j.c.e.f;
import java.security.GeneralSecurityException;

public class AesGcmHkdfStreamingKeyManager implements KeyManager<f0> {
    public String a() {
        return "type.googleapis.com/google.crypto.tink.AesGcmHkdfStreamingKey";
    }

    public int b() {
        return 0;
    }

    /* JADX WARN: Type inference failed for: r4v6, types: [j.c.e.MessageLite, j.c.e.GeneratedMessageLite] */
    public MessageLite b(MessageLite messageLite) {
        if (messageLite instanceof AesGcmHkdfStreamingKeyFormat) {
            AesGcmHkdfStreamingKeyFormat aesGcmHkdfStreamingKeyFormat = (AesGcmHkdfStreamingKeyFormat) messageLite;
            if (aesGcmHkdfStreamingKeyFormat.f2420f >= 16) {
                a(aesGcmHkdfStreamingKeyFormat.g());
                AesGcmHkdfStreamingKey.b bVar = (AesGcmHkdfStreamingKey.b) AesGcmHkdfStreamingKey.h.e();
                ByteString a = ByteString.a(Random.a(aesGcmHkdfStreamingKeyFormat.f2420f));
                bVar.m();
                bVar.c.g = a;
                AesGcmHkdfStreamingParams g = aesGcmHkdfStreamingKeyFormat.g();
                bVar.m();
                AesGcmHkdfStreamingKey.a(bVar.c, g);
                bVar.m();
                bVar.c.f2417e = 0;
                return bVar.k();
            }
            throw new GeneralSecurityException("key_size must be at least 16 bytes");
        }
        throw new GeneralSecurityException("expected AesGcmHkdfStreamingKeyFormat proto");
    }

    /* JADX WARN: Type inference failed for: r2v3, types: [j.c.e.MessageLite, j.c.b.a.z.AesGcmHkdfStreamingKey] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T
     arg types: [j.c.b.a.z.AesGcmHkdfStreamingKey, j.c.e.ByteString]
     candidates:
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, byte[]):T
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite$j, java.lang.Object):java.lang.Object
      j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T */
    public Object c(ByteString byteString) {
        try {
            return a((MessageLite) ((AesGcmHkdfStreamingKey) GeneratedMessageLite.a((GeneratedMessageLite) AesGcmHkdfStreamingKey.h, (f) byteString)));
        } catch (InvalidProtocolBufferException unused) {
            throw new GeneralSecurityException("expected AesGcmHkdfStreamingKey proto");
        }
    }

    public final void a(AesGcmHkdfStreamingParams aesGcmHkdfStreamingParams) {
        Validators.a(aesGcmHkdfStreamingParams.f2423f);
        HashType a = HashType.a(aesGcmHkdfStreamingParams.g);
        if (a == null) {
            a = HashType.UNRECOGNIZED;
        }
        if (a == HashType.UNKNOWN_HASH) {
            throw new GeneralSecurityException("unknown HKDF hash type");
        } else if (aesGcmHkdfStreamingParams.f2422e < aesGcmHkdfStreamingParams.f2423f + 8) {
            throw new GeneralSecurityException("ciphertext_segment_size must be at least (derived_key_size + 8)");
        }
    }

    public NonceBasedStreamingAead a(MessageLite messageLite) {
        if (messageLite instanceof AesGcmHkdfStreamingKey) {
            AesGcmHkdfStreamingKey aesGcmHkdfStreamingKey = (AesGcmHkdfStreamingKey) messageLite;
            Validators.a(aesGcmHkdfStreamingKey.f2417e, 0);
            a(aesGcmHkdfStreamingKey.g());
            byte[] d = aesGcmHkdfStreamingKey.g.d();
            HashType a = HashType.a(aesGcmHkdfStreamingKey.g().g);
            if (a == null) {
                a = HashType.UNRECOGNIZED;
            }
            return new AesGcmHkdfStreaming(d, c.c(a), aesGcmHkdfStreamingKey.g().f2423f, aesGcmHkdfStreamingKey.g().f2422e, 0);
        }
        throw new GeneralSecurityException("expected AesGcmHkdfStreamingKey proto");
    }

    public KeyData b(ByteString byteString) {
        KeyData.b g = KeyData.g();
        g.m();
        KeyData.a((KeyData) g.c, "type.googleapis.com/google.crypto.tink.AesGcmHkdfStreamingKey");
        ByteString a = ((AesGcmHkdfStreamingKey) a(byteString)).a();
        g.m();
        KeyData.a((KeyData) g.c, a);
        g.a(KeyData.c.SYMMETRIC);
        return (KeyData) g.k();
    }

    public boolean a(String str) {
        return str.equals("type.googleapis.com/google.crypto.tink.AesGcmHkdfStreamingKey");
    }

    /* JADX WARN: Type inference failed for: r3v3, types: [j.c.e.MessageLite, j.c.b.a.z.AesGcmHkdfStreamingKeyFormat] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T
     arg types: [j.c.b.a.z.AesGcmHkdfStreamingKeyFormat, j.c.e.ByteString]
     candidates:
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, byte[]):T
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite$j, java.lang.Object):java.lang.Object
      j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T */
    public MessageLite a(ByteString byteString) {
        try {
            return b((MessageLite) ((AesGcmHkdfStreamingKeyFormat) GeneratedMessageLite.a((GeneratedMessageLite) AesGcmHkdfStreamingKeyFormat.g, (f) byteString)));
        } catch (InvalidProtocolBufferException e2) {
            throw new GeneralSecurityException("expected serialized AesGcmHkdfStreamingKeyFormat proto", e2);
        }
    }
}
