package j.c.b.a.t;

import com.google.protobuf.InvalidProtocolBufferException;
import j.c.b.a.Aead;
import j.c.b.a.KeyManager;
import j.c.b.a.Mac;
import j.c.b.a.Registry;
import j.c.b.a.a;
import j.c.b.a.c0.EncryptThenAuthenticate;
import j.c.b.a.c0.IndCpaCipher;
import j.c.b.a.c0.Validators;
import j.c.b.a.z.AesCtrHmacAeadKey;
import j.c.b.a.z.AesCtrHmacAeadKeyFormat;
import j.c.b.a.z.AesCtrKey;
import j.c.b.a.z.HmacKey;
import j.c.b.a.z.KeyData;
import j.c.e.ByteString;
import j.c.e.GeneratedMessageLite;
import j.c.e.MessageLite;
import j.c.e.f;
import j.c.e.o;
import java.security.GeneralSecurityException;
import java.util.logging.Logger;

public class AesCtrHmacAeadKeyManager implements KeyManager<a> {
    static {
        Logger.getLogger(AesCtrHmacAeadKeyManager.class.getName());
    }

    public AesCtrHmacAeadKeyManager() {
        Registry.a(new AesCtrKeyManager());
    }

    public String a() {
        return "type.googleapis.com/google.crypto.tink.AesCtrHmacAeadKey";
    }

    public int b() {
        return 0;
    }

    /* JADX WARN: Type inference failed for: r0v2, types: [j.c.e.MessageLite, j.c.b.a.z.AesCtrKeyFormat] */
    /* JADX WARN: Type inference failed for: r4v3, types: [j.c.b.a.z.HmacKeyFormat, j.c.e.MessageLite] */
    /* JADX WARN: Type inference failed for: r4v7, types: [j.c.e.MessageLite, j.c.e.GeneratedMessageLite] */
    public MessageLite b(MessageLite messageLite) {
        if (messageLite instanceof AesCtrHmacAeadKeyFormat) {
            AesCtrHmacAeadKeyFormat aesCtrHmacAeadKeyFormat = (AesCtrHmacAeadKeyFormat) messageLite;
            AesCtrHmacAeadKey.b bVar = (AesCtrHmacAeadKey.b) AesCtrHmacAeadKey.h.e();
            bVar.m();
            AesCtrHmacAeadKey.a(bVar.c, (AesCtrKey) Registry.a("type.googleapis.com/google.crypto.tink.AesCtrKey", (o) aesCtrHmacAeadKeyFormat.g()));
            bVar.m();
            AesCtrHmacAeadKey.a(bVar.c, (HmacKey) Registry.a("type.googleapis.com/google.crypto.tink.HmacKey", (o) aesCtrHmacAeadKeyFormat.h()));
            bVar.m();
            bVar.c.f2389e = 0;
            return bVar.k();
        }
        throw new GeneralSecurityException("expected AesCtrHmacAeadKeyFormat proto");
    }

    /* JADX WARN: Type inference failed for: r3v3, types: [j.c.e.MessageLite, j.c.b.a.z.AesCtrHmacAeadKey] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T
     arg types: [j.c.b.a.z.AesCtrHmacAeadKey, j.c.e.ByteString]
     candidates:
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, byte[]):T
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite$j, java.lang.Object):java.lang.Object
      j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T */
    public Object c(ByteString byteString) {
        try {
            return a((MessageLite) ((AesCtrHmacAeadKey) GeneratedMessageLite.a((GeneratedMessageLite) AesCtrHmacAeadKey.h, (f) byteString)));
        } catch (InvalidProtocolBufferException e2) {
            throw new GeneralSecurityException("expected serialized AesCtrHmacAeadKey proto", e2);
        }
    }

    /* JADX WARN: Type inference failed for: r1v1, types: [j.c.b.a.z.AesCtrKey, j.c.e.MessageLite] */
    /* JADX WARN: Type inference failed for: r2v2, types: [j.c.e.MessageLite, j.c.b.a.z.HmacKey] */
    public Aead a(MessageLite messageLite) {
        if (messageLite instanceof AesCtrHmacAeadKey) {
            AesCtrHmacAeadKey aesCtrHmacAeadKey = (AesCtrHmacAeadKey) messageLite;
            Validators.a(aesCtrHmacAeadKey.f2389e, 0);
            return new EncryptThenAuthenticate((IndCpaCipher) Registry.b("type.googleapis.com/google.crypto.tink.AesCtrKey").a((o) aesCtrHmacAeadKey.g()), (Mac) Registry.b("type.googleapis.com/google.crypto.tink.HmacKey").a((o) aesCtrHmacAeadKey.h()), aesCtrHmacAeadKey.h().g().f2476f);
        }
        throw new GeneralSecurityException("expected AesCtrHmacAeadKey proto");
    }

    public KeyData b(ByteString byteString) {
        KeyData.b g = KeyData.g();
        g.m();
        KeyData.a((KeyData) g.c, "type.googleapis.com/google.crypto.tink.AesCtrHmacAeadKey");
        ByteString a = ((AesCtrHmacAeadKey) a(byteString)).a();
        g.m();
        KeyData.a((KeyData) g.c, a);
        g.a(KeyData.c.SYMMETRIC);
        return (KeyData) g.k();
    }

    public boolean a(String str) {
        return str.equals("type.googleapis.com/google.crypto.tink.AesCtrHmacAeadKey");
    }

    /* JADX WARN: Type inference failed for: r3v3, types: [j.c.b.a.z.AesCtrHmacAeadKeyFormat, j.c.e.MessageLite] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T
     arg types: [j.c.b.a.z.AesCtrHmacAeadKeyFormat, j.c.e.ByteString]
     candidates:
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, byte[]):T
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite$j, java.lang.Object):java.lang.Object
      j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T */
    public MessageLite a(ByteString byteString) {
        try {
            return b((MessageLite) ((AesCtrHmacAeadKeyFormat) GeneratedMessageLite.a((GeneratedMessageLite) AesCtrHmacAeadKeyFormat.g, (f) byteString)));
        } catch (InvalidProtocolBufferException e2) {
            throw new GeneralSecurityException("expected serialized AesCtrHmacAeadKeyFormat proto", e2);
        }
    }
}
