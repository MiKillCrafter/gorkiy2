package j.c.b.a.c0;

import j.c.b.a.c0.Ed25519;
import java.lang.reflect.Array;
import java.math.BigInteger;

public final class Ed25519Constants {
    public static final long[] a = Field25519.a(a(f2379e));
    public static final Ed25519.a[][] b = ((Ed25519.a[][]) Array.newInstance(Ed25519.a.class, 32, 8));
    public static final Ed25519.a[] c = new Ed25519.a[8];
    public static final BigInteger d = BigInteger.valueOf(2).pow(255).subtract(BigInteger.valueOf(19));

    /* renamed from: e  reason: collision with root package name */
    public static final BigInteger f2379e = BigInteger.valueOf(-121665).multiply(BigInteger.valueOf(121666).modInverse(d)).mod(d);

    /* renamed from: f  reason: collision with root package name */
    public static final BigInteger f2380f = BigInteger.valueOf(2).multiply(f2379e).mod(d);
    public static final BigInteger g = BigInteger.valueOf(2).modPow(d.subtract(BigInteger.ONE).divide(BigInteger.valueOf(4)), d);

    public static class b {
        public BigInteger a;
        public BigInteger b;

        public /* synthetic */ b(a aVar) {
        }
    }

    static {
        b bVar = new b(null);
        BigInteger mod = BigInteger.valueOf(4).multiply(BigInteger.valueOf(5).modInverse(d)).mod(d);
        bVar.b = mod;
        BigInteger multiply = mod.pow(2).subtract(BigInteger.ONE).multiply(f2379e.multiply(mod.pow(2)).add(BigInteger.ONE).modInverse(d));
        BigInteger modPow = multiply.modPow(d.add(BigInteger.valueOf(3)).divide(BigInteger.valueOf(8)), d);
        if (!modPow.pow(2).subtract(multiply).mod(d).equals(BigInteger.ZERO)) {
            modPow = modPow.multiply(g).mod(d);
        }
        if (modPow.testBit(0)) {
            modPow = d.subtract(modPow);
        }
        bVar.a = modPow;
        Field25519.a(a(f2380f));
        Field25519.a(a(g));
        b bVar2 = bVar;
        for (int i2 = 0; i2 < 32; i2++) {
            b bVar3 = bVar2;
            for (int i3 = 0; i3 < 8; i3++) {
                b[i2][i3] = a(bVar3);
                bVar3 = a(bVar3, bVar2);
            }
            for (int i4 = 0; i4 < 8; i4++) {
                bVar2 = a(bVar2, bVar2);
            }
        }
        b a2 = a(bVar, bVar);
        for (int i5 = 0; i5 < 8; i5++) {
            c[i5] = a(bVar);
            bVar = a(bVar, a2);
        }
    }

    public static b a(b bVar, b bVar2) {
        b bVar3 = new b(null);
        BigInteger mod = f2379e.multiply(bVar.a.multiply(bVar2.a).multiply(bVar.b).multiply(bVar2.b)).mod(d);
        bVar3.a = bVar.a.multiply(bVar2.b).add(bVar2.a.multiply(bVar.b)).multiply(BigInteger.ONE.add(mod).modInverse(d)).mod(d);
        bVar3.b = bVar.b.multiply(bVar2.b).add(bVar.a.multiply(bVar2.a)).multiply(BigInteger.ONE.subtract(mod).modInverse(d)).mod(d);
        return bVar3;
    }

    public static byte[] a(BigInteger bigInteger) {
        byte[] bArr = new byte[32];
        byte[] byteArray = bigInteger.toByteArray();
        System.arraycopy(byteArray, 0, bArr, 32 - byteArray.length, byteArray.length);
        for (int i2 = 0; i2 < 16; i2++) {
            byte b2 = bArr[i2];
            int i3 = (32 - i2) - 1;
            bArr[i2] = bArr[i3];
            bArr[i3] = b2;
        }
        return bArr;
    }

    public static Ed25519.a a(b bVar) {
        return new Ed25519.a(Field25519.a(a(bVar.b.add(bVar.a).mod(d))), Field25519.a(a(bVar.b.subtract(bVar.a).mod(d))), Field25519.a(a(f2380f.multiply(bVar.a).multiply(bVar.b).mod(d))));
    }
}
