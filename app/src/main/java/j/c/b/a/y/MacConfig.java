package j.c.b.a.y;

import j.c.a.a.c.n.c;
import j.c.b.a.Registry;
import j.c.b.a.z.RegistryConfig;
import java.security.GeneralSecurityException;

public final class MacConfig {
    @Deprecated
    public static final RegistryConfig a;
    public static final RegistryConfig b;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.String, java.lang.String, java.lang.String, int, boolean):j.c.b.a.z.KeyTypeEntry
     arg types: [java.lang.String, java.lang.String, java.lang.String, int, int]
     candidates:
      j.c.a.a.c.n.c.a(int, byte[], int, int, j.c.a.a.f.e.s2):int
      j.c.a.a.c.n.c.a(j.c.a.a.f.e.t5, byte[], int, int, j.c.a.a.f.e.s2):int
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String):java.security.spec.ECParameterSpec
      j.c.a.a.c.n.c.a(byte, byte, byte, char[], int):void
      j.c.a.a.c.n.c.a(byte[], int, byte[], int, int):byte[]
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String, java.lang.String, int, boolean):j.c.b.a.z.KeyTypeEntry */
    static {
        RegistryConfig.b g = RegistryConfig.g();
        g.m();
        RegistryConfig.a((RegistryConfig) g.c, "TINK_MAC_1_0_0");
        g.a(c.a("TinkMac", "Mac", "HmacKey", 0, true));
        a = (RegistryConfig) g.k();
        RegistryConfig.b g2 = RegistryConfig.g();
        g2.a(a);
        RegistryConfig.b bVar = g2;
        bVar.m();
        RegistryConfig.a((RegistryConfig) bVar.c, "TINK_MAC_1_1_0");
        RegistryConfig registryConfig = (RegistryConfig) bVar.k();
        RegistryConfig.b g3 = RegistryConfig.g();
        g3.a(a);
        RegistryConfig.b bVar2 = g3;
        bVar2.m();
        RegistryConfig.a((RegistryConfig) bVar2.c, "TINK_MAC");
        b = (RegistryConfig) bVar2.k();
        try {
            Registry.a("TinkMac", new MacCatalogue());
            c.a(b);
        } catch (GeneralSecurityException e2) {
            throw new ExceptionInInitializerError(e2);
        }
    }

    public static void a() {
        Registry.a("TinkMac", new MacCatalogue());
        c.a(b);
    }
}
