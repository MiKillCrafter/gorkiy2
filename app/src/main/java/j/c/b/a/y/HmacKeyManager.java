package j.c.b.a.y;

import com.google.protobuf.InvalidProtocolBufferException;
import j.c.b.a.KeyManager;
import j.c.b.a.Mac;
import j.c.b.a.c0.MacJce;
import j.c.b.a.c0.Random;
import j.c.b.a.c0.Validators;
import j.c.b.a.m;
import j.c.b.a.z.HashType;
import j.c.b.a.z.HmacKey;
import j.c.b.a.z.HmacKeyFormat;
import j.c.b.a.z.HmacParams;
import j.c.b.a.z.KeyData;
import j.c.e.ByteString;
import j.c.e.GeneratedMessageLite;
import j.c.e.MessageLite;
import j.c.e.f;
import java.security.GeneralSecurityException;
import javax.crypto.spec.SecretKeySpec;

public class HmacKeyManager implements KeyManager<m> {
    public String a() {
        return "type.googleapis.com/google.crypto.tink.HmacKey";
    }

    public int b() {
        return 0;
    }

    /* JADX WARN: Type inference failed for: r4v7, types: [j.c.e.MessageLite, j.c.e.GeneratedMessageLite] */
    public MessageLite b(MessageLite messageLite) {
        if (messageLite instanceof HmacKeyFormat) {
            HmacKeyFormat hmacKeyFormat = (HmacKeyFormat) messageLite;
            if (hmacKeyFormat.f2474f >= 16) {
                a(hmacKeyFormat.g());
                HmacKey.b bVar = (HmacKey.b) HmacKey.h.e();
                bVar.m();
                bVar.c.f2471e = 0;
                HmacParams g = hmacKeyFormat.g();
                bVar.m();
                HmacKey.a(bVar.c, g);
                ByteString a = ByteString.a(Random.a(hmacKeyFormat.f2474f));
                bVar.m();
                HmacKey.a(bVar.c, a);
                return bVar.k();
            }
            throw new GeneralSecurityException("key too short");
        }
        throw new GeneralSecurityException("expected HmacKeyFormat proto");
    }

    /* JADX WARN: Type inference failed for: r3v3, types: [j.c.e.MessageLite, j.c.b.a.z.HmacKey] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T
     arg types: [j.c.b.a.z.HmacKey, j.c.e.ByteString]
     candidates:
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, byte[]):T
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite$j, java.lang.Object):java.lang.Object
      j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T */
    public Object c(ByteString byteString) {
        try {
            return a((MessageLite) ((HmacKey) GeneratedMessageLite.a((GeneratedMessageLite) HmacKey.h, (f) byteString)));
        } catch (InvalidProtocolBufferException e2) {
            throw new GeneralSecurityException("expected serialized HmacKey proto", e2);
        }
    }

    public Mac a(MessageLite messageLite) {
        if (messageLite instanceof HmacKey) {
            HmacKey hmacKey = (HmacKey) messageLite;
            Validators.a(hmacKey.f2471e, 0);
            if (hmacKey.g.size() >= 16) {
                a(hmacKey.g());
                HashType g = hmacKey.g().g();
                SecretKeySpec secretKeySpec = new SecretKeySpec(hmacKey.g.d(), "HMAC");
                int i2 = hmacKey.g().f2476f;
                int ordinal = g.ordinal();
                if (ordinal == 1) {
                    return new MacJce("HMACSHA1", secretKeySpec, i2);
                }
                if (ordinal == 2) {
                    return new MacJce("HMACSHA256", secretKeySpec, i2);
                }
                if (ordinal == 3) {
                    return new MacJce("HMACSHA512", secretKeySpec, i2);
                }
                throw new GeneralSecurityException("unknown hash");
            }
            throw new GeneralSecurityException("key too short");
        }
        throw new GeneralSecurityException("expected HmacKey proto");
    }

    public KeyData b(ByteString byteString) {
        KeyData.b g = KeyData.g();
        g.m();
        KeyData.a((KeyData) g.c, "type.googleapis.com/google.crypto.tink.HmacKey");
        ByteString a = ((HmacKey) a(byteString)).a();
        g.m();
        KeyData.a((KeyData) g.c, a);
        g.a(KeyData.c.SYMMETRIC);
        return (KeyData) g.k();
    }

    public final void a(HmacParams hmacParams) {
        if (hmacParams.f2476f >= 10) {
            int ordinal = hmacParams.g().ordinal();
            if (ordinal != 1) {
                if (ordinal != 2) {
                    if (ordinal != 3) {
                        throw new GeneralSecurityException("unknown hash type");
                    } else if (hmacParams.f2476f > 64) {
                        throw new GeneralSecurityException("tag size too big");
                    }
                } else if (hmacParams.f2476f > 32) {
                    throw new GeneralSecurityException("tag size too big");
                }
            } else if (hmacParams.f2476f > 20) {
                throw new GeneralSecurityException("tag size too big");
            }
        } else {
            throw new GeneralSecurityException("tag size too small");
        }
    }

    /* JADX WARN: Type inference failed for: r3v3, types: [j.c.b.a.z.HmacKeyFormat, j.c.e.MessageLite] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T
     arg types: [j.c.b.a.z.HmacKeyFormat, j.c.e.ByteString]
     candidates:
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, byte[]):T
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite$j, java.lang.Object):java.lang.Object
      j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T */
    public MessageLite a(ByteString byteString) {
        try {
            return b((MessageLite) ((HmacKeyFormat) GeneratedMessageLite.a((GeneratedMessageLite) HmacKeyFormat.g, (f) byteString)));
        } catch (InvalidProtocolBufferException e2) {
            throw new GeneralSecurityException("expected serialized HmacKeyFormat proto", e2);
        }
    }

    public boolean a(String str) {
        return str.equals("type.googleapis.com/google.crypto.tink.HmacKey");
    }
}
