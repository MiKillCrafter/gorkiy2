package j.c.b.a;

public interface DeterministicAead {
    byte[] a(byte[] bArr, byte[] bArr2);

    byte[] b(byte[] bArr, byte[] bArr2);
}
