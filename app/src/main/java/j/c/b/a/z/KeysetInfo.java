package j.c.b.a.z;

import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.InvalidProtocolBufferException;
import j.c.b.a.z.e2;
import j.c.e.AbstractProtobufList;
import j.c.e.CodedInputStream;
import j.c.e.ExtensionRegistryLite;
import j.c.e.GeneratedMessageLite;
import j.c.e.Internal;
import j.c.e.MessageLiteOrBuilder;
import j.c.e.Parser;
import j.c.e.ProtobufArrayList;
import java.io.IOException;

public final class KeysetInfo extends GeneratedMessageLite<e2, e2.b> implements f2 {
    public static final KeysetInfo h;

    /* renamed from: i  reason: collision with root package name */
    public static volatile Parser<e2> f2495i;

    /* renamed from: e  reason: collision with root package name */
    public int f2496e;

    /* renamed from: f  reason: collision with root package name */
    public int f2497f;
    public Internal.c<e2.c> g = ProtobufArrayList.d;

    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        /* JADX WARNING: Can't wrap try/catch for region: R(20:0|(2:1|2)|3|5|6|7|8|9|11|12|13|14|15|(2:17|18)|19|21|22|23|24|26) */
        /* JADX WARNING: Can't wrap try/catch for region: R(22:0|1|2|3|5|6|7|8|9|11|12|13|14|15|17|18|19|21|22|23|24|26) */
        /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0025 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0039 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0017 */
        static {
            /*
                j.c.e.GeneratedMessageLite$j[] r0 = j.c.e.GeneratedMessageLite.j.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                j.c.b.a.z.KeysetInfo.a.a = r0
                r1 = 1
                r2 = 4
                j.c.e.GeneratedMessageLite$j r3 = j.c.e.GeneratedMessageLite.j.NEW_MUTABLE_INSTANCE     // Catch:{ NoSuchFieldError -> 0x000f }
                r0[r2] = r1     // Catch:{ NoSuchFieldError -> 0x000f }
            L_0x000f:
                r0 = 2
                int[] r3 = j.c.b.a.z.KeysetInfo.a.a     // Catch:{ NoSuchFieldError -> 0x0017 }
                j.c.e.GeneratedMessageLite$j r4 = j.c.e.GeneratedMessageLite.j.IS_INITIALIZED     // Catch:{ NoSuchFieldError -> 0x0017 }
                r4 = 0
                r3[r4] = r0     // Catch:{ NoSuchFieldError -> 0x0017 }
            L_0x0017:
                int[] r3 = j.c.b.a.z.KeysetInfo.a.a     // Catch:{ NoSuchFieldError -> 0x001e }
                j.c.e.GeneratedMessageLite$j r4 = j.c.e.GeneratedMessageLite.j.MAKE_IMMUTABLE     // Catch:{ NoSuchFieldError -> 0x001e }
                r4 = 3
                r3[r4] = r4     // Catch:{ NoSuchFieldError -> 0x001e }
            L_0x001e:
                r3 = 5
                int[] r4 = j.c.b.a.z.KeysetInfo.a.a     // Catch:{ NoSuchFieldError -> 0x0025 }
                j.c.e.GeneratedMessageLite$j r5 = j.c.e.GeneratedMessageLite.j.NEW_BUILDER     // Catch:{ NoSuchFieldError -> 0x0025 }
                r4[r3] = r2     // Catch:{ NoSuchFieldError -> 0x0025 }
            L_0x0025:
                int[] r2 = j.c.b.a.z.KeysetInfo.a.a     // Catch:{ NoSuchFieldError -> 0x002b }
                j.c.e.GeneratedMessageLite$j r4 = j.c.e.GeneratedMessageLite.j.VISIT     // Catch:{ NoSuchFieldError -> 0x002b }
                r2[r1] = r3     // Catch:{ NoSuchFieldError -> 0x002b }
            L_0x002b:
                r1 = 6
                int[] r2 = j.c.b.a.z.KeysetInfo.a.a     // Catch:{ NoSuchFieldError -> 0x0032 }
                j.c.e.GeneratedMessageLite$j r3 = j.c.e.GeneratedMessageLite.j.MERGE_FROM_STREAM     // Catch:{ NoSuchFieldError -> 0x0032 }
                r2[r0] = r1     // Catch:{ NoSuchFieldError -> 0x0032 }
            L_0x0032:
                r0 = 7
                int[] r2 = j.c.b.a.z.KeysetInfo.a.a     // Catch:{ NoSuchFieldError -> 0x0039 }
                j.c.e.GeneratedMessageLite$j r3 = j.c.e.GeneratedMessageLite.j.GET_DEFAULT_INSTANCE     // Catch:{ NoSuchFieldError -> 0x0039 }
                r2[r1] = r0     // Catch:{ NoSuchFieldError -> 0x0039 }
            L_0x0039:
                int[] r1 = j.c.b.a.z.KeysetInfo.a.a     // Catch:{ NoSuchFieldError -> 0x0041 }
                j.c.e.GeneratedMessageLite$j r2 = j.c.e.GeneratedMessageLite.j.GET_PARSER     // Catch:{ NoSuchFieldError -> 0x0041 }
                r2 = 8
                r1[r0] = r2     // Catch:{ NoSuchFieldError -> 0x0041 }
            L_0x0041:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: j.c.b.a.z.KeysetInfo.a.<clinit>():void");
        }
    }

    public static final class c extends GeneratedMessageLite<e2.c, e2.c.a> implements e2.d {

        /* renamed from: i  reason: collision with root package name */
        public static final c f2498i;

        /* renamed from: j  reason: collision with root package name */
        public static volatile Parser<e2.c> f2499j;

        /* renamed from: e  reason: collision with root package name */
        public String f2500e = "";

        /* renamed from: f  reason: collision with root package name */
        public int f2501f;
        public int g;
        public int h;

        static {
            c cVar = new c();
            f2498i = cVar;
            super.f();
        }

        public static /* synthetic */ void a(c cVar, KeyStatusType keyStatusType) {
            if (cVar == null) {
                throw null;
            } else if (keyStatusType != null) {
                cVar.f2501f = keyStatusType.value;
            } else {
                throw null;
            }
        }

        public int c() {
            int i2 = super.d;
            if (i2 != -1) {
                return i2;
            }
            int i3 = 0;
            if (!this.f2500e.isEmpty()) {
                i3 = 0 + CodedOutputStream.b(1, this.f2500e);
            }
            int i4 = this.f2501f;
            if (i4 != KeyStatusType.UNKNOWN_STATUS.value) {
                i3 += CodedOutputStream.c(2, i4);
            }
            int i5 = this.g;
            if (i5 != 0) {
                i3 += CodedOutputStream.d(3, i5);
            }
            int i6 = this.h;
            if (i6 != OutputPrefixType.UNKNOWN_PREFIX.value) {
                i3 += CodedOutputStream.c(4, i6);
            }
            super.d = i3;
            return i3;
        }

        public static final class a extends GeneratedMessageLite.b<e2.c, e2.c.a> implements e2.d {
            public a() {
                super(c.f2498i);
            }

            public /* synthetic */ a(a aVar) {
                super(c.f2498i);
            }
        }

        public static /* synthetic */ void a(c cVar, OutputPrefixType outputPrefixType) {
            if (cVar == null) {
                throw null;
            } else if (outputPrefixType != null) {
                cVar.h = outputPrefixType.value;
            } else {
                throw null;
            }
        }

        public static /* synthetic */ void a(c cVar, String str) {
            if (str != null) {
                cVar.f2500e = str;
                return;
            }
            throw null;
        }

        public void a(CodedOutputStream codedOutputStream) {
            if (!this.f2500e.isEmpty()) {
                codedOutputStream.a(1, this.f2500e);
            }
            int i2 = this.f2501f;
            if (i2 != KeyStatusType.UNKNOWN_STATUS.value) {
                codedOutputStream.a(2, i2);
            }
            int i3 = this.g;
            if (i3 != 0) {
                codedOutputStream.b(3, i3);
            }
            int i4 = this.h;
            if (i4 != OutputPrefixType.UNKNOWN_PREFIX.value) {
                codedOutputStream.a(4, i4);
            }
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        public final Object a(GeneratedMessageLite.j jVar, Object obj, Object obj2) {
            boolean z = false;
            switch (jVar.ordinal()) {
                case 0:
                    return f2498i;
                case 1:
                    GeneratedMessageLite.k kVar = (GeneratedMessageLite.k) obj;
                    c cVar = (c) obj2;
                    this.f2500e = kVar.a(!this.f2500e.isEmpty(), this.f2500e, !cVar.f2500e.isEmpty(), cVar.f2500e);
                    this.f2501f = kVar.a(this.f2501f != 0, this.f2501f, cVar.f2501f != 0, cVar.f2501f);
                    this.g = kVar.a(this.g != 0, this.g, cVar.g != 0, cVar.g);
                    boolean z2 = this.h != 0;
                    int i2 = this.h;
                    if (cVar.h != 0) {
                        z = true;
                    }
                    this.h = kVar.a(z2, i2, z, cVar.h);
                    return this;
                case 2:
                    CodedInputStream codedInputStream = (CodedInputStream) obj;
                    ExtensionRegistryLite extensionRegistryLite = (ExtensionRegistryLite) obj2;
                    while (!z) {
                        try {
                            int g2 = codedInputStream.g();
                            if (g2 != 0) {
                                if (g2 == 10) {
                                    this.f2500e = codedInputStream.f();
                                } else if (g2 == 16) {
                                    this.f2501f = codedInputStream.d();
                                } else if (g2 == 24) {
                                    this.g = codedInputStream.d();
                                } else if (g2 == 32) {
                                    this.h = codedInputStream.d();
                                } else if (!codedInputStream.e(g2)) {
                                }
                            }
                            z = true;
                        } catch (InvalidProtocolBufferException e2) {
                            throw new RuntimeException(e2);
                        } catch (IOException e3) {
                            throw new RuntimeException(new InvalidProtocolBufferException(e3.getMessage()));
                        }
                    }
                    break;
                case 3:
                    return null;
                case 4:
                    return new c();
                case 5:
                    return new a(null);
                case 6:
                    break;
                case 7:
                    if (f2499j == null) {
                        synchronized (c.class) {
                            if (f2499j == null) {
                                f2499j = new GeneratedMessageLite.c(f2498i);
                            }
                        }
                    }
                    return f2499j;
                default:
                    throw new UnsupportedOperationException();
            }
            return f2498i;
        }
    }

    public interface d extends MessageLiteOrBuilder {
    }

    static {
        KeysetInfo keysetInfo = new KeysetInfo();
        h = keysetInfo;
        super.f();
    }

    public static /* synthetic */ void a(KeysetInfo keysetInfo, c cVar) {
        if (cVar != null) {
            Internal.c<e2.c> cVar2 = keysetInfo.g;
            if (!((AbstractProtobufList) cVar2).b) {
                keysetInfo.g = GeneratedMessageLite.a(cVar2);
            }
            keysetInfo.g.add(cVar);
            return;
        }
        throw null;
    }

    public int c() {
        int i2 = super.d;
        if (i2 != -1) {
            return i2;
        }
        int i3 = this.f2497f;
        int d2 = i3 != 0 ? CodedOutputStream.d(1, i3) + 0 : 0;
        for (int i4 = 0; i4 < this.g.size(); i4++) {
            d2 += CodedOutputStream.b(2, this.g.get(i4));
        }
        super.d = d2;
        return d2;
    }

    public static final class b extends GeneratedMessageLite.b<e2, e2.b> implements f2 {
        public b() {
            super(KeysetInfo.h);
        }

        public /* synthetic */ b(a aVar) {
            super(KeysetInfo.h);
        }
    }

    public void a(CodedOutputStream codedOutputStream) {
        int i2 = this.f2497f;
        if (i2 != 0) {
            codedOutputStream.b(1, i2);
        }
        for (int i3 = 0; i3 < this.g.size(); i3++) {
            codedOutputStream.a(2, this.g.get(i3));
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final Object a(GeneratedMessageLite.j jVar, Object obj, Object obj2) {
        boolean z = false;
        switch (jVar.ordinal()) {
            case 0:
                return h;
            case 1:
                GeneratedMessageLite.k kVar = (GeneratedMessageLite.k) obj;
                KeysetInfo keysetInfo = (KeysetInfo) obj2;
                boolean z2 = this.f2497f != 0;
                int i2 = this.f2497f;
                if (keysetInfo.f2497f != 0) {
                    z = true;
                }
                this.f2497f = kVar.a(z2, i2, z, keysetInfo.f2497f);
                this.g = kVar.a(this.g, keysetInfo.g);
                if (kVar == GeneratedMessageLite.i.a) {
                    this.f2496e |= keysetInfo.f2496e;
                }
                return this;
            case 2:
                CodedInputStream codedInputStream = (CodedInputStream) obj;
                ExtensionRegistryLite extensionRegistryLite = (ExtensionRegistryLite) obj2;
                while (!z) {
                    try {
                        int g2 = codedInputStream.g();
                        if (g2 != 0) {
                            if (g2 == 8) {
                                this.f2497f = codedInputStream.d();
                            } else if (g2 == 18) {
                                if (!((AbstractProtobufList) this.g).b) {
                                    this.g = GeneratedMessageLite.a(this.g);
                                }
                                this.g.add(codedInputStream.a(c.f2498i.i(), extensionRegistryLite));
                            } else if (!codedInputStream.e(g2)) {
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e2) {
                        throw new RuntimeException(e2);
                    } catch (IOException e3) {
                        throw new RuntimeException(new InvalidProtocolBufferException(e3.getMessage()));
                    }
                }
                break;
            case 3:
                ((AbstractProtobufList) this.g).b = false;
                return null;
            case 4:
                return new KeysetInfo();
            case 5:
                return new b(null);
            case 6:
                break;
            case 7:
                if (f2495i == null) {
                    synchronized (KeysetInfo.class) {
                        if (f2495i == null) {
                            f2495i = new GeneratedMessageLite.c(h);
                        }
                    }
                }
                return f2495i;
            default:
                throw new UnsupportedOperationException();
        }
        return h;
    }
}
