package j.c.b.a.z;

import j.c.e.Internal;
import j.c.e.l;

public enum EcdsaSignatureEncoding implements l.a {
    UNKNOWN_ENCODING(0),
    IEEE_P1363(1),
    DER(2),
    UNRECOGNIZED(-1);
    
    public static final int DER_VALUE = 2;
    public static final int IEEE_P1363_VALUE = 1;
    public static final int UNKNOWN_ENCODING_VALUE = 0;
    public static final Internal.b<v0> internalValueMap = new a();
    public final int value;

    public class a implements Internal.b<v0> {
    }

    /* access modifiers changed from: public */
    EcdsaSignatureEncoding(int i2) {
        this.value = i2;
    }

    public static EcdsaSignatureEncoding a(int i2) {
        if (i2 == 0) {
            return UNKNOWN_ENCODING;
        }
        if (i2 == 1) {
            return IEEE_P1363;
        }
        if (i2 != 2) {
            return null;
        }
        return DER;
    }
}
