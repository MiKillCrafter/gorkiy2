package j.c.b.a.w;

import com.google.protobuf.InvalidProtocolBufferException;
import j.c.a.a.c.n.c;
import j.c.b.a.HybridEncrypt;
import j.c.b.a.KeyManager;
import j.c.b.a.c0.EciesAeadHkdfHybridEncrypt;
import j.c.b.a.c0.Validators;
import j.c.b.a.f;
import j.c.b.a.z.EciesAeadHkdfParams;
import j.c.b.a.z.EciesAeadHkdfPublicKey;
import j.c.b.a.z.EciesHkdfKemParams;
import j.c.b.a.z.KeyData;
import j.c.e.ByteString;
import j.c.e.GeneratedMessageLite;
import j.c.e.MessageLite;
import java.security.GeneralSecurityException;

public class EciesAeadHkdfPublicKeyManager implements KeyManager<f> {
    public String a() {
        return "type.googleapis.com/google.crypto.tink.EciesAeadHkdfPublicKey";
    }

    public int b() {
        return 0;
    }

    public MessageLite b(MessageLite messageLite) {
        throw new GeneralSecurityException("Not implemented.");
    }

    /* JADX WARN: Type inference failed for: r3v3, types: [j.c.b.a.z.EciesAeadHkdfPublicKey, j.c.e.MessageLite] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T
     arg types: [j.c.b.a.z.EciesAeadHkdfPublicKey, j.c.e.ByteString]
     candidates:
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, byte[]):T
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite$j, java.lang.Object):java.lang.Object
      j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T */
    public Object c(ByteString byteString) {
        try {
            return a((MessageLite) ((EciesAeadHkdfPublicKey) GeneratedMessageLite.a((GeneratedMessageLite) EciesAeadHkdfPublicKey.f2456i, (j.c.e.f) byteString)));
        } catch (InvalidProtocolBufferException e2) {
            throw new GeneralSecurityException("expected serialized EciesAeadHkdfPublicKey proto", e2);
        }
    }

    public HybridEncrypt a(MessageLite messageLite) {
        if (messageLite instanceof EciesAeadHkdfPublicKey) {
            EciesAeadHkdfPublicKey eciesAeadHkdfPublicKey = (EciesAeadHkdfPublicKey) messageLite;
            Validators.a(eciesAeadHkdfPublicKey.f2458e, 0);
            c.a(eciesAeadHkdfPublicKey.g());
            EciesAeadHkdfParams g = eciesAeadHkdfPublicKey.g();
            EciesHkdfKemParams k2 = g.k();
            return new EciesAeadHkdfHybridEncrypt(c.a(c.a(k2.g()), eciesAeadHkdfPublicKey.g.d(), eciesAeadHkdfPublicKey.h.d()), k2.g.d(), c.b(k2.h()), c.a(g.h()), new RegistryEciesAeadHkdfDemHelper(g.g().g()));
        }
        throw new GeneralSecurityException("expected EciesAeadHkdfPublicKey proto");
    }

    public KeyData b(ByteString byteString) {
        throw new GeneralSecurityException("Not implemented.");
    }

    public MessageLite a(ByteString byteString) {
        throw new GeneralSecurityException("Not implemented.");
    }

    public boolean a(String str) {
        return "type.googleapis.com/google.crypto.tink.EciesAeadHkdfPublicKey".equals(str);
    }
}
