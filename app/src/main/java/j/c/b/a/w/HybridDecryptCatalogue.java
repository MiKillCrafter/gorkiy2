package j.c.b.a.w;

import j.c.b.a.Catalogue;
import j.c.b.a.KeyManager;
import j.c.b.a.e;
import java.security.GeneralSecurityException;

public class HybridDecryptCatalogue implements Catalogue<e> {
    public KeyManager<e> a(String str, String str2, int i2) {
        String lowerCase = str2.toLowerCase();
        char c = 65535;
        if (((lowerCase.hashCode() == 275448849 && lowerCase.equals("hybriddecrypt")) ? (char) 0 : 65535) == 0) {
            if (str.hashCode() == -80133005 && str.equals("type.googleapis.com/google.crypto.tink.EciesAeadHkdfPrivateKey")) {
                c = 0;
            }
            if (c == 0) {
                EciesAeadHkdfPrivateKeyManager eciesAeadHkdfPrivateKeyManager = new EciesAeadHkdfPrivateKeyManager();
                if (eciesAeadHkdfPrivateKeyManager.b() >= i2) {
                    return eciesAeadHkdfPrivateKeyManager;
                }
                throw new GeneralSecurityException(String.format("No key manager for key type '%s' with version at least %d.", str, Integer.valueOf(i2)));
            }
            throw new GeneralSecurityException(String.format("No support for primitive 'HybridEncrypt' with key type '%s'.", str));
        }
        throw new GeneralSecurityException(String.format("No support for primitive '%s'.", str2));
    }
}
