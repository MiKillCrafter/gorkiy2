package j.c.b.a.z;

import j.c.e.Internal;
import j.c.e.l;

public enum KeyStatusType implements l.a {
    UNKNOWN_STATUS(0),
    ENABLED(1),
    DISABLED(2),
    DESTROYED(3),
    UNRECOGNIZED(-1);
    
    public static final int DESTROYED_VALUE = 3;
    public static final int DISABLED_VALUE = 2;
    public static final int ENABLED_VALUE = 1;
    public static final int UNKNOWN_STATUS_VALUE = 0;
    public static final Internal.b<y1> internalValueMap = new a();
    public final int value;

    public class a implements Internal.b<y1> {
    }

    /* access modifiers changed from: public */
    KeyStatusType(int i2) {
        this.value = i2;
    }

    public static KeyStatusType a(int i2) {
        if (i2 == 0) {
            return UNKNOWN_STATUS;
        }
        if (i2 == 1) {
            return ENABLED;
        }
        if (i2 == 2) {
            return DISABLED;
        }
        if (i2 != 3) {
            return null;
        }
        return DESTROYED;
    }
}
