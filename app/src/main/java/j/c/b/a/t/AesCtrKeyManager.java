package j.c.b.a.t;

import com.google.protobuf.InvalidProtocolBufferException;
import j.c.b.a.KeyManager;
import j.c.b.a.c0.AesCtrJceCipher;
import j.c.b.a.c0.Random;
import j.c.b.a.c0.Validators;
import j.c.b.a.c0.d0;
import j.c.b.a.z.AesCtrKey;
import j.c.b.a.z.AesCtrKeyFormat;
import j.c.b.a.z.AesCtrParams;
import j.c.b.a.z.KeyData;
import j.c.e.ByteString;
import j.c.e.GeneratedMessageLite;
import j.c.e.MessageLite;
import j.c.e.f;
import java.security.GeneralSecurityException;

public class AesCtrKeyManager implements KeyManager<d0> {
    public String a() {
        return "type.googleapis.com/google.crypto.tink.AesCtrKey";
    }

    public int b() {
        return 0;
    }

    /* JADX WARN: Type inference failed for: r4v8, types: [j.c.e.MessageLite, j.c.e.GeneratedMessageLite] */
    public MessageLite b(MessageLite messageLite) {
        if (messageLite instanceof AesCtrKeyFormat) {
            AesCtrKeyFormat aesCtrKeyFormat = (AesCtrKeyFormat) messageLite;
            Validators.a(aesCtrKeyFormat.f2406f);
            int i2 = aesCtrKeyFormat.g().f2408e;
            if (i2 < 12 || i2 > 16) {
                throw new GeneralSecurityException("invalid IV size");
            }
            AesCtrKey.b bVar = (AesCtrKey.b) AesCtrKey.h.e();
            AesCtrParams g = aesCtrKeyFormat.g();
            bVar.m();
            AesCtrKey.a(bVar.c, g);
            ByteString a = ByteString.a(Random.a(aesCtrKeyFormat.f2406f));
            bVar.m();
            AesCtrKey.a(bVar.c, a);
            bVar.m();
            bVar.c.f2403e = 0;
            return bVar.k();
        }
        throw new GeneralSecurityException("expected AesCtrKeyFormat proto");
    }

    /* JADX WARN: Type inference failed for: r3v3, types: [j.c.b.a.z.AesCtrKey, j.c.e.MessageLite] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T
     arg types: [j.c.b.a.z.AesCtrKey, j.c.e.ByteString]
     candidates:
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, byte[]):T
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite$j, java.lang.Object):java.lang.Object
      j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T */
    public Object c(ByteString byteString) {
        try {
            return a((MessageLite) ((AesCtrKey) GeneratedMessageLite.a((GeneratedMessageLite) AesCtrKey.h, (f) byteString)));
        } catch (InvalidProtocolBufferException e2) {
            throw new GeneralSecurityException("expected serialized AesCtrKey proto", e2);
        }
    }

    public AesCtrJceCipher a(MessageLite messageLite) {
        if (messageLite instanceof AesCtrKey) {
            AesCtrKey aesCtrKey = (AesCtrKey) messageLite;
            Validators.a(aesCtrKey.f2403e, 0);
            Validators.a(aesCtrKey.g.size());
            int i2 = aesCtrKey.g().f2408e;
            if (i2 >= 12 && i2 <= 16) {
                return new AesCtrJceCipher(aesCtrKey.g.d(), aesCtrKey.g().f2408e);
            }
            throw new GeneralSecurityException("invalid IV size");
        }
        throw new GeneralSecurityException("expected AesCtrKey proto");
    }

    /* JADX WARN: Type inference failed for: r3v3, types: [j.c.e.MessageLite, j.c.b.a.z.AesCtrKeyFormat] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T
     arg types: [j.c.b.a.z.AesCtrKeyFormat, j.c.e.ByteString]
     candidates:
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, byte[]):T
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite$j, java.lang.Object):java.lang.Object
      j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T */
    public MessageLite a(ByteString byteString) {
        try {
            return b((MessageLite) ((AesCtrKeyFormat) GeneratedMessageLite.a((GeneratedMessageLite) AesCtrKeyFormat.g, (f) byteString)));
        } catch (InvalidProtocolBufferException e2) {
            throw new GeneralSecurityException("expected serialized AesCtrKeyFormat proto", e2);
        }
    }

    public boolean a(String str) {
        return str.equals("type.googleapis.com/google.crypto.tink.AesCtrKey");
    }

    public KeyData b(ByteString byteString) {
        KeyData.b g = KeyData.g();
        g.m();
        KeyData.a((KeyData) g.c, "type.googleapis.com/google.crypto.tink.AesCtrKey");
        ByteString a = ((AesCtrKey) a(byteString)).a();
        g.m();
        KeyData.a((KeyData) g.c, a);
        g.a(KeyData.c.SYMMETRIC);
        return (KeyData) g.k();
    }
}
