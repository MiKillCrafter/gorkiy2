package j.c.b.a.z;

import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.InvalidProtocolBufferException;
import j.c.b.a.z.EcdsaPublicKey;
import j.c.b.a.z.r0;
import j.c.e.ByteString;
import j.c.e.CodedInputStream;
import j.c.e.ExtensionRegistryLite;
import j.c.e.GeneratedMessageLite;
import j.c.e.MessageLite;
import j.c.e.Parser;
import java.io.IOException;

public final class EcdsaPrivateKey extends GeneratedMessageLite<r0, r0.b> implements s0 {
    public static final EcdsaPrivateKey h;

    /* renamed from: i  reason: collision with root package name */
    public static volatile Parser<r0> f2439i;

    /* renamed from: e  reason: collision with root package name */
    public int f2440e;

    /* renamed from: f  reason: collision with root package name */
    public EcdsaPublicKey f2441f;
    public ByteString g = ByteString.c;

    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        /* JADX WARNING: Can't wrap try/catch for region: R(20:0|(2:1|2)|3|5|6|7|8|9|11|12|13|14|15|(2:17|18)|19|21|22|23|24|26) */
        /* JADX WARNING: Can't wrap try/catch for region: R(22:0|1|2|3|5|6|7|8|9|11|12|13|14|15|17|18|19|21|22|23|24|26) */
        /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0025 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0039 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0017 */
        static {
            /*
                j.c.e.GeneratedMessageLite$j[] r0 = j.c.e.GeneratedMessageLite.j.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                j.c.b.a.z.EcdsaPrivateKey.a.a = r0
                r1 = 1
                r2 = 4
                j.c.e.GeneratedMessageLite$j r3 = j.c.e.GeneratedMessageLite.j.NEW_MUTABLE_INSTANCE     // Catch:{ NoSuchFieldError -> 0x000f }
                r0[r2] = r1     // Catch:{ NoSuchFieldError -> 0x000f }
            L_0x000f:
                r0 = 2
                int[] r3 = j.c.b.a.z.EcdsaPrivateKey.a.a     // Catch:{ NoSuchFieldError -> 0x0017 }
                j.c.e.GeneratedMessageLite$j r4 = j.c.e.GeneratedMessageLite.j.IS_INITIALIZED     // Catch:{ NoSuchFieldError -> 0x0017 }
                r4 = 0
                r3[r4] = r0     // Catch:{ NoSuchFieldError -> 0x0017 }
            L_0x0017:
                int[] r3 = j.c.b.a.z.EcdsaPrivateKey.a.a     // Catch:{ NoSuchFieldError -> 0x001e }
                j.c.e.GeneratedMessageLite$j r4 = j.c.e.GeneratedMessageLite.j.MAKE_IMMUTABLE     // Catch:{ NoSuchFieldError -> 0x001e }
                r4 = 3
                r3[r4] = r4     // Catch:{ NoSuchFieldError -> 0x001e }
            L_0x001e:
                r3 = 5
                int[] r4 = j.c.b.a.z.EcdsaPrivateKey.a.a     // Catch:{ NoSuchFieldError -> 0x0025 }
                j.c.e.GeneratedMessageLite$j r5 = j.c.e.GeneratedMessageLite.j.NEW_BUILDER     // Catch:{ NoSuchFieldError -> 0x0025 }
                r4[r3] = r2     // Catch:{ NoSuchFieldError -> 0x0025 }
            L_0x0025:
                int[] r2 = j.c.b.a.z.EcdsaPrivateKey.a.a     // Catch:{ NoSuchFieldError -> 0x002b }
                j.c.e.GeneratedMessageLite$j r4 = j.c.e.GeneratedMessageLite.j.VISIT     // Catch:{ NoSuchFieldError -> 0x002b }
                r2[r1] = r3     // Catch:{ NoSuchFieldError -> 0x002b }
            L_0x002b:
                r1 = 6
                int[] r2 = j.c.b.a.z.EcdsaPrivateKey.a.a     // Catch:{ NoSuchFieldError -> 0x0032 }
                j.c.e.GeneratedMessageLite$j r3 = j.c.e.GeneratedMessageLite.j.MERGE_FROM_STREAM     // Catch:{ NoSuchFieldError -> 0x0032 }
                r2[r0] = r1     // Catch:{ NoSuchFieldError -> 0x0032 }
            L_0x0032:
                r0 = 7
                int[] r2 = j.c.b.a.z.EcdsaPrivateKey.a.a     // Catch:{ NoSuchFieldError -> 0x0039 }
                j.c.e.GeneratedMessageLite$j r3 = j.c.e.GeneratedMessageLite.j.GET_DEFAULT_INSTANCE     // Catch:{ NoSuchFieldError -> 0x0039 }
                r2[r1] = r0     // Catch:{ NoSuchFieldError -> 0x0039 }
            L_0x0039:
                int[] r1 = j.c.b.a.z.EcdsaPrivateKey.a.a     // Catch:{ NoSuchFieldError -> 0x0041 }
                j.c.e.GeneratedMessageLite$j r2 = j.c.e.GeneratedMessageLite.j.GET_PARSER     // Catch:{ NoSuchFieldError -> 0x0041 }
                r2 = 8
                r1[r0] = r2     // Catch:{ NoSuchFieldError -> 0x0041 }
            L_0x0041:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: j.c.b.a.z.EcdsaPrivateKey.a.<clinit>():void");
        }
    }

    static {
        EcdsaPrivateKey ecdsaPrivateKey = new EcdsaPrivateKey();
        h = ecdsaPrivateKey;
        super.f();
    }

    public static /* synthetic */ void a(EcdsaPrivateKey ecdsaPrivateKey, EcdsaPublicKey ecdsaPublicKey) {
        if (ecdsaPublicKey != null) {
            ecdsaPrivateKey.f2441f = ecdsaPublicKey;
            return;
        }
        throw null;
    }

    /* JADX WARN: Type inference failed for: r2v1, types: [j.c.e.MessageLite, j.c.b.a.z.EcdsaPublicKey] */
    public int c() {
        int i2 = super.d;
        if (i2 != -1) {
            return i2;
        }
        int i3 = 0;
        int i4 = this.f2440e;
        if (i4 != 0) {
            i3 = 0 + CodedOutputStream.d(1, i4);
        }
        if (this.f2441f != null) {
            i3 += CodedOutputStream.b(2, (MessageLite) g());
        }
        if (!this.g.isEmpty()) {
            i3 += CodedOutputStream.b(3, this.g);
        }
        super.d = i3;
        return i3;
    }

    public EcdsaPublicKey g() {
        EcdsaPublicKey ecdsaPublicKey = this.f2441f;
        return ecdsaPublicKey == null ? EcdsaPublicKey.f2442i : ecdsaPublicKey;
    }

    public static final class b extends GeneratedMessageLite.b<r0, r0.b> implements s0 {
        public b() {
            super(EcdsaPrivateKey.h);
        }

        public /* synthetic */ b(a aVar) {
            super(EcdsaPrivateKey.h);
        }
    }

    public static /* synthetic */ void a(EcdsaPrivateKey ecdsaPrivateKey, ByteString byteString) {
        if (byteString != null) {
            ecdsaPrivateKey.g = byteString;
            return;
        }
        throw null;
    }

    /* JADX WARN: Type inference failed for: r1v1, types: [j.c.e.MessageLite, j.c.b.a.z.EcdsaPublicKey] */
    public void a(CodedOutputStream codedOutputStream) {
        int i2 = this.f2440e;
        if (i2 != 0) {
            codedOutputStream.b(1, i2);
        }
        if (this.f2441f != null) {
            codedOutputStream.a(2, (MessageLite) g());
        }
        if (!this.g.isEmpty()) {
            codedOutputStream.a(3, this.g);
        }
    }

    /* JADX WARN: Type inference failed for: r6v6, types: [j.c.e.MessageLite, j.c.b.a.z.EcdsaPublicKey] */
    /* JADX WARN: Type inference failed for: r2v2, types: [j.c.e.MessageLite, j.c.b.a.z.EcdsaPublicKey] */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final Object a(GeneratedMessageLite.j jVar, Object obj, Object obj2) {
        boolean z = false;
        switch (jVar.ordinal()) {
            case 0:
                return h;
            case 1:
                GeneratedMessageLite.k kVar = (GeneratedMessageLite.k) obj;
                EcdsaPrivateKey ecdsaPrivateKey = (EcdsaPrivateKey) obj2;
                this.f2440e = kVar.a(this.f2440e != 0, this.f2440e, ecdsaPrivateKey.f2440e != 0, ecdsaPrivateKey.f2440e);
                this.f2441f = (EcdsaPublicKey) kVar.a((MessageLite) this.f2441f, (MessageLite) ecdsaPrivateKey.f2441f);
                boolean z2 = this.g != ByteString.c;
                ByteString byteString = this.g;
                if (ecdsaPrivateKey.g != ByteString.c) {
                    z = true;
                }
                this.g = kVar.a(z2, byteString, z, ecdsaPrivateKey.g);
                return this;
            case 2:
                CodedInputStream codedInputStream = (CodedInputStream) obj;
                ExtensionRegistryLite extensionRegistryLite = (ExtensionRegistryLite) obj2;
                while (!z) {
                    try {
                        int g2 = codedInputStream.g();
                        if (g2 != 0) {
                            if (g2 == 8) {
                                this.f2440e = codedInputStream.d();
                            } else if (g2 == 18) {
                                EcdsaPublicKey.b bVar = this.f2441f != null ? (EcdsaPublicKey.b) this.f2441f.e() : null;
                                EcdsaPublicKey ecdsaPublicKey = (EcdsaPublicKey) codedInputStream.a(EcdsaPublicKey.f2442i.i(), extensionRegistryLite);
                                this.f2441f = ecdsaPublicKey;
                                if (bVar != null) {
                                    bVar.a(super);
                                    this.f2441f = (EcdsaPublicKey) bVar.l();
                                }
                            } else if (g2 == 26) {
                                this.g = codedInputStream.b();
                            } else if (!codedInputStream.e(g2)) {
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e2) {
                        throw new RuntimeException(e2);
                    } catch (IOException e3) {
                        throw new RuntimeException(new InvalidProtocolBufferException(e3.getMessage()));
                    }
                }
                break;
            case 3:
                return null;
            case 4:
                return new EcdsaPrivateKey();
            case 5:
                return new b(null);
            case 6:
                break;
            case 7:
                if (f2439i == null) {
                    synchronized (EcdsaPrivateKey.class) {
                        if (f2439i == null) {
                            f2439i = new GeneratedMessageLite.c(h);
                        }
                    }
                }
                return f2439i;
            default:
                throw new UnsupportedOperationException();
        }
        return h;
    }
}
