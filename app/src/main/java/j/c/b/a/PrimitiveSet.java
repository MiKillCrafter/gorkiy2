package j.c.b.a;

import j.c.b.a.z.p2;
import j.c.b.a.z.y1;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public final class PrimitiveSet<P> {
    public static final Charset c = Charset.forName("UTF-8");
    public ConcurrentMap<String, List<a<P>>> a = new ConcurrentHashMap();
    public a<P> b;

    public static final class a<P> {
        public final P a;
        public final byte[] b;

        public a(P p2, byte[] bArr, y1 y1Var, p2 p2Var) {
            this.a = p2;
            this.b = Arrays.copyOf(bArr, bArr.length);
        }

        public final byte[] a() {
            byte[] bArr = this.b;
            if (bArr == null) {
                return null;
            }
            return Arrays.copyOf(bArr, bArr.length);
        }
    }

    public List<a<P>> a(byte[] bArr) {
        List<a<P>> list = this.a.get(new String(bArr, c));
        return list != null ? list : Collections.emptyList();
    }
}
