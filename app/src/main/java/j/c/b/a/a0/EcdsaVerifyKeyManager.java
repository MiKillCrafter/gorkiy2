package j.c.b.a.a0;

import com.google.protobuf.InvalidProtocolBufferException;
import j.c.a.a.c.n.c;
import j.c.b.a.KeyManager;
import j.c.b.a.PublicKeyVerify;
import j.c.b.a.c0.EcdsaVerifyJce;
import j.c.b.a.c0.Validators;
import j.c.b.a.q;
import j.c.b.a.z.EcdsaPublicKey;
import j.c.b.a.z.KeyData;
import j.c.e.ByteString;
import j.c.e.GeneratedMessageLite;
import j.c.e.MessageLite;
import j.c.e.f;
import java.security.GeneralSecurityException;

public class EcdsaVerifyKeyManager implements KeyManager<q> {
    public String a() {
        return "type.googleapis.com/google.crypto.tink.EcdsaPublicKey";
    }

    public int b() {
        return 0;
    }

    public MessageLite b(MessageLite messageLite) {
        throw new GeneralSecurityException("Not implemented");
    }

    /* JADX WARN: Type inference failed for: r3v3, types: [j.c.e.MessageLite, j.c.b.a.z.EcdsaPublicKey] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T
     arg types: [j.c.b.a.z.EcdsaPublicKey, j.c.e.ByteString]
     candidates:
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, byte[]):T
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite$j, java.lang.Object):java.lang.Object
      j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T */
    public Object c(ByteString byteString) {
        try {
            return a((MessageLite) ((EcdsaPublicKey) GeneratedMessageLite.a((GeneratedMessageLite) EcdsaPublicKey.f2442i, (f) byteString)));
        } catch (InvalidProtocolBufferException e2) {
            throw new GeneralSecurityException("expected serialized EcdsaPublicKey proto", e2);
        }
    }

    public PublicKeyVerify a(MessageLite messageLite) {
        if (messageLite instanceof EcdsaPublicKey) {
            EcdsaPublicKey ecdsaPublicKey = (EcdsaPublicKey) messageLite;
            Validators.a(ecdsaPublicKey.f2444e, 0);
            c.a(ecdsaPublicKey.g());
            return new EcdsaVerifyJce(c.a(c.b(ecdsaPublicKey.g().g()), ecdsaPublicKey.g.d(), ecdsaPublicKey.h.d()), c.a(ecdsaPublicKey.g().k()), c.a(ecdsaPublicKey.g().h()));
        }
        throw new GeneralSecurityException("expected EcdsaPublicKey proto");
    }

    public KeyData b(ByteString byteString) {
        throw new GeneralSecurityException("Not implemented");
    }

    public MessageLite a(ByteString byteString) {
        throw new GeneralSecurityException("Not implemented");
    }

    public boolean a(String str) {
        return "type.googleapis.com/google.crypto.tink.EcdsaPublicKey".equals(str);
    }
}
