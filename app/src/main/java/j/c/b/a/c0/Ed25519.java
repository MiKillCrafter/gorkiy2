package j.c.b.a.c0;

import java.security.MessageDigest;
import java.util.Arrays;

public final class Ed25519 {
    public static final a a = new a(new long[]{1, 0, 0, 0, 0, 0, 0, 0, 0, 0}, new long[]{1, 0, 0, 0, 0, 0, 0, 0, 0, 0}, new long[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0});
    public static final b b = new b(new c(new long[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, new long[]{1, 0, 0, 0, 0, 0, 0, 0, 0, 0}, new long[]{1, 0, 0, 0, 0, 0, 0, 0, 0, 0}), new long[]{1, 0, 0, 0, 0, 0, 0, 0, 0, 0});

    public static class d {
        public final c a = new c();
        public final long[] b = new long[10];

        public static /* synthetic */ d a(d dVar, b bVar) {
            Field25519.a(dVar.a.a, bVar.a.a, bVar.b);
            long[] jArr = dVar.a.b;
            c cVar = bVar.a;
            Field25519.a(jArr, cVar.b, cVar.c);
            Field25519.a(dVar.a.c, bVar.a.c, bVar.b);
            long[] jArr2 = dVar.b;
            c cVar2 = bVar.a;
            Field25519.a(jArr2, cVar2.a, cVar2.b);
            return dVar;
        }
    }

    public static int a(int i2, int i3) {
        int i4 = (~(i2 ^ i3)) & 255;
        int i5 = i4 & (i4 << 4);
        int i6 = i5 & (i5 << 2);
        return ((i6 & (i6 << 1)) >> 7) & 1;
    }

    public static void a(b bVar, d dVar, a aVar) {
        long[] jArr = new long[10];
        long[] jArr2 = bVar.a.a;
        c cVar = dVar.a;
        Field25519.c(jArr2, cVar.b, cVar.a);
        long[] jArr3 = bVar.a.b;
        c cVar2 = dVar.a;
        Field25519.b(jArr3, cVar2.b, cVar2.a);
        long[] jArr4 = bVar.a.b;
        Field25519.a(jArr4, jArr4, aVar.b);
        c cVar3 = bVar.a;
        Field25519.a(cVar3.c, cVar3.a, aVar.a);
        Field25519.a(bVar.b, dVar.b, aVar.c);
        System.arraycopy(dVar.a.c, 0, bVar.a.a, 0, 10);
        long[] jArr5 = bVar.a.a;
        Field25519.c(jArr, jArr5, jArr5);
        c cVar4 = bVar.a;
        Field25519.b(cVar4.a, cVar4.c, cVar4.b);
        c cVar5 = bVar.a;
        long[] jArr6 = cVar5.b;
        Field25519.c(jArr6, cVar5.c, jArr6);
        Field25519.c(bVar.a.c, jArr, bVar.b);
        long[] jArr7 = bVar.b;
        Field25519.b(jArr7, jArr, jArr7);
    }

    public static byte[] b(byte[] bArr) {
        int i2;
        byte[] bArr2 = new byte[64];
        int i3 = 0;
        while (true) {
            if (i3 >= 32) {
                break;
            }
            int i4 = i3 * 2;
            bArr2[i4 + 0] = (byte) (((bArr[i3] & 255) >> 0) & 15);
            bArr2[i4 + 1] = (byte) (((bArr[i3] & 255) >> 4) & 15);
            i3++;
        }
        int i5 = 0;
        for (int i6 = 0; i6 < 63; i6++) {
            bArr2[i6] = (byte) (bArr2[i6] + i5);
            i5 = (bArr2[i6] + 8) >> 4;
            bArr2[i6] = (byte) (bArr2[i6] - (i5 << 4));
        }
        bArr2[63] = (byte) (bArr2[63] + i5);
        b bVar = new b(b);
        d dVar = new d();
        for (i2 = 1; i2 < 64; i2 += 2) {
            a aVar = new a(a);
            a(aVar, i2 / 2, bArr2[i2]);
            d.a(dVar, bVar);
            a(bVar, dVar, aVar);
        }
        c cVar = new c();
        c.a(cVar, bVar);
        a(bVar, cVar);
        c.a(cVar, bVar);
        a(bVar, cVar);
        c.a(cVar, bVar);
        a(bVar, cVar);
        c.a(cVar, bVar);
        a(bVar, cVar);
        for (int i7 = 0; i7 < 64; i7 += 2) {
            a aVar2 = new a(a);
            a(aVar2, i7 / 2, bArr2[i7]);
            d.a(dVar, bVar);
            a(bVar, dVar, aVar2);
        }
        long[] jArr = new long[10];
        long[] jArr2 = new long[10];
        long[] jArr3 = new long[10];
        Field25519.a(jArr, bVar.a.a, bVar.b);
        c cVar2 = bVar.a;
        Field25519.a(jArr2, cVar2.b, cVar2.c);
        Field25519.a(jArr3, bVar.a.c, bVar.b);
        long[] jArr4 = new long[10];
        Field25519.a(jArr4, jArr);
        long[] jArr5 = new long[10];
        Field25519.a(jArr5, jArr2);
        long[] jArr6 = new long[10];
        Field25519.a(jArr6, jArr3);
        long[] jArr7 = new long[10];
        Field25519.a(jArr7, jArr6);
        long[] jArr8 = new long[10];
        Field25519.b(jArr8, jArr5, jArr4);
        Field25519.a(jArr8, jArr8, jArr6);
        long[] jArr9 = new long[10];
        Field25519.a(jArr9, jArr4, jArr5);
        Field25519.a(jArr9, jArr9, Ed25519Constants.a);
        Field25519.c(jArr9, jArr9, jArr7);
        if (j.c.a.a.c.n.c.b(Field25519.a(jArr8), Field25519.a(jArr9))) {
            long[] jArr10 = new long[10];
            long[] jArr11 = new long[10];
            long[] jArr12 = new long[10];
            long[] jArr13 = new long[10];
            long[] jArr14 = new long[10];
            long[] jArr15 = new long[10];
            long[] jArr16 = new long[10];
            long[] jArr17 = new long[10];
            long[] jArr18 = new long[10];
            long[] jArr19 = new long[10];
            long[] jArr20 = new long[10];
            long[] jArr21 = new long[10];
            long[] jArr22 = jArr2;
            long[] jArr23 = new long[10];
            Field25519.a(jArr13, jArr3);
            Field25519.a(jArr23, jArr13);
            Field25519.a(jArr21, jArr23);
            Field25519.a(jArr14, jArr21, jArr3);
            Field25519.a(jArr15, jArr14, jArr13);
            Field25519.a(jArr21, jArr15);
            Field25519.a(jArr16, jArr21, jArr14);
            Field25519.a(jArr21, jArr16);
            Field25519.a(jArr23, jArr21);
            Field25519.a(jArr21, jArr23);
            Field25519.a(jArr23, jArr21);
            Field25519.a(jArr21, jArr23);
            Field25519.a(jArr17, jArr21, jArr16);
            Field25519.a(jArr21, jArr17);
            Field25519.a(jArr23, jArr21);
            for (int i8 = 2; i8 < 10; i8 += 2) {
                Field25519.a(jArr21, jArr23);
                Field25519.a(jArr23, jArr21);
            }
            Field25519.a(jArr18, jArr23, jArr17);
            Field25519.a(jArr21, jArr18);
            Field25519.a(jArr23, jArr21);
            for (int i9 = 2; i9 < 20; i9 += 2) {
                Field25519.a(jArr21, jArr23);
                Field25519.a(jArr23, jArr21);
            }
            Field25519.a(jArr21, jArr23, jArr18);
            Field25519.a(jArr23, jArr21);
            Field25519.a(jArr21, jArr23);
            for (int i10 = 2; i10 < 10; i10 += 2) {
                Field25519.a(jArr23, jArr21);
                Field25519.a(jArr21, jArr23);
            }
            Field25519.a(jArr19, jArr21, jArr17);
            Field25519.a(jArr21, jArr19);
            Field25519.a(jArr23, jArr21);
            for (int i11 = 2; i11 < 50; i11 += 2) {
                Field25519.a(jArr21, jArr23);
                Field25519.a(jArr23, jArr21);
            }
            Field25519.a(jArr20, jArr23, jArr19);
            Field25519.a(jArr23, jArr20);
            Field25519.a(jArr21, jArr23);
            for (int i12 = 2; i12 < 100; i12 += 2) {
                Field25519.a(jArr23, jArr21);
                Field25519.a(jArr21, jArr23);
            }
            Field25519.a(jArr23, jArr21, jArr20);
            Field25519.a(jArr21, jArr23);
            Field25519.a(jArr23, jArr21);
            for (int i13 = 2; i13 < 50; i13 += 2) {
                Field25519.a(jArr21, jArr23);
                Field25519.a(jArr23, jArr21);
            }
            Field25519.a(jArr21, jArr23, jArr19);
            Field25519.a(jArr23, jArr21);
            Field25519.a(jArr21, jArr23);
            Field25519.a(jArr23, jArr21);
            Field25519.a(jArr21, jArr23);
            Field25519.a(jArr23, jArr21);
            Field25519.a(jArr10, jArr23, jArr15);
            Field25519.a(jArr11, jArr, jArr10);
            Field25519.a(jArr12, jArr22, jArr10);
            byte[] a2 = Field25519.a(jArr12);
            a2[31] = (byte) (a2[31] ^ (a(jArr11) << 7));
            return a2;
        }
        throw new IllegalStateException("arithmetic error in scalar multiplication");
    }

    public static class b {
        public final c a;
        public final long[] b;

        public b(c cVar, long[] jArr) {
            this.a = cVar;
            this.b = jArr;
        }

        public b(b bVar) {
            this.a = new c(bVar.a);
            this.b = Arrays.copyOf(bVar.b, 10);
        }
    }

    public static class a {
        public final long[] a;
        public final long[] b;
        public final long[] c;

        public a() {
            this.a = new long[10];
            this.b = new long[10];
            this.c = new long[10];
        }

        public void a(a aVar, int i2) {
            Curve25519.a(this.a, aVar.a, i2);
            Curve25519.a(this.b, aVar.b, i2);
            Curve25519.a(this.c, aVar.c, i2);
        }

        public a(long[] jArr, long[] jArr2, long[] jArr3) {
            this.a = jArr;
            this.b = jArr2;
            this.c = jArr3;
        }

        public a(a aVar) {
            this.a = Arrays.copyOf(aVar.a, 10);
            this.b = Arrays.copyOf(aVar.b, 10);
            this.c = Arrays.copyOf(aVar.c, 10);
        }
    }

    public static class c {
        public final long[] a;
        public final long[] b;
        public final long[] c;

        public c() {
            this.a = new long[10];
            this.b = new long[10];
            this.c = new long[10];
        }

        public static c a(c cVar, b bVar) {
            Field25519.a(cVar.a, bVar.a.a, bVar.b);
            long[] jArr = cVar.b;
            c cVar2 = bVar.a;
            Field25519.a(jArr, cVar2.b, cVar2.c);
            Field25519.a(cVar.c, bVar.a.c, bVar.b);
            return cVar;
        }

        public c(long[] jArr, long[] jArr2, long[] jArr3) {
            this.a = jArr;
            this.b = jArr2;
            this.c = jArr3;
        }

        public c(c cVar) {
            this.a = Arrays.copyOf(cVar.a, 10);
            this.b = Arrays.copyOf(cVar.b, 10);
            this.c = Arrays.copyOf(cVar.c, 10);
        }
    }

    public static void a(b bVar, c cVar) {
        long[] jArr = new long[10];
        Field25519.a(bVar.a.a, cVar.a);
        Field25519.a(bVar.a.c, cVar.b);
        Field25519.a(bVar.b, cVar.c);
        long[] jArr2 = bVar.b;
        Field25519.c(jArr2, jArr2, jArr2);
        Field25519.c(bVar.a.b, cVar.a, cVar.b);
        Field25519.a(jArr, bVar.a.b);
        c cVar2 = bVar.a;
        Field25519.c(cVar2.b, cVar2.c, cVar2.a);
        c cVar3 = bVar.a;
        long[] jArr3 = cVar3.c;
        Field25519.b(jArr3, jArr3, cVar3.a);
        c cVar4 = bVar.a;
        Field25519.b(cVar4.a, jArr, cVar4.b);
        long[] jArr4 = bVar.b;
        Field25519.b(jArr4, jArr4, bVar.a.c);
    }

    public static void a(a aVar, int i2, byte b2) {
        int i3 = (b2 & 255) >> 7;
        int i4 = b2 - (((-i3) & b2) << 1);
        aVar.a(Ed25519Constants.b[i2][0], a(i4, 1));
        aVar.a(Ed25519Constants.b[i2][1], a(i4, 2));
        aVar.a(Ed25519Constants.b[i2][2], a(i4, 3));
        aVar.a(Ed25519Constants.b[i2][3], a(i4, 4));
        aVar.a(Ed25519Constants.b[i2][4], a(i4, 5));
        aVar.a(Ed25519Constants.b[i2][5], a(i4, 6));
        aVar.a(Ed25519Constants.b[i2][6], a(i4, 7));
        aVar.a(Ed25519Constants.b[i2][7], a(i4, 8));
        long[] copyOf = Arrays.copyOf(aVar.b, 10);
        long[] copyOf2 = Arrays.copyOf(aVar.a, 10);
        long[] copyOf3 = Arrays.copyOf(aVar.c, 10);
        for (int i5 = 0; i5 < copyOf3.length; i5++) {
            copyOf3[i5] = -copyOf3[i5];
        }
        aVar.a(new a(copyOf, copyOf2, copyOf3), i3);
    }

    public static /* synthetic */ int a(long[] jArr) {
        return Field25519.a(jArr)[0] & 1;
    }

    public static byte[] a(byte[] bArr) {
        MessageDigest a2 = EngineFactory.h.a("SHA-512");
        a2.update(bArr, 0, 32);
        byte[] digest = a2.digest();
        digest[0] = (byte) (digest[0] & 248);
        digest[31] = (byte) (digest[31] & Byte.MAX_VALUE);
        digest[31] = (byte) (digest[31] | 64);
        return digest;
    }
}
