package j.c.b.a.x.a;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import j.c.a.a.c.n.c;
import java.io.IOException;

public final class SharedPrefKeysetReader {
    public final SharedPreferences a;
    public final String b;

    public SharedPrefKeysetReader(Context context, String str, String str2) {
        if (str != null) {
            this.b = str;
            Context applicationContext = context.getApplicationContext();
            if (str2 == null) {
                this.a = PreferenceManager.getDefaultSharedPreferences(applicationContext);
            } else {
                this.a = applicationContext.getSharedPreferences(str2, 0);
            }
        } else {
            throw new IllegalArgumentException("keysetName cannot be null");
        }
    }

    public final byte[] a() {
        try {
            String string = this.a.getString(this.b, null);
            if (string != null) {
                return c.c(string);
            }
            throw new IOException(String.format("can't read keyset; the pref value %s does not exist", this.b));
        } catch (ClassCastException | IllegalArgumentException e2) {
            throw new IOException(String.format("can't read keyset; the pref value %s is not a valid hex string", this.b), e2);
        }
    }
}
