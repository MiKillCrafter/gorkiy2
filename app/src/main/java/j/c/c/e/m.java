package j.c.c.e;

/* compiled from: com.google.firebase:firebase-common@@17.1.0 */
public class m {
    public final Class<?> a;
    public final boolean b;

    public /* synthetic */ m(Class cls, boolean z, k kVar) {
        this.a = cls;
        this.b = z;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof m)) {
            return false;
        }
        m mVar = (m) obj;
        if (!mVar.a.equals(this.a) || mVar.b != this.b) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return ((this.a.hashCode() ^ 1000003) * 1000003) ^ Boolean.valueOf(this.b).hashCode();
    }
}
