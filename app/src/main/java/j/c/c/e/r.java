package j.c.c.e;

import j.c.c.f.c;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* compiled from: com.google.firebase:firebase-common@@17.1.0 */
public final class r extends a {
    public final Set<Class<?>> a;
    public final Set<Class<?>> b;
    public final Set<Class<?>> c;
    public final Set<Class<?>> d;

    /* renamed from: e  reason: collision with root package name */
    public final Set<Class<?>> f2520e;

    /* renamed from: f  reason: collision with root package name */
    public final a f2521f;

    /* compiled from: com.google.firebase:firebase-common@@17.1.0 */
    public static class a implements c {
        public a(Set<Class<?>> set, c cVar) {
        }
    }

    public r(d<?> dVar, a aVar) {
        HashSet hashSet = new HashSet();
        HashSet hashSet2 = new HashSet();
        HashSet hashSet3 = new HashSet();
        HashSet hashSet4 = new HashSet();
        for (n next : dVar.b) {
            if (next.c == 0) {
                if (next.a()) {
                    hashSet3.add(next.a);
                } else {
                    hashSet.add(next.a);
                }
            } else if (next.a()) {
                hashSet4.add(next.a);
            } else {
                hashSet2.add(next.a);
            }
        }
        if (!dVar.f2516f.isEmpty()) {
            hashSet.add(c.class);
        }
        this.a = Collections.unmodifiableSet(hashSet);
        this.b = Collections.unmodifiableSet(hashSet2);
        this.c = Collections.unmodifiableSet(hashSet3);
        this.d = Collections.unmodifiableSet(hashSet4);
        this.f2520e = dVar.f2516f;
        this.f2521f = super;
    }

    public <T> T a(Class<T> cls) {
        if (this.a.contains(cls)) {
            T a2 = this.f2521f.a(cls);
            if (!cls.equals(c.class)) {
                return a2;
            }
            return new a(this.f2520e, (c) a2);
        }
        throw new IllegalArgumentException(String.format("Attempting to request an undeclared dependency %s.", cls));
    }

    public <T> j.c.c.h.a<T> b(Class<T> cls) {
        if (this.b.contains(cls)) {
            return this.f2521f.b(cls);
        }
        throw new IllegalArgumentException(String.format("Attempting to request an undeclared dependency Provider<%s>.", cls));
    }

    public <T> Set<T> c(Class<T> cls) {
        if (this.c.contains(cls)) {
            return this.f2521f.c(cls);
        }
        throw new IllegalArgumentException(String.format("Attempting to request an undeclared dependency Set<%s>.", cls));
    }

    public <T> j.c.c.h.a<Set<T>> d(Class<T> cls) {
        if (this.d.contains(cls)) {
            return this.f2521f.d(cls);
        }
        throw new IllegalArgumentException(String.format("Attempting to request an undeclared dependency Provider<Set<%s>>.", cls));
    }
}
