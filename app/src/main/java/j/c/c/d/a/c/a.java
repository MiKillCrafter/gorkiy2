package j.c.c.d.a.c;

import android.content.Context;
import com.google.firebase.FirebaseApp;
import j.c.c.d.a.b;
import j.c.c.e.e;
import j.c.c.f.d;

/* compiled from: com.google.android.gms:play-services-measurement-api@@17.0.1 */
public final /* synthetic */ class a implements e {
    public static final e a = new a();

    public final Object a(j.c.c.e.a aVar) {
        return b.a((FirebaseApp) aVar.a(FirebaseApp.class), (Context) aVar.a(Context.class), (d) aVar.a(d.class));
    }
}
