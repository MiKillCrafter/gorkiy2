package j.c.c;

import android.content.Context;
import android.text.TextUtils;
import i.b.k.ResourcesFlusher;
import j.c.a.a.c.l.n;
import j.c.a.a.c.l.o;
import j.c.a.a.c.n.f;
import java.util.Arrays;

/* compiled from: com.google.firebase:firebase-common@@17.1.0 */
public final class c {
    public final String a;
    public final String b;
    public final String c;
    public final String d;

    /* renamed from: e  reason: collision with root package name */
    public final String f2513e;

    /* renamed from: f  reason: collision with root package name */
    public final String f2514f;
    public final String g;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
      i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
      i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
      i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
      i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
      i.b.k.ResourcesFlusher.a(int, int):boolean
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
      i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
      i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
      i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void */
    public c(String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        ResourcesFlusher.a(!f.a(str), (Object) "ApplicationId must be set.");
        this.b = str;
        this.a = str2;
        this.c = str3;
        this.d = str4;
        this.f2513e = str5;
        this.f2514f = str6;
        this.g = str7;
    }

    public static c a(Context context) {
        o oVar = new o(context);
        String a2 = oVar.a("google_app_id");
        if (TextUtils.isEmpty(a2)) {
            return null;
        }
        return new c(a2, oVar.a("google_api_key"), oVar.a("firebase_database_url"), oVar.a("ga_trackingId"), oVar.a("gcm_defaultSenderId"), oVar.a("google_storage_bucket"), oVar.a("project_id"));
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof c)) {
            return false;
        }
        c cVar = (c) obj;
        if (!ResourcesFlusher.c(this.b, cVar.b) || !ResourcesFlusher.c(this.a, cVar.a) || !ResourcesFlusher.c(this.c, cVar.c) || !ResourcesFlusher.c(this.d, cVar.d) || !ResourcesFlusher.c(this.f2513e, cVar.f2513e) || !ResourcesFlusher.c(this.f2514f, cVar.f2514f) || !ResourcesFlusher.c(this.g, cVar.g)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.b, this.a, this.c, this.d, this.f2513e, this.f2514f, this.g});
    }

    public String toString() {
        n d2 = ResourcesFlusher.d(this);
        d2.a("applicationId", this.b);
        d2.a("apiKey", this.a);
        d2.a("databaseUrl", this.c);
        d2.a("gcmSenderId", this.f2513e);
        d2.a("storageBucket", this.f2514f);
        d2.a("projectId", this.g);
        return d2.toString();
    }
}
