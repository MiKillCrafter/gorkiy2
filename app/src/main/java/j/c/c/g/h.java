package j.c.c.g;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Looper;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.util.SparseArray;
import com.google.firebase.iid.zzam;
import j.c.a.a.c.m.a;
import j.c.a.a.f.d.d;
import java.util.ArrayDeque;
import java.util.Queue;
import javax.annotation.concurrent.GuardedBy;

public final class h implements ServiceConnection {
    @GuardedBy("this")
    public int a = 0;
    public final Messenger b = new Messenger(new d(Looper.getMainLooper(), new g(this)));
    public k c;
    @GuardedBy("this")
    public final Queue<m<?>> d = new ArrayDeque();
    @GuardedBy("this")

    /* renamed from: e  reason: collision with root package name */
    public final SparseArray<m<?>> f2525e = new SparseArray<>();

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ f f2526f;

    public /* synthetic */ h(f fVar, e eVar) {
        this.f2526f = fVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002f, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00a4, code lost:
        return true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized boolean a(j.c.c.g.m r6) {
        /*
            r5 = this;
            monitor-enter(r5)
            int r0 = r5.a     // Catch:{ all -> 0x00a7 }
            r1 = 2
            r2 = 1
            r3 = 0
            if (r0 == 0) goto L_0x004a
            if (r0 == r2) goto L_0x0043
            if (r0 == r1) goto L_0x0030
            r6 = 3
            if (r0 == r6) goto L_0x002e
            r6 = 4
            if (r0 != r6) goto L_0x0013
            goto L_0x002e
        L_0x0013:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException     // Catch:{ all -> 0x00a7 }
            int r0 = r5.a     // Catch:{ all -> 0x00a7 }
            r1 = 26
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00a7 }
            r2.<init>(r1)     // Catch:{ all -> 0x00a7 }
            java.lang.String r1 = "Unknown state: "
            r2.append(r1)     // Catch:{ all -> 0x00a7 }
            r2.append(r0)     // Catch:{ all -> 0x00a7 }
            java.lang.String r0 = r2.toString()     // Catch:{ all -> 0x00a7 }
            r6.<init>(r0)     // Catch:{ all -> 0x00a7 }
            throw r6     // Catch:{ all -> 0x00a7 }
        L_0x002e:
            monitor-exit(r5)
            return r3
        L_0x0030:
            java.util.Queue<j.c.c.g.m<?>> r0 = r5.d     // Catch:{ all -> 0x00a7 }
            r0.add(r6)     // Catch:{ all -> 0x00a7 }
            j.c.c.g.f r6 = r5.f2526f     // Catch:{ all -> 0x00a7 }
            java.util.concurrent.ScheduledExecutorService r6 = r6.b     // Catch:{ all -> 0x00a7 }
            j.c.c.g.i r0 = new j.c.c.g.i     // Catch:{ all -> 0x00a7 }
            r0.<init>(r5)     // Catch:{ all -> 0x00a7 }
            r6.execute(r0)     // Catch:{ all -> 0x00a7 }
            monitor-exit(r5)
            return r2
        L_0x0043:
            java.util.Queue<j.c.c.g.m<?>> r0 = r5.d     // Catch:{ all -> 0x00a7 }
            r0.add(r6)     // Catch:{ all -> 0x00a7 }
            monitor-exit(r5)
            return r2
        L_0x004a:
            java.util.Queue<j.c.c.g.m<?>> r0 = r5.d     // Catch:{ all -> 0x00a7 }
            r0.add(r6)     // Catch:{ all -> 0x00a7 }
            int r6 = r5.a     // Catch:{ all -> 0x00a7 }
            if (r6 != 0) goto L_0x0055
            r6 = 1
            goto L_0x0056
        L_0x0055:
            r6 = 0
        L_0x0056:
            i.b.k.ResourcesFlusher.b(r6)     // Catch:{ all -> 0x00a7 }
            java.lang.String r6 = "MessengerIpcClient"
            boolean r6 = android.util.Log.isLoggable(r6, r1)     // Catch:{ all -> 0x00a7 }
            if (r6 == 0) goto L_0x0068
            java.lang.String r6 = "MessengerIpcClient"
            java.lang.String r0 = "Starting bind to GmsCore"
            android.util.Log.v(r6, r0)     // Catch:{ all -> 0x00a7 }
        L_0x0068:
            r5.a = r2     // Catch:{ all -> 0x00a7 }
            android.content.Intent r6 = new android.content.Intent     // Catch:{ all -> 0x00a7 }
            java.lang.String r0 = "com.google.android.c2dm.intent.REGISTER"
            r6.<init>(r0)     // Catch:{ all -> 0x00a7 }
            java.lang.String r0 = "com.google.android.gms"
            r6.setPackage(r0)     // Catch:{ all -> 0x00a7 }
            j.c.a.a.c.m.a r0 = j.c.a.a.c.m.a.a()     // Catch:{ all -> 0x00a7 }
            j.c.c.g.f r1 = r5.f2526f     // Catch:{ all -> 0x00a7 }
            android.content.Context r1 = r1.a     // Catch:{ all -> 0x00a7 }
            if (r0 == 0) goto L_0x00a5
            java.lang.Class r4 = r1.getClass()     // Catch:{ all -> 0x00a7 }
            r4.getName()     // Catch:{ all -> 0x00a7 }
            boolean r6 = r0.b(r1, r6, r5, r2)     // Catch:{ all -> 0x00a7 }
            if (r6 != 0) goto L_0x0093
            java.lang.String r6 = "Unable to bind to service"
            r5.a(r3, r6)     // Catch:{ all -> 0x00a7 }
            goto L_0x00a3
        L_0x0093:
            j.c.c.g.f r6 = r5.f2526f     // Catch:{ all -> 0x00a7 }
            java.util.concurrent.ScheduledExecutorService r6 = r6.b     // Catch:{ all -> 0x00a7 }
            j.c.c.g.j r0 = new j.c.c.g.j     // Catch:{ all -> 0x00a7 }
            r0.<init>(r5)     // Catch:{ all -> 0x00a7 }
            r3 = 30
            java.util.concurrent.TimeUnit r1 = java.util.concurrent.TimeUnit.SECONDS     // Catch:{ all -> 0x00a7 }
            r6.schedule(r0, r3, r1)     // Catch:{ all -> 0x00a7 }
        L_0x00a3:
            monitor-exit(r5)
            return r2
        L_0x00a5:
            r6 = 0
            throw r6     // Catch:{ all -> 0x00a7 }
        L_0x00a7:
            r6 = move-exception
            monitor-exit(r5)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.c.g.h.a(j.c.c.g.m):boolean");
    }

    public final synchronized void b() {
        if (this.a == 2 && this.d.isEmpty() && this.f2525e.size() == 0) {
            if (Log.isLoggable("MessengerIpcClient", 2)) {
                Log.v("MessengerIpcClient", "Finished handling requests, unbinding");
            }
            this.a = 3;
            a a2 = a.a();
            Context context = this.f2526f.a;
            if (a2 != null) {
                context.unbindService(this);
            } else {
                throw null;
            }
        }
    }

    public final synchronized void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (Log.isLoggable("MessengerIpcClient", 2)) {
            Log.v("MessengerIpcClient", "Service connected");
        }
        if (iBinder == null) {
            a(0, "Null service connection");
            return;
        }
        try {
            this.c = new k(iBinder);
            this.a = 2;
            this.f2526f.b.execute(new i(this));
        } catch (RemoteException e2) {
            a(0, e2.getMessage());
        }
    }

    public final synchronized void onServiceDisconnected(ComponentName componentName) {
        if (Log.isLoggable("MessengerIpcClient", 2)) {
            Log.v("MessengerIpcClient", "Service disconnected");
        }
        a(2, "Service disconnected");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0052, code lost:
        r8 = r8.getData();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x005d, code lost:
        if (r8.getBoolean("unsupported", false) == false) goto L_0x006b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x005f, code lost:
        r2.a(new com.google.firebase.iid.zzam(4, "Not supported by GmsCore"));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x006b, code lost:
        r2 = (j.c.c.g.n) r2;
        r8 = r8.getBundle("data");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0073, code lost:
        if (r8 != null) goto L_0x0077;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0075, code lost:
        r8 = android.os.Bundle.EMPTY;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x007d, code lost:
        if (android.util.Log.isLoggable("MessengerIpcClient", 3) == false) goto L_0x00ae;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x007f, code lost:
        r1 = java.lang.String.valueOf(r2);
        r4 = java.lang.String.valueOf(r8);
        r5 = new java.lang.StringBuilder(r4.length() + (r1.length() + 16));
        r5.append("Finishing ");
        r5.append(r1);
        r5.append(" with ");
        r5.append(r4);
        android.util.Log.d("MessengerIpcClient", r5.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00ae, code lost:
        r2.b.a.a(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x00b5, code lost:
        return true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(android.os.Message r8) {
        /*
            r7 = this;
            int r0 = r8.arg1
            r1 = 3
            java.lang.String r2 = "MessengerIpcClient"
            boolean r2 = android.util.Log.isLoggable(r2, r1)
            if (r2 == 0) goto L_0x0023
            r2 = 41
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>(r2)
            java.lang.String r2 = "Received response to request: "
            r3.append(r2)
            r3.append(r0)
            java.lang.String r2 = r3.toString()
            java.lang.String r3 = "MessengerIpcClient"
            android.util.Log.d(r3, r2)
        L_0x0023:
            monitor-enter(r7)
            android.util.SparseArray<j.c.c.g.m<?>> r2 = r7.f2525e     // Catch:{ all -> 0x00b6 }
            java.lang.Object r2 = r2.get(r0)     // Catch:{ all -> 0x00b6 }
            j.c.c.g.m r2 = (j.c.c.g.m) r2     // Catch:{ all -> 0x00b6 }
            r3 = 1
            if (r2 != 0) goto L_0x0049
            java.lang.String r8 = "MessengerIpcClient"
            r1 = 50
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00b6 }
            r2.<init>(r1)     // Catch:{ all -> 0x00b6 }
            java.lang.String r1 = "Received response for unknown request: "
            r2.append(r1)     // Catch:{ all -> 0x00b6 }
            r2.append(r0)     // Catch:{ all -> 0x00b6 }
            java.lang.String r0 = r2.toString()     // Catch:{ all -> 0x00b6 }
            android.util.Log.w(r8, r0)     // Catch:{ all -> 0x00b6 }
            monitor-exit(r7)     // Catch:{ all -> 0x00b6 }
            return r3
        L_0x0049:
            android.util.SparseArray<j.c.c.g.m<?>> r4 = r7.f2525e     // Catch:{ all -> 0x00b6 }
            r4.remove(r0)     // Catch:{ all -> 0x00b6 }
            r7.b()     // Catch:{ all -> 0x00b6 }
            monitor-exit(r7)     // Catch:{ all -> 0x00b6 }
            android.os.Bundle r8 = r8.getData()
            r0 = 0
            java.lang.String r4 = "unsupported"
            boolean r0 = r8.getBoolean(r4, r0)
            if (r0 == 0) goto L_0x006b
            com.google.firebase.iid.zzam r8 = new com.google.firebase.iid.zzam
            r0 = 4
            java.lang.String r1 = "Not supported by GmsCore"
            r8.<init>(r0, r1)
            r2.a(r8)
            goto L_0x00b5
        L_0x006b:
            j.c.c.g.n r2 = (j.c.c.g.n) r2
            java.lang.String r0 = "data"
            android.os.Bundle r8 = r8.getBundle(r0)
            if (r8 != 0) goto L_0x0077
            android.os.Bundle r8 = android.os.Bundle.EMPTY
        L_0x0077:
            java.lang.String r0 = "MessengerIpcClient"
            boolean r1 = android.util.Log.isLoggable(r0, r1)
            if (r1 == 0) goto L_0x00ae
            java.lang.String r1 = java.lang.String.valueOf(r2)
            java.lang.String r4 = java.lang.String.valueOf(r8)
            int r5 = r1.length()
            int r5 = r5 + 16
            int r6 = r4.length()
            int r6 = r6 + r5
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>(r6)
            java.lang.String r6 = "Finishing "
            r5.append(r6)
            r5.append(r1)
            java.lang.String r1 = " with "
            r5.append(r1)
            r5.append(r4)
            java.lang.String r1 = r5.toString()
            android.util.Log.d(r0, r1)
        L_0x00ae:
            j.c.a.a.i.f<T> r0 = r2.b
            j.c.a.a.i.y<TResult> r0 = r0.a
            r0.a(r8)
        L_0x00b5:
            return r3
        L_0x00b6:
            r8 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x00b6 }
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.c.g.h.a(android.os.Message):boolean");
    }

    public final synchronized void a(int i2, String str) {
        if (Log.isLoggable("MessengerIpcClient", 3)) {
            String valueOf = String.valueOf(str);
            Log.d("MessengerIpcClient", valueOf.length() != 0 ? "Disconnected: ".concat(valueOf) : new String("Disconnected: "));
        }
        int i3 = this.a;
        if (i3 == 0) {
            throw new IllegalStateException();
        } else if (i3 == 1 || i3 == 2) {
            if (Log.isLoggable("MessengerIpcClient", 2)) {
                Log.v("MessengerIpcClient", "Unbinding service");
            }
            this.a = 4;
            a a2 = a.a();
            Context context = this.f2526f.a;
            if (a2 != null) {
                context.unbindService(this);
                zzam zzam = new zzam(i2, str);
                for (m<?> a3 : this.d) {
                    a3.a(zzam);
                }
                this.d.clear();
                for (int i4 = 0; i4 < this.f2525e.size(); i4++) {
                    this.f2525e.valueAt(i4).a(zzam);
                }
                this.f2525e.clear();
                return;
            }
            throw null;
        } else if (i3 == 3) {
            this.a = 4;
        } else if (i3 != 4) {
            int i5 = this.a;
            StringBuilder sb = new StringBuilder(26);
            sb.append("Unknown state: ");
            sb.append(i5);
            throw new IllegalStateException(sb.toString());
        }
    }

    public final synchronized void a() {
        if (this.a == 1) {
            a(1, "Timed out while binding");
        }
    }

    public final synchronized void a(int i2) {
        m mVar = this.f2525e.get(i2);
        if (mVar != null) {
            StringBuilder sb = new StringBuilder(31);
            sb.append("Timing out request: ");
            sb.append(i2);
            Log.w("MessengerIpcClient", sb.toString());
            this.f2525e.remove(i2);
            mVar.a(new zzam(3, "Timed out waiting for response"));
            b();
        }
    }
}
