package j.c.c.g;

import java.security.KeyPair;
import java.util.Arrays;

public final class u0 {
    public final KeyPair a;
    public final long b;

    public u0(KeyPair keyPair, long j2) {
        this.a = keyPair;
        this.b = j2;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof u0)) {
            return false;
        }
        u0 u0Var = (u0) obj;
        if (this.b != u0Var.b || !this.a.getPublic().equals(u0Var.a.getPublic()) || !this.a.getPrivate().equals(u0Var.a.getPrivate())) {
            return false;
        }
        return true;
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{this.a.getPublic(), this.a.getPrivate(), Long.valueOf(this.b)});
    }
}
