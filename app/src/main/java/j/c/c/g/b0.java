package j.c.c.g;

import android.content.Intent;
import android.util.Log;
import j.a.a.a.outline;

public final /* synthetic */ class b0 implements Runnable {
    public final c0 b;
    public final Intent c;

    public b0(c0 c0Var, Intent intent) {
        this.b = c0Var;
        this.c = intent;
    }

    public final void run() {
        c0 c0Var = this.b;
        String action = this.c.getAction();
        StringBuilder sb = new StringBuilder(outline.a(action, 61));
        sb.append("Service took too long to process intent: ");
        sb.append(action);
        sb.append(" App may get closed.");
        Log.w("EnhancedIntentService", sb.toString());
        c0Var.a();
    }
}
