package j.c.c.g;

import android.util.Pair;
import i.e.ArrayMap;
import j.c.a.a.i.e;
import java.util.Map;
import java.util.concurrent.Executor;
import javax.annotation.concurrent.GuardedBy;

public final class r {
    public final Executor a;
    @GuardedBy("this")
    public final Map<Pair<String, String>, e<a>> b = new ArrayMap();

    public r(Executor executor) {
        this.a = executor;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x003a, code lost:
        return r5;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized j.c.a.a.i.e<j.c.c.g.a> a(java.lang.String r5, java.lang.String r6, j.c.c.g.k0 r7) {
        /*
            r4 = this;
            monitor-enter(r4)
            android.util.Pair r0 = new android.util.Pair     // Catch:{ all -> 0x0089 }
            r0.<init>(r5, r6)     // Catch:{ all -> 0x0089 }
            java.util.Map<android.util.Pair<java.lang.String, java.lang.String>, j.c.a.a.i.e<j.c.c.g.a>> r5 = r4.b     // Catch:{ all -> 0x0089 }
            java.lang.Object r5 = r5.get(r0)     // Catch:{ all -> 0x0089 }
            j.c.a.a.i.e r5 = (j.c.a.a.i.e) r5     // Catch:{ all -> 0x0089 }
            r6 = 3
            if (r5 == 0) goto L_0x003b
            java.lang.String r7 = "FirebaseInstanceId"
            boolean r6 = android.util.Log.isLoggable(r7, r6)     // Catch:{ all -> 0x0089 }
            if (r6 == 0) goto L_0x0039
            java.lang.String r6 = "FirebaseInstanceId"
            java.lang.String r7 = java.lang.String.valueOf(r0)     // Catch:{ all -> 0x0089 }
            int r0 = r7.length()     // Catch:{ all -> 0x0089 }
            int r0 = r0 + 29
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0089 }
            r1.<init>(r0)     // Catch:{ all -> 0x0089 }
            java.lang.String r0 = "Joining ongoing request for: "
            r1.append(r0)     // Catch:{ all -> 0x0089 }
            r1.append(r7)     // Catch:{ all -> 0x0089 }
            java.lang.String r7 = r1.toString()     // Catch:{ all -> 0x0089 }
            android.util.Log.d(r6, r7)     // Catch:{ all -> 0x0089 }
        L_0x0039:
            monitor-exit(r4)
            return r5
        L_0x003b:
            java.lang.String r5 = "FirebaseInstanceId"
            boolean r5 = android.util.Log.isLoggable(r5, r6)     // Catch:{ all -> 0x0089 }
            if (r5 == 0) goto L_0x0063
            java.lang.String r5 = "FirebaseInstanceId"
            java.lang.String r6 = java.lang.String.valueOf(r0)     // Catch:{ all -> 0x0089 }
            int r1 = r6.length()     // Catch:{ all -> 0x0089 }
            int r1 = r1 + 24
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0089 }
            r2.<init>(r1)     // Catch:{ all -> 0x0089 }
            java.lang.String r1 = "Making new request for: "
            r2.append(r1)     // Catch:{ all -> 0x0089 }
            r2.append(r6)     // Catch:{ all -> 0x0089 }
            java.lang.String r6 = r2.toString()     // Catch:{ all -> 0x0089 }
            android.util.Log.d(r5, r6)     // Catch:{ all -> 0x0089 }
        L_0x0063:
            j.c.a.a.i.e r5 = r7.a()     // Catch:{ all -> 0x0089 }
            java.util.concurrent.Executor r6 = r4.a     // Catch:{ all -> 0x0089 }
            j.c.c.g.s r7 = new j.c.c.g.s     // Catch:{ all -> 0x0089 }
            r7.<init>(r4, r0)     // Catch:{ all -> 0x0089 }
            j.c.a.a.i.y r5 = (j.c.a.a.i.y) r5     // Catch:{ all -> 0x0089 }
            j.c.a.a.i.y r1 = new j.c.a.a.i.y     // Catch:{ all -> 0x0089 }
            r1.<init>()     // Catch:{ all -> 0x0089 }
            j.c.a.a.i.w<TResult> r2 = r5.b     // Catch:{ all -> 0x0089 }
            j.c.a.a.i.l r3 = new j.c.a.a.i.l     // Catch:{ all -> 0x0089 }
            r3.<init>(r6, r7, r1)     // Catch:{ all -> 0x0089 }
            r2.a(r3)     // Catch:{ all -> 0x0089 }
            r5.f()     // Catch:{ all -> 0x0089 }
            java.util.Map<android.util.Pair<java.lang.String, java.lang.String>, j.c.a.a.i.e<j.c.c.g.a>> r5 = r4.b     // Catch:{ all -> 0x0089 }
            r5.put(r0, r1)     // Catch:{ all -> 0x0089 }
            monitor-exit(r4)
            return r1
        L_0x0089:
            r5 = move-exception
            monitor-exit(r4)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.c.g.r.a(java.lang.String, java.lang.String, j.c.c.g.k0):j.c.a.a.i.e");
    }

    public final /* synthetic */ e a(Pair pair, e eVar) {
        synchronized (this) {
            this.b.remove(pair);
        }
        return eVar;
    }
}
