package j.c.c.g;

import com.google.firebase.iid.FirebaseInstanceId;
import j.c.a.a.i.e;
import j.c.a.a.i.t;
import j.c.a.a.i.y;
import java.util.concurrent.Executor;

public final /* synthetic */ class k0 {
    public final FirebaseInstanceId a;
    public final String b;
    public final String c;
    public final String d;

    /* renamed from: e  reason: collision with root package name */
    public final String f2527e;

    public k0(FirebaseInstanceId firebaseInstanceId, String str, String str2, String str3, String str4) {
        this.a = firebaseInstanceId;
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.f2527e = str4;
    }

    public final e a() {
        FirebaseInstanceId firebaseInstanceId = this.a;
        String str = this.b;
        String str2 = this.c;
        String str3 = this.d;
        String str4 = this.f2527e;
        e<String> a2 = firebaseInstanceId.d.a(str, str2, str3, str4);
        Executor executor = firebaseInstanceId.a;
        m0 m0Var = new m0(firebaseInstanceId, str3, str4, str);
        y yVar = (y) a2;
        if (yVar != null) {
            y yVar2 = new y();
            yVar.b.a(new t(executor, m0Var, yVar2));
            yVar.f();
            return yVar2;
        }
        throw null;
    }
}
