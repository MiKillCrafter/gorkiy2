package j.c.c.i;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import i.h.e.ContextCompat;
import j.c.c.f.c;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: com.google.firebase:firebase-common@@17.1.0 */
public class a {
    public final Context a;
    public final SharedPreferences b;
    public final AtomicBoolean c;

    public a(Context context, String str, c cVar) {
        ApplicationInfo applicationInfo;
        this.a = (Build.VERSION.SDK_INT < 24 || ContextCompat.c(context)) ? context : Build.VERSION.SDK_INT >= 24 ? context.createDeviceProtectedStorageContext() : null;
        this.b = context.getSharedPreferences("com.google.firebase.common.prefs:" + str, 0);
        boolean z = true;
        if (this.b.contains("firebase_data_collection_default_enabled")) {
            z = this.b.getBoolean("firebase_data_collection_default_enabled", true);
        } else {
            try {
                PackageManager packageManager = this.a.getPackageManager();
                if (!(packageManager == null || (applicationInfo = packageManager.getApplicationInfo(this.a.getPackageName(), 128)) == null || applicationInfo.metaData == null || !applicationInfo.metaData.containsKey("firebase_data_collection_default_enabled"))) {
                    z = applicationInfo.metaData.getBoolean("firebase_data_collection_default_enabled");
                }
            } catch (PackageManager.NameNotFoundException unused) {
            }
        }
        this.c = new AtomicBoolean(z);
    }
}
