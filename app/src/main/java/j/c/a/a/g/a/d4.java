package j.c.a.a.g.a;

import android.content.SharedPreferences;
import i.b.k.ResourcesFlusher;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class d4 {
    public final String a;
    public boolean b;
    public String c;
    public final /* synthetic */ w3 d;

    public d4(w3 w3Var, String str) {
        this.d = w3Var;
        ResourcesFlusher.b(str);
        this.a = str;
    }

    public final String a() {
        if (!this.b) {
            this.b = true;
            this.c = this.d.v().getString(this.a, null);
        }
        return this.c;
    }

    public final void a(String str) {
        if (!y8.d(str, this.c)) {
            SharedPreferences.Editor edit = this.d.v().edit();
            edit.putString(this.a, str);
            edit.apply();
            this.c = str;
        }
    }
}
