package j.c.a.a.g.a;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import i.b.k.ResourcesFlusher;
import j.c.a.a.f.e.t3;
import j.c.a.a.f.e.y2;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class g4 implements ServiceConnection {
    public final String a;
    public final /* synthetic */ c4 b;

    public g4(c4 c4Var, String str) {
        this.b = c4Var;
        this.a = str;
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (iBinder == null) {
            this.b.a.a().f2047i.a("Install Referrer connection returned with null binder");
            return;
        }
        try {
            t3 a2 = y2.a(iBinder);
            if (a2 == null) {
                this.b.a.a().f2047i.a("Install Referrer Service implementation was not found");
                return;
            }
            this.b.a.a().f2050l.a("Install Referrer Service connected");
            l4 i2 = this.b.a.i();
            f4 f4Var = new f4(this, a2, this);
            i2.o();
            ResourcesFlusher.b(f4Var);
            i2.a((p4<?>) new p4(i2, f4Var, "Task exception on worker thread"));
        } catch (Exception e2) {
            this.b.a.a().f2047i.a("Exception occurred while calling Install Referrer API", e2);
        }
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        this.b.a.a().f2050l.a("Install Referrer Service disconnected");
    }
}
