package j.c.a.a.g.a;

import android.os.Bundle;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class y5 implements Runnable {
    public final /* synthetic */ String b;
    public final /* synthetic */ String c;
    public final /* synthetic */ long d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ Bundle f2132e;

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ boolean f2133f;
    public final /* synthetic */ boolean g;
    public final /* synthetic */ boolean h;

    /* renamed from: i  reason: collision with root package name */
    public final /* synthetic */ String f2134i;

    /* renamed from: j  reason: collision with root package name */
    public final /* synthetic */ x5 f2135j;

    public y5(x5 x5Var, String str, String str2, long j2, Bundle bundle, boolean z, boolean z2, boolean z3, String str3) {
        this.f2135j = x5Var;
        this.b = str;
        this.c = str2;
        this.d = j2;
        this.f2132e = bundle;
        this.f2133f = z;
        this.g = z2;
        this.h = z3;
        this.f2134i = str3;
    }

    public final void run() {
        this.f2135j.a(this.b, this.c, this.d, this.f2132e, this.f2133f, this.g, this.h, this.f2134i);
    }
}
