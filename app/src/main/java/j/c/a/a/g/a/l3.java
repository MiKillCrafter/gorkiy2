package j.c.a.a.g.a;

import android.os.Bundle;
import i.b.k.ResourcesFlusher;
import j.a.a.a.outline;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class l3 extends n5 {
    public static final AtomicReference<String[]> c = new AtomicReference<>();
    public static final AtomicReference<String[]> d = new AtomicReference<>();

    /* renamed from: e  reason: collision with root package name */
    public static final AtomicReference<String[]> f2026e = new AtomicReference<>();

    public l3(r4 r4Var) {
        super(r4Var);
    }

    public final String a(String str) {
        if (str == null) {
            return null;
        }
        if (!t()) {
            return str;
        }
        return a(str, r5.b, r5.a, c);
    }

    public final String b(String str) {
        if (str == null) {
            return null;
        }
        if (!t()) {
            return str;
        }
        return a(str, q5.b, q5.a, d);
    }

    public final String c(String str) {
        if (str == null) {
            return null;
        }
        if (!t()) {
            return str;
        }
        if (!str.startsWith("_exp_")) {
            return a(str, t5.b, t5.a, f2026e);
        }
        return "experiment_id" + "(" + str + ")";
    }

    public final boolean r() {
        return false;
    }

    public final boolean t() {
        r4 r4Var = this.a;
        h9 h9Var = r4Var.f2086f;
        return r4Var.p() && this.a.a().a(3);
    }

    public static String a(String str, String[] strArr, String[] strArr2, AtomicReference<String[]> atomicReference) {
        String str2;
        ResourcesFlusher.b(strArr);
        ResourcesFlusher.b(strArr2);
        ResourcesFlusher.b(atomicReference);
        ResourcesFlusher.a(strArr.length == strArr2.length);
        for (int i2 = 0; i2 < strArr.length; i2++) {
            if (y8.d(str, strArr[i2])) {
                synchronized (atomicReference) {
                    String[] strArr3 = atomicReference.get();
                    if (strArr3 == null) {
                        strArr3 = new String[strArr2.length];
                        atomicReference.set(strArr3);
                    }
                    if (strArr3[i2] == null) {
                        strArr3[i2] = strArr2[i2] + "(" + strArr[i2] + ")";
                    }
                    str2 = strArr3[i2];
                }
                return str2;
            }
        }
        return str;
    }

    public final String a(i iVar) {
        if (iVar == null) {
            return null;
        }
        if (!t()) {
            return iVar.toString();
        }
        StringBuilder a = outline.a("origin=");
        a.append(iVar.d);
        a.append(",name=");
        a.append(a(iVar.b));
        a.append(",params=");
        a.append(a(iVar.c));
        return a.toString();
    }

    public final String a(f fVar) {
        if (fVar == null) {
            return null;
        }
        if (!t()) {
            return fVar.toString();
        }
        StringBuilder a = outline.a("Event{appId='");
        a.append(fVar.a);
        a.append("', name='");
        a.append(a(fVar.b));
        a.append("', params=");
        a.append(a(fVar.f1986f));
        a.append("}");
        return a.toString();
    }

    public final String a(h hVar) {
        if (hVar == null) {
            return null;
        }
        if (!t()) {
            return hVar.toString();
        }
        return a(hVar.b());
    }

    public final String a(Bundle bundle) {
        if (bundle == null) {
            return null;
        }
        if (!t()) {
            return bundle.toString();
        }
        StringBuilder sb = new StringBuilder();
        for (String next : bundle.keySet()) {
            if (sb.length() != 0) {
                sb.append(", ");
            } else {
                sb.append("Bundle[{");
            }
            sb.append(b(next));
            sb.append("=");
            sb.append(bundle.get(next));
        }
        sb.append("}]");
        return sb.toString();
    }
}
