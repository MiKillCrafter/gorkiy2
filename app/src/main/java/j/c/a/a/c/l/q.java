package j.c.a.a.c.l;

import android.app.Activity;
import android.content.Intent;

public final class q extends e {
    public final /* synthetic */ Intent b;
    public final /* synthetic */ Activity c;
    public final /* synthetic */ int d;

    public q(Intent intent, Activity activity, int i2) {
        this.b = intent;
        this.c = activity;
        this.d = i2;
    }

    public final void a() {
        Intent intent = this.b;
        if (intent != null) {
            this.c.startActivityForResult(intent, this.d);
        }
    }
}
