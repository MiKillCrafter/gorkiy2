package j.c.a.a.g.a;

import java.util.concurrent.atomic.AtomicReference;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class h6 implements Runnable {
    public final /* synthetic */ AtomicReference b;
    public final /* synthetic */ String c;
    public final /* synthetic */ String d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ String f2004e;

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ boolean f2005f;
    public final /* synthetic */ x5 g;

    public h6(x5 x5Var, AtomicReference atomicReference, String str, String str2, String str3, boolean z) {
        this.g = x5Var;
        this.b = atomicReference;
        this.c = str;
        this.d = str2;
        this.f2004e = str3;
        this.f2005f = z;
    }

    public final void run() {
        z6 r2 = this.g.a.r();
        AtomicReference atomicReference = this.b;
        String str = this.c;
        String str2 = this.d;
        String str3 = this.f2004e;
        boolean z = this.f2005f;
        r2.d();
        r2.w();
        r2.a(new p7(r2, atomicReference, str, str2, str3, z, r2.a(false)));
    }
}
