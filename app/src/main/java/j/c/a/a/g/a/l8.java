package j.c.a.a.g.a;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class l8 implements Runnable {
    public final /* synthetic */ long b;
    public final /* synthetic */ f8 c;

    public l8(f8 f8Var, long j2) {
        this.c = f8Var;
        this.b = j2;
    }

    public final void run() {
        f8 f8Var = this.c;
        long j2 = this.b;
        f8Var.d();
        f8Var.A();
        i9 i9Var = f8Var.a.g;
        g3 q2 = f8Var.q();
        q2.w();
        if (i9Var.d(q2.c, k.c0)) {
            f8Var.l().y.a(true);
        }
        f8Var.f1990f.b();
        f8Var.g.b();
        f8Var.a().f2052n.a("Activity paused, time", Long.valueOf(j2));
        if (f8Var.d != 0) {
            f8Var.l().w.a((j2 - f8Var.d) + f8Var.l().w.a());
        }
        if (f8Var.a.g.a(k.I0)) {
            f8Var.c.postDelayed(f8Var.h, 2000);
        }
    }
}
