package j.c.a.a.f.e;

import android.os.Bundle;
import android.os.IInterface;
import j.c.a.a.d.a;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.0.1 */
public interface l8 extends IInterface {
    void beginAdUnitExposure(String str, long j2);

    void clearConditionalUserProperty(String str, String str2, Bundle bundle);

    void endAdUnitExposure(String str, long j2);

    void generateEventId(fb fbVar);

    void getAppInstanceId(fb fbVar);

    void getCachedAppInstanceId(fb fbVar);

    void getConditionalUserProperties(String str, String str2, fb fbVar);

    void getCurrentScreenClass(fb fbVar);

    void getCurrentScreenName(fb fbVar);

    void getGmpAppId(fb fbVar);

    void getMaxUserProperties(String str, fb fbVar);

    void getTestFlag(fb fbVar, int i2);

    void getUserProperties(String str, String str2, boolean z, fb fbVar);

    void initForTests(Map map);

    void initialize(a aVar, nb nbVar, long j2);

    void isDataCollectionEnabled(fb fbVar);

    void logEvent(String str, String str2, Bundle bundle, boolean z, boolean z2, long j2);

    void logEventAndBundle(String str, String str2, Bundle bundle, fb fbVar, long j2);

    void logHealthData(int i2, String str, a aVar, a aVar2, a aVar3);

    void onActivityCreated(a aVar, Bundle bundle, long j2);

    void onActivityDestroyed(a aVar, long j2);

    void onActivityPaused(a aVar, long j2);

    void onActivityResumed(a aVar, long j2);

    void onActivitySaveInstanceState(a aVar, fb fbVar, long j2);

    void onActivityStarted(a aVar, long j2);

    void onActivityStopped(a aVar, long j2);

    void performAction(Bundle bundle, fb fbVar, long j2);

    void registerOnMeasurementEventListener(gb gbVar);

    void resetAnalyticsData(long j2);

    void setConditionalUserProperty(Bundle bundle, long j2);

    void setCurrentScreen(a aVar, String str, String str2, long j2);

    void setDataCollectionEnabled(boolean z);

    void setEventInterceptor(gb gbVar);

    void setInstanceIdProvider(lb lbVar);

    void setMeasurementEnabled(boolean z, long j2);

    void setMinimumSessionDuration(long j2);

    void setSessionTimeoutDuration(long j2);

    void setUserId(String str, long j2);

    void setUserProperty(String str, String str2, a aVar, boolean z, long j2);

    void unregisterOnMeasurementEventListener(gb gbVar);
}
