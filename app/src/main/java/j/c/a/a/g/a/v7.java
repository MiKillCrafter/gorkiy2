package j.c.a.a.g.a;

import android.content.ComponentName;
import android.content.Context;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class v7 implements Runnable {
    public final /* synthetic */ r7 b;

    public v7(r7 r7Var) {
        this.b = r7Var;
    }

    public final void run() {
        z6 z6Var = this.b.c;
        r4 r4Var = this.b.c.a;
        Context context = r4Var.a;
        h9 h9Var = r4Var.f2086f;
        z6.a(z6Var, new ComponentName(context, "com.google.android.gms.measurement.AppMeasurementService"));
    }
}
