package j.c.a.a.f.e;

import j.c.a.a.f.e.x3;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class y0 extends x3<y0, a> implements g5 {
    public static final y0 zzj;
    public static volatile m5<y0> zzk;
    public int zzc;
    public long zzd;
    public String zze = "";
    public String zzf = "";
    public long zzg;
    public float zzh;
    public double zzi;

    static {
        y0 y0Var = new y0();
        zzj = y0Var;
        x3.zzd.put(y0.class, y0Var);
    }

    public static /* synthetic */ void b(y0 y0Var, String str) {
        if (str != null) {
            y0Var.zzc |= 4;
            y0Var.zzf = str;
            return;
        }
        throw null;
    }

    public static a m() {
        return (a) zzj.g();
    }

    public final boolean a() {
        return (this.zzc & 1) != 0;
    }

    public final long i() {
        return this.zzd;
    }

    public final String j() {
        return this.zze;
    }

    /* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
    public static final class a extends x3.a<y0, a> implements g5 {
        public a() {
            super(y0.zzj);
        }

        public final a a(long j2) {
            i();
            y0 y0Var = (y0) super.c;
            y0Var.zzc |= 1;
            y0Var.zzd = j2;
            return this;
        }

        public final a b(long j2) {
            i();
            y0 y0Var = (y0) super.c;
            y0Var.zzc |= 8;
            y0Var.zzg = j2;
            return this;
        }

        public /* synthetic */ a(z0 z0Var) {
            super(y0.zzj);
        }

        public final a a(String str) {
            i();
            y0.a((y0) super.c, str);
            return this;
        }
    }

    public static /* synthetic */ void a(y0 y0Var, String str) {
        if (str != null) {
            y0Var.zzc |= 2;
            y0Var.zze = str;
            return;
        }
        throw null;
    }

    public final Object a(int i2, Object obj, Object obj2) {
        switch (z0.a[i2 - 1]) {
            case 1:
                return new y0();
            case 2:
                return new a(null);
            case 3:
                return new r5(zzj, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0000\u0000\u0001\u0002\u0000\u0002\b\u0001\u0003\b\u0002\u0004\u0002\u0003\u0005\u0001\u0004\u0006\u0000\u0005", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg", "zzh", "zzi"});
            case 4:
                return zzj;
            case 5:
                m5<y0> m5Var = zzk;
                if (m5Var == null) {
                    synchronized (y0.class) {
                        m5Var = zzk;
                        if (m5Var == null) {
                            m5Var = new x3.c<>(zzj);
                            zzk = m5Var;
                        }
                    }
                }
                return m5Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
