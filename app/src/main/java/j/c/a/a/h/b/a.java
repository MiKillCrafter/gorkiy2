package j.c.a.a.h.b;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import j.c.a.a.c.k.d;
import j.c.a.a.c.l.b;
import j.c.a.a.c.l.c;
import j.c.a.a.c.l.g;
import j.c.a.a.h.f;

public class a extends g<e> implements f {
    public final Bundle A;
    public Integer B;
    public final boolean y;
    public final c z;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public a(Context context, Looper looper, c cVar, j.c.a.a.c.k.c cVar2, d dVar) {
        super(context, looper, 44, cVar, cVar2, dVar);
        c cVar3 = cVar;
        j.c.a.a.h.a aVar = cVar3.f1816e;
        Integer num = cVar3.f1817f;
        Bundle bundle = new Bundle();
        bundle.putParcelable("com.google.android.gms.signin.internal.clientRequestedAccount", cVar3.a);
        if (num != null) {
            bundle.putInt("com.google.android.gms.common.internal.ClientSettings.sessionId", num.intValue());
        }
        if (aVar != null) {
            bundle.putBoolean("com.google.android.gms.signin.internal.offlineAccessRequested", aVar.a);
            bundle.putBoolean("com.google.android.gms.signin.internal.idTokenRequested", aVar.b);
            bundle.putString("com.google.android.gms.signin.internal.serverClientId", aVar.c);
            bundle.putBoolean("com.google.android.gms.signin.internal.usePromptModeForAuthCode", true);
            bundle.putBoolean("com.google.android.gms.signin.internal.forceCodeForRefreshToken", aVar.d);
            bundle.putString("com.google.android.gms.signin.internal.hostedDomain", aVar.f2145e);
            bundle.putBoolean("com.google.android.gms.signin.internal.waitForAccessTokenRefresh", aVar.f2146f);
            Long l2 = aVar.g;
            if (l2 != null) {
                bundle.putLong("com.google.android.gms.signin.internal.authApiSignInModuleVersion", l2.longValue());
            }
            Long l3 = aVar.h;
            if (l3 != null) {
                bundle.putLong("com.google.android.gms.signin.internal.realClientLibraryVersion", l3.longValue());
            }
        }
        this.y = true;
        this.z = cVar3;
        this.A = bundle;
        this.B = cVar3.f1817f;
    }

    public /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.signin.internal.ISignInService");
        if (queryLocalInterface instanceof e) {
            return (e) queryLocalInterface;
        }
        return new f(iBinder);
    }

    public final void f() {
        a(new b.d());
    }

    public boolean g() {
        return this.y;
    }

    public int i() {
        return j.c.a.a.c.g.GOOGLE_PLAY_SERVICES_VERSION_CODE;
    }

    public Bundle l() {
        if (!this.b.getPackageName().equals(this.z.c)) {
            this.A.putString("com.google.android.gms.signin.internal.realClientPackageName", this.z.c);
        }
        return this.A;
    }

    public String o() {
        return "com.google.android.gms.signin.internal.ISignInService";
    }

    public String p() {
        return "com.google.android.gms.signin.service.START";
    }
}
