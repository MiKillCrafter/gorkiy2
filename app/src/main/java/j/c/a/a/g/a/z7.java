package j.c.a.a.g.a;

import android.app.job.JobParameters;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final /* synthetic */ class z7 implements Runnable {
    public final x7 b;
    public final n3 c;
    public final JobParameters d;

    public z7(x7 x7Var, n3 n3Var, JobParameters jobParameters) {
        this.b = x7Var;
        this.c = n3Var;
        this.d = jobParameters;
    }

    public final void run() {
        x7 x7Var = this.b;
        n3 n3Var = this.c;
        JobParameters jobParameters = this.d;
        if (x7Var != null) {
            n3Var.f2052n.a("AppMeasurementJobService processed last upload request.");
            ((c8) x7Var.a).a(jobParameters, false);
            return;
        }
        throw null;
    }
}
