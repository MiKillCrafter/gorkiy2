package j.c.a.a.f.e;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class i7 implements f7 {
    public static final p1<Boolean> a = p1.a(new v1(q1.a("com.google.android.gms.measurement")), "measurement.log_androidId_enabled", false);

    public final boolean a() {
        return a.b().booleanValue();
    }
}
