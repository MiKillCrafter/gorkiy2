package j.c.a.a.c.k.e;

import android.os.DeadObjectException;
import android.os.RemoteException;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;
import j.c.a.a.c.k.e.b;
import j.c.a.a.i.f;

public abstract class x<T> extends s {
    public final f<T> a;

    public x(int i2, f<T> fVar) {
        super(i2);
        this.a = fVar;
    }

    public void a(Status status) {
        f<T> fVar = this.a;
        fVar.a.b((Exception) new ApiException(status));
    }

    public abstract void b(b.a<?> aVar);

    public final void a(b.a<?> aVar) {
        try {
            b(aVar);
        } catch (DeadObjectException e2) {
            Status a2 = j.a(e2);
            f<T> fVar = this.a;
            fVar.a.b((Exception) new ApiException(a2));
            throw e2;
        } catch (RemoteException e3) {
            Status a3 = j.a(e3);
            f<T> fVar2 = this.a;
            fVar2.a.b((Exception) new ApiException(a3));
        } catch (RuntimeException e4) {
            this.a.a.b((Exception) e4);
        }
    }
}
