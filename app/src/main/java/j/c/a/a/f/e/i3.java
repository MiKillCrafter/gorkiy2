package j.c.a.a.f.e;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class i3 extends r2<Double> implements e4<Double>, p5, RandomAccess {
    public double[] c;
    public int d;

    static {
        new i3(new double[0], 0).b = false;
    }

    public i3() {
        this.c = new double[10];
        this.d = 0;
    }

    public final void a(double d2) {
        c();
        int i2 = this.d;
        double[] dArr = this.c;
        if (i2 == dArr.length) {
            double[] dArr2 = new double[(((i2 * 3) / 2) + 1)];
            System.arraycopy(dArr, 0, dArr2, 0, i2);
            this.c = dArr2;
        }
        double[] dArr3 = this.c;
        int i3 = this.d;
        this.d = i3 + 1;
        dArr3[i3] = d2;
    }

    public final /* synthetic */ void add(int i2, Object obj) {
        int i3;
        double doubleValue = ((Double) obj).doubleValue();
        c();
        if (i2 < 0 || i2 > (i3 = this.d)) {
            throw new IndexOutOfBoundsException(c(i2));
        }
        double[] dArr = this.c;
        if (i3 < dArr.length) {
            System.arraycopy(dArr, i2, dArr, i2 + 1, i3 - i2);
        } else {
            double[] dArr2 = new double[(((i3 * 3) / 2) + 1)];
            System.arraycopy(dArr, 0, dArr2, 0, i2);
            System.arraycopy(this.c, i2, dArr2, i2 + 1, this.d - i2);
            this.c = dArr2;
        }
        this.c[i2] = doubleValue;
        this.d++;
        this.modCount++;
    }

    public final boolean addAll(Collection<? extends Double> collection) {
        c();
        y3.a(collection);
        if (!(collection instanceof i3)) {
            return super.addAll(collection);
        }
        i3 i3Var = (i3) collection;
        int i2 = i3Var.d;
        if (i2 == 0) {
            return false;
        }
        int i3 = this.d;
        if (Integer.MAX_VALUE - i3 >= i2) {
            int i4 = i3 + i2;
            double[] dArr = this.c;
            if (i4 > dArr.length) {
                this.c = Arrays.copyOf(dArr, i4);
            }
            System.arraycopy(i3Var.c, 0, this.c, this.d, i3Var.d);
            this.d = i4;
            this.modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    public final void b(int i2) {
        if (i2 < 0 || i2 >= this.d) {
            throw new IndexOutOfBoundsException(c(i2));
        }
    }

    public final String c(int i2) {
        int i3 = this.d;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i2);
        sb.append(", Size:");
        sb.append(i3);
        return sb.toString();
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof i3)) {
            return super.equals(obj);
        }
        i3 i3Var = (i3) obj;
        if (this.d != i3Var.d) {
            return false;
        }
        double[] dArr = i3Var.c;
        for (int i2 = 0; i2 < this.d; i2++) {
            if (Double.doubleToLongBits(this.c[i2]) != Double.doubleToLongBits(dArr[i2])) {
                return false;
            }
        }
        return true;
    }

    public final /* synthetic */ Object get(int i2) {
        b(i2);
        return Double.valueOf(this.c[i2]);
    }

    public final int hashCode() {
        int i2 = 1;
        for (int i3 = 0; i3 < this.d; i3++) {
            i2 = (i2 * 31) + y3.a(Double.doubleToLongBits(this.c[i3]));
        }
        return i2;
    }

    public final boolean remove(Object obj) {
        c();
        for (int i2 = 0; i2 < this.d; i2++) {
            if (obj.equals(Double.valueOf(this.c[i2]))) {
                double[] dArr = this.c;
                System.arraycopy(dArr, i2 + 1, dArr, i2, (this.d - i2) - 1);
                this.d--;
                this.modCount++;
                return true;
            }
        }
        return false;
    }

    public final void removeRange(int i2, int i3) {
        c();
        if (i3 >= i2) {
            double[] dArr = this.c;
            System.arraycopy(dArr, i3, dArr, i2, this.d - i3);
            this.d -= i3 - i2;
            this.modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    public final /* synthetic */ Object set(int i2, Object obj) {
        double doubleValue = ((Double) obj).doubleValue();
        c();
        b(i2);
        double[] dArr = this.c;
        double d2 = dArr[i2];
        dArr[i2] = doubleValue;
        return Double.valueOf(d2);
    }

    public final int size() {
        return this.d;
    }

    public i3(double[] dArr, int i2) {
        this.c = dArr;
        this.d = i2;
    }

    public final /* synthetic */ Object remove(int i2) {
        c();
        b(i2);
        double[] dArr = this.c;
        double d2 = dArr[i2];
        int i3 = this.d;
        if (i2 < i3 - 1) {
            System.arraycopy(dArr, i2 + 1, dArr, i2, (i3 - i2) - 1);
        }
        this.d--;
        this.modCount++;
        return Double.valueOf(d2);
    }

    public final /* synthetic */ e4 a(int i2) {
        if (i2 >= this.d) {
            return new i3(Arrays.copyOf(this.c, i2), this.d);
        }
        throw new IllegalArgumentException();
    }

    public final /* synthetic */ boolean add(Object obj) {
        a(((Double) obj).doubleValue());
        return true;
    }
}
