package j.c.a.a.c.l;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import i.b.k.ResourcesFlusher;
import j.c.a.a.c.d;

public final class v implements Parcelable.Creator<u> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = ResourcesFlusher.b(parcel);
        Bundle bundle = null;
        d[] dVarArr = null;
        while (parcel.dataPosition() < b) {
            int readInt = parcel.readInt();
            int i2 = 65535 & readInt;
            if (i2 == 1) {
                bundle = ResourcesFlusher.a(parcel, readInt);
            } else if (i2 != 2) {
                ResourcesFlusher.i(parcel, readInt);
            } else {
                dVarArr = (d[]) ResourcesFlusher.b(parcel, readInt, d.CREATOR);
            }
        }
        ResourcesFlusher.c(parcel, b);
        return new u(bundle, dVarArr);
    }

    public final /* synthetic */ Object[] newArray(int i2) {
        return new u[i2];
    }
}
