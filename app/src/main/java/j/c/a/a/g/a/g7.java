package j.c.a.a.g.a;

import android.os.RemoteException;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class g7 implements Runnable {
    public final /* synthetic */ AtomicReference b;
    public final /* synthetic */ d9 c;
    public final /* synthetic */ z6 d;

    public g7(z6 z6Var, AtomicReference atomicReference, d9 d9Var) {
        this.d = z6Var;
        this.b = atomicReference;
        this.c = d9Var;
    }

    public final void run() {
        synchronized (this.b) {
            try {
                f3 f3Var = this.d.d;
                if (f3Var == null) {
                    this.d.a().f2046f.a("Failed to get app instance id");
                    this.b.notify();
                    return;
                }
                this.b.set(f3Var.b(this.c));
                String str = (String) this.b.get();
                if (str != null) {
                    this.d.p().g.set(str);
                    this.d.l().f2116l.a(str);
                }
                this.d.D();
                this.b.notify();
            } catch (RemoteException e2) {
                try {
                    this.d.a().f2046f.a("Failed to get app instance id", e2);
                    this.b.notify();
                } catch (Throwable th) {
                    this.b.notify();
                    throw th;
                }
            }
        }
    }
}
