package j.c.a.a.f.e;

import j.c.a.a.f.e.o3;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public interface o3<T extends o3<T>> extends Comparable<T> {
    int a();

    e5 a(e5 e5Var, f5 f5Var);

    i5 a(i5 i5Var, i5 i5Var2);

    w6 b();

    z6 c();

    boolean d();

    boolean e();
}
