package j.c.a.a.e;

import android.os.IBinder;
import android.os.Parcel;
import j.c.a.a.d.a;
import j.c.a.a.f.c.a;
import j.c.a.a.f.c.c;

public final class g extends a implements f {
    public g(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.dynamite.IDynamiteLoader");
    }

    public final j.c.a.a.d.a a(j.c.a.a.d.a aVar, String str, int i2) {
        Parcel a = a();
        c.a(a, aVar);
        a.writeString(str);
        a.writeInt(i2);
        Parcel a2 = a(2, a);
        j.c.a.a.d.a a3 = a.C0025a.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    public final int b(j.c.a.a.d.a aVar, String str, boolean z) {
        Parcel a = a();
        c.a(a, aVar);
        a.writeString(str);
        a.writeInt(z ? 1 : 0);
        Parcel a2 = a(3, a);
        int readInt = a2.readInt();
        a2.recycle();
        return readInt;
    }

    public final int f() {
        Parcel a = a(6, a());
        int readInt = a.readInt();
        a.recycle();
        return readInt;
    }

    public final int a(j.c.a.a.d.a aVar, String str, boolean z) {
        Parcel a = a();
        c.a(a, aVar);
        a.writeString(str);
        a.writeInt(z ? 1 : 0);
        Parcel a2 = a(5, a);
        int readInt = a2.readInt();
        a2.recycle();
        return readInt;
    }

    public final j.c.a.a.d.a b(j.c.a.a.d.a aVar, String str, int i2) {
        Parcel a = a();
        c.a(a, aVar);
        a.writeString(str);
        a.writeInt(i2);
        Parcel a2 = a(4, a);
        j.c.a.a.d.a a3 = a.C0025a.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }
}
