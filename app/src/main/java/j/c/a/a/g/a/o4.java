package j.c.a.a.g.a;

import i.b.k.ResourcesFlusher;
import java.util.concurrent.BlockingQueue;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class o4 extends Thread {
    public final Object b = new Object();
    public final BlockingQueue<p4<?>> c;
    public final /* synthetic */ l4 d;

    public o4(l4 l4Var, String str, BlockingQueue<p4<?>> blockingQueue) {
        this.d = l4Var;
        ResourcesFlusher.b((Object) str);
        ResourcesFlusher.b(blockingQueue);
        this.c = blockingQueue;
        setName(str);
    }

    public final void a() {
        synchronized (this.b) {
            this.b.notifyAll();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:0x005f, code lost:
        r1 = r6.d.f2030i;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0063, code lost:
        monitor-enter(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        r6.d.f2031j.release();
        r6.d.f2030i.notifyAll();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0076, code lost:
        if (r6 != r6.d.c) goto L_0x007d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0078, code lost:
        r6.d.c = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0081, code lost:
        if (r6 != r6.d.d) goto L_0x0088;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0083, code lost:
        r6.d.d = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0088, code lost:
        r6.d.a().f2046f.a("Current scheduler thread is neither worker nor network");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0095, code lost:
        monitor-exit(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0096, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r6 = this;
            r0 = 0
        L_0x0001:
            if (r0 != 0) goto L_0x0011
            j.c.a.a.g.a.l4 r1 = r6.d     // Catch:{ InterruptedException -> 0x000c }
            java.util.concurrent.Semaphore r1 = r1.f2031j     // Catch:{ InterruptedException -> 0x000c }
            r1.acquire()     // Catch:{ InterruptedException -> 0x000c }
            r0 = 1
            goto L_0x0001
        L_0x000c:
            r1 = move-exception
            r6.a(r1)
            goto L_0x0001
        L_0x0011:
            r0 = 0
            int r1 = android.os.Process.myTid()     // Catch:{ all -> 0x00a3 }
            int r1 = android.os.Process.getThreadPriority(r1)     // Catch:{ all -> 0x00a3 }
        L_0x001a:
            java.util.concurrent.BlockingQueue<j.c.a.a.g.a.p4<?>> r2 = r6.c     // Catch:{ all -> 0x00a3 }
            java.lang.Object r2 = r2.poll()     // Catch:{ all -> 0x00a3 }
            j.c.a.a.g.a.p4 r2 = (j.c.a.a.g.a.p4) r2     // Catch:{ all -> 0x00a3 }
            if (r2 == 0) goto L_0x0033
            boolean r3 = r2.c     // Catch:{ all -> 0x00a3 }
            if (r3 == 0) goto L_0x002a
            r3 = r1
            goto L_0x002c
        L_0x002a:
            r3 = 10
        L_0x002c:
            android.os.Process.setThreadPriority(r3)     // Catch:{ all -> 0x00a3 }
            r2.run()     // Catch:{ all -> 0x00a3 }
            goto L_0x001a
        L_0x0033:
            java.lang.Object r2 = r6.b     // Catch:{ all -> 0x00a3 }
            monitor-enter(r2)     // Catch:{ all -> 0x00a3 }
            java.util.concurrent.BlockingQueue<j.c.a.a.g.a.p4<?>> r3 = r6.c     // Catch:{ all -> 0x00a0 }
            java.lang.Object r3 = r3.peek()     // Catch:{ all -> 0x00a0 }
            if (r3 != 0) goto L_0x0050
            j.c.a.a.g.a.l4 r3 = r6.d     // Catch:{ all -> 0x00a0 }
            boolean r3 = r3.f2032k     // Catch:{ all -> 0x00a0 }
            if (r3 != 0) goto L_0x0050
            java.lang.Object r3 = r6.b     // Catch:{ InterruptedException -> 0x004c }
            r4 = 30000(0x7530, double:1.4822E-319)
            r3.wait(r4)     // Catch:{ InterruptedException -> 0x004c }
            goto L_0x0050
        L_0x004c:
            r3 = move-exception
            r6.a(r3)     // Catch:{ all -> 0x00a0 }
        L_0x0050:
            monitor-exit(r2)     // Catch:{ all -> 0x00a0 }
            j.c.a.a.g.a.l4 r2 = r6.d     // Catch:{ all -> 0x00a3 }
            java.lang.Object r2 = r2.f2030i     // Catch:{ all -> 0x00a3 }
            monitor-enter(r2)     // Catch:{ all -> 0x00a3 }
            java.util.concurrent.BlockingQueue<j.c.a.a.g.a.p4<?>> r3 = r6.c     // Catch:{ all -> 0x009d }
            java.lang.Object r3 = r3.peek()     // Catch:{ all -> 0x009d }
            if (r3 != 0) goto L_0x009a
            monitor-exit(r2)     // Catch:{ all -> 0x009d }
            j.c.a.a.g.a.l4 r1 = r6.d
            java.lang.Object r1 = r1.f2030i
            monitor-enter(r1)
            j.c.a.a.g.a.l4 r2 = r6.d     // Catch:{ all -> 0x0097 }
            java.util.concurrent.Semaphore r2 = r2.f2031j     // Catch:{ all -> 0x0097 }
            r2.release()     // Catch:{ all -> 0x0097 }
            j.c.a.a.g.a.l4 r2 = r6.d     // Catch:{ all -> 0x0097 }
            java.lang.Object r2 = r2.f2030i     // Catch:{ all -> 0x0097 }
            r2.notifyAll()     // Catch:{ all -> 0x0097 }
            j.c.a.a.g.a.l4 r2 = r6.d     // Catch:{ all -> 0x0097 }
            j.c.a.a.g.a.o4 r2 = r2.c     // Catch:{ all -> 0x0097 }
            if (r6 != r2) goto L_0x007d
            j.c.a.a.g.a.l4 r2 = r6.d     // Catch:{ all -> 0x0097 }
            r2.c = r0     // Catch:{ all -> 0x0097 }
            goto L_0x0095
        L_0x007d:
            j.c.a.a.g.a.l4 r2 = r6.d     // Catch:{ all -> 0x0097 }
            j.c.a.a.g.a.o4 r2 = r2.d     // Catch:{ all -> 0x0097 }
            if (r6 != r2) goto L_0x0088
            j.c.a.a.g.a.l4 r2 = r6.d     // Catch:{ all -> 0x0097 }
            r2.d = r0     // Catch:{ all -> 0x0097 }
            goto L_0x0095
        L_0x0088:
            j.c.a.a.g.a.l4 r0 = r6.d     // Catch:{ all -> 0x0097 }
            j.c.a.a.g.a.n3 r0 = r0.a()     // Catch:{ all -> 0x0097 }
            j.c.a.a.g.a.p3 r0 = r0.f2046f     // Catch:{ all -> 0x0097 }
            java.lang.String r2 = "Current scheduler thread is neither worker nor network"
            r0.a(r2)     // Catch:{ all -> 0x0097 }
        L_0x0095:
            monitor-exit(r1)     // Catch:{ all -> 0x0097 }
            return
        L_0x0097:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0097 }
            throw r0
        L_0x009a:
            monitor-exit(r2)     // Catch:{ all -> 0x009d }
            goto L_0x001a
        L_0x009d:
            r1 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x009d }
            throw r1     // Catch:{ all -> 0x00a3 }
        L_0x00a0:
            r1 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x00a0 }
            throw r1     // Catch:{ all -> 0x00a3 }
        L_0x00a3:
            r1 = move-exception
            j.c.a.a.g.a.l4 r2 = r6.d
            java.lang.Object r2 = r2.f2030i
            monitor-enter(r2)
            j.c.a.a.g.a.l4 r3 = r6.d     // Catch:{ all -> 0x00dc }
            java.util.concurrent.Semaphore r3 = r3.f2031j     // Catch:{ all -> 0x00dc }
            r3.release()     // Catch:{ all -> 0x00dc }
            j.c.a.a.g.a.l4 r3 = r6.d     // Catch:{ all -> 0x00dc }
            java.lang.Object r3 = r3.f2030i     // Catch:{ all -> 0x00dc }
            r3.notifyAll()     // Catch:{ all -> 0x00dc }
            j.c.a.a.g.a.l4 r3 = r6.d     // Catch:{ all -> 0x00dc }
            j.c.a.a.g.a.o4 r3 = r3.c     // Catch:{ all -> 0x00dc }
            if (r6 == r3) goto L_0x00d6
            j.c.a.a.g.a.l4 r3 = r6.d     // Catch:{ all -> 0x00dc }
            j.c.a.a.g.a.o4 r3 = r3.d     // Catch:{ all -> 0x00dc }
            if (r6 != r3) goto L_0x00c8
            j.c.a.a.g.a.l4 r3 = r6.d     // Catch:{ all -> 0x00dc }
            r3.d = r0     // Catch:{ all -> 0x00dc }
            goto L_0x00da
        L_0x00c8:
            j.c.a.a.g.a.l4 r0 = r6.d     // Catch:{ all -> 0x00dc }
            j.c.a.a.g.a.n3 r0 = r0.a()     // Catch:{ all -> 0x00dc }
            j.c.a.a.g.a.p3 r0 = r0.f2046f     // Catch:{ all -> 0x00dc }
            java.lang.String r3 = "Current scheduler thread is neither worker nor network"
            r0.a(r3)     // Catch:{ all -> 0x00dc }
            goto L_0x00da
        L_0x00d6:
            j.c.a.a.g.a.l4 r3 = r6.d     // Catch:{ all -> 0x00dc }
            r3.c = r0     // Catch:{ all -> 0x00dc }
        L_0x00da:
            monitor-exit(r2)     // Catch:{ all -> 0x00dc }
            throw r1
        L_0x00dc:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x00dc }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.o4.run():void");
    }

    public final void a(InterruptedException interruptedException) {
        this.d.a().f2047i.a(String.valueOf(getName()).concat(" was interrupted"), interruptedException);
    }
}
