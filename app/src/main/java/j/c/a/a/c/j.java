package j.c.a.a.c;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import i.l.a.DialogFragment;

public class j extends DialogFragment {
    public Dialog i0 = null;
    public DialogInterface.OnCancelListener j0 = null;

    public Dialog g(Bundle bundle) {
        if (this.i0 == null) {
            super.c0 = false;
        }
        return this.i0;
    }

    public void onCancel(DialogInterface dialogInterface) {
        DialogInterface.OnCancelListener onCancelListener = this.j0;
        if (onCancelListener != null) {
            onCancelListener.onCancel(dialogInterface);
        }
    }
}
