package j.c.a.a.g.a;

import android.text.TextUtils;
import com.crashlytics.android.core.DefaultAppMeasurementEventListenerRegistrar;
import i.b.k.ResourcesFlusher;
import j.a.a.a.outline;
import j.c.a.a.c.n.b;
import j.c.a.a.f.e.a0;
import j.c.a.a.f.e.b0;
import j.c.a.a.f.e.c0;
import j.c.a.a.f.e.d0;
import j.c.a.a.f.e.e0;
import j.c.a.a.f.e.e4;
import j.c.a.a.f.e.o0;
import j.c.a.a.f.e.p0;
import j.c.a.a.f.e.q0;
import j.c.a.a.f.e.s0;
import j.c.a.a.f.e.t0;
import j.c.a.a.f.e.u0;
import j.c.a.a.f.e.w0;
import j.c.a.a.f.e.x0;
import j.c.a.a.f.e.y0;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class u8 extends o8 {
    public u8(q8 q8Var) {
        super(q8Var);
    }

    public static Object b(q0 q0Var, String str) {
        s0 a = a(q0Var, str);
        if (a == null) {
            return null;
        }
        if ((a.zzc & 2) != 0) {
            return a.zze;
        }
        if (a.j()) {
            return Long.valueOf(a.zzf);
        }
        if (a.n()) {
            return Double.valueOf(a.zzh);
        }
        return null;
    }

    public final void a(y0.a aVar, Object obj) {
        ResourcesFlusher.b(obj);
        aVar.i();
        y0 y0Var = (y0) aVar.c;
        y0Var.zzc &= -5;
        y0Var.zzf = y0.zzj.zzf;
        aVar.i();
        y0 y0Var2 = (y0) aVar.c;
        y0Var2.zzc &= -9;
        y0Var2.zzg = 0;
        aVar.i();
        y0 y0Var3 = (y0) aVar.c;
        y0Var3.zzc &= -33;
        y0Var3.zzi = 0.0d;
        if (obj instanceof String) {
            aVar.i();
            y0.b((y0) aVar.c, (String) obj);
        } else if (obj instanceof Long) {
            aVar.b(((Long) obj).longValue());
        } else if (obj instanceof Double) {
            double doubleValue = ((Double) obj).doubleValue();
            aVar.i();
            y0 y0Var4 = (y0) aVar.c;
            y0Var4.zzc |= 32;
            y0Var4.zzi = doubleValue;
        } else {
            a().f2046f.a("Ignoring invalid (type) user attribute value", obj);
        }
    }

    public final byte[] c(byte[] bArr) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            gZIPOutputStream.write(bArr);
            gZIPOutputStream.close();
            byteArrayOutputStream.close();
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e2) {
            a().f2046f.a("Failed to gzip content", e2);
            throw e2;
        }
    }

    public final boolean q() {
        return false;
    }

    public final List<Integer> u() {
        Map<String, String> a = k.a(super.b.f2073i.a);
        if (a == null || a.size() == 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        int intValue = k.R.a(null).intValue();
        Iterator<Map.Entry<String, String>> it = a.entrySet().iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Map.Entry next = it.next();
            if (((String) next.getKey()).startsWith("measurement.id.")) {
                try {
                    int parseInt = Integer.parseInt((String) next.getValue());
                    if (parseInt != 0) {
                        arrayList.add(Integer.valueOf(parseInt));
                        if (arrayList.size() >= intValue) {
                            a().f2047i.a("Too many experiment IDs. Number of IDs", Integer.valueOf(arrayList.size()));
                            break;
                        }
                    } else {
                        continue;
                    }
                } catch (NumberFormatException e2) {
                    a().f2047i.a("Experiment ID NumberFormatException", e2);
                }
            }
        }
        if (arrayList.size() == 0) {
            return null;
        }
        return arrayList;
    }

    public final byte[] b(byte[] bArr) {
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
            GZIPInputStream gZIPInputStream = new GZIPInputStream(byteArrayInputStream);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] bArr2 = new byte[1024];
            while (true) {
                int read = gZIPInputStream.read(bArr2);
                if (read > 0) {
                    byteArrayOutputStream.write(bArr2, 0, read);
                } else {
                    gZIPInputStream.close();
                    byteArrayInputStream.close();
                    return byteArrayOutputStream.toByteArray();
                }
            }
        } catch (IOException e2) {
            a().f2046f.a("Failed to ungzip content", e2);
            throw e2;
        }
    }

    public static s0 a(q0 q0Var, String str) {
        for (s0 next : q0Var.zzd) {
            if (next.zzd.equals(str)) {
                return next;
            }
        }
        return null;
    }

    public final boolean a(long j2, long j3) {
        if (j2 == 0 || j3 <= 0) {
            return true;
        }
        if (((b) this.a.f2092n) == null) {
            throw null;
        } else if (Math.abs(System.currentTimeMillis() - j2) > j3) {
            return true;
        } else {
            return false;
        }
    }

    public final void a(s0.a aVar, Object obj) {
        ResourcesFlusher.b(obj);
        aVar.i();
        s0 s0Var = (s0) aVar.c;
        s0Var.zzc &= -3;
        s0Var.zze = s0.zzi.zze;
        aVar.i();
        s0 s0Var2 = (s0) aVar.c;
        s0Var2.zzc &= -5;
        s0Var2.zzf = 0;
        aVar.i();
        s0 s0Var3 = (s0) aVar.c;
        s0Var3.zzc &= -17;
        s0Var3.zzh = 0.0d;
        if (obj instanceof String) {
            aVar.i();
            s0.b((s0) aVar.c, (String) obj);
        } else if (obj instanceof Long) {
            aVar.a(((Long) obj).longValue());
        } else if (obj instanceof Double) {
            double doubleValue = ((Double) obj).doubleValue();
            aVar.i();
            s0 s0Var4 = (s0) aVar.c;
            s0Var4.zzc |= 16;
            s0Var4.zzh = doubleValue;
        } else {
            a().f2046f.a("Ignoring invalid (type) event param value", obj);
        }
    }

    public static void a(q0.a aVar, String str, Object obj) {
        List<s0> a = aVar.a();
        int i2 = 0;
        while (true) {
            if (i2 >= a.size()) {
                i2 = -1;
                break;
            } else if (str.equals(a.get(i2).zzd)) {
                break;
            } else {
                i2++;
            }
        }
        s0.a p2 = s0.p();
        p2.i();
        s0.a((s0) p2.c, str);
        if (obj instanceof Long) {
            p2.a(((Long) obj).longValue());
        } else if (obj instanceof String) {
            p2.b((String) obj);
        } else if (obj instanceof Double) {
            double doubleValue = ((Double) obj).doubleValue();
            p2.i();
            s0 s0Var = (s0) p2.c;
            s0Var.zzc |= 16;
            s0Var.zzh = doubleValue;
        }
        if (i2 >= 0) {
            aVar.i();
            q0.a((q0) aVar.c, i2, p2);
            return;
        }
        aVar.i();
        q0.a((q0) aVar.c, p2);
    }

    public final String a(t0 t0Var) {
        e4<s0> e4Var;
        t0 t0Var2 = t0Var;
        if (t0Var2 == null) {
            return "";
        }
        StringBuilder a = outline.a("\nbatch {\n");
        for (u0 next : t0Var2.zzc) {
            if (next != null) {
                int i2 = 1;
                a(a, 1);
                a.append("bundle {\n");
                if ((next.zzc & 1) != 0) {
                    a(a, 1, "protocol_version", Integer.valueOf(next.zze));
                }
                a(a, 1, "platform", next.zzm);
                if ((next.zzc & 16384) != 0) {
                    a(a, 1, "gmp_version", Long.valueOf(next.zzu));
                }
                if ((next.zzc & 32768) != 0) {
                    a(a, 1, "uploading_gmp_version", Long.valueOf(next.zzv));
                }
                if ((next.zzd & 16) != 0) {
                    a(a, 1, "dynamite_version", Long.valueOf(next.zzas));
                }
                if ((next.zzc & 536870912) != 0) {
                    a(a, 1, "config_version", Long.valueOf(next.zzak));
                }
                a(a, 1, "gmp_app_id", next.zzac);
                a(a, 1, "admob_app_id", next.zzap);
                a(a, 1, "app_id", next.zzs);
                a(a, 1, "app_version", next.zzt);
                if ((next.zzc & 33554432) != 0) {
                    a(a, 1, "app_version_major", Integer.valueOf(next.zzag));
                }
                a(a, 1, "firebase_instance_id", next.zzaf);
                if ((next.zzc & 524288) != 0) {
                    a(a, 1, "dev_cert_hash", Long.valueOf(next.zzz));
                }
                a(a, 1, "app_store", next.zzr);
                if ((next.zzc & 2) != 0) {
                    a(a, 1, "upload_timestamp_millis", Long.valueOf(next.zzh));
                }
                if ((next.zzc & 4) != 0) {
                    a(a, 1, "start_timestamp_millis", Long.valueOf(next.zzi));
                }
                if ((next.zzc & 8) != 0) {
                    a(a, 1, "end_timestamp_millis", Long.valueOf(next.zzj));
                }
                if ((next.zzc & 16) != 0) {
                    a(a, 1, "previous_bundle_start_timestamp_millis", Long.valueOf(next.zzk));
                }
                if ((next.zzc & 32) != 0) {
                    a(a, 1, "previous_bundle_end_timestamp_millis", Long.valueOf(next.zzl));
                }
                a(a, 1, "app_instance_id", next.zzy);
                a(a, 1, "resettable_device_id", next.zzw);
                a(a, 1, "device_id", next.zzaj);
                a(a, 1, "ds_id", next.zzam);
                if ((next.zzc & 131072) != 0) {
                    a(a, 1, "limited_ad_tracking", Boolean.valueOf(next.zzx));
                }
                a(a, 1, "os_version", next.zzn);
                a(a, 1, "device_model", next.zzo);
                a(a, 1, "user_default_language", next.zzp);
                if ((next.zzc & 1024) != 0) {
                    a(a, 1, "time_zone_offset_minutes", Integer.valueOf(next.zzq));
                }
                if ((next.zzc & 1048576) != 0) {
                    a(a, 1, "bundle_sequential_index", Integer.valueOf(next.zzaa));
                }
                if ((next.zzc & 8388608) != 0) {
                    a(a, 1, "service_upload", Boolean.valueOf(next.zzad));
                }
                a(a, 1, "health_monitor", next.zzab);
                if ((next.zzc & 1073741824) != 0) {
                    long j2 = next.zzal;
                    if (j2 != 0) {
                        a(a, 1, "android_id", Long.valueOf(j2));
                    }
                }
                if ((next.zzd & 2) != 0) {
                    a(a, 1, "retry_counter", Integer.valueOf(next.zzao));
                }
                e4<y0> e4Var2 = next.zzg;
                if (e4Var2 != null) {
                    for (y0 next2 : e4Var2) {
                        if (next2 != null) {
                            a(a, 2);
                            a.append("user_property {\n");
                            a(a, 2, "set_timestamp_millis", (next2.zzc & 1) != 0 ? Long.valueOf(next2.zzd) : null);
                            a(a, 2, DefaultAppMeasurementEventListenerRegistrar.NAME, g().c(next2.zze));
                            a(a, 2, "string_value", next2.zzf);
                            a(a, 2, "int_value", (next2.zzc & 8) != 0 ? Long.valueOf(next2.zzg) : null);
                            a(a, 2, "double_value", (next2.zzc & 32) != 0 ? Double.valueOf(next2.zzi) : null);
                            a(a, 2);
                            a.append("}\n");
                        }
                    }
                }
                e4<o0> e4Var3 = next.zzae;
                String str = next.zzs;
                if (e4Var3 != null) {
                    for (o0 next3 : e4Var3) {
                        if (next3 != null) {
                            a(a, 2);
                            a.append("audience_membership {\n");
                            if ((i2 & next3.zzc) != 0) {
                                a(a, 2, "audience_id", Integer.valueOf(next3.zzd));
                            }
                            if ((next3.zzc & 8) != 0) {
                                a(a, 2, "new_audience", Boolean.valueOf(next3.zzg));
                            }
                            w0 w0Var = next3.zze;
                            if (w0Var == null) {
                                w0Var = w0.zzg;
                            }
                            String str2 = str;
                            a(a, 2, "current_data", w0Var, str);
                            w0 w0Var2 = next3.zzf;
                            if (w0Var2 == null) {
                                w0Var2 = w0.zzg;
                            }
                            str = str2;
                            a(a, 2, "previous_data", w0Var2, str);
                            a(a, 2);
                            a.append("}\n");
                            i2 = 1;
                        }
                    }
                }
                e4<q0> e4Var4 = next.zzf;
                if (e4Var4 != null) {
                    for (q0 next4 : e4Var4) {
                        if (next4 != null) {
                            a(a, 2);
                            a.append("event {\n");
                            a(a, 2, DefaultAppMeasurementEventListenerRegistrar.NAME, g().a(next4.zze));
                            if (next4.j()) {
                                a(a, 2, "timestamp_millis", Long.valueOf(next4.zzf));
                            }
                            if ((next4.zzc & 4) != 0) {
                                a(a, 2, "previous_timestamp_millis", Long.valueOf(next4.zzg));
                            }
                            if ((next4.zzc & 8) != 0) {
                                a(a, 2, "count", Integer.valueOf(next4.zzh));
                            }
                            if (!(next4.zzd.size() == 0 || (e4Var = next4.zzd) == null)) {
                                for (s0 next5 : e4Var) {
                                    if (next5 != null) {
                                        a(a, 3);
                                        a.append("param {\n");
                                        a(a, 3, DefaultAppMeasurementEventListenerRegistrar.NAME, g().b(next5.zzd));
                                        a(a, 3, "string_value", next5.zze);
                                        a(a, 3, "int_value", next5.j() ? Long.valueOf(next5.zzf) : null);
                                        a(a, 3, "double_value", next5.n() ? Double.valueOf(next5.zzh) : null);
                                        a(a, 3);
                                        a.append("}\n");
                                    }
                                }
                            }
                            a(a, 2);
                            a.append("}\n");
                        }
                    }
                }
                a(a, 1);
                a.append("}\n");
            }
        }
        a.append("}\n");
        return a.toString();
    }

    public final String a(a0 a0Var) {
        if (a0Var == null) {
            return "null";
        }
        StringBuilder a = outline.a("\nevent_filter {\n");
        if (a0Var.a()) {
            a(a, 0, "filter_id", Integer.valueOf(a0Var.zzd));
        }
        a(a, 0, "event_name", g().a(a0Var.zze));
        String a2 = a(a0Var.zzi, a0Var.zzj, a0Var.zzk);
        if (!a2.isEmpty()) {
            a(a, 0, "filter_type", a2);
        }
        c0 c0Var = a0Var.zzh;
        if (c0Var == null) {
            c0Var = c0.zzi;
        }
        a(a, 1, "event_count_filter", c0Var);
        a.append("  filters {\n");
        for (b0 a3 : a0Var.zzf) {
            a(a, 2, a3);
        }
        a(a, 1);
        a.append("}\n}\n");
        return a.toString();
    }

    public final String a(d0 d0Var) {
        if (d0Var == null) {
            return "null";
        }
        StringBuilder a = outline.a("\nproperty_filter {\n");
        if (d0Var.a()) {
            a(a, 0, "filter_id", Integer.valueOf(d0Var.zzd));
        }
        a(a, 0, "property_name", g().c(d0Var.zze));
        String a2 = a(d0Var.zzg, d0Var.zzh, d0Var.zzi);
        if (!a2.isEmpty()) {
            a(a, 0, "filter_type", a2);
        }
        b0 b0Var = d0Var.zzf;
        if (b0Var == null) {
            b0Var = b0.zzh;
        }
        a(a, 1, b0Var);
        a.append("}\n");
        return a.toString();
    }

    public static String a(boolean z, boolean z2, boolean z3) {
        StringBuilder sb = new StringBuilder();
        if (z) {
            sb.append("Dynamic ");
        }
        if (z2) {
            sb.append("Sequence ");
        }
        if (z3) {
            sb.append("Session-Scoped ");
        }
        return sb.toString();
    }

    public final void a(StringBuilder sb, int i2, String str, w0 w0Var, String str2) {
        if (w0Var != null) {
            a(sb, 3);
            sb.append(str);
            sb.append(" {\n");
            if (w0Var.zzd.size() != 0) {
                a(sb, 4);
                sb.append("results: ");
                int i3 = 0;
                for (Long l2 : w0Var.zzd) {
                    int i4 = i3 + 1;
                    if (i3 != 0) {
                        sb.append(", ");
                    }
                    sb.append(l2);
                    i3 = i4;
                }
                sb.append(10);
            }
            if (w0Var.zzc.size() != 0) {
                a(sb, 4);
                sb.append("status: ");
                int i5 = 0;
                for (Long l3 : w0Var.zzc) {
                    int i6 = i5 + 1;
                    if (i5 != 0) {
                        sb.append(", ");
                    }
                    sb.append(l3);
                    i5 = i6;
                }
                sb.append(10);
            }
            if (this.a.g.d(str2)) {
                if (w0Var.m() != 0) {
                    a(sb, 4);
                    sb.append("dynamic_filter_timestamps: {");
                    int i7 = 0;
                    for (p0 next : w0Var.zze) {
                        int i8 = i7 + 1;
                        if (i7 != 0) {
                            sb.append(", ");
                        }
                        boolean z = true;
                        sb.append((next.zzc & 1) != 0 ? Integer.valueOf(next.zzd) : null);
                        sb.append(":");
                        if ((next.zzc & 2) == 0) {
                            z = false;
                        }
                        sb.append(z ? Long.valueOf(next.zze) : null);
                        i7 = i8;
                    }
                    sb.append("}\n");
                }
                if (w0Var.zzf.size() != 0) {
                    a(sb, 4);
                    sb.append("sequence_filter_timestamps: {");
                    int i9 = 0;
                    for (x0 next2 : w0Var.zzf) {
                        int i10 = i9 + 1;
                        if (i9 != 0) {
                            sb.append(", ");
                        }
                        sb.append(next2.a() ? Integer.valueOf(next2.zzd) : null);
                        sb.append(": [");
                        int i11 = 0;
                        for (Long longValue : next2.zze) {
                            long longValue2 = longValue.longValue();
                            int i12 = i11 + 1;
                            if (i11 != 0) {
                                sb.append(", ");
                            }
                            sb.append(longValue2);
                            i11 = i12;
                        }
                        sb.append("]");
                        i9 = i10;
                    }
                    sb.append("}\n");
                }
            }
            a(sb, 3);
            sb.append("}\n");
        }
    }

    public final void a(StringBuilder sb, int i2, String str, c0 c0Var) {
        if (c0Var != null) {
            a(sb, i2);
            sb.append(str);
            sb.append(" {\n");
            boolean z = true;
            if ((c0Var.zzc & 1) != 0) {
                a(sb, i2, "comparison_type", c0Var.i().name());
            }
            if ((c0Var.zzc & 2) == 0) {
                z = false;
            }
            if (z) {
                a(sb, i2, "match_as_float", Boolean.valueOf(c0Var.zze));
            }
            a(sb, i2, "comparison_value", c0Var.zzf);
            a(sb, i2, "min_comparison_value", c0Var.zzg);
            a(sb, i2, "max_comparison_value", c0Var.zzh);
            a(sb, i2);
            sb.append("}\n");
        }
    }

    public final void a(StringBuilder sb, int i2, b0 b0Var) {
        if (b0Var != null) {
            a(sb, i2);
            sb.append("filter {\n");
            boolean z = false;
            if ((b0Var.zzc & 4) != 0) {
                a(sb, i2, "complement", Boolean.valueOf(b0Var.zzf));
            }
            a(sb, i2, "param_name", g().b(b0Var.zzg));
            int i3 = i2 + 1;
            e0 i4 = b0Var.i();
            if (i4 != null) {
                a(sb, i3);
                sb.append("string_filter");
                sb.append(" {\n");
                if ((i4.zzc & 1) != 0) {
                    a(sb, i3, "match_type", i4.i().name());
                }
                a(sb, i3, "expression", i4.zze);
                if ((i4.zzc & 4) != 0) {
                    z = true;
                }
                if (z) {
                    a(sb, i3, "case_sensitive", Boolean.valueOf(i4.zzf));
                }
                if (i4.j() > 0) {
                    a(sb, i3 + 1);
                    sb.append("expression_list {\n");
                    for (String append : i4.zzg) {
                        a(sb, i3 + 2);
                        sb.append(append);
                        sb.append("\n");
                    }
                    sb.append("}\n");
                }
                a(sb, i3);
                sb.append("}\n");
            }
            a(sb, i3, "number_filter", b0Var.m());
            a(sb, i2);
            sb.append("}\n");
        }
    }

    public static void a(StringBuilder sb, int i2) {
        for (int i3 = 0; i3 < i2; i3++) {
            sb.append("  ");
        }
    }

    public static void a(StringBuilder sb, int i2, String str, Object obj) {
        if (obj != null) {
            a(sb, i2 + 1);
            sb.append(str);
            sb.append(": ");
            sb.append(obj);
            sb.append(10);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        a().f2046f.a("Failed to load parcelable from buffer");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002a, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002b, code lost:
        r1.recycle();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002e, code lost:
        throw r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001a, code lost:
        r5 = move-exception;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x001c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final <T extends android.os.Parcelable> T a(byte[] r5, android.os.Parcelable.Creator r6) {
        /*
            r4 = this;
            r0 = 0
            if (r5 != 0) goto L_0x0004
            return r0
        L_0x0004:
            android.os.Parcel r1 = android.os.Parcel.obtain()
            int r2 = r5.length     // Catch:{ SafeParcelReader$ParseException -> 0x001c }
            r3 = 0
            r1.unmarshall(r5, r3, r2)     // Catch:{ SafeParcelReader$ParseException -> 0x001c }
            r1.setDataPosition(r3)     // Catch:{ SafeParcelReader$ParseException -> 0x001c }
            java.lang.Object r5 = r6.createFromParcel(r1)     // Catch:{ SafeParcelReader$ParseException -> 0x001c }
            android.os.Parcelable r5 = (android.os.Parcelable) r5     // Catch:{ SafeParcelReader$ParseException -> 0x001c }
            r1.recycle()
            return r5
        L_0x001a:
            r5 = move-exception
            goto L_0x002b
        L_0x001c:
            j.c.a.a.g.a.n3 r5 = r4.a()     // Catch:{ all -> 0x001a }
            j.c.a.a.g.a.p3 r5 = r5.f2046f     // Catch:{ all -> 0x001a }
            java.lang.String r6 = "Failed to load parcelable from buffer"
            r5.a(r6)     // Catch:{ all -> 0x001a }
            r1.recycle()
            return r0
        L_0x002b:
            r1.recycle()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.u8.a(byte[], android.os.Parcelable$Creator):android.os.Parcelable");
    }

    public final boolean a(i iVar, d9 d9Var) {
        ResourcesFlusher.b(iVar);
        ResourcesFlusher.b(d9Var);
        if (!TextUtils.isEmpty(d9Var.c) || !TextUtils.isEmpty(d9Var.f1963s)) {
            return true;
        }
        h9 h9Var = this.a.f2086f;
        return false;
    }

    public static boolean a(String str) {
        return str != null && str.matches("([+-])?([0-9]+\\.?[0-9]*|[0-9]*\\.?[0-9]+)") && str.length() <= 310;
    }

    public static boolean a(List<Long> list, int i2) {
        if (i2 >= (list.size() << 6)) {
            return false;
        }
        return ((1 << (i2 % 64)) & list.get(i2 / 64).longValue()) != 0;
    }

    public static List<Long> a(BitSet bitSet) {
        int length = (bitSet.length() + 63) / 64;
        ArrayList arrayList = new ArrayList(length);
        for (int i2 = 0; i2 < length; i2++) {
            long j2 = 0;
            for (int i3 = 0; i3 < 64; i3++) {
                int i4 = (i2 << 6) + i3;
                if (i4 >= bitSet.length()) {
                    break;
                }
                if (bitSet.get(i4)) {
                    j2 |= 1 << i3;
                }
            }
            arrayList.add(Long.valueOf(j2));
        }
        return arrayList;
    }

    public final List<Long> a(List<Long> list, List<Integer> list2) {
        int i2;
        ArrayList arrayList = new ArrayList(list);
        for (Integer next : list2) {
            if (next.intValue() < 0) {
                a().f2047i.a("Ignoring negative bit index to be cleared", next);
            } else {
                int intValue = next.intValue() / 64;
                if (intValue >= arrayList.size()) {
                    a().f2047i.a("Ignoring bit index greater than bitSet size", next, Integer.valueOf(arrayList.size()));
                } else {
                    arrayList.set(intValue, Long.valueOf(((Long) arrayList.get(intValue)).longValue() & (~(1 << (next.intValue() % 64)))));
                }
            }
        }
        int size = arrayList.size();
        int size2 = arrayList.size() - 1;
        while (true) {
            int i3 = size2;
            i2 = size;
            size = i3;
            if (size >= 0 && ((Long) arrayList.get(size)).longValue() == 0) {
                size2 = size - 1;
            }
        }
        return arrayList.subList(0, i2);
    }

    public final long a(byte[] bArr) {
        ResourcesFlusher.b(bArr);
        k().d();
        MessageDigest x = y8.x();
        if (x != null) {
            return y8.a(x.digest(bArr));
        }
        a().f2046f.a("Failed to get MD5");
        return 0;
    }
}
