package j.c.a.a.f.e;

import j.c.a.a.f.e.s0;
import j.c.a.a.f.e.x3;
import java.util.Collections;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class q0 extends x3<q0, a> implements g5 {
    public static final q0 zzi;
    public static volatile m5<q0> zzj;
    public int zzc;
    public e4<s0> zzd = s5.f1905e;
    public String zze = "";
    public long zzf;
    public long zzg;
    public int zzh;

    static {
        q0 q0Var = new q0();
        zzi = q0Var;
        x3.zzd.put(q0.class, q0Var);
    }

    public static a o() {
        return (a) zzi.g();
    }

    public final List<s0> a() {
        return this.zzd;
    }

    public final String i() {
        return this.zze;
    }

    public final boolean j() {
        return (this.zzc & 2) != 0;
    }

    public final long m() {
        return this.zzf;
    }

    public final void n() {
        if (!this.zzd.a()) {
            this.zzd = x3.a(this.zzd);
        }
    }

    /* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
    public static final class a extends x3.a<q0, a> implements g5 {
        public a() {
            super(q0.zzi);
        }

        public final List<s0> a() {
            return Collections.unmodifiableList(((q0) super.c).zzd);
        }

        public final a b(int i2) {
            i();
            q0.a((q0) super.c, i2);
            return this;
        }

        public final int n() {
            return ((q0) super.c).zzd.size();
        }

        public final String o() {
            return ((q0) super.c).zze;
        }

        public final long p() {
            return ((q0) super.c).zzf;
        }

        public final long q() {
            return ((q0) super.c).zzg;
        }

        public /* synthetic */ a(z0 z0Var) {
            super(q0.zzi);
        }

        public final s0 a(int i2) {
            return ((q0) super.c).zzd.get(i2);
        }

        public final a a(int i2, s0 s0Var) {
            i();
            q0.a((q0) super.c, i2, s0Var);
            return this;
        }

        public final a a(s0.a aVar) {
            i();
            q0.a((q0) super.c, aVar);
            return this;
        }

        public final a a(String str) {
            i();
            q0.a((q0) super.c, str);
            return this;
        }

        public final a a(long j2) {
            i();
            q0 q0Var = (q0) super.c;
            q0Var.zzc |= 2;
            q0Var.zzf = j2;
            return this;
        }
    }

    public static /* synthetic */ void a(q0 q0Var, int i2, s0 s0Var) {
        if (s0Var != null) {
            q0Var.n();
            q0Var.zzd.set(i2, s0Var);
            return;
        }
        throw null;
    }

    public static /* synthetic */ void a(q0 q0Var, int i2, s0.a aVar) {
        q0Var.n();
        q0Var.zzd.set(i2, (s0) ((x3) aVar.m()));
    }

    public static /* synthetic */ void a(q0 q0Var, s0 s0Var) {
        if (s0Var != null) {
            q0Var.n();
            q0Var.zzd.add(s0Var);
            return;
        }
        throw null;
    }

    public static /* synthetic */ void a(q0 q0Var, s0.a aVar) {
        q0Var.n();
        q0Var.zzd.add((s0) ((x3) aVar.m()));
    }

    public static /* synthetic */ void a(q0 q0Var, int i2) {
        q0Var.n();
        q0Var.zzd.remove(i2);
    }

    public static /* synthetic */ void a(q0 q0Var, String str) {
        if (str != null) {
            q0Var.zzc |= 1;
            q0Var.zze = str;
            return;
        }
        throw null;
    }

    public final Object a(int i2, Object obj, Object obj2) {
        switch (z0.a[i2 - 1]) {
            case 1:
                return new q0();
            case 2:
                return new a(null);
            case 3:
                return new r5(zzi, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0001\u0000\u0001\u001b\u0002\b\u0000\u0003\u0002\u0001\u0004\u0002\u0002\u0005\u0004\u0003", new Object[]{"zzc", "zzd", s0.class, "zze", "zzf", "zzg", "zzh"});
            case 4:
                return zzi;
            case 5:
                m5<q0> m5Var = zzj;
                if (m5Var == null) {
                    synchronized (q0.class) {
                        m5Var = zzj;
                        if (m5Var == null) {
                            m5Var = new x3.c<>(zzi);
                            zzj = m5Var;
                        }
                    }
                }
                return m5Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
