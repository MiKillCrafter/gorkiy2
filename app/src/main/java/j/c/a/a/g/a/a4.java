package j.c.a.a.g.a;

import android.content.SharedPreferences;
import i.b.k.ResourcesFlusher;
import j.c.a.a.c.n.b;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class a4 {
    public final String a;
    public final String b;
    public final String c;
    public final long d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ w3 f1933e;

    public /* synthetic */ a4(w3 w3Var, String str, long j2, z3 z3Var) {
        this.f1933e = w3Var;
        ResourcesFlusher.b(str);
        ResourcesFlusher.a(j2 > 0);
        this.a = String.valueOf(str).concat(":start");
        this.b = String.valueOf(str).concat(":count");
        this.c = String.valueOf(str).concat(":value");
        this.d = j2;
    }

    public final void a() {
        this.f1933e.d();
        if (((b) this.f1933e.a.f2092n) != null) {
            long currentTimeMillis = System.currentTimeMillis();
            SharedPreferences.Editor edit = this.f1933e.v().edit();
            edit.remove(this.b);
            edit.remove(this.c);
            edit.putLong(this.a, currentTimeMillis);
            edit.apply();
            return;
        }
        throw null;
    }
}
