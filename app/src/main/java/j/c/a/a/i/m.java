package j.c.a.a.i;

import com.google.android.gms.tasks.RuntimeExecutionException;

public final class m implements Runnable {
    public final /* synthetic */ e b;
    public final /* synthetic */ l c;

    public m(l lVar, e eVar) {
        this.c = lVar;
        this.b = eVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.d):j.c.a.a.i.e<TResult>
     arg types: [java.util.concurrent.Executor, j.c.a.a.i.l]
     candidates:
      j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.b):j.c.a.a.i.e<TResult>
      j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.c):j.c.a.a.i.e<TResult>
      j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.d):j.c.a.a.i.e<TResult> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.c):j.c.a.a.i.e<TResult>
     arg types: [java.util.concurrent.Executor, j.c.a.a.i.l]
     candidates:
      j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.b):j.c.a.a.i.e<TResult>
      j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.d):j.c.a.a.i.e<TResult>
      j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.c):j.c.a.a.i.e<TResult> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.b):j.c.a.a.i.e<TResult>
     arg types: [java.util.concurrent.Executor, j.c.a.a.i.l]
     candidates:
      j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.c):j.c.a.a.i.e<TResult>
      j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.d):j.c.a.a.i.e<TResult>
      j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.b):j.c.a.a.i.e<TResult> */
    public final void run() {
        try {
            e a = this.c.b.a(this.b);
            if (a == null) {
                l lVar = this.c;
                lVar.c.a((Exception) new NullPointerException("Continuation returned null"));
                return;
            }
            a.a(g.a, (d) this.c);
            a.a(g.a, (c) this.c);
            a.a(g.a, (b) this.c);
        } catch (RuntimeExecutionException e2) {
            if (e2.getCause() instanceof Exception) {
                this.c.c.a((Exception) e2.getCause());
            } else {
                this.c.c.a((Exception) e2);
            }
        } catch (Exception e3) {
            this.c.c.a(e3);
        }
    }
}
