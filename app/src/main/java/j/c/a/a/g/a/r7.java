package j.c.a.a.g.a;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Looper;
import i.b.k.ResourcesFlusher;
import j.c.a.a.c.l.b;
import j.c.a.a.c.m.a;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class r7 implements ServiceConnection, b.a, b.C0023b {
    public volatile boolean a;
    public volatile k3 b;
    public final /* synthetic */ z6 c;

    public r7(z6 z6Var) {
        this.c = z6Var;
    }

    public final void a(Intent intent) {
        this.c.d();
        Context context = this.c.a.a;
        a a2 = a.a();
        synchronized (this) {
            if (this.a) {
                this.c.a().f2052n.a("Connection attempt already in progress");
                return;
            }
            this.c.a().f2052n.a("Using local app measurement service");
            this.a = true;
            r7 r7Var = this.c.c;
            if (a2 != null) {
                context.getClass().getName();
                a2.b(context, intent, r7Var, 129);
                return;
            }
            throw null;
        }
    }

    public final void b(Bundle bundle) {
        ResourcesFlusher.a("MeasurementServiceConnection.onConnected");
        synchronized (this) {
            try {
                this.c.i().a(new w7(this, (f3) this.b.n()));
            } catch (DeadObjectException | IllegalStateException unused) {
                this.b = null;
                this.a = false;
            }
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(6:13|(1:15)(1:16)|17|18|24|25) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:24:0x005a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onServiceConnected(android.content.ComponentName r4, android.os.IBinder r5) {
        /*
            r3 = this;
            java.lang.String r4 = "MeasurementServiceConnection.onServiceConnected"
            i.b.k.ResourcesFlusher.a(r4)
            monitor-enter(r3)
            r4 = 0
            if (r5 != 0) goto L_0x001d
            r3.a = r4     // Catch:{ all -> 0x001a }
            j.c.a.a.g.a.z6 r4 = r3.c     // Catch:{ all -> 0x001a }
            j.c.a.a.g.a.n3 r4 = r4.a()     // Catch:{ all -> 0x001a }
            j.c.a.a.g.a.p3 r4 = r4.f2046f     // Catch:{ all -> 0x001a }
            java.lang.String r5 = "Service connected with null binder"
            r4.a(r5)     // Catch:{ all -> 0x001a }
            monitor-exit(r3)     // Catch:{ all -> 0x001a }
            return
        L_0x001a:
            r4 = move-exception
            goto L_0x009d
        L_0x001d:
            r0 = 0
            java.lang.String r1 = r5.getInterfaceDescriptor()     // Catch:{ RemoteException -> 0x0059 }
            java.lang.String r2 = "com.google.android.gms.measurement.internal.IMeasurementService"
            boolean r2 = r2.equals(r1)     // Catch:{ RemoteException -> 0x0059 }
            if (r2 == 0) goto L_0x004a
            java.lang.String r1 = "com.google.android.gms.measurement.internal.IMeasurementService"
            android.os.IInterface r1 = r5.queryLocalInterface(r1)     // Catch:{ RemoteException -> 0x0059 }
            boolean r2 = r1 instanceof j.c.a.a.g.a.f3     // Catch:{ RemoteException -> 0x0059 }
            if (r2 == 0) goto L_0x0037
            j.c.a.a.g.a.f3 r1 = (j.c.a.a.g.a.f3) r1     // Catch:{ RemoteException -> 0x0059 }
            goto L_0x003c
        L_0x0037:
            j.c.a.a.g.a.h3 r1 = new j.c.a.a.g.a.h3     // Catch:{ RemoteException -> 0x0059 }
            r1.<init>(r5)     // Catch:{ RemoteException -> 0x0059 }
        L_0x003c:
            j.c.a.a.g.a.z6 r5 = r3.c     // Catch:{ RemoteException -> 0x005a }
            j.c.a.a.g.a.n3 r5 = r5.a()     // Catch:{ RemoteException -> 0x005a }
            j.c.a.a.g.a.p3 r5 = r5.f2052n     // Catch:{ RemoteException -> 0x005a }
            java.lang.String r2 = "Bound to IMeasurementService interface"
            r5.a(r2)     // Catch:{ RemoteException -> 0x005a }
            goto L_0x0067
        L_0x004a:
            j.c.a.a.g.a.z6 r5 = r3.c     // Catch:{ RemoteException -> 0x0059 }
            j.c.a.a.g.a.n3 r5 = r5.a()     // Catch:{ RemoteException -> 0x0059 }
            j.c.a.a.g.a.p3 r5 = r5.f2046f     // Catch:{ RemoteException -> 0x0059 }
            java.lang.String r2 = "Got binder with a wrong descriptor"
            r5.a(r2, r1)     // Catch:{ RemoteException -> 0x0059 }
            r1 = r0
            goto L_0x0067
        L_0x0059:
            r1 = r0
        L_0x005a:
            j.c.a.a.g.a.z6 r5 = r3.c     // Catch:{ all -> 0x001a }
            j.c.a.a.g.a.n3 r5 = r5.a()     // Catch:{ all -> 0x001a }
            j.c.a.a.g.a.p3 r5 = r5.f2046f     // Catch:{ all -> 0x001a }
            java.lang.String r2 = "Service connect failed to get IMeasurementService"
            r5.a(r2)     // Catch:{ all -> 0x001a }
        L_0x0067:
            if (r1 != 0) goto L_0x0080
            r3.a = r4     // Catch:{ all -> 0x001a }
            j.c.a.a.c.m.a r4 = j.c.a.a.c.m.a.a()     // Catch:{ IllegalArgumentException -> 0x009b }
            j.c.a.a.g.a.z6 r5 = r3.c     // Catch:{ IllegalArgumentException -> 0x009b }
            j.c.a.a.g.a.r4 r5 = r5.a     // Catch:{ IllegalArgumentException -> 0x009b }
            android.content.Context r5 = r5.a     // Catch:{ IllegalArgumentException -> 0x009b }
            j.c.a.a.g.a.z6 r1 = r3.c     // Catch:{ IllegalArgumentException -> 0x009b }
            j.c.a.a.g.a.r7 r1 = r1.c     // Catch:{ IllegalArgumentException -> 0x009b }
            if (r4 == 0) goto L_0x007f
            r5.unbindService(r1)     // Catch:{ IllegalArgumentException -> 0x009b }
            goto L_0x009b
        L_0x007f:
            throw r0     // Catch:{ IllegalArgumentException -> 0x009b }
        L_0x0080:
            j.c.a.a.g.a.z6 r4 = r3.c     // Catch:{ all -> 0x001a }
            j.c.a.a.g.a.l4 r4 = r4.i()     // Catch:{ all -> 0x001a }
            j.c.a.a.g.a.u7 r5 = new j.c.a.a.g.a.u7     // Catch:{ all -> 0x001a }
            r5.<init>(r3, r1)     // Catch:{ all -> 0x001a }
            r4.o()     // Catch:{ all -> 0x001a }
            i.b.k.ResourcesFlusher.b(r5)     // Catch:{ all -> 0x001a }
            j.c.a.a.g.a.p4 r0 = new j.c.a.a.g.a.p4     // Catch:{ all -> 0x001a }
            java.lang.String r1 = "Task exception on worker thread"
            r0.<init>(r4, r5, r1)     // Catch:{ all -> 0x001a }
            r4.a(r0)     // Catch:{ all -> 0x001a }
        L_0x009b:
            monitor-exit(r3)     // Catch:{ all -> 0x001a }
            return
        L_0x009d:
            monitor-exit(r3)     // Catch:{ all -> 0x001a }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.r7.onServiceConnected(android.content.ComponentName, android.os.IBinder):void");
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        ResourcesFlusher.a("MeasurementServiceConnection.onServiceDisconnected");
        this.c.a().f2051m.a("Service disconnected");
        l4 i2 = this.c.i();
        t7 t7Var = new t7(this, componentName);
        i2.o();
        ResourcesFlusher.b(t7Var);
        i2.a((p4<?>) new p4(i2, t7Var, "Task exception on worker thread"));
    }

    public final void a() {
        this.c.d();
        Context context = this.c.a.a;
        synchronized (this) {
            if (this.a) {
                this.c.a().f2052n.a("Connection attempt already in progress");
            } else if (this.b == null || (!this.b.a() && !this.b.c())) {
                this.b = new k3(context, Looper.getMainLooper(), this, this);
                this.c.a().f2052n.a("Connecting to remote service");
                this.a = true;
                this.b.j();
            } else {
                this.c.a().f2052n.a("Already awaiting connection attempt");
            }
        }
    }

    public final void a(int i2) {
        ResourcesFlusher.a("MeasurementServiceConnection.onConnectionSuspended");
        this.c.a().f2051m.a("Service connection suspended");
        l4 i3 = this.c.i();
        v7 v7Var = new v7(this);
        i3.o();
        ResourcesFlusher.b(v7Var);
        i3.a((p4<?>) new p4(i3, v7Var, "Task exception on worker thread"));
    }

    public final void a(j.c.a.a.c.b bVar) {
        ResourcesFlusher.a("MeasurementServiceConnection.onConnectionFailed");
        r4 r4Var = this.c.a;
        n3 n3Var = r4Var.f2087i;
        n3 n3Var2 = (n3Var == null || !n3Var.s()) ? null : r4Var.f2087i;
        if (n3Var2 != null) {
            n3Var2.f2047i.a("Service connection failed", bVar);
        }
        synchronized (this) {
            this.a = false;
            this.b = null;
        }
        l4 i2 = this.c.i();
        y7 y7Var = new y7(this);
        i2.o();
        ResourcesFlusher.b(y7Var);
        i2.a((p4<?>) new p4(i2, y7Var, "Task exception on worker thread"));
    }
}
