package j.c.a.a.f.e;

import java.util.NoSuchElementException;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class v2 implements z2 {
    public int b = 0;
    public final int c = this.d.a();
    public final /* synthetic */ w2 d;

    public v2(w2 w2Var) {
        this.d = w2Var;
    }

    public final byte a() {
        int i2 = this.b;
        if (i2 < this.c) {
            this.b = i2 + 1;
            return this.d.b(i2);
        }
        throw new NoSuchElementException();
    }

    public final boolean hasNext() {
        return this.b < this.c;
    }

    public Object next() {
        return Byte.valueOf(a());
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
