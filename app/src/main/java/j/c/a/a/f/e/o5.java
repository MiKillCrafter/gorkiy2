package j.c.a.a.f.e;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class o5 {
    public static final o5 c = new o5();
    public final v5 a = new t4();
    public final ConcurrentMap<Class<?>, t5<?>> b = new ConcurrentHashMap();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.f.e.y3.a(java.lang.Object, java.lang.String):T
     arg types: [java.lang.Class, java.lang.String]
     candidates:
      j.c.a.a.f.e.y3.a(java.lang.Object, java.lang.Object):java.lang.Object
      j.c.a.a.f.e.y3.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.f.e.y3.a(java.lang.Object, java.lang.String):T
     arg types: [j.c.a.a.f.e.t5<T>, java.lang.String]
     candidates:
      j.c.a.a.f.e.y3.a(java.lang.Object, java.lang.Object):java.lang.Object
      j.c.a.a.f.e.y3.a(java.lang.Object, java.lang.String):T */
    public final <T> t5<T> a(Class cls) {
        t5<T> t5Var;
        j5 j5Var;
        y3.a((Object) cls, "messageType");
        t5<T> t5Var2 = this.b.get(cls);
        if (t5Var2 != null) {
            return t5Var2;
        }
        t4 t4Var = (t4) this.a;
        if (t4Var != null) {
            Class<x3> cls2 = x3.class;
            u5.a(cls);
            d5 a2 = t4Var.a.a(cls);
            if (a2.b()) {
                if (cls2.isAssignableFrom(cls)) {
                    j5Var = new j5(u5.d, n3.a, a2.c());
                } else {
                    g6<?, ?> g6Var = u5.b;
                    l3<?> l3Var = n3.b;
                    if (l3Var != null) {
                        j5Var = new j5(g6Var, l3Var, a2.c());
                    } else {
                        throw new IllegalStateException("Protobuf runtime is not correctly loaded.");
                    }
                }
                t5Var = j5Var;
            } else {
                boolean isAssignableFrom = cls2.isAssignableFrom(cls);
                boolean z = false;
                if (isAssignableFrom) {
                    if (a2.a() == 1) {
                        z = true;
                    }
                    if (z) {
                        t5Var = h5.a(cls, a2, n5.b, p4.b, u5.d, n3.a, a5.b);
                    } else {
                        t5Var = h5.a(cls, a2, n5.b, p4.b, u5.d, (l3<?>) null, a5.b);
                    }
                } else {
                    if (a2.a() == 1) {
                        z = true;
                    }
                    if (z) {
                        l5 l5Var = n5.a;
                        p4 p4Var = p4.a;
                        g6<?, ?> g6Var2 = u5.b;
                        l3<?> l3Var2 = n3.b;
                        if (l3Var2 != null) {
                            t5Var = h5.a(cls, a2, l5Var, p4Var, g6Var2, l3Var2, a5.a);
                        } else {
                            throw new IllegalStateException("Protobuf runtime is not correctly loaded.");
                        }
                    } else {
                        t5Var = h5.a(cls, a2, n5.a, p4.a, u5.c, (l3<?>) null, a5.a);
                    }
                }
            }
            y3.a((Object) cls, "messageType");
            y3.a((Object) t5Var, "schema");
            t5<T> putIfAbsent = this.b.putIfAbsent(cls, t5Var);
            if (putIfAbsent != null) {
                return putIfAbsent;
            }
            return t5Var;
        }
        throw null;
    }

    public final <T> t5<T> a(Object obj) {
        return a((Class) obj.getClass());
    }
}
