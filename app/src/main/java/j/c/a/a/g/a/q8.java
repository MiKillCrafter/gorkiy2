package j.c.a.a.g.a;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.crashlytics.android.core.CrashlyticsController;
import i.b.k.ResourcesFlusher;
import i.e.ArrayMap;
import j.c.a.a.c.n.b;
import j.c.a.a.f.e.k0;
import j.c.a.a.f.e.nb;
import j.c.a.a.f.e.q0;
import j.c.a.a.f.e.s0;
import j.c.a.a.f.e.u0;
import j.c.a.a.f.e.x3;
import j.c.a.a.f.e.y0;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import l.a.a.a.o.b.AbstractSpiCall;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public class q8 implements p5 {
    public static volatile q8 y;
    public m4 a;
    public r3 b;
    public n9 c;
    public u3 d;

    /* renamed from: e  reason: collision with root package name */
    public n8 f2071e;

    /* renamed from: f  reason: collision with root package name */
    public f9 f2072f;
    public final u8 g;
    public u6 h;

    /* renamed from: i  reason: collision with root package name */
    public final r4 f2073i;

    /* renamed from: j  reason: collision with root package name */
    public boolean f2074j = false;

    /* renamed from: k  reason: collision with root package name */
    public boolean f2075k;

    /* renamed from: l  reason: collision with root package name */
    public boolean f2076l;

    /* renamed from: m  reason: collision with root package name */
    public long f2077m;

    /* renamed from: n  reason: collision with root package name */
    public List<Runnable> f2078n;

    /* renamed from: o  reason: collision with root package name */
    public int f2079o;

    /* renamed from: p  reason: collision with root package name */
    public int f2080p;

    /* renamed from: q  reason: collision with root package name */
    public boolean f2081q;

    /* renamed from: r  reason: collision with root package name */
    public boolean f2082r;

    /* renamed from: s  reason: collision with root package name */
    public boolean f2083s;

    /* renamed from: t  reason: collision with root package name */
    public FileLock f2084t;
    public FileChannel u;
    public List<Long> v;
    public List<Long> w;
    public long x;

    public q8(v8 v8Var) {
        ResourcesFlusher.b(v8Var);
        this.f2073i = r4.a(v8Var.a, (nb) null);
        this.x = -1;
        u8 u8Var = new u8(this);
        u8Var.p();
        this.g = u8Var;
        r3 r3Var = new r3(this);
        r3Var.p();
        this.b = r3Var;
        m4 m4Var = new m4(this);
        m4Var.p();
        this.a = m4Var;
        l4 i2 = this.f2073i.i();
        p8 p8Var = new p8(this, v8Var);
        i2.o();
        ResourcesFlusher.b(p8Var);
        i2.a((p4<?>) new p4(i2, p8Var, "Task exception on worker thread"));
    }

    public static q8 a(Context context) {
        ResourcesFlusher.b(context);
        ResourcesFlusher.b(context.getApplicationContext());
        if (y == null) {
            synchronized (q8.class) {
                if (y == null) {
                    y = new q8(new v8(context));
                }
            }
        }
        return y;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.g.a.y8.a(android.os.Bundle, java.lang.String, java.lang.Object):void
     arg types: [android.os.Bundle, java.lang.String, long]
     candidates:
      j.c.a.a.g.a.y8.a(int, java.lang.Object, boolean):java.lang.Object
      j.c.a.a.g.a.y8.a(java.lang.String, int, boolean):java.lang.String
      j.c.a.a.g.a.y8.a(java.lang.String, int, java.lang.String):boolean
      j.c.a.a.g.a.y8.a(java.lang.String, java.lang.String[], java.lang.String):boolean
      j.c.a.a.g.a.y8.a(android.os.Bundle, java.lang.String, java.lang.Object):void */
    /* JADX WARNING: Code restructure failed: missing block: B:247:0x0820, code lost:
        if (r7.f2044e < ((long) r1.f2073i.k().a(r4.a))) goto L_0x0822;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:248:0x0822, code lost:
        r5 = true;
     */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x026e A[Catch:{ SQLiteException -> 0x0235, all -> 0x08a0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x02a5 A[Catch:{ SQLiteException -> 0x0235, all -> 0x08a0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x02f3 A[Catch:{ SQLiteException -> 0x0235, all -> 0x08a0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x0320  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void b(j.c.a.a.g.a.i r35, j.c.a.a.g.a.d9 r36) {
        /*
            r34 = this;
            r1 = r34
            r2 = r35
            r3 = r36
            java.lang.String r4 = "_s"
            i.b.k.ResourcesFlusher.b(r36)
            java.lang.String r5 = r3.b
            i.b.k.ResourcesFlusher.b(r5)
            long r5 = java.lang.System.nanoTime()
            r34.r()
            r34.m()
            java.lang.String r15 = r3.b
            j.c.a.a.g.a.u8 r7 = r34.k()
            boolean r7 = r7.a(r2, r3)
            if (r7 != 0) goto L_0x0027
            return
        L_0x0027:
            boolean r7 = r3.f1953i
            if (r7 != 0) goto L_0x002f
            r1.b(r3)
            return
        L_0x002f:
            j.c.a.a.g.a.m4 r7 = r34.c()
            java.lang.String r8 = r2.b
            boolean r7 = r7.b(r15, r8)
            java.lang.String r14 = "_err"
            r17 = 1
            r13 = 0
            if (r7 == 0) goto L_0x00dd
            j.c.a.a.g.a.r4 r3 = r1.f2073i
            j.c.a.a.g.a.n3 r3 = r3.a()
            j.c.a.a.g.a.p3 r3 = r3.v()
            java.lang.Object r4 = j.c.a.a.g.a.n3.a(r15)
            j.c.a.a.g.a.r4 r5 = r1.f2073i
            j.c.a.a.g.a.l3 r5 = r5.o()
            java.lang.String r6 = r2.b
            java.lang.String r5 = r5.a(r6)
            java.lang.String r6 = "Dropping blacklisted event. appId"
            r3.a(r6, r4, r5)
            j.c.a.a.g.a.m4 r3 = r34.c()
            boolean r3 = r3.d(r15)
            if (r3 != 0) goto L_0x0076
            j.c.a.a.g.a.m4 r3 = r34.c()
            boolean r3 = r3.e(r15)
            if (r3 == 0) goto L_0x0074
            goto L_0x0076
        L_0x0074:
            r17 = 0
        L_0x0076:
            if (r17 != 0) goto L_0x0091
            java.lang.String r3 = r2.b
            boolean r3 = r14.equals(r3)
            if (r3 != 0) goto L_0x0091
            j.c.a.a.g.a.r4 r3 = r1.f2073i
            j.c.a.a.g.a.y8 r7 = r3.n()
            r9 = 11
            java.lang.String r11 = r2.b
            r12 = 0
            java.lang.String r10 = "_ev"
            r8 = r15
            r7.a(r8, r9, r10, r11, r12)
        L_0x0091:
            if (r17 == 0) goto L_0x00dc
            j.c.a.a.g.a.n9 r2 = r34.e()
            j.c.a.a.g.a.e4 r2 = r2.b(r15)
            if (r2 == 0) goto L_0x00dc
            long r3 = r2.w()
            long r5 = r2.v()
            long r3 = java.lang.Math.max(r3, r5)
            j.c.a.a.g.a.r4 r5 = r1.f2073i
            j.c.a.a.c.n.a r5 = r5.j()
            j.c.a.a.c.n.b r5 = (j.c.a.a.c.n.b) r5
            long r5 = r5.a()
            long r5 = r5 - r3
            long r3 = java.lang.Math.abs(r5)
            j.c.a.a.g.a.b3<java.lang.Long> r5 = j.c.a.a.g.a.k.E
            java.lang.Object r5 = r5.a(r13)
            java.lang.Long r5 = (java.lang.Long) r5
            long r5 = r5.longValue()
            int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r7 <= 0) goto L_0x00dc
            j.c.a.a.g.a.r4 r3 = r1.f2073i
            j.c.a.a.g.a.n3 r3 = r3.a()
            j.c.a.a.g.a.p3 r3 = r3.x()
            java.lang.String r4 = "Fetching config for blacklisted app"
            r3.a(r4)
            r1.a(r2)
        L_0x00dc:
            return
        L_0x00dd:
            j.c.a.a.g.a.r4 r7 = r1.f2073i
            j.c.a.a.g.a.n3 r7 = r7.a()
            r12 = 2
            boolean r7 = r7.a(r12)
            if (r7 == 0) goto L_0x0103
            j.c.a.a.g.a.r4 r7 = r1.f2073i
            j.c.a.a.g.a.n3 r7 = r7.a()
            j.c.a.a.g.a.p3 r7 = r7.y()
            j.c.a.a.g.a.r4 r8 = r1.f2073i
            j.c.a.a.g.a.l3 r8 = r8.o()
            java.lang.String r8 = r8.a(r2)
            java.lang.String r9 = "Logging event"
            r7.a(r9, r8)
        L_0x0103:
            j.c.a.a.g.a.n9 r7 = r34.e()
            r7.z()
            r1.b(r3)     // Catch:{ all -> 0x08a0 }
            java.lang.String r7 = "_iap"
            java.lang.String r8 = r2.b     // Catch:{ all -> 0x08a0 }
            boolean r7 = r7.equals(r8)     // Catch:{ all -> 0x08a0 }
            java.lang.String r8 = "ecommerce_purchase"
            if (r7 != 0) goto L_0x0127
            java.lang.String r7 = r2.b     // Catch:{ all -> 0x08a0 }
            boolean r7 = r8.equals(r7)     // Catch:{ all -> 0x08a0 }
            if (r7 == 0) goto L_0x0122
            goto L_0x0127
        L_0x0122:
            r23 = r5
            r5 = 0
            goto L_0x02b4
        L_0x0127:
            j.c.a.a.g.a.h r7 = r2.c     // Catch:{ all -> 0x08a0 }
            java.lang.String r9 = "currency"
            java.lang.String r7 = r7.c(r9)     // Catch:{ all -> 0x08a0 }
            java.lang.String r9 = r2.b     // Catch:{ all -> 0x08a0 }
            boolean r8 = r8.equals(r9)     // Catch:{ all -> 0x08a0 }
            java.lang.String r9 = "value"
            if (r8 == 0) goto L_0x018b
            j.c.a.a.g.a.h r8 = r2.c     // Catch:{ all -> 0x08a0 }
            java.lang.Double r8 = r8.b(r9)     // Catch:{ all -> 0x08a0 }
            double r18 = r8.doubleValue()     // Catch:{ all -> 0x08a0 }
            r20 = 4696837146684686336(0x412e848000000000, double:1000000.0)
            double r18 = r18 * r20
            r22 = 0
            int r8 = (r18 > r22 ? 1 : (r18 == r22 ? 0 : -1))
            if (r8 != 0) goto L_0x015d
            j.c.a.a.g.a.h r8 = r2.c     // Catch:{ all -> 0x08a0 }
            java.lang.Long r8 = r8.a(r9)     // Catch:{ all -> 0x08a0 }
            long r8 = r8.longValue()     // Catch:{ all -> 0x08a0 }
            double r8 = (double) r8     // Catch:{ all -> 0x08a0 }
            double r18 = r8 * r20
        L_0x015d:
            r8 = 4890909195324358656(0x43e0000000000000, double:9.223372036854776E18)
            int r10 = (r18 > r8 ? 1 : (r18 == r8 ? 0 : -1))
            if (r10 > 0) goto L_0x016e
            r8 = -4332462841530417152(0xc3e0000000000000, double:-9.223372036854776E18)
            int r10 = (r18 > r8 ? 1 : (r18 == r8 ? 0 : -1))
            if (r10 < 0) goto L_0x016e
            long r8 = java.lang.Math.round(r18)     // Catch:{ all -> 0x08a0 }
            goto L_0x0195
        L_0x016e:
            j.c.a.a.g.a.r4 r7 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.n3 r7 = r7.a()     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.p3 r7 = r7.v()     // Catch:{ all -> 0x08a0 }
            java.lang.String r8 = "Data lost. Currency value is too big. appId"
            java.lang.Object r9 = j.c.a.a.g.a.n3.a(r15)     // Catch:{ all -> 0x08a0 }
            java.lang.Double r10 = java.lang.Double.valueOf(r18)     // Catch:{ all -> 0x08a0 }
            r7.a(r8, r9, r10)     // Catch:{ all -> 0x08a0 }
            r23 = r5
            r5 = 0
            r11 = 0
            goto L_0x02a3
        L_0x018b:
            j.c.a.a.g.a.h r8 = r2.c     // Catch:{ all -> 0x08a0 }
            java.lang.Long r8 = r8.a(r9)     // Catch:{ all -> 0x08a0 }
            long r8 = r8.longValue()     // Catch:{ all -> 0x08a0 }
        L_0x0195:
            boolean r10 = android.text.TextUtils.isEmpty(r7)     // Catch:{ all -> 0x08a0 }
            if (r10 != 0) goto L_0x029f
            java.util.Locale r10 = java.util.Locale.US     // Catch:{ all -> 0x08a0 }
            java.lang.String r7 = r7.toUpperCase(r10)     // Catch:{ all -> 0x08a0 }
            java.lang.String r10 = "[A-Z]{3}"
            boolean r10 = r7.matches(r10)     // Catch:{ all -> 0x08a0 }
            if (r10 == 0) goto L_0x029f
            java.lang.String r10 = "_ltv_"
            int r16 = r7.length()     // Catch:{ all -> 0x08a0 }
            if (r16 == 0) goto L_0x01b6
            java.lang.String r7 = r10.concat(r7)     // Catch:{ all -> 0x08a0 }
            goto L_0x01bb
        L_0x01b6:
            java.lang.String r7 = new java.lang.String     // Catch:{ all -> 0x08a0 }
            r7.<init>(r10)     // Catch:{ all -> 0x08a0 }
        L_0x01bb:
            r10 = r7
            j.c.a.a.g.a.n9 r7 = r34.e()     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.z8 r7 = r7.c(r15, r10)     // Catch:{ all -> 0x08a0 }
            if (r7 == 0) goto L_0x01fd
            java.lang.Object r11 = r7.f2143e     // Catch:{ all -> 0x08a0 }
            boolean r11 = r11 instanceof java.lang.Long     // Catch:{ all -> 0x08a0 }
            if (r11 != 0) goto L_0x01cd
            goto L_0x01fd
        L_0x01cd:
            java.lang.Object r7 = r7.f2143e     // Catch:{ all -> 0x08a0 }
            java.lang.Long r7 = (java.lang.Long) r7     // Catch:{ all -> 0x08a0 }
            long r18 = r7.longValue()     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.z8 r20 = new j.c.a.a.g.a.z8     // Catch:{ all -> 0x08a0 }
            java.lang.String r11 = r2.d     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.r4 r7 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.c.n.a r7 = r7.j()     // Catch:{ all -> 0x08a0 }
            j.c.a.a.c.n.b r7 = (j.c.a.a.c.n.b) r7
            long r21 = r7.a()     // Catch:{ all -> 0x08a0 }
            long r18 = r18 + r8
            java.lang.Long r18 = java.lang.Long.valueOf(r18)     // Catch:{ all -> 0x08a0 }
            r7 = r20
            r8 = r15
            r9 = r11
            r23 = r5
            r5 = 0
            r6 = 2
            r11 = r21
            r13 = r18
            r7.<init>(r8, r9, r10, r11, r13)     // Catch:{ all -> 0x08a0 }
        L_0x01fa:
            r6 = r20
            goto L_0x0264
        L_0x01fd:
            r23 = r5
            r5 = 0
            r6 = 2
            j.c.a.a.g.a.n9 r7 = r34.e()     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.r4 r11 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.i9 r11 = r11.k()     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.b3<java.lang.Integer> r12 = j.c.a.a.g.a.k.J     // Catch:{ all -> 0x08a0 }
            int r11 = r11.b(r15, r12)     // Catch:{ all -> 0x08a0 }
            int r11 = r11 + -1
            i.b.k.ResourcesFlusher.b(r15)     // Catch:{ all -> 0x08a0 }
            r7.d()     // Catch:{ all -> 0x08a0 }
            r7.o()     // Catch:{ all -> 0x08a0 }
            android.database.sqlite.SQLiteDatabase r12 = r7.v()     // Catch:{ SQLiteException -> 0x0235 }
            java.lang.String r13 = "delete from user_attributes where app_id=? and name in (select name from user_attributes where app_id=? and name like '_ltv_%' order by set_timestamp desc limit ?,10);"
            r6 = 3
            java.lang.String[] r6 = new java.lang.String[r6]     // Catch:{ SQLiteException -> 0x0235 }
            r6[r5] = r15     // Catch:{ SQLiteException -> 0x0235 }
            r6[r17] = r15     // Catch:{ SQLiteException -> 0x0235 }
            java.lang.String r11 = java.lang.String.valueOf(r11)     // Catch:{ SQLiteException -> 0x0235 }
            r16 = 2
            r6[r16] = r11     // Catch:{ SQLiteException -> 0x0235 }
            r12.execSQL(r13, r6)     // Catch:{ SQLiteException -> 0x0235 }
            goto L_0x0248
        L_0x0235:
            r0 = move-exception
            r6 = r0
            j.c.a.a.g.a.n3 r7 = r7.a()     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.p3 r7 = r7.u()     // Catch:{ all -> 0x08a0 }
            java.lang.String r11 = "Error pruning currencies. appId"
            java.lang.Object r12 = j.c.a.a.g.a.n3.a(r15)     // Catch:{ all -> 0x08a0 }
            r7.a(r11, r12, r6)     // Catch:{ all -> 0x08a0 }
        L_0x0248:
            j.c.a.a.g.a.z8 r20 = new j.c.a.a.g.a.z8     // Catch:{ all -> 0x08a0 }
            java.lang.String r6 = r2.d     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.r4 r7 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.c.n.a r7 = r7.j()     // Catch:{ all -> 0x08a0 }
            j.c.a.a.c.n.b r7 = (j.c.a.a.c.n.b) r7
            long r11 = r7.a()     // Catch:{ all -> 0x08a0 }
            java.lang.Long r13 = java.lang.Long.valueOf(r8)     // Catch:{ all -> 0x08a0 }
            r7 = r20
            r8 = r15
            r9 = r6
            r7.<init>(r8, r9, r10, r11, r13)     // Catch:{ all -> 0x08a0 }
            goto L_0x01fa
        L_0x0264:
            j.c.a.a.g.a.n9 r7 = r34.e()     // Catch:{ all -> 0x08a0 }
            boolean r7 = r7.a(r6)     // Catch:{ all -> 0x08a0 }
            if (r7 != 0) goto L_0x02a2
            j.c.a.a.g.a.r4 r7 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.n3 r7 = r7.a()     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.p3 r7 = r7.u()     // Catch:{ all -> 0x08a0 }
            java.lang.String r8 = "Too many unique user properties are set. Ignoring user property. appId"
            java.lang.Object r9 = j.c.a.a.g.a.n3.a(r15)     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.r4 r10 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.l3 r10 = r10.o()     // Catch:{ all -> 0x08a0 }
            java.lang.String r11 = r6.c     // Catch:{ all -> 0x08a0 }
            java.lang.String r10 = r10.c(r11)     // Catch:{ all -> 0x08a0 }
            java.lang.Object r6 = r6.f2143e     // Catch:{ all -> 0x08a0 }
            r7.a(r8, r9, r10, r6)     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.r4 r6 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.y8 r7 = r6.n()     // Catch:{ all -> 0x08a0 }
            r9 = 9
            r10 = 0
            r11 = 0
            r12 = 0
            r8 = r15
            r7.a(r8, r9, r10, r11, r12)     // Catch:{ all -> 0x08a0 }
            goto L_0x02a2
        L_0x029f:
            r23 = r5
            r5 = 0
        L_0x02a2:
            r11 = 1
        L_0x02a3:
            if (r11 != 0) goto L_0x02b4
            j.c.a.a.g.a.n9 r2 = r34.e()     // Catch:{ all -> 0x08a0 }
            r2.u()     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.n9 r2 = r34.e()
            r2.A()
            return
        L_0x02b4:
            java.lang.String r6 = r2.b     // Catch:{ all -> 0x08a0 }
            boolean r6 = j.c.a.a.g.a.y8.e(r6)     // Catch:{ all -> 0x08a0 }
            java.lang.String r7 = r2.b     // Catch:{ all -> 0x08a0 }
            boolean r16 = r14.equals(r7)     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.n9 r7 = r34.e()     // Catch:{ all -> 0x08a0 }
            long r8 = r34.s()     // Catch:{ all -> 0x08a0 }
            r11 = 1
            r13 = 0
            r19 = 0
            r10 = r15
            r12 = r6
            r14 = r16
            r20 = r15
            r15 = r19
            j.c.a.a.g.a.m9 r7 = r7.a(r8, r10, r11, r12, r13, r14, r15)     // Catch:{ all -> 0x08a0 }
            long r8 = r7.b     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.b3<java.lang.Integer> r10 = j.c.a.a.g.a.k.f2018p     // Catch:{ all -> 0x08a0 }
            r14 = 0
            java.lang.Object r10 = r10.a(r14)     // Catch:{ all -> 0x08a0 }
            java.lang.Integer r10 = (java.lang.Integer) r10     // Catch:{ all -> 0x08a0 }
            int r10 = r10.intValue()     // Catch:{ all -> 0x08a0 }
            long r10 = (long) r10     // Catch:{ all -> 0x08a0 }
            long r8 = r8 - r10
            r10 = 1000(0x3e8, double:4.94E-321)
            r12 = 0
            r21 = 1
            int r15 = (r8 > r12 ? 1 : (r8 == r12 ? 0 : -1))
            if (r15 <= 0) goto L_0x0320
            long r8 = r8 % r10
            int r2 = (r8 > r21 ? 1 : (r8 == r21 ? 0 : -1))
            if (r2 != 0) goto L_0x0311
            j.c.a.a.g.a.r4 r2 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.n3 r2 = r2.a()     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.p3 r2 = r2.u()     // Catch:{ all -> 0x08a0 }
            java.lang.String r3 = "Data loss. Too many events logged. appId, count"
            java.lang.Object r4 = j.c.a.a.g.a.n3.a(r20)     // Catch:{ all -> 0x08a0 }
            long r5 = r7.b     // Catch:{ all -> 0x08a0 }
            java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ all -> 0x08a0 }
            r2.a(r3, r4, r5)     // Catch:{ all -> 0x08a0 }
        L_0x0311:
            j.c.a.a.g.a.n9 r2 = r34.e()     // Catch:{ all -> 0x08a0 }
            r2.u()     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.n9 r2 = r34.e()
            r2.A()
            return
        L_0x0320:
            if (r6 == 0) goto L_0x0375
            long r8 = r7.a     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.b3<java.lang.Integer> r15 = j.c.a.a.g.a.k.f2020r     // Catch:{ all -> 0x08a0 }
            java.lang.Object r15 = r15.a(r14)     // Catch:{ all -> 0x08a0 }
            java.lang.Integer r15 = (java.lang.Integer) r15     // Catch:{ all -> 0x08a0 }
            int r15 = r15.intValue()     // Catch:{ all -> 0x08a0 }
            long r14 = (long) r15     // Catch:{ all -> 0x08a0 }
            long r8 = r8 - r14
            int r14 = (r8 > r12 ? 1 : (r8 == r12 ? 0 : -1))
            if (r14 <= 0) goto L_0x0375
            long r8 = r8 % r10
            int r3 = (r8 > r21 ? 1 : (r8 == r21 ? 0 : -1))
            if (r3 != 0) goto L_0x0354
            j.c.a.a.g.a.r4 r3 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.n3 r3 = r3.a()     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.p3 r3 = r3.u()     // Catch:{ all -> 0x08a0 }
            java.lang.String r4 = "Data loss. Too many public events logged. appId, count"
            java.lang.Object r5 = j.c.a.a.g.a.n3.a(r20)     // Catch:{ all -> 0x08a0 }
            long r6 = r7.a     // Catch:{ all -> 0x08a0 }
            java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x08a0 }
            r3.a(r4, r5, r6)     // Catch:{ all -> 0x08a0 }
        L_0x0354:
            j.c.a.a.g.a.r4 r3 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.y8 r7 = r3.n()     // Catch:{ all -> 0x08a0 }
            r9 = 16
            java.lang.String r10 = "_ev"
            java.lang.String r11 = r2.b     // Catch:{ all -> 0x08a0 }
            r12 = 0
            r8 = r20
            r7.a(r8, r9, r10, r11, r12)     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.n9 r2 = r34.e()     // Catch:{ all -> 0x08a0 }
            r2.u()     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.n9 r2 = r34.e()
            r2.A()
            return
        L_0x0375:
            if (r16 == 0) goto L_0x03c4
            long r8 = r7.d     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.r4 r10 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.i9 r10 = r10.k()     // Catch:{ all -> 0x08a0 }
            java.lang.String r11 = r3.b     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.b3<java.lang.Integer> r14 = j.c.a.a.g.a.k.f2019q     // Catch:{ all -> 0x08a0 }
            int r10 = r10.b(r11, r14)     // Catch:{ all -> 0x08a0 }
            r11 = 1000000(0xf4240, float:1.401298E-39)
            int r10 = java.lang.Math.min(r11, r10)     // Catch:{ all -> 0x08a0 }
            int r10 = java.lang.Math.max(r5, r10)     // Catch:{ all -> 0x08a0 }
            long r10 = (long) r10     // Catch:{ all -> 0x08a0 }
            long r8 = r8 - r10
            int r10 = (r8 > r12 ? 1 : (r8 == r12 ? 0 : -1))
            if (r10 <= 0) goto L_0x03c4
            int r2 = (r8 > r21 ? 1 : (r8 == r21 ? 0 : -1))
            if (r2 != 0) goto L_0x03b5
            j.c.a.a.g.a.r4 r2 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.n3 r2 = r2.a()     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.p3 r2 = r2.u()     // Catch:{ all -> 0x08a0 }
            java.lang.String r3 = "Too many error events logged. appId, count"
            java.lang.Object r4 = j.c.a.a.g.a.n3.a(r20)     // Catch:{ all -> 0x08a0 }
            long r5 = r7.d     // Catch:{ all -> 0x08a0 }
            java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ all -> 0x08a0 }
            r2.a(r3, r4, r5)     // Catch:{ all -> 0x08a0 }
        L_0x03b5:
            j.c.a.a.g.a.n9 r2 = r34.e()     // Catch:{ all -> 0x08a0 }
            r2.u()     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.n9 r2 = r34.e()
            r2.A()
            return
        L_0x03c4:
            j.c.a.a.g.a.h r7 = r2.c     // Catch:{ all -> 0x08a0 }
            android.os.Bundle r14 = r7.b()     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.r4 r7 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.y8 r7 = r7.n()     // Catch:{ all -> 0x08a0 }
            java.lang.String r8 = "_o"
            java.lang.String r9 = r2.d     // Catch:{ all -> 0x08a0 }
            r7.a(r14, r8, r9)     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.r4 r7 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.y8 r7 = r7.n()     // Catch:{ all -> 0x08a0 }
            r15 = r20
            boolean r7 = r7.d(r15)     // Catch:{ all -> 0x08a0 }
            java.lang.String r11 = "_r"
            if (r7 == 0) goto L_0x0403
            j.c.a.a.g.a.r4 r7 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.y8 r7 = r7.n()     // Catch:{ all -> 0x08a0 }
            java.lang.String r8 = "_dbg"
            java.lang.Long r9 = java.lang.Long.valueOf(r21)     // Catch:{ all -> 0x08a0 }
            r7.a(r14, r8, r9)     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.r4 r7 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.y8 r7 = r7.n()     // Catch:{ all -> 0x08a0 }
            java.lang.Long r8 = java.lang.Long.valueOf(r21)     // Catch:{ all -> 0x08a0 }
            r7.a(r14, r11, r8)     // Catch:{ all -> 0x08a0 }
        L_0x0403:
            java.lang.String r7 = r2.b     // Catch:{ all -> 0x08a0 }
            boolean r7 = r4.equals(r7)     // Catch:{ all -> 0x08a0 }
            java.lang.String r8 = "_sno"
            if (r7 == 0) goto L_0x0438
            j.c.a.a.g.a.r4 r7 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.i9 r7 = r7.k()     // Catch:{ all -> 0x08a0 }
            java.lang.String r9 = r3.b     // Catch:{ all -> 0x08a0 }
            boolean r7 = r7.g(r9)     // Catch:{ all -> 0x08a0 }
            if (r7 == 0) goto L_0x0438
            j.c.a.a.g.a.n9 r7 = r34.e()     // Catch:{ all -> 0x08a0 }
            java.lang.String r9 = r3.b     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.z8 r7 = r7.c(r9, r8)     // Catch:{ all -> 0x08a0 }
            if (r7 == 0) goto L_0x0438
            java.lang.Object r9 = r7.f2143e     // Catch:{ all -> 0x08a0 }
            boolean r9 = r9 instanceof java.lang.Long     // Catch:{ all -> 0x08a0 }
            if (r9 == 0) goto L_0x0438
            j.c.a.a.g.a.r4 r9 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.y8 r9 = r9.n()     // Catch:{ all -> 0x08a0 }
            java.lang.Object r7 = r7.f2143e     // Catch:{ all -> 0x08a0 }
            r9.a(r14, r8, r7)     // Catch:{ all -> 0x08a0 }
        L_0x0438:
            java.lang.String r7 = r2.b     // Catch:{ all -> 0x08a0 }
            boolean r4 = r4.equals(r7)     // Catch:{ all -> 0x08a0 }
            if (r4 == 0) goto L_0x0466
            j.c.a.a.g.a.r4 r4 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.i9 r4 = r4.k()     // Catch:{ all -> 0x08a0 }
            java.lang.String r7 = r3.b     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.b3<java.lang.Boolean> r9 = j.c.a.a.g.a.k.e0     // Catch:{ all -> 0x08a0 }
            boolean r4 = r4.e(r7, r9)     // Catch:{ all -> 0x08a0 }
            if (r4 == 0) goto L_0x0466
            j.c.a.a.g.a.r4 r4 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.i9 r4 = r4.k()     // Catch:{ all -> 0x08a0 }
            java.lang.String r7 = r3.b     // Catch:{ all -> 0x08a0 }
            boolean r4 = r4.g(r7)     // Catch:{ all -> 0x08a0 }
            if (r4 != 0) goto L_0x0466
            j.c.a.a.g.a.x8 r4 = new j.c.a.a.g.a.x8     // Catch:{ all -> 0x08a0 }
            r4.<init>(r8)     // Catch:{ all -> 0x08a0 }
            r1.b(r4, r3)     // Catch:{ all -> 0x08a0 }
        L_0x0466:
            j.c.a.a.g.a.n9 r4 = r34.e()     // Catch:{ all -> 0x08a0 }
            long r7 = r4.c(r15)     // Catch:{ all -> 0x08a0 }
            int r4 = (r7 > r12 ? 1 : (r7 == r12 ? 0 : -1))
            if (r4 <= 0) goto L_0x0489
            j.c.a.a.g.a.r4 r4 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.n3 r4 = r4.a()     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.p3 r4 = r4.v()     // Catch:{ all -> 0x08a0 }
            java.lang.String r9 = "Data lost. Too many events stored on disk, deleted. appId"
            java.lang.Object r10 = j.c.a.a.g.a.n3.a(r15)     // Catch:{ all -> 0x08a0 }
            java.lang.Long r7 = java.lang.Long.valueOf(r7)     // Catch:{ all -> 0x08a0 }
            r4.a(r9, r10, r7)     // Catch:{ all -> 0x08a0 }
        L_0x0489:
            j.c.a.a.g.a.f r4 = new j.c.a.a.g.a.f     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.r4 r8 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            java.lang.String r9 = r2.d     // Catch:{ all -> 0x08a0 }
            java.lang.String r10 = r2.b     // Catch:{ all -> 0x08a0 }
            long r12 = r2.f2007e     // Catch:{ all -> 0x08a0 }
            r21 = 0
            r7 = r4
            r2 = r10
            r10 = r15
            r5 = r11
            r11 = r2
            r16 = r14
            r2 = r15
            r19 = 0
            r14 = r21
            r7.<init>(r8, r9, r10, r11, r12, r14, r16)     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.n9 r7 = r34.e()     // Catch:{ all -> 0x08a0 }
            java.lang.String r8 = r4.b     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.e r7 = r7.a(r2, r8)     // Catch:{ all -> 0x08a0 }
            if (r7 != 0) goto L_0x050c
            j.c.a.a.g.a.n9 r7 = r34.e()     // Catch:{ all -> 0x08a0 }
            long r7 = r7.f(r2)     // Catch:{ all -> 0x08a0 }
            r9 = 500(0x1f4, double:2.47E-321)
            int r11 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r11 < 0) goto L_0x04fc
            if (r6 == 0) goto L_0x04fc
            j.c.a.a.g.a.r4 r3 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.n3 r3 = r3.a()     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.p3 r3 = r3.u()     // Catch:{ all -> 0x08a0 }
            java.lang.String r5 = "Too many event names used, ignoring event. appId, name, supported count"
            java.lang.Object r6 = j.c.a.a.g.a.n3.a(r2)     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.r4 r7 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.l3 r7 = r7.o()     // Catch:{ all -> 0x08a0 }
            java.lang.String r4 = r4.b     // Catch:{ all -> 0x08a0 }
            java.lang.String r4 = r7.a(r4)     // Catch:{ all -> 0x08a0 }
            r7 = 500(0x1f4, float:7.0E-43)
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ all -> 0x08a0 }
            r3.a(r5, r6, r4, r7)     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.r4 r3 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.y8 r7 = r3.n()     // Catch:{ all -> 0x08a0 }
            r9 = 8
            r10 = 0
            r11 = 0
            r12 = 0
            r8 = r2
            r7.a(r8, r9, r10, r11, r12)     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.n9 r2 = r34.e()
            r2.A()
            return
        L_0x04fc:
            j.c.a.a.g.a.e r6 = new j.c.a.a.g.a.e     // Catch:{ all -> 0x08a0 }
            java.lang.String r9 = r4.b     // Catch:{ all -> 0x08a0 }
            r10 = 0
            r12 = 0
            long r14 = r4.d     // Catch:{ all -> 0x08a0 }
            r7 = r6
            r8 = r2
            r7.<init>(r8, r9, r10, r12, r14)     // Catch:{ all -> 0x08a0 }
            goto L_0x051a
        L_0x050c:
            j.c.a.a.g.a.r4 r2 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            long r8 = r7.f1966f     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.f r4 = r4.a(r2, r8)     // Catch:{ all -> 0x08a0 }
            long r8 = r4.d     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.e r6 = r7.a(r8)     // Catch:{ all -> 0x08a0 }
        L_0x051a:
            j.c.a.a.g.a.n9 r2 = r34.e()     // Catch:{ all -> 0x08a0 }
            r2.a(r6)     // Catch:{ all -> 0x08a0 }
            r34.r()     // Catch:{ all -> 0x08a0 }
            r34.m()     // Catch:{ all -> 0x08a0 }
            i.b.k.ResourcesFlusher.b(r4)     // Catch:{ all -> 0x08a0 }
            i.b.k.ResourcesFlusher.b(r36)     // Catch:{ all -> 0x08a0 }
            java.lang.String r2 = r4.a     // Catch:{ all -> 0x08a0 }
            i.b.k.ResourcesFlusher.b(r2)     // Catch:{ all -> 0x08a0 }
            java.lang.String r2 = r4.a     // Catch:{ all -> 0x08a0 }
            java.lang.String r6 = r3.b     // Catch:{ all -> 0x08a0 }
            boolean r2 = r2.equals(r6)     // Catch:{ all -> 0x08a0 }
            i.b.k.ResourcesFlusher.a(r2)     // Catch:{ all -> 0x08a0 }
            j.c.a.a.f.e.u0$a r2 = j.c.a.a.f.e.u0.o()     // Catch:{ all -> 0x08a0 }
            r2.a()     // Catch:{ all -> 0x08a0 }
            java.lang.String r6 = "android"
            r2.a(r6)     // Catch:{ all -> 0x08a0 }
            java.lang.String r6 = r3.b     // Catch:{ all -> 0x08a0 }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x08a0 }
            if (r6 != 0) goto L_0x0556
            java.lang.String r6 = r3.b     // Catch:{ all -> 0x08a0 }
            r2.f(r6)     // Catch:{ all -> 0x08a0 }
        L_0x0556:
            java.lang.String r6 = r3.f1951e     // Catch:{ all -> 0x08a0 }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x08a0 }
            if (r6 != 0) goto L_0x0563
            java.lang.String r6 = r3.f1951e     // Catch:{ all -> 0x08a0 }
            r2.e(r6)     // Catch:{ all -> 0x08a0 }
        L_0x0563:
            java.lang.String r6 = r3.d     // Catch:{ all -> 0x08a0 }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x08a0 }
            if (r6 != 0) goto L_0x0570
            java.lang.String r6 = r3.d     // Catch:{ all -> 0x08a0 }
            r2.g(r6)     // Catch:{ all -> 0x08a0 }
        L_0x0570:
            long r6 = r3.f1955k     // Catch:{ all -> 0x08a0 }
            r8 = -2147483648(0xffffffff80000000, double:NaN)
            int r10 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r10 == 0) goto L_0x057f
            long r6 = r3.f1955k     // Catch:{ all -> 0x08a0 }
            int r7 = (int) r6     // Catch:{ all -> 0x08a0 }
            r2.f(r7)     // Catch:{ all -> 0x08a0 }
        L_0x057f:
            long r6 = r3.f1952f     // Catch:{ all -> 0x08a0 }
            r2.e(r6)     // Catch:{ all -> 0x08a0 }
            java.lang.String r6 = r3.c     // Catch:{ all -> 0x08a0 }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x08a0 }
            if (r6 != 0) goto L_0x0591
            java.lang.String r6 = r3.c     // Catch:{ all -> 0x08a0 }
            r2.k(r6)     // Catch:{ all -> 0x08a0 }
        L_0x0591:
            j.c.a.a.g.a.r4 r6 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.i9 r6 = r6.k()     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.b3<java.lang.Boolean> r7 = j.c.a.a.g.a.k.r0     // Catch:{ all -> 0x08a0 }
            boolean r6 = r6.a(r7)     // Catch:{ all -> 0x08a0 }
            if (r6 == 0) goto L_0x05b7
            java.lang.String r6 = r2.x()     // Catch:{ all -> 0x08a0 }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x08a0 }
            if (r6 == 0) goto L_0x05c4
            java.lang.String r6 = r3.f1963s     // Catch:{ all -> 0x08a0 }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x08a0 }
            if (r6 != 0) goto L_0x05c4
            java.lang.String r6 = r3.f1963s     // Catch:{ all -> 0x08a0 }
            r2.n(r6)     // Catch:{ all -> 0x08a0 }
            goto L_0x05c4
        L_0x05b7:
            java.lang.String r6 = r3.f1963s     // Catch:{ all -> 0x08a0 }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x08a0 }
            if (r6 != 0) goto L_0x05c4
            java.lang.String r6 = r3.f1963s     // Catch:{ all -> 0x08a0 }
            r2.n(r6)     // Catch:{ all -> 0x08a0 }
        L_0x05c4:
            long r6 = r3.g     // Catch:{ all -> 0x08a0 }
            r8 = 0
            int r10 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r10 == 0) goto L_0x05d1
            long r6 = r3.g     // Catch:{ all -> 0x08a0 }
            r2.f(r6)     // Catch:{ all -> 0x08a0 }
        L_0x05d1:
            long r6 = r3.u     // Catch:{ all -> 0x08a0 }
            r2.i(r6)     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.r4 r6 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.i9 r6 = r6.k()     // Catch:{ all -> 0x08a0 }
            java.lang.String r7 = r3.b     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.b3<java.lang.Boolean> r10 = j.c.a.a.g.a.k.m0     // Catch:{ all -> 0x08a0 }
            boolean r6 = r6.e(r7, r10)     // Catch:{ all -> 0x08a0 }
            if (r6 == 0) goto L_0x05f3
            j.c.a.a.g.a.u8 r6 = r34.k()     // Catch:{ all -> 0x08a0 }
            java.util.List r6 = r6.u()     // Catch:{ all -> 0x08a0 }
            if (r6 == 0) goto L_0x05f3
            r2.c(r6)     // Catch:{ all -> 0x08a0 }
        L_0x05f3:
            j.c.a.a.g.a.r4 r6 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.w3 r6 = r6.l()     // Catch:{ all -> 0x08a0 }
            java.lang.String r7 = r3.b     // Catch:{ all -> 0x08a0 }
            android.util.Pair r6 = r6.a(r7)     // Catch:{ all -> 0x08a0 }
            java.lang.Object r7 = r6.first     // Catch:{ all -> 0x08a0 }
            java.lang.CharSequence r7 = (java.lang.CharSequence) r7     // Catch:{ all -> 0x08a0 }
            boolean r7 = android.text.TextUtils.isEmpty(r7)     // Catch:{ all -> 0x08a0 }
            if (r7 != 0) goto L_0x0624
            boolean r7 = r3.f1960p     // Catch:{ all -> 0x08a0 }
            if (r7 == 0) goto L_0x0686
            java.lang.Object r7 = r6.first     // Catch:{ all -> 0x08a0 }
            java.lang.String r7 = (java.lang.String) r7     // Catch:{ all -> 0x08a0 }
            r2.h(r7)     // Catch:{ all -> 0x08a0 }
            java.lang.Object r7 = r6.second     // Catch:{ all -> 0x08a0 }
            if (r7 == 0) goto L_0x0686
            java.lang.Object r6 = r6.second     // Catch:{ all -> 0x08a0 }
            java.lang.Boolean r6 = (java.lang.Boolean) r6     // Catch:{ all -> 0x08a0 }
            boolean r6 = r6.booleanValue()     // Catch:{ all -> 0x08a0 }
            r2.a(r6)     // Catch:{ all -> 0x08a0 }
            goto L_0x0686
        L_0x0624:
            j.c.a.a.g.a.r4 r6 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.c r6 = r6.s()     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.r4 r7 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            android.content.Context r7 = r7.f()     // Catch:{ all -> 0x08a0 }
            boolean r6 = r6.a(r7)     // Catch:{ all -> 0x08a0 }
            if (r6 != 0) goto L_0x0686
            boolean r6 = r3.f1961q     // Catch:{ all -> 0x08a0 }
            if (r6 == 0) goto L_0x0686
            j.c.a.a.g.a.r4 r6 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            android.content.Context r6 = r6.f()     // Catch:{ all -> 0x08a0 }
            android.content.ContentResolver r6 = r6.getContentResolver()     // Catch:{ all -> 0x08a0 }
            java.lang.String r7 = "android_id"
            java.lang.String r6 = android.provider.Settings.Secure.getString(r6, r7)     // Catch:{ all -> 0x08a0 }
            if (r6 != 0) goto L_0x0666
            j.c.a.a.g.a.r4 r6 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.n3 r6 = r6.a()     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.p3 r6 = r6.v()     // Catch:{ all -> 0x08a0 }
            java.lang.String r7 = "null secure ID. appId"
            java.lang.String r10 = r2.v()     // Catch:{ all -> 0x08a0 }
            java.lang.Object r10 = j.c.a.a.g.a.n3.a(r10)     // Catch:{ all -> 0x08a0 }
            r6.a(r7, r10)     // Catch:{ all -> 0x08a0 }
            java.lang.String r6 = "null"
            goto L_0x0683
        L_0x0666:
            boolean r7 = r6.isEmpty()     // Catch:{ all -> 0x08a0 }
            if (r7 == 0) goto L_0x0683
            j.c.a.a.g.a.r4 r7 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.n3 r7 = r7.a()     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.p3 r7 = r7.v()     // Catch:{ all -> 0x08a0 }
            java.lang.String r10 = "empty secure ID. appId"
            java.lang.String r11 = r2.v()     // Catch:{ all -> 0x08a0 }
            java.lang.Object r11 = j.c.a.a.g.a.n3.a(r11)     // Catch:{ all -> 0x08a0 }
            r7.a(r10, r11)     // Catch:{ all -> 0x08a0 }
        L_0x0683:
            r2.m(r6)     // Catch:{ all -> 0x08a0 }
        L_0x0686:
            j.c.a.a.g.a.r4 r6 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.c r6 = r6.s()     // Catch:{ all -> 0x08a0 }
            r6.o()     // Catch:{ all -> 0x08a0 }
            java.lang.String r6 = android.os.Build.MODEL     // Catch:{ all -> 0x08a0 }
            r2.c(r6)     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.r4 r6 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.c r6 = r6.s()     // Catch:{ all -> 0x08a0 }
            r6.o()     // Catch:{ all -> 0x08a0 }
            java.lang.String r6 = android.os.Build.VERSION.RELEASE     // Catch:{ all -> 0x08a0 }
            r2.b(r6)     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.r4 r6 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.c r6 = r6.s()     // Catch:{ all -> 0x08a0 }
            long r6 = r6.t()     // Catch:{ all -> 0x08a0 }
            int r7 = (int) r6     // Catch:{ all -> 0x08a0 }
            r2.d(r7)     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.r4 r6 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.c r6 = r6.s()     // Catch:{ all -> 0x08a0 }
            java.lang.String r6 = r6.u()     // Catch:{ all -> 0x08a0 }
            r2.d(r6)     // Catch:{ all -> 0x08a0 }
            long r6 = r3.f1957m     // Catch:{ all -> 0x08a0 }
            r2.h(r6)     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.r4 r6 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            boolean r6 = r6.c()     // Catch:{ all -> 0x08a0 }
            if (r6 == 0) goto L_0x06de
            boolean r6 = j.c.a.a.g.a.i9.v()     // Catch:{ all -> 0x08a0 }
            if (r6 == 0) goto L_0x06de
            r2.v()     // Catch:{ all -> 0x08a0 }
            boolean r6 = android.text.TextUtils.isEmpty(r19)     // Catch:{ all -> 0x08a0 }
            if (r6 == 0) goto L_0x06da
            goto L_0x06de
        L_0x06da:
            r2.z()     // Catch:{ all -> 0x08a0 }
            throw r19
        L_0x06de:
            j.c.a.a.g.a.n9 r6 = r34.e()     // Catch:{ all -> 0x08a0 }
            java.lang.String r7 = r3.b     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.e4 r6 = r6.b(r7)     // Catch:{ all -> 0x08a0 }
            if (r6 != 0) goto L_0x0751
            j.c.a.a.g.a.e4 r6 = new j.c.a.a.g.a.e4     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.r4 r7 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            java.lang.String r10 = r3.b     // Catch:{ all -> 0x08a0 }
            r6.<init>(r7, r10)     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.r4 r7 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.y8 r7 = r7.n()     // Catch:{ all -> 0x08a0 }
            java.lang.String r7 = r7.w()     // Catch:{ all -> 0x08a0 }
            r6.a(r7)     // Catch:{ all -> 0x08a0 }
            java.lang.String r7 = r3.f1956l     // Catch:{ all -> 0x08a0 }
            r6.e(r7)     // Catch:{ all -> 0x08a0 }
            java.lang.String r7 = r3.c     // Catch:{ all -> 0x08a0 }
            r6.b(r7)     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.r4 r7 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.w3 r7 = r7.l()     // Catch:{ all -> 0x08a0 }
            java.lang.String r10 = r3.b     // Catch:{ all -> 0x08a0 }
            java.lang.String r7 = r7.b(r10)     // Catch:{ all -> 0x08a0 }
            r6.d(r7)     // Catch:{ all -> 0x08a0 }
            r6.g(r8)     // Catch:{ all -> 0x08a0 }
            r6.a(r8)     // Catch:{ all -> 0x08a0 }
            r6.b(r8)     // Catch:{ all -> 0x08a0 }
            java.lang.String r7 = r3.d     // Catch:{ all -> 0x08a0 }
            r6.f(r7)     // Catch:{ all -> 0x08a0 }
            long r10 = r3.f1955k     // Catch:{ all -> 0x08a0 }
            r6.c(r10)     // Catch:{ all -> 0x08a0 }
            java.lang.String r7 = r3.f1951e     // Catch:{ all -> 0x08a0 }
            r6.g(r7)     // Catch:{ all -> 0x08a0 }
            long r10 = r3.f1952f     // Catch:{ all -> 0x08a0 }
            r6.d(r10)     // Catch:{ all -> 0x08a0 }
            long r10 = r3.g     // Catch:{ all -> 0x08a0 }
            r6.e(r10)     // Catch:{ all -> 0x08a0 }
            boolean r7 = r3.f1953i     // Catch:{ all -> 0x08a0 }
            r6.a(r7)     // Catch:{ all -> 0x08a0 }
            long r10 = r3.f1957m     // Catch:{ all -> 0x08a0 }
            r6.j(r10)     // Catch:{ all -> 0x08a0 }
            long r10 = r3.u     // Catch:{ all -> 0x08a0 }
            r6.f(r10)     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.n9 r7 = r34.e()     // Catch:{ all -> 0x08a0 }
            r7.a(r6)     // Catch:{ all -> 0x08a0 }
        L_0x0751:
            java.lang.String r7 = r6.h()     // Catch:{ all -> 0x08a0 }
            boolean r7 = android.text.TextUtils.isEmpty(r7)     // Catch:{ all -> 0x08a0 }
            if (r7 != 0) goto L_0x0762
            java.lang.String r7 = r6.h()     // Catch:{ all -> 0x08a0 }
            r2.i(r7)     // Catch:{ all -> 0x08a0 }
        L_0x0762:
            java.lang.String r7 = r6.k()     // Catch:{ all -> 0x08a0 }
            boolean r7 = android.text.TextUtils.isEmpty(r7)     // Catch:{ all -> 0x08a0 }
            if (r7 != 0) goto L_0x0773
            java.lang.String r6 = r6.k()     // Catch:{ all -> 0x08a0 }
            r2.l(r6)     // Catch:{ all -> 0x08a0 }
        L_0x0773:
            j.c.a.a.g.a.n9 r6 = r34.e()     // Catch:{ all -> 0x08a0 }
            java.lang.String r3 = r3.b     // Catch:{ all -> 0x08a0 }
            java.util.List r3 = r6.a(r3)     // Catch:{ all -> 0x08a0 }
            r11 = 0
        L_0x077e:
            int r6 = r3.size()     // Catch:{ all -> 0x08a0 }
            if (r11 >= r6) goto L_0x07b3
            j.c.a.a.f.e.y0$a r6 = j.c.a.a.f.e.y0.m()     // Catch:{ all -> 0x08a0 }
            java.lang.Object r7 = r3.get(r11)     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.z8 r7 = (j.c.a.a.g.a.z8) r7     // Catch:{ all -> 0x08a0 }
            java.lang.String r7 = r7.c     // Catch:{ all -> 0x08a0 }
            r6.a(r7)     // Catch:{ all -> 0x08a0 }
            java.lang.Object r7 = r3.get(r11)     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.z8 r7 = (j.c.a.a.g.a.z8) r7     // Catch:{ all -> 0x08a0 }
            long r12 = r7.d     // Catch:{ all -> 0x08a0 }
            r6.a(r12)     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.u8 r7 = r34.k()     // Catch:{ all -> 0x08a0 }
            java.lang.Object r10 = r3.get(r11)     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.z8 r10 = (j.c.a.a.g.a.z8) r10     // Catch:{ all -> 0x08a0 }
            java.lang.Object r10 = r10.f2143e     // Catch:{ all -> 0x08a0 }
            r7.a(r6, r10)     // Catch:{ all -> 0x08a0 }
            r2.a(r6)     // Catch:{ all -> 0x08a0 }
            int r11 = r11 + 1
            goto L_0x077e
        L_0x07b3:
            j.c.a.a.g.a.n9 r3 = r34.e()     // Catch:{ IOException -> 0x082e }
            j.c.a.a.f.e.f5 r6 = r2.m()     // Catch:{ IOException -> 0x082e }
            j.c.a.a.f.e.x3 r6 = (j.c.a.a.f.e.x3) r6     // Catch:{ IOException -> 0x082e }
            j.c.a.a.f.e.u0 r6 = (j.c.a.a.f.e.u0) r6     // Catch:{ IOException -> 0x082e }
            long r2 = r3.a(r6)     // Catch:{ IOException -> 0x082e }
            j.c.a.a.g.a.n9 r6 = r34.e()     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.h r7 = r4.f1986f     // Catch:{ all -> 0x08a0 }
            if (r7 == 0) goto L_0x0824
            j.c.a.a.g.a.h r7 = r4.f1986f     // Catch:{ all -> 0x08a0 }
            java.util.Iterator r7 = r7.iterator()     // Catch:{ all -> 0x08a0 }
        L_0x07d1:
            r10 = r7
            j.c.a.a.g.a.g r10 = (j.c.a.a.g.a.g) r10
            boolean r11 = r10.hasNext()     // Catch:{ all -> 0x08a0 }
            if (r11 == 0) goto L_0x07e7
            java.lang.Object r10 = r10.next()     // Catch:{ all -> 0x08a0 }
            java.lang.String r10 = (java.lang.String) r10     // Catch:{ all -> 0x08a0 }
            boolean r10 = r5.equals(r10)     // Catch:{ all -> 0x08a0 }
            if (r10 == 0) goto L_0x07d1
            goto L_0x0822
        L_0x07e7:
            j.c.a.a.g.a.m4 r5 = r34.c()     // Catch:{ all -> 0x08a0 }
            java.lang.String r7 = r4.a     // Catch:{ all -> 0x08a0 }
            java.lang.String r10 = r4.b     // Catch:{ all -> 0x08a0 }
            boolean r5 = r5.c(r7, r10)     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.n9 r25 = r34.e()     // Catch:{ all -> 0x08a0 }
            long r26 = r34.s()     // Catch:{ all -> 0x08a0 }
            java.lang.String r7 = r4.a     // Catch:{ all -> 0x08a0 }
            r29 = 0
            r30 = 0
            r31 = 0
            r32 = 0
            r33 = 0
            r28 = r7
            j.c.a.a.g.a.m9 r7 = r25.a(r26, r28, r29, r30, r31, r32, r33)     // Catch:{ all -> 0x08a0 }
            if (r5 == 0) goto L_0x0824
            long r10 = r7.f2044e     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.r4 r5 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.i9 r5 = r5.k()     // Catch:{ all -> 0x08a0 }
            java.lang.String r7 = r4.a     // Catch:{ all -> 0x08a0 }
            int r5 = r5.a(r7)     // Catch:{ all -> 0x08a0 }
            long r12 = (long) r5     // Catch:{ all -> 0x08a0 }
            int r5 = (r10 > r12 ? 1 : (r10 == r12 ? 0 : -1))
            if (r5 >= 0) goto L_0x0824
        L_0x0822:
            r5 = 1
            goto L_0x0825
        L_0x0824:
            r5 = 0
        L_0x0825:
            boolean r2 = r6.a(r4, r2, r5)     // Catch:{ all -> 0x08a0 }
            if (r2 == 0) goto L_0x0847
            r1.f2077m = r8     // Catch:{ all -> 0x08a0 }
            goto L_0x0847
        L_0x082e:
            r0 = move-exception
            r3 = r0
            j.c.a.a.g.a.r4 r5 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.n3 r5 = r5.a()     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.p3 r5 = r5.u()     // Catch:{ all -> 0x08a0 }
            java.lang.String r6 = "Data loss. Failed to insert raw event metadata. appId"
            java.lang.String r2 = r2.v()     // Catch:{ all -> 0x08a0 }
            java.lang.Object r2 = j.c.a.a.g.a.n3.a(r2)     // Catch:{ all -> 0x08a0 }
            r5.a(r6, r2, r3)     // Catch:{ all -> 0x08a0 }
        L_0x0847:
            j.c.a.a.g.a.n9 r2 = r34.e()     // Catch:{ all -> 0x08a0 }
            r2.u()     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.r4 r2 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.n3 r2 = r2.a()     // Catch:{ all -> 0x08a0 }
            r3 = 2
            boolean r2 = r2.a(r3)     // Catch:{ all -> 0x08a0 }
            if (r2 == 0) goto L_0x0874
            j.c.a.a.g.a.r4 r2 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.n3 r2 = r2.a()     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.p3 r2 = r2.y()     // Catch:{ all -> 0x08a0 }
            java.lang.String r3 = "Event recorded"
            j.c.a.a.g.a.r4 r5 = r1.f2073i     // Catch:{ all -> 0x08a0 }
            j.c.a.a.g.a.l3 r5 = r5.o()     // Catch:{ all -> 0x08a0 }
            java.lang.String r4 = r5.a(r4)     // Catch:{ all -> 0x08a0 }
            r2.a(r3, r4)     // Catch:{ all -> 0x08a0 }
        L_0x0874:
            j.c.a.a.g.a.n9 r2 = r34.e()
            r2.A()
            r34.u()
            j.c.a.a.g.a.r4 r2 = r1.f2073i
            j.c.a.a.g.a.n3 r2 = r2.a()
            j.c.a.a.g.a.p3 r2 = r2.y()
            long r3 = java.lang.System.nanoTime()
            long r3 = r3 - r23
            r5 = 500000(0x7a120, double:2.47033E-318)
            long r3 = r3 + r5
            r5 = 1000000(0xf4240, double:4.940656E-318)
            long r3 = r3 / r5
            java.lang.Long r3 = java.lang.Long.valueOf(r3)
            java.lang.String r4 = "Background event processing time, ms"
            r2.a(r4, r3)
            return
        L_0x08a0:
            r0 = move-exception
            r2 = r0
            j.c.a.a.g.a.n9 r3 = r34.e()
            r3.A()
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.q8.b(j.c.a.a.g.a.i, j.c.a.a.g.a.d9):void");
    }

    public final m4 c() {
        a(this.a);
        return this.a;
    }

    public final r3 d() {
        a(this.b);
        return this.b;
    }

    public final n9 e() {
        a(this.c);
        return this.c;
    }

    public final Context f() {
        return this.f2073i.a;
    }

    public final f9 g() {
        a(this.f2072f);
        return this.f2072f;
    }

    public final h9 h() {
        return this.f2073i.f2086f;
    }

    public final l4 i() {
        return this.f2073i.i();
    }

    public final j.c.a.a.c.n.a j() {
        return this.f2073i.f2092n;
    }

    public final u8 k() {
        a(this.g);
        return this.g;
    }

    public final l3 l() {
        return this.f2073i.o();
    }

    public final void m() {
        if (!this.f2074j) {
            throw new IllegalStateException("UploadController is not initialized");
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:98|99) */
    /* JADX WARNING: Code restructure failed: missing block: B:99:?, code lost:
        r1.f2073i.a().f2046f.a("Failed to parse upload URL. Not uploading. appId", j.c.a.a.g.a.n3.a(r3), r0);
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:98:0x0309 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void n() {
        /*
            r18 = this;
            r1 = r18
            r18.r()
            r18.m()
            r0 = 1
            r1.f2083s = r0
            r2 = 0
            j.c.a.a.g.a.r4 r3 = r1.f2073i     // Catch:{ all -> 0x034e }
            j.c.a.a.g.a.h9 r3 = r3.f2086f     // Catch:{ all -> 0x034e }
            j.c.a.a.g.a.r4 r3 = r1.f2073i     // Catch:{ all -> 0x034e }
            j.c.a.a.g.a.z6 r3 = r3.r()     // Catch:{ all -> 0x034e }
            java.lang.Boolean r3 = r3.f2140e     // Catch:{ all -> 0x034e }
            if (r3 != 0) goto L_0x002d
            j.c.a.a.g.a.r4 r0 = r1.f2073i     // Catch:{ all -> 0x034e }
            j.c.a.a.g.a.n3 r0 = r0.a()     // Catch:{ all -> 0x034e }
            j.c.a.a.g.a.p3 r0 = r0.f2047i     // Catch:{ all -> 0x034e }
            java.lang.String r3 = "Upload data called on the client side before use of service was decided"
            r0.a(r3)     // Catch:{ all -> 0x034e }
            r1.f2083s = r2
            r18.b()
            return
        L_0x002d:
            boolean r3 = r3.booleanValue()     // Catch:{ all -> 0x034e }
            if (r3 == 0) goto L_0x0046
            j.c.a.a.g.a.r4 r0 = r1.f2073i     // Catch:{ all -> 0x034e }
            j.c.a.a.g.a.n3 r0 = r0.a()     // Catch:{ all -> 0x034e }
            j.c.a.a.g.a.p3 r0 = r0.f2046f     // Catch:{ all -> 0x034e }
            java.lang.String r3 = "Upload called in the client side when service should be used"
            r0.a(r3)     // Catch:{ all -> 0x034e }
            r1.f2083s = r2
            r18.b()
            return
        L_0x0046:
            long r3 = r1.f2077m     // Catch:{ all -> 0x034e }
            r5 = 0
            int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r7 <= 0) goto L_0x0057
            r18.u()     // Catch:{ all -> 0x034e }
            r1.f2083s = r2
            r18.b()
            return
        L_0x0057:
            r18.r()     // Catch:{ all -> 0x034e }
            java.util.List<java.lang.Long> r3 = r1.v     // Catch:{ all -> 0x034e }
            if (r3 == 0) goto L_0x0060
            r3 = 1
            goto L_0x0061
        L_0x0060:
            r3 = 0
        L_0x0061:
            if (r3 == 0) goto L_0x0076
            j.c.a.a.g.a.r4 r0 = r1.f2073i     // Catch:{ all -> 0x034e }
            j.c.a.a.g.a.n3 r0 = r0.a()     // Catch:{ all -> 0x034e }
            j.c.a.a.g.a.p3 r0 = r0.f2052n     // Catch:{ all -> 0x034e }
            java.lang.String r3 = "Uploading requested multiple times"
            r0.a(r3)     // Catch:{ all -> 0x034e }
            r1.f2083s = r2
            r18.b()
            return
        L_0x0076:
            j.c.a.a.g.a.r3 r3 = r18.d()     // Catch:{ all -> 0x034e }
            boolean r3 = r3.u()     // Catch:{ all -> 0x034e }
            if (r3 != 0) goto L_0x0096
            j.c.a.a.g.a.r4 r0 = r1.f2073i     // Catch:{ all -> 0x034e }
            j.c.a.a.g.a.n3 r0 = r0.a()     // Catch:{ all -> 0x034e }
            j.c.a.a.g.a.p3 r0 = r0.f2052n     // Catch:{ all -> 0x034e }
            java.lang.String r3 = "Network not connected, ignoring upload request"
            r0.a(r3)     // Catch:{ all -> 0x034e }
            r18.u()     // Catch:{ all -> 0x034e }
            r1.f2083s = r2
            r18.b()
            return
        L_0x0096:
            j.c.a.a.g.a.r4 r3 = r1.f2073i     // Catch:{ all -> 0x034e }
            j.c.a.a.c.n.a r3 = r3.f2092n     // Catch:{ all -> 0x034e }
            j.c.a.a.c.n.b r3 = (j.c.a.a.c.n.b) r3     // Catch:{ all -> 0x034e }
            r4 = 0
            if (r3 == 0) goto L_0x034c
            long r7 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x034e }
            j.c.a.a.g.a.b3<java.lang.Long> r3 = j.c.a.a.g.a.k.f2011i     // Catch:{ all -> 0x034e }
            java.lang.Object r3 = r3.a(r4)     // Catch:{ all -> 0x034e }
            java.lang.Long r3 = (java.lang.Long) r3     // Catch:{ all -> 0x034e }
            long r9 = r3.longValue()     // Catch:{ all -> 0x034e }
            long r9 = r7 - r9
            r1.a(r9)     // Catch:{ all -> 0x034e }
            j.c.a.a.g.a.r4 r3 = r1.f2073i     // Catch:{ all -> 0x034e }
            j.c.a.a.g.a.w3 r3 = r3.l()     // Catch:{ all -> 0x034e }
            j.c.a.a.g.a.b4 r3 = r3.f2111e     // Catch:{ all -> 0x034e }
            long r9 = r3.a()     // Catch:{ all -> 0x034e }
            int r3 = (r9 > r5 ? 1 : (r9 == r5 ? 0 : -1))
            if (r3 == 0) goto L_0x00db
            j.c.a.a.g.a.r4 r3 = r1.f2073i     // Catch:{ all -> 0x034e }
            j.c.a.a.g.a.n3 r3 = r3.a()     // Catch:{ all -> 0x034e }
            j.c.a.a.g.a.p3 r3 = r3.f2051m     // Catch:{ all -> 0x034e }
            java.lang.String r5 = "Uploading events. Elapsed time since last upload attempt (ms)"
            long r9 = r7 - r9
            long r9 = java.lang.Math.abs(r9)     // Catch:{ all -> 0x034e }
            java.lang.Long r6 = java.lang.Long.valueOf(r9)     // Catch:{ all -> 0x034e }
            r3.a(r5, r6)     // Catch:{ all -> 0x034e }
        L_0x00db:
            j.c.a.a.g.a.n9 r3 = r18.e()     // Catch:{ all -> 0x034e }
            java.lang.String r3 = r3.w()     // Catch:{ all -> 0x034e }
            boolean r5 = android.text.TextUtils.isEmpty(r3)     // Catch:{ all -> 0x034e }
            r9 = -1
            if (r5 != 0) goto L_0x031b
            long r5 = r1.x     // Catch:{ all -> 0x034e }
            int r11 = (r5 > r9 ? 1 : (r5 == r9 ? 0 : -1))
            if (r11 != 0) goto L_0x00fb
            j.c.a.a.g.a.n9 r5 = r18.e()     // Catch:{ all -> 0x034e }
            long r5 = r5.x()     // Catch:{ all -> 0x034e }
            r1.x = r5     // Catch:{ all -> 0x034e }
        L_0x00fb:
            j.c.a.a.g.a.r4 r5 = r1.f2073i     // Catch:{ all -> 0x034e }
            j.c.a.a.g.a.i9 r5 = r5.g     // Catch:{ all -> 0x034e }
            j.c.a.a.g.a.b3<java.lang.Integer> r6 = j.c.a.a.g.a.k.f2014l     // Catch:{ all -> 0x034e }
            int r5 = r5.b(r3, r6)     // Catch:{ all -> 0x034e }
            j.c.a.a.g.a.r4 r6 = r1.f2073i     // Catch:{ all -> 0x034e }
            j.c.a.a.g.a.i9 r6 = r6.g     // Catch:{ all -> 0x034e }
            j.c.a.a.g.a.b3<java.lang.Integer> r9 = j.c.a.a.g.a.k.f2015m     // Catch:{ all -> 0x034e }
            int r6 = r6.b(r3, r9)     // Catch:{ all -> 0x034e }
            int r6 = java.lang.Math.max(r2, r6)     // Catch:{ all -> 0x034e }
            j.c.a.a.g.a.n9 r9 = r18.e()     // Catch:{ all -> 0x034e }
            java.util.List r5 = r9.a(r3, r5, r6)     // Catch:{ all -> 0x034e }
            boolean r6 = r5.isEmpty()     // Catch:{ all -> 0x034e }
            if (r6 != 0) goto L_0x0346
            java.util.Iterator r6 = r5.iterator()     // Catch:{ all -> 0x034e }
        L_0x0125:
            boolean r9 = r6.hasNext()     // Catch:{ all -> 0x034e }
            if (r9 == 0) goto L_0x0140
            java.lang.Object r9 = r6.next()     // Catch:{ all -> 0x034e }
            android.util.Pair r9 = (android.util.Pair) r9     // Catch:{ all -> 0x034e }
            java.lang.Object r9 = r9.first     // Catch:{ all -> 0x034e }
            j.c.a.a.f.e.u0 r9 = (j.c.a.a.f.e.u0) r9     // Catch:{ all -> 0x034e }
            java.lang.String r10 = r9.zzw     // Catch:{ all -> 0x034e }
            boolean r10 = android.text.TextUtils.isEmpty(r10)     // Catch:{ all -> 0x034e }
            if (r10 != 0) goto L_0x0125
            java.lang.String r6 = r9.zzw     // Catch:{ all -> 0x034e }
            goto L_0x0141
        L_0x0140:
            r6 = r4
        L_0x0141:
            if (r6 == 0) goto L_0x016c
            r9 = 0
        L_0x0144:
            int r10 = r5.size()     // Catch:{ all -> 0x034e }
            if (r9 >= r10) goto L_0x016c
            java.lang.Object r10 = r5.get(r9)     // Catch:{ all -> 0x034e }
            android.util.Pair r10 = (android.util.Pair) r10     // Catch:{ all -> 0x034e }
            java.lang.Object r10 = r10.first     // Catch:{ all -> 0x034e }
            j.c.a.a.f.e.u0 r10 = (j.c.a.a.f.e.u0) r10     // Catch:{ all -> 0x034e }
            java.lang.String r11 = r10.zzw     // Catch:{ all -> 0x034e }
            boolean r11 = android.text.TextUtils.isEmpty(r11)     // Catch:{ all -> 0x034e }
            if (r11 != 0) goto L_0x0169
            java.lang.String r10 = r10.zzw     // Catch:{ all -> 0x034e }
            boolean r10 = r10.equals(r6)     // Catch:{ all -> 0x034e }
            if (r10 != 0) goto L_0x0169
            java.util.List r5 = r5.subList(r2, r9)     // Catch:{ all -> 0x034e }
            goto L_0x016c
        L_0x0169:
            int r9 = r9 + 1
            goto L_0x0144
        L_0x016c:
            j.c.a.a.f.e.t0 r6 = j.c.a.a.f.e.t0.zzd     // Catch:{ all -> 0x034e }
            j.c.a.a.f.e.x3$a r6 = r6.g()     // Catch:{ all -> 0x034e }
            j.c.a.a.f.e.t0$a r6 = (j.c.a.a.f.e.t0.a) r6     // Catch:{ all -> 0x034e }
            int r9 = r5.size()     // Catch:{ all -> 0x034e }
            java.util.ArrayList r10 = new java.util.ArrayList     // Catch:{ all -> 0x034e }
            int r11 = r5.size()     // Catch:{ all -> 0x034e }
            r10.<init>(r11)     // Catch:{ all -> 0x034e }
            boolean r11 = j.c.a.a.g.a.i9.v()     // Catch:{ all -> 0x034e }
            if (r11 == 0) goto L_0x019d
            j.c.a.a.g.a.r4 r11 = r1.f2073i     // Catch:{ all -> 0x034e }
            j.c.a.a.g.a.i9 r11 = r11.g     // Catch:{ all -> 0x034e }
            j.c.a.a.g.a.k9 r11 = r11.c     // Catch:{ all -> 0x034e }
            java.lang.String r12 = "gaia_collection_enabled"
            java.lang.String r11 = r11.a(r3, r12)     // Catch:{ all -> 0x034e }
            java.lang.String r12 = "1"
            boolean r11 = r12.equals(r11)     // Catch:{ all -> 0x034e }
            if (r11 == 0) goto L_0x019d
            r11 = 1
            goto L_0x019e
        L_0x019d:
            r11 = 0
        L_0x019e:
            r12 = 0
        L_0x019f:
            r13 = 2
            if (r12 >= r9) goto L_0x024f
            java.lang.Object r14 = r5.get(r12)     // Catch:{ all -> 0x034e }
            android.util.Pair r14 = (android.util.Pair) r14     // Catch:{ all -> 0x034e }
            java.lang.Object r14 = r14.first     // Catch:{ all -> 0x034e }
            j.c.a.a.f.e.u0 r14 = (j.c.a.a.f.e.u0) r14     // Catch:{ all -> 0x034e }
            j.c.a.a.f.e.x3$a r14 = r14.h()     // Catch:{ all -> 0x034e }
            j.c.a.a.f.e.u0$a r14 = (j.c.a.a.f.e.u0.a) r14     // Catch:{ all -> 0x034e }
            java.lang.Object r15 = r5.get(r12)     // Catch:{ all -> 0x034e }
            android.util.Pair r15 = (android.util.Pair) r15     // Catch:{ all -> 0x034e }
            java.lang.Object r15 = r15.second     // Catch:{ all -> 0x034e }
            java.lang.Long r15 = (java.lang.Long) r15     // Catch:{ all -> 0x034e }
            r10.add(r15)     // Catch:{ all -> 0x034e }
            j.c.a.a.g.a.r4 r15 = r1.f2073i     // Catch:{ all -> 0x034e }
            j.c.a.a.g.a.i9 r15 = r15.g     // Catch:{ all -> 0x034e }
            r15.o()     // Catch:{ all -> 0x034e }
            r16 = r5
            r4 = 18079(0x469f, double:8.932E-320)
            r14.i()     // Catch:{ all -> 0x034e }
            MessageType r15 = r14.c     // Catch:{ all -> 0x034e }
            j.c.a.a.f.e.u0 r15 = (j.c.a.a.f.e.u0) r15     // Catch:{ all -> 0x034e }
            int r0 = r15.zzc     // Catch:{ all -> 0x034e }
            r17 = 32768(0x8000, float:4.5918E-41)
            r0 = r0 | r17
            r15.zzc = r0     // Catch:{ all -> 0x034e }
            r15.zzv = r4     // Catch:{ all -> 0x034e }
            r14.i()     // Catch:{ all -> 0x034e }
            MessageType r0 = r14.c     // Catch:{ all -> 0x034e }
            j.c.a.a.f.e.u0 r0 = (j.c.a.a.f.e.u0) r0     // Catch:{ all -> 0x034e }
            int r4 = r0.zzc     // Catch:{ all -> 0x034e }
            r4 = r4 | r13
            r0.zzc = r4     // Catch:{ all -> 0x034e }
            r0.zzh = r7     // Catch:{ all -> 0x034e }
            j.c.a.a.g.a.r4 r0 = r1.f2073i     // Catch:{ all -> 0x034e }
            j.c.a.a.g.a.h9 r0 = r0.f2086f     // Catch:{ all -> 0x034e }
            r14.i()     // Catch:{ all -> 0x034e }
            MessageType r0 = r14.c     // Catch:{ all -> 0x034e }
            j.c.a.a.f.e.u0 r0 = (j.c.a.a.f.e.u0) r0     // Catch:{ all -> 0x034e }
            int r4 = r0.zzc     // Catch:{ all -> 0x034e }
            r5 = 8388608(0x800000, float:1.17549435E-38)
            r4 = r4 | r5
            r0.zzc = r4     // Catch:{ all -> 0x034e }
            r0.zzad = r2     // Catch:{ all -> 0x034e }
            if (r11 != 0) goto L_0x0215
            r14.i()     // Catch:{ all -> 0x034e }
            MessageType r0 = r14.c     // Catch:{ all -> 0x034e }
            j.c.a.a.f.e.u0 r0 = (j.c.a.a.f.e.u0) r0     // Catch:{ all -> 0x034e }
            int r4 = r0.zzc     // Catch:{ all -> 0x034e }
            r5 = 2147483647(0x7fffffff, float:NaN)
            r4 = r4 & r5
            r0.zzc = r4     // Catch:{ all -> 0x034e }
            j.c.a.a.f.e.u0 r4 = j.c.a.a.f.e.u0.zzav     // Catch:{ all -> 0x034e }
            java.lang.String r4 = r4.zzam     // Catch:{ all -> 0x034e }
            r0.zzam = r4     // Catch:{ all -> 0x034e }
        L_0x0215:
            j.c.a.a.g.a.r4 r0 = r1.f2073i     // Catch:{ all -> 0x034e }
            j.c.a.a.g.a.i9 r0 = r0.g     // Catch:{ all -> 0x034e }
            j.c.a.a.g.a.b3<java.lang.Boolean> r4 = j.c.a.a.g.a.k.q0     // Catch:{ all -> 0x034e }
            boolean r0 = r0.d(r3, r4)     // Catch:{ all -> 0x034e }
            if (r0 == 0) goto L_0x0244
            j.c.a.a.f.e.f5 r0 = r14.m()     // Catch:{ all -> 0x034e }
            j.c.a.a.f.e.x3 r0 = (j.c.a.a.f.e.x3) r0     // Catch:{ all -> 0x034e }
            j.c.a.a.f.e.u0 r0 = (j.c.a.a.f.e.u0) r0     // Catch:{ all -> 0x034e }
            byte[] r0 = r0.f()     // Catch:{ all -> 0x034e }
            j.c.a.a.g.a.u8 r4 = r18.k()     // Catch:{ all -> 0x034e }
            long r4 = r4.a(r0)     // Catch:{ all -> 0x034e }
            r14.i()     // Catch:{ all -> 0x034e }
            MessageType r0 = r14.c     // Catch:{ all -> 0x034e }
            j.c.a.a.f.e.u0 r0 = (j.c.a.a.f.e.u0) r0     // Catch:{ all -> 0x034e }
            int r13 = r0.zzd     // Catch:{ all -> 0x034e }
            r13 = r13 | 32
            r0.zzd = r13     // Catch:{ all -> 0x034e }
            r0.zzat = r4     // Catch:{ all -> 0x034e }
        L_0x0244:
            r6.a(r14)     // Catch:{ all -> 0x034e }
            int r12 = r12 + 1
            r5 = r16
            r0 = 1
            r4 = 0
            goto L_0x019f
        L_0x024f:
            j.c.a.a.g.a.r4 r0 = r1.f2073i     // Catch:{ all -> 0x034e }
            j.c.a.a.g.a.n3 r0 = r0.a()     // Catch:{ all -> 0x034e }
            boolean r0 = r0.a(r13)     // Catch:{ all -> 0x034e }
            if (r0 == 0) goto L_0x026c
            j.c.a.a.g.a.u8 r0 = r18.k()     // Catch:{ all -> 0x034e }
            j.c.a.a.f.e.f5 r4 = r6.m()     // Catch:{ all -> 0x034e }
            j.c.a.a.f.e.x3 r4 = (j.c.a.a.f.e.x3) r4     // Catch:{ all -> 0x034e }
            j.c.a.a.f.e.t0 r4 = (j.c.a.a.f.e.t0) r4     // Catch:{ all -> 0x034e }
            java.lang.String r15 = r0.a(r4)     // Catch:{ all -> 0x034e }
            goto L_0x026d
        L_0x026c:
            r15 = 0
        L_0x026d:
            r18.k()     // Catch:{ all -> 0x034e }
            j.c.a.a.f.e.f5 r0 = r6.m()     // Catch:{ all -> 0x034e }
            j.c.a.a.f.e.x3 r0 = (j.c.a.a.f.e.x3) r0     // Catch:{ all -> 0x034e }
            j.c.a.a.f.e.t0 r0 = (j.c.a.a.f.e.t0) r0     // Catch:{ all -> 0x034e }
            byte[] r13 = r0.f()     // Catch:{ all -> 0x034e }
            j.c.a.a.g.a.b3<java.lang.String> r0 = j.c.a.a.g.a.k.v     // Catch:{ all -> 0x034e }
            r4 = 0
            java.lang.Object r0 = r0.a(r4)     // Catch:{ all -> 0x034e }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x034e }
            java.net.URL r12 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0309 }
            r12.<init>(r0)     // Catch:{ MalformedURLException -> 0x0309 }
            boolean r4 = r10.isEmpty()     // Catch:{ MalformedURLException -> 0x0309 }
            if (r4 != 0) goto L_0x0292
            r4 = 1
            goto L_0x0293
        L_0x0292:
            r4 = 0
        L_0x0293:
            i.b.k.ResourcesFlusher.a(r4)     // Catch:{ MalformedURLException -> 0x0309 }
            java.util.List<java.lang.Long> r4 = r1.v     // Catch:{ MalformedURLException -> 0x0309 }
            if (r4 == 0) goto L_0x02a8
            j.c.a.a.g.a.r4 r4 = r1.f2073i     // Catch:{ MalformedURLException -> 0x0309 }
            j.c.a.a.g.a.n3 r4 = r4.a()     // Catch:{ MalformedURLException -> 0x0309 }
            j.c.a.a.g.a.p3 r4 = r4.f2046f     // Catch:{ MalformedURLException -> 0x0309 }
            java.lang.String r5 = "Set uploading progress before finishing the previous upload"
            r4.a(r5)     // Catch:{ MalformedURLException -> 0x0309 }
            goto L_0x02af
        L_0x02a8:
            java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ MalformedURLException -> 0x0309 }
            r4.<init>(r10)     // Catch:{ MalformedURLException -> 0x0309 }
            r1.v = r4     // Catch:{ MalformedURLException -> 0x0309 }
        L_0x02af:
            j.c.a.a.g.a.r4 r4 = r1.f2073i     // Catch:{ MalformedURLException -> 0x0309 }
            j.c.a.a.g.a.w3 r4 = r4.l()     // Catch:{ MalformedURLException -> 0x0309 }
            j.c.a.a.g.a.b4 r4 = r4.f2112f     // Catch:{ MalformedURLException -> 0x0309 }
            r4.a(r7)     // Catch:{ MalformedURLException -> 0x0309 }
            java.lang.String r4 = "?"
            if (r9 <= 0) goto L_0x02cc
            MessageType r4 = r6.c     // Catch:{ MalformedURLException -> 0x0309 }
            j.c.a.a.f.e.t0 r4 = (j.c.a.a.f.e.t0) r4     // Catch:{ MalformedURLException -> 0x0309 }
            j.c.a.a.f.e.e4<j.c.a.a.f.e.u0> r4 = r4.zzc     // Catch:{ MalformedURLException -> 0x0309 }
            java.lang.Object r4 = r4.get(r2)     // Catch:{ MalformedURLException -> 0x0309 }
            j.c.a.a.f.e.u0 r4 = (j.c.a.a.f.e.u0) r4     // Catch:{ MalformedURLException -> 0x0309 }
            java.lang.String r4 = r4.zzs     // Catch:{ MalformedURLException -> 0x0309 }
        L_0x02cc:
            j.c.a.a.g.a.r4 r5 = r1.f2073i     // Catch:{ MalformedURLException -> 0x0309 }
            j.c.a.a.g.a.n3 r5 = r5.a()     // Catch:{ MalformedURLException -> 0x0309 }
            j.c.a.a.g.a.p3 r5 = r5.f2052n     // Catch:{ MalformedURLException -> 0x0309 }
            java.lang.String r6 = "Uploading data. app, uncompressed size, data"
            int r7 = r13.length     // Catch:{ MalformedURLException -> 0x0309 }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ MalformedURLException -> 0x0309 }
            r5.a(r6, r4, r7, r15)     // Catch:{ MalformedURLException -> 0x0309 }
            r4 = 1
            r1.f2082r = r4     // Catch:{ MalformedURLException -> 0x0309 }
            j.c.a.a.g.a.r3 r10 = r18.d()     // Catch:{ MalformedURLException -> 0x0309 }
            j.c.a.a.g.a.s8 r15 = new j.c.a.a.g.a.s8     // Catch:{ MalformedURLException -> 0x0309 }
            r15.<init>(r1, r3)     // Catch:{ MalformedURLException -> 0x0309 }
            r10.d()     // Catch:{ MalformedURLException -> 0x0309 }
            r10.o()     // Catch:{ MalformedURLException -> 0x0309 }
            i.b.k.ResourcesFlusher.b(r12)     // Catch:{ MalformedURLException -> 0x0309 }
            i.b.k.ResourcesFlusher.b(r13)     // Catch:{ MalformedURLException -> 0x0309 }
            i.b.k.ResourcesFlusher.b(r15)     // Catch:{ MalformedURLException -> 0x0309 }
            j.c.a.a.g.a.l4 r4 = r10.i()     // Catch:{ MalformedURLException -> 0x0309 }
            j.c.a.a.g.a.v3 r5 = new j.c.a.a.g.a.v3     // Catch:{ MalformedURLException -> 0x0309 }
            r14 = 0
            r9 = r5
            r11 = r3
            r9.<init>(r10, r11, r12, r13, r14, r15)     // Catch:{ MalformedURLException -> 0x0309 }
            r4.b(r5)     // Catch:{ MalformedURLException -> 0x0309 }
            goto L_0x0346
        L_0x0309:
            j.c.a.a.g.a.r4 r4 = r1.f2073i     // Catch:{ all -> 0x034e }
            j.c.a.a.g.a.n3 r4 = r4.a()     // Catch:{ all -> 0x034e }
            j.c.a.a.g.a.p3 r4 = r4.f2046f     // Catch:{ all -> 0x034e }
            java.lang.String r5 = "Failed to parse upload URL. Not uploading. appId"
            java.lang.Object r3 = j.c.a.a.g.a.n3.a(r3)     // Catch:{ all -> 0x034e }
            r4.a(r5, r3, r0)     // Catch:{ all -> 0x034e }
            goto L_0x0346
        L_0x031b:
            r1.x = r9     // Catch:{ all -> 0x034e }
            j.c.a.a.g.a.n9 r0 = r18.e()     // Catch:{ all -> 0x034e }
            j.c.a.a.g.a.b3<java.lang.Long> r3 = j.c.a.a.g.a.k.f2011i     // Catch:{ all -> 0x034e }
            r4 = 0
            java.lang.Object r3 = r3.a(r4)     // Catch:{ all -> 0x034e }
            java.lang.Long r3 = (java.lang.Long) r3     // Catch:{ all -> 0x034e }
            long r3 = r3.longValue()     // Catch:{ all -> 0x034e }
            long r7 = r7 - r3
            java.lang.String r0 = r0.a(r7)     // Catch:{ all -> 0x034e }
            boolean r3 = android.text.TextUtils.isEmpty(r0)     // Catch:{ all -> 0x034e }
            if (r3 != 0) goto L_0x0346
            j.c.a.a.g.a.n9 r3 = r18.e()     // Catch:{ all -> 0x034e }
            j.c.a.a.g.a.e4 r0 = r3.b(r0)     // Catch:{ all -> 0x034e }
            if (r0 == 0) goto L_0x0346
            r1.a(r0)     // Catch:{ all -> 0x034e }
        L_0x0346:
            r1.f2083s = r2
            r18.b()
            return
        L_0x034c:
            r0 = r4
            throw r0     // Catch:{ all -> 0x034e }
        L_0x034e:
            r0 = move-exception
            r1.f2083s = r2
            r18.b()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.q8.n():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0027, code lost:
        if (r11.f2075k != false) goto L_0x0029;
     */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00bf  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0127  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x013e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void o() {
        /*
            r11 = this;
            r11.r()
            r11.m()
            boolean r0 = r11.f2076l
            r1 = 1
            if (r0 != 0) goto L_0x01c9
            r11.f2076l = r1
            r11.r()
            r11.m()
            j.c.a.a.g.a.r4 r0 = r11.f2073i
            j.c.a.a.g.a.i9 r0 = r0.g
            j.c.a.a.g.a.b3<java.lang.Boolean> r2 = j.c.a.a.g.a.k.l0
            boolean r0 = r0.a(r2)
            if (r0 != 0) goto L_0x0029
            r11.r()
            r11.m()
            boolean r0 = r11.f2075k
            if (r0 == 0) goto L_0x01c9
        L_0x0029:
            r11.r()
            j.c.a.a.g.a.r4 r0 = r11.f2073i
            j.c.a.a.g.a.i9 r0 = r0.g
            j.c.a.a.g.a.b3<java.lang.Boolean> r2 = j.c.a.a.g.a.k.G0
            boolean r0 = r0.a(r2)
            java.lang.String r2 = "Storage concurrent access okay"
            r3 = 0
            if (r0 == 0) goto L_0x0051
            java.nio.channels.FileLock r0 = r11.f2084t
            if (r0 == 0) goto L_0x0051
            boolean r0 = r0.isValid()
            if (r0 == 0) goto L_0x0051
            j.c.a.a.g.a.r4 r0 = r11.f2073i
            j.c.a.a.g.a.n3 r0 = r0.a()
            j.c.a.a.g.a.p3 r0 = r0.f2052n
            r0.a(r2)
            goto L_0x0080
        L_0x0051:
            j.c.a.a.g.a.r4 r0 = r11.f2073i
            android.content.Context r0 = r0.a
            java.io.File r0 = r0.getFilesDir()
            java.io.File r4 = new java.io.File
            java.lang.String r5 = "google_app_measurement.db"
            r4.<init>(r0, r5)
            java.io.RandomAccessFile r0 = new java.io.RandomAccessFile     // Catch:{ FileNotFoundException -> 0x00ae, IOException -> 0x009f, OverlappingFileLockException -> 0x0090 }
            java.lang.String r5 = "rw"
            r0.<init>(r4, r5)     // Catch:{ FileNotFoundException -> 0x00ae, IOException -> 0x009f, OverlappingFileLockException -> 0x0090 }
            java.nio.channels.FileChannel r0 = r0.getChannel()     // Catch:{ FileNotFoundException -> 0x00ae, IOException -> 0x009f, OverlappingFileLockException -> 0x0090 }
            r11.u = r0     // Catch:{ FileNotFoundException -> 0x00ae, IOException -> 0x009f, OverlappingFileLockException -> 0x0090 }
            java.nio.channels.FileLock r0 = r0.tryLock()     // Catch:{ FileNotFoundException -> 0x00ae, IOException -> 0x009f, OverlappingFileLockException -> 0x0090 }
            r11.f2084t = r0     // Catch:{ FileNotFoundException -> 0x00ae, IOException -> 0x009f, OverlappingFileLockException -> 0x0090 }
            if (r0 == 0) goto L_0x0082
            j.c.a.a.g.a.r4 r0 = r11.f2073i     // Catch:{ FileNotFoundException -> 0x00ae, IOException -> 0x009f, OverlappingFileLockException -> 0x0090 }
            j.c.a.a.g.a.n3 r0 = r0.a()     // Catch:{ FileNotFoundException -> 0x00ae, IOException -> 0x009f, OverlappingFileLockException -> 0x0090 }
            j.c.a.a.g.a.p3 r0 = r0.f2052n     // Catch:{ FileNotFoundException -> 0x00ae, IOException -> 0x009f, OverlappingFileLockException -> 0x0090 }
            r0.a(r2)     // Catch:{ FileNotFoundException -> 0x00ae, IOException -> 0x009f, OverlappingFileLockException -> 0x0090 }
        L_0x0080:
            r0 = 1
            goto L_0x00bd
        L_0x0082:
            j.c.a.a.g.a.r4 r0 = r11.f2073i     // Catch:{ FileNotFoundException -> 0x00ae, IOException -> 0x009f, OverlappingFileLockException -> 0x0090 }
            j.c.a.a.g.a.n3 r0 = r0.a()     // Catch:{ FileNotFoundException -> 0x00ae, IOException -> 0x009f, OverlappingFileLockException -> 0x0090 }
            j.c.a.a.g.a.p3 r0 = r0.f2046f     // Catch:{ FileNotFoundException -> 0x00ae, IOException -> 0x009f, OverlappingFileLockException -> 0x0090 }
            java.lang.String r2 = "Storage concurrent data access panic"
            r0.a(r2)     // Catch:{ FileNotFoundException -> 0x00ae, IOException -> 0x009f, OverlappingFileLockException -> 0x0090 }
            goto L_0x00bc
        L_0x0090:
            r0 = move-exception
            j.c.a.a.g.a.r4 r2 = r11.f2073i
            j.c.a.a.g.a.n3 r2 = r2.a()
            j.c.a.a.g.a.p3 r2 = r2.f2047i
            java.lang.String r4 = "Storage lock already acquired"
            r2.a(r4, r0)
            goto L_0x00bc
        L_0x009f:
            r0 = move-exception
            j.c.a.a.g.a.r4 r2 = r11.f2073i
            j.c.a.a.g.a.n3 r2 = r2.a()
            j.c.a.a.g.a.p3 r2 = r2.f2046f
            java.lang.String r4 = "Failed to access storage lock file"
            r2.a(r4, r0)
            goto L_0x00bc
        L_0x00ae:
            r0 = move-exception
            j.c.a.a.g.a.r4 r2 = r11.f2073i
            j.c.a.a.g.a.n3 r2 = r2.a()
            j.c.a.a.g.a.p3 r2 = r2.f2046f
            java.lang.String r4 = "Failed to acquire storage lock"
            r2.a(r4, r0)
        L_0x00bc:
            r0 = 0
        L_0x00bd:
            if (r0 == 0) goto L_0x01c9
            java.nio.channels.FileChannel r0 = r11.u
            r11.r()
            r4 = 0
            r2 = 4
            java.lang.String r6 = "Bad channel to read from"
            if (r0 == 0) goto L_0x010b
            boolean r7 = r0.isOpen()
            if (r7 != 0) goto L_0x00d2
            goto L_0x010b
        L_0x00d2:
            java.nio.ByteBuffer r7 = java.nio.ByteBuffer.allocate(r2)
            r0.position(r4)     // Catch:{ IOException -> 0x00fc }
            int r0 = r0.read(r7)     // Catch:{ IOException -> 0x00fc }
            if (r0 == r2) goto L_0x00f4
            r7 = -1
            if (r0 == r7) goto L_0x0116
            j.c.a.a.g.a.r4 r7 = r11.f2073i     // Catch:{ IOException -> 0x00fc }
            j.c.a.a.g.a.n3 r7 = r7.a()     // Catch:{ IOException -> 0x00fc }
            j.c.a.a.g.a.p3 r7 = r7.f2047i     // Catch:{ IOException -> 0x00fc }
            java.lang.String r8 = "Unexpected data length. Bytes read"
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ IOException -> 0x00fc }
            r7.a(r8, r0)     // Catch:{ IOException -> 0x00fc }
            goto L_0x0116
        L_0x00f4:
            r7.flip()     // Catch:{ IOException -> 0x00fc }
            int r0 = r7.getInt()     // Catch:{ IOException -> 0x00fc }
            goto L_0x0117
        L_0x00fc:
            r0 = move-exception
            j.c.a.a.g.a.r4 r7 = r11.f2073i
            j.c.a.a.g.a.n3 r7 = r7.a()
            j.c.a.a.g.a.p3 r7 = r7.f2046f
            java.lang.String r8 = "Failed to read from channel"
            r7.a(r8, r0)
            goto L_0x0116
        L_0x010b:
            j.c.a.a.g.a.r4 r0 = r11.f2073i
            j.c.a.a.g.a.n3 r0 = r0.a()
            j.c.a.a.g.a.p3 r0 = r0.f2046f
            r0.a(r6)
        L_0x0116:
            r0 = 0
        L_0x0117:
            j.c.a.a.g.a.r4 r7 = r11.f2073i
            j.c.a.a.g.a.g3 r7 = r7.t()
            r7.w()
            int r7 = r7.f1991e
            r11.r()
            if (r0 <= r7) goto L_0x013e
            j.c.a.a.g.a.r4 r2 = r11.f2073i
            j.c.a.a.g.a.n3 r2 = r2.a()
            j.c.a.a.g.a.p3 r2 = r2.f2046f
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            java.lang.Integer r3 = java.lang.Integer.valueOf(r7)
            java.lang.String r4 = "Panic: can't downgrade version. Previous, current version"
            r2.a(r4, r0, r3)
            goto L_0x01c9
        L_0x013e:
            if (r0 >= r7) goto L_0x01c9
            java.nio.channels.FileChannel r8 = r11.u
            r11.r()
            if (r8 == 0) goto L_0x0191
            boolean r9 = r8.isOpen()
            if (r9 != 0) goto L_0x014e
            goto L_0x0191
        L_0x014e:
            java.nio.ByteBuffer r2 = java.nio.ByteBuffer.allocate(r2)
            r2.putInt(r7)
            r2.flip()
            r8.truncate(r4)     // Catch:{ IOException -> 0x0182 }
            r8.write(r2)     // Catch:{ IOException -> 0x0182 }
            r8.force(r1)     // Catch:{ IOException -> 0x0182 }
            long r4 = r8.size()     // Catch:{ IOException -> 0x0182 }
            r9 = 4
            int r2 = (r4 > r9 ? 1 : (r4 == r9 ? 0 : -1))
            if (r2 == 0) goto L_0x0180
            j.c.a.a.g.a.r4 r2 = r11.f2073i     // Catch:{ IOException -> 0x0182 }
            j.c.a.a.g.a.n3 r2 = r2.a()     // Catch:{ IOException -> 0x0182 }
            j.c.a.a.g.a.p3 r2 = r2.f2046f     // Catch:{ IOException -> 0x0182 }
            java.lang.String r4 = "Error writing to channel. Bytes written"
            long r5 = r8.size()     // Catch:{ IOException -> 0x0182 }
            java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ IOException -> 0x0182 }
            r2.a(r4, r5)     // Catch:{ IOException -> 0x0182 }
        L_0x0180:
            r3 = 1
            goto L_0x019c
        L_0x0182:
            r2 = move-exception
            j.c.a.a.g.a.r4 r4 = r11.f2073i
            j.c.a.a.g.a.n3 r4 = r4.a()
            j.c.a.a.g.a.p3 r4 = r4.f2046f
            java.lang.String r5 = "Failed to write to channel"
            r4.a(r5, r2)
            goto L_0x019c
        L_0x0191:
            j.c.a.a.g.a.r4 r2 = r11.f2073i
            j.c.a.a.g.a.n3 r2 = r2.a()
            j.c.a.a.g.a.p3 r2 = r2.f2046f
            r2.a(r6)
        L_0x019c:
            if (r3 == 0) goto L_0x01b4
            j.c.a.a.g.a.r4 r2 = r11.f2073i
            j.c.a.a.g.a.n3 r2 = r2.a()
            j.c.a.a.g.a.p3 r2 = r2.f2052n
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            java.lang.Integer r3 = java.lang.Integer.valueOf(r7)
            java.lang.String r4 = "Storage version upgraded. Previous, current version"
            r2.a(r4, r0, r3)
            goto L_0x01c9
        L_0x01b4:
            j.c.a.a.g.a.r4 r2 = r11.f2073i
            j.c.a.a.g.a.n3 r2 = r2.a()
            j.c.a.a.g.a.p3 r2 = r2.f2046f
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            java.lang.Integer r3 = java.lang.Integer.valueOf(r7)
            java.lang.String r4 = "Storage version upgrade failed. Previous, current version"
            r2.a(r4, r0, r3)
        L_0x01c9:
            boolean r0 = r11.f2075k
            if (r0 != 0) goto L_0x01eb
            j.c.a.a.g.a.r4 r0 = r11.f2073i
            j.c.a.a.g.a.i9 r0 = r0.g
            j.c.a.a.g.a.b3<java.lang.Boolean> r2 = j.c.a.a.g.a.k.l0
            boolean r0 = r0.a(r2)
            if (r0 != 0) goto L_0x01eb
            j.c.a.a.g.a.r4 r0 = r11.f2073i
            j.c.a.a.g.a.n3 r0 = r0.a()
            j.c.a.a.g.a.p3 r0 = r0.f2050l
            java.lang.String r2 = "This instance being marked as an uploader"
            r0.a(r2)
            r11.f2075k = r1
            r11.u()
        L_0x01eb:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.q8.o():void");
    }

    public final u3 p() {
        u3 u3Var = this.d;
        if (u3Var != null) {
            return u3Var;
        }
        throw new IllegalStateException("Network broadcast receiver not created");
    }

    public final n8 q() {
        a(this.f2071e);
        return this.f2071e;
    }

    public final void r() {
        this.f2073i.i().d();
    }

    public final long s() {
        if (((b) this.f2073i.f2092n) != null) {
            long currentTimeMillis = System.currentTimeMillis();
            w3 l2 = this.f2073i.l();
            l2.o();
            l2.d();
            long a2 = l2.f2113i.a();
            if (a2 == 0) {
                a2 = 1 + ((long) l2.k().u().nextInt(86400000));
                l2.f2113i.a(a2);
            }
            return ((((currentTimeMillis + a2) / 1000) / 60) / 60) / 24;
        }
        throw null;
    }

    public final boolean t() {
        r();
        m();
        return ((e().b("select count(1) > 0 from raw_events", null) > 0 ? 1 : (e().b("select count(1) > 0 from raw_events", null) == 0 ? 0 : -1)) != 0) || !TextUtils.isEmpty(e().w());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00bf  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00fb  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0141  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0144  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x01bf  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x01dd  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void u() {
        /*
            r20 = this;
            r0 = r20
            r20.r()
            r20.m()
            r20.r()
            r20.m()
            boolean r1 = r0.f2075k
            if (r1 != 0) goto L_0x001f
            j.c.a.a.g.a.r4 r1 = r0.f2073i
            j.c.a.a.g.a.i9 r1 = r1.g
            j.c.a.a.g.a.b3<java.lang.Boolean> r2 = j.c.a.a.g.a.k.l0
            boolean r1 = r1.a(r2)
            if (r1 != 0) goto L_0x001f
            return
        L_0x001f:
            long r1 = r0.f2077m
            r3 = 0
            r4 = 0
            int r6 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r6 <= 0) goto L_0x0067
            j.c.a.a.g.a.r4 r1 = r0.f2073i
            j.c.a.a.c.n.a r1 = r1.f2092n
            j.c.a.a.c.n.b r1 = (j.c.a.a.c.n.b) r1
            if (r1 == 0) goto L_0x0066
            long r1 = android.os.SystemClock.elapsedRealtime()
            r6 = 3600000(0x36ee80, double:1.7786363E-317)
            long r8 = r0.f2077m
            long r1 = r1 - r8
            long r1 = java.lang.Math.abs(r1)
            long r6 = r6 - r1
            int r1 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r1 <= 0) goto L_0x0063
            j.c.a.a.g.a.r4 r1 = r0.f2073i
            j.c.a.a.g.a.n3 r1 = r1.a()
            j.c.a.a.g.a.p3 r1 = r1.f2052n
            java.lang.Long r2 = java.lang.Long.valueOf(r6)
            java.lang.String r3 = "Upload has been suspended. Will update scheduling later in approximately ms"
            r1.a(r3, r2)
            j.c.a.a.g.a.u3 r1 = r20.p()
            r1.a()
            j.c.a.a.g.a.n8 r1 = r20.q()
            r1.u()
            return
        L_0x0063:
            r0.f2077m = r4
            goto L_0x0067
        L_0x0066:
            throw r3
        L_0x0067:
            j.c.a.a.g.a.r4 r1 = r0.f2073i
            boolean r1 = r1.e()
            if (r1 == 0) goto L_0x03ba
            boolean r1 = r20.t()
            if (r1 != 0) goto L_0x0077
            goto L_0x03ba
        L_0x0077:
            j.c.a.a.g.a.r4 r1 = r0.f2073i
            j.c.a.a.c.n.a r1 = r1.f2092n
            j.c.a.a.c.n.b r1 = (j.c.a.a.c.n.b) r1
            if (r1 == 0) goto L_0x03b9
            long r1 = java.lang.System.currentTimeMillis()
            j.c.a.a.g.a.b3<java.lang.Long> r6 = j.c.a.a.g.a.k.F
            java.lang.Object r6 = r6.a(r3)
            java.lang.Long r6 = (java.lang.Long) r6
            long r6 = r6.longValue()
            long r6 = java.lang.Math.max(r4, r6)
            j.c.a.a.g.a.n9 r8 = r20.e()
            java.lang.String r9 = "select count(1) > 0 from raw_events where realtime = 1"
            long r8 = r8.b(r9, r3)
            int r12 = (r8 > r4 ? 1 : (r8 == r4 ? 0 : -1))
            if (r12 == 0) goto L_0x00a3
            r8 = 1
            goto L_0x00a4
        L_0x00a3:
            r8 = 0
        L_0x00a4:
            if (r8 != 0) goto L_0x00bc
            j.c.a.a.g.a.n9 r8 = r20.e()
            java.lang.String r9 = "select count(1) > 0 from queue where has_realtime = 1"
            long r8 = r8.b(r9, r3)
            int r12 = (r8 > r4 ? 1 : (r8 == r4 ? 0 : -1))
            if (r12 == 0) goto L_0x00b6
            r8 = 1
            goto L_0x00b7
        L_0x00b6:
            r8 = 0
        L_0x00b7:
            if (r8 == 0) goto L_0x00ba
            goto L_0x00bc
        L_0x00ba:
            r8 = 0
            goto L_0x00bd
        L_0x00bc:
            r8 = 1
        L_0x00bd:
            if (r8 == 0) goto L_0x00fb
            j.c.a.a.g.a.r4 r9 = r0.f2073i
            j.c.a.a.g.a.i9 r9 = r9.g
            java.lang.String r12 = "debug.firebase.analytics.app"
            java.lang.String r13 = ""
            java.lang.String r9 = r9.a(r12, r13)
            boolean r12 = android.text.TextUtils.isEmpty(r9)
            if (r12 != 0) goto L_0x00ea
            java.lang.String r12 = ".none."
            boolean r9 = r12.equals(r9)
            if (r9 != 0) goto L_0x00ea
            j.c.a.a.g.a.b3<java.lang.Long> r9 = j.c.a.a.g.a.k.A
            java.lang.Object r9 = r9.a(r3)
            java.lang.Long r9 = (java.lang.Long) r9
            long r12 = r9.longValue()
            long r12 = java.lang.Math.max(r4, r12)
            goto L_0x010b
        L_0x00ea:
            j.c.a.a.g.a.b3<java.lang.Long> r9 = j.c.a.a.g.a.k.z
            java.lang.Object r9 = r9.a(r3)
            java.lang.Long r9 = (java.lang.Long) r9
            long r12 = r9.longValue()
            long r12 = java.lang.Math.max(r4, r12)
            goto L_0x010b
        L_0x00fb:
            j.c.a.a.g.a.b3<java.lang.Long> r9 = j.c.a.a.g.a.k.y
            java.lang.Object r9 = r9.a(r3)
            java.lang.Long r9 = (java.lang.Long) r9
            long r12 = r9.longValue()
            long r12 = java.lang.Math.max(r4, r12)
        L_0x010b:
            j.c.a.a.g.a.r4 r9 = r0.f2073i
            j.c.a.a.g.a.w3 r9 = r9.l()
            j.c.a.a.g.a.b4 r9 = r9.f2111e
            long r14 = r9.a()
            j.c.a.a.g.a.r4 r9 = r0.f2073i
            j.c.a.a.g.a.w3 r9 = r9.l()
            j.c.a.a.g.a.b4 r9 = r9.f2112f
            long r16 = r9.a()
            j.c.a.a.g.a.n9 r9 = r20.e()
            java.lang.String r11 = "select max(bundle_end_timestamp) from queue"
            long r10 = r9.a(r11, r3, r4)
            j.c.a.a.g.a.n9 r9 = r20.e()
            java.lang.String r0 = "select max(timestamp) from raw_events"
            r18 = r12
            long r12 = r9.a(r0, r3, r4)
            long r9 = java.lang.Math.max(r10, r12)
            int r0 = (r9 > r4 ? 1 : (r9 == r4 ? 0 : -1))
            if (r0 != 0) goto L_0x0144
            r6 = r4
            goto L_0x01ba
        L_0x0144:
            long r9 = r9 - r1
            long r9 = java.lang.Math.abs(r9)
            long r9 = r1 - r9
            long r14 = r14 - r1
            long r11 = java.lang.Math.abs(r14)
            long r11 = r1 - r11
            long r16 = r16 - r1
            long r13 = java.lang.Math.abs(r16)
            long r1 = r1 - r13
            long r11 = java.lang.Math.max(r11, r1)
            long r6 = r6 + r9
            if (r8 == 0) goto L_0x016a
            int r0 = (r11 > r4 ? 1 : (r11 == r4 ? 0 : -1))
            if (r0 <= 0) goto L_0x016a
            long r6 = java.lang.Math.min(r9, r11)
            long r6 = r6 + r18
        L_0x016a:
            j.c.a.a.g.a.u8 r0 = r20.k()
            r13 = r18
            boolean r0 = r0.a(r11, r13)
            if (r0 != 0) goto L_0x0178
            long r6 = r11 + r13
        L_0x0178:
            int r0 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r0 == 0) goto L_0x01ba
            int r0 = (r1 > r9 ? 1 : (r1 == r9 ? 0 : -1))
            if (r0 < 0) goto L_0x01ba
            r0 = 0
        L_0x0181:
            r8 = 20
            j.c.a.a.g.a.b3<java.lang.Integer> r9 = j.c.a.a.g.a.k.H
            java.lang.Object r9 = r9.a(r3)
            java.lang.Integer r9 = (java.lang.Integer) r9
            int r9 = r9.intValue()
            r10 = 0
            int r9 = java.lang.Math.max(r10, r9)
            int r8 = java.lang.Math.min(r8, r9)
            if (r0 >= r8) goto L_0x01b8
            r8 = 1
            long r8 = r8 << r0
            j.c.a.a.g.a.b3<java.lang.Long> r11 = j.c.a.a.g.a.k.G
            java.lang.Object r11 = r11.a(r3)
            java.lang.Long r11 = (java.lang.Long) r11
            long r11 = r11.longValue()
            long r11 = java.lang.Math.max(r4, r11)
            long r11 = r11 * r8
            long r6 = r6 + r11
            int r8 = (r6 > r1 ? 1 : (r6 == r1 ? 0 : -1))
            if (r8 <= 0) goto L_0x01b5
            goto L_0x01bb
        L_0x01b5:
            int r0 = r0 + 1
            goto L_0x0181
        L_0x01b8:
            r6 = r4
            goto L_0x01bb
        L_0x01ba:
            r10 = 0
        L_0x01bb:
            int r0 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r0 != 0) goto L_0x01dd
            r0 = r20
            j.c.a.a.g.a.r4 r1 = r0.f2073i
            j.c.a.a.g.a.n3 r1 = r1.a()
            j.c.a.a.g.a.p3 r1 = r1.f2052n
            java.lang.String r2 = "Next upload time is 0"
            r1.a(r2)
            j.c.a.a.g.a.u3 r1 = r20.p()
            r1.a()
            j.c.a.a.g.a.n8 r1 = r20.q()
            r1.u()
            return
        L_0x01dd:
            r0 = r20
            j.c.a.a.g.a.r3 r1 = r20.d()
            boolean r1 = r1.u()
            if (r1 != 0) goto L_0x0247
            j.c.a.a.g.a.r4 r1 = r0.f2073i
            j.c.a.a.g.a.n3 r1 = r1.a()
            j.c.a.a.g.a.p3 r1 = r1.f2052n
            java.lang.String r2 = "No network"
            r1.a(r2)
            j.c.a.a.g.a.u3 r1 = r20.p()
            j.c.a.a.g.a.q8 r2 = r1.a
            r2.m()
            j.c.a.a.g.a.q8 r2 = r1.a
            j.c.a.a.g.a.l4 r2 = r2.i()
            r2.d()
            boolean r2 = r1.b
            if (r2 == 0) goto L_0x020d
            goto L_0x023f
        L_0x020d:
            j.c.a.a.g.a.q8 r2 = r1.a
            j.c.a.a.g.a.r4 r2 = r2.f2073i
            android.content.Context r2 = r2.a
            android.content.IntentFilter r3 = new android.content.IntentFilter
            java.lang.String r4 = "android.net.conn.CONNECTIVITY_CHANGE"
            r3.<init>(r4)
            r2.registerReceiver(r1, r3)
            j.c.a.a.g.a.q8 r2 = r1.a
            j.c.a.a.g.a.r3 r2 = r2.d()
            boolean r2 = r2.u()
            r1.c = r2
            j.c.a.a.g.a.q8 r2 = r1.a
            j.c.a.a.g.a.n3 r2 = r2.a()
            j.c.a.a.g.a.p3 r2 = r2.f2052n
            boolean r3 = r1.c
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r3)
            java.lang.String r4 = "Registering connectivity change receiver. Network connected"
            r2.a(r4, r3)
            r2 = 1
            r1.b = r2
        L_0x023f:
            j.c.a.a.g.a.n8 r1 = r20.q()
            r1.u()
            return
        L_0x0247:
            j.c.a.a.g.a.r4 r1 = r0.f2073i
            j.c.a.a.g.a.w3 r1 = r1.l()
            j.c.a.a.g.a.b4 r1 = r1.g
            long r1 = r1.a()
            j.c.a.a.g.a.b3<java.lang.Long> r8 = j.c.a.a.g.a.k.w
            java.lang.Object r8 = r8.a(r3)
            java.lang.Long r8 = (java.lang.Long) r8
            long r8 = r8.longValue()
            long r8 = java.lang.Math.max(r4, r8)
            j.c.a.a.g.a.u8 r11 = r20.k()
            boolean r11 = r11.a(r1, r8)
            if (r11 != 0) goto L_0x0272
            long r1 = r1 + r8
            long r6 = java.lang.Math.max(r6, r1)
        L_0x0272:
            j.c.a.a.g.a.u3 r1 = r20.p()
            r1.a()
            j.c.a.a.g.a.r4 r1 = r0.f2073i
            j.c.a.a.c.n.a r1 = r1.f2092n
            j.c.a.a.c.n.b r1 = (j.c.a.a.c.n.b) r1
            if (r1 == 0) goto L_0x03b8
            long r1 = java.lang.System.currentTimeMillis()
            long r6 = r6 - r1
            int r1 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r1 > 0) goto L_0x02b3
            j.c.a.a.g.a.b3<java.lang.Long> r1 = j.c.a.a.g.a.k.B
            java.lang.Object r1 = r1.a(r3)
            java.lang.Long r1 = (java.lang.Long) r1
            long r1 = r1.longValue()
            long r6 = java.lang.Math.max(r4, r1)
            j.c.a.a.g.a.r4 r1 = r0.f2073i
            j.c.a.a.g.a.w3 r1 = r1.l()
            j.c.a.a.g.a.b4 r1 = r1.f2111e
            j.c.a.a.g.a.r4 r2 = r0.f2073i
            j.c.a.a.c.n.a r2 = r2.f2092n
            j.c.a.a.c.n.b r2 = (j.c.a.a.c.n.b) r2
            if (r2 == 0) goto L_0x02b2
            long r8 = java.lang.System.currentTimeMillis()
            r1.a(r8)
            goto L_0x02b3
        L_0x02b2:
            throw r3
        L_0x02b3:
            j.c.a.a.g.a.r4 r1 = r0.f2073i
            j.c.a.a.g.a.n3 r1 = r1.a()
            j.c.a.a.g.a.p3 r1 = r1.f2052n
            java.lang.Long r2 = java.lang.Long.valueOf(r6)
            java.lang.String r8 = "Upload scheduled in approximately ms"
            r1.a(r8, r2)
            j.c.a.a.g.a.n8 r1 = r20.q()
            r1.o()
            j.c.a.a.g.a.r4 r2 = r1.a
            j.c.a.a.g.a.h9 r8 = r2.f2086f
            android.content.Context r2 = r2.a
            boolean r8 = j.c.a.a.g.a.i4.a(r2)
            if (r8 != 0) goto L_0x02e2
            j.c.a.a.g.a.n3 r8 = r1.a()
            j.c.a.a.g.a.p3 r8 = r8.f2051m
            java.lang.String r9 = "Receiver not registered/enabled"
            r8.a(r9)
        L_0x02e2:
            boolean r2 = j.c.a.a.g.a.y8.a(r2)
            if (r2 != 0) goto L_0x02f3
            j.c.a.a.g.a.n3 r2 = r1.a()
            j.c.a.a.g.a.p3 r2 = r2.f2051m
            java.lang.String r8 = "Service not registered/enabled"
            r2.a(r8)
        L_0x02f3:
            r1.u()
            j.c.a.a.g.a.r4 r2 = r1.a
            j.c.a.a.c.n.a r2 = r2.f2092n
            j.c.a.a.c.n.b r2 = (j.c.a.a.c.n.b) r2
            if (r2 == 0) goto L_0x03b7
            long r8 = android.os.SystemClock.elapsedRealtime()
            long r13 = r8 + r6
            j.c.a.a.g.a.b3<java.lang.Long> r2 = j.c.a.a.g.a.k.C
            java.lang.Object r2 = r2.a(r3)
            java.lang.Long r2 = (java.lang.Long) r2
            long r8 = r2.longValue()
            long r8 = java.lang.Math.max(r4, r8)
            int r2 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r2 >= 0) goto L_0x0333
            j.c.a.a.g.a.b r2 = r1.f2055e
            long r8 = r2.c
            int r2 = (r8 > r4 ? 1 : (r8 == r4 ? 0 : -1))
            if (r2 == 0) goto L_0x0321
            r10 = 1
        L_0x0321:
            if (r10 != 0) goto L_0x0333
            j.c.a.a.g.a.n3 r2 = r1.a()
            j.c.a.a.g.a.p3 r2 = r2.f2052n
            java.lang.String r4 = "Scheduling upload with DelayedRunnable"
            r2.a(r4)
            j.c.a.a.g.a.b r2 = r1.f2055e
            r2.a(r6)
        L_0x0333:
            j.c.a.a.g.a.r4 r2 = r1.a
            j.c.a.a.g.a.h9 r2 = r2.f2086f
            int r2 = android.os.Build.VERSION.SDK_INT
            r4 = 24
            if (r2 < r4) goto L_0x0391
            j.c.a.a.g.a.n3 r2 = r1.a()
            j.c.a.a.g.a.p3 r2 = r2.f2052n
            java.lang.String r3 = "Scheduling upload with JobScheduler"
            r2.a(r3)
            j.c.a.a.g.a.r4 r2 = r1.a
            android.content.Context r2 = r2.a
            android.content.ComponentName r3 = new android.content.ComponentName
            java.lang.String r4 = "com.google.android.gms.measurement.AppMeasurementJobService"
            r3.<init>(r2, r4)
            int r4 = r1.w()
            android.os.PersistableBundle r5 = new android.os.PersistableBundle
            r5.<init>()
            java.lang.String r8 = "action"
            java.lang.String r9 = "com.google.android.gms.measurement.UPLOAD"
            r5.putString(r8, r9)
            android.app.job.JobInfo$Builder r8 = new android.app.job.JobInfo$Builder
            r8.<init>(r4, r3)
            android.app.job.JobInfo$Builder r3 = r8.setMinimumLatency(r6)
            r8 = 1
            long r6 = r6 << r8
            android.app.job.JobInfo$Builder r3 = r3.setOverrideDeadline(r6)
            android.app.job.JobInfo$Builder r3 = r3.setExtras(r5)
            android.app.job.JobInfo r3 = r3.build()
            j.c.a.a.g.a.n3 r1 = r1.a()
            j.c.a.a.g.a.p3 r1 = r1.f2052n
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            java.lang.String r5 = "Scheduling job. JobID"
            r1.a(r5, r4)
            java.lang.String r1 = "com.google.android.gms"
            java.lang.String r4 = "UploadAlarm"
            j.c.a.a.f.e.l6.a(r2, r3, r1, r4)
            goto L_0x03b6
        L_0x0391:
            j.c.a.a.g.a.n3 r2 = r1.a()
            j.c.a.a.g.a.p3 r2 = r2.f2052n
            java.lang.String r4 = "Scheduling upload with AlarmManager"
            r2.a(r4)
            android.app.AlarmManager r11 = r1.d
            r12 = 2
            j.c.a.a.g.a.b3<java.lang.Long> r2 = j.c.a.a.g.a.k.x
            java.lang.Object r2 = r2.a(r3)
            java.lang.Long r2 = (java.lang.Long) r2
            long r2 = r2.longValue()
            long r15 = java.lang.Math.max(r2, r6)
            android.app.PendingIntent r17 = r1.x()
            r11.setInexactRepeating(r12, r13, r15, r17)
        L_0x03b6:
            return
        L_0x03b7:
            throw r3
        L_0x03b8:
            throw r3
        L_0x03b9:
            throw r3
        L_0x03ba:
            j.c.a.a.g.a.r4 r1 = r0.f2073i
            j.c.a.a.g.a.n3 r1 = r1.a()
            j.c.a.a.g.a.p3 r1 = r1.f2052n
            java.lang.String r2 = "Nothing to upload or uploading impossible"
            r1.a(r2)
            j.c.a.a.g.a.u3 r1 = r20.p()
            r1.a()
            j.c.a.a.g.a.n8 r1 = r20.q()
            r1.u()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.q8.u():void");
    }

    /* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
    public class a {
        public u0 a;
        public List<Long> b;
        public List<q0> c;
        public long d;

        public /* synthetic */ a(q8 q8Var, p8 p8Var) {
        }

        public final void a(u0 u0Var) {
            ResourcesFlusher.b(u0Var);
            this.a = u0Var;
        }

        public final boolean a(long j2, q0 q0Var) {
            ResourcesFlusher.b(q0Var);
            if (this.c == null) {
                this.c = new ArrayList();
            }
            if (this.b == null) {
                this.b = new ArrayList();
            }
            if (this.c.size() > 0 && ((this.c.get(0).zzf / 1000) / 60) / 60 != ((q0Var.zzf / 1000) / 60) / 60) {
                return false;
            }
            long c2 = this.d + ((long) q0Var.c());
            if (c2 >= ((long) Math.max(0, k.f2016n.a(null).intValue()))) {
                return false;
            }
            this.d = c2;
            this.c.add(q0Var);
            this.b.add(Long.valueOf(j2));
            if (this.c.size() >= Math.max(1, k.f2017o.a(null).intValue())) {
                return false;
            }
            return true;
        }
    }

    public static void a(o8 o8Var) {
        if (o8Var == null) {
            throw new IllegalStateException("Upload Component not created");
        } else if (!o8Var.c) {
            String valueOf = String.valueOf(o8Var.getClass());
            StringBuilder sb = new StringBuilder(valueOf.length() + 27);
            sb.append("Component not initialized: ");
            sb.append(valueOf);
            throw new IllegalStateException(sb.toString());
        }
    }

    public final n3 a() {
        return this.f2073i.a();
    }

    public final void a(i iVar, d9 d9Var) {
        List<g9> list;
        List<g9> list2;
        List<g9> list3;
        List<String> list4;
        i iVar2 = iVar;
        d9 d9Var2 = d9Var;
        ResourcesFlusher.b(d9Var);
        ResourcesFlusher.b(d9Var2.b);
        r();
        m();
        String str = d9Var2.b;
        long j2 = iVar2.f2007e;
        if (k().a(iVar2, d9Var2)) {
            if (!d9Var2.f1953i) {
                b(d9Var2);
                return;
            }
            if (this.f2073i.g.d(str, k.v0) && (list4 = d9Var2.v) != null) {
                if (list4.contains(iVar2.b)) {
                    Bundle b2 = iVar2.c.b();
                    b2.putLong("ga_safelisted", 1);
                    iVar2 = new i(iVar2.b, new h(b2), iVar2.d, iVar2.f2007e);
                } else {
                    this.f2073i.a().f2051m.a("Dropping non-safelisted event. appId, event name, origin", str, iVar2.b, iVar2.d);
                    return;
                }
            }
            e().z();
            try {
                n9 e2 = e();
                ResourcesFlusher.b(str);
                e2.d();
                e2.o();
                int i2 = (j2 > 0 ? 1 : (j2 == 0 ? 0 : -1));
                if (i2 < 0) {
                    e2.a().f2047i.a("Invalid time querying timed out conditional properties", n3.a(str), Long.valueOf(j2));
                    list = Collections.emptyList();
                } else {
                    list = e2.a("active=0 and app_id=? and abs(? - creation_timestamp) > trigger_timeout", new String[]{str, String.valueOf(j2)});
                }
                for (g9 g9Var : list) {
                    if (g9Var != null) {
                        this.f2073i.a().f2051m.a("User property timed out", g9Var.b, this.f2073i.o().c(g9Var.d.c), g9Var.d.a());
                        if (g9Var.h != null) {
                            b(new i(g9Var.h, j2), d9Var2);
                        }
                        e().e(str, g9Var.d.c);
                    }
                }
                n9 e3 = e();
                ResourcesFlusher.b(str);
                e3.d();
                e3.o();
                if (i2 < 0) {
                    e3.a().f2047i.a("Invalid time querying expired conditional properties", n3.a(str), Long.valueOf(j2));
                    list2 = Collections.emptyList();
                } else {
                    list2 = e3.a("active<>0 and app_id=? and abs(? - triggered_timestamp) > time_to_live", new String[]{str, String.valueOf(j2)});
                }
                ArrayList arrayList = new ArrayList(list2.size());
                for (g9 g9Var2 : list2) {
                    if (g9Var2 != null) {
                        this.f2073i.a().f2051m.a("User property expired", g9Var2.b, this.f2073i.o().c(g9Var2.d.c), g9Var2.d.a());
                        e().b(str, g9Var2.d.c);
                        if (g9Var2.f2003l != null) {
                            arrayList.add(g9Var2.f2003l);
                        }
                        e().e(str, g9Var2.d.c);
                    }
                }
                int size = arrayList.size();
                int i3 = 0;
                while (i3 < size) {
                    Object obj = arrayList.get(i3);
                    i3++;
                    b(new i((i) obj, j2), d9Var2);
                }
                n9 e4 = e();
                String str2 = iVar2.b;
                ResourcesFlusher.b(str);
                ResourcesFlusher.b(str2);
                e4.d();
                e4.o();
                if (i2 < 0) {
                    e4.a().f2047i.a("Invalid time querying triggered conditional properties", n3.a(str), e4.g().a(str2), Long.valueOf(j2));
                    list3 = Collections.emptyList();
                } else {
                    list3 = e4.a("active=0 and app_id=? and trigger_event_name=? and abs(? - creation_timestamp) <= trigger_timeout", new String[]{str, str2, String.valueOf(j2)});
                }
                ArrayList arrayList2 = new ArrayList(list3.size());
                for (g9 g9Var3 : list3) {
                    if (g9Var3 != null) {
                        x8 x8Var = g9Var3.d;
                        z8 z8Var = r4;
                        z8 z8Var2 = new z8(g9Var3.b, g9Var3.c, x8Var.c, j2, x8Var.a());
                        if (e().a(z8Var)) {
                            this.f2073i.a().f2051m.a("User property triggered", g9Var3.b, this.f2073i.o().c(z8Var.c), z8Var.f2143e);
                        } else {
                            this.f2073i.a().f2046f.a("Too many active user properties, ignoring", n3.a(g9Var3.b), this.f2073i.o().c(z8Var.c), z8Var.f2143e);
                        }
                        if (g9Var3.f2001j != null) {
                            arrayList2.add(g9Var3.f2001j);
                        }
                        g9Var3.d = new x8(z8Var);
                        g9Var3.f1999f = true;
                        e().a(g9Var3);
                    }
                }
                b(iVar2, d9Var2);
                int size2 = arrayList2.size();
                int i4 = 0;
                while (i4 < size2) {
                    Object obj2 = arrayList2.get(i4);
                    i4++;
                    b(new i((i) obj2, j2), d9Var2);
                }
                e().u();
            } finally {
                e().A();
            }
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:51:0x00e7 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:57:0x0126 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:74:0x01b0 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:42:0x00d0 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:24:0x007d */
    /* JADX WARN: Type inference failed for: r4v0 */
    /* JADX WARN: Type inference failed for: r4v99, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r4v103 */
    /* JADX WARN: Type inference failed for: r4v107 */
    /* JADX WARN: Type inference failed for: r4v108 */
    /* JADX WARN: Type inference failed for: r4v109 */
    /* JADX WARN: Type inference failed for: r4v110 */
    /* JADX WARN: Type inference failed for: r4v111, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r4v112, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r4v117, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r4v118 */
    /* JADX WARN: Type inference failed for: r4v124, types: [android.database.Cursor] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.g.a.q8.a(j.c.a.a.f.e.u0$a, long, boolean):void
     arg types: [j.c.a.a.f.e.u0$a, long, int]
     candidates:
      j.c.a.a.g.a.q8.a(j.c.a.a.f.e.q0$a, int, java.lang.String):void
      j.c.a.a.g.a.q8.a(j.c.a.a.f.e.u0$a, long, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.g.a.u8.a(j.c.a.a.f.e.q0$a, java.lang.String, java.lang.Object):void
     arg types: [j.c.a.a.f.e.q0$a, java.lang.String, long]
     candidates:
      j.c.a.a.g.a.u8.a(boolean, boolean, boolean):java.lang.String
      j.c.a.a.g.a.u8.a(java.lang.StringBuilder, int, j.c.a.a.f.e.b0):void
      j.c.a.a.g.a.u8.a(j.c.a.a.f.e.q0$a, java.lang.String, java.lang.Object):void */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0044, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0045, code lost:
        r2 = r0;
        r4 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0048, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0049, code lost:
        r6 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0058, code lost:
        r7 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:479:0x0ca1, code lost:
        if (java.lang.Math.abs(r7.p() - r11.g) >= 86400000) goto L_0x0ca3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x021d, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x021e, code lost:
        r6 = r0;
        r7 = r4;
        r4 = r8;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x0243 A[SYNTHETIC, Splitter:B:110:0x0243] */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x024a A[Catch:{ IOException -> 0x0206, all -> 0x0f0c }] */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x0258 A[Catch:{ IOException -> 0x0206, all -> 0x0f0c }] */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0044 A[ExcHandler: all (r0v10 'th' java.lang.Throwable A[CUSTOM_DECLARE]), PHI: r4 10  PHI: (r4v118 ?) = (r4v110 ?), (r4v111 ?), (r4v111 ?), (r4v111 ?), (r4v111 ?), (r4v112 ?), (r4v112 ?), (r4v112 ?), (r4v117 ?), (r4v117 ?), (r4v117 ?), (r4v124 ?), (r4v124 ?), (r4v124 ?), (r4v124 ?), (r4v0 ?), (r4v0 ?) binds: [B:51:0x00e7, B:57:0x0126, B:94:0x0208, B:95:?, B:61:0x0132, B:74:0x01b0, B:86:0x01eb, B:79:0x01c8, B:42:0x00d0, B:47:0x00db, B:48:?, B:24:0x007d, B:29:0x0088, B:31:0x008c, B:32:?, B:10:0x0035, B:11:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:10:0x0035] */
    /* JADX WARNING: Removed duplicated region for block: B:163:0x0395 A[Catch:{ IOException -> 0x0206, all -> 0x0f0c }] */
    /* JADX WARNING: Removed duplicated region for block: B:170:0x03a0 A[Catch:{ IOException -> 0x0206, all -> 0x0f0c }] */
    /* JADX WARNING: Removed duplicated region for block: B:171:0x03a1 A[Catch:{ IOException -> 0x0206, all -> 0x0f0c }] */
    /* JADX WARNING: Removed duplicated region for block: B:178:0x03c0 A[SYNTHETIC, Splitter:B:178:0x03c0] */
    /* JADX WARNING: Removed duplicated region for block: B:191:0x045a A[Catch:{ IOException -> 0x0206, all -> 0x0f0c }] */
    /* JADX WARNING: Removed duplicated region for block: B:194:0x04b9 A[Catch:{ IOException -> 0x0206, all -> 0x0f0c }] */
    /* JADX WARNING: Removed duplicated region for block: B:195:0x04bd A[Catch:{ IOException -> 0x0206, all -> 0x0f0c }] */
    /* JADX WARNING: Removed duplicated region for block: B:204:0x0523 A[Catch:{ IOException -> 0x0206, all -> 0x0f0c }] */
    /* JADX WARNING: Removed duplicated region for block: B:215:0x0553 A[Catch:{ IOException -> 0x0206, all -> 0x0f0c }] */
    /* JADX WARNING: Removed duplicated region for block: B:216:0x0570 A[Catch:{ IOException -> 0x0206, all -> 0x0f0c }] */
    /* JADX WARNING: Removed duplicated region for block: B:259:0x0676 A[Catch:{ IOException -> 0x0206, all -> 0x0f0c }] */
    /* JADX WARNING: Removed duplicated region for block: B:296:0x0783 A[Catch:{ IOException -> 0x0206, all -> 0x0f0c }] */
    /* JADX WARNING: Removed duplicated region for block: B:303:0x079e A[Catch:{ IOException -> 0x0206, all -> 0x0f0c }] */
    /* JADX WARNING: Removed duplicated region for block: B:304:0x07b8 A[Catch:{ IOException -> 0x0206, all -> 0x0f0c }] */
    /* JADX WARNING: Removed duplicated region for block: B:420:0x0aef A[Catch:{ IOException -> 0x0206, all -> 0x0f0c }] */
    /* JADX WARNING: Removed duplicated region for block: B:421:0x0b02 A[Catch:{ IOException -> 0x0206, all -> 0x0f0c }] */
    /* JADX WARNING: Removed duplicated region for block: B:423:0x0b05 A[Catch:{ IOException -> 0x0206, all -> 0x0f0c }] */
    /* JADX WARNING: Removed duplicated region for block: B:424:0x0b2c A[SYNTHETIC, Splitter:B:424:0x0b2c] */
    /* JADX WARNING: Removed duplicated region for block: B:438:0x0bd2 A[Catch:{ all -> 0x0d49 }] */
    /* JADX WARNING: Removed duplicated region for block: B:439:0x0bd4 A[Catch:{ all -> 0x0d49 }] */
    /* JADX WARNING: Removed duplicated region for block: B:443:0x0bdc A[SYNTHETIC, Splitter:B:443:0x0bdc] */
    /* JADX WARNING: Removed duplicated region for block: B:455:0x0c10 A[SYNTHETIC, Splitter:B:455:0x0c10] */
    /* JADX WARNING: Removed duplicated region for block: B:483:0x0ca8 A[Catch:{ all -> 0x0d49 }] */
    /* JADX WARNING: Removed duplicated region for block: B:487:0x0cf2 A[Catch:{ all -> 0x0d49 }] */
    /* JADX WARNING: Removed duplicated region for block: B:562:0x0ef3 A[SYNTHETIC, Splitter:B:562:0x0ef3] */
    /* JADX WARNING: Removed duplicated region for block: B:569:0x0f08 A[SYNTHETIC, Splitter:B:569:0x0f08] */
    /* JADX WARNING: Removed duplicated region for block: B:579:0x0424 A[EDGE_INSN: B:579:0x0424->B:186:0x0424 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(long r53) {
        /*
            r52 = this;
            r1 = r52
            java.lang.String r2 = "_npa"
            j.c.a.a.g.a.n9 r3 = r52.e()
            r3.z()
            j.c.a.a.g.a.q8$a r3 = new j.c.a.a.g.a.q8$a     // Catch:{ all -> 0x0f0c }
            r4 = 0
            r3.<init>(r1, r4)     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.n9 r5 = r52.e()     // Catch:{ all -> 0x0f0c }
            long r6 = r1.x     // Catch:{ all -> 0x0f0c }
            i.b.k.ResourcesFlusher.b(r3)     // Catch:{ all -> 0x0f0c }
            r5.d()     // Catch:{ all -> 0x0f0c }
            r5.o()     // Catch:{ all -> 0x0f0c }
            r9 = -1
            r11 = 2
            r12 = 0
            r13 = 1
            android.database.sqlite.SQLiteDatabase r15 = r5.v()     // Catch:{ SQLiteException -> 0x022a, all -> 0x0225 }
            boolean r14 = android.text.TextUtils.isEmpty(r4)     // Catch:{ SQLiteException -> 0x022a, all -> 0x0225 }
            java.lang.String r16 = ""
            if (r14 == 0) goto L_0x0094
            int r14 = (r6 > r9 ? 1 : (r6 == r9 ? 0 : -1))
            if (r14 == 0) goto L_0x004b
            java.lang.String[] r8 = new java.lang.String[r11]     // Catch:{ SQLiteException -> 0x0048, all -> 0x0044 }
            java.lang.String r17 = java.lang.String.valueOf(r6)     // Catch:{ SQLiteException -> 0x0048, all -> 0x0044 }
            r8[r12] = r17     // Catch:{ SQLiteException -> 0x0048, all -> 0x0044 }
            java.lang.String r17 = java.lang.String.valueOf(r53)     // Catch:{ SQLiteException -> 0x0048, all -> 0x0044 }
            r8[r13] = r17     // Catch:{ SQLiteException -> 0x0048, all -> 0x0044 }
            goto L_0x0053
        L_0x0044:
            r0 = move-exception
            r2 = r0
            goto L_0x0f06
        L_0x0048:
            r0 = move-exception
            r6 = r0
            goto L_0x0058
        L_0x004b:
            java.lang.String[] r8 = new java.lang.String[r13]     // Catch:{ SQLiteException -> 0x022a, all -> 0x0225 }
            java.lang.String r17 = java.lang.String.valueOf(r53)     // Catch:{ SQLiteException -> 0x022a, all -> 0x0225 }
            r8[r12] = r17     // Catch:{ SQLiteException -> 0x022a, all -> 0x0225 }
        L_0x0053:
            if (r14 == 0) goto L_0x005b
            java.lang.String r16 = "rowid <= ? and "
            goto L_0x005b
        L_0x0058:
            r7 = r4
            goto L_0x0230
        L_0x005b:
            r14 = r16
            int r4 = r14.length()     // Catch:{ SQLiteException -> 0x022a, all -> 0x0225 }
            int r4 = r4 + 148
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x022a, all -> 0x0225 }
            r11.<init>(r4)     // Catch:{ SQLiteException -> 0x022a, all -> 0x0225 }
            java.lang.String r4 = "select app_id, metadata_fingerprint from raw_events where "
            r11.append(r4)     // Catch:{ SQLiteException -> 0x022a, all -> 0x0225 }
            r11.append(r14)     // Catch:{ SQLiteException -> 0x022a, all -> 0x0225 }
            java.lang.String r4 = "app_id in (select app_id from apps where config_fetched_time >= ?) order by rowid limit 1;"
            r11.append(r4)     // Catch:{ SQLiteException -> 0x022a, all -> 0x0225 }
            java.lang.String r4 = r11.toString()     // Catch:{ SQLiteException -> 0x022a, all -> 0x0225 }
            android.database.Cursor r4 = r15.rawQuery(r4, r8)     // Catch:{ SQLiteException -> 0x022a, all -> 0x0225 }
            boolean r8 = r4.moveToFirst()     // Catch:{ SQLiteException -> 0x0222, all -> 0x0044 }
            if (r8 != 0) goto L_0x0088
            r4.close()     // Catch:{ all -> 0x0f0c }
            goto L_0x0246
        L_0x0088:
            java.lang.String r8 = r4.getString(r12)     // Catch:{ SQLiteException -> 0x0222, all -> 0x0044 }
            java.lang.String r11 = r4.getString(r13)     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            r4.close()     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            goto L_0x00e3
        L_0x0094:
            int r4 = (r6 > r9 ? 1 : (r6 == r9 ? 0 : -1))
            if (r4 == 0) goto L_0x00a5
            r8 = 2
            java.lang.String[] r11 = new java.lang.String[r8]     // Catch:{ SQLiteException -> 0x022a, all -> 0x0225 }
            r8 = 0
            r11[r12] = r8     // Catch:{ SQLiteException -> 0x022a, all -> 0x0225 }
            java.lang.String r8 = java.lang.String.valueOf(r6)     // Catch:{ SQLiteException -> 0x022a, all -> 0x0225 }
            r11[r13] = r8     // Catch:{ SQLiteException -> 0x022a, all -> 0x0225 }
            goto L_0x00aa
        L_0x00a5:
            r8 = 0
            java.lang.String[] r11 = new java.lang.String[]{r8}     // Catch:{ SQLiteException -> 0x022a, all -> 0x0225 }
        L_0x00aa:
            if (r4 == 0) goto L_0x00ae
            java.lang.String r16 = " and rowid <= ?"
        L_0x00ae:
            r4 = r16
            int r8 = r4.length()     // Catch:{ SQLiteException -> 0x022a, all -> 0x0225 }
            int r8 = r8 + 84
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x022a, all -> 0x0225 }
            r14.<init>(r8)     // Catch:{ SQLiteException -> 0x022a, all -> 0x0225 }
            java.lang.String r8 = "select metadata_fingerprint from raw_events where app_id = ?"
            r14.append(r8)     // Catch:{ SQLiteException -> 0x022a, all -> 0x0225 }
            r14.append(r4)     // Catch:{ SQLiteException -> 0x022a, all -> 0x0225 }
            java.lang.String r4 = " order by rowid limit 1;"
            r14.append(r4)     // Catch:{ SQLiteException -> 0x022a, all -> 0x0225 }
            java.lang.String r4 = r14.toString()     // Catch:{ SQLiteException -> 0x022a, all -> 0x0225 }
            android.database.Cursor r4 = r15.rawQuery(r4, r11)     // Catch:{ SQLiteException -> 0x022a, all -> 0x0225 }
            boolean r8 = r4.moveToFirst()     // Catch:{ SQLiteException -> 0x0222, all -> 0x0044 }
            if (r8 != 0) goto L_0x00db
            r4.close()     // Catch:{ all -> 0x0f0c }
            goto L_0x0246
        L_0x00db:
            java.lang.String r11 = r4.getString(r12)     // Catch:{ SQLiteException -> 0x0222, all -> 0x0044 }
            r4.close()     // Catch:{ SQLiteException -> 0x0222, all -> 0x0044 }
            r8 = 0
        L_0x00e3:
            java.lang.String r16 = "raw_events_metadata"
            java.lang.String r14 = "metadata"
            java.lang.String[] r17 = new java.lang.String[]{r14}     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            java.lang.String r18 = "app_id = ? and metadata_fingerprint = ?"
            r14 = 2
            java.lang.String[] r9 = new java.lang.String[r14]     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            r9[r12] = r8     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            r9[r13] = r11     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            r19 = 0
            r20 = 0
            java.lang.String r21 = "rowid"
            java.lang.String r22 = "2"
            r14 = r15
            r10 = r15
            r15 = r16
            r16 = r17
            r17 = r18
            r18 = r9
            android.database.Cursor r4 = r14.query(r15, r16, r17, r18, r19, r20, r21, r22)     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            boolean r9 = r4.moveToFirst()     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            if (r9 != 0) goto L_0x0126
            j.c.a.a.g.a.n3 r6 = r5.a()     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            j.c.a.a.g.a.p3 r6 = r6.u()     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            java.lang.String r7 = "Raw event metadata record is missing. appId"
            java.lang.Object r9 = j.c.a.a.g.a.n3.a(r8)     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            r6.a(r7, r9)     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            r4.close()     // Catch:{ all -> 0x0f0c }
            goto L_0x0246
        L_0x0126:
            byte[] r9 = r4.getBlob(r12)     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            j.c.a.a.f.e.j3 r14 = j.c.a.a.f.e.j3.b()     // Catch:{ IOException -> 0x0206 }
            j.c.a.a.f.e.u0 r9 = j.c.a.a.f.e.u0.a(r9, r14)     // Catch:{ IOException -> 0x0206 }
            boolean r14 = r4.moveToNext()     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            if (r14 == 0) goto L_0x0149
            j.c.a.a.g.a.n3 r14 = r5.a()     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            j.c.a.a.g.a.p3 r14 = r14.v()     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            java.lang.String r15 = "Get multiple raw event metadata records, expected one. appId"
            java.lang.Object r13 = j.c.a.a.g.a.n3.a(r8)     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            r14.a(r15, r13)     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
        L_0x0149:
            r4.close()     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            r3.a(r9)     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            r13 = -1
            int r9 = (r6 > r13 ? 1 : (r6 == r13 ? 0 : -1))
            if (r9 == 0) goto L_0x016b
            java.lang.String r9 = "app_id = ? and metadata_fingerprint = ? and rowid <= ?"
            r13 = 3
            java.lang.String[] r14 = new java.lang.String[r13]     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            r14[r12] = r8     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            r13 = 1
            r14[r13] = r11     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            r7 = 2
            r14[r7] = r6     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            r17 = r9
            r18 = r14
            goto L_0x0179
        L_0x016b:
            java.lang.String r6 = "app_id = ? and metadata_fingerprint = ?"
            r7 = 2
            java.lang.String[] r9 = new java.lang.String[r7]     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            r9[r12] = r8     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            r7 = 1
            r9[r7] = r11     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            r17 = r6
            r18 = r9
        L_0x0179:
            java.lang.String r15 = "raw_events"
            java.lang.String r6 = "rowid"
            java.lang.String r7 = "name"
            java.lang.String r9 = "timestamp"
            java.lang.String r11 = "data"
            java.lang.String[] r16 = new java.lang.String[]{r6, r7, r9, r11}     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            r19 = 0
            r20 = 0
            java.lang.String r21 = "rowid"
            r22 = 0
            r14 = r10
            android.database.Cursor r4 = r14.query(r15, r16, r17, r18, r19, r20, r21, r22)     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            boolean r6 = r4.moveToFirst()     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            if (r6 != 0) goto L_0x01b0
            j.c.a.a.g.a.n3 r6 = r5.a()     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            j.c.a.a.g.a.p3 r6 = r6.v()     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            java.lang.String r7 = "Raw event data disappeared while in transaction. appId"
            java.lang.Object r9 = j.c.a.a.g.a.n3.a(r8)     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            r6.a(r7, r9)     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            r4.close()     // Catch:{ all -> 0x0f0c }
            goto L_0x0246
        L_0x01b0:
            long r6 = r4.getLong(r12)     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            r9 = 3
            byte[] r10 = r4.getBlob(r9)     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            j.c.a.a.f.e.q0$a r9 = j.c.a.a.f.e.q0.o()     // Catch:{ IOException -> 0x01e9 }
            j.c.a.a.f.e.j3 r11 = j.c.a.a.f.e.j3.b()     // Catch:{ IOException -> 0x01e9 }
            j.c.a.a.f.e.p2 r9 = r9.a(r10, r11)     // Catch:{ IOException -> 0x01e9 }
            j.c.a.a.f.e.q0$a r9 = (j.c.a.a.f.e.q0.a) r9     // Catch:{ IOException -> 0x01e9 }
            r10 = 1
            java.lang.String r11 = r4.getString(r10)     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            r9.a(r11)     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            r10 = 2
            long r13 = r4.getLong(r10)     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            r9.a(r13)     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            j.c.a.a.f.e.f5 r9 = r9.m()     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            j.c.a.a.f.e.x3 r9 = (j.c.a.a.f.e.x3) r9     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            j.c.a.a.f.e.q0 r9 = (j.c.a.a.f.e.q0) r9     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            boolean r6 = r3.a(r6, r9)     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            if (r6 != 0) goto L_0x01fc
            r4.close()     // Catch:{ all -> 0x0f0c }
            goto L_0x0246
        L_0x01e9:
            r0 = move-exception
            r6 = r0
            j.c.a.a.g.a.n3 r7 = r5.a()     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            j.c.a.a.g.a.p3 r7 = r7.u()     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            java.lang.String r9 = "Data loss. Failed to merge raw event. appId"
            java.lang.Object r10 = j.c.a.a.g.a.n3.a(r8)     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            r7.a(r9, r10, r6)     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
        L_0x01fc:
            boolean r6 = r4.moveToNext()     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            if (r6 != 0) goto L_0x01b0
            r4.close()     // Catch:{ all -> 0x0f0c }
            goto L_0x0246
        L_0x0206:
            r0 = move-exception
            r6 = r0
            j.c.a.a.g.a.n3 r7 = r5.a()     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            j.c.a.a.g.a.p3 r7 = r7.u()     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            java.lang.String r9 = "Data loss. Failed to merge raw event metadata. appId"
            java.lang.Object r10 = j.c.a.a.g.a.n3.a(r8)     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            r7.a(r9, r10, r6)     // Catch:{ SQLiteException -> 0x021d, all -> 0x0044 }
            r4.close()     // Catch:{ all -> 0x0f0c }
            goto L_0x0246
        L_0x021d:
            r0 = move-exception
            r6 = r0
            r7 = r4
            r4 = r8
            goto L_0x0230
        L_0x0222:
            r0 = move-exception
            r6 = r0
            goto L_0x022e
        L_0x0225:
            r0 = move-exception
            r2 = r0
            r4 = 0
            goto L_0x0f06
        L_0x022a:
            r0 = move-exception
            r4 = r0
            r6 = r4
            r4 = 0
        L_0x022e:
            r7 = r4
            r4 = 0
        L_0x0230:
            j.c.a.a.g.a.n3 r5 = r5.a()     // Catch:{ all -> 0x0f03 }
            j.c.a.a.g.a.p3 r5 = r5.u()     // Catch:{ all -> 0x0f03 }
            java.lang.String r8 = "Data loss. Error selecting raw event. appId"
            java.lang.Object r4 = j.c.a.a.g.a.n3.a(r4)     // Catch:{ all -> 0x0f03 }
            r5.a(r8, r4, r6)     // Catch:{ all -> 0x0f03 }
            if (r7 == 0) goto L_0x0246
            r7.close()     // Catch:{ all -> 0x0f0c }
        L_0x0246:
            java.util.List<j.c.a.a.f.e.q0> r4 = r3.c     // Catch:{ all -> 0x0f0c }
            if (r4 == 0) goto L_0x0255
            java.util.List<j.c.a.a.f.e.q0> r4 = r3.c     // Catch:{ all -> 0x0f0c }
            boolean r4 = r4.isEmpty()     // Catch:{ all -> 0x0f0c }
            if (r4 == 0) goto L_0x0253
            goto L_0x0255
        L_0x0253:
            r4 = 0
            goto L_0x0256
        L_0x0255:
            r4 = 1
        L_0x0256:
            if (r4 != 0) goto L_0x0ef3
            j.c.a.a.f.e.u0 r4 = r3.a     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.x3$a r4 = r4.h()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.u0$a r4 = (j.c.a.a.f.e.u0.a) r4     // Catch:{ all -> 0x0f0c }
            r4.o()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.r4 r5 = r1.f2073i     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.i9 r5 = r5.k()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.u0 r6 = r3.a     // Catch:{ all -> 0x0f0c }
            java.lang.String r6 = r6.n()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.b3<java.lang.Boolean> r7 = j.c.a.a.g.a.k.h0     // Catch:{ all -> 0x0f0c }
            boolean r5 = r5.e(r6, r7)     // Catch:{ all -> 0x0f0c }
            r6 = -1
            r9 = 0
            r10 = 0
            r11 = 0
            r12 = -1
            r13 = 0
            r15 = 0
            r16 = 0
        L_0x027f:
            java.util.List<j.c.a.a.f.e.q0> r7 = r3.c     // Catch:{ all -> 0x0f0c }
            int r7 = r7.size()     // Catch:{ all -> 0x0f0c }
            java.lang.String r8 = "_fr"
            r19 = r11
            java.lang.String r11 = "_e"
            r20 = r2
            java.lang.String r2 = "_et"
            r21 = r13
            if (r15 >= r7) goto L_0x080f
            java.util.List<j.c.a.a.f.e.q0> r7 = r3.c     // Catch:{ all -> 0x0f0c }
            java.lang.Object r7 = r7.get(r15)     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.q0 r7 = (j.c.a.a.f.e.q0) r7     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.x3$a r7 = r7.h()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.q0$a r7 = (j.c.a.a.f.e.q0.a) r7     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.m4 r13 = r52.c()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.u0 r14 = r3.a     // Catch:{ all -> 0x0f0c }
            java.lang.String r14 = r14.n()     // Catch:{ all -> 0x0f0c }
            r26 = r15
            java.lang.String r15 = r7.o()     // Catch:{ all -> 0x0f0c }
            boolean r13 = r13.b(r14, r15)     // Catch:{ all -> 0x0f0c }
            java.lang.String r14 = "_err"
            if (r13 == 0) goto L_0x033a
            j.c.a.a.g.a.r4 r2 = r1.f2073i     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.n3 r2 = r2.a()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.p3 r2 = r2.v()     // Catch:{ all -> 0x0f0c }
            java.lang.String r8 = "Dropping blacklisted raw event. appId"
            j.c.a.a.f.e.u0 r11 = r3.a     // Catch:{ all -> 0x0f0c }
            java.lang.String r11 = r11.n()     // Catch:{ all -> 0x0f0c }
            java.lang.Object r11 = j.c.a.a.g.a.n3.a(r11)     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.r4 r13 = r1.f2073i     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.l3 r13 = r13.o()     // Catch:{ all -> 0x0f0c }
            java.lang.String r15 = r7.o()     // Catch:{ all -> 0x0f0c }
            java.lang.String r13 = r13.a(r15)     // Catch:{ all -> 0x0f0c }
            r2.a(r8, r11, r13)     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.m4 r2 = r52.c()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.u0 r8 = r3.a     // Catch:{ all -> 0x0f0c }
            java.lang.String r8 = r8.n()     // Catch:{ all -> 0x0f0c }
            boolean r2 = r2.d(r8)     // Catch:{ all -> 0x0f0c }
            if (r2 != 0) goto L_0x0303
            j.c.a.a.g.a.m4 r2 = r52.c()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.u0 r8 = r3.a     // Catch:{ all -> 0x0f0c }
            java.lang.String r8 = r8.n()     // Catch:{ all -> 0x0f0c }
            boolean r2 = r2.e(r8)     // Catch:{ all -> 0x0f0c }
            if (r2 == 0) goto L_0x0301
            goto L_0x0303
        L_0x0301:
            r2 = 0
            goto L_0x0304
        L_0x0303:
            r2 = 1
        L_0x0304:
            if (r2 != 0) goto L_0x0329
            java.lang.String r2 = r7.o()     // Catch:{ all -> 0x0f0c }
            boolean r2 = r14.equals(r2)     // Catch:{ all -> 0x0f0c }
            if (r2 != 0) goto L_0x0329
            j.c.a.a.g.a.r4 r2 = r1.f2073i     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.y8 r27 = r2.n()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.u0 r2 = r3.a     // Catch:{ all -> 0x0f0c }
            java.lang.String r28 = r2.n()     // Catch:{ all -> 0x0f0c }
            r29 = 11
            java.lang.String r30 = "_ev"
            java.lang.String r31 = r7.o()     // Catch:{ all -> 0x0f0c }
            r32 = 0
            r27.a(r28, r29, r30, r31, r32)     // Catch:{ all -> 0x0f0c }
        L_0x0329:
            r27 = r5
            r29 = r9
            r32 = r10
            r11 = r19
            r13 = r21
            r8 = r26
            r10 = 3
            r9 = r4
            r4 = -1
            goto L_0x0802
        L_0x033a:
            j.c.a.a.g.a.m4 r13 = r52.c()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.u0 r15 = r3.a     // Catch:{ all -> 0x0f0c }
            java.lang.String r15 = r15.n()     // Catch:{ all -> 0x0f0c }
            r27 = r5
            java.lang.String r5 = r7.o()     // Catch:{ all -> 0x0f0c }
            boolean r5 = r13.c(r15, r5)     // Catch:{ all -> 0x0f0c }
            java.lang.String r13 = "_c"
            if (r5 != 0) goto L_0x03ad
            r52.k()     // Catch:{ all -> 0x0f0c }
            java.lang.String r15 = r7.o()     // Catch:{ all -> 0x0f0c }
            i.b.k.ResourcesFlusher.b(r15)     // Catch:{ all -> 0x0f0c }
            r28 = r12
            int r12 = r15.hashCode()     // Catch:{ all -> 0x0f0c }
            r29 = r9
            r9 = 94660(0x171c4, float:1.32647E-40)
            if (r12 == r9) goto L_0x0388
            r9 = 95025(0x17331, float:1.33158E-40)
            if (r12 == r9) goto L_0x037e
            r9 = 95027(0x17333, float:1.33161E-40)
            if (r12 == r9) goto L_0x0374
            goto L_0x0392
        L_0x0374:
            java.lang.String r9 = "_ui"
            boolean r9 = r15.equals(r9)     // Catch:{ all -> 0x0f0c }
            if (r9 == 0) goto L_0x0392
            r9 = 1
            goto L_0x0393
        L_0x037e:
            java.lang.String r9 = "_ug"
            boolean r9 = r15.equals(r9)     // Catch:{ all -> 0x0f0c }
            if (r9 == 0) goto L_0x0392
            r9 = 2
            goto L_0x0393
        L_0x0388:
            java.lang.String r9 = "_in"
            boolean r9 = r15.equals(r9)     // Catch:{ all -> 0x0f0c }
            if (r9 == 0) goto L_0x0392
            r9 = 0
            goto L_0x0393
        L_0x0392:
            r9 = -1
        L_0x0393:
            if (r9 == 0) goto L_0x039d
            r12 = 1
            if (r9 == r12) goto L_0x039d
            r12 = 2
            if (r9 == r12) goto L_0x039d
            r9 = 0
            goto L_0x039e
        L_0x039d:
            r9 = 1
        L_0x039e:
            if (r9 == 0) goto L_0x03a1
            goto L_0x03b1
        L_0x03a1:
            r30 = r2
            r31 = r4
            r32 = r10
            r33 = r11
        L_0x03a9:
            r11 = r19
            goto L_0x058b
        L_0x03ad:
            r29 = r9
            r28 = r12
        L_0x03b1:
            r30 = r2
            r9 = 0
            r12 = 0
            r15 = 0
        L_0x03b6:
            int r2 = r7.n()     // Catch:{ all -> 0x0f0c }
            r31 = r4
            java.lang.String r4 = "_r"
            if (r9 >= r2) goto L_0x0424
            j.c.a.a.f.e.s0 r2 = r7.a(r9)     // Catch:{ all -> 0x0f0c }
            java.lang.String r2 = r2.a()     // Catch:{ all -> 0x0f0c }
            boolean r2 = r13.equals(r2)     // Catch:{ all -> 0x0f0c }
            if (r2 == 0) goto L_0x03ee
            j.c.a.a.f.e.s0 r2 = r7.a(r9)     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.x3$a r2 = r2.h()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.s0$a r2 = (j.c.a.a.f.e.s0.a) r2     // Catch:{ all -> 0x0f0c }
            r32 = r10
            r33 = r11
            r10 = 1
            r2.a(r10)     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.f5 r2 = r2.m()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.x3 r2 = (j.c.a.a.f.e.x3) r2     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.s0 r2 = (j.c.a.a.f.e.s0) r2     // Catch:{ all -> 0x0f0c }
            r7.a(r9, r2)     // Catch:{ all -> 0x0f0c }
            r12 = 1
            goto L_0x041b
        L_0x03ee:
            r32 = r10
            r33 = r11
            j.c.a.a.f.e.s0 r2 = r7.a(r9)     // Catch:{ all -> 0x0f0c }
            java.lang.String r2 = r2.a()     // Catch:{ all -> 0x0f0c }
            boolean r2 = r4.equals(r2)     // Catch:{ all -> 0x0f0c }
            if (r2 == 0) goto L_0x041b
            j.c.a.a.f.e.s0 r2 = r7.a(r9)     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.x3$a r2 = r2.h()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.s0$a r2 = (j.c.a.a.f.e.s0.a) r2     // Catch:{ all -> 0x0f0c }
            r10 = 1
            r2.a(r10)     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.f5 r2 = r2.m()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.x3 r2 = (j.c.a.a.f.e.x3) r2     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.s0 r2 = (j.c.a.a.f.e.s0) r2     // Catch:{ all -> 0x0f0c }
            r7.a(r9, r2)     // Catch:{ all -> 0x0f0c }
            r15 = 1
        L_0x041b:
            int r9 = r9 + 1
            r4 = r31
            r10 = r32
            r11 = r33
            goto L_0x03b6
        L_0x0424:
            r32 = r10
            r33 = r11
            if (r12 != 0) goto L_0x0458
            if (r5 == 0) goto L_0x0458
            j.c.a.a.g.a.r4 r2 = r1.f2073i     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.n3 r2 = r2.a()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.p3 r2 = r2.y()     // Catch:{ all -> 0x0f0c }
            java.lang.String r9 = "Marking event as conversion"
            j.c.a.a.g.a.r4 r10 = r1.f2073i     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.l3 r10 = r10.o()     // Catch:{ all -> 0x0f0c }
            java.lang.String r11 = r7.o()     // Catch:{ all -> 0x0f0c }
            java.lang.String r10 = r10.a(r11)     // Catch:{ all -> 0x0f0c }
            r2.a(r9, r10)     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.s0$a r2 = j.c.a.a.f.e.s0.p()     // Catch:{ all -> 0x0f0c }
            r2.a(r13)     // Catch:{ all -> 0x0f0c }
            r9 = 1
            r2.a(r9)     // Catch:{ all -> 0x0f0c }
            r7.a(r2)     // Catch:{ all -> 0x0f0c }
        L_0x0458:
            if (r15 != 0) goto L_0x0486
            j.c.a.a.g.a.r4 r2 = r1.f2073i     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.n3 r2 = r2.a()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.p3 r2 = r2.y()     // Catch:{ all -> 0x0f0c }
            java.lang.String r9 = "Marking event as real-time"
            j.c.a.a.g.a.r4 r10 = r1.f2073i     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.l3 r10 = r10.o()     // Catch:{ all -> 0x0f0c }
            java.lang.String r11 = r7.o()     // Catch:{ all -> 0x0f0c }
            java.lang.String r10 = r10.a(r11)     // Catch:{ all -> 0x0f0c }
            r2.a(r9, r10)     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.s0$a r2 = j.c.a.a.f.e.s0.p()     // Catch:{ all -> 0x0f0c }
            r2.a(r4)     // Catch:{ all -> 0x0f0c }
            r9 = 1
            r2.a(r9)     // Catch:{ all -> 0x0f0c }
            r7.a(r2)     // Catch:{ all -> 0x0f0c }
        L_0x0486:
            j.c.a.a.g.a.n9 r34 = r52.e()     // Catch:{ all -> 0x0f0c }
            long r35 = r52.s()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.u0 r2 = r3.a     // Catch:{ all -> 0x0f0c }
            java.lang.String r37 = r2.n()     // Catch:{ all -> 0x0f0c }
            r38 = 0
            r39 = 0
            r40 = 0
            r41 = 0
            r42 = 1
            j.c.a.a.g.a.m9 r2 = r34.a(r35, r37, r38, r39, r40, r41, r42)     // Catch:{ all -> 0x0f0c }
            long r9 = r2.f2044e     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.r4 r2 = r1.f2073i     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.i9 r2 = r2.k()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.u0 r11 = r3.a     // Catch:{ all -> 0x0f0c }
            java.lang.String r11 = r11.n()     // Catch:{ all -> 0x0f0c }
            int r2 = r2.a(r11)     // Catch:{ all -> 0x0f0c }
            long r11 = (long) r2     // Catch:{ all -> 0x0f0c }
            int r2 = (r9 > r11 ? 1 : (r9 == r11 ? 0 : -1))
            if (r2 <= 0) goto L_0x04bd
            a(r7, r4)     // Catch:{ all -> 0x0f0c }
            goto L_0x04bf
        L_0x04bd:
            r19 = 1
        L_0x04bf:
            java.lang.String r2 = r7.o()     // Catch:{ all -> 0x0f0c }
            boolean r2 = j.c.a.a.g.a.y8.e(r2)     // Catch:{ all -> 0x0f0c }
            if (r2 == 0) goto L_0x03a9
            if (r5 == 0) goto L_0x03a9
            j.c.a.a.g.a.n9 r34 = r52.e()     // Catch:{ all -> 0x0f0c }
            long r35 = r52.s()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.u0 r2 = r3.a     // Catch:{ all -> 0x0f0c }
            java.lang.String r37 = r2.n()     // Catch:{ all -> 0x0f0c }
            r38 = 0
            r39 = 0
            r40 = 1
            r41 = 0
            r42 = 0
            j.c.a.a.g.a.m9 r2 = r34.a(r35, r37, r38, r39, r40, r41, r42)     // Catch:{ all -> 0x0f0c }
            long r9 = r2.c     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.r4 r2 = r1.f2073i     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.i9 r2 = r2.k()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.u0 r4 = r3.a     // Catch:{ all -> 0x0f0c }
            java.lang.String r4 = r4.n()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.b3<java.lang.Integer> r11 = j.c.a.a.g.a.k.f2021s     // Catch:{ all -> 0x0f0c }
            int r2 = r2.b(r4, r11)     // Catch:{ all -> 0x0f0c }
            long r11 = (long) r2     // Catch:{ all -> 0x0f0c }
            int r2 = (r9 > r11 ? 1 : (r9 == r11 ? 0 : -1))
            if (r2 <= 0) goto L_0x03a9
            j.c.a.a.g.a.r4 r2 = r1.f2073i     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.n3 r2 = r2.a()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.p3 r2 = r2.v()     // Catch:{ all -> 0x0f0c }
            java.lang.String r4 = "Too many conversions. Not logging as conversion. appId"
            j.c.a.a.f.e.u0 r9 = r3.a     // Catch:{ all -> 0x0f0c }
            java.lang.String r9 = r9.n()     // Catch:{ all -> 0x0f0c }
            java.lang.Object r9 = j.c.a.a.g.a.n3.a(r9)     // Catch:{ all -> 0x0f0c }
            r2.a(r4, r9)     // Catch:{ all -> 0x0f0c }
            r2 = 0
            r4 = -1
            r9 = 0
            r10 = 0
        L_0x051d:
            int r11 = r7.n()     // Catch:{ all -> 0x0f0c }
            if (r2 >= r11) goto L_0x0548
            j.c.a.a.f.e.s0 r11 = r7.a(r2)     // Catch:{ all -> 0x0f0c }
            java.lang.String r12 = r11.a()     // Catch:{ all -> 0x0f0c }
            boolean r12 = r13.equals(r12)     // Catch:{ all -> 0x0f0c }
            if (r12 == 0) goto L_0x053a
            j.c.a.a.f.e.x3$a r4 = r11.h()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.s0$a r4 = (j.c.a.a.f.e.s0.a) r4     // Catch:{ all -> 0x0f0c }
            r9 = r4
            r4 = r2
            goto L_0x0545
        L_0x053a:
            java.lang.String r11 = r11.a()     // Catch:{ all -> 0x0f0c }
            boolean r11 = r14.equals(r11)     // Catch:{ all -> 0x0f0c }
            if (r11 == 0) goto L_0x0545
            r10 = 1
        L_0x0545:
            int r2 = r2 + 1
            goto L_0x051d
        L_0x0548:
            if (r10 == 0) goto L_0x0551
            if (r9 == 0) goto L_0x0551
            r7.b(r4)     // Catch:{ all -> 0x0f0c }
            goto L_0x03a9
        L_0x0551:
            if (r9 == 0) goto L_0x0570
            java.lang.Object r2 = r9.clone()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.x3$a r2 = (j.c.a.a.f.e.x3.a) r2     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.s0$a r2 = (j.c.a.a.f.e.s0.a) r2     // Catch:{ all -> 0x0f0c }
            r2.a(r14)     // Catch:{ all -> 0x0f0c }
            r9 = 10
            r2.a(r9)     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.f5 r2 = r2.m()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.x3 r2 = (j.c.a.a.f.e.x3) r2     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.s0 r2 = (j.c.a.a.f.e.s0) r2     // Catch:{ all -> 0x0f0c }
            r7.a(r4, r2)     // Catch:{ all -> 0x0f0c }
            goto L_0x03a9
        L_0x0570:
            j.c.a.a.g.a.r4 r2 = r1.f2073i     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.n3 r2 = r2.a()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.p3 r2 = r2.u()     // Catch:{ all -> 0x0f0c }
            java.lang.String r4 = "Did not find conversion parameter. appId"
            j.c.a.a.f.e.u0 r9 = r3.a     // Catch:{ all -> 0x0f0c }
            java.lang.String r9 = r9.n()     // Catch:{ all -> 0x0f0c }
            java.lang.Object r9 = j.c.a.a.g.a.n3.a(r9)     // Catch:{ all -> 0x0f0c }
            r2.a(r4, r9)     // Catch:{ all -> 0x0f0c }
            goto L_0x03a9
        L_0x058b:
            j.c.a.a.g.a.r4 r2 = r1.f2073i     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.i9 r2 = r2.k()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.u0 r4 = r3.a     // Catch:{ all -> 0x0f0c }
            java.lang.String r4 = r4.n()     // Catch:{ all -> 0x0f0c }
            boolean r2 = r2.e(r4)     // Catch:{ all -> 0x0f0c }
            if (r2 == 0) goto L_0x0660
            if (r5 == 0) goto L_0x0660
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ all -> 0x0f0c }
            java.util.List r4 = r7.a()     // Catch:{ all -> 0x0f0c }
            r2.<init>(r4)     // Catch:{ all -> 0x0f0c }
            r4 = 0
            r5 = -1
            r9 = -1
        L_0x05ab:
            int r10 = r2.size()     // Catch:{ all -> 0x0f0c }
            java.lang.String r12 = "currency"
            java.lang.String r14 = "value"
            if (r4 >= r10) goto L_0x05db
            java.lang.Object r10 = r2.get(r4)     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.s0 r10 = (j.c.a.a.f.e.s0) r10     // Catch:{ all -> 0x0f0c }
            java.lang.String r10 = r10.a()     // Catch:{ all -> 0x0f0c }
            boolean r10 = r14.equals(r10)     // Catch:{ all -> 0x0f0c }
            if (r10 == 0) goto L_0x05c7
            r5 = r4
            goto L_0x05d8
        L_0x05c7:
            java.lang.Object r10 = r2.get(r4)     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.s0 r10 = (j.c.a.a.f.e.s0) r10     // Catch:{ all -> 0x0f0c }
            java.lang.String r10 = r10.a()     // Catch:{ all -> 0x0f0c }
            boolean r10 = r12.equals(r10)     // Catch:{ all -> 0x0f0c }
            if (r10 == 0) goto L_0x05d8
            r9 = r4
        L_0x05d8:
            int r4 = r4 + 1
            goto L_0x05ab
        L_0x05db:
            r4 = -1
            if (r5 == r4) goto L_0x0661
            java.lang.Object r4 = r2.get(r5)     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.s0 r4 = (j.c.a.a.f.e.s0) r4     // Catch:{ all -> 0x0f0c }
            boolean r4 = r4.j()     // Catch:{ all -> 0x0f0c }
            if (r4 != 0) goto L_0x0611
            java.lang.Object r4 = r2.get(r5)     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.s0 r4 = (j.c.a.a.f.e.s0) r4     // Catch:{ all -> 0x0f0c }
            boolean r4 = r4.n()     // Catch:{ all -> 0x0f0c }
            if (r4 != 0) goto L_0x0611
            j.c.a.a.g.a.r4 r2 = r1.f2073i     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.n3 r2 = r2.a()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.p3 r2 = r2.w()     // Catch:{ all -> 0x0f0c }
            java.lang.String r4 = "Value must be specified with a numeric type."
            r2.a(r4)     // Catch:{ all -> 0x0f0c }
            r7.b(r5)     // Catch:{ all -> 0x0f0c }
            a(r7, r13)     // Catch:{ all -> 0x0f0c }
            r2 = 18
            a(r7, r2, r14)     // Catch:{ all -> 0x0f0c }
            goto L_0x0660
        L_0x0611:
            r4 = -1
            if (r9 != r4) goto L_0x0617
            r2 = 1
            r10 = 3
            goto L_0x0643
        L_0x0617:
            java.lang.Object r2 = r2.get(r9)     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.s0 r2 = (j.c.a.a.f.e.s0) r2     // Catch:{ all -> 0x0f0c }
            java.lang.String r2 = r2.i()     // Catch:{ all -> 0x0f0c }
            int r9 = r2.length()     // Catch:{ all -> 0x0f0c }
            r10 = 3
            if (r9 == r10) goto L_0x0629
            goto L_0x063a
        L_0x0629:
            r9 = 0
        L_0x062a:
            int r14 = r2.length()     // Catch:{ all -> 0x0f0c }
            if (r9 >= r14) goto L_0x0642
            int r14 = r2.codePointAt(r9)     // Catch:{ all -> 0x0f0c }
            boolean r15 = java.lang.Character.isLetter(r14)     // Catch:{ all -> 0x0f0c }
            if (r15 != 0) goto L_0x063c
        L_0x063a:
            r2 = 1
            goto L_0x0643
        L_0x063c:
            int r14 = java.lang.Character.charCount(r14)     // Catch:{ all -> 0x0f0c }
            int r9 = r9 + r14
            goto L_0x062a
        L_0x0642:
            r2 = 0
        L_0x0643:
            if (r2 == 0) goto L_0x0662
            j.c.a.a.g.a.r4 r2 = r1.f2073i     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.n3 r2 = r2.a()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.p3 r2 = r2.w()     // Catch:{ all -> 0x0f0c }
            java.lang.String r9 = "Value parameter discarded. You must also supply a 3-letter ISO_4217 currency code in the currency parameter."
            r2.a(r9)     // Catch:{ all -> 0x0f0c }
            r7.b(r5)     // Catch:{ all -> 0x0f0c }
            a(r7, r13)     // Catch:{ all -> 0x0f0c }
            r2 = 19
            a(r7, r2, r12)     // Catch:{ all -> 0x0f0c }
            goto L_0x0662
        L_0x0660:
            r4 = -1
        L_0x0661:
            r10 = 3
        L_0x0662:
            j.c.a.a.g.a.r4 r2 = r1.f2073i     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.i9 r2 = r2.k()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.u0 r5 = r3.a     // Catch:{ all -> 0x0f0c }
            java.lang.String r5 = r5.n()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.b3<java.lang.Boolean> r9 = j.c.a.a.g.a.k.g0     // Catch:{ all -> 0x0f0c }
            boolean r2 = r2.e(r5, r9)     // Catch:{ all -> 0x0f0c }
            if (r2 == 0) goto L_0x0783
            java.lang.String r2 = r7.o()     // Catch:{ all -> 0x0f0c }
            r5 = r33
            boolean r2 = r5.equals(r2)     // Catch:{ all -> 0x0f0c }
            r12 = 1000(0x3e8, double:4.94E-321)
            if (r2 == 0) goto L_0x06cf
            r52.k()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.f5 r2 = r7.m()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.x3 r2 = (j.c.a.a.f.e.x3) r2     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.q0 r2 = (j.c.a.a.f.e.q0) r2     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.s0 r2 = j.c.a.a.g.a.u8.a(r2, r8)     // Catch:{ all -> 0x0f0c }
            if (r2 != 0) goto L_0x06c7
            if (r32 == 0) goto L_0x06c0
            long r8 = r32.p()     // Catch:{ all -> 0x0f0c }
            long r14 = r7.p()     // Catch:{ all -> 0x0f0c }
            long r8 = r8 - r14
            long r8 = java.lang.Math.abs(r8)     // Catch:{ all -> 0x0f0c }
            int r2 = (r8 > r12 ? 1 : (r8 == r12 ? 0 : -1))
            if (r2 > 0) goto L_0x06c0
            java.lang.Object r2 = r32.clone()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.x3$a r2 = (j.c.a.a.f.e.x3.a) r2     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.q0$a r2 = (j.c.a.a.f.e.q0.a) r2     // Catch:{ all -> 0x0f0c }
            boolean r8 = r1.a(r7, r2)     // Catch:{ all -> 0x0f0c }
            if (r8 == 0) goto L_0x06c0
            r9 = r31
            r9.a(r6, r2)     // Catch:{ all -> 0x0f0c }
            r8 = r28
            r14 = r30
            goto L_0x0717
        L_0x06c0:
            r9 = r31
            r29 = r7
            r12 = r16
            goto L_0x06cb
        L_0x06c7:
            r9 = r31
            r12 = r28
        L_0x06cb:
            r14 = r30
            goto L_0x078c
        L_0x06cf:
            r9 = r31
            java.lang.String r2 = "_vs"
            java.lang.String r8 = r7.o()     // Catch:{ all -> 0x0f0c }
            boolean r2 = r2.equals(r8)     // Catch:{ all -> 0x0f0c }
            if (r2 == 0) goto L_0x0726
            r52.k()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.f5 r2 = r7.m()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.x3 r2 = (j.c.a.a.f.e.x3) r2     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.q0 r2 = (j.c.a.a.f.e.q0) r2     // Catch:{ all -> 0x0f0c }
            r14 = r30
            j.c.a.a.f.e.s0 r2 = j.c.a.a.g.a.u8.a(r2, r14)     // Catch:{ all -> 0x0f0c }
            if (r2 != 0) goto L_0x0723
            if (r29 == 0) goto L_0x071a
            long r18 = r29.p()     // Catch:{ all -> 0x0f0c }
            long r24 = r7.p()     // Catch:{ all -> 0x0f0c }
            long r18 = r18 - r24
            long r18 = java.lang.Math.abs(r18)     // Catch:{ all -> 0x0f0c }
            int r2 = (r18 > r12 ? 1 : (r18 == r12 ? 0 : -1))
            if (r2 > 0) goto L_0x071a
            java.lang.Object r2 = r29.clone()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.x3$a r2 = (j.c.a.a.f.e.x3.a) r2     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.q0$a r2 = (j.c.a.a.f.e.q0.a) r2     // Catch:{ all -> 0x0f0c }
            boolean r8 = r1.a(r2, r7)     // Catch:{ all -> 0x0f0c }
            if (r8 == 0) goto L_0x071a
            r8 = r28
            r9.a(r8, r2)     // Catch:{ all -> 0x0f0c }
        L_0x0717:
            r32 = 0
            goto L_0x077f
        L_0x071a:
            r8 = r28
            r32 = r7
            r12 = r8
            r6 = r16
            goto L_0x078c
        L_0x0723:
            r8 = r28
            goto L_0x078b
        L_0x0726:
            r8 = r28
            r14 = r30
            j.c.a.a.g.a.r4 r2 = r1.f2073i     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.i9 r2 = r2.k()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.u0 r12 = r3.a     // Catch:{ all -> 0x0f0c }
            java.lang.String r12 = r12.n()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.b3<java.lang.Boolean> r13 = j.c.a.a.g.a.k.L0     // Catch:{ all -> 0x0f0c }
            boolean r2 = r2.e(r12, r13)     // Catch:{ all -> 0x0f0c }
            if (r2 == 0) goto L_0x078b
            java.lang.String r2 = "_ab"
            java.lang.String r12 = r7.o()     // Catch:{ all -> 0x0f0c }
            boolean r2 = r2.equals(r12)     // Catch:{ all -> 0x0f0c }
            if (r2 == 0) goto L_0x078b
            r52.k()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.f5 r2 = r7.m()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.x3 r2 = (j.c.a.a.f.e.x3) r2     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.q0 r2 = (j.c.a.a.f.e.q0) r2     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.s0 r2 = j.c.a.a.g.a.u8.a(r2, r14)     // Catch:{ all -> 0x0f0c }
            if (r2 != 0) goto L_0x078b
            if (r29 == 0) goto L_0x078b
            long r12 = r29.p()     // Catch:{ all -> 0x0f0c }
            long r18 = r7.p()     // Catch:{ all -> 0x0f0c }
            long r12 = r12 - r18
            long r12 = java.lang.Math.abs(r12)     // Catch:{ all -> 0x0f0c }
            r18 = 4000(0xfa0, double:1.9763E-320)
            int r2 = (r12 > r18 ? 1 : (r12 == r18 ? 0 : -1))
            if (r2 > 0) goto L_0x078b
            java.lang.Object r2 = r29.clone()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.x3$a r2 = (j.c.a.a.f.e.x3.a) r2     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.q0$a r2 = (j.c.a.a.f.e.q0.a) r2     // Catch:{ all -> 0x0f0c }
            r1.b(r2, r7)     // Catch:{ all -> 0x0f0c }
            r9.a(r8, r2)     // Catch:{ all -> 0x0f0c }
        L_0x077f:
            r12 = r8
            r29 = 0
            goto L_0x078c
        L_0x0783:
            r8 = r28
            r14 = r30
            r9 = r31
            r5 = r33
        L_0x078b:
            r12 = r8
        L_0x078c:
            if (r27 != 0) goto L_0x07ec
            java.lang.String r2 = r7.o()     // Catch:{ all -> 0x0f0c }
            boolean r2 = r5.equals(r2)     // Catch:{ all -> 0x0f0c }
            if (r2 == 0) goto L_0x07ec
            int r2 = r7.n()     // Catch:{ all -> 0x0f0c }
            if (r2 != 0) goto L_0x07b8
            j.c.a.a.g.a.r4 r2 = r1.f2073i     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.n3 r2 = r2.a()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.p3 r2 = r2.v()     // Catch:{ all -> 0x0f0c }
            java.lang.String r5 = "Engagement event does not contain any parameters. appId"
            j.c.a.a.f.e.u0 r8 = r3.a     // Catch:{ all -> 0x0f0c }
            java.lang.String r8 = r8.n()     // Catch:{ all -> 0x0f0c }
            java.lang.Object r8 = j.c.a.a.g.a.n3.a(r8)     // Catch:{ all -> 0x0f0c }
            r2.a(r5, r8)     // Catch:{ all -> 0x0f0c }
            goto L_0x07ec
        L_0x07b8:
            r52.k()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.f5 r2 = r7.m()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.x3 r2 = (j.c.a.a.f.e.x3) r2     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.q0 r2 = (j.c.a.a.f.e.q0) r2     // Catch:{ all -> 0x0f0c }
            java.lang.Object r2 = j.c.a.a.g.a.u8.b(r2, r14)     // Catch:{ all -> 0x0f0c }
            java.lang.Long r2 = (java.lang.Long) r2     // Catch:{ all -> 0x0f0c }
            if (r2 != 0) goto L_0x07e5
            j.c.a.a.g.a.r4 r2 = r1.f2073i     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.n3 r2 = r2.a()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.p3 r2 = r2.v()     // Catch:{ all -> 0x0f0c }
            java.lang.String r5 = "Engagement event does not include duration. appId"
            j.c.a.a.f.e.u0 r8 = r3.a     // Catch:{ all -> 0x0f0c }
            java.lang.String r8 = r8.n()     // Catch:{ all -> 0x0f0c }
            java.lang.Object r8 = j.c.a.a.g.a.n3.a(r8)     // Catch:{ all -> 0x0f0c }
            r2.a(r5, r8)     // Catch:{ all -> 0x0f0c }
            goto L_0x07ec
        L_0x07e5:
            long r13 = r2.longValue()     // Catch:{ all -> 0x0f0c }
            long r13 = r21 + r13
            goto L_0x07ee
        L_0x07ec:
            r13 = r21
        L_0x07ee:
            java.util.List<j.c.a.a.f.e.q0> r2 = r3.c     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.f5 r5 = r7.m()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.x3 r5 = (j.c.a.a.f.e.x3) r5     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.q0 r5 = (j.c.a.a.f.e.q0) r5     // Catch:{ all -> 0x0f0c }
            r8 = r26
            r2.set(r8, r5)     // Catch:{ all -> 0x0f0c }
            int r16 = r16 + 1
            r9.a(r7)     // Catch:{ all -> 0x0f0c }
        L_0x0802:
            int r15 = r8 + 1
            r4 = r9
            r2 = r20
            r5 = r27
            r9 = r29
            r10 = r32
            goto L_0x027f
        L_0x080f:
            r14 = r2
            r9 = r4
            r27 = r5
            r5 = r11
            if (r27 == 0) goto L_0x0868
            r2 = r16
            r4 = 0
        L_0x0819:
            if (r4 >= r2) goto L_0x0868
            j.c.a.a.f.e.q0 r6 = r9.a(r4)     // Catch:{ all -> 0x0f0c }
            java.lang.String r7 = r6.i()     // Catch:{ all -> 0x0f0c }
            boolean r7 = r5.equals(r7)     // Catch:{ all -> 0x0f0c }
            if (r7 == 0) goto L_0x083a
            r52.k()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.s0 r7 = j.c.a.a.g.a.u8.a(r6, r8)     // Catch:{ all -> 0x0f0c }
            if (r7 == 0) goto L_0x083a
            r9.b(r4)     // Catch:{ all -> 0x0f0c }
            int r2 = r2 + -1
            int r4 = r4 + -1
            goto L_0x0865
        L_0x083a:
            r52.k()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.s0 r6 = j.c.a.a.g.a.u8.a(r6, r14)     // Catch:{ all -> 0x0f0c }
            if (r6 == 0) goto L_0x0865
            boolean r7 = r6.j()     // Catch:{ all -> 0x0f0c }
            if (r7 == 0) goto L_0x0852
            long r6 = r6.m()     // Catch:{ all -> 0x0f0c }
            java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x0f0c }
            goto L_0x0853
        L_0x0852:
            r6 = 0
        L_0x0853:
            if (r6 == 0) goto L_0x0865
            long r10 = r6.longValue()     // Catch:{ all -> 0x0f0c }
            r12 = 0
            int r7 = (r10 > r12 ? 1 : (r10 == r12 ? 0 : -1))
            if (r7 <= 0) goto L_0x0865
            long r6 = r6.longValue()     // Catch:{ all -> 0x0f0c }
            long r21 = r21 + r6
        L_0x0865:
            r6 = 1
            int r4 = r4 + r6
            goto L_0x0819
        L_0x0868:
            r13 = r21
            r2 = 0
            r1.a(r9, r13, r2)     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.r4 r2 = r1.f2073i     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.i9 r2 = r2.k()     // Catch:{ all -> 0x0f0c }
            java.lang.String r4 = r9.v()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.b3<java.lang.Boolean> r5 = j.c.a.a.g.a.k.y0     // Catch:{ all -> 0x0f0c }
            boolean r2 = r2.e(r4, r5)     // Catch:{ all -> 0x0f0c }
            java.lang.String r4 = "_se"
            if (r2 == 0) goto L_0x08b7
            java.util.List r2 = r9.m14a()     // Catch:{ all -> 0x0f0c }
            java.util.Iterator r2 = r2.iterator()     // Catch:{ all -> 0x0f0c }
        L_0x088a:
            boolean r5 = r2.hasNext()     // Catch:{ all -> 0x0f0c }
            if (r5 == 0) goto L_0x08a4
            java.lang.Object r5 = r2.next()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.q0 r5 = (j.c.a.a.f.e.q0) r5     // Catch:{ all -> 0x0f0c }
            java.lang.String r6 = "_s"
            java.lang.String r5 = r5.i()     // Catch:{ all -> 0x0f0c }
            boolean r5 = r6.equals(r5)     // Catch:{ all -> 0x0f0c }
            if (r5 == 0) goto L_0x088a
            r2 = 1
            goto L_0x08a5
        L_0x08a4:
            r2 = 0
        L_0x08a5:
            if (r2 == 0) goto L_0x08b2
            j.c.a.a.g.a.n9 r2 = r52.e()     // Catch:{ all -> 0x0f0c }
            java.lang.String r5 = r9.v()     // Catch:{ all -> 0x0f0c }
            r2.b(r5, r4)     // Catch:{ all -> 0x0f0c }
        L_0x08b2:
            r2 = 1
            r1.a(r9, r13, r2)     // Catch:{ all -> 0x0f0c }
            goto L_0x08d4
        L_0x08b7:
            j.c.a.a.g.a.r4 r2 = r1.f2073i     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.i9 r2 = r2.k()     // Catch:{ all -> 0x0f0c }
            java.lang.String r5 = r9.v()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.b3<java.lang.Boolean> r6 = j.c.a.a.g.a.k.z0     // Catch:{ all -> 0x0f0c }
            boolean r2 = r2.e(r5, r6)     // Catch:{ all -> 0x0f0c }
            if (r2 == 0) goto L_0x08d4
            j.c.a.a.g.a.n9 r2 = r52.e()     // Catch:{ all -> 0x0f0c }
            java.lang.String r5 = r9.v()     // Catch:{ all -> 0x0f0c }
            r2.b(r5, r4)     // Catch:{ all -> 0x0f0c }
        L_0x08d4:
            j.c.a.a.g.a.r4 r2 = r1.f2073i     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.i9 r2 = r2.k()     // Catch:{ all -> 0x0f0c }
            java.lang.String r4 = r9.v()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.b3<java.lang.Boolean> r5 = j.c.a.a.g.a.k.i0     // Catch:{ all -> 0x0f0c }
            boolean r2 = r2.e(r4, r5)     // Catch:{ all -> 0x0f0c }
            if (r2 == 0) goto L_0x0974
            j.c.a.a.g.a.u8 r2 = r52.k()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.n3 r4 = r2.a()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.p3 r4 = r4.y()     // Catch:{ all -> 0x0f0c }
            java.lang.String r5 = "Checking account type status for ad personalization signals"
            r4.a(r5)     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.m4 r4 = r2.t()     // Catch:{ all -> 0x0f0c }
            java.lang.String r5 = r9.v()     // Catch:{ all -> 0x0f0c }
            boolean r4 = r4.b(r5)     // Catch:{ all -> 0x0f0c }
            if (r4 == 0) goto L_0x0974
            j.c.a.a.g.a.n9 r4 = r2.s()     // Catch:{ all -> 0x0f0c }
            java.lang.String r5 = r9.v()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.e4 r4 = r4.b(r5)     // Catch:{ all -> 0x0f0c }
            if (r4 == 0) goto L_0x0974
            boolean r4 = r4.c()     // Catch:{ all -> 0x0f0c }
            if (r4 == 0) goto L_0x0974
            j.c.a.a.g.a.c r4 = r2.e()     // Catch:{ all -> 0x0f0c }
            boolean r4 = r4.w()     // Catch:{ all -> 0x0f0c }
            if (r4 == 0) goto L_0x0974
            j.c.a.a.g.a.n3 r4 = r2.a()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.p3 r4 = r4.x()     // Catch:{ all -> 0x0f0c }
            java.lang.String r5 = "Turning off ad personalization due to account type"
            r4.a(r5)     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.y0$a r4 = j.c.a.a.f.e.y0.m()     // Catch:{ all -> 0x0f0c }
            r5 = r20
            r4.a(r5)     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.c r2 = r2.e()     // Catch:{ all -> 0x0f0c }
            long r6 = r2.v()     // Catch:{ all -> 0x0f0c }
            r4.a(r6)     // Catch:{ all -> 0x0f0c }
            r6 = 1
            r4.b(r6)     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.f5 r2 = r4.m()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.x3 r2 = (j.c.a.a.f.e.x3) r2     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.y0 r2 = (j.c.a.a.f.e.y0) r2     // Catch:{ all -> 0x0f0c }
            r4 = 0
        L_0x0952:
            int r6 = r9.q()     // Catch:{ all -> 0x0f0c }
            if (r4 >= r6) goto L_0x096e
            j.c.a.a.f.e.y0 r6 = r9.c(r4)     // Catch:{ all -> 0x0f0c }
            java.lang.String r6 = r6.j()     // Catch:{ all -> 0x0f0c }
            boolean r6 = r5.equals(r6)     // Catch:{ all -> 0x0f0c }
            if (r6 == 0) goto L_0x096b
            r9.a(r4, r2)     // Catch:{ all -> 0x0f0c }
            r4 = 1
            goto L_0x096f
        L_0x096b:
            int r4 = r4 + 1
            goto L_0x0952
        L_0x096e:
            r4 = 0
        L_0x096f:
            if (r4 != 0) goto L_0x0974
            r9.a(r2)     // Catch:{ all -> 0x0f0c }
        L_0x0974:
            j.c.a.a.g.a.r4 r2 = r1.f2073i     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.i9 r2 = r2.k()     // Catch:{ all -> 0x0f0c }
            java.lang.String r4 = r9.v()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.b3<java.lang.Boolean> r5 = j.c.a.a.g.a.k.H0     // Catch:{ all -> 0x0f0c }
            boolean r2 = r2.e(r4, r5)     // Catch:{ all -> 0x0f0c }
            if (r2 == 0) goto L_0x0989
            a(r9)     // Catch:{ all -> 0x0f0c }
        L_0x0989:
            r9.y()     // Catch:{ all -> 0x0f0c }
            java.lang.String r2 = r9.v()     // Catch:{ all -> 0x0f0c }
            java.util.List r4 = r9.p()     // Catch:{ all -> 0x0f0c }
            java.util.List r5 = r9.m14a()     // Catch:{ all -> 0x0f0c }
            long r6 = r9.r()     // Catch:{ all -> 0x0f0c }
            i.b.k.ResourcesFlusher.b(r2)     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.f9 r8 = r52.g()     // Catch:{ all -> 0x0f0c }
            java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x0f0c }
            java.util.List r2 = r8.a(r2, r5, r4, r6)     // Catch:{ all -> 0x0f0c }
            r9.b(r2)     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.r4 r2 = r1.f2073i     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.i9 r2 = r2.k()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.u0 r4 = r3.a     // Catch:{ all -> 0x0f0c }
            java.lang.String r4 = r4.n()     // Catch:{ all -> 0x0f0c }
            boolean r2 = r2.c(r4)     // Catch:{ all -> 0x0f0c }
            if (r2 == 0) goto L_0x0d50
            java.util.HashMap r2 = new java.util.HashMap     // Catch:{ all -> 0x0d49 }
            r2.<init>()     // Catch:{ all -> 0x0d49 }
            java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ all -> 0x0d49 }
            r4.<init>()     // Catch:{ all -> 0x0d49 }
            j.c.a.a.g.a.r4 r5 = r1.f2073i     // Catch:{ all -> 0x0d49 }
            j.c.a.a.g.a.y8 r5 = r5.n()     // Catch:{ all -> 0x0d49 }
            java.security.SecureRandom r5 = r5.u()     // Catch:{ all -> 0x0d49 }
            r6 = 0
        L_0x09d5:
            int r7 = r9.n()     // Catch:{ all -> 0x0d49 }
            if (r6 >= r7) goto L_0x0d16
            j.c.a.a.f.e.q0 r7 = r9.a(r6)     // Catch:{ all -> 0x0d49 }
            j.c.a.a.f.e.x3$a r7 = r7.h()     // Catch:{ all -> 0x0d49 }
            j.c.a.a.f.e.q0$a r7 = (j.c.a.a.f.e.q0.a) r7     // Catch:{ all -> 0x0d49 }
            java.lang.String r8 = r7.o()     // Catch:{ all -> 0x0d49 }
            java.lang.String r10 = "_ep"
            boolean r8 = r8.equals(r10)     // Catch:{ all -> 0x0d49 }
            java.lang.String r10 = "_sr"
            java.lang.String r11 = "_efs"
            if (r8 == 0) goto L_0x0a61
            r52.k()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.f5 r8 = r7.m()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.x3 r8 = (j.c.a.a.f.e.x3) r8     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.q0 r8 = (j.c.a.a.f.e.q0) r8     // Catch:{ all -> 0x0f0c }
            java.lang.String r12 = "_en"
            java.lang.Object r8 = j.c.a.a.g.a.u8.b(r8, r12)     // Catch:{ all -> 0x0f0c }
            java.lang.String r8 = (java.lang.String) r8     // Catch:{ all -> 0x0f0c }
            java.lang.Object r12 = r2.get(r8)     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.e r12 = (j.c.a.a.g.a.e) r12     // Catch:{ all -> 0x0f0c }
            if (r12 != 0) goto L_0x0a21
            j.c.a.a.g.a.n9 r12 = r52.e()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.u0 r13 = r3.a     // Catch:{ all -> 0x0f0c }
            java.lang.String r13 = r13.n()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.e r12 = r12.a(r13, r8)     // Catch:{ all -> 0x0f0c }
            r2.put(r8, r12)     // Catch:{ all -> 0x0f0c }
        L_0x0a21:
            java.lang.Long r8 = r12.f1967i     // Catch:{ all -> 0x0f0c }
            if (r8 != 0) goto L_0x0a5c
            java.lang.Long r8 = r12.f1968j     // Catch:{ all -> 0x0f0c }
            long r13 = r8.longValue()     // Catch:{ all -> 0x0f0c }
            r15 = 1
            int r8 = (r13 > r15 ? 1 : (r13 == r15 ? 0 : -1))
            if (r8 <= 0) goto L_0x0a39
            r52.k()     // Catch:{ all -> 0x0f0c }
            java.lang.Long r8 = r12.f1968j     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.u8.a(r7, r10, r8)     // Catch:{ all -> 0x0f0c }
        L_0x0a39:
            java.lang.Boolean r8 = r12.f1969k     // Catch:{ all -> 0x0f0c }
            if (r8 == 0) goto L_0x0a51
            java.lang.Boolean r8 = r12.f1969k     // Catch:{ all -> 0x0f0c }
            boolean r8 = r8.booleanValue()     // Catch:{ all -> 0x0f0c }
            if (r8 == 0) goto L_0x0a51
            r52.k()     // Catch:{ all -> 0x0f0c }
            r12 = 1
            java.lang.Long r8 = java.lang.Long.valueOf(r12)     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.u8.a(r7, r11, r8)     // Catch:{ all -> 0x0f0c }
        L_0x0a51:
            j.c.a.a.f.e.f5 r8 = r7.m()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.x3 r8 = (j.c.a.a.f.e.x3) r8     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.q0 r8 = (j.c.a.a.f.e.q0) r8     // Catch:{ all -> 0x0f0c }
            r4.add(r8)     // Catch:{ all -> 0x0f0c }
        L_0x0a5c:
            r9.a(r6, r7)     // Catch:{ all -> 0x0f0c }
            goto L_0x0c08
        L_0x0a61:
            j.c.a.a.g.a.m4 r8 = r52.c()     // Catch:{ all -> 0x0d49 }
            j.c.a.a.f.e.u0 r12 = r3.a     // Catch:{ all -> 0x0d49 }
            java.lang.String r12 = r12.n()     // Catch:{ all -> 0x0d49 }
            long r12 = r8.c(r12)     // Catch:{ all -> 0x0d49 }
            j.c.a.a.g.a.r4 r8 = r1.f2073i     // Catch:{ all -> 0x0d49 }
            r8.n()     // Catch:{ all -> 0x0d49 }
            long r14 = r7.p()     // Catch:{ all -> 0x0d49 }
            long r14 = j.c.a.a.g.a.y8.a(r14, r12)     // Catch:{ all -> 0x0d49 }
            j.c.a.a.f.e.f5 r8 = r7.m()     // Catch:{ all -> 0x0d49 }
            j.c.a.a.f.e.x3 r8 = (j.c.a.a.f.e.x3) r8     // Catch:{ all -> 0x0d49 }
            j.c.a.a.f.e.q0 r8 = (j.c.a.a.f.e.q0) r8     // Catch:{ all -> 0x0d49 }
            r16 = r11
            java.lang.String r11 = "_dbg"
            r22 = r12
            r20 = 1
            java.lang.Long r12 = java.lang.Long.valueOf(r20)     // Catch:{ all -> 0x0d49 }
            boolean r13 = android.text.TextUtils.isEmpty(r11)     // Catch:{ all -> 0x0d49 }
            if (r13 != 0) goto L_0x0aec
            if (r12 != 0) goto L_0x0a99
            goto L_0x0aec
        L_0x0a99:
            java.util.List r8 = r8.a()     // Catch:{ all -> 0x0f0c }
            java.util.Iterator r8 = r8.iterator()     // Catch:{ all -> 0x0f0c }
        L_0x0aa1:
            boolean r13 = r8.hasNext()     // Catch:{ all -> 0x0f0c }
            if (r13 == 0) goto L_0x0aec
            java.lang.Object r13 = r8.next()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.s0 r13 = (j.c.a.a.f.e.s0) r13     // Catch:{ all -> 0x0f0c }
            r18 = r8
            java.lang.String r8 = r13.a()     // Catch:{ all -> 0x0f0c }
            boolean r8 = r11.equals(r8)     // Catch:{ all -> 0x0f0c }
            if (r8 == 0) goto L_0x0ae9
            long r20 = r13.m()     // Catch:{ all -> 0x0f0c }
            java.lang.Long r8 = java.lang.Long.valueOf(r20)     // Catch:{ all -> 0x0f0c }
            boolean r8 = r12.equals(r8)     // Catch:{ all -> 0x0f0c }
            if (r8 != 0) goto L_0x0ae7
            boolean r8 = r12 instanceof java.lang.String     // Catch:{ all -> 0x0f0c }
            if (r8 == 0) goto L_0x0ad5
            java.lang.String r8 = r13.i()     // Catch:{ all -> 0x0f0c }
            boolean r8 = r12.equals(r8)     // Catch:{ all -> 0x0f0c }
            if (r8 != 0) goto L_0x0ae7
        L_0x0ad5:
            boolean r8 = r12 instanceof java.lang.Double     // Catch:{ all -> 0x0f0c }
            if (r8 == 0) goto L_0x0aec
            double r20 = r13.o()     // Catch:{ all -> 0x0f0c }
            java.lang.Double r8 = java.lang.Double.valueOf(r20)     // Catch:{ all -> 0x0f0c }
            boolean r8 = r12.equals(r8)     // Catch:{ all -> 0x0f0c }
            if (r8 == 0) goto L_0x0aec
        L_0x0ae7:
            r8 = 1
            goto L_0x0aed
        L_0x0ae9:
            r8 = r18
            goto L_0x0aa1
        L_0x0aec:
            r8 = 0
        L_0x0aed:
            if (r8 != 0) goto L_0x0b02
            j.c.a.a.g.a.m4 r8 = r52.c()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.u0 r11 = r3.a     // Catch:{ all -> 0x0f0c }
            java.lang.String r11 = r11.n()     // Catch:{ all -> 0x0f0c }
            java.lang.String r12 = r7.o()     // Catch:{ all -> 0x0f0c }
            int r8 = r8.d(r11, r12)     // Catch:{ all -> 0x0f0c }
            goto L_0x0b03
        L_0x0b02:
            r8 = 1
        L_0x0b03:
            if (r8 > 0) goto L_0x0b2c
            j.c.a.a.g.a.r4 r10 = r1.f2073i     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.n3 r10 = r10.a()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.p3 r10 = r10.v()     // Catch:{ all -> 0x0f0c }
            java.lang.String r11 = "Sample rate must be positive. event, rate"
            java.lang.String r12 = r7.o()     // Catch:{ all -> 0x0f0c }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ all -> 0x0f0c }
            r10.a(r11, r12, r8)     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.f5 r8 = r7.m()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.x3 r8 = (j.c.a.a.f.e.x3) r8     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.q0 r8 = (j.c.a.a.f.e.q0) r8     // Catch:{ all -> 0x0f0c }
            r4.add(r8)     // Catch:{ all -> 0x0f0c }
            r9.a(r6, r7)     // Catch:{ all -> 0x0f0c }
            goto L_0x0c08
        L_0x0b2c:
            java.lang.String r11 = r7.o()     // Catch:{ all -> 0x0d49 }
            java.lang.Object r11 = r2.get(r11)     // Catch:{ all -> 0x0d49 }
            j.c.a.a.g.a.e r11 = (j.c.a.a.g.a.e) r11     // Catch:{ all -> 0x0d49 }
            if (r11 != 0) goto L_0x0bbb
            j.c.a.a.g.a.n9 r11 = r52.e()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.u0 r12 = r3.a     // Catch:{ all -> 0x0f0c }
            java.lang.String r12 = r12.n()     // Catch:{ all -> 0x0f0c }
            java.lang.String r13 = r7.o()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.e r11 = r11.a(r12, r13)     // Catch:{ all -> 0x0f0c }
            if (r11 != 0) goto L_0x0bbb
            j.c.a.a.g.a.r4 r11 = r1.f2073i     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.n3 r11 = r11.a()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.p3 r11 = r11.v()     // Catch:{ all -> 0x0f0c }
            java.lang.String r12 = "Event being bundled has no eventAggregate. appId, eventName"
            j.c.a.a.f.e.u0 r13 = r3.a     // Catch:{ all -> 0x0f0c }
            java.lang.String r13 = r13.n()     // Catch:{ all -> 0x0f0c }
            r20 = r14
            java.lang.String r14 = r7.o()     // Catch:{ all -> 0x0f0c }
            r11.a(r12, r13, r14)     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.r4 r11 = r1.f2073i     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.i9 r11 = r11.k()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.u0 r12 = r3.a     // Catch:{ all -> 0x0f0c }
            java.lang.String r12 = r12.n()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.b3<java.lang.Boolean> r13 = j.c.a.a.g.a.k.x0     // Catch:{ all -> 0x0f0c }
            boolean r11 = r11.e(r12, r13)     // Catch:{ all -> 0x0f0c }
            if (r11 == 0) goto L_0x0ba1
            j.c.a.a.g.a.e r11 = new j.c.a.a.g.a.e     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.u0 r12 = r3.a     // Catch:{ all -> 0x0f0c }
            java.lang.String r27 = r12.n()     // Catch:{ all -> 0x0f0c }
            java.lang.String r28 = r7.o()     // Catch:{ all -> 0x0f0c }
            r29 = 1
            r31 = 1
            r33 = 1
            long r35 = r7.p()     // Catch:{ all -> 0x0f0c }
            r37 = 0
            r39 = 0
            r40 = 0
            r41 = 0
            r42 = 0
            r26 = r11
            r26.<init>(r27, r28, r29, r31, r33, r35, r37, r39, r40, r41, r42)     // Catch:{ all -> 0x0f0c }
            goto L_0x0bbd
        L_0x0ba1:
            j.c.a.a.g.a.e r11 = new j.c.a.a.g.a.e     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.u0 r12 = r3.a     // Catch:{ all -> 0x0f0c }
            java.lang.String r44 = r12.n()     // Catch:{ all -> 0x0f0c }
            java.lang.String r45 = r7.o()     // Catch:{ all -> 0x0f0c }
            r46 = 1
            r48 = 1
            long r50 = r7.p()     // Catch:{ all -> 0x0f0c }
            r43 = r11
            r43.<init>(r44, r45, r46, r48, r50)     // Catch:{ all -> 0x0f0c }
            goto L_0x0bbd
        L_0x0bbb:
            r20 = r14
        L_0x0bbd:
            r52.k()     // Catch:{ all -> 0x0d49 }
            j.c.a.a.f.e.f5 r12 = r7.m()     // Catch:{ all -> 0x0d49 }
            j.c.a.a.f.e.x3 r12 = (j.c.a.a.f.e.x3) r12     // Catch:{ all -> 0x0d49 }
            j.c.a.a.f.e.q0 r12 = (j.c.a.a.f.e.q0) r12     // Catch:{ all -> 0x0d49 }
            java.lang.String r13 = "_eid"
            java.lang.Object r12 = j.c.a.a.g.a.u8.b(r12, r13)     // Catch:{ all -> 0x0d49 }
            java.lang.Long r12 = (java.lang.Long) r12     // Catch:{ all -> 0x0d49 }
            if (r12 == 0) goto L_0x0bd4
            r13 = 1
            goto L_0x0bd5
        L_0x0bd4:
            r13 = 0
        L_0x0bd5:
            java.lang.Boolean r13 = java.lang.Boolean.valueOf(r13)     // Catch:{ all -> 0x0d49 }
            r14 = 1
            if (r8 != r14) goto L_0x0c10
            j.c.a.a.f.e.f5 r8 = r7.m()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.x3 r8 = (j.c.a.a.f.e.x3) r8     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.q0 r8 = (j.c.a.a.f.e.q0) r8     // Catch:{ all -> 0x0f0c }
            r4.add(r8)     // Catch:{ all -> 0x0f0c }
            boolean r8 = r13.booleanValue()     // Catch:{ all -> 0x0f0c }
            if (r8 == 0) goto L_0x0c05
            java.lang.Long r8 = r11.f1967i     // Catch:{ all -> 0x0f0c }
            if (r8 != 0) goto L_0x0bf9
            java.lang.Long r8 = r11.f1968j     // Catch:{ all -> 0x0f0c }
            if (r8 != 0) goto L_0x0bf9
            java.lang.Boolean r8 = r11.f1969k     // Catch:{ all -> 0x0f0c }
            if (r8 == 0) goto L_0x0c05
        L_0x0bf9:
            r8 = 0
            j.c.a.a.g.a.e r10 = r11.a(r8, r8, r8)     // Catch:{ all -> 0x0f0c }
            java.lang.String r8 = r7.o()     // Catch:{ all -> 0x0f0c }
            r2.put(r8, r10)     // Catch:{ all -> 0x0f0c }
        L_0x0c05:
            r9.a(r6, r7)     // Catch:{ all -> 0x0f0c }
        L_0x0c08:
            r8 = r2
            r18 = r5
            r5 = r6
            r1 = 1
            goto L_0x0d0d
        L_0x0c10:
            int r14 = r5.nextInt(r8)     // Catch:{ all -> 0x0d49 }
            if (r14 != 0) goto L_0x0c54
            r52.k()     // Catch:{ all -> 0x0f0c }
            long r14 = (long) r8     // Catch:{ all -> 0x0f0c }
            java.lang.Long r8 = java.lang.Long.valueOf(r14)     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.u8.a(r7, r10, r8)     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.f5 r8 = r7.m()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.x3 r8 = (j.c.a.a.f.e.x3) r8     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.q0 r8 = (j.c.a.a.f.e.q0) r8     // Catch:{ all -> 0x0f0c }
            r4.add(r8)     // Catch:{ all -> 0x0f0c }
            boolean r8 = r13.booleanValue()     // Catch:{ all -> 0x0f0c }
            if (r8 == 0) goto L_0x0c3b
            java.lang.Long r8 = java.lang.Long.valueOf(r14)     // Catch:{ all -> 0x0f0c }
            r10 = 0
            j.c.a.a.g.a.e r11 = r11.a(r10, r8, r10)     // Catch:{ all -> 0x0f0c }
        L_0x0c3b:
            java.lang.String r8 = r7.o()     // Catch:{ all -> 0x0f0c }
            long r12 = r7.p()     // Catch:{ all -> 0x0f0c }
            r14 = r20
            j.c.a.a.g.a.e r10 = r11.a(r12, r14)     // Catch:{ all -> 0x0f0c }
            r2.put(r8, r10)     // Catch:{ all -> 0x0f0c }
            r8 = r2
            r18 = r5
            r5 = r6
            r1 = 1
            goto L_0x0d0a
        L_0x0c54:
            r14 = r20
            r18 = r5
            j.c.a.a.g.a.r4 r5 = r1.f2073i     // Catch:{ all -> 0x0d49 }
            j.c.a.a.g.a.i9 r5 = r5.k()     // Catch:{ all -> 0x0d49 }
            r20 = r6
            j.c.a.a.f.e.u0 r6 = r3.a     // Catch:{ all -> 0x0d49 }
            java.lang.String r6 = r6.n()     // Catch:{ all -> 0x0d49 }
            boolean r5 = r5.f(r6)     // Catch:{ all -> 0x0d49 }
            if (r5 == 0) goto L_0x0c8f
            java.lang.Long r5 = r11.h     // Catch:{ all -> 0x0d49 }
            if (r5 == 0) goto L_0x0c79
            java.lang.Long r5 = r11.h     // Catch:{ all -> 0x0f0c }
            long r5 = r5.longValue()     // Catch:{ all -> 0x0f0c }
            r21 = r2
            goto L_0x0c8a
        L_0x0c79:
            j.c.a.a.g.a.r4 r5 = r1.f2073i     // Catch:{ all -> 0x0d49 }
            r5.n()     // Catch:{ all -> 0x0d49 }
            long r5 = r7.q()     // Catch:{ all -> 0x0d49 }
            r21 = r2
            r1 = r22
            long r5 = j.c.a.a.g.a.y8.a(r5, r1)     // Catch:{ all -> 0x0d49 }
        L_0x0c8a:
            int r1 = (r5 > r14 ? 1 : (r5 == r14 ? 0 : -1))
            if (r1 == 0) goto L_0x0ca5
            goto L_0x0ca3
        L_0x0c8f:
            r21 = r2
            long r1 = r11.g     // Catch:{ all -> 0x0d49 }
            long r5 = r7.p()     // Catch:{ all -> 0x0d49 }
            long r5 = r5 - r1
            long r1 = java.lang.Math.abs(r5)     // Catch:{ all -> 0x0d49 }
            r5 = 86400000(0x5265c00, double:4.2687272E-316)
            int r22 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r22 < 0) goto L_0x0ca5
        L_0x0ca3:
            r1 = 1
            goto L_0x0ca6
        L_0x0ca5:
            r1 = 0
        L_0x0ca6:
            if (r1 == 0) goto L_0x0cf2
            r52.k()     // Catch:{ all -> 0x0d49 }
            r1 = 1
            java.lang.Long r5 = java.lang.Long.valueOf(r1)     // Catch:{ all -> 0x0d49 }
            r6 = r16
            j.c.a.a.g.a.u8.a(r7, r6, r5)     // Catch:{ all -> 0x0d49 }
            r52.k()     // Catch:{ all -> 0x0d49 }
            long r5 = (long) r8     // Catch:{ all -> 0x0d49 }
            java.lang.Long r8 = java.lang.Long.valueOf(r5)     // Catch:{ all -> 0x0d49 }
            j.c.a.a.g.a.u8.a(r7, r10, r8)     // Catch:{ all -> 0x0d49 }
            j.c.a.a.f.e.f5 r8 = r7.m()     // Catch:{ all -> 0x0d49 }
            j.c.a.a.f.e.x3 r8 = (j.c.a.a.f.e.x3) r8     // Catch:{ all -> 0x0d49 }
            j.c.a.a.f.e.q0 r8 = (j.c.a.a.f.e.q0) r8     // Catch:{ all -> 0x0d49 }
            r4.add(r8)     // Catch:{ all -> 0x0d49 }
            boolean r8 = r13.booleanValue()     // Catch:{ all -> 0x0d49 }
            if (r8 == 0) goto L_0x0ce0
            java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ all -> 0x0d49 }
            r6 = 1
            java.lang.Boolean r8 = java.lang.Boolean.valueOf(r6)     // Catch:{ all -> 0x0d49 }
            r6 = 0
            j.c.a.a.g.a.e r11 = r11.a(r6, r5, r8)     // Catch:{ all -> 0x0d49 }
        L_0x0ce0:
            java.lang.String r5 = r7.o()     // Catch:{ all -> 0x0d49 }
            long r12 = r7.p()     // Catch:{ all -> 0x0d49 }
            j.c.a.a.g.a.e r6 = r11.a(r12, r14)     // Catch:{ all -> 0x0d49 }
            r8 = r21
            r8.put(r5, r6)     // Catch:{ all -> 0x0d49 }
            goto L_0x0d08
        L_0x0cf2:
            r8 = r21
            r1 = 1
            boolean r5 = r13.booleanValue()     // Catch:{ all -> 0x0d49 }
            if (r5 == 0) goto L_0x0d08
            java.lang.String r5 = r7.o()     // Catch:{ all -> 0x0d49 }
            r6 = 0
            j.c.a.a.g.a.e r10 = r11.a(r12, r6, r6)     // Catch:{ all -> 0x0d49 }
            r8.put(r5, r10)     // Catch:{ all -> 0x0d49 }
        L_0x0d08:
            r5 = r20
        L_0x0d0a:
            r9.a(r5, r7)     // Catch:{ all -> 0x0d49 }
        L_0x0d0d:
            int r6 = r5 + 1
            r1 = r52
            r2 = r8
            r5 = r18
            goto L_0x09d5
        L_0x0d16:
            r8 = r2
            int r1 = r4.size()     // Catch:{ all -> 0x0d49 }
            int r2 = r9.n()     // Catch:{ all -> 0x0d49 }
            if (r1 >= r2) goto L_0x0d27
            r9.o()     // Catch:{ all -> 0x0d49 }
            r9.a(r4)     // Catch:{ all -> 0x0d49 }
        L_0x0d27:
            java.util.Set r1 = r8.entrySet()     // Catch:{ all -> 0x0d49 }
            java.util.Iterator r1 = r1.iterator()     // Catch:{ all -> 0x0d49 }
        L_0x0d2f:
            boolean r2 = r1.hasNext()     // Catch:{ all -> 0x0d49 }
            if (r2 == 0) goto L_0x0d50
            java.lang.Object r2 = r1.next()     // Catch:{ all -> 0x0d49 }
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2     // Catch:{ all -> 0x0d49 }
            j.c.a.a.g.a.n9 r4 = r52.e()     // Catch:{ all -> 0x0d49 }
            java.lang.Object r2 = r2.getValue()     // Catch:{ all -> 0x0d49 }
            j.c.a.a.g.a.e r2 = (j.c.a.a.g.a.e) r2     // Catch:{ all -> 0x0d49 }
            r4.a(r2)     // Catch:{ all -> 0x0d49 }
            goto L_0x0d2f
        L_0x0d49:
            r0 = move-exception
            r1 = r0
            r2 = r1
            r1 = r52
            goto L_0x0f0e
        L_0x0d50:
            r1 = r52
            j.c.a.a.g.a.r4 r2 = r1.f2073i     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.i9 r2 = r2.k()     // Catch:{ all -> 0x0f0c }
            java.lang.String r4 = r9.v()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.b3<java.lang.Boolean> r5 = j.c.a.a.g.a.k.H0     // Catch:{ all -> 0x0f0c }
            boolean r2 = r2.e(r4, r5)     // Catch:{ all -> 0x0f0c }
            if (r2 != 0) goto L_0x0d67
            a(r9)     // Catch:{ all -> 0x0f0c }
        L_0x0d67:
            j.c.a.a.f.e.u0 r2 = r3.a     // Catch:{ all -> 0x0f0c }
            java.lang.String r2 = r2.n()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.n9 r4 = r52.e()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.e4 r4 = r4.b(r2)     // Catch:{ all -> 0x0f0c }
            if (r4 != 0) goto L_0x0d91
            j.c.a.a.g.a.r4 r4 = r1.f2073i     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.n3 r4 = r4.a()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.p3 r4 = r4.u()     // Catch:{ all -> 0x0f0c }
            java.lang.String r5 = "Bundling raw events w/o app info. appId"
            j.c.a.a.f.e.u0 r6 = r3.a     // Catch:{ all -> 0x0f0c }
            java.lang.String r6 = r6.n()     // Catch:{ all -> 0x0f0c }
            java.lang.Object r6 = j.c.a.a.g.a.n3.a(r6)     // Catch:{ all -> 0x0f0c }
            r4.a(r5, r6)     // Catch:{ all -> 0x0f0c }
            goto L_0x0dec
        L_0x0d91:
            int r5 = r9.n()     // Catch:{ all -> 0x0f0c }
            if (r5 <= 0) goto L_0x0dec
            long r5 = r4.m()     // Catch:{ all -> 0x0f0c }
            r7 = 0
            int r10 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r10 == 0) goto L_0x0da5
            r9.d(r5)     // Catch:{ all -> 0x0f0c }
            goto L_0x0da8
        L_0x0da5:
            r9.u()     // Catch:{ all -> 0x0f0c }
        L_0x0da8:
            long r7 = r4.l()     // Catch:{ all -> 0x0f0c }
            r10 = 0
            int r12 = (r7 > r10 ? 1 : (r7 == r10 ? 0 : -1))
            if (r12 != 0) goto L_0x0db3
            goto L_0x0db4
        L_0x0db3:
            r5 = r7
        L_0x0db4:
            int r7 = (r5 > r10 ? 1 : (r5 == r10 ? 0 : -1))
            if (r7 == 0) goto L_0x0dbc
            r9.c(r5)     // Catch:{ all -> 0x0f0c }
            goto L_0x0dbf
        L_0x0dbc:
            r9.t()     // Catch:{ all -> 0x0f0c }
        L_0x0dbf:
            r4.x()     // Catch:{ all -> 0x0f0c }
            long r5 = r4.u()     // Catch:{ all -> 0x0f0c }
            int r6 = (int) r5     // Catch:{ all -> 0x0f0c }
            r9.e(r6)     // Catch:{ all -> 0x0f0c }
            long r5 = r9.r()     // Catch:{ all -> 0x0f0c }
            r4.a(r5)     // Catch:{ all -> 0x0f0c }
            long r5 = r9.s()     // Catch:{ all -> 0x0f0c }
            r4.b(r5)     // Catch:{ all -> 0x0f0c }
            java.lang.String r5 = r4.a()     // Catch:{ all -> 0x0f0c }
            if (r5 == 0) goto L_0x0de2
            r9.j(r5)     // Catch:{ all -> 0x0f0c }
            goto L_0x0de5
        L_0x0de2:
            r9.w()     // Catch:{ all -> 0x0f0c }
        L_0x0de5:
            j.c.a.a.g.a.n9 r5 = r52.e()     // Catch:{ all -> 0x0f0c }
            r5.a(r4)     // Catch:{ all -> 0x0f0c }
        L_0x0dec:
            int r4 = r9.n()     // Catch:{ all -> 0x0f0c }
            if (r4 <= 0) goto L_0x0e52
            j.c.a.a.g.a.r4 r4 = r1.f2073i     // Catch:{ all -> 0x0f0c }
            r4.h()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.m4 r4 = r52.c()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.u0 r5 = r3.a     // Catch:{ all -> 0x0f0c }
            java.lang.String r5 = r5.n()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.k0 r4 = r4.a(r5)     // Catch:{ all -> 0x0f0c }
            if (r4 == 0) goto L_0x0e16
            boolean r5 = r4.a()     // Catch:{ all -> 0x0f0c }
            if (r5 != 0) goto L_0x0e0e
            goto L_0x0e16
        L_0x0e0e:
            long r4 = r4.i()     // Catch:{ all -> 0x0f0c }
            r9.g(r4)     // Catch:{ all -> 0x0f0c }
            goto L_0x0e41
        L_0x0e16:
            j.c.a.a.f.e.u0 r4 = r3.a     // Catch:{ all -> 0x0f0c }
            java.lang.String r4 = r4.i()     // Catch:{ all -> 0x0f0c }
            boolean r4 = android.text.TextUtils.isEmpty(r4)     // Catch:{ all -> 0x0f0c }
            if (r4 == 0) goto L_0x0e28
            r4 = -1
            r9.g(r4)     // Catch:{ all -> 0x0f0c }
            goto L_0x0e41
        L_0x0e28:
            j.c.a.a.g.a.r4 r4 = r1.f2073i     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.n3 r4 = r4.a()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.p3 r4 = r4.v()     // Catch:{ all -> 0x0f0c }
            java.lang.String r5 = "Did not find measurement config or missing version info. appId"
            j.c.a.a.f.e.u0 r6 = r3.a     // Catch:{ all -> 0x0f0c }
            java.lang.String r6 = r6.n()     // Catch:{ all -> 0x0f0c }
            java.lang.Object r6 = j.c.a.a.g.a.n3.a(r6)     // Catch:{ all -> 0x0f0c }
            r4.a(r5, r6)     // Catch:{ all -> 0x0f0c }
        L_0x0e41:
            j.c.a.a.g.a.n9 r4 = r52.e()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.f5 r5 = r9.m()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.x3 r5 = (j.c.a.a.f.e.x3) r5     // Catch:{ all -> 0x0f0c }
            j.c.a.a.f.e.u0 r5 = (j.c.a.a.f.e.u0) r5     // Catch:{ all -> 0x0f0c }
            r11 = r19
            r4.a(r5, r11)     // Catch:{ all -> 0x0f0c }
        L_0x0e52:
            j.c.a.a.g.a.n9 r4 = r52.e()     // Catch:{ all -> 0x0f0c }
            java.util.List<java.lang.Long> r3 = r3.b     // Catch:{ all -> 0x0f0c }
            i.b.k.ResourcesFlusher.b(r3)     // Catch:{ all -> 0x0f0c }
            r4.d()     // Catch:{ all -> 0x0f0c }
            r4.o()     // Catch:{ all -> 0x0f0c }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0f0c }
            java.lang.String r6 = "rowid in ("
            r5.<init>(r6)     // Catch:{ all -> 0x0f0c }
            r6 = 0
        L_0x0e69:
            int r7 = r3.size()     // Catch:{ all -> 0x0f0c }
            if (r6 >= r7) goto L_0x0e86
            if (r6 == 0) goto L_0x0e76
            java.lang.String r7 = ","
            r5.append(r7)     // Catch:{ all -> 0x0f0c }
        L_0x0e76:
            java.lang.Object r7 = r3.get(r6)     // Catch:{ all -> 0x0f0c }
            java.lang.Long r7 = (java.lang.Long) r7     // Catch:{ all -> 0x0f0c }
            long r7 = r7.longValue()     // Catch:{ all -> 0x0f0c }
            r5.append(r7)     // Catch:{ all -> 0x0f0c }
            int r6 = r6 + 1
            goto L_0x0e69
        L_0x0e86:
            java.lang.String r6 = ")"
            r5.append(r6)     // Catch:{ all -> 0x0f0c }
            android.database.sqlite.SQLiteDatabase r6 = r4.v()     // Catch:{ all -> 0x0f0c }
            java.lang.String r7 = "raw_events"
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0f0c }
            r8 = 0
            int r5 = r6.delete(r7, r5, r8)     // Catch:{ all -> 0x0f0c }
            int r6 = r3.size()     // Catch:{ all -> 0x0f0c }
            if (r5 == r6) goto L_0x0eb9
            j.c.a.a.g.a.n3 r4 = r4.a()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.p3 r4 = r4.u()     // Catch:{ all -> 0x0f0c }
            java.lang.String r6 = "Deleted fewer rows from raw events table than expected"
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ all -> 0x0f0c }
            int r3 = r3.size()     // Catch:{ all -> 0x0f0c }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x0f0c }
            r4.a(r6, r5, r3)     // Catch:{ all -> 0x0f0c }
        L_0x0eb9:
            j.c.a.a.g.a.n9 r3 = r52.e()     // Catch:{ all -> 0x0f0c }
            android.database.sqlite.SQLiteDatabase r4 = r3.v()     // Catch:{ all -> 0x0f0c }
            java.lang.String r5 = "delete from raw_events_metadata where app_id=? and metadata_fingerprint not in (select distinct metadata_fingerprint from raw_events where app_id=?)"
            r6 = 2
            java.lang.String[] r6 = new java.lang.String[r6]     // Catch:{ SQLiteException -> 0x0ed0 }
            r7 = 0
            r6[r7] = r2     // Catch:{ SQLiteException -> 0x0ed0 }
            r7 = 1
            r6[r7] = r2     // Catch:{ SQLiteException -> 0x0ed0 }
            r4.execSQL(r5, r6)     // Catch:{ SQLiteException -> 0x0ed0 }
            goto L_0x0ee3
        L_0x0ed0:
            r0 = move-exception
            r4 = r0
            j.c.a.a.g.a.n3 r3 = r3.a()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.p3 r3 = r3.u()     // Catch:{ all -> 0x0f0c }
            java.lang.String r5 = "Failed to remove unused event metadata. appId"
            java.lang.Object r2 = j.c.a.a.g.a.n3.a(r2)     // Catch:{ all -> 0x0f0c }
            r3.a(r5, r2, r4)     // Catch:{ all -> 0x0f0c }
        L_0x0ee3:
            j.c.a.a.g.a.n9 r2 = r52.e()     // Catch:{ all -> 0x0f0c }
            r2.u()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.n9 r2 = r52.e()
            r2.A()
            r2 = 1
            return r2
        L_0x0ef3:
            j.c.a.a.g.a.n9 r2 = r52.e()     // Catch:{ all -> 0x0f0c }
            r2.u()     // Catch:{ all -> 0x0f0c }
            j.c.a.a.g.a.n9 r2 = r52.e()
            r2.A()
            r2 = 0
            return r2
        L_0x0f03:
            r0 = move-exception
            r2 = r0
            r4 = r7
        L_0x0f06:
            if (r4 == 0) goto L_0x0f0b
            r4.close()     // Catch:{ all -> 0x0f0c }
        L_0x0f0b:
            throw r2     // Catch:{ all -> 0x0f0c }
        L_0x0f0c:
            r0 = move-exception
            r2 = r0
        L_0x0f0e:
            j.c.a.a.g.a.n9 r3 = r52.e()
            r3.A()
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.q8.a(long):boolean");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.g.a.u8.a(j.c.a.a.f.e.q0$a, java.lang.String, java.lang.Object):void
     arg types: [j.c.a.a.f.e.q0$a, java.lang.String, long]
     candidates:
      j.c.a.a.g.a.u8.a(boolean, boolean, boolean):java.lang.String
      j.c.a.a.g.a.u8.a(java.lang.StringBuilder, int, j.c.a.a.f.e.b0):void
      j.c.a.a.g.a.u8.a(j.c.a.a.f.e.q0$a, java.lang.String, java.lang.Object):void */
    public final void b(q0.a aVar, q0.a aVar2) {
        ResourcesFlusher.a("_e".equals(aVar.o()));
        k();
        s0 a2 = u8.a((q0) ((x3) aVar.m()), "_et");
        if (a2.j()) {
            long j2 = a2.zzf;
            if (j2 > 0) {
                k();
                s0 a3 = u8.a((q0) ((x3) aVar2.m()), "_et");
                if (a3 != null) {
                    long j3 = a3.zzf;
                    if (j3 > 0) {
                        j2 += j3;
                    }
                }
                k();
                u8.a(aVar2, "_et", Long.valueOf(j2));
                k();
                u8.a(aVar, "_fr", (Object) 1L);
            }
        }
    }

    public final void b() {
        r();
        if (this.f2081q || this.f2082r || this.f2083s) {
            this.f2073i.a().f2052n.a("Not stopping services. fetch, network, upload", Boolean.valueOf(this.f2081q), Boolean.valueOf(this.f2082r), Boolean.valueOf(this.f2083s));
            return;
        }
        this.f2073i.a().f2052n.a("Stopping uploading service(s)");
        List<Runnable> list = this.f2078n;
        if (list != null) {
            for (Runnable run : list) {
                run.run();
            }
            this.f2078n.clear();
        }
    }

    public final Boolean b(e4 e4Var) {
        try {
            if (e4Var.o() != -2147483648L) {
                if (e4Var.o() == ((long) j.c.a.a.c.o.b.b(this.f2073i.a).b(e4Var.g(), 0).versionCode)) {
                    return true;
                }
            } else {
                String str = j.c.a.a.c.o.b.b(this.f2073i.a).b(e4Var.g(), 0).versionName;
                if (e4Var.n() != null && e4Var.n().equals(str)) {
                    return true;
                }
            }
            return false;
        } catch (PackageManager.NameNotFoundException unused) {
            return null;
        }
    }

    public final void b(x8 x8Var, d9 d9Var) {
        r();
        m();
        if (TextUtils.isEmpty(d9Var.c) && TextUtils.isEmpty(d9Var.f1963s)) {
            return;
        }
        if (!d9Var.f1953i) {
            b(d9Var);
        } else if (!this.f2073i.g.d(d9Var.b, k.i0)) {
            this.f2073i.a().f2051m.a("Removing user property", this.f2073i.o().c(x8Var.c));
            e().z();
            try {
                b(d9Var);
                e().b(d9Var.b, x8Var.c);
                e().u();
                this.f2073i.a().f2051m.a("User property removed", this.f2073i.o().c(x8Var.c));
            } finally {
                e().A();
            }
        } else if (!"_npa".equals(x8Var.c) || d9Var.f1964t == null) {
            this.f2073i.a().f2051m.a("Removing user property", this.f2073i.o().c(x8Var.c));
            e().z();
            try {
                b(d9Var);
                e().b(d9Var.b, x8Var.c);
                e().u();
                this.f2073i.a().f2051m.a("User property removed", this.f2073i.o().c(x8Var.c));
            } finally {
                e().A();
            }
        } else {
            this.f2073i.a().f2051m.a("Falling back to manifest metadata value for ad personalization");
            if (((b) this.f2073i.f2092n) != null) {
                a(new x8("_npa", System.currentTimeMillis(), Long.valueOf(d9Var.f1964t.booleanValue() ? 1 : 0), "auto"), d9Var);
                return;
            }
            throw null;
        }
    }

    public final void b(g9 g9Var, d9 d9Var) {
        ResourcesFlusher.b(g9Var);
        ResourcesFlusher.b(g9Var.b);
        ResourcesFlusher.b(g9Var.d);
        ResourcesFlusher.b(g9Var.d.c);
        r();
        m();
        if (TextUtils.isEmpty(d9Var.c) && TextUtils.isEmpty(d9Var.f1963s)) {
            return;
        }
        if (!d9Var.f1953i) {
            b(d9Var);
            return;
        }
        e().z();
        try {
            b(d9Var);
            g9 d2 = e().d(g9Var.b, g9Var.d.c);
            if (d2 != null) {
                this.f2073i.a().f2051m.a("Removing conditional user property", g9Var.b, this.f2073i.o().c(g9Var.d.c));
                e().e(g9Var.b, g9Var.d.c);
                if (d2.f1999f) {
                    e().b(g9Var.b, g9Var.d.c);
                }
                if (g9Var.f2003l != null) {
                    Bundle bundle = null;
                    if (g9Var.f2003l.c != null) {
                        bundle = g9Var.f2003l.c.b();
                    }
                    Bundle bundle2 = bundle;
                    b(this.f2073i.n().a(g9Var.b, g9Var.f2003l.b, bundle2, d2.c, g9Var.f2003l.f2007e), d9Var);
                }
            } else {
                this.f2073i.a().f2047i.a("Conditional user property doesn't exist", n3.a(g9Var.b), this.f2073i.o().c(g9Var.d.c));
            }
            e().u();
        } finally {
            e().A();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0084  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00de  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0102  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0110  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x011e  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0141  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x014f  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x015d  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x018f  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x01b0  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final j.c.a.a.g.a.e4 b(j.c.a.a.g.a.d9 r12) {
        /*
            r11 = this;
            r11.r()
            r11.m()
            i.b.k.ResourcesFlusher.b(r12)
            java.lang.String r0 = r12.b
            i.b.k.ResourcesFlusher.b(r0)
            j.c.a.a.g.a.n9 r0 = r11.e()
            java.lang.String r1 = r12.b
            j.c.a.a.g.a.e4 r0 = r0.b(r1)
            j.c.a.a.g.a.r4 r1 = r11.f2073i
            j.c.a.a.g.a.w3 r1 = r1.l()
            java.lang.String r2 = r12.b
            java.lang.String r1 = r1.b(r2)
            r2 = 0
            r3 = 1
            if (r0 != 0) goto L_0x0042
            j.c.a.a.g.a.e4 r0 = new j.c.a.a.g.a.e4
            j.c.a.a.g.a.r4 r4 = r11.f2073i
            java.lang.String r5 = r12.b
            r0.<init>(r4, r5)
            j.c.a.a.g.a.r4 r4 = r11.f2073i
            j.c.a.a.g.a.y8 r4 = r4.n()
            java.lang.String r4 = r4.w()
            r0.a(r4)
            r0.d(r1)
            goto L_0x0063
        L_0x0042:
            j.c.a.a.g.a.r4 r4 = r0.a
            j.c.a.a.g.a.l4 r4 = r4.i()
            r4.d()
            java.lang.String r4 = r0.f1970e
            boolean r4 = r1.equals(r4)
            if (r4 != 0) goto L_0x0065
            r0.d(r1)
            j.c.a.a.g.a.r4 r1 = r11.f2073i
            j.c.a.a.g.a.y8 r1 = r1.n()
            java.lang.String r1 = r1.w()
            r0.a(r1)
        L_0x0063:
            r1 = 1
            goto L_0x0066
        L_0x0065:
            r1 = 0
        L_0x0066:
            java.lang.String r4 = r12.c
            java.lang.String r5 = r0.i()
            boolean r4 = android.text.TextUtils.equals(r4, r5)
            if (r4 != 0) goto L_0x0078
            java.lang.String r1 = r12.c
            r0.b(r1)
            r1 = 1
        L_0x0078:
            java.lang.String r4 = r12.f1963s
            java.lang.String r5 = r0.j()
            boolean r4 = android.text.TextUtils.equals(r4, r5)
            if (r4 != 0) goto L_0x008a
            java.lang.String r1 = r12.f1963s
            r0.c(r1)
            r1 = 1
        L_0x008a:
            java.lang.String r4 = r12.f1956l
            boolean r4 = android.text.TextUtils.isEmpty(r4)
            if (r4 != 0) goto L_0x00a4
            java.lang.String r4 = r12.f1956l
            java.lang.String r5 = r0.k()
            boolean r4 = r4.equals(r5)
            if (r4 != 0) goto L_0x00a4
            java.lang.String r1 = r12.f1956l
            r0.e(r1)
            r1 = 1
        L_0x00a4:
            long r4 = r12.f1952f
            r6 = 0
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x00ba
            long r8 = r0.q()
            int r10 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1))
            if (r10 == 0) goto L_0x00ba
            long r4 = r12.f1952f
            r0.d(r4)
            r1 = 1
        L_0x00ba:
            java.lang.String r4 = r12.d
            boolean r4 = android.text.TextUtils.isEmpty(r4)
            if (r4 != 0) goto L_0x00d4
            java.lang.String r4 = r12.d
            java.lang.String r5 = r0.n()
            boolean r4 = r4.equals(r5)
            if (r4 != 0) goto L_0x00d4
            java.lang.String r1 = r12.d
            r0.f(r1)
            r1 = 1
        L_0x00d4:
            long r4 = r12.f1955k
            long r8 = r0.o()
            int r10 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1))
            if (r10 == 0) goto L_0x00e4
            long r4 = r12.f1955k
            r0.c(r4)
            r1 = 1
        L_0x00e4:
            java.lang.String r4 = r12.f1951e
            if (r4 == 0) goto L_0x00f8
            java.lang.String r5 = r0.p()
            boolean r4 = r4.equals(r5)
            if (r4 != 0) goto L_0x00f8
            java.lang.String r1 = r12.f1951e
            r0.g(r1)
            r1 = 1
        L_0x00f8:
            long r4 = r12.g
            long r8 = r0.r()
            int r10 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1))
            if (r10 == 0) goto L_0x0108
            long r4 = r12.g
            r0.e(r4)
            r1 = 1
        L_0x0108:
            boolean r4 = r12.f1953i
            boolean r5 = r0.t()
            if (r4 == r5) goto L_0x0116
            boolean r1 = r12.f1953i
            r0.a(r1)
            r1 = 1
        L_0x0116:
            java.lang.String r4 = r12.h
            boolean r4 = android.text.TextUtils.isEmpty(r4)
            if (r4 != 0) goto L_0x0137
            java.lang.String r4 = r12.h
            j.c.a.a.g.a.r4 r5 = r0.a
            j.c.a.a.g.a.l4 r5 = r5.i()
            r5.d()
            java.lang.String r5 = r0.C
            boolean r4 = r4.equals(r5)
            if (r4 != 0) goto L_0x0137
            java.lang.String r1 = r12.h
            r0.h(r1)
            r1 = 1
        L_0x0137:
            long r4 = r12.f1957m
            long r8 = r0.b()
            int r10 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1))
            if (r10 == 0) goto L_0x0147
            long r4 = r12.f1957m
            r0.j(r4)
            r1 = 1
        L_0x0147:
            boolean r4 = r12.f1960p
            boolean r5 = r0.c()
            if (r4 == r5) goto L_0x0155
            boolean r1 = r12.f1960p
            r0.b(r1)
            r1 = 1
        L_0x0155:
            boolean r4 = r12.f1961q
            boolean r5 = r0.d()
            if (r4 == r5) goto L_0x0163
            boolean r1 = r12.f1961q
            r0.c(r1)
            r1 = 1
        L_0x0163:
            j.c.a.a.g.a.r4 r4 = r11.f2073i
            j.c.a.a.g.a.i9 r4 = r4.g
            java.lang.String r5 = r12.b
            j.c.a.a.g.a.b3<java.lang.Boolean> r8 = j.c.a.a.g.a.k.i0
            boolean r4 = r4.d(r5, r8)
            if (r4 == 0) goto L_0x0199
            java.lang.Boolean r4 = r12.f1964t
            java.lang.Boolean r5 = r0.e()
            if (r4 == r5) goto L_0x0199
            java.lang.Boolean r1 = r12.f1964t
            j.c.a.a.g.a.r4 r4 = r0.a
            j.c.a.a.g.a.l4 r4 = r4.i()
            r4.d()
            java.lang.Boolean r4 = r0.f1983t
            if (r4 != 0) goto L_0x018c
            if (r1 != 0) goto L_0x018c
            r2 = 1
            goto L_0x0193
        L_0x018c:
            if (r4 != 0) goto L_0x018f
            goto L_0x0193
        L_0x018f:
            boolean r2 = r4.equals(r1)
        L_0x0193:
            r2 = r2 ^ r3
            r0.D = r2
            r0.f1983t = r1
            r1 = 1
        L_0x0199:
            long r4 = r12.u
            int r2 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r2 == 0) goto L_0x01ad
            long r6 = r0.s()
            int r2 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r2 == 0) goto L_0x01ad
            long r1 = r12.u
            r0.f(r1)
            goto L_0x01ae
        L_0x01ad:
            r3 = r1
        L_0x01ae:
            if (r3 == 0) goto L_0x01b7
            j.c.a.a.g.a.n9 r12 = r11.e()
            r12.a(r0)
        L_0x01b7:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.q8.b(j.c.a.a.g.a.d9):j.c.a.a.g.a.e4");
    }

    public static void a(u0.a aVar) {
        aVar.a((long) RecyclerView.FOREVER_NS);
        aVar.b(Long.MIN_VALUE);
        for (int i2 = 0; i2 < aVar.n(); i2++) {
            q0 a2 = aVar.a(i2);
            if (a2.zzf < aVar.r()) {
                aVar.a(a2.zzf);
            }
            if (a2.zzf > aVar.s()) {
                aVar.b(a2.zzf);
            }
        }
    }

    public final void a(u0.a aVar, long j2, boolean z) {
        z8 z8Var;
        String str = z ? "_se" : "_lte";
        z8 c2 = e().c(aVar.v(), str);
        if (c2 == null || c2.f2143e == null) {
            String v2 = aVar.v();
            if (((b) this.f2073i.f2092n) != null) {
                z8Var = new z8(v2, "auto", str, System.currentTimeMillis(), Long.valueOf(j2));
            } else {
                throw null;
            }
        } else {
            String v3 = aVar.v();
            if (((b) this.f2073i.f2092n) != null) {
                z8Var = new z8(v3, "auto", str, System.currentTimeMillis(), Long.valueOf(((Long) c2.f2143e).longValue() + j2));
            } else {
                throw null;
            }
        }
        y0.a m2 = y0.m();
        m2.i();
        y0.a((y0) m2.c, str);
        if (((b) this.f2073i.f2092n) != null) {
            m2.a(System.currentTimeMillis());
            m2.b(((Long) z8Var.f2143e).longValue());
            y0 y0Var = (y0) ((x3) m2.m());
            boolean z2 = false;
            int i2 = 0;
            while (true) {
                if (i2 >= ((u0) aVar.c).zzg.size()) {
                    break;
                } else if (str.equals(((u0) aVar.c).zzg.get(i2).zze)) {
                    aVar.i();
                    u0.a((u0) aVar.c, i2, y0Var);
                    z2 = true;
                    break;
                } else {
                    i2++;
                }
            }
            if (!z2) {
                aVar.i();
                u0.a((u0) aVar.c, y0Var);
            }
            if (j2 > 0) {
                e().a(z8Var);
                this.f2073i.a().f2051m.a("Updated engagement user property. scope, value", z ? "session-scoped" : "lifetime", z8Var.f2143e);
                return;
            }
            return;
        }
        throw null;
    }

    public final boolean a(q0.a aVar, q0.a aVar2) {
        String str;
        ResourcesFlusher.a("_e".equals(aVar.o()));
        k();
        s0 a2 = u8.a((q0) ((x3) aVar.m()), "_sc");
        String str2 = null;
        if (a2 == null) {
            str = null;
        } else {
            str = a2.zze;
        }
        k();
        s0 a3 = u8.a((q0) ((x3) aVar2.m()), "_pc");
        if (a3 != null) {
            str2 = a3.zze;
        }
        if (str2 == null || !str2.equals(str)) {
            return false;
        }
        b(aVar, aVar2);
        return true;
    }

    public static void a(q0.a aVar, String str) {
        List<s0> a2 = aVar.a();
        for (int i2 = 0; i2 < a2.size(); i2++) {
            if (str.equals(a2.get(i2).zzd)) {
                aVar.b(i2);
                return;
            }
        }
    }

    public static void a(q0.a aVar, int i2, String str) {
        List<s0> a2 = aVar.a();
        int i3 = 0;
        while (i3 < a2.size()) {
            if (!"_err".equals(a2.get(i3).zzd)) {
                i3++;
            } else {
                return;
            }
        }
        s0.a p2 = s0.p();
        p2.i();
        s0.a((s0) p2.c, "_err");
        p2.a(Long.valueOf((long) i2).longValue());
        s0.a p3 = s0.p();
        p3.i();
        s0.a((s0) p3.c, "_ev");
        p3.i();
        s0.b((s0) p3.c, str);
        aVar.i();
        q0.a((q0) aVar.c, (s0) ((x3) p2.m()));
        aVar.i();
        q0.a((q0) aVar.c, (s0) ((x3) p3.m()));
    }

    public final void a(e4 e4Var) {
        r();
        if (!TextUtils.isEmpty(e4Var.i()) || !TextUtils.isEmpty(e4Var.j())) {
            i9 i9Var = this.f2073i.g;
            Uri.Builder builder = new Uri.Builder();
            String i2 = e4Var.i();
            if (TextUtils.isEmpty(i2)) {
                i2 = e4Var.j();
            }
            ArrayMap arrayMap = null;
            Uri.Builder encodedAuthority = builder.scheme(k.f2012j.a(null)).encodedAuthority(k.f2013k.a(null));
            String valueOf = String.valueOf(i2);
            Uri.Builder appendQueryParameter = encodedAuthority.path(valueOf.length() != 0 ? "config/app/".concat(valueOf) : new String("config/app/")).appendQueryParameter("app_instance_id", e4Var.h()).appendQueryParameter("platform", AbstractSpiCall.ANDROID_CLIENT_TYPE);
            i9Var.o();
            appendQueryParameter.appendQueryParameter("gmp_version", String.valueOf(18079L));
            String uri = builder.build().toString();
            try {
                URL url = new URL(uri);
                this.f2073i.a().f2052n.a("Fetching remote configuration", e4Var.g());
                k0 a2 = c().a(e4Var.g());
                m4 c2 = c();
                String g2 = e4Var.g();
                c2.d();
                String str = c2.f2041i.get(g2);
                if (a2 != null && !TextUtils.isEmpty(str)) {
                    arrayMap = new ArrayMap();
                    arrayMap.put("If-Modified-Since", str);
                }
                this.f2081q = true;
                r3 d2 = d();
                String g3 = e4Var.g();
                r8 r8Var = new r8(this);
                d2.d();
                d2.o();
                ResourcesFlusher.b(url);
                ResourcesFlusher.b(r8Var);
                d2.i().b(new v3(d2, g3, url, null, arrayMap, r8Var));
            } catch (MalformedURLException unused) {
                this.f2073i.a().f2046f.a("Failed to parse config URL. Not fetching. appId", n3.a(e4Var.g()), uri);
            }
        } else {
            a(e4Var.g(), 204, null, null, null);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:67:0x0136 A[Catch:{ all -> 0x0196, all -> 0x019f }] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x0195 A[SYNTHETIC, Splitter:B:82:0x0195] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(java.lang.String r8, int r9, java.lang.Throwable r10, byte[] r11, java.util.Map<java.lang.String, java.util.List<java.lang.String>> r12) {
        /*
            r7 = this;
            r7.r()
            r7.m()
            i.b.k.ResourcesFlusher.b(r8)
            r0 = 0
            if (r11 != 0) goto L_0x000e
            byte[] r11 = new byte[r0]     // Catch:{ all -> 0x019f }
        L_0x000e:
            j.c.a.a.g.a.r4 r1 = r7.f2073i     // Catch:{ all -> 0x019f }
            j.c.a.a.g.a.n3 r1 = r1.a()     // Catch:{ all -> 0x019f }
            j.c.a.a.g.a.p3 r1 = r1.f2052n     // Catch:{ all -> 0x019f }
            java.lang.String r2 = "onConfigFetched. Response size"
            int r3 = r11.length     // Catch:{ all -> 0x019f }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x019f }
            r1.a(r2, r3)     // Catch:{ all -> 0x019f }
            j.c.a.a.g.a.n9 r1 = r7.e()     // Catch:{ all -> 0x019f }
            r1.z()     // Catch:{ all -> 0x019f }
            j.c.a.a.g.a.n9 r1 = r7.e()     // Catch:{ all -> 0x0196 }
            j.c.a.a.g.a.e4 r1 = r1.b(r8)     // Catch:{ all -> 0x0196 }
            r2 = 200(0xc8, float:2.8E-43)
            r3 = 304(0x130, float:4.26E-43)
            r4 = 1
            if (r9 == r2) goto L_0x003c
            r2 = 204(0xcc, float:2.86E-43)
            if (r9 == r2) goto L_0x003c
            if (r9 != r3) goto L_0x0040
        L_0x003c:
            if (r10 != 0) goto L_0x0040
            r2 = 1
            goto L_0x0041
        L_0x0040:
            r2 = 0
        L_0x0041:
            if (r1 != 0) goto L_0x0056
            j.c.a.a.g.a.r4 r9 = r7.f2073i     // Catch:{ all -> 0x0196 }
            j.c.a.a.g.a.n3 r9 = r9.a()     // Catch:{ all -> 0x0196 }
            j.c.a.a.g.a.p3 r9 = r9.f2047i     // Catch:{ all -> 0x0196 }
            java.lang.String r10 = "App does not exist in onConfigFetched. appId"
            java.lang.Object r8 = j.c.a.a.g.a.n3.a(r8)     // Catch:{ all -> 0x0196 }
            r9.a(r10, r8)     // Catch:{ all -> 0x0196 }
            goto L_0x0181
        L_0x0056:
            r5 = 404(0x194, float:5.66E-43)
            r6 = 0
            if (r2 != 0) goto L_0x00d5
            if (r9 != r5) goto L_0x005f
            goto L_0x00d5
        L_0x005f:
            j.c.a.a.g.a.r4 r11 = r7.f2073i     // Catch:{ all -> 0x0196 }
            j.c.a.a.c.n.a r11 = r11.f2092n     // Catch:{ all -> 0x0196 }
            j.c.a.a.c.n.b r11 = (j.c.a.a.c.n.b) r11     // Catch:{ all -> 0x0196 }
            if (r11 == 0) goto L_0x00d4
            long r11 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0196 }
            r1.i(r11)     // Catch:{ all -> 0x0196 }
            j.c.a.a.g.a.n9 r11 = r7.e()     // Catch:{ all -> 0x0196 }
            r11.a(r1)     // Catch:{ all -> 0x0196 }
            j.c.a.a.g.a.r4 r11 = r7.f2073i     // Catch:{ all -> 0x0196 }
            j.c.a.a.g.a.n3 r11 = r11.a()     // Catch:{ all -> 0x0196 }
            j.c.a.a.g.a.p3 r11 = r11.f2052n     // Catch:{ all -> 0x0196 }
            java.lang.String r12 = "Fetching config failed. code, error"
            java.lang.Integer r1 = java.lang.Integer.valueOf(r9)     // Catch:{ all -> 0x0196 }
            r11.a(r12, r1, r10)     // Catch:{ all -> 0x0196 }
            j.c.a.a.g.a.m4 r10 = r7.c()     // Catch:{ all -> 0x0196 }
            r10.d()     // Catch:{ all -> 0x0196 }
            java.util.Map<java.lang.String, java.lang.String> r10 = r10.f2041i     // Catch:{ all -> 0x0196 }
            r10.put(r8, r6)     // Catch:{ all -> 0x0196 }
            j.c.a.a.g.a.r4 r8 = r7.f2073i     // Catch:{ all -> 0x0196 }
            j.c.a.a.g.a.w3 r8 = r8.l()     // Catch:{ all -> 0x0196 }
            j.c.a.a.g.a.b4 r8 = r8.f2112f     // Catch:{ all -> 0x0196 }
            j.c.a.a.g.a.r4 r10 = r7.f2073i     // Catch:{ all -> 0x0196 }
            j.c.a.a.c.n.a r10 = r10.f2092n     // Catch:{ all -> 0x0196 }
            j.c.a.a.c.n.b r10 = (j.c.a.a.c.n.b) r10     // Catch:{ all -> 0x0196 }
            if (r10 == 0) goto L_0x00d3
            long r10 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0196 }
            r8.a(r10)     // Catch:{ all -> 0x0196 }
            r8 = 503(0x1f7, float:7.05E-43)
            if (r9 == r8) goto L_0x00b3
            r8 = 429(0x1ad, float:6.01E-43)
            if (r9 != r8) goto L_0x00b2
            goto L_0x00b3
        L_0x00b2:
            r4 = 0
        L_0x00b3:
            if (r4 == 0) goto L_0x00ce
            j.c.a.a.g.a.r4 r8 = r7.f2073i     // Catch:{ all -> 0x0196 }
            j.c.a.a.g.a.w3 r8 = r8.l()     // Catch:{ all -> 0x0196 }
            j.c.a.a.g.a.b4 r8 = r8.g     // Catch:{ all -> 0x0196 }
            j.c.a.a.g.a.r4 r9 = r7.f2073i     // Catch:{ all -> 0x0196 }
            j.c.a.a.c.n.a r9 = r9.f2092n     // Catch:{ all -> 0x0196 }
            j.c.a.a.c.n.b r9 = (j.c.a.a.c.n.b) r9     // Catch:{ all -> 0x0196 }
            if (r9 == 0) goto L_0x00cd
            long r9 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0196 }
            r8.a(r9)     // Catch:{ all -> 0x0196 }
            goto L_0x00ce
        L_0x00cd:
            throw r6     // Catch:{ all -> 0x0196 }
        L_0x00ce:
            r7.u()     // Catch:{ all -> 0x0196 }
            goto L_0x0181
        L_0x00d3:
            throw r6     // Catch:{ all -> 0x0196 }
        L_0x00d4:
            throw r6     // Catch:{ all -> 0x0196 }
        L_0x00d5:
            if (r12 == 0) goto L_0x00e0
            java.lang.String r10 = "Last-Modified"
            java.lang.Object r10 = r12.get(r10)     // Catch:{ all -> 0x0196 }
            java.util.List r10 = (java.util.List) r10     // Catch:{ all -> 0x0196 }
            goto L_0x00e1
        L_0x00e0:
            r10 = r6
        L_0x00e1:
            if (r10 == 0) goto L_0x00f0
            int r12 = r10.size()     // Catch:{ all -> 0x0196 }
            if (r12 <= 0) goto L_0x00f0
            java.lang.Object r10 = r10.get(r0)     // Catch:{ all -> 0x0196 }
            java.lang.String r10 = (java.lang.String) r10     // Catch:{ all -> 0x0196 }
            goto L_0x00f1
        L_0x00f0:
            r10 = r6
        L_0x00f1:
            if (r9 == r5) goto L_0x010d
            if (r9 != r3) goto L_0x00f6
            goto L_0x010d
        L_0x00f6:
            j.c.a.a.g.a.m4 r12 = r7.c()     // Catch:{ all -> 0x0196 }
            boolean r10 = r12.a(r8, r11, r10)     // Catch:{ all -> 0x0196 }
            if (r10 != 0) goto L_0x012e
            j.c.a.a.g.a.n9 r8 = r7.e()     // Catch:{ all -> 0x019f }
            r8.A()     // Catch:{ all -> 0x019f }
            r7.f2081q = r0
            r7.b()
            return
        L_0x010d:
            j.c.a.a.g.a.m4 r10 = r7.c()     // Catch:{ all -> 0x0196 }
            j.c.a.a.f.e.k0 r10 = r10.a(r8)     // Catch:{ all -> 0x0196 }
            if (r10 != 0) goto L_0x012e
            j.c.a.a.g.a.m4 r10 = r7.c()     // Catch:{ all -> 0x0196 }
            boolean r10 = r10.a(r8, r6, r6)     // Catch:{ all -> 0x0196 }
            if (r10 != 0) goto L_0x012e
            j.c.a.a.g.a.n9 r8 = r7.e()     // Catch:{ all -> 0x019f }
            r8.A()     // Catch:{ all -> 0x019f }
            r7.f2081q = r0
            r7.b()
            return
        L_0x012e:
            j.c.a.a.g.a.r4 r10 = r7.f2073i     // Catch:{ all -> 0x0196 }
            j.c.a.a.c.n.a r10 = r10.f2092n     // Catch:{ all -> 0x0196 }
            j.c.a.a.c.n.b r10 = (j.c.a.a.c.n.b) r10     // Catch:{ all -> 0x0196 }
            if (r10 == 0) goto L_0x0195
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0196 }
            r1.h(r2)     // Catch:{ all -> 0x0196 }
            j.c.a.a.g.a.n9 r10 = r7.e()     // Catch:{ all -> 0x0196 }
            r10.a(r1)     // Catch:{ all -> 0x0196 }
            if (r9 != r5) goto L_0x0154
            j.c.a.a.g.a.r4 r9 = r7.f2073i     // Catch:{ all -> 0x0196 }
            j.c.a.a.g.a.n3 r9 = r9.a()     // Catch:{ all -> 0x0196 }
            j.c.a.a.g.a.p3 r9 = r9.f2049k     // Catch:{ all -> 0x0196 }
            java.lang.String r10 = "Config not found. Using empty config. appId"
            r9.a(r10, r8)     // Catch:{ all -> 0x0196 }
            goto L_0x016a
        L_0x0154:
            j.c.a.a.g.a.r4 r8 = r7.f2073i     // Catch:{ all -> 0x0196 }
            j.c.a.a.g.a.n3 r8 = r8.a()     // Catch:{ all -> 0x0196 }
            j.c.a.a.g.a.p3 r8 = r8.f2052n     // Catch:{ all -> 0x0196 }
            java.lang.String r10 = "Successfully fetched config. Got network response. code, size"
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ all -> 0x0196 }
            int r11 = r11.length     // Catch:{ all -> 0x0196 }
            java.lang.Integer r11 = java.lang.Integer.valueOf(r11)     // Catch:{ all -> 0x0196 }
            r8.a(r10, r9, r11)     // Catch:{ all -> 0x0196 }
        L_0x016a:
            j.c.a.a.g.a.r3 r8 = r7.d()     // Catch:{ all -> 0x0196 }
            boolean r8 = r8.u()     // Catch:{ all -> 0x0196 }
            if (r8 == 0) goto L_0x017e
            boolean r8 = r7.t()     // Catch:{ all -> 0x0196 }
            if (r8 == 0) goto L_0x017e
            r7.n()     // Catch:{ all -> 0x0196 }
            goto L_0x0181
        L_0x017e:
            r7.u()     // Catch:{ all -> 0x0196 }
        L_0x0181:
            j.c.a.a.g.a.n9 r8 = r7.e()     // Catch:{ all -> 0x0196 }
            r8.u()     // Catch:{ all -> 0x0196 }
            j.c.a.a.g.a.n9 r8 = r7.e()     // Catch:{ all -> 0x019f }
            r8.A()     // Catch:{ all -> 0x019f }
            r7.f2081q = r0
            r7.b()
            return
        L_0x0195:
            throw r6     // Catch:{ all -> 0x0196 }
        L_0x0196:
            r8 = move-exception
            j.c.a.a.g.a.n9 r9 = r7.e()     // Catch:{ all -> 0x019f }
            r9.A()     // Catch:{ all -> 0x019f }
            throw r8     // Catch:{ all -> 0x019f }
        L_0x019f:
            r8 = move-exception
            r7.f2081q = r0
            r7.b()
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.q8.a(java.lang.String, int, java.lang.Throwable, byte[], java.util.Map):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.g.a.y8.a(java.lang.String, int, boolean):java.lang.String
     arg types: [java.lang.String, int, int]
     candidates:
      j.c.a.a.g.a.y8.a(int, java.lang.Object, boolean):java.lang.Object
      j.c.a.a.g.a.y8.a(android.os.Bundle, java.lang.String, java.lang.Object):void
      j.c.a.a.g.a.y8.a(java.lang.String, int, java.lang.String):boolean
      j.c.a.a.g.a.y8.a(java.lang.String, java.lang.String[], java.lang.String):boolean
      j.c.a.a.g.a.y8.a(java.lang.String, int, boolean):java.lang.String */
    public final void a(x8 x8Var, d9 d9Var) {
        e a2;
        r();
        m();
        if (TextUtils.isEmpty(d9Var.c) && TextUtils.isEmpty(d9Var.f1963s)) {
            return;
        }
        if (!d9Var.f1953i) {
            b(d9Var);
            return;
        }
        int b2 = this.f2073i.n().b(x8Var.c);
        if (b2 != 0) {
            this.f2073i.n();
            String a3 = y8.a(x8Var.c, 24, true);
            String str = x8Var.c;
            this.f2073i.n().a(d9Var.b, b2, "_ev", a3, str != null ? str.length() : 0);
            return;
        }
        int b3 = this.f2073i.n().b(x8Var.c, x8Var.a());
        if (b3 != 0) {
            this.f2073i.n();
            String a4 = y8.a(x8Var.c, 24, true);
            Object a5 = x8Var.a();
            this.f2073i.n().a(d9Var.b, b3, "_ev", a4, (a5 == null || (!(a5 instanceof String) && !(a5 instanceof CharSequence))) ? 0 : String.valueOf(a5).length());
            return;
        }
        Object c2 = this.f2073i.n().c(x8Var.c, x8Var.a());
        if (c2 != null) {
            if ("_sid".equals(x8Var.c) && this.f2073i.g.g(d9Var.b)) {
                long j2 = x8Var.d;
                String str2 = x8Var.g;
                long j3 = 0;
                z8 c3 = e().c(d9Var.b, "_sno");
                if (c3 != null) {
                    Object obj = c3.f2143e;
                    if (obj instanceof Long) {
                        j3 = ((Long) obj).longValue();
                        a(new x8("_sno", j2, Long.valueOf(j3 + 1), str2), d9Var);
                    }
                }
                if (c3 != null) {
                    this.f2073i.a().f2047i.a("Retrieved last session number from database does not contain a valid (long) value", c3.f2143e);
                }
                if (this.f2073i.g.d(d9Var.b, k.d0) && (a2 = e().a(d9Var.b, "_s")) != null) {
                    j3 = a2.c;
                    this.f2073i.a().f2052n.a("Backfill the session number. Last used session number", Long.valueOf(j3));
                }
                a(new x8("_sno", j2, Long.valueOf(j3 + 1), str2), d9Var);
            }
            z8 z8Var = new z8(d9Var.b, x8Var.g, x8Var.c, x8Var.d, c2);
            this.f2073i.a().f2051m.a("Setting user property", this.f2073i.o().c(z8Var.c), c2);
            e().z();
            try {
                b(d9Var);
                boolean a6 = e().a(z8Var);
                e().u();
                if (a6) {
                    this.f2073i.a().f2051m.a("User property set", this.f2073i.o().c(z8Var.c), z8Var.f2143e);
                } else {
                    this.f2073i.a().f2046f.a("Too many unique user properties are set. Ignoring user property", this.f2073i.o().c(z8Var.c), z8Var.f2143e);
                    this.f2073i.n().a(d9Var.b, 9, (String) null, (String) null, 0);
                }
            } finally {
                e().A();
            }
        }
    }

    public final void a(d9 d9Var) {
        int i2;
        int i3;
        int i4;
        e4 b2;
        e eVar;
        String str;
        long j2;
        x8 x8Var;
        long j3;
        Bundle bundle;
        PackageInfo packageInfo;
        ApplicationInfo applicationInfo;
        long j4;
        boolean z;
        n9 e2;
        String g2;
        z8 c2;
        d9 d9Var2 = d9Var;
        r();
        m();
        ResourcesFlusher.b(d9Var);
        ResourcesFlusher.b(d9Var2.b);
        if (!TextUtils.isEmpty(d9Var2.c) || !TextUtils.isEmpty(d9Var2.f1963s)) {
            e4 b3 = e().b(d9Var2.b);
            if (b3 != null && TextUtils.isEmpty(b3.i()) && !TextUtils.isEmpty(d9Var2.c)) {
                b3.h(0);
                e().a(b3);
                m4 c3 = c();
                String str2 = d9Var2.b;
                c3.d();
                c3.g.remove(str2);
            }
            if (!d9Var2.f1953i) {
                b(d9Var);
                return;
            }
            long j5 = d9Var2.f1958n;
            if (j5 == 0) {
                if (((b) this.f2073i.f2092n) != null) {
                    j5 = System.currentTimeMillis();
                } else {
                    throw null;
                }
            }
            if (this.f2073i.g.d(d9Var2.b, k.i0)) {
                c s2 = this.f2073i.s();
                s2.d();
                s2.g = null;
                s2.h = 0;
            }
            int i5 = d9Var2.f1959o;
            if (i5 == 0 || i5 == 1) {
                i2 = i5;
            } else {
                this.f2073i.a().f2047i.a("Incorrect app type, assuming installed app. appId, appType", n3.a(d9Var2.b), Integer.valueOf(i5));
                i2 = 0;
            }
            e().z();
            try {
                if (!this.f2073i.g.d(d9Var2.b, k.i0) || ((c2 = e().c(d9Var2.b, "_npa")) != null && !"auto".equals(c2.b))) {
                    i3 = i2;
                    i4 = 1;
                } else if (d9Var2.f1964t != null) {
                    x8 x8Var2 = r12;
                    i3 = i2;
                    z8 z8Var = c2;
                    i4 = 1;
                    x8 x8Var3 = new x8("_npa", j5, Long.valueOf(d9Var2.f1964t.booleanValue() ? 1 : 0), "auto");
                    if (z8Var == null || !z8Var.f2143e.equals(x8Var2.f2128e)) {
                        a(x8Var2, d9Var2);
                    }
                } else {
                    i3 = i2;
                    i4 = 1;
                    if (c2 != null) {
                        b(new x8("_npa", j5, null, "auto"), d9Var2);
                    }
                }
                b2 = e().b(d9Var2.b);
                if (b2 != null) {
                    this.f2073i.n();
                    if (y8.a(d9Var2.c, b2.i(), d9Var2.f1963s, b2.j())) {
                        this.f2073i.a().f2047i.a("New GMP App Id passed in. Removing cached database data. appId", n3.a(b2.g()));
                        e2 = e();
                        g2 = b2.g();
                        e2.o();
                        e2.d();
                        ResourcesFlusher.b(g2);
                        SQLiteDatabase v2 = e2.v();
                        String[] strArr = new String[i4];
                        strArr[0] = g2;
                        int delete = v2.delete("events", "app_id=?", strArr) + 0 + v2.delete("user_attributes", "app_id=?", strArr) + v2.delete("conditional_properties", "app_id=?", strArr) + v2.delete("apps", "app_id=?", strArr) + v2.delete("raw_events", "app_id=?", strArr) + v2.delete("raw_events_metadata", "app_id=?", strArr) + v2.delete("event_filters", "app_id=?", strArr) + v2.delete("property_filters", "app_id=?", strArr) + v2.delete("audience_filter_values", "app_id=?", strArr);
                        if (delete > 0) {
                            e2.a().f2052n.a("Deleted application data. app, records", g2, Integer.valueOf(delete));
                        }
                        b2 = null;
                    }
                }
            } catch (SQLiteException e3) {
                e2.a().f2046f.a("Error deleting application data. appId, error", n3.a(g2), e3);
            } catch (Throwable th) {
                e().A();
                throw th;
            }
            if (b2 != null) {
                if (b2.o() != -2147483648L) {
                    if (b2.o() != d9Var2.f1955k) {
                        Bundle bundle2 = new Bundle();
                        bundle2.putString("_pv", b2.n());
                        a(new i("_au", new h(bundle2), "auto", j5), d9Var2);
                    }
                } else if (b2.n() != null && !b2.n().equals(d9Var2.d)) {
                    Bundle bundle3 = new Bundle();
                    bundle3.putString("_pv", b2.n());
                    a(new i("_au", new h(bundle3), "auto", j5), d9Var2);
                }
            }
            b(d9Var);
            int i6 = i3;
            if (i6 == 0) {
                eVar = e().a(d9Var2.b, "_f");
            } else {
                eVar = i6 == i4 ? e().a(d9Var2.b, "_v") : null;
            }
            if (eVar == null) {
                long j6 = ((j5 / 3600000) + 1) * 3600000;
                if (i6 == 0) {
                    String str3 = "_dac";
                    str = "_et";
                    new x8("_fot", j5, Long.valueOf(j6), "auto");
                    a(x8Var, d9Var2);
                    i9 i9Var = this.f2073i.g;
                    String str4 = d9Var2.c;
                    if (i9Var != null) {
                        if (i9Var.d(str4, k.X)) {
                            r();
                            this.f2073i.w.a(d9Var2.b);
                        }
                        r();
                        m();
                        Bundle bundle4 = new Bundle();
                        bundle4.putLong("_c", 1);
                        bundle4.putLong(CrashlyticsController.FIREBASE_REALTIME, 1);
                        bundle4.putLong("_uwa", 0);
                        bundle4.putLong("_pfo", 0);
                        bundle4.putLong("_sys", 0);
                        bundle4.putLong("_sysu", 0);
                        if (this.f2073i.g.h(d9Var2.b)) {
                            j3 = 1;
                            bundle4.putLong(str, 1);
                        } else {
                            j3 = 1;
                        }
                        if (d9Var2.f1962r) {
                            bundle4.putLong(str3, j3);
                        }
                        if (this.f2073i.a.getPackageManager() == null) {
                            this.f2073i.a().f2046f.a("PackageManager is null, first open report might be inaccurate. appId", n3.a(d9Var2.b));
                            bundle = bundle4;
                        } else {
                            try {
                                packageInfo = j.c.a.a.c.o.b.b(this.f2073i.a).b(d9Var2.b, 0);
                            } catch (PackageManager.NameNotFoundException e4) {
                                this.f2073i.a().f2046f.a("Package info is null, first open report might be inaccurate. appId", n3.a(d9Var2.b), e4);
                                packageInfo = null;
                            }
                            if (packageInfo == null || packageInfo.firstInstallTime == 0) {
                                bundle = bundle4;
                            } else {
                                if (packageInfo.firstInstallTime != packageInfo.lastUpdateTime) {
                                    bundle4.putLong("_uwa", 1);
                                    z = false;
                                } else {
                                    z = true;
                                }
                                bundle = bundle4;
                                a(new x8("_fi", j5, Long.valueOf(z ? 1 : 0), "auto"), d9Var2);
                            }
                            try {
                                applicationInfo = j.c.a.a.c.o.b.b(this.f2073i.a).a(d9Var2.b, 0);
                            } catch (PackageManager.NameNotFoundException e5) {
                                this.f2073i.a().f2046f.a("Application info is null, first open report might be inaccurate. appId", n3.a(d9Var2.b), e5);
                                applicationInfo = null;
                            }
                            if (applicationInfo != null) {
                                if ((applicationInfo.flags & 1) != 0) {
                                    j4 = 1;
                                    bundle.putLong("_sys", 1);
                                } else {
                                    j4 = 1;
                                }
                                if ((applicationInfo.flags & 128) != 0) {
                                    bundle.putLong("_sysu", j4);
                                }
                            }
                        }
                        n9 e6 = e();
                        String str5 = d9Var2.b;
                        ResourcesFlusher.b(str5);
                        e6.d();
                        e6.o();
                        long h2 = e6.h(str5, "first_open_count");
                        if (h2 >= 0) {
                            bundle.putLong("_pfo", h2);
                        }
                        a(new i("_f", new h(bundle), "auto", j5), d9Var2);
                    } else {
                        throw null;
                    }
                } else {
                    str = "_et";
                    if (i6 == 1) {
                        a(new x8("_fvt", j5, Long.valueOf(j6), "auto"), d9Var2);
                        r();
                        m();
                        Bundle bundle5 = new Bundle();
                        bundle5.putLong("_c", 1);
                        bundle5.putLong(CrashlyticsController.FIREBASE_REALTIME, 1);
                        if (this.f2073i.g.h(d9Var2.b)) {
                            j2 = 1;
                            bundle5.putLong(str, 1);
                        } else {
                            j2 = 1;
                        }
                        if (d9Var2.f1962r) {
                            bundle5.putLong("_dac", j2);
                        }
                        a(new i("_v", new h(bundle5), "auto", j5), d9Var2);
                    }
                }
                if (!this.f2073i.g.d(d9Var2.b, k.h0)) {
                    Bundle bundle6 = new Bundle();
                    bundle6.putLong(str, 1);
                    if (this.f2073i.g.h(d9Var2.b)) {
                        bundle6.putLong("_fr", 1);
                    }
                    a(new i("_e", new h(bundle6), "auto", j5), d9Var2);
                }
            } else if (d9Var2.f1954j) {
                a(new i("_cd", new h(new Bundle()), "auto", j5), d9Var2);
            }
            e().u();
            e().A();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.g.a.d9.<init>(java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, java.lang.String, boolean, boolean, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>):void
     arg types: [java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, ?[OBJECT, ARRAY], boolean, int, java.lang.String, long, int, int, boolean, boolean, int, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>]
     candidates:
      j.c.a.a.g.a.d9.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, long, long, java.lang.String, boolean, boolean, long, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>):void
      j.c.a.a.g.a.d9.<init>(java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, java.lang.String, boolean, boolean, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>):void */
    public final d9 a(String str) {
        String str2 = str;
        e4 b2 = e().b(str2);
        if (b2 == null || TextUtils.isEmpty(b2.n())) {
            this.f2073i.a().f2051m.a("No app data available; dropping", str2);
            return null;
        }
        Boolean b3 = b(b2);
        if (b3 == null || b3.booleanValue()) {
            e4 e4Var = b2;
            return new d9(str, b2.i(), b2.n(), b2.o(), b2.p(), b2.q(), b2.r(), (String) null, b2.t(), false, b2.k(), e4Var.b(), 0L, 0, e4Var.c(), e4Var.d(), false, e4Var.j(), e4Var.e(), e4Var.s(), e4Var.f());
        }
        this.f2073i.a().f2046f.a("App version does not match; dropping. appId", n3.a(str));
        return null;
    }

    public final void a(g9 g9Var, d9 d9Var) {
        ResourcesFlusher.b(g9Var);
        ResourcesFlusher.b(g9Var.b);
        ResourcesFlusher.b((Object) g9Var.c);
        ResourcesFlusher.b(g9Var.d);
        ResourcesFlusher.b(g9Var.d.c);
        r();
        m();
        if (TextUtils.isEmpty(d9Var.c) && TextUtils.isEmpty(d9Var.f1963s)) {
            return;
        }
        if (!d9Var.f1953i) {
            b(d9Var);
            return;
        }
        g9 g9Var2 = new g9(g9Var);
        boolean z = false;
        g9Var2.f1999f = false;
        e().z();
        try {
            g9 d2 = e().d(g9Var2.b, g9Var2.d.c);
            if (d2 != null && !d2.c.equals(g9Var2.c)) {
                this.f2073i.a().f2047i.a("Updating a conditional user property with different origin. name, origin, origin (from DB)", this.f2073i.o().c(g9Var2.d.c), g9Var2.c, d2.c);
            }
            if (d2 != null && d2.f1999f) {
                g9Var2.c = d2.c;
                g9Var2.f1998e = d2.f1998e;
                g9Var2.f2000i = d2.f2000i;
                g9Var2.g = d2.g;
                g9Var2.f2001j = d2.f2001j;
                g9Var2.f1999f = d2.f1999f;
                g9Var2.d = new x8(g9Var2.d.c, d2.d.d, g9Var2.d.a(), d2.d.g);
            } else if (TextUtils.isEmpty(g9Var2.g)) {
                g9Var2.d = new x8(g9Var2.d.c, g9Var2.f1998e, g9Var2.d.a(), g9Var2.d.g);
                g9Var2.f1999f = true;
                z = true;
            }
            if (g9Var2.f1999f) {
                x8 x8Var = g9Var2.d;
                z8 z8Var = new z8(g9Var2.b, g9Var2.c, x8Var.c, x8Var.d, x8Var.a());
                if (e().a(z8Var)) {
                    this.f2073i.a().f2051m.a("User property updated immediately", g9Var2.b, this.f2073i.o().c(z8Var.c), z8Var.f2143e);
                } else {
                    this.f2073i.a().f2046f.a("(2)Too many active user properties, ignoring", n3.a(g9Var2.b), this.f2073i.o().c(z8Var.c), z8Var.f2143e);
                }
                if (z && g9Var2.f2001j != null) {
                    b(new i(g9Var2.f2001j, g9Var2.f1998e), d9Var);
                }
            }
            if (e().a(g9Var2)) {
                this.f2073i.a().f2051m.a("Conditional property added", g9Var2.b, this.f2073i.o().c(g9Var2.d.c), g9Var2.d.a());
            } else {
                this.f2073i.a().f2046f.a("Too many conditional properties, ignoring", n3.a(g9Var2.b), this.f2073i.o().c(g9Var2.d.c), g9Var2.d.a());
            }
            e().u();
        } finally {
            e().A();
        }
    }
}
