package j.c.a.a.g.a;

import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import i.b.k.ResourcesFlusher;
import j.a.a.a.outline;
import j.c.a.a.c.n.b;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class n3 extends n5 {
    public char c = 0;
    public long d = -1;

    /* renamed from: e  reason: collision with root package name */
    public String f2045e;

    /* renamed from: f  reason: collision with root package name */
    public final p3 f2046f = new p3(this, 6, false, false);
    public final p3 g = new p3(this, 6, true, false);
    public final p3 h = new p3(this, 6, false, true);

    /* renamed from: i  reason: collision with root package name */
    public final p3 f2047i = new p3(this, 5, false, false);

    /* renamed from: j  reason: collision with root package name */
    public final p3 f2048j = new p3(this, 5, true, false);

    /* renamed from: k  reason: collision with root package name */
    public final p3 f2049k = new p3(this, 5, false, true);

    /* renamed from: l  reason: collision with root package name */
    public final p3 f2050l = new p3(this, 4, false, false);

    /* renamed from: m  reason: collision with root package name */
    public final p3 f2051m = new p3(this, 3, false, false);

    /* renamed from: n  reason: collision with root package name */
    public final p3 f2052n = new p3(this, 2, false, false);

    public n3(r4 r4Var) {
        super(r4Var);
    }

    public static Object a(String str) {
        if (str == null) {
            return null;
        }
        return new o3(str);
    }

    public static String b(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        int lastIndexOf = str.lastIndexOf(46);
        if (lastIndexOf == -1) {
            return str;
        }
        return str.substring(0, lastIndexOf);
    }

    public final boolean r() {
        return false;
    }

    public final String t() {
        String str;
        synchronized (this) {
            if (this.f2045e == null) {
                if (this.a.d != null) {
                    this.f2045e = this.a.d;
                } else {
                    this.f2045e = k.f2010f.a(null);
                }
            }
            str = this.f2045e;
        }
        return str;
    }

    public final p3 u() {
        return this.f2046f;
    }

    public final p3 v() {
        return this.f2047i;
    }

    public final p3 w() {
        return this.f2049k;
    }

    public final p3 x() {
        return this.f2051m;
    }

    public final p3 y() {
        return this.f2052n;
    }

    public final String z() {
        long j2;
        Pair<String, Long> pair;
        a4 a4Var = l().d;
        a4Var.f1933e.d();
        a4Var.f1933e.d();
        long j3 = a4Var.f1933e.v().getLong(a4Var.a, 0);
        if (j3 == 0) {
            a4Var.a();
            j2 = 0;
        } else if (((b) a4Var.f1933e.a.f2092n) != null) {
            j2 = Math.abs(j3 - System.currentTimeMillis());
        } else {
            throw null;
        }
        long j4 = a4Var.d;
        if (j2 >= j4) {
            if (j2 > (j4 << 1)) {
                a4Var.a();
            } else {
                String string = a4Var.f1933e.v().getString(a4Var.c, null);
                long j5 = a4Var.f1933e.v().getLong(a4Var.b, 0);
                a4Var.a();
                if (string == null || j5 <= 0) {
                    pair = w3.B;
                } else {
                    pair = new Pair<>(string, Long.valueOf(j5));
                }
                if (pair != null || pair == w3.B) {
                    return null;
                }
                String valueOf = String.valueOf(pair.second);
                String str = (String) pair.first;
                StringBuilder sb = new StringBuilder(outline.a(str, valueOf.length() + 1));
                sb.append(valueOf);
                sb.append(":");
                sb.append(str);
                return sb.toString();
            }
        }
        pair = null;
        if (pair != null) {
        }
        return null;
    }

    public final void a(int i2, boolean z, boolean z2, String str, Object obj, Object obj2, Object obj3) {
        if (!z && Log.isLoggable(t(), i2)) {
            Log.println(i2, t(), a(false, str, obj, obj2, obj3));
        }
        if (!z2 && i2 >= 5) {
            ResourcesFlusher.b((Object) str);
            l4 l4Var = this.a.f2088j;
            if (l4Var == null) {
                a(6, "Scheduler not set. Not logging error/warn");
            } else if (!super.s()) {
                a(6, "Scheduler not initialized. Not logging error/warn");
            } else {
                if (i2 < 0) {
                    i2 = 0;
                }
                m3 m3Var = new m3(this, i2 >= 9 ? 8 : i2, str, obj, obj2, obj3);
                super.o();
                ResourcesFlusher.b(m3Var);
                l4Var.a((p4<?>) new p4(l4Var, m3Var, "Task exception on worker thread"));
            }
        }
    }

    public final boolean a(int i2) {
        return Log.isLoggable(t(), i2);
    }

    public final void a(int i2, String str) {
        Log.println(i2, t(), str);
    }

    public static String a(boolean z, String str, Object obj, Object obj2, Object obj3) {
        String str2 = "";
        if (str == null) {
            str = str2;
        }
        String a = a(z, obj);
        String a2 = a(z, obj2);
        String a3 = a(z, obj3);
        StringBuilder sb = new StringBuilder();
        if (!TextUtils.isEmpty(str)) {
            sb.append(str);
            str2 = ": ";
        }
        String str3 = ", ";
        if (!TextUtils.isEmpty(a)) {
            sb.append(str2);
            sb.append(a);
            str2 = str3;
        }
        if (!TextUtils.isEmpty(a2)) {
            sb.append(str2);
            sb.append(a2);
        } else {
            str3 = str2;
        }
        if (!TextUtils.isEmpty(a3)) {
            sb.append(str3);
            sb.append(a3);
        }
        return sb.toString();
    }

    public static String a(boolean z, Object obj) {
        String className;
        String str = "";
        if (obj == null) {
            return str;
        }
        if (obj instanceof Integer) {
            obj = Long.valueOf((long) ((Integer) obj).intValue());
        }
        int i2 = 0;
        if (obj instanceof Long) {
            if (!z) {
                return String.valueOf(obj);
            }
            Long l2 = (Long) obj;
            if (Math.abs(l2.longValue()) < 100) {
                return String.valueOf(obj);
            }
            if (String.valueOf(obj).charAt(0) == '-') {
                str = "-";
            }
            String valueOf = String.valueOf(Math.abs(l2.longValue()));
            long round = Math.round(Math.pow(10.0d, (double) (valueOf.length() - 1)));
            long round2 = Math.round(Math.pow(10.0d, (double) valueOf.length()) - 1.0d);
            StringBuilder sb = new StringBuilder(str.length() + str.length() + 43);
            sb.append(str);
            sb.append(round);
            sb.append("...");
            sb.append(str);
            sb.append(round2);
            return sb.toString();
        } else if (obj instanceof Boolean) {
            return String.valueOf(obj);
        } else {
            if (obj instanceof Throwable) {
                Throwable th = (Throwable) obj;
                StringBuilder sb2 = new StringBuilder(z ? th.getClass().getName() : th.toString());
                String b = b(r4.class.getCanonicalName());
                StackTraceElement[] stackTrace = th.getStackTrace();
                int length = stackTrace.length;
                while (true) {
                    if (i2 >= length) {
                        break;
                    }
                    StackTraceElement stackTraceElement = stackTrace[i2];
                    if (!stackTraceElement.isNativeMethod() && (className = stackTraceElement.getClassName()) != null && b(className).equals(b)) {
                        sb2.append(": ");
                        sb2.append(stackTraceElement);
                        break;
                    }
                    i2++;
                }
                return sb2.toString();
            } else if (obj instanceof o3) {
                return ((o3) obj).a;
            } else {
                if (z) {
                    return "-";
                }
                return String.valueOf(obj);
            }
        }
    }
}
