package j.c.a.a.f.e;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;

/* compiled from: com.google.android.gms:play-services-measurement-base@@17.0.1 */
public final class hb extends q implements fb {
    public hb(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.measurement.api.internal.IBundleReceiver");
    }

    public final void a(Bundle bundle) {
        Parcel g = g();
        c2.a(g, bundle);
        b(1, g);
    }
}
