package j.c.a.a.c;

import android.os.Parcel;
import android.os.Parcelable;
import com.crashlytics.android.core.DefaultAppMeasurementEventListenerRegistrar;
import i.b.k.ResourcesFlusher;
import j.c.a.a.c.l.n;
import j.c.a.a.c.l.p.a;
import java.util.Arrays;

public class d extends a {
    public static final Parcelable.Creator<d> CREATOR = new q();
    public final String b;
    @Deprecated
    public final int c;
    public final long d;

    public d(String str, int i2, long j2) {
        this.b = str;
        this.c = i2;
        this.d = j2;
    }

    public long c() {
        long j2 = this.d;
        return j2 == -1 ? (long) this.c : j2;
    }

    public boolean equals(Object obj) {
        if (obj instanceof d) {
            d dVar = (d) obj;
            String str = this.b;
            if (((str == null || !str.equals(dVar.b)) && (this.b != null || dVar.b != null)) || c() != dVar.c()) {
                return false;
            }
            return true;
        }
        return false;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.b, Long.valueOf(c())});
    }

    public String toString() {
        n d2 = ResourcesFlusher.d(this);
        d2.a(DefaultAppMeasurementEventListenerRegistrar.NAME, this.b);
        d2.a("version", Long.valueOf(c()));
        return d2.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, int, int, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int, boolean, int):int
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, org.xmlpull.v1.XmlPullParser, android.util.AttributeSet, android.content.res.Resources$Theme):android.content.res.ColorStateList
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, android.content.res.Resources$Theme, android.util.AttributeSet, int[]):android.content.res.TypedArray
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, org.xmlpull.v1.XmlPullParser, java.lang.String, int):java.lang.String
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      i.b.k.ResourcesFlusher.a(int, android.graphics.Rect, android.graphics.Rect, android.graphics.Rect):boolean
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, int, int, boolean):boolean
      i.b.k.ResourcesFlusher.a(i.f.a.h.d, i.f.a.h.f, java.util.List<i.f.a.h.f>, boolean):boolean
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, java.lang.String, boolean):void */
    public void writeToParcel(Parcel parcel, int i2) {
        int a = ResourcesFlusher.a(parcel);
        ResourcesFlusher.a(parcel, 1, this.b, false);
        ResourcesFlusher.a(parcel, 2, this.c);
        ResourcesFlusher.a(parcel, 3, c());
        ResourcesFlusher.k(parcel, a);
    }
}
