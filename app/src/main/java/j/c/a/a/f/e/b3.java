package j.c.a.a.f.e;

import com.google.android.gms.internal.measurement.zzek;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class b3 {
    public final zzek a;
    public final byte[] b;

    public /* synthetic */ b3(int i2, v2 v2Var) {
        byte[] bArr = new byte[i2];
        this.b = bArr;
        this.a = zzek.a(bArr);
    }

    public final w2 a() {
        if (this.a.a() == 0) {
            return new d3(this.b);
        }
        throw new IllegalStateException("Did not write as much data as expected.");
    }
}
