package j.c.a.a.f.e;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final /* synthetic */ class s1 implements b2 {
    public static final b2 b = new s1();

    /* JADX WARNING: Code restructure failed: missing block: B:67:0x011c, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0127, code lost:
        throw r1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a() {
        /*
            r10 = this;
            android.content.Context r0 = j.c.a.a.f.e.p1.g
            java.lang.String r1 = "HermeticFileOverrides"
            java.lang.String r2 = android.os.Build.TYPE
            java.lang.String r3 = android.os.Build.TAGS
            java.lang.String r4 = android.os.Build.HARDWARE
            java.lang.String r5 = "eng"
            boolean r5 = r2.equals(r5)
            r6 = 1
            r7 = 0
            if (r5 != 0) goto L_0x001d
            java.lang.String r5 = "userdebug"
            boolean r2 = r2.equals(r5)
            if (r2 != 0) goto L_0x001d
            goto L_0x0046
        L_0x001d:
            java.lang.String r2 = "goldfish"
            boolean r2 = r4.equals(r2)
            if (r2 != 0) goto L_0x0036
            java.lang.String r2 = "ranchu"
            boolean r2 = r4.equals(r2)
            if (r2 != 0) goto L_0x0036
            java.lang.String r2 = "robolectric"
            boolean r2 = r4.equals(r2)
            if (r2 != 0) goto L_0x0036
            goto L_0x0046
        L_0x0036:
            java.lang.String r2 = "dev-keys"
            boolean r2 = r3.contains(r2)
            if (r2 != 0) goto L_0x0048
            java.lang.String r2 = "test-keys"
            boolean r2 = r3.contains(r2)
            if (r2 != 0) goto L_0x0048
        L_0x0046:
            r2 = 0
            goto L_0x0049
        L_0x0048:
            r2 = 1
        L_0x0049:
            r3 = 0
            if (r2 != 0) goto L_0x0053
            j.c.a.a.f.e.m1 r0 = new j.c.a.a.f.e.m1
            r0.<init>(r3)
            goto L_0x0134
        L_0x0053:
            boolean r2 = j.c.a.a.f.e.f1.a()
            if (r2 == 0) goto L_0x0064
            boolean r2 = r0.isDeviceProtectedStorage()
            if (r2 == 0) goto L_0x0060
            goto L_0x0064
        L_0x0060:
            android.content.Context r0 = r0.createDeviceProtectedStorageContext()
        L_0x0064:
            android.os.StrictMode$ThreadPolicy r2 = android.os.StrictMode.allowThreadDiskReads()
            android.os.StrictMode.allowThreadDiskWrites()     // Catch:{ all -> 0x0135 }
            java.lang.String r4 = "hermetic_phenotype_overrides.txt"
            java.io.File r0 = r0.getFileStreamPath(r4)     // Catch:{ RuntimeException -> 0x0083 }
            boolean r4 = r0.exists()     // Catch:{ all -> 0x0135 }
            if (r4 == 0) goto L_0x007d
            j.c.a.a.f.e.d2 r4 = new j.c.a.a.f.e.d2     // Catch:{ all -> 0x0135 }
            r4.<init>(r0)     // Catch:{ all -> 0x0135 }
            goto L_0x007f
        L_0x007d:
            j.c.a.a.f.e.z1<java.lang.Object> r4 = j.c.a.a.f.e.z1.b     // Catch:{ all -> 0x0135 }
        L_0x007f:
            android.os.StrictMode.setThreadPolicy(r2)
            goto L_0x008e
        L_0x0083:
            r0 = move-exception
            java.lang.String r4 = "no data dir"
            android.util.Log.e(r1, r4, r0)     // Catch:{ all -> 0x0135 }
            j.c.a.a.f.e.z1<java.lang.Object> r4 = j.c.a.a.f.e.z1.b     // Catch:{ all -> 0x0135 }
            android.os.StrictMode.setThreadPolicy(r2)
        L_0x008e:
            boolean r0 = r4.a()
            if (r0 == 0) goto L_0x012f
            java.lang.Object r0 = r4.b()
            java.io.File r0 = (java.io.File) r0
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0128 }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0128 }
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0128 }
            r4.<init>(r0)     // Catch:{ IOException -> 0x0128 }
            r3.<init>(r4)     // Catch:{ IOException -> 0x0128 }
            r2.<init>(r3)     // Catch:{ IOException -> 0x0128 }
            java.util.HashMap r3 = new java.util.HashMap     // Catch:{ all -> 0x011a }
            r3.<init>()     // Catch:{ all -> 0x011a }
        L_0x00ae:
            java.lang.String r4 = r2.readLine()     // Catch:{ all -> 0x011a }
            if (r4 == 0) goto L_0x00f3
            java.lang.String r5 = " "
            r8 = 3
            java.lang.String[] r5 = r4.split(r5, r8)     // Catch:{ all -> 0x011a }
            int r9 = r5.length     // Catch:{ all -> 0x011a }
            if (r9 == r8) goto L_0x00d4
            java.lang.String r5 = "Invalid: "
            int r8 = r4.length()     // Catch:{ all -> 0x011a }
            if (r8 == 0) goto L_0x00cb
            java.lang.String r4 = r5.concat(r4)     // Catch:{ all -> 0x011a }
            goto L_0x00d0
        L_0x00cb:
            java.lang.String r4 = new java.lang.String     // Catch:{ all -> 0x011a }
            r4.<init>(r5)     // Catch:{ all -> 0x011a }
        L_0x00d0:
            android.util.Log.e(r1, r4)     // Catch:{ all -> 0x011a }
            goto L_0x00ae
        L_0x00d4:
            r4 = r5[r7]     // Catch:{ all -> 0x011a }
            r8 = r5[r6]     // Catch:{ all -> 0x011a }
            r9 = 2
            r5 = r5[r9]     // Catch:{ all -> 0x011a }
            boolean r9 = r3.containsKey(r4)     // Catch:{ all -> 0x011a }
            if (r9 != 0) goto L_0x00e9
            java.util.HashMap r9 = new java.util.HashMap     // Catch:{ all -> 0x011a }
            r9.<init>()     // Catch:{ all -> 0x011a }
            r3.put(r4, r9)     // Catch:{ all -> 0x011a }
        L_0x00e9:
            java.lang.Object r4 = r3.get(r4)     // Catch:{ all -> 0x011a }
            java.util.Map r4 = (java.util.Map) r4     // Catch:{ all -> 0x011a }
            r4.put(r8, r5)     // Catch:{ all -> 0x011a }
            goto L_0x00ae
        L_0x00f3:
            java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch:{ all -> 0x011a }
            int r4 = r0.length()     // Catch:{ all -> 0x011a }
            int r4 = r4 + 7
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x011a }
            r5.<init>(r4)     // Catch:{ all -> 0x011a }
            java.lang.String r4 = "Parsed "
            r5.append(r4)     // Catch:{ all -> 0x011a }
            r5.append(r0)     // Catch:{ all -> 0x011a }
            java.lang.String r0 = r5.toString()     // Catch:{ all -> 0x011a }
            android.util.Log.i(r1, r0)     // Catch:{ all -> 0x011a }
            j.c.a.a.f.e.m1 r0 = new j.c.a.a.f.e.m1     // Catch:{ all -> 0x011a }
            r0.<init>(r3)     // Catch:{ all -> 0x011a }
            r2.close()     // Catch:{ IOException -> 0x0128 }
            goto L_0x0134
        L_0x011a:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x011c }
        L_0x011c:
            r1 = move-exception
            r2.close()     // Catch:{ all -> 0x0121 }
            goto L_0x0127
        L_0x0121:
            r2 = move-exception
            j.c.a.a.f.e.h2 r3 = j.c.a.a.f.e.i2.a     // Catch:{ IOException -> 0x0128 }
            r3.a(r0, r2)     // Catch:{ IOException -> 0x0128 }
        L_0x0127:
            throw r1     // Catch:{ IOException -> 0x0128 }
        L_0x0128:
            r0 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            r1.<init>(r0)
            throw r1
        L_0x012f:
            j.c.a.a.f.e.m1 r0 = new j.c.a.a.f.e.m1
            r0.<init>(r3)
        L_0x0134:
            return r0
        L_0x0135:
            r0 = move-exception
            android.os.StrictMode.setThreadPolicy(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.f.e.s1.a():java.lang.Object");
    }
}
