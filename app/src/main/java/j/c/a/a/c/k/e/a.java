package j.c.a.a.c.k.e;

import android.app.Activity;
import android.app.Application;
import android.content.ComponentCallbacks2;
import android.content.res.Configuration;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.annotation.concurrent.GuardedBy;

public final class a implements Application.ActivityLifecycleCallbacks, ComponentCallbacks2 {

    /* renamed from: f  reason: collision with root package name */
    public static final a f1777f = new a();
    public final AtomicBoolean b = new AtomicBoolean();
    public final AtomicBoolean c = new AtomicBoolean();
    @GuardedBy("sInstance")
    public final ArrayList<C0021a> d = new ArrayList<>();
    @GuardedBy("sInstance")

    /* renamed from: e  reason: collision with root package name */
    public boolean f1778e = false;

    /* renamed from: j.c.a.a.c.k.e.a$a  reason: collision with other inner class name */
    public interface C0021a {
        void a(boolean z);
    }

    public static void a(Application application) {
        synchronized (f1777f) {
            if (!f1777f.f1778e) {
                application.registerActivityLifecycleCallbacks(f1777f);
                application.registerComponentCallbacks(f1777f);
                f1777f.f1778e = true;
            }
        }
    }

    public final void onActivityCreated(Activity activity, Bundle bundle) {
        boolean compareAndSet = this.b.compareAndSet(true, false);
        this.c.set(true);
        if (compareAndSet) {
            a(false);
        }
    }

    public final void onActivityDestroyed(Activity activity) {
    }

    public final void onActivityPaused(Activity activity) {
    }

    public final void onActivityResumed(Activity activity) {
        boolean compareAndSet = this.b.compareAndSet(true, false);
        this.c.set(true);
        if (compareAndSet) {
            a(false);
        }
    }

    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    public final void onActivityStarted(Activity activity) {
    }

    public final void onActivityStopped(Activity activity) {
    }

    public final void onConfigurationChanged(Configuration configuration) {
    }

    public final void onLowMemory() {
    }

    public final void onTrimMemory(int i2) {
        if (i2 == 20 && this.b.compareAndSet(false, true)) {
            this.c.set(true);
            a(true);
        }
    }

    public final void a(C0021a aVar) {
        synchronized (f1777f) {
            this.d.add(aVar);
        }
    }

    public final void a(boolean z) {
        synchronized (f1777f) {
            ArrayList<C0021a> arrayList = this.d;
            int size = arrayList.size();
            int i2 = 0;
            while (i2 < size) {
                C0021a aVar = arrayList.get(i2);
                i2++;
                aVar.a(z);
            }
        }
    }
}
