package j.c.a.a.c;

import android.os.Parcel;
import android.os.Parcelable;
import i.b.k.ResourcesFlusher;

public final class q implements Parcelable.Creator<d> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = ResourcesFlusher.b(parcel);
        String str = null;
        int i2 = 0;
        long j2 = -1;
        while (parcel.dataPosition() < b) {
            int readInt = parcel.readInt();
            int i3 = 65535 & readInt;
            if (i3 == 1) {
                str = ResourcesFlusher.b(parcel, readInt);
            } else if (i3 == 2) {
                i2 = ResourcesFlusher.f(parcel, readInt);
            } else if (i3 != 3) {
                ResourcesFlusher.i(parcel, readInt);
            } else {
                j2 = ResourcesFlusher.g(parcel, readInt);
            }
        }
        ResourcesFlusher.c(parcel, b);
        return new d(str, i2, j2);
    }

    public final /* synthetic */ Object[] newArray(int i2) {
        return new d[i2];
    }
}
