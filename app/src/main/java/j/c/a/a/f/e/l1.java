package j.c.a.a.f.e;

import android.content.Context;
import android.database.ContentObserver;
import i.b.k.ResourcesFlusher;
import javax.annotation.Nullable;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class l1 implements i1 {
    public static l1 c;
    @Nullable
    public final Context a;
    @Nullable
    public final ContentObserver b;

    public l1(Context context) {
        this.a = context;
        this.b = new n1();
        context.getContentResolver().registerContentObserver(d1.a, true, this.b);
    }

    public static l1 a(Context context) {
        l1 l1Var;
        synchronized (l1.class) {
            if (c == null) {
                c = ResourcesFlusher.a(context, "com.google.android.providers.gsf.permission.READ_GSERVICES") == 0 ? new l1(context) : new l1();
            }
            l1Var = c;
        }
        return l1Var;
    }

    public l1() {
        this.a = null;
        this.b = null;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(7:5|6|7|8|9|10|22) */
    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        android.os.Binder.restoreCallingIdentity(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0023, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0024, code lost:
        android.os.Binder.restoreCallingIdentity(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0027, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0028, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0029, code lost:
        r5 = java.lang.String.valueOf(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0033, code lost:
        if (r5.length() != 0) goto L_0x0035;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0035, code lost:
        r5 = "Unable to read GServices for: ".concat(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003a, code lost:
        r5 = new java.lang.String("Unable to read GServices for: ");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x003f, code lost:
        android.util.Log.e("GservicesLoader", r5, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:?, code lost:
        r2 = android.os.Binder.clearCallingIdentity();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:?, code lost:
        r0 = j.c.a.a.f.e.d1.a(r4.a.getContentResolver(), r5);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x0010 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(java.lang.String r5) {
        /*
            r4 = this;
            android.content.Context r0 = r4.a
            r1 = 0
            if (r0 != 0) goto L_0x0006
            goto L_0x0044
        L_0x0006:
            android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ SecurityException -> 0x0010 }
            java.lang.String r5 = j.c.a.a.f.e.d1.a(r0, r5)     // Catch:{ SecurityException -> 0x0010 }
            r1 = r5
            goto L_0x0044
        L_0x0010:
            long r2 = android.os.Binder.clearCallingIdentity()     // Catch:{ SecurityException -> 0x0028 }
            android.content.Context r0 = r4.a     // Catch:{ all -> 0x0023 }
            android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ all -> 0x0023 }
            java.lang.String r0 = j.c.a.a.f.e.d1.a(r0, r5)     // Catch:{ all -> 0x0023 }
            android.os.Binder.restoreCallingIdentity(r2)     // Catch:{ SecurityException -> 0x0028 }
            r1 = r0
            goto L_0x0044
        L_0x0023:
            r0 = move-exception
            android.os.Binder.restoreCallingIdentity(r2)     // Catch:{ SecurityException -> 0x0028 }
            throw r0     // Catch:{ SecurityException -> 0x0028 }
        L_0x0028:
            r0 = move-exception
            java.lang.String r2 = "Unable to read GServices for: "
            java.lang.String r5 = java.lang.String.valueOf(r5)
            int r3 = r5.length()
            if (r3 == 0) goto L_0x003a
            java.lang.String r5 = r2.concat(r5)
            goto L_0x003f
        L_0x003a:
            java.lang.String r5 = new java.lang.String
            r5.<init>(r2)
        L_0x003f:
            java.lang.String r2 = "GservicesLoader"
            android.util.Log.e(r2, r5, r0)
        L_0x0044:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.f.e.l1.a(java.lang.String):java.lang.Object");
    }

    public static synchronized void a() {
        synchronized (l1.class) {
            if (!(c == null || c.a == null || c.b == null)) {
                c.a.getContentResolver().unregisterContentObserver(c.b);
            }
            c = null;
        }
    }
}
