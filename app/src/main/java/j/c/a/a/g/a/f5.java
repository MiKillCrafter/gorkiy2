package j.c.a.a.g.a;

import android.text.TextUtils;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class f5 implements Runnable {
    public final /* synthetic */ i b;
    public final /* synthetic */ String c;
    public final /* synthetic */ s4 d;

    public f5(s4 s4Var, i iVar, String str) {
        this.d = s4Var;
        this.b = iVar;
        this.c = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.g.a.d9.<init>(java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, java.lang.String, boolean, boolean, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>):void
     arg types: [java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, ?[OBJECT, ARRAY], boolean, int, java.lang.String, long, int, int, boolean, boolean, int, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>]
     candidates:
      j.c.a.a.g.a.d9.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, long, long, java.lang.String, boolean, boolean, long, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>):void
      j.c.a.a.g.a.d9.<init>(java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, java.lang.String, boolean, boolean, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>):void */
    public final void run() {
        this.d.a.o();
        q8 q8Var = this.d.a;
        i iVar = this.b;
        String str = this.c;
        e4 b2 = q8Var.e().b(str);
        if (b2 == null || TextUtils.isEmpty(b2.n())) {
            q8Var.f2073i.a().f2051m.a("No app data available; dropping event", str);
            return;
        }
        Boolean b3 = q8Var.b(b2);
        if (b3 == null) {
            if (!"_ui".equals(iVar.b)) {
                q8Var.f2073i.a().f2047i.a("Could not find package. appId", n3.a(str));
            }
        } else if (!b3.booleanValue()) {
            q8Var.f2073i.a().f2046f.a("App version does not match; dropping event. appId", n3.a(str));
            return;
        }
        d9 d9Var = r3;
        e4 e4Var = b2;
        d9 d9Var2 = new d9(str, b2.i(), b2.n(), b2.o(), b2.p(), b2.q(), b2.r(), (String) null, b2.t(), false, e4Var.k(), e4Var.b(), 0L, 0, e4Var.c(), e4Var.d(), false, e4Var.j(), e4Var.e(), e4Var.s(), e4Var.f());
        q8Var.a(iVar, d9Var);
    }
}
