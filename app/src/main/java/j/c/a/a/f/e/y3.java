package j.c.a.a.f.e;

import com.google.android.gms.internal.measurement.zzfn;
import j.c.a.a.f.e.x3;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class y3 {
    public static final Charset a = Charset.forName("UTF-8");
    public static final byte[] b;

    static {
        Charset.forName("ISO-8859-1");
        byte[] bArr = new byte[0];
        b = bArr;
        ByteBuffer.wrap(bArr);
        int length = b.length;
        int i2 = length + 0;
        if (length < 0) {
            throw zzfn.b();
        } else if ((0 - 0) + length <= Integer.MAX_VALUE) {
            int i3 = i2 + 0 + 0;
        } else {
            try {
                throw zzfn.a();
            } catch (zzfn e2) {
                throw new IllegalArgumentException(e2);
            }
        }
    }

    public static int a(long j2) {
        return (int) (j2 ^ (j2 >>> 32));
    }

    public static int a(boolean z) {
        return z ? 1231 : 1237;
    }

    public static <T> T a(Object obj) {
        if (obj != null) {
            return obj;
        }
        throw null;
    }

    public static int b(byte[] bArr) {
        int length = bArr.length;
        int i2 = length;
        for (int i3 = 0; i3 < 0 + length; i3++) {
            i2 = (i2 * 31) + bArr[i3];
        }
        if (i2 == 0) {
            return 1;
        }
        return i2;
    }

    public static <T> T a(Object obj, String str) {
        if (obj != null) {
            return obj;
        }
        throw new NullPointerException(str);
    }

    public static String a(byte[] bArr) {
        return new String(bArr, a);
    }

    public static int a(int i2, byte[] bArr, int i3, int i4) {
        for (int i5 = i3; i5 < i3 + i4; i5++) {
            i2 = (i2 * 31) + bArr[i5];
        }
        return i2;
    }

    public static Object a(Object obj, Object obj2) {
        f5 f5Var = (f5) obj2;
        p2 p2Var = (p2) ((f5) obj).d();
        if (p2Var != null) {
            x3.a aVar = (x3.a) p2Var;
            if (aVar.b.getClass().isInstance(f5Var)) {
                aVar.a((x3) ((n2) f5Var));
                return aVar.j();
            }
            throw new IllegalArgumentException("mergeFrom(MessageLite) can only merge messages of the same type.");
        }
        throw null;
    }
}
