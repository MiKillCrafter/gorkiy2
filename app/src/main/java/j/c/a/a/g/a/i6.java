package j.c.a.a.g.a;

import java.util.concurrent.atomic.AtomicReference;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class i6 implements Runnable {
    public final /* synthetic */ AtomicReference b;
    public final /* synthetic */ x5 c;

    public i6(x5 x5Var, AtomicReference atomicReference) {
        this.c = x5Var;
        this.b = atomicReference;
    }

    public final void run() {
        String str;
        synchronized (this.b) {
            try {
                AtomicReference atomicReference = this.b;
                i9 i9Var = this.c.a.g;
                g3 q2 = this.c.q();
                q2.w();
                String str2 = q2.c;
                if (i9Var != null) {
                    b3<String> b3Var = k.N;
                    if (str2 == null) {
                        str = b3Var.a(null);
                    } else {
                        str = b3Var.a(i9Var.c.a(str2, b3Var.a));
                    }
                    atomicReference.set(str);
                    this.b.notify();
                } else {
                    throw null;
                }
            } catch (Throwable th) {
                this.b.notify();
                throw th;
            }
        }
    }
}
