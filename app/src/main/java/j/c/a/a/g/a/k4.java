package j.c.a.a.g.a;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.os.Bundle;
import j.c.a.a.f.e.nb;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class k4 implements Runnable {
    public final /* synthetic */ r4 b;
    public final /* synthetic */ long c;
    public final /* synthetic */ Bundle d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ Context f2023e;

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ n3 f2024f;
    public final /* synthetic */ BroadcastReceiver.PendingResult g;

    public k4(r4 r4Var, long j2, Bundle bundle, Context context, n3 n3Var, BroadcastReceiver.PendingResult pendingResult) {
        this.b = r4Var;
        this.c = j2;
        this.d = bundle;
        this.f2023e = context;
        this.f2024f = n3Var;
        this.g = pendingResult;
    }

    public final void run() {
        long a = this.b.l().f2114j.a();
        long j2 = this.c;
        if (a > 0 && (j2 >= a || j2 <= 0)) {
            j2 = a - 1;
        }
        if (j2 > 0) {
            this.d.putLong("click_timestamp", j2);
        }
        this.d.putString("_cis", "referrer broadcast");
        r4.a(this.f2023e, (nb) null).m().a("auto", "_cmp", this.d);
        this.f2024f.f2052n.a("Install campaign recorded");
        BroadcastReceiver.PendingResult pendingResult = this.g;
        if (pendingResult != null) {
            pendingResult.finish();
        }
    }
}
