package j.c.a.a.f.e;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class n3 {
    public static final l3<?> a = new k3();
    public static final l3<?> b;

    static {
        l3<?> l3Var;
        try {
            l3Var = (l3) Class.forName("com.google.protobuf.ExtensionSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            l3Var = null;
        }
        b = l3Var;
    }
}
