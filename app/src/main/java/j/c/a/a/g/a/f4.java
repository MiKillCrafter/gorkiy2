package j.c.a.a.g.a;

import android.content.ServiceConnection;
import j.c.a.a.f.e.t3;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class f4 implements Runnable {
    public final /* synthetic */ t3 b;
    public final /* synthetic */ ServiceConnection c;
    public final /* synthetic */ g4 d;

    public f4(g4 g4Var, t3 t3Var, ServiceConnection serviceConnection) {
        this.d = g4Var;
        this.b = t3Var;
        this.c = serviceConnection;
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0061  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0168  */
    /* JADX WARNING: Removed duplicated region for block: B:50:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r14 = this;
            j.c.a.a.g.a.g4 r0 = r14.d
            j.c.a.a.g.a.c4 r1 = r0.b
            java.lang.String r0 = r0.a
            j.c.a.a.f.e.t3 r2 = r14.b
            android.content.ServiceConnection r3 = r14.c
            j.c.a.a.g.a.r4 r4 = r1.a
            j.c.a.a.g.a.l4 r4 = r4.i()
            r4.d()
            r4 = 0
            if (r2 != 0) goto L_0x0025
            j.c.a.a.g.a.r4 r0 = r1.a
            j.c.a.a.g.a.n3 r0 = r0.a()
            j.c.a.a.g.a.p3 r0 = r0.f2047i
            java.lang.String r2 = "Attempting to use Install Referrer Service while it is not initialized"
            r0.a(r2)
        L_0x0023:
            r0 = r4
            goto L_0x0056
        L_0x0025:
            android.os.Bundle r5 = new android.os.Bundle
            r5.<init>()
            java.lang.String r6 = "package_name"
            r5.putString(r6, r0)
            android.os.Bundle r0 = r2.a(r5)     // Catch:{ Exception -> 0x0043 }
            if (r0 != 0) goto L_0x0056
            j.c.a.a.g.a.r4 r0 = r1.a     // Catch:{ Exception -> 0x0043 }
            j.c.a.a.g.a.n3 r0 = r0.a()     // Catch:{ Exception -> 0x0043 }
            j.c.a.a.g.a.p3 r0 = r0.f2046f     // Catch:{ Exception -> 0x0043 }
            java.lang.String r2 = "Install Referrer Service returned a null response"
            r0.a(r2)     // Catch:{ Exception -> 0x0043 }
            goto L_0x0023
        L_0x0043:
            r0 = move-exception
            j.c.a.a.g.a.r4 r2 = r1.a
            j.c.a.a.g.a.n3 r2 = r2.a()
            j.c.a.a.g.a.p3 r2 = r2.f2046f
            java.lang.String r0 = r0.getMessage()
            java.lang.String r5 = "Exception occurred while retrieving the Install Referrer"
            r2.a(r5, r0)
            goto L_0x0023
        L_0x0056:
            j.c.a.a.g.a.r4 r2 = r1.a
            j.c.a.a.g.a.l4 r2 = r2.i()
            r2.d()
            if (r0 == 0) goto L_0x0166
            r5 = 0
            java.lang.String r2 = "install_begin_timestamp_seconds"
            long r7 = r0.getLong(r2, r5)
            r9 = 1000(0x3e8, double:4.94E-321)
            long r7 = r7 * r9
            int r2 = (r7 > r5 ? 1 : (r7 == r5 ? 0 : -1))
            if (r2 != 0) goto L_0x0080
            j.c.a.a.g.a.r4 r0 = r1.a
            j.c.a.a.g.a.n3 r0 = r0.a()
            j.c.a.a.g.a.p3 r0 = r0.f2046f
            java.lang.String r2 = "Service response is missing Install Referrer install timestamp"
            r0.a(r2)
            goto L_0x0166
        L_0x0080:
            java.lang.String r2 = "install_referrer"
            java.lang.String r2 = r0.getString(r2)
            if (r2 == 0) goto L_0x0159
            boolean r11 = r2.isEmpty()
            if (r11 == 0) goto L_0x0090
            goto L_0x0159
        L_0x0090:
            j.c.a.a.g.a.r4 r11 = r1.a
            j.c.a.a.g.a.n3 r11 = r11.a()
            j.c.a.a.g.a.p3 r11 = r11.f2052n
            java.lang.String r12 = "InstallReferrer API result"
            r11.a(r12, r2)
            j.c.a.a.g.a.r4 r11 = r1.a
            j.c.a.a.g.a.y8 r11 = r11.n()
            java.lang.String r12 = "?"
            int r13 = r2.length()
            if (r13 == 0) goto L_0x00b0
            java.lang.String r2 = r12.concat(r2)
            goto L_0x00b5
        L_0x00b0:
            java.lang.String r2 = new java.lang.String
            r2.<init>(r12)
        L_0x00b5:
            android.net.Uri r2 = android.net.Uri.parse(r2)
            android.os.Bundle r2 = r11.a(r2)
            if (r2 != 0) goto L_0x00ce
            j.c.a.a.g.a.r4 r0 = r1.a
            j.c.a.a.g.a.n3 r0 = r0.a()
            j.c.a.a.g.a.p3 r0 = r0.f2046f
            java.lang.String r2 = "No campaign params defined in install referrer result"
            r0.a(r2)
            goto L_0x0166
        L_0x00ce:
            java.lang.String r11 = "medium"
            java.lang.String r11 = r2.getString(r11)
            if (r11 == 0) goto L_0x00e8
            java.lang.String r12 = "(not set)"
            boolean r12 = r12.equalsIgnoreCase(r11)
            if (r12 != 0) goto L_0x00e8
            java.lang.String r12 = "organic"
            boolean r11 = r12.equalsIgnoreCase(r11)
            if (r11 != 0) goto L_0x00e8
            r11 = 1
            goto L_0x00e9
        L_0x00e8:
            r11 = 0
        L_0x00e9:
            if (r11 == 0) goto L_0x010a
            java.lang.String r11 = "referrer_click_timestamp_seconds"
            long r11 = r0.getLong(r11, r5)
            long r11 = r11 * r9
            int r0 = (r11 > r5 ? 1 : (r11 == r5 ? 0 : -1))
            if (r0 != 0) goto L_0x0105
            j.c.a.a.g.a.r4 r0 = r1.a
            j.c.a.a.g.a.n3 r0 = r0.a()
            j.c.a.a.g.a.p3 r0 = r0.f2046f
            java.lang.String r2 = "Install Referrer is missing click timestamp for ad campaign"
            r0.a(r2)
            goto L_0x0166
        L_0x0105:
            java.lang.String r0 = "click_timestamp"
            r2.putLong(r0, r11)
        L_0x010a:
            j.c.a.a.g.a.r4 r0 = r1.a
            j.c.a.a.g.a.w3 r0 = r0.l()
            j.c.a.a.g.a.b4 r0 = r0.f2115k
            long r5 = r0.a()
            int r0 = (r7 > r5 ? 1 : (r7 == r5 ? 0 : -1))
            if (r0 != 0) goto L_0x012a
            j.c.a.a.g.a.r4 r0 = r1.a
            j.c.a.a.g.a.h9 r2 = r0.f2086f
            j.c.a.a.g.a.n3 r0 = r0.a()
            j.c.a.a.g.a.p3 r0 = r0.f2052n
            java.lang.String r2 = "Campaign has already been logged"
            r0.a(r2)
            goto L_0x0166
        L_0x012a:
            j.c.a.a.g.a.r4 r0 = r1.a
            j.c.a.a.g.a.w3 r0 = r0.l()
            j.c.a.a.g.a.b4 r0 = r0.f2115k
            r0.a(r7)
            j.c.a.a.g.a.r4 r0 = r1.a
            j.c.a.a.g.a.h9 r5 = r0.f2086f
            j.c.a.a.g.a.n3 r0 = r0.a()
            j.c.a.a.g.a.p3 r0 = r0.f2052n
            java.lang.String r5 = "referrer API"
            java.lang.String r6 = "Logging Install Referrer campaign from sdk with "
            r0.a(r6, r5)
            java.lang.String r0 = "_cis"
            r2.putString(r0, r5)
            j.c.a.a.g.a.r4 r0 = r1.a
            j.c.a.a.g.a.x5 r0 = r0.m()
            java.lang.String r5 = "auto"
            java.lang.String r6 = "_cmp"
            r0.a(r5, r6, r2)
            goto L_0x0166
        L_0x0159:
            j.c.a.a.g.a.r4 r0 = r1.a
            j.c.a.a.g.a.n3 r0 = r0.a()
            j.c.a.a.g.a.p3 r0 = r0.f2046f
            java.lang.String r2 = "No referrer defined in install referrer response"
            r0.a(r2)
        L_0x0166:
            if (r3 == 0) goto L_0x0177
            j.c.a.a.c.m.a r0 = j.c.a.a.c.m.a.a()
            j.c.a.a.g.a.r4 r1 = r1.a
            android.content.Context r1 = r1.a
            if (r0 == 0) goto L_0x0176
            r1.unbindService(r3)
            goto L_0x0177
        L_0x0176:
            throw r4
        L_0x0177:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.f4.run():void");
    }
}
