package j.c.a.a.f.e;

import android.net.Uri;
import i.e.ArrayMap;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class q1 {
    public static final ArrayMap<String, Uri> a = new ArrayMap<>();

    public static synchronized Uri a(String str) {
        Uri orDefault;
        synchronized (q1.class) {
            orDefault = a.getOrDefault(str, null);
            if (orDefault == null) {
                String valueOf = String.valueOf(Uri.encode(str));
                orDefault = Uri.parse(valueOf.length() != 0 ? "content://com.google.android.gms.phenotype/".concat(valueOf) : new String("content://com.google.android.gms.phenotype/"));
                a.put(str, orDefault);
            }
        }
        return orDefault;
    }
}
