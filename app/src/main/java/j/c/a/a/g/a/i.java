package j.c.a.a.g.a;

import android.os.Parcel;
import android.os.Parcelable;
import i.b.k.ResourcesFlusher;
import j.a.a.a.outline;
import j.c.a.a.c.l.p.a;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class i extends a {
    public static final Parcelable.Creator<i> CREATOR = new l();
    public final String b;
    public final h c;
    public final String d;

    /* renamed from: e  reason: collision with root package name */
    public final long f2007e;

    public i(String str, h hVar, String str2, long j2) {
        this.b = str;
        this.c = hVar;
        this.d = str2;
        this.f2007e = j2;
    }

    public final String toString() {
        String str = this.d;
        String str2 = this.b;
        String valueOf = String.valueOf(this.c);
        StringBuilder sb = new StringBuilder(valueOf.length() + outline.a(str2, outline.a(str, 21)));
        sb.append("origin=");
        sb.append(str);
        sb.append(",name=");
        sb.append(str2);
        return outline.a(sb, ",params=", valueOf);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, int, int, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int, boolean, int):int
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, org.xmlpull.v1.XmlPullParser, android.util.AttributeSet, android.content.res.Resources$Theme):android.content.res.ColorStateList
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, android.content.res.Resources$Theme, android.util.AttributeSet, int[]):android.content.res.TypedArray
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, org.xmlpull.v1.XmlPullParser, java.lang.String, int):java.lang.String
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      i.b.k.ResourcesFlusher.a(int, android.graphics.Rect, android.graphics.Rect, android.graphics.Rect):boolean
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, int, int, boolean):boolean
      i.b.k.ResourcesFlusher.a(i.f.a.h.d, i.f.a.h.f, java.util.List<i.f.a.h.f>, boolean):boolean
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, j.c.a.a.g.a.h, int, int]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, org.xmlpull.v1.XmlPullParser, java.lang.String, int, float):float
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, org.xmlpull.v1.XmlPullParser, java.lang.String, int, int):int
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, int, int, int, java.lang.String):android.animation.PropertyValuesHolder
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetContainer, i.f.a.LinearSystem, int, int, i.f.a.h.ChainHead):void
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i2) {
        int a = ResourcesFlusher.a(parcel);
        ResourcesFlusher.a(parcel, 2, this.b, false);
        ResourcesFlusher.a(parcel, 3, (Parcelable) this.c, i2, false);
        ResourcesFlusher.a(parcel, 4, this.d, false);
        ResourcesFlusher.a(parcel, 5, this.f2007e);
        ResourcesFlusher.k(parcel, a);
    }

    public i(i iVar, long j2) {
        ResourcesFlusher.b(iVar);
        this.b = iVar.b;
        this.c = iVar.c;
        this.d = iVar.d;
        this.f2007e = j2;
    }
}
