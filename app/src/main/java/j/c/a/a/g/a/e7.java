package j.c.a.a.g.a;

import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class e7 implements Runnable {
    public final /* synthetic */ d9 b;
    public final /* synthetic */ z6 c;

    public e7(z6 z6Var, d9 d9Var) {
        this.c = z6Var;
        this.b = d9Var;
    }

    public final void run() {
        z6 z6Var = this.c;
        f3 f3Var = z6Var.d;
        if (f3Var == null) {
            z6Var.a().f2046f.a("Failed to reset data on the service; null service");
            return;
        }
        try {
            f3Var.d(this.b);
        } catch (RemoteException e2) {
            this.c.a().f2046f.a("Failed to reset data on the service", e2);
        }
        this.c.D();
    }
}
