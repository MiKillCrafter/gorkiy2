package j.c.a.a.g.a;

import android.content.SharedPreferences;
import i.b.k.ResourcesFlusher;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class b4 {
    public final String a;
    public final long b;
    public boolean c;
    public long d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ w3 f1940e;

    public b4(w3 w3Var, String str, long j2) {
        this.f1940e = w3Var;
        ResourcesFlusher.b(str);
        this.a = str;
        this.b = j2;
    }

    public final long a() {
        if (!this.c) {
            this.c = true;
            this.d = this.f1940e.v().getLong(this.a, this.b);
        }
        return this.d;
    }

    public final void a(long j2) {
        SharedPreferences.Editor edit = this.f1940e.v().edit();
        edit.putLong(this.a, j2);
        edit.apply();
        this.d = j2;
    }
}
