package j.c.a.a.g.a;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class l5 implements Runnable {
    public final /* synthetic */ String b;
    public final /* synthetic */ String c;
    public final /* synthetic */ String d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ long f2033e;

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ s4 f2034f;

    public l5(s4 s4Var, String str, String str2, String str3, long j2) {
        this.f2034f = s4Var;
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.f2033e = j2;
    }

    public final void run() {
        String str = this.b;
        if (str == null) {
            this.f2034f.a.f2073i.q().a(this.c, (w6) null);
            return;
        }
        this.f2034f.a.f2073i.q().a(this.c, new w6(this.d, str, this.f2033e));
    }
}
