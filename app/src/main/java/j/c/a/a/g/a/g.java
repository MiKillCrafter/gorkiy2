package j.c.a.a.g.a;

import java.util.Iterator;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class g implements Iterator<String> {
    public Iterator<String> b = this.c.b.keySet().iterator();
    public final /* synthetic */ h c;

    public g(h hVar) {
        this.c = hVar;
    }

    public final boolean hasNext() {
        return this.b.hasNext();
    }

    public final /* synthetic */ Object next() {
        return this.b.next();
    }

    public final void remove() {
        throw new UnsupportedOperationException("Remove not supported");
    }
}
