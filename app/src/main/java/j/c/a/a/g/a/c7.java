package j.c.a.a.g.a;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class c7 implements Runnable {
    public final /* synthetic */ boolean b;
    public final /* synthetic */ x8 c;
    public final /* synthetic */ d9 d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ z6 f1948e;

    public c7(z6 z6Var, boolean z, x8 x8Var, d9 d9Var) {
        this.f1948e = z6Var;
        this.b = z;
        this.c = x8Var;
        this.d = d9Var;
    }

    public final void run() {
        z6 z6Var = this.f1948e;
        f3 f3Var = z6Var.d;
        if (f3Var == null) {
            z6Var.a().f2046f.a("Discarding data. Failed to set user attribute");
            return;
        }
        z6Var.a(f3Var, this.b ? null : this.c, this.d);
        this.f1948e.D();
    }
}
