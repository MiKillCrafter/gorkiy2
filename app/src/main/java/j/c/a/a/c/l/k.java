package j.c.a.a.c.l;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.util.Log;
import i.b.k.ResourcesFlusher;
import j.c.a.a.c.l.b;
import j.c.a.a.f.c.b;
import j.c.a.a.f.c.c;

public interface k extends IInterface {

    public static abstract class a extends b implements k {
        public a() {
            super("com.google.android.gms.common.internal.IGmsCallbacks");
        }

        public final boolean a(int i2, Parcel parcel, Parcel parcel2, int i3) {
            if (i2 == 1) {
                ((b.i) this).a(parcel.readInt(), parcel.readStrongBinder(), (Bundle) c.a(parcel, Bundle.CREATOR));
            } else if (i2 == 2) {
                parcel.readInt();
                Bundle bundle = (Bundle) c.a(parcel, Bundle.CREATOR);
                Log.wtf("GmsClient", "received deprecated onAccountValidationComplete callback, ignoring", new Exception());
            } else if (i2 != 3) {
                return false;
            } else {
                int readInt = parcel.readInt();
                IBinder readStrongBinder = parcel.readStrongBinder();
                u uVar = (u) c.a(parcel, u.CREATOR);
                b.i iVar = (b.i) this;
                ResourcesFlusher.b(iVar.a, "onPostInitCompleteWithConnectionInfo can be called only once per call togetRemoteService");
                ResourcesFlusher.b(uVar);
                iVar.a.f1813t = uVar;
                iVar.a(readInt, readStrongBinder, uVar.b);
            }
            parcel2.writeNoException();
            return true;
        }
    }
}
