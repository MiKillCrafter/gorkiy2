package j.c.a.a.f.e;

import android.os.Bundle;
import j.c.a.a.f.e.pb;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.0.1 */
public final class c extends pb.a {

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ String f1832f;
    public final /* synthetic */ String g;
    public final /* synthetic */ Bundle h;

    /* renamed from: i  reason: collision with root package name */
    public final /* synthetic */ pb f1833i;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public c(pb pbVar, String str, String str2, Bundle bundle) {
        super(true);
        this.f1833i = pbVar;
        this.f1832f = str;
        this.g = str2;
        this.h = bundle;
    }

    public final void a() {
        this.f1833i.g.clearConditionalUserProperty(this.f1832f, this.g, this.h);
    }
}
