package j.c.a.a.f.e;

import android.content.Context;
import android.content.SharedPreferences;
import i.e.ArrayMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class y1 implements i1 {

    /* renamed from: f  reason: collision with root package name */
    public static final Map<String, y1> f1930f = new ArrayMap();
    public final SharedPreferences a;
    public final SharedPreferences.OnSharedPreferenceChangeListener b = new x1(this);
    public final Object c = new Object();
    public volatile Map<String, ?> d;

    /* renamed from: e  reason: collision with root package name */
    public final List<j1> f1931e = new ArrayList();

    public y1(SharedPreferences sharedPreferences) {
        this.a = sharedPreferences;
        sharedPreferences.registerOnSharedPreferenceChangeListener(this.b);
    }

    public static y1 a(Context context, String str) {
        y1 y1Var;
        SharedPreferences sharedPreferences;
        boolean z = true;
        if (f1.a() && !str.startsWith("direct_boot:") && f1.a() && !f1.a(context)) {
            z = false;
        }
        if (!z) {
            return null;
        }
        synchronized (y1.class) {
            y1Var = f1930f.get(str);
            if (y1Var == null) {
                if (str.startsWith("direct_boot:")) {
                    if (f1.a()) {
                        context = context.createDeviceProtectedStorageContext();
                    }
                    sharedPreferences = context.getSharedPreferences(str.substring(12), 0);
                } else {
                    sharedPreferences = context.getSharedPreferences(str, 0);
                }
                y1Var = new y1(sharedPreferences);
                f1930f.put(str, y1Var);
            }
        }
        return y1Var;
    }

    public static synchronized void b() {
        synchronized (y1.class) {
            for (y1 next : f1930f.values()) {
                next.a.unregisterOnSharedPreferenceChangeListener(next.b);
            }
            f1930f.clear();
        }
    }

    public final Object a(String str) {
        Map<String, ?> map = this.d;
        if (map == null) {
            synchronized (this.c) {
                map = this.d;
                if (map == null) {
                    map = this.a.getAll();
                    this.d = map;
                }
            }
        }
        if (map != null) {
            return map.get(str);
        }
        return null;
    }

    public final /* synthetic */ void a() {
        synchronized (this.c) {
            this.d = null;
            p1.f1894i.incrementAndGet();
        }
        synchronized (this) {
            for (j1 a2 : this.f1931e) {
                a2.a();
            }
        }
    }
}
