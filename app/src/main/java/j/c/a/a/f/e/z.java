package j.c.a.a.f.e;

import j.c.a.a.f.e.x3;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class z extends x3<z, a> implements g5 {
    public static final z zzi;
    public static volatile m5<z> zzj;
    public int zzc;
    public int zzd;
    public e4<d0> zze;
    public e4<a0> zzf;
    public boolean zzg;
    public boolean zzh;

    static {
        z zVar = new z();
        zzi = zVar;
        x3.zzd.put(z.class, zVar);
    }

    public z() {
        s5<Object> s5Var = s5.f1905e;
        this.zze = s5Var;
        this.zzf = s5Var;
    }

    public final Object a(int i2, Object obj, Object obj2) {
        switch (f0.a[i2 - 1]) {
            case 1:
                return new z();
            case 2:
                return new a(null);
            case 3:
                return new r5(zzi, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0002\u0000\u0001\u0004\u0000\u0002\u001b\u0003\u001b\u0004\u0007\u0001\u0005\u0007\u0002", new Object[]{"zzc", "zzd", "zze", d0.class, "zzf", a0.class, "zzg", "zzh"});
            case 4:
                return zzi;
            case 5:
                m5<z> m5Var = zzj;
                if (m5Var == null) {
                    synchronized (z.class) {
                        m5Var = zzj;
                        if (m5Var == null) {
                            m5Var = new x3.c<>(zzi);
                            zzj = m5Var;
                        }
                    }
                }
                return m5Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    /* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
    public static final class a extends x3.a<z, a> implements g5 {
        public a() {
            super(z.zzi);
        }

        public /* synthetic */ a(f0 f0Var) {
            super(z.zzi);
        }
    }
}
