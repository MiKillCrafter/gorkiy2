package j.c.a.a.f.e;

import j.c.a.a.f.e.x3;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class l0 extends x3<l0, a> implements g5 {
    public static final l0 zzf;
    public static volatile m5<l0> zzg;
    public int zzc;
    public String zzd = "";
    public String zze = "";

    /* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
    public static final class a extends x3.a<l0, a> implements g5 {
        public /* synthetic */ a(n0 n0Var) {
            super(l0.zzf);
        }
    }

    static {
        l0 l0Var = new l0();
        zzf = l0Var;
        x3.zzd.put(l0.class, l0Var);
    }

    public final Object a(int i2, Object obj, Object obj2) {
        switch (n0.a[i2 - 1]) {
            case 1:
                return new l0();
            case 2:
                return new a(null);
            case 3:
                return new r5(zzf, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001\b\u0000\u0002\b\u0001", new Object[]{"zzc", "zzd", "zze"});
            case 4:
                return zzf;
            case 5:
                m5<l0> m5Var = zzg;
                if (m5Var == null) {
                    synchronized (l0.class) {
                        m5Var = zzg;
                        if (m5Var == null) {
                            m5Var = new x3.c<>(zzf);
                            zzg = m5Var;
                        }
                    }
                }
                return m5Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
