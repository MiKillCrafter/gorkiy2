package j.c.a.a.f.e;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class z8 implements w8 {
    public static final p1<Boolean> a = p1.a(new v1(q1.a("com.google.android.gms.measurement")), "measurement.referrer.enable_logging_install_referrer_cmp_from_apk", false);

    public final boolean a() {
        return a.b().booleanValue();
    }
}
