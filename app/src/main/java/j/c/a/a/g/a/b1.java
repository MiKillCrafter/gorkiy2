package j.c.a.a.g.a;

import i.b.k.ResourcesFlusher;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class b1 implements Runnable {
    public final /* synthetic */ String b;
    public final /* synthetic */ long c;
    public final /* synthetic */ a d;

    public b1(a aVar, String str, long j2) {
        this.d = aVar;
        this.b = str;
        this.c = j2;
    }

    public final void run() {
        a aVar = this.d;
        String str = this.b;
        long j2 = this.c;
        aVar.b();
        aVar.d();
        ResourcesFlusher.b(str);
        if (aVar.c.isEmpty()) {
            aVar.d = j2;
        }
        Integer num = aVar.c.get(str);
        if (num != null) {
            aVar.c.put(str, Integer.valueOf(num.intValue() + 1));
        } else if (aVar.c.size() >= 100) {
            aVar.a().f2047i.a("Too many ads visible");
        } else {
            aVar.c.put(str, 1);
            aVar.b.put(str, Long.valueOf(j2));
        }
    }
}
