package j.c.a.a.c.k.e;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import com.google.android.gms.common.api.Status;
import i.b.k.ResourcesFlusher;
import j.c.a.a.c.i;
import j.c.a.a.c.l.g0;
import javax.annotation.concurrent.GuardedBy;

@Deprecated
public final class c {
    public static final Object d = new Object();
    @GuardedBy("sLock")

    /* renamed from: e  reason: collision with root package name */
    public static c f1797e;
    public final String a;
    public final Status b;
    public final boolean c;

    public c(Context context) {
        Resources resources = context.getResources();
        int identifier = resources.getIdentifier("google_app_measurement_enable", "integer", resources.getResourcePackageName(i.common_google_play_services_unknown_issue));
        boolean z = false;
        if (identifier != 0) {
            this.c = !(resources.getInteger(identifier) != 0 ? true : z);
        } else {
            this.c = false;
        }
        g0.a(context);
        String str = g0.c;
        if (str == null) {
            ResourcesFlusher.b(context);
            Resources resources2 = context.getResources();
            int identifier2 = resources2.getIdentifier("google_app_id", "string", resources2.getResourcePackageName(i.common_google_play_services_unknown_issue));
            if (identifier2 == 0) {
                str = null;
            } else {
                str = resources2.getString(identifier2);
            }
        }
        if (TextUtils.isEmpty(str)) {
            this.b = new Status(10, "Missing google app id value from from string resources with name google_app_id.");
            this.a = null;
            return;
        }
        this.a = str;
        this.b = Status.f375f;
    }

    public static Status a(Context context) {
        Status status;
        ResourcesFlusher.b(context, "Context must not be null.");
        synchronized (d) {
            if (f1797e == null) {
                f1797e = new c(context);
            }
            status = f1797e.b;
        }
        return status;
    }

    public static boolean b() {
        return a("isMeasurementExplicitlyDisabled").c;
    }

    public static String a() {
        return a("getGoogleAppId").a;
    }

    public static c a(String str) {
        c cVar;
        synchronized (d) {
            if (f1797e != null) {
                cVar = f1797e;
            } else {
                StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 34);
                sb.append("Initialize must be called before ");
                sb.append(str);
                sb.append(".");
                throw new IllegalStateException(sb.toString());
            }
        }
        return cVar;
    }
}
