package j.c.a.a.g.a;

import android.text.TextUtils;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class g5 implements Runnable {
    public final /* synthetic */ i b;
    public final /* synthetic */ d9 c;
    public final /* synthetic */ s4 d;

    public g5(s4 s4Var, i iVar, d9 d9Var) {
        this.d = s4Var;
        this.b = iVar;
        this.c = d9Var;
    }

    public final void run() {
        h hVar;
        s4 s4Var = this.d;
        i iVar = this.b;
        d9 d9Var = this.c;
        if (s4Var != null) {
            boolean z = false;
            if (!(!"_cmp".equals(iVar.b) || (hVar = iVar.c) == null || hVar.b.size() == 0)) {
                String string = iVar.c.b.getString("_cis");
                if (!TextUtils.isEmpty(string) && ("referrer broadcast".equals(string) || "referrer API".equals(string))) {
                    i9 i9Var = s4Var.a.f2073i.g;
                    String str = d9Var.b;
                    if (i9Var == null) {
                        throw null;
                    } else if (i9Var.d(str, k.X)) {
                        z = true;
                    }
                }
            }
            if (z) {
                s4Var.a.a().f2050l.a("Event has been filtered ", iVar.toString());
                iVar = new i("_cmpx", iVar.c, iVar.d, iVar.f2007e);
            }
            this.d.a.o();
            this.d.a.a(iVar, this.c);
            return;
        }
        throw null;
    }
}
