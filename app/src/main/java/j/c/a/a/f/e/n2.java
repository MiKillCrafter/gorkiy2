package j.c.a.a.f.e;

import com.google.android.gms.internal.measurement.zzek;
import j.c.a.a.f.e.n2;
import j.c.a.a.f.e.p2;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public abstract class n2<MessageType extends n2<MessageType, BuilderType>, BuilderType extends p2<MessageType, BuilderType>> implements f5 {
    public int zza = 0;

    public static <T> void a(Iterable<T> iterable, List<? super T> list) {
        y3.a(iterable);
        if (iterable instanceof m4) {
            List<?> b = ((m4) iterable).b();
            m4 m4Var = (m4) list;
            int size = list.size();
            for (Object next : b) {
                if (next == null) {
                    StringBuilder sb = new StringBuilder(37);
                    sb.append("Element at index ");
                    sb.append(m4Var.size() - size);
                    sb.append(" is null.");
                    String sb2 = sb.toString();
                    for (int size2 = m4Var.size() - 1; size2 >= size; size2--) {
                        m4Var.remove(size2);
                    }
                    throw new NullPointerException(sb2);
                } else if (next instanceof w2) {
                    m4Var.a((w2) next);
                } else {
                    m4Var.add((String) next);
                }
            }
        } else if (iterable instanceof p5) {
            list.addAll((Collection) iterable);
        } else {
            if ((list instanceof ArrayList) && (iterable instanceof Collection)) {
                ((ArrayList) list).ensureCapacity(((Collection) iterable).size() + list.size());
            }
            int size3 = list.size();
            for (T next2 : iterable) {
                if (next2 == null) {
                    StringBuilder sb3 = new StringBuilder(37);
                    sb3.append("Element at index ");
                    sb3.append(list.size() - size3);
                    sb3.append(" is null.");
                    String sb4 = sb3.toString();
                    for (int size4 = list.size() - 1; size4 >= size3; size4--) {
                        list.remove(size4);
                    }
                    throw new NullPointerException(sb4);
                }
                list.add(next2);
            }
        }
    }

    public final w2 e() {
        try {
            b3 c = w2.c(c());
            a(c.a);
            return c.a();
        } catch (IOException e2) {
            String name = getClass().getName();
            StringBuilder sb = new StringBuilder("ByteString".length() + name.length() + 62);
            sb.append("Serializing ");
            sb.append(name);
            sb.append(" to a ");
            sb.append("ByteString");
            sb.append(" threw an IOException (should never happen).");
            throw new RuntimeException(sb.toString(), e2);
        }
    }

    public final byte[] f() {
        try {
            byte[] bArr = new byte[c()];
            zzek a = zzek.a(bArr);
            a(a);
            if (a.a() == 0) {
                return bArr;
            }
            throw new IllegalStateException("Did not write as much data as expected.");
        } catch (IOException e2) {
            String name = getClass().getName();
            StringBuilder sb = new StringBuilder(10 + name.length() + 62);
            sb.append("Serializing ");
            sb.append(name);
            sb.append(" to a ");
            sb.append("byte array");
            sb.append(" threw an IOException (should never happen).");
            throw new RuntimeException(sb.toString(), e2);
        }
    }
}
