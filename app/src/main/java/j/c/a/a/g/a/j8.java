package j.c.a.a.g.a;

import android.os.Bundle;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class j8 implements Runnable {
    public final /* synthetic */ g8 b;

    public j8(g8 g8Var) {
        this.b = g8Var;
    }

    public final void run() {
        f8 f8Var = this.b.b;
        f8Var.d();
        f8Var.a().f2051m.a("Application backgrounded");
        f8Var.p().b("auto", "_ab", new Bundle());
    }
}
