package j.c.a.a.g.a;

import android.os.Parcel;
import android.os.Parcelable;
import i.b.k.ResourcesFlusher;
import java.util.ArrayList;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class c9 implements Parcelable.Creator<d9> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = ResourcesFlusher.b(parcel);
        long j2 = 0;
        long j3 = 0;
        long j4 = 0;
        long j5 = 0;
        long j6 = 0;
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        String str7 = null;
        Boolean bool = null;
        ArrayList<String> arrayList = null;
        long j7 = -2147483648L;
        boolean z = true;
        boolean z2 = false;
        int i2 = 0;
        boolean z3 = true;
        boolean z4 = true;
        boolean z5 = false;
        while (parcel.dataPosition() < b) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    str = ResourcesFlusher.b(parcel2, readInt);
                    break;
                case 3:
                    str2 = ResourcesFlusher.b(parcel2, readInt);
                    break;
                case 4:
                    str3 = ResourcesFlusher.b(parcel2, readInt);
                    break;
                case 5:
                    str4 = ResourcesFlusher.b(parcel2, readInt);
                    break;
                case 6:
                    j2 = ResourcesFlusher.g(parcel2, readInt);
                    break;
                case 7:
                    j3 = ResourcesFlusher.g(parcel2, readInt);
                    break;
                case 8:
                    str5 = ResourcesFlusher.b(parcel2, readInt);
                    break;
                case 9:
                    z = ResourcesFlusher.d(parcel2, readInt);
                    break;
                case 10:
                    z2 = ResourcesFlusher.d(parcel2, readInt);
                    break;
                case 11:
                    j7 = ResourcesFlusher.g(parcel2, readInt);
                    break;
                case 12:
                    str6 = ResourcesFlusher.b(parcel2, readInt);
                    break;
                case 13:
                    j4 = ResourcesFlusher.g(parcel2, readInt);
                    break;
                case 14:
                    j5 = ResourcesFlusher.g(parcel2, readInt);
                    break;
                case 15:
                    i2 = ResourcesFlusher.f(parcel2, readInt);
                    break;
                case 16:
                    z3 = ResourcesFlusher.d(parcel2, readInt);
                    break;
                case 17:
                    z4 = ResourcesFlusher.d(parcel2, readInt);
                    break;
                case 18:
                    z5 = ResourcesFlusher.d(parcel2, readInt);
                    break;
                case 19:
                    str7 = ResourcesFlusher.b(parcel2, readInt);
                    break;
                case 20:
                default:
                    ResourcesFlusher.i(parcel2, readInt);
                    break;
                case 21:
                    int h = ResourcesFlusher.h(parcel2, readInt);
                    if (h != 0) {
                        ResourcesFlusher.c(parcel2, h, 4);
                        bool = Boolean.valueOf(parcel.readInt() != 0);
                        break;
                    } else {
                        bool = null;
                        break;
                    }
                case 22:
                    j6 = ResourcesFlusher.g(parcel2, readInt);
                    break;
                case 23:
                    int h2 = ResourcesFlusher.h(parcel2, readInt);
                    int dataPosition = parcel.dataPosition();
                    if (h2 != 0) {
                        ArrayList<String> createStringArrayList = parcel.createStringArrayList();
                        parcel2.setDataPosition(dataPosition + h2);
                        arrayList = createStringArrayList;
                        break;
                    } else {
                        arrayList = null;
                        break;
                    }
            }
        }
        ResourcesFlusher.c(parcel2, b);
        return new d9(str, str2, str3, str4, j2, j3, str5, z, z2, j7, str6, j4, j5, i2, z3, z4, z5, str7, bool, j6, arrayList);
    }

    public final /* synthetic */ Object[] newArray(int i2) {
        return new d9[i2];
    }
}
