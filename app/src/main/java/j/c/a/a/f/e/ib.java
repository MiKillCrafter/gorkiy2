package j.c.a.a.f.e;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.0.1 */
public final class ib extends q implements gb {
    public ib(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.measurement.api.internal.IEventHandlerProxy");
    }

    public final void a(String str, String str2, Bundle bundle, long j2) {
        Parcel g = g();
        g.writeString(str);
        g.writeString(str2);
        c2.a(g, bundle);
        g.writeLong(j2);
        b(1, g);
    }

    public final int a() {
        Parcel a = a(2, g());
        int readInt = a.readInt();
        a.recycle();
        return readInt;
    }
}
