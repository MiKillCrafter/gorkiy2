package j.c.a.a.g.a;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import i.b.k.ResourcesFlusher;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class j implements Parcelable.Creator<h> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = ResourcesFlusher.b(parcel);
        Bundle bundle = null;
        while (parcel.dataPosition() < b) {
            int readInt = parcel.readInt();
            if ((65535 & readInt) != 2) {
                ResourcesFlusher.i(parcel, readInt);
            } else {
                bundle = ResourcesFlusher.a(parcel, readInt);
            }
        }
        ResourcesFlusher.c(parcel, b);
        return new h(bundle);
    }

    public final /* synthetic */ Object[] newArray(int i2) {
        return new h[i2];
    }
}
