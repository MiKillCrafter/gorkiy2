package j.c.a.a.f.e;

import android.os.Bundle;
import j.c.a.a.f.e.pb;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.0.1 */
public final class m extends pb.a {

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ String f1874f;
    public final /* synthetic */ String g;
    public final /* synthetic */ boolean h;

    /* renamed from: i  reason: collision with root package name */
    public final /* synthetic */ m9 f1875i;

    /* renamed from: j  reason: collision with root package name */
    public final /* synthetic */ pb f1876j;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public m(pb pbVar, String str, String str2, boolean z, m9 m9Var) {
        super(true);
        this.f1876j = pbVar;
        this.f1874f = str;
        this.g = str2;
        this.h = z;
        this.f1875i = m9Var;
    }

    public final void a() {
        this.f1876j.g.getUserProperties(this.f1874f, this.g, this.h, this.f1875i);
    }

    public final void b() {
        this.f1875i.a((Bundle) null);
    }
}
