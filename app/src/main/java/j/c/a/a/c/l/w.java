package j.c.a.a.c.l;

import android.os.Parcelable;

public final class w implements Parcelable.Creator<f> {
    /* JADX WARN: Type inference failed for: r2v3, types: [java.lang.Object[]] */
    /* JADX WARN: Type inference failed for: r2v4, types: [android.os.Parcelable] */
    /* JADX WARN: Type inference failed for: r2v5, types: [java.lang.Object[]] */
    /* JADX WARN: Type inference failed for: r2v6, types: [java.lang.Object[]] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object createFromParcel(android.os.Parcel r17) {
        /*
            r16 = this;
            r0 = r17
            int r1 = i.b.k.ResourcesFlusher.b(r17)
            r2 = 0
            r3 = 0
            r8 = r3
            r9 = r8
            r10 = r9
            r11 = r10
            r12 = r11
            r13 = r12
            r14 = r13
            r5 = 0
            r6 = 0
            r7 = 0
            r15 = 0
        L_0x0013:
            int r2 = r17.dataPosition()
            if (r2 >= r1) goto L_0x0073
            int r2 = r17.readInt()
            r3 = 65535(0xffff, float:9.1834E-41)
            r3 = r3 & r2
            switch(r3) {
                case 1: goto L_0x006e;
                case 2: goto L_0x0069;
                case 3: goto L_0x0064;
                case 4: goto L_0x005f;
                case 5: goto L_0x005a;
                case 6: goto L_0x0050;
                case 7: goto L_0x004b;
                case 8: goto L_0x0041;
                case 9: goto L_0x0024;
                case 10: goto L_0x0037;
                case 11: goto L_0x002d;
                case 12: goto L_0x0028;
                default: goto L_0x0024;
            }
        L_0x0024:
            i.b.k.ResourcesFlusher.i(r0, r2)
            goto L_0x0013
        L_0x0028:
            boolean r15 = i.b.k.ResourcesFlusher.d(r0, r2)
            goto L_0x0013
        L_0x002d:
            android.os.Parcelable$Creator<j.c.a.a.c.d> r3 = j.c.a.a.c.d.CREATOR
            java.lang.Object[] r2 = i.b.k.ResourcesFlusher.b(r0, r2, r3)
            r14 = r2
            j.c.a.a.c.d[] r14 = (j.c.a.a.c.d[]) r14
            goto L_0x0013
        L_0x0037:
            android.os.Parcelable$Creator<j.c.a.a.c.d> r3 = j.c.a.a.c.d.CREATOR
            java.lang.Object[] r2 = i.b.k.ResourcesFlusher.b(r0, r2, r3)
            r13 = r2
            j.c.a.a.c.d[] r13 = (j.c.a.a.c.d[]) r13
            goto L_0x0013
        L_0x0041:
            android.os.Parcelable$Creator r3 = android.accounts.Account.CREATOR
            android.os.Parcelable r2 = i.b.k.ResourcesFlusher.a(r0, r2, r3)
            r12 = r2
            android.accounts.Account r12 = (android.accounts.Account) r12
            goto L_0x0013
        L_0x004b:
            android.os.Bundle r11 = i.b.k.ResourcesFlusher.a(r0, r2)
            goto L_0x0013
        L_0x0050:
            android.os.Parcelable$Creator<com.google.android.gms.common.api.Scope> r3 = com.google.android.gms.common.api.Scope.CREATOR
            java.lang.Object[] r2 = i.b.k.ResourcesFlusher.b(r0, r2, r3)
            r10 = r2
            com.google.android.gms.common.api.Scope[] r10 = (com.google.android.gms.common.api.Scope[]) r10
            goto L_0x0013
        L_0x005a:
            android.os.IBinder r9 = i.b.k.ResourcesFlusher.e(r0, r2)
            goto L_0x0013
        L_0x005f:
            java.lang.String r8 = i.b.k.ResourcesFlusher.b(r0, r2)
            goto L_0x0013
        L_0x0064:
            int r7 = i.b.k.ResourcesFlusher.f(r0, r2)
            goto L_0x0013
        L_0x0069:
            int r6 = i.b.k.ResourcesFlusher.f(r0, r2)
            goto L_0x0013
        L_0x006e:
            int r5 = i.b.k.ResourcesFlusher.f(r0, r2)
            goto L_0x0013
        L_0x0073:
            i.b.k.ResourcesFlusher.c(r0, r1)
            j.c.a.a.c.l.f r0 = new j.c.a.a.c.l.f
            r4 = r0
            r4.<init>(r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.c.l.w.createFromParcel(android.os.Parcel):java.lang.Object");
    }

    public final /* synthetic */ Object[] newArray(int i2) {
        return new f[i2];
    }
}
