package j.c.a.a.g.a;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import j.c.a.a.c.n.b;
import j.c.a.a.f.e.q5;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class f8 extends e5 {
    public Handler c;
    public long d;

    /* renamed from: e  reason: collision with root package name */
    public long f1989e;

    /* renamed from: f  reason: collision with root package name */
    public final b f1990f = new e8(this, this.a);
    public final b g = new h8(this, this.a);
    public final Runnable h = new g8(this);

    public f8(r4 r4Var) {
        super(r4Var);
        if (((b) this.a.f2092n) != null) {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            this.d = elapsedRealtime;
            this.f1989e = elapsedRealtime;
            return;
        }
        throw null;
    }

    public final void A() {
        d();
        if (this.c == null) {
            this.c = new q5(Looper.getMainLooper());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public final void a(long j2, boolean z) {
        d();
        A();
        this.f1990f.b();
        this.g.b();
        if (l().a(j2)) {
            l().f2122r.a(true);
            l().w.a(0);
        }
        if (z) {
            i9 i9Var = this.a.g;
            g3 q2 = q();
            super.w();
            String str = q2.c;
            if (i9Var == null) {
                throw null;
            } else if (i9Var.d(str, k.b0)) {
                l().v.a(j2);
            }
        }
        if (l().f2122r.a()) {
            b(j2, z);
        } else {
            this.g.a(Math.max(0L, 3600000 - l().w.a()));
        }
    }

    public final void b(long j2, boolean z) {
        d();
        if (((b) this.a.f2092n) != null) {
            a().f2052n.a("Session started, time", Long.valueOf(SystemClock.elapsedRealtime()));
            i9 i9Var = this.a.g;
            g3 q2 = q();
            super.w();
            String str = q2.c;
            if (i9Var != null) {
                Long valueOf = i9Var.d(str, k.Y) ? Long.valueOf(j2 / 1000) : null;
                p().a("auto", "_sid", valueOf, j2);
                l().f2122r.a(false);
                Bundle bundle = new Bundle();
                i9 i9Var2 = this.a.g;
                g3 q3 = q();
                super.w();
                String str2 = q3.c;
                if (i9Var2 != null) {
                    if (i9Var2.d(str2, k.Y)) {
                        bundle.putLong("_sid", valueOf.longValue());
                    }
                    if (this.a.g.a(k.J0) && z) {
                        bundle.putLong("_aib", 1);
                    }
                    p().a("auto", "_s", j2, bundle);
                    l().v.a(j2);
                    return;
                }
                throw null;
            }
            throw null;
        }
        throw null;
    }

    public final boolean y() {
        return false;
    }

    public final long z() {
        if (((b) this.a.f2092n) != null) {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            long j2 = elapsedRealtime - this.f1989e;
            this.f1989e = elapsedRealtime;
            return j2;
        }
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.g.a.y6.a(j.c.a.a.g.a.w6, android.os.Bundle, boolean):void
     arg types: [j.c.a.a.g.a.w6, android.os.Bundle, int]
     candidates:
      j.c.a.a.g.a.y6.a(j.c.a.a.g.a.y6, j.c.a.a.g.a.w6, boolean):void
      j.c.a.a.g.a.y6.a(android.app.Activity, j.c.a.a.g.a.w6, boolean):void
      j.c.a.a.g.a.y6.a(android.app.Activity, java.lang.String, java.lang.String):void
      j.c.a.a.g.a.y6.a(j.c.a.a.g.a.w6, android.os.Bundle, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public final boolean a(boolean z, boolean z2) {
        d();
        w();
        if (((b) this.a.f2092n) != null) {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            b4 b4Var = l().v;
            if (((b) this.a.f2092n) != null) {
                b4Var.a(System.currentTimeMillis());
                long j2 = elapsedRealtime - this.d;
                if (z || j2 >= 1000) {
                    l().w.a(j2);
                    a().f2052n.a("Recording user engagement, ms", Long.valueOf(j2));
                    Bundle bundle = new Bundle();
                    bundle.putLong("_et", j2);
                    y6.a(s().z(), bundle, true);
                    i9 i9Var = this.a.g;
                    g3 q2 = q();
                    super.w();
                    if (i9Var.h(q2.c)) {
                        i9 i9Var2 = this.a.g;
                        g3 q3 = q();
                        super.w();
                        if (i9Var2.d(q3.c, k.h0)) {
                            if (!z2) {
                                z();
                            }
                        } else if (z2) {
                            bundle.putLong("_fr", 1);
                        } else {
                            z();
                        }
                    }
                    i9 i9Var3 = this.a.g;
                    g3 q4 = q();
                    super.w();
                    if (!i9Var3.d(q4.c, k.h0) || !z2) {
                        p().a("auto", "_e", bundle);
                    }
                    this.d = elapsedRealtime;
                    this.g.b();
                    this.g.a(Math.max(0L, 3600000 - l().w.a()));
                    return true;
                }
                a().f2052n.a("Screen exposed for less than 1000 ms. Event not sent. time", Long.valueOf(j2));
                return false;
            }
            throw null;
        }
        throw null;
    }
}
