package j.c.a.a.f.e;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import j.c.a.a.d.a;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.0.1 */
public final class na extends q implements l8 {
    public na(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.measurement.api.internal.IAppMeasurementDynamiteService");
    }

    public final void beginAdUnitExposure(String str, long j2) {
        Parcel g = g();
        g.writeString(str);
        g.writeLong(j2);
        b(23, g);
    }

    public final void clearConditionalUserProperty(String str, String str2, Bundle bundle) {
        Parcel g = g();
        g.writeString(str);
        g.writeString(str2);
        c2.a(g, bundle);
        b(9, g);
    }

    public final void endAdUnitExposure(String str, long j2) {
        Parcel g = g();
        g.writeString(str);
        g.writeLong(j2);
        b(24, g);
    }

    public final void generateEventId(fb fbVar) {
        Parcel g = g();
        c2.a(g, fbVar);
        b(22, g);
    }

    public final void getAppInstanceId(fb fbVar) {
        Parcel g = g();
        c2.a(g, fbVar);
        b(20, g);
    }

    public final void getCachedAppInstanceId(fb fbVar) {
        Parcel g = g();
        c2.a(g, fbVar);
        b(19, g);
    }

    public final void getConditionalUserProperties(String str, String str2, fb fbVar) {
        Parcel g = g();
        g.writeString(str);
        g.writeString(str2);
        c2.a(g, fbVar);
        b(10, g);
    }

    public final void getCurrentScreenClass(fb fbVar) {
        Parcel g = g();
        c2.a(g, fbVar);
        b(17, g);
    }

    public final void getCurrentScreenName(fb fbVar) {
        Parcel g = g();
        c2.a(g, fbVar);
        b(16, g);
    }

    public final void getGmpAppId(fb fbVar) {
        Parcel g = g();
        c2.a(g, fbVar);
        b(21, g);
    }

    public final void getMaxUserProperties(String str, fb fbVar) {
        Parcel g = g();
        g.writeString(str);
        c2.a(g, fbVar);
        b(6, g);
    }

    public final void getTestFlag(fb fbVar, int i2) {
        Parcel g = g();
        c2.a(g, fbVar);
        g.writeInt(i2);
        b(38, g);
    }

    public final void getUserProperties(String str, String str2, boolean z, fb fbVar) {
        Parcel g = g();
        g.writeString(str);
        g.writeString(str2);
        c2.a(g, z);
        c2.a(g, fbVar);
        b(5, g);
    }

    public final void initForTests(Map map) {
        Parcel g = g();
        g.writeMap(map);
        b(37, g);
    }

    public final void initialize(a aVar, nb nbVar, long j2) {
        Parcel g = g();
        c2.a(g, aVar);
        c2.a(g, nbVar);
        g.writeLong(j2);
        b(1, g);
    }

    public final void isDataCollectionEnabled(fb fbVar) {
        Parcel g = g();
        c2.a(g, fbVar);
        b(40, g);
    }

    public final void logEvent(String str, String str2, Bundle bundle, boolean z, boolean z2, long j2) {
        Parcel g = g();
        g.writeString(str);
        g.writeString(str2);
        c2.a(g, bundle);
        g.writeInt(z ? 1 : 0);
        g.writeInt(z2 ? 1 : 0);
        g.writeLong(j2);
        b(2, g);
    }

    public final void logEventAndBundle(String str, String str2, Bundle bundle, fb fbVar, long j2) {
        Parcel g = g();
        g.writeString(str);
        g.writeString(str2);
        c2.a(g, bundle);
        c2.a(g, fbVar);
        g.writeLong(j2);
        b(3, g);
    }

    public final void logHealthData(int i2, String str, a aVar, a aVar2, a aVar3) {
        Parcel g = g();
        g.writeInt(i2);
        g.writeString(str);
        c2.a(g, aVar);
        c2.a(g, aVar2);
        c2.a(g, aVar3);
        b(33, g);
    }

    public final void onActivityCreated(a aVar, Bundle bundle, long j2) {
        Parcel g = g();
        c2.a(g, aVar);
        c2.a(g, bundle);
        g.writeLong(j2);
        b(27, g);
    }

    public final void onActivityDestroyed(a aVar, long j2) {
        Parcel g = g();
        c2.a(g, aVar);
        g.writeLong(j2);
        b(28, g);
    }

    public final void onActivityPaused(a aVar, long j2) {
        Parcel g = g();
        c2.a(g, aVar);
        g.writeLong(j2);
        b(29, g);
    }

    public final void onActivityResumed(a aVar, long j2) {
        Parcel g = g();
        c2.a(g, aVar);
        g.writeLong(j2);
        b(30, g);
    }

    public final void onActivitySaveInstanceState(a aVar, fb fbVar, long j2) {
        Parcel g = g();
        c2.a(g, aVar);
        c2.a(g, fbVar);
        g.writeLong(j2);
        b(31, g);
    }

    public final void onActivityStarted(a aVar, long j2) {
        Parcel g = g();
        c2.a(g, aVar);
        g.writeLong(j2);
        b(25, g);
    }

    public final void onActivityStopped(a aVar, long j2) {
        Parcel g = g();
        c2.a(g, aVar);
        g.writeLong(j2);
        b(26, g);
    }

    public final void performAction(Bundle bundle, fb fbVar, long j2) {
        Parcel g = g();
        c2.a(g, bundle);
        c2.a(g, fbVar);
        g.writeLong(j2);
        b(32, g);
    }

    public final void registerOnMeasurementEventListener(gb gbVar) {
        Parcel g = g();
        c2.a(g, gbVar);
        b(35, g);
    }

    public final void resetAnalyticsData(long j2) {
        Parcel g = g();
        g.writeLong(j2);
        b(12, g);
    }

    public final void setConditionalUserProperty(Bundle bundle, long j2) {
        Parcel g = g();
        c2.a(g, bundle);
        g.writeLong(j2);
        b(8, g);
    }

    public final void setCurrentScreen(a aVar, String str, String str2, long j2) {
        Parcel g = g();
        c2.a(g, aVar);
        g.writeString(str);
        g.writeString(str2);
        g.writeLong(j2);
        b(15, g);
    }

    public final void setDataCollectionEnabled(boolean z) {
        Parcel g = g();
        c2.a(g, z);
        b(39, g);
    }

    public final void setEventInterceptor(gb gbVar) {
        Parcel g = g();
        c2.a(g, gbVar);
        b(34, g);
    }

    public final void setInstanceIdProvider(lb lbVar) {
        Parcel g = g();
        c2.a(g, lbVar);
        b(18, g);
    }

    public final void setMeasurementEnabled(boolean z, long j2) {
        Parcel g = g();
        c2.a(g, z);
        g.writeLong(j2);
        b(11, g);
    }

    public final void setMinimumSessionDuration(long j2) {
        Parcel g = g();
        g.writeLong(j2);
        b(13, g);
    }

    public final void setSessionTimeoutDuration(long j2) {
        Parcel g = g();
        g.writeLong(j2);
        b(14, g);
    }

    public final void setUserId(String str, long j2) {
        Parcel g = g();
        g.writeString(str);
        g.writeLong(j2);
        b(7, g);
    }

    public final void setUserProperty(String str, String str2, a aVar, boolean z, long j2) {
        Parcel g = g();
        g.writeString(str);
        g.writeString(str2);
        c2.a(g, aVar);
        g.writeInt(z ? 1 : 0);
        g.writeLong(j2);
        b(4, g);
    }

    public final void unregisterOnMeasurementEventListener(gb gbVar) {
        Parcel g = g();
        c2.a(g, gbVar);
        b(36, g);
    }
}
