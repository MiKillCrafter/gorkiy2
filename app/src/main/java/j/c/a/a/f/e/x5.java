package j.c.a.a.f.e;

import java.lang.Comparable;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public class x5<K extends Comparable<K>, V> extends AbstractMap<K, V> {
    public final int b;
    public List<a6> c = Collections.emptyList();
    public Map<K, V> d = Collections.emptyMap();

    /* renamed from: e  reason: collision with root package name */
    public boolean f1927e;

    /* renamed from: f  reason: collision with root package name */
    public volatile c6 f1928f;
    public Map<K, V> g = Collections.emptyMap();

    public /* synthetic */ x5(int i2, w5 w5Var) {
        this.b = i2;
    }

    public static <FieldDescriptorType extends o3<FieldDescriptorType>> x5<FieldDescriptorType, Object> c(int i2) {
        return new w5(i2);
    }

    public void a() {
        Map<K, V> map;
        Map<K, V> map2;
        if (!this.f1927e) {
            if (this.d.isEmpty()) {
                map = Collections.emptyMap();
            } else {
                map = Collections.unmodifiableMap(this.d);
            }
            this.d = map;
            if (this.g.isEmpty()) {
                map2 = Collections.emptyMap();
            } else {
                map2 = Collections.unmodifiableMap(this.g);
            }
            this.g = map2;
            this.f1927e = true;
        }
    }

    public final int b() {
        return this.c.size();
    }

    public void clear() {
        d();
        if (!this.c.isEmpty()) {
            this.c.clear();
        }
        if (!this.d.isEmpty()) {
            this.d.clear();
        }
    }

    public boolean containsKey(Object obj) {
        Comparable comparable = (Comparable) obj;
        return a(comparable) >= 0 || this.d.containsKey(comparable);
    }

    public final void d() {
        if (this.f1927e) {
            throw new UnsupportedOperationException();
        }
    }

    public final SortedMap<K, V> e() {
        d();
        if (this.d.isEmpty() && !(this.d instanceof TreeMap)) {
            TreeMap treeMap = new TreeMap();
            this.d = treeMap;
            this.g = treeMap.descendingMap();
        }
        return (SortedMap) this.d;
    }

    public Set<Map.Entry<K, V>> entrySet() {
        if (this.f1928f == null) {
            this.f1928f = new c6(this, null);
        }
        return this.f1928f;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof x5)) {
            return super.equals(obj);
        }
        x5 x5Var = (x5) obj;
        int size = size();
        if (size != x5Var.size()) {
            return false;
        }
        int b2 = b();
        if (b2 != x5Var.b()) {
            return entrySet().equals(x5Var.entrySet());
        }
        for (int i2 = 0; i2 < b2; i2++) {
            if (!a(i2).equals(x5Var.a(i2))) {
                return false;
            }
        }
        if (b2 != size) {
            return this.d.equals(x5Var.d);
        }
        return true;
    }

    public V get(Object obj) {
        Comparable comparable = (Comparable) obj;
        int a = a(comparable);
        if (a >= 0) {
            return this.c.get(a).c;
        }
        return this.d.get(comparable);
    }

    public int hashCode() {
        int b2 = b();
        int i2 = 0;
        for (int i3 = 0; i3 < b2; i3++) {
            i2 += this.c.get(i3).hashCode();
        }
        return this.d.size() > 0 ? i2 + this.d.hashCode() : i2;
    }

    public V remove(Object obj) {
        d();
        Comparable comparable = (Comparable) obj;
        int a = a(comparable);
        if (a >= 0) {
            return b(a);
        }
        if (this.d.isEmpty()) {
            return null;
        }
        return this.d.remove(comparable);
    }

    public int size() {
        return this.d.size() + this.c.size();
    }

    public final V b(int i2) {
        d();
        V v = this.c.remove(i2).c;
        if (!this.d.isEmpty()) {
            Iterator it = e().entrySet().iterator();
            this.c.add(new a6(this, (Map.Entry) it.next()));
            it.remove();
        }
        return v;
    }

    public final Iterable<Map.Entry<K, V>> c() {
        if (this.d.isEmpty()) {
            return z5.b;
        }
        return this.d.entrySet();
    }

    public final Map.Entry<K, V> a(int i2) {
        return this.c.get(i2);
    }

    /* renamed from: a */
    public final V put(K k2, V v) {
        d();
        int a = a((Comparable) k2);
        if (a >= 0) {
            a6 a6Var = this.c.get(a);
            a6Var.d.d();
            V v2 = a6Var.c;
            a6Var.c = v;
            return v2;
        }
        d();
        if (this.c.isEmpty() && !(this.c instanceof ArrayList)) {
            this.c = new ArrayList(this.b);
        }
        int i2 = -(a + 1);
        if (i2 >= this.b) {
            return e().put(k2, v);
        }
        int size = this.c.size();
        int i3 = this.b;
        if (size == i3) {
            a6 remove = this.c.remove(i3 - 1);
            e().put(remove.b, remove.c);
        }
        this.c.add(i2, new a6(this, k2, v));
        return null;
    }

    public final int a(Comparable comparable) {
        int size = this.c.size() - 1;
        if (size >= 0) {
            int compareTo = comparable.compareTo(this.c.get(size).b);
            if (compareTo > 0) {
                return -(size + 2);
            }
            if (compareTo == 0) {
                return size;
            }
        }
        int i2 = 0;
        while (i2 <= size) {
            int i3 = (i2 + size) / 2;
            int compareTo2 = comparable.compareTo(this.c.get(i3).b);
            if (compareTo2 < 0) {
                size = i3 - 1;
            } else if (compareTo2 <= 0) {
                return i3;
            } else {
                i2 = i3 + 1;
            }
        }
        return -(i2 + 1);
    }
}
