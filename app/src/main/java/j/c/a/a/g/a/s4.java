package j.c.a.a.g.a;

import android.os.Binder;
import android.text.TextUtils;
import com.crashlytics.android.answers.RetryManager;
import i.b.k.ResourcesFlusher;
import j.c.a.a.c.g;
import j.c.a.a.c.h;
import j.c.a.a.c.n.b;
import j.c.a.a.c.n.c;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class s4 extends e3 {
    public final q8 a;
    public Boolean b;
    public String c = null;

    public s4(q8 q8Var) {
        ResourcesFlusher.b(q8Var);
        this.a = q8Var;
    }

    public final void a(i iVar, d9 d9Var) {
        ResourcesFlusher.b(iVar);
        e(d9Var);
        a(new g5(this, iVar, d9Var));
    }

    public final String b(d9 d9Var) {
        e(d9Var);
        q8 q8Var = this.a;
        try {
            return (String) ((FutureTask) q8Var.f2073i.i().a(new t8(q8Var, d9Var))).get(30000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e2) {
            q8Var.f2073i.a().f2046f.a("Failed to get app instance id. appId", n3.a(d9Var.b), e2);
            return null;
        }
    }

    public final void c(d9 d9Var) {
        e(d9Var);
        a(new v4(this, d9Var));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.g.a.s4.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      j.c.a.a.g.a.s4.a(j.c.a.a.g.a.d9, boolean):java.util.List<j.c.a.a.g.a.x8>
      j.c.a.a.g.a.s4.a(j.c.a.a.g.a.g9, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.s4.a(j.c.a.a.g.a.i, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.s4.a(j.c.a.a.g.a.x8, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.s4.a(j.c.a.a.g.a.i, java.lang.String):byte[]
      j.c.a.a.g.a.f3.a(j.c.a.a.g.a.d9, boolean):java.util.List<j.c.a.a.g.a.x8>
      j.c.a.a.g.a.f3.a(j.c.a.a.g.a.g9, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.f3.a(j.c.a.a.g.a.i, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.f3.a(j.c.a.a.g.a.x8, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.f3.a(j.c.a.a.g.a.i, java.lang.String):byte[]
      j.c.a.a.g.a.s4.a(java.lang.String, boolean):void */
    public final void d(d9 d9Var) {
        a(d9Var.b, false);
        a(new c5(this, d9Var));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.g.a.s4.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      j.c.a.a.g.a.s4.a(j.c.a.a.g.a.d9, boolean):java.util.List<j.c.a.a.g.a.x8>
      j.c.a.a.g.a.s4.a(j.c.a.a.g.a.g9, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.s4.a(j.c.a.a.g.a.i, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.s4.a(j.c.a.a.g.a.x8, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.s4.a(j.c.a.a.g.a.i, java.lang.String):byte[]
      j.c.a.a.g.a.f3.a(j.c.a.a.g.a.d9, boolean):java.util.List<j.c.a.a.g.a.x8>
      j.c.a.a.g.a.f3.a(j.c.a.a.g.a.g9, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.f3.a(j.c.a.a.g.a.i, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.f3.a(j.c.a.a.g.a.x8, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.f3.a(j.c.a.a.g.a.i, java.lang.String):byte[]
      j.c.a.a.g.a.s4.a(java.lang.String, boolean):void */
    public final void e(d9 d9Var) {
        ResourcesFlusher.b(d9Var);
        a(d9Var.b, false);
        this.a.f2073i.n().c(d9Var.c, d9Var.f1963s);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.g.a.s4.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      j.c.a.a.g.a.s4.a(j.c.a.a.g.a.d9, boolean):java.util.List<j.c.a.a.g.a.x8>
      j.c.a.a.g.a.s4.a(j.c.a.a.g.a.g9, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.s4.a(j.c.a.a.g.a.i, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.s4.a(j.c.a.a.g.a.x8, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.s4.a(j.c.a.a.g.a.i, java.lang.String):byte[]
      j.c.a.a.g.a.f3.a(j.c.a.a.g.a.d9, boolean):java.util.List<j.c.a.a.g.a.x8>
      j.c.a.a.g.a.f3.a(j.c.a.a.g.a.g9, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.f3.a(j.c.a.a.g.a.i, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.f3.a(j.c.a.a.g.a.x8, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.f3.a(j.c.a.a.g.a.i, java.lang.String):byte[]
      j.c.a.a.g.a.s4.a(java.lang.String, boolean):void */
    public final void a(i iVar, String str, String str2) {
        ResourcesFlusher.b(iVar);
        ResourcesFlusher.b(str);
        a(str, true);
        a(new f5(this, iVar, str));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.g.a.s4.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      j.c.a.a.g.a.s4.a(j.c.a.a.g.a.d9, boolean):java.util.List<j.c.a.a.g.a.x8>
      j.c.a.a.g.a.s4.a(j.c.a.a.g.a.g9, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.s4.a(j.c.a.a.g.a.i, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.s4.a(j.c.a.a.g.a.x8, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.s4.a(j.c.a.a.g.a.i, java.lang.String):byte[]
      j.c.a.a.g.a.f3.a(j.c.a.a.g.a.d9, boolean):java.util.List<j.c.a.a.g.a.x8>
      j.c.a.a.g.a.f3.a(j.c.a.a.g.a.g9, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.f3.a(j.c.a.a.g.a.i, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.f3.a(j.c.a.a.g.a.x8, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.f3.a(j.c.a.a.g.a.i, java.lang.String):byte[]
      j.c.a.a.g.a.s4.a(java.lang.String, boolean):void */
    public final byte[] a(i iVar, String str) {
        ResourcesFlusher.b(str);
        ResourcesFlusher.b(iVar);
        a(str, true);
        this.a.a().f2051m.a("Log and bundle. event", this.a.l().a(iVar.b));
        if (((b) this.a.f2073i.f2092n) != null) {
            long nanoTime = System.nanoTime() / RetryManager.NANOSECONDS_IN_MS;
            l4 i2 = this.a.i();
            i5 i5Var = new i5(this, iVar, str);
            i2.o();
            ResourcesFlusher.b(i5Var);
            p4 p4Var = new p4(i2, i5Var, true, "Task exception on worker thread");
            if (Thread.currentThread() == i2.c) {
                p4Var.run();
            } else {
                i2.a((p4<?>) p4Var);
            }
            try {
                byte[] bArr = (byte[]) p4Var.get();
                if (bArr == null) {
                    this.a.a().f2046f.a("Log and bundle returned null. appId", n3.a(str));
                    bArr = new byte[0];
                }
                if (((b) this.a.f2073i.f2092n) != null) {
                    this.a.a().f2051m.a("Log and bundle processed. event, size, time_ms", this.a.l().a(iVar.b), Integer.valueOf(bArr.length), Long.valueOf((System.nanoTime() / RetryManager.NANOSECONDS_IN_MS) - nanoTime));
                    return bArr;
                }
                throw null;
            } catch (InterruptedException | ExecutionException e2) {
                this.a.a().f2046f.a("Failed to log and bundle. appId, event, error", n3.a(str), this.a.l().a(iVar.b), e2);
                return null;
            }
        } else {
            throw null;
        }
    }

    public final void a(x8 x8Var, d9 d9Var) {
        ResourcesFlusher.b(x8Var);
        e(d9Var);
        if (x8Var.a() == null) {
            a(new h5(this, x8Var, d9Var));
        } else {
            a(new k5(this, x8Var, d9Var));
        }
    }

    public final List<x8> a(d9 d9Var, boolean z) {
        e(d9Var);
        try {
            List<z8> list = (List) ((FutureTask) this.a.i().a(new j5(this, d9Var))).get();
            ArrayList arrayList = new ArrayList(list.size());
            for (z8 z8Var : list) {
                if (z || !y8.f(z8Var.c)) {
                    arrayList.add(new x8(z8Var));
                }
            }
            return arrayList;
        } catch (InterruptedException | ExecutionException e2) {
            this.a.a().f2046f.a("Failed to get user attributes. appId", n3.a(d9Var.b), e2);
            return null;
        }
    }

    public final void a(d9 d9Var) {
        e(d9Var);
        a(new m5(this, d9Var));
    }

    public final void a(String str, boolean z) {
        boolean z2;
        if (!TextUtils.isEmpty(str)) {
            if (z) {
                try {
                    if (this.b == null) {
                        if (!"com.google.android.gms".equals(this.c) && !c.b(this.a.f2073i.a, Binder.getCallingUid())) {
                            if (!h.a(this.a.f2073i.a).a(Binder.getCallingUid())) {
                                z2 = false;
                                this.b = Boolean.valueOf(z2);
                            }
                        }
                        z2 = true;
                        this.b = Boolean.valueOf(z2);
                    }
                    if (this.b.booleanValue()) {
                        return;
                    }
                } catch (SecurityException e2) {
                    this.a.a().f2046f.a("Measurement Service called with invalid calling package. appId", n3.a(str));
                    throw e2;
                }
            }
            if (this.c == null && g.uidHasPackageName(this.a.f2073i.a, Binder.getCallingUid(), str)) {
                this.c = str;
            }
            if (!str.equals(this.c)) {
                throw new SecurityException(String.format("Unknown calling package name '%s'.", str));
            }
            return;
        }
        this.a.a().f2046f.a("Measurement Service called without app package");
        throw new SecurityException("Measurement Service called without app package");
    }

    public final void a(long j2, String str, String str2, String str3) {
        a(new l5(this, str2, str3, str, j2));
    }

    public final void a(g9 g9Var, d9 d9Var) {
        ResourcesFlusher.b(g9Var);
        ResourcesFlusher.b(g9Var.d);
        e(d9Var);
        g9 g9Var2 = new g9(g9Var);
        g9Var2.b = d9Var.b;
        if (g9Var.d.a() == null) {
            a(new u4(this, g9Var2, d9Var));
        } else {
            a(new x4(this, g9Var2, d9Var));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.g.a.s4.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      j.c.a.a.g.a.s4.a(j.c.a.a.g.a.d9, boolean):java.util.List<j.c.a.a.g.a.x8>
      j.c.a.a.g.a.s4.a(j.c.a.a.g.a.g9, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.s4.a(j.c.a.a.g.a.i, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.s4.a(j.c.a.a.g.a.x8, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.s4.a(j.c.a.a.g.a.i, java.lang.String):byte[]
      j.c.a.a.g.a.f3.a(j.c.a.a.g.a.d9, boolean):java.util.List<j.c.a.a.g.a.x8>
      j.c.a.a.g.a.f3.a(j.c.a.a.g.a.g9, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.f3.a(j.c.a.a.g.a.i, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.f3.a(j.c.a.a.g.a.x8, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.f3.a(j.c.a.a.g.a.i, java.lang.String):byte[]
      j.c.a.a.g.a.s4.a(java.lang.String, boolean):void */
    public final void a(g9 g9Var) {
        ResourcesFlusher.b(g9Var);
        ResourcesFlusher.b(g9Var.d);
        a(g9Var.b, true);
        g9 g9Var2 = new g9(g9Var);
        if (g9Var.d.a() == null) {
            a(new w4(this, g9Var2));
        } else {
            a(new z4(this, g9Var2));
        }
    }

    public final List<x8> a(String str, String str2, boolean z, d9 d9Var) {
        e(d9Var);
        try {
            List<z8> list = (List) ((FutureTask) this.a.i().a(new y4(this, d9Var, str, str2))).get();
            ArrayList arrayList = new ArrayList(list.size());
            for (z8 z8Var : list) {
                if (z || !y8.f(z8Var.c)) {
                    arrayList.add(new x8(z8Var));
                }
            }
            return arrayList;
        } catch (InterruptedException | ExecutionException e2) {
            this.a.a().f2046f.a("Failed to get user attributes. appId", n3.a(d9Var.b), e2);
            return Collections.emptyList();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.g.a.s4.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      j.c.a.a.g.a.s4.a(j.c.a.a.g.a.d9, boolean):java.util.List<j.c.a.a.g.a.x8>
      j.c.a.a.g.a.s4.a(j.c.a.a.g.a.g9, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.s4.a(j.c.a.a.g.a.i, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.s4.a(j.c.a.a.g.a.x8, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.s4.a(j.c.a.a.g.a.i, java.lang.String):byte[]
      j.c.a.a.g.a.f3.a(j.c.a.a.g.a.d9, boolean):java.util.List<j.c.a.a.g.a.x8>
      j.c.a.a.g.a.f3.a(j.c.a.a.g.a.g9, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.f3.a(j.c.a.a.g.a.i, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.f3.a(j.c.a.a.g.a.x8, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.f3.a(j.c.a.a.g.a.i, java.lang.String):byte[]
      j.c.a.a.g.a.s4.a(java.lang.String, boolean):void */
    public final List<x8> a(String str, String str2, String str3, boolean z) {
        a(str, true);
        try {
            List<z8> list = (List) ((FutureTask) this.a.i().a(new b5(this, str, str2, str3))).get();
            ArrayList arrayList = new ArrayList(list.size());
            for (z8 z8Var : list) {
                if (z || !y8.f(z8Var.c)) {
                    arrayList.add(new x8(z8Var));
                }
            }
            return arrayList;
        } catch (InterruptedException | ExecutionException e2) {
            this.a.a().f2046f.a("Failed to get user attributes. appId", n3.a(str), e2);
            return Collections.emptyList();
        }
    }

    public final List<g9> a(String str, String str2, d9 d9Var) {
        e(d9Var);
        try {
            return (List) ((FutureTask) this.a.i().a(new a5(this, d9Var, str, str2))).get();
        } catch (InterruptedException | ExecutionException e2) {
            this.a.a().f2046f.a("Failed to get conditional user properties", e2);
            return Collections.emptyList();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.g.a.s4.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      j.c.a.a.g.a.s4.a(j.c.a.a.g.a.d9, boolean):java.util.List<j.c.a.a.g.a.x8>
      j.c.a.a.g.a.s4.a(j.c.a.a.g.a.g9, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.s4.a(j.c.a.a.g.a.i, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.s4.a(j.c.a.a.g.a.x8, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.s4.a(j.c.a.a.g.a.i, java.lang.String):byte[]
      j.c.a.a.g.a.f3.a(j.c.a.a.g.a.d9, boolean):java.util.List<j.c.a.a.g.a.x8>
      j.c.a.a.g.a.f3.a(j.c.a.a.g.a.g9, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.f3.a(j.c.a.a.g.a.i, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.f3.a(j.c.a.a.g.a.x8, j.c.a.a.g.a.d9):void
      j.c.a.a.g.a.f3.a(j.c.a.a.g.a.i, java.lang.String):byte[]
      j.c.a.a.g.a.s4.a(java.lang.String, boolean):void */
    public final List<g9> a(String str, String str2, String str3) {
        a(str, true);
        try {
            return (List) ((FutureTask) this.a.i().a(new d5(this, str, str2, str3))).get();
        } catch (InterruptedException | ExecutionException e2) {
            this.a.a().f2046f.a("Failed to get conditional user properties", e2);
            return Collections.emptyList();
        }
    }

    public final void a(Runnable runnable) {
        ResourcesFlusher.b(runnable);
        if (this.a.i().t()) {
            runnable.run();
            return;
        }
        l4 i2 = this.a.i();
        i2.o();
        ResourcesFlusher.b(runnable);
        i2.a((p4<?>) new p4(i2, runnable, "Task exception on worker thread"));
    }
}
