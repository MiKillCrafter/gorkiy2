package j.c.a.a.g.a;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class j3 extends e5 {
    public final i3 c = new i3(this, this.a.a, "google_app_measurement_local.db");
    public boolean d;

    public j3(r4 r4Var) {
        super(r4Var);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:42:0x008d, code lost:
        r3 = r3 + 1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean A() {
        /*
            r11 = this;
            java.lang.String r0 = "Error deleting app launch break from local database"
            r11.d()
            r11.b()
            boolean r1 = r11.d
            r2 = 0
            if (r1 == 0) goto L_0x000e
            return r2
        L_0x000e:
            j.c.a.a.g.a.r4 r1 = r11.a
            android.content.Context r1 = r1.a
            java.lang.String r3 = "google_app_measurement_local.db"
            java.io.File r1 = r1.getDatabasePath(r3)
            boolean r1 = r1.exists()
            if (r1 != 0) goto L_0x001f
            return r2
        L_0x001f:
            r1 = 5
            r3 = 0
            r4 = 5
        L_0x0022:
            if (r3 >= r1) goto L_0x0096
            r5 = 0
            r6 = 1
            android.database.sqlite.SQLiteDatabase r5 = r11.B()     // Catch:{ SQLiteFullException -> 0x007c, SQLiteDatabaseLockedException -> 0x0070, SQLiteException -> 0x0053 }
            if (r5 != 0) goto L_0x0034
            r11.d = r6     // Catch:{ SQLiteFullException -> 0x007c, SQLiteDatabaseLockedException -> 0x0070, SQLiteException -> 0x0053 }
            if (r5 == 0) goto L_0x0033
            r5.close()
        L_0x0033:
            return r2
        L_0x0034:
            r5.beginTransaction()     // Catch:{ SQLiteFullException -> 0x007c, SQLiteDatabaseLockedException -> 0x0070, SQLiteException -> 0x0053 }
            java.lang.String r7 = "messages"
            java.lang.String r8 = "type == ?"
            java.lang.String[] r9 = new java.lang.String[r6]     // Catch:{ SQLiteFullException -> 0x007c, SQLiteDatabaseLockedException -> 0x0070, SQLiteException -> 0x0053 }
            r10 = 3
            java.lang.String r10 = java.lang.Integer.toString(r10)     // Catch:{ SQLiteFullException -> 0x007c, SQLiteDatabaseLockedException -> 0x0070, SQLiteException -> 0x0053 }
            r9[r2] = r10     // Catch:{ SQLiteFullException -> 0x007c, SQLiteDatabaseLockedException -> 0x0070, SQLiteException -> 0x0053 }
            r5.delete(r7, r8, r9)     // Catch:{ SQLiteFullException -> 0x007c, SQLiteDatabaseLockedException -> 0x0070, SQLiteException -> 0x0053 }
            r5.setTransactionSuccessful()     // Catch:{ SQLiteFullException -> 0x007c, SQLiteDatabaseLockedException -> 0x0070, SQLiteException -> 0x0053 }
            r5.endTransaction()     // Catch:{ SQLiteFullException -> 0x007c, SQLiteDatabaseLockedException -> 0x0070, SQLiteException -> 0x0053 }
            r5.close()
            return r6
        L_0x0051:
            r0 = move-exception
            goto L_0x0090
        L_0x0053:
            r7 = move-exception
            if (r5 == 0) goto L_0x005f
            boolean r8 = r5.inTransaction()     // Catch:{ all -> 0x0051 }
            if (r8 == 0) goto L_0x005f
            r5.endTransaction()     // Catch:{ all -> 0x0051 }
        L_0x005f:
            j.c.a.a.g.a.n3 r8 = r11.a()     // Catch:{ all -> 0x0051 }
            j.c.a.a.g.a.p3 r8 = r8.f2046f     // Catch:{ all -> 0x0051 }
            r8.a(r0, r7)     // Catch:{ all -> 0x0051 }
            r11.d = r6     // Catch:{ all -> 0x0051 }
            if (r5 == 0) goto L_0x008d
            r5.close()
            goto L_0x008d
        L_0x0070:
            long r6 = (long) r4
            android.os.SystemClock.sleep(r6)     // Catch:{ all -> 0x0051 }
            int r4 = r4 + 20
            if (r5 == 0) goto L_0x008d
            r5.close()
            goto L_0x008d
        L_0x007c:
            r7 = move-exception
            j.c.a.a.g.a.n3 r8 = r11.a()     // Catch:{ all -> 0x0051 }
            j.c.a.a.g.a.p3 r8 = r8.f2046f     // Catch:{ all -> 0x0051 }
            r8.a(r0, r7)     // Catch:{ all -> 0x0051 }
            r11.d = r6     // Catch:{ all -> 0x0051 }
            if (r5 == 0) goto L_0x008d
            r5.close()
        L_0x008d:
            int r3 = r3 + 1
            goto L_0x0022
        L_0x0090:
            if (r5 == 0) goto L_0x0095
            r5.close()
        L_0x0095:
            throw r0
        L_0x0096:
            j.c.a.a.g.a.n3 r0 = r11.a()
            j.c.a.a.g.a.p3 r0 = r0.f2047i
            java.lang.String r1 = "Error deleting app launch break from local database in reasonable time"
            r0.a(r1)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.j3.A():boolean");
    }

    public final SQLiteDatabase B() {
        if (this.d) {
            return null;
        }
        SQLiteDatabase writableDatabase = this.c.getWritableDatabase();
        if (writableDatabase != null) {
            return writableDatabase;
        }
        this.d = true;
        return null;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:79:0x010d */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:70:0x00fa */
    /* JADX WARN: Type inference failed for: r3v0 */
    /* JADX WARN: Type inference failed for: r3v1, types: [int, boolean] */
    /* JADX WARN: Type inference failed for: r8v0 */
    /* JADX WARN: Type inference failed for: r8v1, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r8v2 */
    /* JADX WARN: Type inference failed for: r3v2 */
    /* JADX WARN: Type inference failed for: r8v3, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r8v4, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r8v5 */
    /* JADX WARN: Type inference failed for: r8v6, types: [android.database.sqlite.SQLiteDatabase] */
    /* JADX WARN: Type inference failed for: r8v7 */
    /* JADX WARN: Type inference failed for: r8v8 */
    /* JADX WARN: Type inference failed for: r8v9 */
    /* JADX WARN: Type inference failed for: r8v13 */
    /* JADX WARN: Type inference failed for: r8v14 */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x003a, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003b, code lost:
        r13 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003e, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00c4, code lost:
        r8 = 0;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:100:0x0123 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x0123 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:47:? A[ExcHandler: SQLiteDatabaseLockedException (unused android.database.sqlite.SQLiteDatabaseLockedException), SYNTHETIC, Splitter:B:9:0x0032] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00d3 A[SYNTHETIC, Splitter:B:55:0x00d3] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00ea  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x00ef  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0101  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0106  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x011b  */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x0120  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x012c  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x0131  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x0123 A[SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(int r18, byte[] r19) {
        /*
            r17 = this;
            r1 = r17
            java.lang.String r2 = "Error writing entry to local database"
            r17.b()
            r17.d()
            boolean r0 = r1.d
            r3 = 0
            if (r0 == 0) goto L_0x0010
            return r3
        L_0x0010:
            android.content.ContentValues r4 = new android.content.ContentValues
            r4.<init>()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r18)
            java.lang.String r5 = "type"
            r4.put(r5, r0)
            java.lang.String r0 = "entry"
            r5 = r19
            r4.put(r0, r5)
            r5 = 5
            r6 = 0
            r7 = 5
        L_0x0028:
            if (r6 >= r5) goto L_0x0135
            r8 = 0
            r9 = 1
            android.database.sqlite.SQLiteDatabase r10 = r17.B()     // Catch:{ SQLiteFullException -> 0x010a, SQLiteDatabaseLockedException -> 0x00f7, SQLiteException -> 0x00ce, all -> 0x00ca }
            if (r10 != 0) goto L_0x0041
            r1.d = r9     // Catch:{ SQLiteFullException -> 0x003e, SQLiteDatabaseLockedException -> 0x00c4, SQLiteException -> 0x003a }
            if (r10 == 0) goto L_0x0039
            r10.close()
        L_0x0039:
            return r3
        L_0x003a:
            r0 = move-exception
            r13 = r8
            goto L_0x00c2
        L_0x003e:
            r0 = move-exception
            goto L_0x010d
        L_0x0041:
            r10.beginTransaction()     // Catch:{ SQLiteFullException -> 0x00c7, SQLiteDatabaseLockedException -> 0x00c4, SQLiteException -> 0x00bf, all -> 0x00bb }
            r11 = 0
            java.lang.String r0 = "select count(1) from messages"
            android.database.Cursor r13 = r10.rawQuery(r0, r8)     // Catch:{ SQLiteFullException -> 0x00c7, SQLiteDatabaseLockedException -> 0x00c4, SQLiteException -> 0x00bf, all -> 0x00bb }
            if (r13 == 0) goto L_0x0062
            boolean r0 = r13.moveToFirst()     // Catch:{ SQLiteFullException -> 0x005e, SQLiteDatabaseLockedException -> 0x00b9, SQLiteException -> 0x005c, all -> 0x0059 }
            if (r0 == 0) goto L_0x0062
            long r11 = r13.getLong(r3)     // Catch:{ SQLiteFullException -> 0x005e, SQLiteDatabaseLockedException -> 0x00b9, SQLiteException -> 0x005c, all -> 0x0059 }
            goto L_0x0062
        L_0x0059:
            r0 = move-exception
            goto L_0x00f5
        L_0x005c:
            r0 = move-exception
            goto L_0x00c2
        L_0x005e:
            r0 = move-exception
            r8 = r13
            goto L_0x010d
        L_0x0062:
            java.lang.String r0 = "messages"
            r14 = 100000(0x186a0, double:4.94066E-319)
            int r16 = (r11 > r14 ? 1 : (r11 == r14 ? 0 : -1))
            if (r16 < 0) goto L_0x00a5
            j.c.a.a.g.a.n3 r5 = r17.a()     // Catch:{ SQLiteFullException -> 0x005e, SQLiteDatabaseLockedException -> 0x00b9, SQLiteException -> 0x005c, all -> 0x0059 }
            j.c.a.a.g.a.p3 r5 = r5.f2046f     // Catch:{ SQLiteFullException -> 0x005e, SQLiteDatabaseLockedException -> 0x00b9, SQLiteException -> 0x005c, all -> 0x0059 }
            java.lang.String r8 = "Data loss, local db full"
            r5.a(r8)     // Catch:{ SQLiteFullException -> 0x005e, SQLiteDatabaseLockedException -> 0x00b9, SQLiteException -> 0x005c, all -> 0x0059 }
            long r14 = r14 - r11
            r11 = 1
            long r14 = r14 + r11
            java.lang.String r5 = "rowid in (select rowid from messages order by rowid asc limit ?)"
            java.lang.String[] r8 = new java.lang.String[r9]     // Catch:{ SQLiteFullException -> 0x005e, SQLiteDatabaseLockedException -> 0x00b9, SQLiteException -> 0x005c, all -> 0x0059 }
            java.lang.String r11 = java.lang.Long.toString(r14)     // Catch:{ SQLiteFullException -> 0x005e, SQLiteDatabaseLockedException -> 0x00b9, SQLiteException -> 0x005c, all -> 0x0059 }
            r8[r3] = r11     // Catch:{ SQLiteFullException -> 0x005e, SQLiteDatabaseLockedException -> 0x00b9, SQLiteException -> 0x005c, all -> 0x0059 }
            int r5 = r10.delete(r0, r5, r8)     // Catch:{ SQLiteFullException -> 0x005e, SQLiteDatabaseLockedException -> 0x00b9, SQLiteException -> 0x005c, all -> 0x0059 }
            long r11 = (long) r5     // Catch:{ SQLiteFullException -> 0x005e, SQLiteDatabaseLockedException -> 0x00b9, SQLiteException -> 0x005c, all -> 0x0059 }
            int r5 = (r11 > r14 ? 1 : (r11 == r14 ? 0 : -1))
            if (r5 == 0) goto L_0x00a5
            j.c.a.a.g.a.n3 r5 = r17.a()     // Catch:{ SQLiteFullException -> 0x005e, SQLiteDatabaseLockedException -> 0x00b9, SQLiteException -> 0x005c, all -> 0x0059 }
            j.c.a.a.g.a.p3 r5 = r5.f2046f     // Catch:{ SQLiteFullException -> 0x005e, SQLiteDatabaseLockedException -> 0x00b9, SQLiteException -> 0x005c, all -> 0x0059 }
            java.lang.String r8 = "Different delete count than expected in local db. expected, received, difference"
            java.lang.Long r3 = java.lang.Long.valueOf(r14)     // Catch:{ SQLiteFullException -> 0x005e, SQLiteDatabaseLockedException -> 0x00b9, SQLiteException -> 0x005c, all -> 0x0059 }
            java.lang.Long r9 = java.lang.Long.valueOf(r11)     // Catch:{ SQLiteFullException -> 0x005e, SQLiteDatabaseLockedException -> 0x00b9, SQLiteException -> 0x005c, all -> 0x0059 }
            long r14 = r14 - r11
            java.lang.Long r11 = java.lang.Long.valueOf(r14)     // Catch:{ SQLiteFullException -> 0x005e, SQLiteDatabaseLockedException -> 0x00b9, SQLiteException -> 0x005c, all -> 0x0059 }
            r5.a(r8, r3, r9, r11)     // Catch:{ SQLiteFullException -> 0x005e, SQLiteDatabaseLockedException -> 0x00b9, SQLiteException -> 0x005c, all -> 0x0059 }
        L_0x00a5:
            r3 = 0
            r10.insertOrThrow(r0, r3, r4)     // Catch:{ SQLiteFullException -> 0x005e, SQLiteDatabaseLockedException -> 0x00b9, SQLiteException -> 0x005c, all -> 0x0059 }
            r10.setTransactionSuccessful()     // Catch:{ SQLiteFullException -> 0x005e, SQLiteDatabaseLockedException -> 0x00b9, SQLiteException -> 0x005c, all -> 0x0059 }
            r10.endTransaction()     // Catch:{ SQLiteFullException -> 0x005e, SQLiteDatabaseLockedException -> 0x00b9, SQLiteException -> 0x005c, all -> 0x0059 }
            if (r13 == 0) goto L_0x00b4
            r13.close()
        L_0x00b4:
            r10.close()
            r2 = 1
            return r2
        L_0x00b9:
            r8 = r13
            goto L_0x00f9
        L_0x00bb:
            r0 = move-exception
            r3 = r8
            goto L_0x012a
        L_0x00bf:
            r0 = move-exception
            r3 = r8
            r13 = r3
        L_0x00c2:
            r8 = r10
            goto L_0x00d1
        L_0x00c4:
            r3 = r8
            r8 = r3
            goto L_0x00f9
        L_0x00c7:
            r0 = move-exception
            r3 = r8
            goto L_0x010d
        L_0x00ca:
            r0 = move-exception
            r3 = r8
            r10 = r8
            goto L_0x012a
        L_0x00ce:
            r0 = move-exception
            r3 = r8
            r13 = r8
        L_0x00d1:
            if (r8 == 0) goto L_0x00dc
            boolean r3 = r8.inTransaction()     // Catch:{ all -> 0x00f3 }
            if (r3 == 0) goto L_0x00dc
            r8.endTransaction()     // Catch:{ all -> 0x00f3 }
        L_0x00dc:
            j.c.a.a.g.a.n3 r3 = r17.a()     // Catch:{ all -> 0x00f3 }
            j.c.a.a.g.a.p3 r3 = r3.f2046f     // Catch:{ all -> 0x00f3 }
            r3.a(r2, r0)     // Catch:{ all -> 0x00f3 }
            r3 = 1
            r1.d = r3     // Catch:{ all -> 0x00f3 }
            if (r13 == 0) goto L_0x00ed
            r13.close()
        L_0x00ed:
            if (r8 == 0) goto L_0x0123
            r8.close()
            goto L_0x0123
        L_0x00f3:
            r0 = move-exception
            r10 = r8
        L_0x00f5:
            r8 = r13
            goto L_0x012a
        L_0x00f7:
            r3 = r8
            r10 = r8
        L_0x00f9:
            long r11 = (long) r7
            android.os.SystemClock.sleep(r11)     // Catch:{ all -> 0x0129 }
            int r7 = r7 + 20
            if (r8 == 0) goto L_0x0104
            r8.close()
        L_0x0104:
            if (r10 == 0) goto L_0x0123
            r10.close()
            goto L_0x0123
        L_0x010a:
            r0 = move-exception
            r3 = r8
            r10 = r8
        L_0x010d:
            j.c.a.a.g.a.n3 r3 = r17.a()     // Catch:{ all -> 0x0129 }
            j.c.a.a.g.a.p3 r3 = r3.f2046f     // Catch:{ all -> 0x0129 }
            r3.a(r2, r0)     // Catch:{ all -> 0x0129 }
            r3 = 1
            r1.d = r3     // Catch:{ all -> 0x0129 }
            if (r8 == 0) goto L_0x011e
            r8.close()
        L_0x011e:
            if (r10 == 0) goto L_0x0123
            r10.close()
        L_0x0123:
            int r6 = r6 + 1
            r3 = 0
            r5 = 5
            goto L_0x0028
        L_0x0129:
            r0 = move-exception
        L_0x012a:
            if (r8 == 0) goto L_0x012f
            r8.close()
        L_0x012f:
            if (r10 == 0) goto L_0x0134
            r10.close()
        L_0x0134:
            throw r0
        L_0x0135:
            j.c.a.a.g.a.n3 r0 = r17.a()
            j.c.a.a.g.a.p3 r0 = r0.f2047i
            java.lang.String r2 = "Failed to write entry to local database"
            r0.a(r2)
            r2 = 0
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.j3.a(int, byte[]):boolean");
    }

    public final boolean y() {
        return false;
    }

    public final void z() {
        b();
        d();
        try {
            int delete = B().delete("messages", null, null) + 0;
            if (delete > 0) {
                a().f2052n.a("Reset local analytics data. records", Integer.valueOf(delete));
            }
        } catch (SQLiteException e2) {
            a().f2046f.a("Error resetting local analytics data. error", e2);
        }
    }

    public static long a(SQLiteDatabase sQLiteDatabase) {
        Cursor cursor = null;
        try {
            Cursor query = sQLiteDatabase.query("messages", new String[]{"rowid"}, "type=?", new String[]{"3"}, null, null, "rowid desc", "1");
            if (query.moveToFirst()) {
                long j2 = query.getLong(0);
                query.close();
                return j2;
            }
            query.close();
            return -1;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }
}
