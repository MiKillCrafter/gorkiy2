package j.c.a.a.g.a;

import android.os.RemoteException;
import android.text.TextUtils;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class o7 implements Runnable {
    public final /* synthetic */ boolean b;
    public final /* synthetic */ boolean c;
    public final /* synthetic */ g9 d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ d9 f2064e;

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ g9 f2065f;
    public final /* synthetic */ z6 g;

    public o7(z6 z6Var, boolean z, boolean z2, g9 g9Var, d9 d9Var, g9 g9Var2) {
        this.g = z6Var;
        this.b = z;
        this.c = z2;
        this.d = g9Var;
        this.f2064e = d9Var;
        this.f2065f = g9Var2;
    }

    public final void run() {
        z6 z6Var = this.g;
        f3 f3Var = z6Var.d;
        if (f3Var == null) {
            z6Var.a().f2046f.a("Discarding data. Failed to send conditional user property to service");
            return;
        }
        if (this.b) {
            z6Var.a(f3Var, this.c ? null : this.d, this.f2064e);
        } else {
            try {
                if (TextUtils.isEmpty(this.f2065f.b)) {
                    f3Var.a(this.d, this.f2064e);
                } else {
                    f3Var.a(this.d);
                }
            } catch (RemoteException e2) {
                this.g.a().f2046f.a("Failed to send conditional user property to the service", e2);
            }
        }
        this.g.D();
    }
}
