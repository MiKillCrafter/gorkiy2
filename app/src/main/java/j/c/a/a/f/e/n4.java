package j.c.a.a.f.e;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class n4 extends r2<String> implements m4, RandomAccess {
    public static final n4 d;
    public final List<Object> c;

    static {
        n4 n4Var = new n4(10);
        d = n4Var;
        super.b = false;
    }

    public n4(int i2) {
        this.c = new ArrayList(i2);
    }

    public final void a(w2 w2Var) {
        c();
        this.c.add(w2Var);
        this.modCount++;
    }

    public final /* synthetic */ void add(int i2, Object obj) {
        c();
        this.c.add(i2, (String) obj);
        this.modCount++;
    }

    public final boolean addAll(Collection<? extends String> collection) {
        return addAll(size(), collection);
    }

    public final Object b(int i2) {
        return this.c.get(i2);
    }

    public final void clear() {
        c();
        this.c.clear();
        this.modCount++;
    }

    public final /* synthetic */ Object get(int i2) {
        Object obj = this.c.get(i2);
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof w2) {
            w2 w2Var = (w2) obj;
            String b = w2Var.b();
            d3 d3Var = (d3) w2Var;
            int c2 = d3Var.c();
            if (p6.a(d3Var.f1843e, c2, d3Var.a() + c2)) {
                this.c.set(i2, b);
            }
            return b;
        }
        byte[] bArr = (byte[]) obj;
        String a = y3.a(bArr);
        boolean z = false;
        if (p6.a.a(0, bArr, 0, bArr.length) == 0) {
            z = true;
        }
        if (z) {
            this.c.set(i2, a);
        }
        return a;
    }

    public final m4 j() {
        return super.b ? new k6(this) : this;
    }

    public final /* synthetic */ Object remove(int i2) {
        c();
        Object remove = this.c.remove(i2);
        this.modCount++;
        return a(remove);
    }

    public final /* synthetic */ Object set(int i2, Object obj) {
        c();
        return a(this.c.set(i2, (String) obj));
    }

    public final int size() {
        return this.c.size();
    }

    public final boolean addAll(int i2, Collection<? extends String> collection) {
        c();
        if (collection instanceof m4) {
            collection = ((m4) collection).b();
        }
        boolean addAll = this.c.addAll(i2, collection);
        this.modCount++;
        return addAll;
    }

    public final List<?> b() {
        return Collections.unmodifiableList(this.c);
    }

    public n4(ArrayList<Object> arrayList) {
        this.c = arrayList;
    }

    public static String a(Object obj) {
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof w2) {
            return ((w2) obj).b();
        }
        return y3.a((byte[]) obj);
    }

    public final /* synthetic */ e4 a(int i2) {
        if (i2 >= size()) {
            ArrayList arrayList = new ArrayList(i2);
            arrayList.addAll(this.c);
            return new n4(arrayList);
        }
        throw new IllegalArgumentException();
    }
}
