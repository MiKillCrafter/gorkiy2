package j.c.a.a.g.a;

import j.c.a.a.c.n.b;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class p8 implements Runnable {
    public final /* synthetic */ v8 b;
    public final /* synthetic */ q8 c;

    public p8(q8 q8Var, v8 v8Var) {
        this.c = q8Var;
        this.b = v8Var;
    }

    public final void run() {
        q8 q8Var = this.c;
        q8Var.f2073i.i().d();
        n9 n9Var = new n9(q8Var);
        n9Var.p();
        q8Var.c = n9Var;
        q8Var.f2073i.g.c = q8Var.a;
        f9 f9Var = new f9(q8Var);
        f9Var.p();
        q8Var.f2072f = f9Var;
        u6 u6Var = new u6(q8Var);
        u6Var.p();
        q8Var.h = u6Var;
        n8 n8Var = new n8(q8Var);
        n8Var.p();
        q8Var.f2071e = n8Var;
        q8Var.d = new u3(q8Var);
        if (q8Var.f2079o != q8Var.f2080p) {
            q8Var.f2073i.a().f2046f.a("Not all upload components initialized", Integer.valueOf(q8Var.f2079o), Integer.valueOf(q8Var.f2080p));
        }
        q8Var.f2074j = true;
        q8 q8Var2 = this.c;
        q8Var2.f2073i.i().d();
        q8Var2.e().B();
        if (q8Var2.f2073i.l().f2111e.a() == 0) {
            b4 b4Var = q8Var2.f2073i.l().f2111e;
            if (((b) q8Var2.f2073i.f2092n) != null) {
                b4Var.a(System.currentTimeMillis());
            } else {
                throw null;
            }
        }
        q8Var2.u();
    }
}
