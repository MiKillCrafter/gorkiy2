package j.c.a.a.f.e;

import java.util.Arrays;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class s5<E> extends r2<E> {

    /* renamed from: e  reason: collision with root package name */
    public static final s5<Object> f1905e;
    public E[] c;
    public int d;

    static {
        s5<Object> s5Var = new s5<>(new Object[0], 0);
        f1905e = s5Var;
        super.b = false;
    }

    public s5(E[] eArr, int i2) {
        this.c = eArr;
        this.d = i2;
    }

    public final /* synthetic */ e4 a(int i2) {
        if (i2 >= this.d) {
            return new s5(Arrays.copyOf(this.c, i2), this.d);
        }
        throw new IllegalArgumentException();
    }

    public final boolean add(E e2) {
        c();
        int i2 = this.d;
        E[] eArr = this.c;
        if (i2 == eArr.length) {
            this.c = Arrays.copyOf(eArr, ((i2 * 3) / 2) + 1);
        }
        E[] eArr2 = this.c;
        int i3 = this.d;
        this.d = i3 + 1;
        eArr2[i3] = e2;
        this.modCount++;
        return true;
    }

    public final void b(int i2) {
        if (i2 < 0 || i2 >= this.d) {
            throw new IndexOutOfBoundsException(c(i2));
        }
    }

    public final String c(int i2) {
        int i3 = this.d;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i2);
        sb.append(", Size:");
        sb.append(i3);
        return sb.toString();
    }

    public final E get(int i2) {
        b(i2);
        return this.c[i2];
    }

    public final E remove(int i2) {
        c();
        b(i2);
        E[] eArr = this.c;
        E e2 = eArr[i2];
        int i3 = this.d;
        if (i2 < i3 - 1) {
            System.arraycopy(eArr, i2 + 1, eArr, i2, (i3 - i2) - 1);
        }
        this.d--;
        this.modCount++;
        return e2;
    }

    public final E set(int i2, E e2) {
        c();
        b(i2);
        E[] eArr = this.c;
        E e3 = eArr[i2];
        eArr[i2] = e2;
        this.modCount++;
        return e3;
    }

    public final int size() {
        return this.d;
    }

    public final void add(int i2, E e2) {
        int i3;
        c();
        if (i2 < 0 || i2 > (i3 = this.d)) {
            throw new IndexOutOfBoundsException(c(i2));
        }
        E[] eArr = this.c;
        if (i3 < eArr.length) {
            System.arraycopy(eArr, i2, eArr, i2 + 1, i3 - i2);
        } else {
            E[] eArr2 = new Object[(((i3 * 3) / 2) + 1)];
            System.arraycopy(eArr, 0, eArr2, 0, i2);
            System.arraycopy(this.c, i2, eArr2, i2 + 1, this.d - i2);
            this.c = eArr2;
        }
        this.c[i2] = e2;
        this.d++;
        this.modCount++;
    }
}
