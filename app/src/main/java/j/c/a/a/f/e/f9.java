package j.c.a.a.f.e;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class f9 implements c9 {
    public static final p1<Boolean> a;
    public static final p1<Boolean> b;
    public static final p1<Boolean> c;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.f.e.p1.a(j.c.a.a.f.e.v1, java.lang.String, boolean):j.c.a.a.f.e.p1
     arg types: [j.c.a.a.f.e.v1, java.lang.String, int]
     candidates:
      j.c.a.a.f.e.p1.a(j.c.a.a.f.e.v1, java.lang.String, long):j.c.a.a.f.e.p1
      j.c.a.a.f.e.p1.a(j.c.a.a.f.e.v1, java.lang.String, java.lang.String):j.c.a.a.f.e.p1
      j.c.a.a.f.e.p1.a(j.c.a.a.f.e.v1, java.lang.String, boolean):j.c.a.a.f.e.p1 */
    static {
        v1 v1Var = new v1(q1.a("com.google.android.gms.measurement"));
        a = p1.a(v1Var, "measurement.lifecycle.app_backgrounded_engagement", false);
        b = p1.a(v1Var, "measurement.lifecycle.app_backgrounded_tracking", false);
        c = p1.a(v1Var, "measurement.lifecycle.app_in_background_parameter", false);
    }

    public final boolean a() {
        return a.b().booleanValue();
    }

    public final boolean b() {
        return b.b().booleanValue();
    }

    public final boolean c() {
        return c.b().booleanValue();
    }
}
