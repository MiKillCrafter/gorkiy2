package j.c.a.a.g.a;

import android.content.Context;
import android.os.Bundle;
import i.b.k.ResourcesFlusher;
import j.c.a.a.f.e.nb;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class u5 {
    public final Context a;
    public String b;
    public String c;
    public String d;

    /* renamed from: e  reason: collision with root package name */
    public Boolean f2105e;

    /* renamed from: f  reason: collision with root package name */
    public long f2106f;
    public nb g;
    public boolean h = true;

    public u5(Context context, nb nbVar) {
        ResourcesFlusher.b(context);
        Context applicationContext = context.getApplicationContext();
        ResourcesFlusher.b(applicationContext);
        this.a = applicationContext;
        if (nbVar != null) {
            this.g = nbVar;
            this.b = nbVar.g;
            this.c = nbVar.f1883f;
            this.d = nbVar.f1882e;
            this.h = nbVar.d;
            this.f2106f = nbVar.c;
            Bundle bundle = nbVar.h;
            if (bundle != null) {
                this.f2105e = Boolean.valueOf(bundle.getBoolean("dataCollectionDefaultEnabled", true));
            }
        }
    }
}
