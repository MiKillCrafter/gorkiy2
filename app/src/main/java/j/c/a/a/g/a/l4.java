package j.c.a.a.g.a;

import i.b.k.ResourcesFlusher;
import java.lang.Thread;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicLong;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class l4 extends n5 {

    /* renamed from: l  reason: collision with root package name */
    public static final AtomicLong f2027l = new AtomicLong(Long.MIN_VALUE);
    public o4 c;
    public o4 d;

    /* renamed from: e  reason: collision with root package name */
    public final PriorityBlockingQueue<p4<?>> f2028e = new PriorityBlockingQueue<>();

    /* renamed from: f  reason: collision with root package name */
    public final BlockingQueue<p4<?>> f2029f = new LinkedBlockingQueue();
    public final Thread.UncaughtExceptionHandler g = new n4(this, "Thread death: Uncaught exception on worker thread");
    public final Thread.UncaughtExceptionHandler h = new n4(this, "Thread death: Uncaught exception on network thread");

    /* renamed from: i  reason: collision with root package name */
    public final Object f2030i = new Object();

    /* renamed from: j  reason: collision with root package name */
    public final Semaphore f2031j = new Semaphore(2);

    /* renamed from: k  reason: collision with root package name */
    public volatile boolean f2032k;

    public l4(r4 r4Var) {
        super(r4Var);
    }

    public final <V> Future<V> a(Callable callable) {
        o();
        ResourcesFlusher.b(callable);
        p4 p4Var = new p4(this, callable, false, "Task exception on worker thread");
        if (Thread.currentThread() == this.c) {
            if (!this.f2028e.isEmpty()) {
                a().f2047i.a("Callable skipped the worker queue.");
            }
            p4Var.run();
        } else {
            a((p4<?>) p4Var);
        }
        return p4Var;
    }

    public final void b(Runnable runnable) {
        o();
        ResourcesFlusher.b(runnable);
        p4 p4Var = new p4(this, runnable, "Task exception on network thread");
        synchronized (this.f2030i) {
            this.f2029f.add(p4Var);
            if (this.d == null) {
                o4 o4Var = new o4(this, "Measurement Network", this.f2029f);
                this.d = o4Var;
                o4Var.setUncaughtExceptionHandler(this.h);
                this.d.start();
            } else {
                this.d.a();
            }
        }
    }

    public final void c() {
        if (Thread.currentThread() != this.d) {
            throw new IllegalStateException("Call expected from network thread");
        }
    }

    public final void d() {
        if (Thread.currentThread() != this.c) {
            throw new IllegalStateException("Call expected from worker thread");
        }
    }

    public final boolean r() {
        return false;
    }

    public final boolean t() {
        return Thread.currentThread() == this.c;
    }

    public final void a(Runnable runnable) {
        o();
        ResourcesFlusher.b(runnable);
        a((p4<?>) new p4(this, runnable, "Task exception on worker thread"));
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(6:16|17|(1:19)(1:20)|21|22|23) */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0021, code lost:
        r6 = a().f2047i;
        r5 = java.lang.String.valueOf(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0031, code lost:
        if (r5.length() == 0) goto L_0x0038;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0033, code lost:
        r5 = "Timed out waiting for ".concat(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0038, code lost:
        r5 = new java.lang.String("Timed out waiting for ");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x003d, code lost:
        r6.a(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0040, code lost:
        return r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        r6 = a().f2047i;
        r5 = java.lang.String.valueOf(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0051, code lost:
        if (r5.length() != 0) goto L_0x0053;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0053, code lost:
        r5 = "Interrupted waiting for ".concat(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0058, code lost:
        r5 = new java.lang.String("Interrupted waiting for ");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x005d, code lost:
        r6.a(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0062, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001b, code lost:
        r4 = r4.get();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001f, code lost:
        if (r4 != null) goto L_0x0040;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:16:0x0041 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final <T> java.lang.Object a(java.util.concurrent.atomic.AtomicReference r4, java.lang.String r5, java.lang.Runnable r6) {
        /*
            r3 = this;
            monitor-enter(r4)
            j.c.a.a.g.a.l4 r0 = r3.i()     // Catch:{ all -> 0x0063 }
            r0.o()     // Catch:{ all -> 0x0063 }
            i.b.k.ResourcesFlusher.b(r6)     // Catch:{ all -> 0x0063 }
            j.c.a.a.g.a.p4 r1 = new j.c.a.a.g.a.p4     // Catch:{ all -> 0x0063 }
            java.lang.String r2 = "Task exception on worker thread"
            r1.<init>(r0, r6, r2)     // Catch:{ all -> 0x0063 }
            r0.a(r1)     // Catch:{ all -> 0x0063 }
            r0 = 15000(0x3a98, double:7.411E-320)
            r4.wait(r0)     // Catch:{ InterruptedException -> 0x0041 }
            monitor-exit(r4)     // Catch:{ all -> 0x0063 }
            java.lang.Object r4 = r4.get()
            if (r4 != 0) goto L_0x0040
            j.c.a.a.g.a.n3 r6 = r3.a()
            j.c.a.a.g.a.p3 r6 = r6.f2047i
            java.lang.String r0 = "Timed out waiting for "
            java.lang.String r5 = java.lang.String.valueOf(r5)
            int r1 = r5.length()
            if (r1 == 0) goto L_0x0038
            java.lang.String r5 = r0.concat(r5)
            goto L_0x003d
        L_0x0038:
            java.lang.String r5 = new java.lang.String
            r5.<init>(r0)
        L_0x003d:
            r6.a(r5)
        L_0x0040:
            return r4
        L_0x0041:
            j.c.a.a.g.a.n3 r6 = r3.a()     // Catch:{ all -> 0x0063 }
            j.c.a.a.g.a.p3 r6 = r6.f2047i     // Catch:{ all -> 0x0063 }
            java.lang.String r0 = "Interrupted waiting for "
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ all -> 0x0063 }
            int r1 = r5.length()     // Catch:{ all -> 0x0063 }
            if (r1 == 0) goto L_0x0058
            java.lang.String r5 = r0.concat(r5)     // Catch:{ all -> 0x0063 }
            goto L_0x005d
        L_0x0058:
            java.lang.String r5 = new java.lang.String     // Catch:{ all -> 0x0063 }
            r5.<init>(r0)     // Catch:{ all -> 0x0063 }
        L_0x005d:
            r6.a(r5)     // Catch:{ all -> 0x0063 }
            r5 = 0
            monitor-exit(r4)     // Catch:{ all -> 0x0063 }
            return r5
        L_0x0063:
            r5 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0063 }
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.l4.a(java.util.concurrent.atomic.AtomicReference, java.lang.String, java.lang.Runnable):java.lang.Object");
    }

    public final void a(p4<?> p4Var) {
        synchronized (this.f2030i) {
            this.f2028e.add(p4Var);
            if (this.c == null) {
                o4 o4Var = new o4(this, "Measurement Worker", this.f2028e);
                this.c = o4Var;
                o4Var.setUncaughtExceptionHandler(this.g);
                this.c.start();
            } else {
                this.c.a();
            }
        }
    }
}
