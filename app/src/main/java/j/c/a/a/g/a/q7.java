package j.c.a.a.g.a;

import android.os.Bundle;
import android.os.RemoteException;
import j.c.a.a.f.e.fb;
import java.util.ArrayList;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class q7 implements Runnable {
    public final /* synthetic */ String b;
    public final /* synthetic */ String c;
    public final /* synthetic */ d9 d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ fb f2069e;

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ z6 f2070f;

    public q7(z6 z6Var, String str, String str2, d9 d9Var, fb fbVar) {
        this.f2070f = z6Var;
        this.b = str;
        this.c = str2;
        this.d = d9Var;
        this.f2069e = fbVar;
    }

    public final void run() {
        ArrayList<Bundle> arrayList = new ArrayList<>();
        try {
            f3 f3Var = this.f2070f.d;
            if (f3Var == null) {
                this.f2070f.a().f2046f.a("Failed to get conditional properties", this.b, this.c);
                return;
            }
            arrayList = y8.b(f3Var.a(this.b, this.c, this.d));
            this.f2070f.D();
            this.f2070f.k().a(this.f2069e, arrayList);
        } catch (RemoteException e2) {
            this.f2070f.a().f2046f.a("Failed to get conditional properties", this.b, this.c, e2);
        } finally {
            this.f2070f.k().a(this.f2069e, arrayList);
        }
    }
}
