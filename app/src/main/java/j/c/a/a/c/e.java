package j.c.a.a.c;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Icon;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import androidx.core.graphics.drawable.IconCompat;
import com.google.android.gms.common.GooglePlayServicesUtil;
import i.b.k.ResourcesFlusher;
import i.h.d.NotificationCompat;
import i.h.d.NotificationCompat0;
import i.h.d.NotificationCompat1;
import i.h.d.NotificationCompat2;
import i.h.d.NotificationCompatJellybean;
import i.h.d.RemoteInput;
import i.l.a.BackStackRecord;
import i.l.a.FragmentActivity;
import i.l.a.FragmentManager;
import i.l.a.FragmentManagerImpl;
import j.c.a.a.c.l.d;
import j.c.a.a.c.l.q;
import j.c.a.a.c.n.c;
import j.c.a.a.f.b.b;
import j.d.a.TedPermission;
import java.util.ArrayList;
import java.util.Iterator;

public class e extends f {
    public static final Object c = new Object();
    public static final e d = new e();

    @SuppressLint({"HandlerLeak"})
    public class a extends b {
        public final Context a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Context context) {
            super(Looper.myLooper() == null ? Looper.getMainLooper() : Looper.myLooper());
            this.a = context.getApplicationContext();
        }

        public final void handleMessage(Message message) {
            int i2 = message.what;
            if (i2 != 1) {
                StringBuilder sb = new StringBuilder(50);
                sb.append("Don't know how to handle this message: ");
                sb.append(i2);
                Log.w("GoogleApiAvailability", sb.toString());
                return;
            }
            int a2 = e.this.a(this.a);
            if (e.this == null) {
                throw null;
            } else if (g.isUserRecoverableError(a2)) {
                e.this.b(this.a, a2);
            }
        }
    }

    public int a(Context context) {
        return a(context, f.a);
    }

    public boolean b(Activity activity, int i2, int i3, DialogInterface.OnCancelListener onCancelListener) {
        Dialog a2 = a(activity, i2, new q(super.a(activity, i2, TedPermission.a), activity, i3), onCancelListener);
        if (a2 == null) {
            return false;
        }
        a(activity, a2, GooglePlayServicesUtil.GMS_ERROR_DIALOG, onCancelListener);
        return true;
    }

    public PendingIntent a(Context context, int i2, int i3) {
        Intent a2 = a(context, i2, (String) null);
        if (a2 == null) {
            return null;
        }
        return PendingIntent.getActivity(context, i3, a2, 134217728);
    }

    public final String a() {
        synchronized (c) {
        }
        return null;
    }

    public void b(Context context, int i2) {
        PendingIntent pendingIntent;
        Intent a2 = super.a(context, i2, "n");
        if (a2 == null) {
            pendingIntent = null;
        } else {
            pendingIntent = PendingIntent.getActivity(context, 0, a2, 134217728);
        }
        a(context, i2, pendingIntent);
    }

    public int a(Context context, int i2) {
        return super.a(context, i2);
    }

    public Dialog a(Activity activity, int i2, int i3, DialogInterface.OnCancelListener onCancelListener) {
        return a(activity, i2, new q(super.a(activity, i2, TedPermission.a), activity, i3), onCancelListener);
    }

    public Intent a(Context context, int i2, String str) {
        return super.a(context, i2, str);
    }

    public static Dialog a(Context context, int i2, j.c.a.a.c.l.e eVar, DialogInterface.OnCancelListener onCancelListener) {
        String str;
        AlertDialog.Builder builder = null;
        if (i2 == 0) {
            return null;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(16843529, typedValue, true);
        if ("Theme.Dialog.Alert".equals(context.getResources().getResourceEntryName(typedValue.resourceId))) {
            builder = new AlertDialog.Builder(context, 5);
        }
        if (builder == null) {
            builder = new AlertDialog.Builder(context);
        }
        builder.setMessage(d.a(context, i2));
        if (onCancelListener != null) {
            builder.setOnCancelListener(onCancelListener);
        }
        Resources resources = context.getResources();
        if (i2 == 1) {
            str = resources.getString(j.c.a.a.b.b.common_google_play_services_install_button);
        } else if (i2 == 2) {
            str = resources.getString(j.c.a.a.b.b.common_google_play_services_update_button);
        } else if (i2 != 3) {
            str = resources.getString(17039370);
        } else {
            str = resources.getString(j.c.a.a.b.b.common_google_play_services_enable_button);
        }
        if (str != null) {
            builder.setPositiveButton(str, eVar);
        }
        String b = d.b(context, i2);
        if (b != null) {
            builder.setTitle(b);
        }
        return builder.create();
    }

    public static void a(Activity activity, Dialog dialog, String str, DialogInterface.OnCancelListener onCancelListener) {
        if (activity instanceof FragmentActivity) {
            FragmentManager j2 = ((FragmentActivity) activity).j();
            j jVar = new j();
            ResourcesFlusher.b(dialog, "Cannot display null dialog");
            dialog.setOnCancelListener(null);
            dialog.setOnDismissListener(null);
            jVar.i0 = dialog;
            if (onCancelListener != null) {
                jVar.j0 = onCancelListener;
            }
            jVar.g0 = false;
            jVar.h0 = true;
            FragmentManagerImpl fragmentManagerImpl = (FragmentManagerImpl) j2;
            if (fragmentManagerImpl != null) {
                BackStackRecord backStackRecord = new BackStackRecord(fragmentManagerImpl);
                backStackRecord.a(0, jVar, str, 1);
                backStackRecord.b();
                return;
            }
            throw null;
        }
        android.app.FragmentManager fragmentManager = activity.getFragmentManager();
        c cVar = new c();
        ResourcesFlusher.b(dialog, "Cannot display null dialog");
        dialog.setOnCancelListener(null);
        dialog.setOnDismissListener(null);
        cVar.b = dialog;
        if (onCancelListener != null) {
            cVar.c = onCancelListener;
        }
        cVar.show(fragmentManager, str);
    }

    @TargetApi(20)
    public final void a(Context context, int i2, PendingIntent pendingIntent) {
        String str;
        String str2;
        Notification.Builder builder;
        Notification notification;
        int i3;
        Notification.Action.Builder builder2;
        Bundle bundle;
        Icon icon;
        int i4;
        Context context2 = context;
        int i5 = i2;
        PendingIntent pendingIntent2 = pendingIntent;
        if (i5 == 18) {
            new a(context2).sendEmptyMessageDelayed(1, 120000);
        } else if (pendingIntent2 != null) {
            if (i5 == 6) {
                str = d.a(context2, "common_google_play_services_resolution_required_title");
            } else {
                str = d.b(context, i2);
            }
            if (str == null) {
                str = context.getResources().getString(j.c.a.a.b.b.common_google_play_services_notification_ticker);
            }
            if (i5 == 6) {
                str2 = d.a(context2, "common_google_play_services_resolution_required_text", d.a(context));
            } else {
                str2 = d.a(context, i2);
            }
            Resources resources = context.getResources();
            NotificationManager notificationManager = (NotificationManager) context2.getSystemService("notification");
            NotificationCompat0 notificationCompat0 = new NotificationCompat0(context2);
            notificationCompat0.f1165j = true;
            notificationCompat0.f1173r.flags |= 16;
            notificationCompat0.d = NotificationCompat0.a(str);
            NotificationCompat2 notificationCompat2 = new NotificationCompat2();
            notificationCompat2.c = NotificationCompat0.a(str2);
            if (notificationCompat0.f1164i != notificationCompat2) {
                notificationCompat0.f1164i = notificationCompat2;
                if (notificationCompat2.a != notificationCompat0) {
                    notificationCompat2.a = notificationCompat0;
                    notificationCompat0.a(notificationCompat2);
                }
            }
            if (c.c(context)) {
                ResourcesFlusher.b(true);
                notificationCompat0.f1173r.icon = context.getApplicationInfo().icon;
                notificationCompat0.g = 2;
                if (c.d(context)) {
                    notificationCompat0.b.add(new NotificationCompat(j.c.a.a.b.a.common_full_open_on_phone, resources.getString(j.c.a.a.b.b.common_open_on_phone), pendingIntent2));
                } else {
                    notificationCompat0.f1163f = pendingIntent2;
                }
            } else {
                notificationCompat0.f1173r.icon = 17301642;
                notificationCompat0.f1173r.tickerText = NotificationCompat0.a(resources.getString(j.c.a.a.b.b.common_google_play_services_notification_ticker));
                notificationCompat0.f1173r.when = System.currentTimeMillis();
                notificationCompat0.f1163f = pendingIntent2;
                notificationCompat0.f1162e = NotificationCompat0.a(str2);
            }
            if (c.e()) {
                ResourcesFlusher.b(c.e());
                a();
                NotificationChannel notificationChannel = notificationManager.getNotificationChannel("com.google.android.gms.availability");
                String b = d.b(context);
                if (notificationChannel == null) {
                    notificationManager.createNotificationChannel(new NotificationChannel("com.google.android.gms.availability", b, 4));
                } else if (!b.contentEquals(notificationChannel.getName())) {
                    notificationChannel.setName(b);
                    notificationManager.createNotificationChannel(notificationChannel);
                }
                notificationCompat0.f1169n = "com.google.android.gms.availability";
            }
            new ArrayList();
            Bundle bundle2 = new Bundle();
            if (Build.VERSION.SDK_INT >= 26) {
                builder = new Notification.Builder(notificationCompat0.a, notificationCompat0.f1169n);
            } else {
                builder = new Notification.Builder(notificationCompat0.a);
            }
            Notification notification2 = notificationCompat0.f1173r;
            builder.setWhen(notification2.when).setSmallIcon(notification2.icon, notification2.iconLevel).setContent(notification2.contentView).setTicker(notification2.tickerText, null).setVibrate(notification2.vibrate).setLights(notification2.ledARGB, notification2.ledOnMS, notification2.ledOffMS).setOngoing((notification2.flags & 2) != 0).setOnlyAlertOnce((notification2.flags & 8) != 0).setAutoCancel((notification2.flags & 16) != 0).setDefaults(notification2.defaults).setContentTitle(notificationCompat0.d).setContentText(notificationCompat0.f1162e).setContentInfo(null).setContentIntent(notificationCompat0.f1163f).setDeleteIntent(notification2.deleteIntent).setFullScreenIntent(null, (notification2.flags & 128) != 0).setLargeIcon((Bitmap) null).setNumber(0).setProgress(0, 0, false);
            builder.setSubText(null).setUsesChronometer(false).setPriority(notificationCompat0.g);
            Iterator<i.h.d.e> it = notificationCompat0.b.iterator();
            while (it.hasNext()) {
                NotificationCompat next = it.next();
                if (Build.VERSION.SDK_INT >= 23) {
                    if (next.b == null && (i4 = next.f1159i) != 0) {
                        next.b = IconCompat.a(null, "", i4);
                    }
                    IconCompat iconCompat = next.b;
                    if (iconCompat == null) {
                        icon = null;
                    } else {
                        icon = iconCompat.c();
                    }
                    builder2 = new Notification.Action.Builder(icon, next.f1160j, next.f1161k);
                } else {
                    builder2 = new Notification.Action.Builder(next.f1159i, next.f1160j, next.f1161k);
                }
                RemoteInput[] remoteInputArr = next.c;
                if (remoteInputArr != null) {
                    int length = remoteInputArr.length;
                    android.app.RemoteInput[] remoteInputArr2 = new android.app.RemoteInput[length];
                    if (remoteInputArr.length <= 0) {
                        for (int i6 = 0; i6 < length; i6++) {
                            builder2.addRemoteInput(remoteInputArr2[i6]);
                        }
                    } else {
                        RemoteInput remoteInput = remoteInputArr[0];
                        throw null;
                    }
                }
                if (next.a != null) {
                    bundle = new Bundle(next.a);
                } else {
                    bundle = new Bundle();
                }
                bundle.putBoolean("android.support.allowGeneratedReplies", next.f1157e);
                if (Build.VERSION.SDK_INT >= 24) {
                    builder2.setAllowGeneratedReplies(next.f1157e);
                }
                bundle.putInt("android.support.action.semanticAction", next.g);
                if (Build.VERSION.SDK_INT >= 28) {
                    builder2.setSemanticAction(next.g);
                }
                if (Build.VERSION.SDK_INT >= 29) {
                    builder2.setContextual(next.h);
                }
                bundle.putBoolean("android.support.action.showsUserInterface", next.f1158f);
                builder2.addExtras(bundle);
                builder.addAction(builder2.build());
            }
            Bundle bundle3 = notificationCompat0.f1166k;
            if (bundle3 != null) {
                bundle2.putAll(bundle3);
            }
            builder.setShowWhen(notificationCompat0.h);
            builder.setLocalOnly(notificationCompat0.f1165j).setGroup(null).setGroupSummary(false).setSortKey(null);
            int i7 = notificationCompat0.f1171p;
            builder.setCategory(null).setColor(notificationCompat0.f1167l).setVisibility(notificationCompat0.f1168m).setPublicVersion(null).setSound(notification2.sound, notification2.audioAttributes);
            Iterator<String> it2 = notificationCompat0.f1174s.iterator();
            while (it2.hasNext()) {
                builder.addPerson(it2.next());
            }
            if (notificationCompat0.c.size() > 0) {
                if (notificationCompat0.f1166k == null) {
                    notificationCompat0.f1166k = new Bundle();
                }
                Bundle bundle4 = notificationCompat0.f1166k.getBundle("android.car.EXTENSIONS");
                if (bundle4 == null) {
                    bundle4 = new Bundle();
                }
                Bundle bundle5 = new Bundle();
                for (int i8 = 0; i8 < notificationCompat0.c.size(); i8++) {
                    bundle5.putBundle(Integer.toString(i8), NotificationCompatJellybean.a(notificationCompat0.c.get(i8)));
                }
                bundle4.putBundle("invisible_actions", bundle5);
                if (notificationCompat0.f1166k == null) {
                    notificationCompat0.f1166k = new Bundle();
                }
                notificationCompat0.f1166k.putBundle("android.car.EXTENSIONS", bundle4);
                bundle2.putBundle("android.car.EXTENSIONS", bundle4);
            }
            if (Build.VERSION.SDK_INT >= 24) {
                builder.setExtras(notificationCompat0.f1166k).setRemoteInputHistory(null);
            }
            if (Build.VERSION.SDK_INT >= 26) {
                builder.setBadgeIconType(notificationCompat0.f1170o).setShortcutId(null).setTimeoutAfter(0).setGroupAlertBehavior(notificationCompat0.f1171p);
                if (!TextUtils.isEmpty(notificationCompat0.f1169n)) {
                    builder.setSound(null).setDefaults(0).setLights(0, 0, 0).setVibrate(null);
                }
            }
            if (Build.VERSION.SDK_INT >= 29) {
                builder.setAllowSystemGeneratedContextualActions(notificationCompat0.f1172q);
                builder.setBubbleMetadata(null);
            }
            NotificationCompat1 notificationCompat1 = notificationCompat0.f1164i;
            if (notificationCompat1 != null) {
                NotificationCompat2 notificationCompat22 = (NotificationCompat2) notificationCompat1;
                Notification.BigTextStyle bigText = new Notification.BigTextStyle(builder).setBigContentTitle(null).bigText(notificationCompat22.c);
                if (notificationCompat22.b) {
                    bigText.setSummaryText(null);
                }
            }
            int i9 = Build.VERSION.SDK_INT;
            if (i9 >= 26) {
                notification = builder.build();
            } else if (i9 >= 24) {
                notification = builder.build();
                if (i7 != 0) {
                    if (!(notification.getGroup() == null || (notification.flags & 512) == 0 || i7 != 2)) {
                        notification.sound = null;
                        notification.vibrate = null;
                        int i10 = notification.defaults & -2;
                        notification.defaults = i10;
                        notification.defaults = i10 & -3;
                    }
                    if (notification.getGroup() != null && (notification.flags & 512) == 0 && i7 == 1) {
                        notification.sound = null;
                        notification.vibrate = null;
                        int i11 = notification.defaults & -2;
                        notification.defaults = i11;
                        notification.defaults = i11 & -3;
                    }
                }
            } else {
                builder.setExtras(bundle2);
                notification = builder.build();
                if (i7 != 0) {
                    if (!(notification.getGroup() == null || (notification.flags & 512) == 0 || i7 != 2)) {
                        notification.sound = null;
                        notification.vibrate = null;
                        int i12 = notification.defaults & -2;
                        notification.defaults = i12;
                        notification.defaults = i12 & -3;
                    }
                    if (notification.getGroup() != null && (notification.flags & 512) == 0 && i7 == 1) {
                        notification.sound = null;
                        notification.vibrate = null;
                        int i13 = notification.defaults & -2;
                        notification.defaults = i13;
                        notification.defaults = i13 & -3;
                    }
                }
            }
            if (notificationCompat1 == null || notificationCompat0.f1164i != null) {
                if (notificationCompat1 != null) {
                    Bundle bundle6 = notification.extras;
                }
                if (i5 == 1 || i5 == 2 || i5 == 3) {
                    i3 = g.GMS_AVAILABILITY_NOTIFICATION_ID;
                    g.sCanceledAvailabilityNotification.set(false);
                } else {
                    i3 = g.GMS_GENERAL_ERROR_NOTIFICATION_ID;
                }
                notificationManager.notify(i3, notification);
                return;
            }
            throw null;
        } else if (i5 == 6) {
            Log.w("GoogleApiAvailability", "Missing resolution for ConnectionResult.RESOLUTION_REQUIRED. Call GoogleApiAvailability#showErrorNotification(Context, ConnectionResult) instead.");
        }
    }
}
