package j.c.a.a.g.a;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import i.b.k.ResourcesFlusher;
import j.c.a.a.c.o.b;
import j.c.a.a.f.e.k8;
import java.util.ArrayList;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class c5 implements Runnable {
    public final /* synthetic */ d9 b;
    public final /* synthetic */ s4 c;

    public c5(s4 s4Var, d9 d9Var) {
        this.c = s4Var;
        this.b = d9Var;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.g.a.d9.<init>(java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, java.lang.String, boolean, boolean, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>):void
     arg types: [java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, int, long, ?[OBJECT, ARRAY], boolean, int, java.lang.String, int, long, int, boolean, boolean, int, java.lang.String, ?[OBJECT, ARRAY], int, ?[OBJECT, ARRAY]]
     candidates:
      j.c.a.a.g.a.d9.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, long, long, java.lang.String, boolean, boolean, long, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>):void
      j.c.a.a.g.a.d9.<init>(java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, java.lang.String, boolean, boolean, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>):void */
    public final void run() {
        d9 d9Var;
        String str;
        String str2;
        String str3;
        int i2;
        this.c.a.o();
        q8 q8Var = this.c.a;
        d9 d9Var2 = this.b;
        if (q8Var.v != null) {
            ArrayList arrayList = new ArrayList();
            q8Var.w = arrayList;
            arrayList.addAll(q8Var.v);
        }
        n9 e2 = q8Var.e();
        String str4 = d9Var2.b;
        ResourcesFlusher.b(str4);
        e2.d();
        e2.o();
        try {
            SQLiteDatabase v = e2.v();
            String[] strArr = {str4};
            int delete = v.delete("apps", "app_id=?", strArr) + 0 + v.delete("events", "app_id=?", strArr) + v.delete("user_attributes", "app_id=?", strArr) + v.delete("conditional_properties", "app_id=?", strArr) + v.delete("raw_events", "app_id=?", strArr) + v.delete("raw_events_metadata", "app_id=?", strArr) + v.delete("queue", "app_id=?", strArr) + v.delete("audience_filter_values", "app_id=?", strArr) + v.delete("main_event_params", "app_id=?", strArr);
            if (delete > 0) {
                e2.a().f2052n.a("Reset analytics data. app, records", str4, Integer.valueOf(delete));
            }
        } catch (SQLiteException e3) {
            e2.a().f2046f.a("Error resetting analytics data. appId, error", n3.a(str4), e3);
        }
        k8.c.a();
        if (!q8Var.f2073i.g.a(k.M0)) {
            Context context = q8Var.f2073i.a;
            String str5 = d9Var2.b;
            String str6 = d9Var2.c;
            boolean z = d9Var2.f1953i;
            boolean z2 = d9Var2.f1960p;
            boolean z3 = d9Var2.f1961q;
            long j2 = d9Var2.f1958n;
            String str7 = d9Var2.f1963s;
            PackageManager packageManager = context.getPackageManager();
            if (packageManager == null) {
                q8Var.f2073i.a().f2046f.a("PackageManager is null, can not log app install information");
                d9Var = null;
            } else {
                try {
                    str = packageManager.getInstallerPackageName(str5);
                } catch (IllegalArgumentException unused) {
                    q8Var.f2073i.a().f2046f.a("Error retrieving installer package name. appId", n3.a(str5));
                    str = "Unknown";
                }
                if (str == null) {
                    str2 = "manual_install";
                } else {
                    str2 = "com.android.vending".equals(str) ? "" : str;
                }
                try {
                    PackageInfo packageInfo = b.b(context).a.getPackageManager().getPackageInfo(str5, 0);
                    if (packageInfo != null) {
                        CharSequence a = b.b(context).a(str5);
                        if (!TextUtils.isEmpty(a)) {
                            String charSequence = a.toString();
                        }
                        String str8 = packageInfo.versionName;
                        i2 = packageInfo.versionCode;
                        str3 = str8;
                    } else {
                        i2 = RecyclerView.UNDEFINED_DURATION;
                        str3 = "Unknown";
                    }
                    r4 r4Var = q8Var.f2073i;
                    h9 h9Var = r4Var.f2086f;
                    i9 i9Var = r4Var.g;
                    if (i9Var != null) {
                        long j3 = i9Var.d(str5, k.U) ? j2 : 0;
                        q8Var.f2073i.g.o();
                        d9Var = new d9(str5, str6, str3, (long) i2, str2, 18079L, q8Var.f2073i.n().a(context, str5), (String) null, z, false, "", 0L, j3, 0, z2, z3, false, str7, (Boolean) null, 0L, (List<String>) null);
                    } else {
                        throw null;
                    }
                } catch (PackageManager.NameNotFoundException unused2) {
                    q8Var.f2073i.a().f2046f.a("Error retrieving newly installed package info. appId, appName", n3.a(str5), "Unknown");
                    d9Var = null;
                }
            }
            if (d9Var2.f1953i) {
                q8Var.a(d9Var);
            }
        } else if (d9Var2.f1953i) {
            q8Var.a(d9Var2);
        }
    }
}
