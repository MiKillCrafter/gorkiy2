package j.c.a.a.g.a;

import android.content.ComponentName;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class t7 implements Runnable {
    public final /* synthetic */ ComponentName b;
    public final /* synthetic */ r7 c;

    public t7(r7 r7Var, ComponentName componentName) {
        this.c = r7Var;
        this.b = componentName;
    }

    public final void run() {
        z6.a(this.c.c, this.b);
    }
}
