package j.c.a.a.g.a;

import android.content.Context;
import i.b.k.ResourcesFlusher;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class v8 {
    public final Context a;

    public v8(Context context) {
        ResourcesFlusher.b(context);
        Context applicationContext = context.getApplicationContext();
        ResourcesFlusher.b(applicationContext);
        this.a = applicationContext;
    }
}
