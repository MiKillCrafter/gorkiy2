package j.c.a.a.c.k.e;

import android.app.ActivityManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiActivity;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.UnsupportedApiCallException;
import i.b.k.ResourcesFlusher;
import i.e.ArrayMap;
import i.e.ArraySet;
import j.a.a.a.outline;
import j.c.a.a.c.d;
import j.c.a.a.c.e;
import j.c.a.a.c.g;
import j.c.a.a.c.k.a;
import j.c.a.a.c.l.b;
import j.c.a.a.c.l.i;
import j.c.a.a.c.l.j;
import j.c.a.a.c.l.n;
import j.c.a.a.i.f;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import javax.annotation.concurrent.GuardedBy;

public class b implements Handler.Callback {

    /* renamed from: m  reason: collision with root package name */
    public static final Status f1779m = new Status(4, "Sign-out occurred while this API call was in progress.");

    /* renamed from: n  reason: collision with root package name */
    public static final Status f1780n = new Status(4, "The user must be signed in to make this API call.");

    /* renamed from: o  reason: collision with root package name */
    public static final Object f1781o = new Object();
    @GuardedBy("lock")

    /* renamed from: p  reason: collision with root package name */
    public static b f1782p;
    public long a = 5000;
    public long b = 120000;
    public long c = 10000;
    public final Context d;

    /* renamed from: e  reason: collision with root package name */
    public final e f1783e;

    /* renamed from: f  reason: collision with root package name */
    public final i f1784f;
    public final AtomicInteger g;
    public final Map<z<?>, a<?>> h;
    @GuardedBy("lock")

    /* renamed from: i  reason: collision with root package name */
    public h f1785i;
    @GuardedBy("lock")

    /* renamed from: j  reason: collision with root package name */
    public final Set<z<?>> f1786j;

    /* renamed from: k  reason: collision with root package name */
    public final Set<z<?>> f1787k;

    /* renamed from: l  reason: collision with root package name */
    public final Handler f1788l;

    /* renamed from: j.c.a.a.c.k.e.b$b  reason: collision with other inner class name */
    public static class C0022b {
        public final z<?> a;
        public final d b;

        public final boolean equals(Object obj) {
            if (obj != null && (obj instanceof C0022b)) {
                C0022b bVar = (C0022b) obj;
                if (!ResourcesFlusher.c(this.a, bVar.a) || !ResourcesFlusher.c(this.b, bVar.b)) {
                    return false;
                }
                return true;
            }
            return false;
        }

        public final int hashCode() {
            return Arrays.hashCode(new Object[]{this.a, this.b});
        }

        public final String toString() {
            n d = ResourcesFlusher.d(this);
            d.a("key", this.a);
            d.a("feature", this.b);
            return d.toString();
        }
    }

    public class c implements v, b.c {
        public final a.e a;
        public final z<?> b;
        public j c = null;
        public Set<Scope> d = null;

        /* renamed from: e  reason: collision with root package name */
        public boolean f1795e = false;

        public c(a.e eVar, z<?> zVar) {
            this.a = eVar;
            this.b = zVar;
        }

        public final void a(j.c.a.a.c.b bVar) {
            b.this.f1788l.post(new p(this, bVar));
        }

        public final void b(j.c.a.a.c.b bVar) {
            a aVar = b.this.h.get(this.b);
            ResourcesFlusher.a(aVar.f1794l.f1788l);
            aVar.b.e();
            aVar.a(bVar);
        }
    }

    public b(Context context, Looper looper, e eVar) {
        new AtomicInteger(1);
        this.g = new AtomicInteger(0);
        this.h = new ConcurrentHashMap(5, 0.75f, 1);
        this.f1785i = null;
        this.f1786j = new ArraySet();
        this.f1787k = new ArraySet();
        this.d = context;
        this.f1788l = new j.c.a.a.f.b.b(looper, this);
        this.f1783e = eVar;
        this.f1784f = new i(eVar);
        Handler handler = this.f1788l;
        handler.sendMessage(handler.obtainMessage(6));
    }

    public static b a(Context context) {
        b bVar;
        synchronized (f1781o) {
            if (f1782p == null) {
                HandlerThread handlerThread = new HandlerThread("GoogleApiHandler", 9);
                handlerThread.start();
                f1782p = new b(context.getApplicationContext(), handlerThread.getLooper(), e.d);
            }
            bVar = f1782p;
        }
        return bVar;
    }

    public boolean handleMessage(Message message) {
        a aVar;
        Status status;
        int i2 = message.what;
        long j2 = 300000;
        int i3 = 0;
        switch (i2) {
            case 1:
                if (((Boolean) message.obj).booleanValue()) {
                    j2 = 10000;
                }
                this.c = j2;
                this.f1788l.removeMessages(12);
                for (z<?> obtainMessage : this.h.keySet()) {
                    Handler handler = this.f1788l;
                    handler.sendMessageDelayed(handler.obtainMessage(12, obtainMessage), this.c);
                }
                break;
            case 2:
                if (((a0) message.obj) != null) {
                    throw null;
                }
                throw null;
            case 3:
                for (a next : this.h.values()) {
                    next.g();
                    next.a();
                }
                break;
            case 4:
            case 8:
            case 13:
                q qVar = (q) message.obj;
                Map<z<?>, a<?>> map = this.h;
                if (qVar.c != null) {
                    a aVar2 = map.get(null);
                    if (aVar2 == null) {
                        a(qVar.c);
                        Map<z<?>, a<?>> map2 = this.h;
                        if (qVar.c != null) {
                            aVar2 = map2.get(null);
                        } else {
                            throw null;
                        }
                    }
                    if (aVar2.b() && this.g.get() != qVar.b) {
                        qVar.a.a(f1779m);
                        aVar2.f();
                        break;
                    } else {
                        aVar2.a(qVar.a);
                        break;
                    }
                } else {
                    throw null;
                }
            case 5:
                int i4 = message.arg1;
                j.c.a.a.c.b bVar = (j.c.a.a.c.b) message.obj;
                Iterator<a<?>> it = this.h.values().iterator();
                while (true) {
                    if (it.hasNext()) {
                        aVar = it.next();
                        if (aVar.g == i4) {
                        }
                    } else {
                        aVar = null;
                    }
                }
                if (aVar == null) {
                    StringBuilder sb = new StringBuilder(76);
                    sb.append("Could not find API instance ");
                    sb.append(i4);
                    sb.append(" while trying to fail enqueued calls.");
                    Log.wtf("GoogleApiManager", sb.toString(), new Exception());
                    break;
                } else {
                    e eVar = this.f1783e;
                    int i5 = bVar.c;
                    if (eVar != null) {
                        String errorString = g.getErrorString(i5);
                        String str = bVar.f1775e;
                        StringBuilder sb2 = new StringBuilder(outline.a(str, outline.a(errorString, 69)));
                        sb2.append("Error resolution was canceled by the user, original error message: ");
                        sb2.append(errorString);
                        sb2.append(": ");
                        sb2.append(str);
                        aVar.a(new Status(17, sb2.toString()));
                        break;
                    } else {
                        throw null;
                    }
                }
            case 6:
                if (this.d.getApplicationContext() instanceof Application) {
                    a.a((Application) this.d.getApplicationContext());
                    a.f1777f.a(new k(this));
                    a aVar3 = a.f1777f;
                    if (!aVar3.c.get()) {
                        ActivityManager.RunningAppProcessInfo runningAppProcessInfo = new ActivityManager.RunningAppProcessInfo();
                        ActivityManager.getMyMemoryState(runningAppProcessInfo);
                        if (!aVar3.c.getAndSet(true) && runningAppProcessInfo.importance > 100) {
                            aVar3.b.set(true);
                        }
                    }
                    if (!aVar3.b.get()) {
                        this.c = 300000;
                        break;
                    }
                }
                break;
            case 7:
                a((j.c.a.a.c.k.b) message.obj);
                break;
            case 9:
                if (this.h.containsKey(message.obj)) {
                    a aVar4 = this.h.get(message.obj);
                    ResourcesFlusher.a(aVar4.f1794l.f1788l);
                    if (aVar4.f1791i) {
                        aVar4.a();
                        break;
                    }
                }
                break;
            case 10:
                for (z<?> remove : this.f1787k) {
                    this.h.remove(remove).f();
                }
                this.f1787k.clear();
                break;
            case 11:
                if (this.h.containsKey(message.obj)) {
                    a aVar5 = this.h.get(message.obj);
                    ResourcesFlusher.a(aVar5.f1794l.f1788l);
                    if (aVar5.f1791i) {
                        aVar5.h();
                        b bVar2 = aVar5.f1794l;
                        if (bVar2.f1783e.a(bVar2.d) == 18) {
                            status = new Status(8, "Connection timed out while waiting for Google Play services update to complete.");
                        } else {
                            status = new Status(8, "API failed to connect while resuming due to an unknown error.");
                        }
                        aVar5.a(status);
                        aVar5.b.e();
                        break;
                    }
                }
                break;
            case 12:
                if (this.h.containsKey(message.obj)) {
                    this.h.get(message.obj).a(true);
                    break;
                }
                break;
            case 14:
                if (((i) message.obj) == null) {
                    throw null;
                } else if (!this.h.containsKey(null)) {
                    throw null;
                } else {
                    this.h.get(null).a(false);
                    throw null;
                }
            case 15:
                C0022b bVar3 = (C0022b) message.obj;
                if (this.h.containsKey(bVar3.a)) {
                    a aVar6 = this.h.get(bVar3.a);
                    if (aVar6.f1792j.contains(bVar3) && !aVar6.f1791i) {
                        if (aVar6.b.c()) {
                            aVar6.e();
                            break;
                        } else {
                            aVar6.a();
                            break;
                        }
                    }
                }
                break;
            case 16:
                C0022b bVar4 = (C0022b) message.obj;
                if (this.h.containsKey(bVar4.a)) {
                    a aVar7 = this.h.get(bVar4.a);
                    if (aVar7.f1792j.remove(bVar4)) {
                        aVar7.f1794l.f1788l.removeMessages(15, bVar4);
                        aVar7.f1794l.f1788l.removeMessages(16, bVar4);
                        d dVar = bVar4.b;
                        ArrayList arrayList = new ArrayList(aVar7.a.size());
                        for (j next2 : aVar7.a) {
                            if (next2 instanceof s) {
                                y yVar = (y) ((s) next2);
                                if (yVar == null) {
                                    throw null;
                                } else if (aVar7.f1790f.get(yVar.b) != null) {
                                    throw null;
                                }
                            }
                        }
                        int size = arrayList.size();
                        while (i3 < size) {
                            Object obj = arrayList.get(i3);
                            i3++;
                            j jVar = (j) obj;
                            aVar7.a.remove(jVar);
                            ((x) jVar).a.a.b((Exception) new UnsupportedApiCallException(dVar));
                        }
                        break;
                    }
                }
                break;
            default:
                StringBuilder sb3 = new StringBuilder(31);
                sb3.append("Unknown message id: ");
                sb3.append(i2);
                Log.w("GoogleApiManager", sb3.toString());
                return false;
        }
        return true;
    }

    public class a<O extends a.c> implements j.c.a.a.c.k.c, j.c.a.a.c.k.d, c0 {
        public final Queue<j> a;
        public final a.e b;
        public final z<O> c;
        public final g d;

        /* renamed from: e  reason: collision with root package name */
        public final Set<a0> f1789e;

        /* renamed from: f  reason: collision with root package name */
        public final Map<f<?>, r> f1790f;
        public final int g;
        public final t h;

        /* renamed from: i  reason: collision with root package name */
        public boolean f1791i;

        /* renamed from: j  reason: collision with root package name */
        public final List<C0022b> f1792j;

        /* renamed from: k  reason: collision with root package name */
        public j.c.a.a.c.b f1793k;

        /* renamed from: l  reason: collision with root package name */
        public final /* synthetic */ b f1794l;

        public final void a(int i2) {
            if (Looper.myLooper() == this.f1794l.f1788l.getLooper()) {
                d();
            } else {
                this.f1794l.f1788l.post(new m(this));
            }
        }

        public final void b(Bundle bundle) {
            if (Looper.myLooper() == this.f1794l.f1788l.getLooper()) {
                c();
            } else {
                this.f1794l.f1788l.post(new l(this));
            }
        }

        public final void c() {
            g();
            c(j.c.a.a.c.b.f1774f);
            h();
            Iterator<r> it = this.f1790f.values().iterator();
            if (!it.hasNext()) {
                e();
                i();
                return;
            }
            r next = it.next();
            throw null;
        }

        public final void d() {
            g();
            this.f1791i = true;
            g gVar = this.d;
            if (gVar != null) {
                gVar.a(true, w.a);
                Handler handler = this.f1794l.f1788l;
                handler.sendMessageDelayed(Message.obtain(handler, 9, this.c), this.f1794l.a);
                Handler handler2 = this.f1794l.f1788l;
                handler2.sendMessageDelayed(Message.obtain(handler2, 11, this.c), this.f1794l.b);
                this.f1794l.f1784f.a.clear();
                return;
            }
            throw null;
        }

        public final void e() {
            ArrayList arrayList = new ArrayList(this.a);
            int size = arrayList.size();
            int i2 = 0;
            while (i2 < size) {
                Object obj = arrayList.get(i2);
                i2++;
                j jVar = (j) obj;
                if (!this.b.c()) {
                    return;
                }
                if (b(jVar)) {
                    this.a.remove(jVar);
                }
            }
        }

        public final void f() {
            ResourcesFlusher.a(this.f1794l.f1788l);
            a(b.f1779m);
            g gVar = this.d;
            if (gVar != null) {
                gVar.a(false, b.f1779m);
                for (f yVar : (f[]) this.f1790f.keySet().toArray(new f[this.f1790f.size()])) {
                    a(new y(yVar, new f()));
                }
                c(new j.c.a.a.c.b(4));
                if (this.b.c()) {
                    this.b.a(new n(this));
                    return;
                }
                return;
            }
            throw null;
        }

        public final void g() {
            ResourcesFlusher.a(this.f1794l.f1788l);
            this.f1793k = null;
        }

        public final void h() {
            if (this.f1791i) {
                this.f1794l.f1788l.removeMessages(11, this.c);
                this.f1794l.f1788l.removeMessages(9, this.c);
                this.f1791i = false;
            }
        }

        public final void i() {
            this.f1794l.f1788l.removeMessages(12, this.c);
            Handler handler = this.f1794l.f1788l;
            handler.sendMessageDelayed(handler.obtainMessage(12, this.c), this.f1794l.c);
        }

        public final void a(j.c.a.a.c.b bVar) {
            j.c.a.a.h.f fVar;
            ResourcesFlusher.a(this.f1794l.f1788l);
            t tVar = this.h;
            if (!(tVar == null || (fVar = tVar.f1799f) == null)) {
                fVar.e();
            }
            g();
            this.f1794l.f1784f.a.clear();
            c(bVar);
            if (bVar.c == 4) {
                a(b.f1780n);
            } else if (this.a.isEmpty()) {
                this.f1793k = bVar;
            } else {
                b(bVar);
                if (!this.f1794l.a(bVar, this.g)) {
                    if (bVar.c == 18) {
                        this.f1791i = true;
                    }
                    if (this.f1791i) {
                        Handler handler = this.f1794l.f1788l;
                        handler.sendMessageDelayed(Message.obtain(handler, 9, this.c), this.f1794l.a);
                    } else if (this.c != null) {
                        throw null;
                    } else {
                        throw null;
                    }
                }
            }
        }

        public final boolean b(j jVar) {
            if (!(jVar instanceof s)) {
                c(jVar);
                return true;
            }
            s sVar = (s) jVar;
            y yVar = (y) sVar;
            if (yVar == null) {
                throw null;
            } else if (this.f1790f.get(yVar.b) == null) {
                d a2 = a((d[]) null);
                if (a2 == null) {
                    c(jVar);
                    return true;
                } else if (this.f1790f.get(yVar.b) == null) {
                    ((x) sVar).a.a.b((Exception) new UnsupportedApiCallException(a2));
                    return false;
                } else {
                    throw null;
                }
            } else {
                throw null;
            }
        }

        public final void c(j jVar) {
            b();
            if (((y) jVar) != null) {
                try {
                    jVar.a(this);
                } catch (DeadObjectException unused) {
                    a(1);
                    this.b.e();
                }
            } else {
                throw null;
            }
        }

        public final void c(j.c.a.a.c.b bVar) {
            Iterator<a0> it = this.f1789e.iterator();
            if (it.hasNext()) {
                a0 next = it.next();
                if (ResourcesFlusher.c(bVar, j.c.a.a.c.b.f1774f)) {
                    this.b.d();
                }
                if (next != null) {
                    throw null;
                }
                throw null;
            }
            this.f1789e.clear();
        }

        public final boolean b(j.c.a.a.c.b bVar) {
            synchronized (b.f1781o) {
            }
            return false;
        }

        public final boolean b() {
            return this.b.g();
        }

        public final void a(j jVar) {
            ResourcesFlusher.a(this.f1794l.f1788l);
            if (!this.b.c()) {
                this.a.add(jVar);
                j.c.a.a.c.b bVar = this.f1793k;
                if (bVar != null) {
                    if ((bVar.c == 0 || bVar.d == null) ? false : true) {
                        a(this.f1793k);
                        return;
                    }
                }
                a();
            } else if (b(jVar)) {
                i();
            } else {
                this.a.add(jVar);
            }
        }

        public final void a(Status status) {
            ResourcesFlusher.a(this.f1794l.f1788l);
            Iterator<j> it = this.a.iterator();
            while (it.hasNext()) {
                f<T> fVar = ((x) it.next()).a;
                fVar.a.b((Exception) new ApiException(status));
            }
            this.a.clear();
        }

        public final boolean a(boolean z) {
            ResourcesFlusher.a(this.f1794l.f1788l);
            if (!this.b.c() || this.f1790f.size() != 0) {
                return false;
            }
            g gVar = this.d;
            if (!gVar.a.isEmpty() || !gVar.b.isEmpty()) {
                if (z) {
                    i();
                }
                return false;
            }
            this.b.e();
            return true;
        }

        public final void a() {
            ResourcesFlusher.a(this.f1794l.f1788l);
            if (!this.b.c() && !this.b.a()) {
                b bVar = this.f1794l;
                i iVar = bVar.f1784f;
                Context context = bVar.d;
                a.e eVar = this.b;
                if (iVar != null) {
                    ResourcesFlusher.b(context);
                    ResourcesFlusher.b(eVar);
                    int i2 = 0;
                    if (eVar.h()) {
                        int i3 = eVar.i();
                        int i4 = iVar.a.get(i3, -1);
                        if (i4 != -1) {
                            i2 = i4;
                        } else {
                            int i5 = 0;
                            while (true) {
                                if (i5 >= iVar.a.size()) {
                                    i2 = i4;
                                    break;
                                }
                                int keyAt = iVar.a.keyAt(i5);
                                if (keyAt > i3 && iVar.a.get(keyAt) == 0) {
                                    break;
                                }
                                i5++;
                            }
                            if (i2 == -1) {
                                i2 = iVar.b.a(context, i3);
                            }
                            iVar.a.put(i3, i2);
                        }
                    }
                    if (i2 != 0) {
                        a(new j.c.a.a.c.b(i2, null));
                        return;
                    }
                    c cVar = new c(this.b, this.c);
                    if (this.b.g()) {
                        t tVar = this.h;
                        j.c.a.a.h.f fVar = tVar.f1799f;
                        if (fVar != null) {
                            fVar.e();
                        }
                        tVar.f1798e.f1817f = Integer.valueOf(System.identityHashCode(tVar));
                        a.C0018a<? extends j.c.a.a.h.f, j.c.a.a.h.a> aVar = tVar.c;
                        Context context2 = tVar.a;
                        Looper looper = tVar.b.getLooper();
                        j.c.a.a.c.l.c cVar2 = tVar.f1798e;
                        tVar.f1799f = (j.c.a.a.h.f) aVar.a(context2, looper, cVar2, cVar2.f1816e, tVar, tVar);
                        tVar.g = cVar;
                        Set<Scope> set = tVar.d;
                        if (set == null || set.isEmpty()) {
                            tVar.b.post(new u(tVar));
                        } else {
                            tVar.f1799f.f();
                        }
                    }
                    this.b.a(cVar);
                    return;
                }
                throw null;
            }
        }

        public final d a(d[] dVarArr) {
            if (!(dVarArr == null || dVarArr.length == 0)) {
                d[] b2 = this.b.b();
                if (b2 == null) {
                    b2 = new d[0];
                }
                ArrayMap arrayMap = new ArrayMap(b2.length);
                for (d dVar : b2) {
                    arrayMap.put(dVar.b, Long.valueOf(dVar.c()));
                }
                for (d dVar2 : dVarArr) {
                    if (!arrayMap.containsKey(dVar2.b) || ((Long) arrayMap.get(dVar2.b)).longValue() < dVar2.c()) {
                        return dVar2;
                    }
                }
            }
            return null;
        }
    }

    public final void a(j.c.a.a.c.k.b<?> bVar) {
        if (bVar != null) {
            a aVar = this.h.get(null);
            if (aVar != null) {
                if (aVar.b()) {
                    this.f1787k.add(null);
                }
                aVar.a();
                return;
            }
            new LinkedList();
            new HashSet();
            new HashMap();
            new ArrayList();
            this.f1788l.getLooper();
            if (bVar != null) {
                j.c.a.a.h.a aVar2 = j.c.a.a.h.a.f2144i;
                new ArraySet().addAll(Collections.emptySet());
                throw null;
            }
            throw null;
        }
        throw null;
    }

    public final boolean a(j.c.a.a.c.b bVar, int i2) {
        e eVar = this.f1783e;
        Context context = this.d;
        PendingIntent pendingIntent = null;
        if (eVar != null) {
            if ((bVar.c == 0 || bVar.d == null) ? false : true) {
                pendingIntent = bVar.d;
            } else {
                Intent a2 = eVar.a(context, bVar.c, (String) null);
                if (a2 != null) {
                    pendingIntent = PendingIntent.getActivity(context, 0, a2, 134217728);
                }
            }
            if (pendingIntent == null) {
                return false;
            }
            eVar.a(context, bVar.c, GoogleApiActivity.a(context, pendingIntent, i2));
            return true;
        }
        throw null;
    }
}
