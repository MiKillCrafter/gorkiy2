package j.c.a.b.x;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import j.c.a.a.c.n.c;
import j.c.a.b.h;
import j.c.a.b.j;
import j.c.a.b.x.v;
import java.util.Calendar;
import java.util.Locale;

/* compiled from: YearGridAdapter */
public class YearGridAdapter0 extends RecyclerView.g<v.a> {
    public final MaterialCalendar<?> c;

    /* compiled from: YearGridAdapter */
    public static class a extends RecyclerView.d0 {

        /* renamed from: t  reason: collision with root package name */
        public final TextView f2370t;

        public a(TextView textView) {
            super(textView);
            this.f2370t = textView;
        }
    }

    public YearGridAdapter0(MaterialCalendar<?> materialCalendar) {
        this.c = materialCalendar;
    }

    public void a(RecyclerView.d0 d0Var, int i2) {
        a aVar = (a) d0Var;
        int i3 = this.c.a0.b.f2363e + i2;
        String string = aVar.f2370t.getContext().getString(j.mtrl_picker_navigate_to_year_description);
        aVar.f2370t.setText(String.format(Locale.getDefault(), "%d", Integer.valueOf(i3)));
        aVar.f2370t.setContentDescription(String.format(string, Integer.valueOf(i3)));
        CalendarStyle calendarStyle = this.c.d0;
        Calendar b = c.b();
        CalendarItemStyle calendarItemStyle = b.get(1) == i3 ? calendarStyle.f2361f : calendarStyle.d;
        for (Long longValue : this.c.Z.p()) {
            b.setTimeInMillis(longValue.longValue());
            if (b.get(1) == i3) {
                calendarItemStyle = calendarStyle.f2360e;
            }
        }
        calendarItemStyle.a(aVar.f2370t);
        aVar.f2370t.setOnClickListener(new YearGridAdapter(this, i3));
    }

    public int b(int i2) {
        return i2 - this.c.a0.b.f2363e;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public RecyclerView.d0 a(ViewGroup viewGroup, int i2) {
        return new a((TextView) LayoutInflater.from(viewGroup.getContext()).inflate(h.mtrl_calendar_year, viewGroup, false));
    }

    public int a() {
        return this.c.a0.f2357f;
    }
}
