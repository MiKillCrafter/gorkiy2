package j.c.a.b.k0;

import android.graphics.drawable.Drawable;
import com.google.android.material.textfield.TextInputLayout;

public class NoEndIconDelegate extends EndIconDelegate {
    public NoEndIconDelegate(TextInputLayout textInputLayout) {
        super(textInputLayout);
    }

    public void a() {
        super.a.setEndIconOnClickListener(null);
        super.a.setEndIconDrawable((Drawable) null);
        super.a.setEndIconContentDescription((CharSequence) null);
    }
}
