package j.c.a.b.x;

import android.graphics.Canvas;
import android.view.View;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import i.h.k.Pair;
import j.c.a.a.c.n.c;
import java.util.Calendar;

/* compiled from: MaterialCalendar */
public class MaterialCalendar3 extends RecyclerView.n {
    public final Calendar a = c.c();
    public final Calendar b = c.c();
    public final /* synthetic */ MaterialCalendar c;

    public MaterialCalendar3(MaterialCalendar materialCalendar) {
        this.c = materialCalendar;
    }

    public void a(Canvas canvas, RecyclerView recyclerView, RecyclerView.a0 a0Var) {
        int i2;
        int i3;
        if ((recyclerView.getAdapter() instanceof YearGridAdapter0) && (recyclerView.getLayoutManager() instanceof GridLayoutManager)) {
            YearGridAdapter0 yearGridAdapter0 = (YearGridAdapter0) recyclerView.getAdapter();
            GridLayoutManager gridLayoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
            for (Pair next : this.c.Z.l()) {
                F f2 = next.a;
                if (!(f2 == null || next.b == null)) {
                    this.a.setTimeInMillis(((Long) f2).longValue());
                    this.b.setTimeInMillis(((Long) next.b).longValue());
                    int b2 = yearGridAdapter0.b(this.a.get(1));
                    int b3 = yearGridAdapter0.b(this.b.get(1));
                    View b4 = gridLayoutManager.b(b2);
                    View b5 = gridLayoutManager.b(b3);
                    int i4 = gridLayoutManager.I;
                    int i5 = b2 / i4;
                    int i6 = b3 / i4;
                    for (int i7 = i5; i7 <= i6; i7++) {
                        View b6 = gridLayoutManager.b(gridLayoutManager.I * i7);
                        if (b6 != null) {
                            int top = b6.getTop() + this.c.d0.d.a.top;
                            int bottom = b6.getBottom() - this.c.d0.d.a.bottom;
                            if (i7 == i5) {
                                i2 = (b4.getWidth() / 2) + b4.getLeft();
                            } else {
                                i2 = 0;
                            }
                            if (i7 == i6) {
                                i3 = (b5.getWidth() / 2) + b5.getLeft();
                            } else {
                                i3 = recyclerView.getWidth();
                            }
                            canvas.drawRect((float) i2, (float) top, (float) i3, (float) bottom, this.c.d0.h);
                        }
                    }
                }
            }
        }
    }
}
