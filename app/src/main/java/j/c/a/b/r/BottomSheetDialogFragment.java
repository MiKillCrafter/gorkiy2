package j.c.a.b.r;

import android.app.Dialog;
import android.os.Bundle;
import i.b.k.AppCompatDialogFragment;

public class BottomSheetDialogFragment extends AppCompatDialogFragment {
    public Dialog g(Bundle bundle) {
        return new BottomSheetDialog(p(), this.a0);
    }
}
