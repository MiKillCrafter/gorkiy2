package j.c.a.b.h0;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

public class BaseTransientBottomBar extends AnimatorListenerAdapter {
    public final /* synthetic */ com.google.android.material.snackbar.BaseTransientBottomBar a;

    public BaseTransientBottomBar(com.google.android.material.snackbar.BaseTransientBottomBar baseTransientBottomBar) {
        this.a = baseTransientBottomBar;
    }

    public void onAnimationEnd(Animator animator) {
        this.a.b();
    }
}
