package j.c.a.b.q;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

/* compiled from: BottomAppBar */
public class BottomAppBar2 extends AnimatorListenerAdapter {
    public final /* synthetic */ BottomAppBar a;

    public BottomAppBar2(BottomAppBar bottomAppBar) {
        this.a = bottomAppBar;
    }

    public void onAnimationStart(Animator animator) {
        this.a.b0.onAnimationStart(animator);
        FloatingActionButton h = this.a.h();
        if (h != null) {
            h.setTranslationX(this.a.getFabTranslationX());
        }
    }
}
