package j.c.a.b.k0;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

/* compiled from: ClearTextEndIconDelegate */
public class ClearTextEndIconDelegate1 extends AnimatorListenerAdapter {
    public final /* synthetic */ ClearTextEndIconDelegate a;

    public ClearTextEndIconDelegate1(ClearTextEndIconDelegate clearTextEndIconDelegate) {
        this.a = clearTextEndIconDelegate;
    }

    public void onAnimationEnd(Animator animator) {
        this.a.a.setEndIconVisible(false);
    }
}
