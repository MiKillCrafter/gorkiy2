package j.c.a.b.d0;

import android.graphics.Typeface;
import j.c.a.b.b0.CollapsingTextHelper;
import j.c.a.b.b0.CollapsingTextHelper0;

public final class CancelableFontCallback extends TextAppearanceFontCallback {
    public final Typeface a;
    public final a b;
    public boolean c;

    public interface a {
    }

    public CancelableFontCallback(a aVar, Typeface typeface) {
        this.a = typeface;
        this.b = aVar;
    }

    public void a(Typeface typeface, boolean z) {
        a(typeface);
    }

    public void a(int i2) {
        a(this.a);
    }

    public final void a(Typeface typeface) {
        if (!this.c) {
            CollapsingTextHelper collapsingTextHelper = ((CollapsingTextHelper0) this.b).a;
            CancelableFontCallback cancelableFontCallback = collapsingTextHelper.v;
            boolean z = true;
            if (cancelableFontCallback != null) {
                cancelableFontCallback.c = true;
            }
            if (collapsingTextHelper.f2178s != typeface) {
                collapsingTextHelper.f2178s = typeface;
            } else {
                z = false;
            }
            if (z) {
                collapsingTextHelper.e();
            }
        }
    }
}
