package j.c.a.b.x;

import android.os.Parcel;
import android.os.Parcelable;
import j.c.a.a.c.n.c;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

public final class Month implements Comparable<n>, Parcelable {
    public static final Parcelable.Creator<n> CREATOR = new a();
    public final Calendar b;
    public final String c;
    public final int d;

    /* renamed from: e  reason: collision with root package name */
    public final int f2363e = this.b.get(1);

    /* renamed from: f  reason: collision with root package name */
    public final int f2364f = this.b.getMaximum(7);
    public final int g = this.b.getActualMaximum(5);

    public static class a implements Parcelable.Creator<n> {
        public Object createFromParcel(Parcel parcel) {
            int readInt = parcel.readInt();
            int readInt2 = parcel.readInt();
            Calendar c = c.c();
            c.set(1, readInt);
            c.set(2, readInt2);
            return new Month(c);
        }

        public Object[] newArray(int i2) {
            return new Month[i2];
        }
    }

    public Month(Calendar calendar) {
        calendar.set(5, 1);
        Calendar a2 = c.a(calendar);
        this.b = a2;
        this.d = a2.get(2);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("LLLL, yyyy", Locale.getDefault());
        simpleDateFormat.setTimeZone(c.a());
        this.c = simpleDateFormat.format(this.b.getTime());
        this.b.getTimeInMillis();
    }

    /* renamed from: a */
    public int compareTo(Month month) {
        return this.b.compareTo(month.b);
    }

    public int b(Month month) {
        if (this.b instanceof GregorianCalendar) {
            return (month.d - this.d) + ((month.f2363e - this.f2363e) * 12);
        }
        throw new IllegalArgumentException("Only Gregorian calendars are supported.");
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Month)) {
            return false;
        }
        Month month = (Month) obj;
        if (this.d == month.d && this.f2363e == month.f2363e) {
            return true;
        }
        return false;
    }

    public int f() {
        int firstDayOfWeek = this.b.get(7) - this.b.getFirstDayOfWeek();
        return firstDayOfWeek < 0 ? firstDayOfWeek + this.f2364f : firstDayOfWeek;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.d), Integer.valueOf(this.f2363e)});
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeInt(this.f2363e);
        parcel.writeInt(this.d);
    }

    public Month a(int i2) {
        Calendar a2 = c.a(this.b);
        a2.add(2, i2);
        return new Month(a2);
    }
}
