package j.c.a.b.n;

import android.view.View;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.appbar.AppBarLayout;
import i.h.l.x.AccessibilityViewCommand;

/* compiled from: AppBarLayout */
public class AppBarLayout1 implements AccessibilityViewCommand {
    public final /* synthetic */ CoordinatorLayout a;
    public final /* synthetic */ AppBarLayout b;
    public final /* synthetic */ View c;
    public final /* synthetic */ int d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ AppBarLayout.BaseBehavior f2296e;

    public AppBarLayout1(AppBarLayout.BaseBehavior baseBehavior, CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, View view, int i2) {
        this.f2296e = baseBehavior;
        this.a = coordinatorLayout;
        this.b = appBarLayout;
        this.c = view;
        this.d = i2;
    }

    public boolean a(View view, AccessibilityViewCommand.a aVar) {
        this.f2296e.a(this.a, this.b, this.c, this.d, new int[]{0, 0});
        return true;
    }
}
