package j.c.a.b.k0;

import android.widget.AutoCompleteTextView;

/* compiled from: DropdownMenuEndIconDelegate */
public class DropdownMenuEndIconDelegate2 implements AutoCompleteTextView.OnDismissListener {
    public final /* synthetic */ DropdownMenuEndIconDelegate a;

    public DropdownMenuEndIconDelegate2(DropdownMenuEndIconDelegate dropdownMenuEndIconDelegate) {
        this.a = dropdownMenuEndIconDelegate;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.b.k0.DropdownMenuEndIconDelegate.a(j.c.a.b.k0.DropdownMenuEndIconDelegate, boolean):void
     arg types: [j.c.a.b.k0.DropdownMenuEndIconDelegate, int]
     candidates:
      j.c.a.b.k0.DropdownMenuEndIconDelegate.a(j.c.a.b.k0.DropdownMenuEndIconDelegate, android.widget.EditText):android.widget.AutoCompleteTextView
      j.c.a.b.k0.DropdownMenuEndIconDelegate.a(j.c.a.b.k0.DropdownMenuEndIconDelegate, android.widget.AutoCompleteTextView):void
      j.c.a.b.k0.DropdownMenuEndIconDelegate.a(j.c.a.b.k0.DropdownMenuEndIconDelegate, boolean):void */
    public void onDismiss() {
        DropdownMenuEndIconDelegate dropdownMenuEndIconDelegate = this.a;
        dropdownMenuEndIconDelegate.g = true;
        dropdownMenuEndIconDelegate.f2268i = System.currentTimeMillis();
        DropdownMenuEndIconDelegate.a(this.a, false);
    }
}
