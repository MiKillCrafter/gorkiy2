package j.c.a.b.i0;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import i.e.SimpleArrayMap;
import i.j.a.AbsSavedState;
import j.a.a.a.outline;

public class ExtendableSavedState extends AbsSavedState {
    public static final Parcelable.Creator<a> CREATOR = new a();
    public final SimpleArrayMap<String, Bundle> d;

    public static class a implements Parcelable.ClassLoaderCreator<a> {
        public Object createFromParcel(Parcel parcel, ClassLoader classLoader) {
            return new ExtendableSavedState(parcel, classLoader, null);
        }

        public Object[] newArray(int i2) {
            return new ExtendableSavedState[i2];
        }

        public Object createFromParcel(Parcel parcel) {
            return new ExtendableSavedState(parcel, null, null);
        }
    }

    public ExtendableSavedState(Parcelable parcelable) {
        super(parcelable);
        this.d = new SimpleArrayMap<>();
    }

    public String toString() {
        StringBuilder a2 = outline.a("ExtendableSavedState{");
        a2.append(Integer.toHexString(System.identityHashCode(this)));
        a2.append(" states=");
        a2.append(this.d);
        a2.append("}");
        return a2.toString();
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeParcelable(super.b, i2);
        int i3 = this.d.d;
        parcel.writeInt(i3);
        String[] strArr = new String[i3];
        Bundle[] bundleArr = new Bundle[i3];
        for (int i4 = 0; i4 < i3; i4++) {
            strArr[i4] = this.d.c(i4);
            bundleArr[i4] = this.d.e(i4);
        }
        parcel.writeStringArray(strArr);
        parcel.writeTypedArray(bundleArr, 0);
    }

    public /* synthetic */ ExtendableSavedState(Parcel parcel, ClassLoader classLoader, a aVar) {
        super(parcel, classLoader);
        int readInt = parcel.readInt();
        String[] strArr = new String[readInt];
        parcel.readStringArray(strArr);
        Bundle[] bundleArr = new Bundle[readInt];
        parcel.readTypedArray(bundleArr, Bundle.CREATOR);
        this.d = new SimpleArrayMap<>(readInt);
        for (int i2 = 0; i2 < readInt; i2++) {
            this.d.put(strArr[i2], bundleArr[i2]);
        }
    }
}
