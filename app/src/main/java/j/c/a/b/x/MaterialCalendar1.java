package j.c.a.b.x;

import android.view.View;

/* compiled from: MaterialCalendar */
public class MaterialCalendar1 implements View.OnClickListener {
    public final /* synthetic */ MonthsPagerAdapter0 b;
    public final /* synthetic */ MaterialCalendar c;

    public MaterialCalendar1(MaterialCalendar materialCalendar, MonthsPagerAdapter0 monthsPagerAdapter0) {
        this.c = materialCalendar;
        this.b = monthsPagerAdapter0;
    }

    public void onClick(View view) {
        int s2 = this.c.N().s() + 1;
        if (s2 < this.c.f0.getAdapter().a()) {
            this.c.a(this.b.b(s2));
        }
    }
}
