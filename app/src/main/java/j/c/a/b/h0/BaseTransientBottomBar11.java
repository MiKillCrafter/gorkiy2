package j.c.a.b.h0;

import android.view.View;
import com.google.android.material.behavior.SwipeDismissBehavior;
import com.google.android.material.snackbar.BaseTransientBottomBar;

/* compiled from: BaseTransientBottomBar */
public class BaseTransientBottomBar11 implements SwipeDismissBehavior.b {
    public final /* synthetic */ BaseTransientBottomBar a;

    public BaseTransientBottomBar11(BaseTransientBottomBar baseTransientBottomBar) {
        this.a = baseTransientBottomBar;
    }

    public void a(View view) {
        view.setVisibility(8);
        BaseTransientBottomBar baseTransientBottomBar = this.a;
        if (baseTransientBottomBar != null) {
            SnackbarManager.b().a(baseTransientBottomBar.f491n, 0);
            return;
        }
        throw null;
    }
}
