package j.c.a.b.q;

import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

/* compiled from: BottomAppBar */
public class BottomAppBar3 extends FloatingActionButton.a {
    public final /* synthetic */ int a;
    public final /* synthetic */ BottomAppBar b;

    /* compiled from: BottomAppBar */
    public class a extends FloatingActionButton.a {
        public a() {
        }

        public void b(FloatingActionButton floatingActionButton) {
            BottomAppBar.a(BottomAppBar3.this.b);
        }
    }

    public BottomAppBar3(BottomAppBar bottomAppBar, int i2) {
        this.b = bottomAppBar;
        this.a = i2;
    }

    public void a(FloatingActionButton floatingActionButton) {
        floatingActionButton.setTranslationX(this.b.b(this.a));
        floatingActionButton.b(new a(), true);
    }
}
