package j.c.a.b.g0;

import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RadialGradient;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.Shader;
import j.c.a.b.f0.ShadowRenderer;
import j.c.a.b.g0.m;
import java.util.ArrayList;
import java.util.List;

public class ShapePath {
    @Deprecated
    public float a;
    @Deprecated
    public float b;
    @Deprecated
    public float c;
    @Deprecated
    public float d;
    @Deprecated

    /* renamed from: e  reason: collision with root package name */
    public float f2258e;
    @Deprecated

    /* renamed from: f  reason: collision with root package name */
    public float f2259f;
    public final List<m.e> g = new ArrayList();
    public final List<m.f> h = new ArrayList();

    /* renamed from: i  reason: collision with root package name */
    public boolean f2260i;

    public static class a extends f {
        public final c b;

        public a(c cVar) {
            this.b = cVar;
        }

        public void a(Matrix matrix, ShadowRenderer shadowRenderer, int i2, Canvas canvas) {
            ShadowRenderer shadowRenderer2 = shadowRenderer;
            int i3 = i2;
            Canvas canvas2 = canvas;
            c cVar = this.b;
            float f2 = cVar.f2262f;
            float f3 = cVar.g;
            c cVar2 = this.b;
            RectF rectF = new RectF(cVar2.b, cVar2.c, cVar2.d, cVar2.f2261e);
            boolean z = f3 < 0.0f;
            Path path = shadowRenderer2.g;
            if (z) {
                int[] iArr = ShadowRenderer.f2210k;
                iArr[0] = 0;
                iArr[1] = shadowRenderer2.f2213f;
                iArr[2] = shadowRenderer2.f2212e;
                iArr[3] = shadowRenderer2.d;
            } else {
                path.rewind();
                path.moveTo(rectF.centerX(), rectF.centerY());
                path.arcTo(rectF, f2, f3);
                path.close();
                float f4 = (float) (-i3);
                rectF.inset(f4, f4);
                int[] iArr2 = ShadowRenderer.f2210k;
                iArr2[0] = 0;
                iArr2[1] = shadowRenderer2.d;
                iArr2[2] = shadowRenderer2.f2212e;
                iArr2[3] = shadowRenderer2.f2213f;
            }
            float width = 1.0f - (((float) i3) / (rectF.width() / 2.0f));
            float[] fArr = ShadowRenderer.f2211l;
            fArr[1] = width;
            fArr[2] = ((1.0f - width) / 2.0f) + width;
            shadowRenderer2.b.setShader(new RadialGradient(rectF.centerX(), rectF.centerY(), rectF.width() / 2.0f, ShadowRenderer.f2210k, ShadowRenderer.f2211l, Shader.TileMode.CLAMP));
            canvas.save();
            canvas2.concat(matrix);
            if (!z) {
                canvas2.clipPath(path, Region.Op.DIFFERENCE);
                canvas2.drawPath(path, shadowRenderer2.h);
            }
            canvas.drawArc(rectF, f2, f3, true, shadowRenderer2.b);
            canvas.restore();
        }
    }

    public static class c extends e {
        public static final RectF h = new RectF();
        @Deprecated
        public float b;
        @Deprecated
        public float c;
        @Deprecated
        public float d;
        @Deprecated

        /* renamed from: e  reason: collision with root package name */
        public float f2261e;
        @Deprecated

        /* renamed from: f  reason: collision with root package name */
        public float f2262f;
        @Deprecated
        public float g;

        public c(float f2, float f3, float f4, float f5) {
            this.b = f2;
            this.c = f3;
            this.d = f4;
            this.f2261e = f5;
        }

        public void a(Matrix matrix, Path path) {
            Matrix matrix2 = super.a;
            matrix.invert(matrix2);
            path.transform(matrix2);
            h.set(this.b, this.c, this.d, this.f2261e);
            path.arcTo(h, this.f2262f, this.g, false);
            path.transform(matrix);
        }
    }

    public static class d extends e {
        public float b;
        public float c;

        public void a(Matrix matrix, Path path) {
            Matrix matrix2 = super.a;
            matrix.invert(matrix2);
            path.transform(matrix2);
            path.lineTo(this.b, this.c);
            path.transform(matrix);
        }
    }

    public static abstract class e {
        public final Matrix a = new Matrix();

        public abstract void a(Matrix matrix, Path path);
    }

    public static abstract class f {
        public static final Matrix a = new Matrix();

        public abstract void a(Matrix matrix, ShadowRenderer shadowRenderer, int i2, Canvas canvas);
    }

    public ShapePath() {
        a(0.0f, 0.0f, 270.0f, 0.0f);
    }

    public void a(float f2, float f3) {
        d dVar = new d();
        dVar.b = f2;
        dVar.c = f3;
        this.g.add(dVar);
        b bVar = new b(dVar, this.c, this.d);
        a(bVar.a() + 270.0f);
        this.h.add(bVar);
        this.f2258e = bVar.a() + 270.0f;
        this.c = f2;
        this.d = f3;
    }

    public void a(float f2, float f3, float f4, float f5, float f6, float f7) {
        c cVar = new c(f2, f3, f4, f5);
        cVar.f2262f = f6;
        cVar.g = f7;
        this.g.add(cVar);
        a aVar = new a(cVar);
        float f8 = f6 + f7;
        boolean z = f7 < 0.0f;
        if (z) {
            f6 = (f6 + 180.0f) % 360.0f;
        }
        float f9 = z ? (180.0f + f8) % 360.0f : f8;
        a(f6);
        this.h.add(aVar);
        this.f2258e = f9;
        double d2 = (double) f8;
        this.c = (((f4 - f2) / 2.0f) * ((float) Math.cos(Math.toRadians(d2)))) + ((f2 + f4) * 0.5f);
        this.d = (((f5 - f3) / 2.0f) * ((float) Math.sin(Math.toRadians(d2)))) + ((f3 + f5) * 0.5f);
    }

    public static class b extends f {
        public final d b;
        public final float c;
        public final float d;

        public b(d dVar, float f2, float f3) {
            this.b = dVar;
            this.c = f2;
            this.d = f3;
        }

        public void a(Matrix matrix, ShadowRenderer shadowRenderer, int i2, Canvas canvas) {
            d dVar = this.b;
            RectF rectF = new RectF(0.0f, 0.0f, (float) Math.hypot((double) (dVar.c - this.d), (double) (dVar.b - this.c)), 0.0f);
            Matrix matrix2 = new Matrix(matrix);
            matrix2.preTranslate(this.c, this.d);
            matrix2.preRotate(a());
            if (shadowRenderer != null) {
                rectF.bottom += (float) i2;
                rectF.offset(0.0f, (float) (-i2));
                int[] iArr = ShadowRenderer.f2208i;
                iArr[0] = shadowRenderer.f2213f;
                iArr[1] = shadowRenderer.f2212e;
                iArr[2] = shadowRenderer.d;
                Paint paint = shadowRenderer.c;
                float f2 = rectF.left;
                paint.setShader(new LinearGradient(f2, rectF.top, f2, rectF.bottom, ShadowRenderer.f2208i, ShadowRenderer.f2209j, Shader.TileMode.CLAMP));
                canvas.save();
                canvas.concat(matrix2);
                canvas.drawRect(rectF, shadowRenderer.c);
                canvas.restore();
                return;
            }
            throw null;
        }

        public float a() {
            d dVar = this.b;
            return (float) Math.toDegrees(Math.atan((double) ((dVar.c - this.d) / (dVar.b - this.c))));
        }
    }

    public void a(Matrix matrix, Path path) {
        int size = this.g.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.g.get(i2).a(matrix, path);
        }
    }

    public final void a(float f2) {
        float f3 = this.f2258e;
        if (f3 != f2) {
            float f4 = ((f2 - f3) + 360.0f) % 360.0f;
            if (f4 <= 180.0f) {
                float f5 = this.c;
                float f6 = this.d;
                c cVar = new c(f5, f6, f5, f6);
                cVar.f2262f = this.f2258e;
                cVar.g = f4;
                this.h.add(new a(cVar));
                this.f2258e = f2;
            }
        }
    }

    public void a(float f2, float f3, float f4, float f5) {
        this.a = f2;
        this.b = f3;
        this.c = f2;
        this.d = f3;
        this.f2258e = f4;
        this.f2259f = (f4 + f5) % 360.0f;
        this.g.clear();
        this.h.clear();
        this.f2260i = false;
    }
}
