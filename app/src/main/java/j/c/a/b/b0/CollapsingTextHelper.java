package j.c.a.b.b0;

import android.animation.TimeInterpolator;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import i.b.k.ResourcesFlusher;
import i.h.j.TextDirectionHeuristicsCompat;
import i.h.l.ViewCompat;
import j.c.a.b.b0.StaticLayoutBuilderCompat;
import j.c.a.b.d0.CancelableFontCallback;
import j.c.a.b.m.AnimationUtils;

public final class CollapsingTextHelper {
    public static final Paint X = null;
    public Bitmap A;
    public Paint B;
    public float C;
    public float D;
    public int[] E;
    public boolean F;
    public final TextPaint G;
    public final TextPaint H;
    public TimeInterpolator I;
    public TimeInterpolator J;
    public float K;
    public float L;
    public float M;
    public ColorStateList N;
    public float O;
    public float P;
    public float Q;
    public StaticLayout R;
    public float S;
    public float T;
    public float U;
    public CharSequence V;
    public int W = 1;
    public final View a;
    public boolean b;
    public float c;
    public final Rect d;

    /* renamed from: e  reason: collision with root package name */
    public final Rect f2166e;

    /* renamed from: f  reason: collision with root package name */
    public final RectF f2167f;
    public int g = 16;
    public int h = 16;

    /* renamed from: i  reason: collision with root package name */
    public float f2168i = 15.0f;

    /* renamed from: j  reason: collision with root package name */
    public float f2169j = 15.0f;

    /* renamed from: k  reason: collision with root package name */
    public ColorStateList f2170k;

    /* renamed from: l  reason: collision with root package name */
    public ColorStateList f2171l;

    /* renamed from: m  reason: collision with root package name */
    public float f2172m;

    /* renamed from: n  reason: collision with root package name */
    public float f2173n;

    /* renamed from: o  reason: collision with root package name */
    public float f2174o;

    /* renamed from: p  reason: collision with root package name */
    public float f2175p;

    /* renamed from: q  reason: collision with root package name */
    public float f2176q;

    /* renamed from: r  reason: collision with root package name */
    public float f2177r;

    /* renamed from: s  reason: collision with root package name */
    public Typeface f2178s;

    /* renamed from: t  reason: collision with root package name */
    public Typeface f2179t;
    public Typeface u;
    public CancelableFontCallback v;
    public CharSequence w;
    public CharSequence x;
    public boolean y;
    public boolean z;

    static {
        Paint paint = null;
        if (paint != null) {
            paint.setAntiAlias(true);
            X.setColor(-65281);
        }
    }

    public CollapsingTextHelper(View view) {
        this.a = view;
        this.G = new TextPaint(129);
        this.H = new TextPaint(this.G);
        this.f2166e = new Rect();
        this.d = new Rect();
        this.f2167f = new RectF();
    }

    public float a() {
        if (this.w == null) {
            return 0.0f;
        }
        TextPaint textPaint = this.H;
        textPaint.setTextSize(this.f2169j);
        textPaint.setTypeface(this.f2178s);
        TextPaint textPaint2 = this.H;
        CharSequence charSequence = this.w;
        return textPaint2.measureText(charSequence, 0, charSequence.length());
    }

    public void b(ColorStateList colorStateList) {
        if (this.f2171l != colorStateList) {
            this.f2171l = colorStateList;
            e();
        }
    }

    public void c(float f2) {
        if (f2 < 0.0f) {
            f2 = 0.0f;
        } else if (f2 > 1.0f) {
            f2 = 1.0f;
        }
        if (f2 != this.c) {
            this.c = f2;
            a(f2);
        }
    }

    public void d() {
        this.b = this.f2166e.width() > 0 && this.f2166e.height() > 0 && this.d.width() > 0 && this.d.height() > 0;
    }

    public void e() {
        float f2;
        float f3;
        StaticLayout staticLayout;
        if (this.a.getHeight() > 0 && this.a.getWidth() > 0) {
            float f4 = this.D;
            b(this.f2169j);
            CharSequence charSequence = this.x;
            if (!(charSequence == null || (staticLayout = this.R) == null)) {
                this.V = TextUtils.ellipsize(charSequence, this.G, (float) staticLayout.getWidth(), TextUtils.TruncateAt.END);
            }
            CharSequence charSequence2 = this.V;
            float f5 = 0.0f;
            float measureText = charSequence2 != null ? this.G.measureText(charSequence2, 0, charSequence2.length()) : 0.0f;
            int absoluteGravity = Gravity.getAbsoluteGravity(this.h, this.y ? 1 : 0);
            StaticLayout staticLayout2 = this.R;
            float height = staticLayout2 != null ? (float) staticLayout2.getHeight() : 0.0f;
            int i2 = absoluteGravity & 112;
            if (i2 == 48) {
                this.f2173n = ((float) this.f2166e.top) - this.G.ascent();
            } else if (i2 != 80) {
                float descent = (this.G.descent() - this.G.ascent()) / 2.0f;
                float descent2 = descent - this.G.descent();
                if (f()) {
                    f3 = ((float) this.f2166e.centerY()) - descent;
                } else {
                    f3 = descent2 + ((float) this.f2166e.centerY());
                }
                this.f2173n = f3;
            } else {
                this.f2173n = (float) this.f2166e.bottom;
            }
            int i3 = absoluteGravity & 8388615;
            if (i3 == 1) {
                this.f2175p = ((float) this.f2166e.centerX()) - (measureText / 2.0f);
            } else if (i3 != 5) {
                this.f2175p = (float) this.f2166e.left;
            } else {
                this.f2175p = ((float) this.f2166e.right) - measureText;
            }
            b(this.f2168i);
            CharSequence charSequence3 = this.x;
            float measureText2 = charSequence3 != null ? this.G.measureText(charSequence3, 0, charSequence3.length()) : 0.0f;
            StaticLayout staticLayout3 = this.R;
            if (staticLayout3 != null && this.W > 1 && !this.y) {
                measureText2 = staticLayout3.getLineWidth(0);
            }
            StaticLayout staticLayout4 = this.R;
            this.U = staticLayout4 != null ? staticLayout4.getLineLeft(0) : 0.0f;
            int absoluteGravity2 = Gravity.getAbsoluteGravity(this.g, this.y ? 1 : 0);
            int i4 = absoluteGravity2 & 112;
            if (i4 == 48) {
                this.f2172m = ((float) this.d.top) - this.G.ascent();
            } else if (i4 != 80) {
                float descent3 = ((this.G.descent() - this.G.ascent()) / 2.0f) - this.G.descent();
                if (f()) {
                    f2 = ((float) this.d.centerY()) - (height / 2.0f);
                } else {
                    f2 = descent3 + ((float) this.d.centerY());
                }
                this.f2172m = f2;
            } else {
                if (f()) {
                    f5 = height - this.G.descent();
                }
                this.f2172m = ((float) this.d.bottom) - f5;
            }
            int i5 = absoluteGravity2 & 8388615;
            if (i5 == 1) {
                this.f2174o = ((float) this.d.centerX()) - (measureText2 / 2.0f);
            } else if (i5 != 5) {
                this.f2174o = (float) this.d.left;
            } else {
                this.f2174o = ((float) this.d.right) - measureText2;
            }
            Bitmap bitmap = this.A;
            if (bitmap != null) {
                bitmap.recycle();
                this.A = null;
            }
            d(f4);
            a(this.c);
        }
    }

    public final boolean f() {
        return this.W > 1 && !this.y && !this.z;
    }

    public float b() {
        TextPaint textPaint = this.H;
        textPaint.setTextSize(this.f2169j);
        textPaint.setTypeface(this.f2178s);
        return -this.H.ascent();
    }

    public int c() {
        return a(this.f2171l);
    }

    public void a(int i2) {
        if (this.h != i2) {
            this.h = i2;
            e();
        }
    }

    public final void d(float f2) {
        b(f2);
        this.z = false;
        if (0 != 0 && this.A == null && !this.d.isEmpty() && !TextUtils.isEmpty(this.x)) {
            a(0.0f);
            int width = this.R.getWidth();
            int height = this.R.getHeight();
            if (width > 0 && height > 0) {
                this.A = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                this.R.draw(new Canvas(this.A));
                if (this.B == null) {
                    this.B = new Paint(3);
                }
            }
        }
        ViewCompat.A(this.a);
    }

    public final void b(float f2) {
        float f3;
        boolean z2;
        StaticLayout staticLayout;
        if (this.w != null) {
            float width = (float) this.f2166e.width();
            float width2 = (float) this.d.width();
            int i2 = 1;
            if (Math.abs(f2 - this.f2169j) < 0.001f) {
                f3 = this.f2169j;
                this.C = 1.0f;
                Typeface typeface = this.u;
                Typeface typeface2 = this.f2178s;
                if (typeface != typeface2) {
                    this.u = typeface2;
                    z2 = true;
                } else {
                    z2 = false;
                }
            } else {
                float f4 = this.f2168i;
                Typeface typeface3 = this.u;
                Typeface typeface4 = this.f2179t;
                if (typeface3 != typeface4) {
                    this.u = typeface4;
                    z2 = true;
                } else {
                    z2 = false;
                }
                if (Math.abs(f2 - this.f2168i) < 0.001f) {
                    this.C = 1.0f;
                } else {
                    this.C = f2 / this.f2168i;
                }
                float f5 = this.f2169j / this.f2168i;
                width = width2 * f5 > width ? Math.min(width / f5, width2) : width2;
                f3 = f4;
            }
            if (width > 0.0f) {
                z2 = this.D != f3 || this.F || z2;
                this.D = f3;
                this.F = false;
            }
            if (this.x == null || z2) {
                this.G.setTextSize(this.D);
                this.G.setTypeface(this.u);
                this.G.setLinearText(this.C != 1.0f);
                this.y = a(this.w);
                if (f()) {
                    i2 = this.W;
                }
                boolean z3 = this.y;
                try {
                    StaticLayoutBuilderCompat staticLayoutBuilderCompat = new StaticLayoutBuilderCompat(this.w, this.G, (int) width);
                    staticLayoutBuilderCompat.f2191j = TextUtils.TruncateAt.END;
                    staticLayoutBuilderCompat.f2190i = z3;
                    staticLayoutBuilderCompat.f2189f = Layout.Alignment.ALIGN_NORMAL;
                    staticLayoutBuilderCompat.h = false;
                    staticLayoutBuilderCompat.g = i2;
                    staticLayout = staticLayoutBuilderCompat.a();
                } catch (StaticLayoutBuilderCompat.a e2) {
                    Log.e("CollapsingTextHelper", e2.getCause().getMessage(), e2);
                    staticLayout = null;
                }
                ResourcesFlusher.a(staticLayout);
                this.R = staticLayout;
                this.x = staticLayout.getText();
            }
        }
    }

    public void a(Typeface typeface) {
        boolean z2;
        CancelableFontCallback cancelableFontCallback = this.v;
        boolean z3 = true;
        if (cancelableFontCallback != null) {
            cancelableFontCallback.c = true;
        }
        if (this.f2178s != typeface) {
            this.f2178s = typeface;
            z2 = true;
        } else {
            z2 = false;
        }
        if (this.f2179t != typeface) {
            this.f2179t = typeface;
        } else {
            z3 = false;
        }
        if (z2 || z3) {
            e();
        }
    }

    public final int a(ColorStateList colorStateList) {
        if (colorStateList == null) {
            return 0;
        }
        int[] iArr = this.E;
        if (iArr != null) {
            return colorStateList.getColorForState(iArr, 0);
        }
        return colorStateList.getDefaultColor();
    }

    public final void a(float f2) {
        this.f2167f.left = a((float) this.d.left, (float) this.f2166e.left, f2, this.I);
        this.f2167f.top = a(this.f2172m, this.f2173n, f2, this.I);
        this.f2167f.right = a((float) this.d.right, (float) this.f2166e.right, f2, this.I);
        this.f2167f.bottom = a((float) this.d.bottom, (float) this.f2166e.bottom, f2, this.I);
        this.f2176q = a(this.f2174o, this.f2175p, f2, this.I);
        this.f2177r = a(this.f2172m, this.f2173n, f2, this.I);
        d(a(this.f2168i, this.f2169j, f2, this.J));
        this.S = 1.0f - a(0.0f, 1.0f, 1.0f - f2, AnimationUtils.b);
        ViewCompat.A(this.a);
        this.T = a(1.0f, 0.0f, f2, AnimationUtils.b);
        ViewCompat.A(this.a);
        ColorStateList colorStateList = this.f2171l;
        ColorStateList colorStateList2 = this.f2170k;
        if (colorStateList != colorStateList2) {
            this.G.setColor(a(a(colorStateList2), c(), f2));
        } else {
            this.G.setColor(c());
        }
        this.G.setShadowLayer(a(this.O, this.K, f2, null), a(this.P, this.L, f2, null), a(this.Q, this.M, f2, null), a(a((ColorStateList) null), a(this.N), f2));
        this.a.postInvalidateOnAnimation();
    }

    public final boolean a(CharSequence charSequence) {
        boolean z2 = true;
        if (ViewCompat.k(this.a) != 1) {
            z2 = false;
        }
        return ((TextDirectionHeuristicsCompat.d) (z2 ? TextDirectionHeuristicsCompat.d : TextDirectionHeuristicsCompat.c)).a(charSequence, 0, charSequence.length());
    }

    public static int a(int i2, int i3, float f2) {
        float f3 = 1.0f - f2;
        return Color.argb((int) ((((float) Color.alpha(i3)) * f2) + (((float) Color.alpha(i2)) * f3)), (int) ((((float) Color.red(i3)) * f2) + (((float) Color.red(i2)) * f3)), (int) ((((float) Color.green(i3)) * f2) + (((float) Color.green(i2)) * f3)), (int) ((((float) Color.blue(i3)) * f2) + (((float) Color.blue(i2)) * f3)));
    }

    public static float a(float f2, float f3, float f4, TimeInterpolator timeInterpolator) {
        if (timeInterpolator != null) {
            f4 = timeInterpolator.getInterpolation(f4);
        }
        return AnimationUtils.a(f2, f3, f4);
    }

    public static boolean a(Rect rect, int i2, int i3, int i4, int i5) {
        return rect.left == i2 && rect.top == i3 && rect.right == i4 && rect.bottom == i5;
    }
}
