package j.c.a.b.v;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import i.b.k.ResourcesFlusher;
import i.h.f.ColorUtils;
import i.h.f.j.TintAwareDrawable;
import j.c.a.a.c.n.c;
import j.c.a.b.b0.TextDrawableHelper;
import j.c.a.b.d0.TextAppearance;
import j.c.a.b.e0.RippleUtils;
import j.c.a.b.g0.MaterialShapeDrawable;
import j.c.a.b.g0.ShapeAppearanceModel;
import j.c.a.b.m.MotionSpec;
import j.c.a.b.v.b;
import j.c.a.b.y.ElevationOverlayProvider;
import java.lang.ref.WeakReference;
import java.util.Arrays;

public class ChipDrawable extends MaterialShapeDrawable implements TintAwareDrawable, Drawable.Callback, TextDrawableHelper.b {
    public static final int[] I0 = {16842910};
    public static final ShapeDrawable J0 = new ShapeDrawable(new OvalShape());
    public ColorStateList A;
    public int[] A0;
    public float B;
    public boolean B0;
    public float C;
    public ColorStateList C0;
    public ColorStateList D;
    public WeakReference<b.a> D0 = new WeakReference<>(null);
    public float E;
    public TextUtils.TruncateAt E0;
    public ColorStateList F;
    public boolean F0;
    public CharSequence G;
    public int G0;
    public boolean H;
    public boolean H0;
    public Drawable I;
    public ColorStateList J;
    public float K;
    public boolean L;
    public boolean M;
    public Drawable N;
    public Drawable O;
    public ColorStateList P;
    public float Q;
    public CharSequence R;
    public boolean S;
    public boolean T;
    public Drawable U;
    public MotionSpec V;
    public MotionSpec W;
    public float X;
    public float Y;
    public float Z;
    public float a0;
    public float b0;
    public float c0;
    public float d0;
    public float e0;
    public final Context f0;
    public final Paint g0 = new Paint(1);
    public final Paint h0;
    public final Paint.FontMetrics i0 = new Paint.FontMetrics();
    public final RectF j0 = new RectF();
    public final PointF k0 = new PointF();
    public final Path l0 = new Path();
    public final TextDrawableHelper m0;
    public int n0;
    public int o0;
    public int p0;
    public int q0;
    public int r0;
    public int s0;
    public boolean t0;
    public int u0;
    public int v0 = 255;
    public ColorFilter w0;
    public PorterDuffColorFilter x0;
    public ColorStateList y0;
    public ColorStateList z;
    public PorterDuff.Mode z0 = PorterDuff.Mode.SRC_IN;

    public interface a {
        void a();
    }

    public ChipDrawable(Context context, AttributeSet attributeSet, int i2, int i3) {
        super(ShapeAppearanceModel.a(context, attributeSet, i2, i3).a());
        super.b.b = new ElevationOverlayProvider(context);
        j();
        this.f0 = context;
        TextDrawableHelper textDrawableHelper = new TextDrawableHelper(this);
        this.m0 = textDrawableHelper;
        this.G = "";
        textDrawableHelper.a.density = context.getResources().getDisplayMetrics().density;
        this.h0 = null;
        setState(I0);
        b(I0);
        this.F0 = true;
        if (RippleUtils.a) {
            J0.setTint(-1);
        }
    }

    public static boolean f(Drawable drawable) {
        return drawable != null && drawable.isStateful();
    }

    public static boolean h(ColorStateList colorStateList) {
        return colorStateList != null && colorStateList.isStateful();
    }

    public final void a(Rect rect, RectF rectF) {
        rectF.setEmpty();
        if (q() || p()) {
            float f2 = this.X + this.Y;
            if (ResourcesFlusher.b((Drawable) this) == 0) {
                float f3 = ((float) rect.left) + f2;
                rectF.left = f3;
                rectF.right = f3 + this.K;
            } else {
                float f4 = ((float) rect.right) - f2;
                rectF.right = f4;
                rectF.left = f4 - this.K;
            }
            float exactCenterY = rect.exactCenterY();
            float f5 = this.K;
            float f6 = exactCenterY - (f5 / 2.0f);
            rectF.top = f6;
            rectF.bottom = f6 + f5;
        }
    }

    public final void b(Rect rect, RectF rectF) {
        rectF.setEmpty();
        if (r()) {
            float f2 = this.e0 + this.d0;
            if (ResourcesFlusher.b((Drawable) this) == 0) {
                float f3 = ((float) rect.right) - f2;
                rectF.right = f3;
                rectF.left = f3 - this.Q;
            } else {
                float f4 = ((float) rect.left) + f2;
                rectF.left = f4;
                rectF.right = f4 + this.Q;
            }
            float exactCenterY = rect.exactCenterY();
            float f5 = this.Q;
            float f6 = exactCenterY - (f5 / 2.0f);
            rectF.top = f6;
            rectF.bottom = f6 + f5;
        }
    }

    public final void c(Rect rect, RectF rectF) {
        rectF.setEmpty();
        if (r()) {
            float f2 = this.e0 + this.d0 + this.Q + this.c0 + this.b0;
            if (ResourcesFlusher.b((Drawable) this) == 0) {
                float f3 = (float) rect.right;
                rectF.right = f3;
                rectF.left = f3 - f2;
            } else {
                int i2 = rect.left;
                rectF.left = (float) i2;
                rectF.right = ((float) i2) + f2;
            }
            rectF.top = (float) rect.top;
            rectF.bottom = (float) rect.bottom;
        }
    }

    public void d(ColorStateList colorStateList) {
        this.L = true;
        if (this.J != colorStateList) {
            this.J = colorStateList;
            if (q()) {
                this.I.setTintList(colorStateList);
            }
            onStateChange(getState());
        }
    }

    public void draw(Canvas canvas) {
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        Canvas canvas2 = canvas;
        Rect bounds = getBounds();
        if (!bounds.isEmpty() && (i2 = this.v0) != 0) {
            if (i2 < 255) {
                float f2 = (float) bounds.left;
                float f3 = (float) bounds.top;
                float f4 = (float) bounds.right;
                float f5 = (float) bounds.bottom;
                if (Build.VERSION.SDK_INT > 21) {
                    i10 = canvas.saveLayerAlpha(f2, f3, f4, f5, i2);
                } else {
                    i10 = canvas.saveLayerAlpha(f2, f3, f4, f5, i2, 31);
                }
                i3 = i10;
            } else {
                i3 = 0;
            }
            if (!this.H0) {
                this.g0.setColor(this.n0);
                this.g0.setStyle(Paint.Style.FILL);
                this.j0.set(bounds);
                canvas2.drawRoundRect(this.j0, m(), m(), this.g0);
            }
            if (!this.H0) {
                this.g0.setColor(this.o0);
                this.g0.setStyle(Paint.Style.FILL);
                Paint paint = this.g0;
                ColorFilter colorFilter = this.w0;
                if (colorFilter == null) {
                    colorFilter = this.x0;
                }
                paint.setColorFilter(colorFilter);
                this.j0.set(bounds);
                canvas2.drawRoundRect(this.j0, m(), m(), this.g0);
            }
            if (this.H0) {
                super.draw(canvas);
            }
            if (this.E > 0.0f && !this.H0) {
                this.g0.setColor(this.q0);
                this.g0.setStyle(Paint.Style.STROKE);
                if (!this.H0) {
                    Paint paint2 = this.g0;
                    ColorFilter colorFilter2 = this.w0;
                    if (colorFilter2 == null) {
                        colorFilter2 = this.x0;
                    }
                    paint2.setColorFilter(colorFilter2);
                }
                RectF rectF = this.j0;
                float f6 = this.E / 2.0f;
                rectF.set(((float) bounds.left) + f6, ((float) bounds.top) + f6, ((float) bounds.right) - f6, ((float) bounds.bottom) - f6);
                float f7 = this.C - (this.E / 2.0f);
                canvas2.drawRoundRect(this.j0, f7, f7, this.g0);
            }
            this.g0.setColor(this.r0);
            this.g0.setStyle(Paint.Style.FILL);
            this.j0.set(bounds);
            if (!this.H0) {
                canvas2.drawRoundRect(this.j0, m(), m(), this.g0);
                i4 = 0;
            } else {
                b(new RectF(bounds), this.l0);
                i4 = 0;
                a(canvas, this.g0, this.l0, super.b.a, b());
            }
            if (q()) {
                a(bounds, this.j0);
                RectF rectF2 = this.j0;
                float f8 = rectF2.left;
                float f9 = rectF2.top;
                canvas2.translate(f8, f9);
                this.I.setBounds(i4, i4, (int) this.j0.width(), (int) this.j0.height());
                this.I.draw(canvas2);
                canvas2.translate(-f8, -f9);
            }
            if (p()) {
                a(bounds, this.j0);
                RectF rectF3 = this.j0;
                float f10 = rectF3.left;
                float f11 = rectF3.top;
                canvas2.translate(f10, f11);
                this.U.setBounds(i4, i4, (int) this.j0.width(), (int) this.j0.height());
                this.U.draw(canvas2);
                canvas2.translate(-f10, -f11);
            }
            if (!this.F0 || this.G == null) {
                i6 = i3;
                i5 = 0;
            } else {
                PointF pointF = this.k0;
                pointF.set(0.0f, 0.0f);
                Paint.Align align = Paint.Align.LEFT;
                if (this.G != null) {
                    float k2 = k() + this.X + this.a0;
                    if (ResourcesFlusher.b((Drawable) this) == 0) {
                        pointF.x = ((float) bounds.left) + k2;
                        align = Paint.Align.LEFT;
                    } else {
                        pointF.x = ((float) bounds.right) - k2;
                        align = Paint.Align.RIGHT;
                    }
                    this.m0.a.getFontMetrics(this.i0);
                    Paint.FontMetrics fontMetrics = this.i0;
                    pointF.y = ((float) bounds.centerY()) - ((fontMetrics.descent + fontMetrics.ascent) / 2.0f);
                }
                RectF rectF4 = this.j0;
                rectF4.setEmpty();
                if (this.G != null) {
                    float k3 = k() + this.X + this.a0;
                    float l2 = l() + this.e0 + this.b0;
                    if (ResourcesFlusher.b((Drawable) this) == 0) {
                        rectF4.left = ((float) bounds.left) + k3;
                        rectF4.right = ((float) bounds.right) - l2;
                    } else {
                        rectF4.left = ((float) bounds.left) + l2;
                        rectF4.right = ((float) bounds.right) - k3;
                    }
                    rectF4.top = (float) bounds.top;
                    rectF4.bottom = (float) bounds.bottom;
                }
                TextDrawableHelper textDrawableHelper = this.m0;
                if (textDrawableHelper.f2193f != null) {
                    textDrawableHelper.a.drawableState = getState();
                    TextDrawableHelper textDrawableHelper2 = this.m0;
                    textDrawableHelper2.f2193f.a(this.f0, textDrawableHelper2.a, textDrawableHelper2.b);
                }
                this.m0.a.setTextAlign(align);
                boolean z2 = Math.round(this.m0.a(this.G.toString())) > Math.round(this.j0.width());
                if (z2) {
                    i9 = canvas.save();
                    canvas2.clipRect(this.j0);
                } else {
                    i9 = 0;
                }
                CharSequence charSequence = this.G;
                if (z2 && this.E0 != null) {
                    charSequence = TextUtils.ellipsize(charSequence, this.m0.a, this.j0.width(), this.E0);
                }
                CharSequence charSequence2 = charSequence;
                int length = charSequence2.length();
                PointF pointF2 = this.k0;
                float f12 = pointF2.x;
                i5 = 0;
                float f13 = pointF2.y;
                i6 = i3;
                canvas.drawText(charSequence2, 0, length, f12, f13, this.m0.a);
                if (z2) {
                    canvas2.restoreToCount(i9);
                }
            }
            if (r()) {
                b(bounds, this.j0);
                RectF rectF5 = this.j0;
                float f14 = rectF5.left;
                float f15 = rectF5.top;
                canvas2.translate(f14, f15);
                this.N.setBounds(i5, i5, (int) this.j0.width(), (int) this.j0.height());
                if (RippleUtils.a) {
                    this.O.setBounds(this.N.getBounds());
                    this.O.jumpToCurrentState();
                    this.O.draw(canvas2);
                } else {
                    this.N.draw(canvas2);
                }
                canvas2.translate(-f14, -f15);
            }
            Paint paint3 = this.h0;
            if (paint3 != null) {
                paint3.setColor(ColorUtils.b(-16777216, 127));
                canvas2.drawRect(bounds, this.h0);
                if (q() || p()) {
                    a(bounds, this.j0);
                    canvas2.drawRect(this.j0, this.h0);
                }
                if (this.G != null) {
                    i7 = i6;
                    i8 = 255;
                    canvas.drawLine((float) bounds.left, bounds.exactCenterY(), (float) bounds.right, bounds.exactCenterY(), this.h0);
                } else {
                    i7 = i6;
                    i8 = 255;
                }
                if (r()) {
                    b(bounds, this.j0);
                    canvas2.drawRect(this.j0, this.h0);
                }
                this.h0.setColor(ColorUtils.b(-65536, 127));
                RectF rectF6 = this.j0;
                rectF6.set(bounds);
                if (r()) {
                    float f16 = this.e0 + this.d0 + this.Q + this.c0 + this.b0;
                    if (ResourcesFlusher.b((Drawable) this) == 0) {
                        rectF6.right = ((float) bounds.right) - f16;
                    } else {
                        rectF6.left = ((float) bounds.left) + f16;
                    }
                }
                canvas2.drawRect(this.j0, this.h0);
                this.h0.setColor(ColorUtils.b(-16711936, 127));
                c(bounds, this.j0);
                canvas2.drawRect(this.j0, this.h0);
            } else {
                i7 = i6;
                i8 = 255;
            }
            if (this.v0 < i8) {
                canvas2.restoreToCount(i7);
            }
        }
    }

    public void e(boolean z2) {
        if (this.B0 != z2) {
            this.B0 = z2;
            this.C0 = z2 ? RippleUtils.a(this.F) : null;
            onStateChange(getState());
        }
    }

    public void g(ColorStateList colorStateList) {
        if (this.F != colorStateList) {
            this.F = colorStateList;
            this.C0 = this.B0 ? RippleUtils.a(colorStateList) : null;
            onStateChange(getState());
        }
    }

    public int getAlpha() {
        return this.v0;
    }

    public ColorFilter getColorFilter() {
        return this.w0;
    }

    public int getIntrinsicHeight() {
        return (int) this.B;
    }

    public int getIntrinsicWidth() {
        return Math.min(Math.round(l() + this.m0.a(this.G.toString()) + k() + this.X + this.a0 + this.b0 + this.e0), this.G0);
    }

    public int getOpacity() {
        return -3;
    }

    @TargetApi(21)
    public void getOutline(Outline outline) {
        if (this.H0) {
            super.getOutline(outline);
            return;
        }
        Rect bounds = getBounds();
        if (!bounds.isEmpty()) {
            outline.setRoundRect(bounds, this.C);
        } else {
            outline.setRoundRect(0, 0, getIntrinsicWidth(), (int) this.B, this.C);
        }
        outline.setAlpha(((float) this.v0) / 255.0f);
    }

    public void i(float f2) {
        if (this.d0 != f2) {
            this.d0 = f2;
            invalidateSelf();
            if (r()) {
                o();
            }
        }
    }

    public void invalidateDrawable(Drawable drawable) {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.invalidateDrawable(this);
        }
    }

    public boolean isStateful() {
        ColorStateList colorStateList;
        if (!h(this.z) && !h(this.A) && !h(this.D) && (!this.B0 || !h(this.C0))) {
            TextAppearance textAppearance = this.m0.f2193f;
            if (!((textAppearance == null || (colorStateList = textAppearance.b) == null || !colorStateList.isStateful()) ? false : true)) {
                if ((this.T && this.U != null && this.S) || f(this.I) || f(this.U) || h(this.y0)) {
                    return true;
                }
                return false;
            }
        }
        return true;
    }

    public void j(float f2) {
        if (this.Q != f2) {
            this.Q = f2;
            invalidateSelf();
            if (r()) {
                o();
            }
        }
    }

    public float k() {
        if (q() || p()) {
            return this.Y + this.K + this.Z;
        }
        return 0.0f;
    }

    public float l() {
        if (r()) {
            return this.c0 + this.Q + this.d0;
        }
        return 0.0f;
    }

    public float m() {
        return this.H0 ? f() : this.C;
    }

    public Drawable n() {
        Drawable drawable = this.N;
        if (drawable != null) {
            return ResourcesFlusher.c(drawable);
        }
        return null;
    }

    public void o() {
        a aVar = this.D0.get();
        if (aVar != null) {
            aVar.a();
        }
    }

    public boolean onLayoutDirectionChanged(int i2) {
        boolean onLayoutDirectionChanged = super.onLayoutDirectionChanged(i2);
        if (q()) {
            onLayoutDirectionChanged |= ResourcesFlusher.a(this.I, i2);
        }
        if (p()) {
            onLayoutDirectionChanged |= ResourcesFlusher.a(this.U, i2);
        }
        if (r()) {
            onLayoutDirectionChanged |= ResourcesFlusher.a(this.N, i2);
        }
        if (!onLayoutDirectionChanged) {
            return true;
        }
        invalidateSelf();
        return true;
    }

    public boolean onLevelChange(int i2) {
        boolean onLevelChange = super.onLevelChange(i2);
        if (q()) {
            onLevelChange |= this.I.setLevel(i2);
        }
        if (p()) {
            onLevelChange |= this.U.setLevel(i2);
        }
        if (r()) {
            onLevelChange |= this.N.setLevel(i2);
        }
        if (onLevelChange) {
            invalidateSelf();
        }
        return onLevelChange;
    }

    public boolean onStateChange(int[] iArr) {
        if (this.H0) {
            super.onStateChange(iArr);
        }
        return a(iArr, this.A0);
    }

    public final boolean p() {
        return this.T && this.U != null && this.t0;
    }

    public final boolean q() {
        return this.H && this.I != null;
    }

    public final boolean r() {
        return this.M && this.N != null;
    }

    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j2) {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.scheduleDrawable(this, runnable, j2);
        }
    }

    public void setAlpha(int i2) {
        if (this.v0 != i2) {
            this.v0 = i2;
            invalidateSelf();
        }
    }

    public void setColorFilter(ColorFilter colorFilter) {
        if (this.w0 != colorFilter) {
            this.w0 = colorFilter;
            invalidateSelf();
        }
    }

    public void setTintList(ColorStateList colorStateList) {
        if (this.y0 != colorStateList) {
            this.y0 = colorStateList;
            onStateChange(getState());
        }
    }

    public void setTintMode(PorterDuff.Mode mode) {
        if (this.z0 != mode) {
            this.z0 = mode;
            this.x0 = c.a(this, this.y0, mode);
            invalidateSelf();
        }
    }

    public boolean setVisible(boolean z2, boolean z3) {
        boolean visible = super.setVisible(z2, z3);
        if (q()) {
            visible |= this.I.setVisible(z2, z3);
        }
        if (p()) {
            visible |= this.U.setVisible(z2, z3);
        }
        if (r()) {
            visible |= this.N.setVisible(z2, z3);
        }
        if (visible) {
            invalidateSelf();
        }
        return visible;
    }

    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.unscheduleDrawable(this, runnable);
        }
    }

    public void f(float f2) {
        if (this.B != f2) {
            this.B = f2;
            invalidateSelf();
            o();
        }
    }

    public void h(float f2) {
        if (this.E != f2) {
            this.E = f2;
            this.g0.setStrokeWidth(f2);
            if (this.H0) {
                super.b.f2233l = f2;
                invalidateSelf();
            }
            invalidateSelf();
        }
    }

    public void m(float f2) {
        if (this.Y != f2) {
            float k2 = k();
            this.Y = f2;
            float k3 = k();
            invalidateSelf();
            if (k2 != k3) {
                o();
            }
        }
    }

    public void n(float f2) {
        if (this.b0 != f2) {
            this.b0 = f2;
            invalidateSelf();
            o();
        }
    }

    public void k(float f2) {
        if (this.c0 != f2) {
            this.c0 = f2;
            invalidateSelf();
            if (r()) {
                o();
            }
        }
    }

    public void l(float f2) {
        if (this.Z != f2) {
            float k2 = k();
            this.Z = f2;
            float k3 = k();
            invalidateSelf();
            if (k2 != k3) {
                o();
            }
        }
    }

    public void o(float f2) {
        if (this.a0 != f2) {
            this.a0 = f2;
            invalidateSelf();
            o();
        }
    }

    public final void e(Drawable drawable) {
        if (drawable != null) {
            drawable.setCallback(null);
        }
    }

    public void f(ColorStateList colorStateList) {
        if (this.P != colorStateList) {
            this.P = colorStateList;
            if (r()) {
                this.N.setTintList(colorStateList);
            }
            onStateChange(getState());
        }
    }

    public void g(float f2) {
        if (this.X != f2) {
            this.X = f2;
            invalidateSelf();
            o();
        }
    }

    public void e(ColorStateList colorStateList) {
        if (this.D != colorStateList) {
            this.D = colorStateList;
            if (this.H0) {
                b(colorStateList);
            }
            onStateChange(getState());
        }
    }

    public void d(boolean z2) {
        if (this.M != z2) {
            boolean r2 = r();
            this.M = z2;
            boolean r3 = r();
            if (r2 != r3) {
                if (r3) {
                    a(this.N);
                } else {
                    e(this.N);
                }
                invalidateSelf();
                o();
            }
        }
    }

    public void a() {
        o();
        invalidateSelf();
    }

    public boolean b(int[] iArr) {
        if (Arrays.equals(this.A0, iArr)) {
            return false;
        }
        this.A0 = iArr;
        if (r()) {
            return a(getState(), iArr);
        }
        return false;
    }

    public void c(ColorStateList colorStateList) {
        if (this.A != colorStateList) {
            this.A = colorStateList;
            onStateChange(getState());
        }
    }

    public void e(float f2) {
        if (this.K != f2) {
            float k2 = k();
            this.K = f2;
            float k3 = k();
            invalidateSelf();
            if (k2 != k3) {
                o();
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:100:0x0147  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x00cf  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x00d2  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x00d8  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x00df  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x00e4  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x00f1  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x00fa  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x0109  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x0118  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x0142  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(int[] r9, int[] r10) {
        /*
            r8 = this;
            boolean r0 = super.onStateChange(r9)
            android.content.res.ColorStateList r1 = r8.z
            r2 = 0
            if (r1 == 0) goto L_0x0010
            int r3 = r8.n0
            int r1 = r1.getColorForState(r9, r3)
            goto L_0x0011
        L_0x0010:
            r1 = 0
        L_0x0011:
            int r3 = r8.n0
            r4 = 1
            if (r3 == r1) goto L_0x0019
            r8.n0 = r1
            r0 = 1
        L_0x0019:
            android.content.res.ColorStateList r3 = r8.A
            if (r3 == 0) goto L_0x0024
            int r5 = r8.o0
            int r3 = r3.getColorForState(r9, r5)
            goto L_0x0025
        L_0x0024:
            r3 = 0
        L_0x0025:
            int r5 = r8.o0
            if (r5 == r3) goto L_0x002c
            r8.o0 = r3
            r0 = 1
        L_0x002c:
            int r1 = i.h.f.ColorUtils.a(r3, r1)
            int r3 = r8.p0
            if (r3 == r1) goto L_0x0036
            r3 = 1
            goto L_0x0037
        L_0x0036:
            r3 = 0
        L_0x0037:
            j.c.a.b.g0.MaterialShapeDrawable$b r5 = r8.b
            android.content.res.ColorStateList r5 = r5.d
            if (r5 != 0) goto L_0x003f
            r5 = 1
            goto L_0x0040
        L_0x003f:
            r5 = 0
        L_0x0040:
            r3 = r3 | r5
            if (r3 == 0) goto L_0x004d
            r8.p0 = r1
            android.content.res.ColorStateList r0 = android.content.res.ColorStateList.valueOf(r1)
            r8.a(r0)
            r0 = 1
        L_0x004d:
            android.content.res.ColorStateList r1 = r8.D
            if (r1 == 0) goto L_0x0058
            int r3 = r8.q0
            int r1 = r1.getColorForState(r9, r3)
            goto L_0x0059
        L_0x0058:
            r1 = 0
        L_0x0059:
            int r3 = r8.q0
            if (r3 == r1) goto L_0x0060
            r8.q0 = r1
            r0 = 1
        L_0x0060:
            android.content.res.ColorStateList r1 = r8.C0
            if (r1 == 0) goto L_0x0073
            boolean r1 = j.c.a.b.e0.RippleUtils.a(r9)
            if (r1 == 0) goto L_0x0073
            android.content.res.ColorStateList r1 = r8.C0
            int r3 = r8.r0
            int r1 = r1.getColorForState(r9, r3)
            goto L_0x0074
        L_0x0073:
            r1 = 0
        L_0x0074:
            int r3 = r8.r0
            if (r3 == r1) goto L_0x007f
            r8.r0 = r1
            boolean r1 = r8.B0
            if (r1 == 0) goto L_0x007f
            r0 = 1
        L_0x007f:
            j.c.a.b.b0.TextDrawableHelper r1 = r8.m0
            j.c.a.b.d0.TextAppearance r1 = r1.f2193f
            if (r1 == 0) goto L_0x0090
            android.content.res.ColorStateList r1 = r1.b
            if (r1 == 0) goto L_0x0090
            int r3 = r8.s0
            int r1 = r1.getColorForState(r9, r3)
            goto L_0x0091
        L_0x0090:
            r1 = 0
        L_0x0091:
            int r3 = r8.s0
            if (r3 == r1) goto L_0x0098
            r8.s0 = r1
            r0 = 1
        L_0x0098:
            int[] r1 = r8.getState()
            r3 = 16842912(0x10100a0, float:2.3694006E-38)
            if (r1 != 0) goto L_0x00a3
        L_0x00a1:
            r1 = 0
            goto L_0x00b0
        L_0x00a3:
            int r5 = r1.length
            r6 = 0
        L_0x00a5:
            if (r6 >= r5) goto L_0x00a1
            r7 = r1[r6]
            if (r7 != r3) goto L_0x00ad
            r1 = 1
            goto L_0x00b0
        L_0x00ad:
            int r6 = r6 + 1
            goto L_0x00a5
        L_0x00b0:
            if (r1 == 0) goto L_0x00b8
            boolean r1 = r8.S
            if (r1 == 0) goto L_0x00b8
            r1 = 1
            goto L_0x00b9
        L_0x00b8:
            r1 = 0
        L_0x00b9:
            boolean r3 = r8.t0
            if (r3 == r1) goto L_0x00d3
            android.graphics.drawable.Drawable r3 = r8.U
            if (r3 == 0) goto L_0x00d3
            float r0 = r8.k()
            r8.t0 = r1
            float r1 = r8.k()
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x00d2
            r0 = 1
            r1 = 1
            goto L_0x00d4
        L_0x00d2:
            r0 = 1
        L_0x00d3:
            r1 = 0
        L_0x00d4:
            android.content.res.ColorStateList r3 = r8.y0
            if (r3 == 0) goto L_0x00df
            int r5 = r8.u0
            int r3 = r3.getColorForState(r9, r5)
            goto L_0x00e0
        L_0x00df:
            r3 = 0
        L_0x00e0:
            int r5 = r8.u0
            if (r5 == r3) goto L_0x00f1
            r8.u0 = r3
            android.content.res.ColorStateList r0 = r8.y0
            android.graphics.PorterDuff$Mode r3 = r8.z0
            android.graphics.PorterDuffColorFilter r0 = j.c.a.a.c.n.c.a(r8, r0, r3)
            r8.x0 = r0
            goto L_0x00f2
        L_0x00f1:
            r4 = r0
        L_0x00f2:
            android.graphics.drawable.Drawable r0 = r8.I
            boolean r0 = f(r0)
            if (r0 == 0) goto L_0x0101
            android.graphics.drawable.Drawable r0 = r8.I
            boolean r0 = r0.setState(r9)
            r4 = r4 | r0
        L_0x0101:
            android.graphics.drawable.Drawable r0 = r8.U
            boolean r0 = f(r0)
            if (r0 == 0) goto L_0x0110
            android.graphics.drawable.Drawable r0 = r8.U
            boolean r0 = r0.setState(r9)
            r4 = r4 | r0
        L_0x0110:
            android.graphics.drawable.Drawable r0 = r8.N
            boolean r0 = f(r0)
            if (r0 == 0) goto L_0x012d
            int r0 = r9.length
            int r3 = r10.length
            int r0 = r0 + r3
            int[] r0 = new int[r0]
            int r3 = r9.length
            java.lang.System.arraycopy(r9, r2, r0, r2, r3)
            int r9 = r9.length
            int r3 = r10.length
            java.lang.System.arraycopy(r10, r2, r0, r9, r3)
            android.graphics.drawable.Drawable r9 = r8.N
            boolean r9 = r9.setState(r0)
            r4 = r4 | r9
        L_0x012d:
            boolean r9 = j.c.a.b.e0.RippleUtils.a
            if (r9 == 0) goto L_0x0140
            android.graphics.drawable.Drawable r9 = r8.O
            boolean r9 = f(r9)
            if (r9 == 0) goto L_0x0140
            android.graphics.drawable.Drawable r9 = r8.O
            boolean r9 = r9.setState(r10)
            r4 = r4 | r9
        L_0x0140:
            if (r4 == 0) goto L_0x0145
            r8.invalidateSelf()
        L_0x0145:
            if (r1 == 0) goto L_0x014a
            r8.o()
        L_0x014a:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.b.v.ChipDrawable.a(int[], int[]):boolean");
    }

    @Deprecated
    public void c(float f2) {
        if (this.C != f2) {
            this.C = f2;
            setShapeAppearanceModel(super.b.a.a(f2));
        }
    }

    public void b(boolean z2) {
        if (this.T != z2) {
            boolean p2 = p();
            this.T = z2;
            boolean p3 = p();
            if (p2 != p3) {
                if (p3) {
                    a(this.U);
                } else {
                    e(this.U);
                }
                invalidateSelf();
                o();
            }
        }
    }

    public void d(Drawable drawable) {
        Drawable n2 = n();
        if (n2 != drawable) {
            float l2 = l();
            this.N = drawable != null ? ResourcesFlusher.d(drawable).mutate() : null;
            if (RippleUtils.a) {
                this.O = new RippleDrawable(RippleUtils.a(this.F), this.N, J0);
            }
            float l3 = l();
            e(n2);
            if (r()) {
                a(this.N);
            }
            invalidateSelf();
            if (l2 != l3) {
                o();
            }
        }
    }

    public void c(boolean z2) {
        if (this.H != z2) {
            boolean q2 = q();
            this.H = z2;
            boolean q3 = q();
            if (q2 != q3) {
                if (q3) {
                    a(this.I);
                } else {
                    e(this.I);
                }
                invalidateSelf();
                o();
            }
        }
    }

    public void b(Drawable drawable) {
        if (this.U != drawable) {
            float k2 = k();
            this.U = drawable;
            float k3 = k();
            e(this.U);
            a(this.U);
            invalidateSelf();
            if (k2 != k3) {
                o();
            }
        }
    }

    public void c(Drawable drawable) {
        Drawable drawable2 = this.I;
        Drawable drawable3 = null;
        Drawable c = drawable2 != null ? ResourcesFlusher.c(drawable2) : null;
        if (c != drawable) {
            float k2 = k();
            if (drawable != null) {
                drawable3 = ResourcesFlusher.d(drawable).mutate();
            }
            this.I = drawable3;
            float k3 = k();
            e(c);
            if (q()) {
                a(this.I);
            }
            invalidateSelf();
            if (k2 != k3) {
                o();
            }
        }
    }

    public void d(float f2) {
        if (this.e0 != f2) {
            this.e0 = f2;
            invalidateSelf();
            o();
        }
    }

    public final void a(Drawable drawable) {
        if (drawable != null) {
            drawable.setCallback(this);
            ResourcesFlusher.a(drawable, ResourcesFlusher.b((Drawable) this));
            drawable.setLevel(getLevel());
            drawable.setVisible(isVisible(), false);
            if (drawable == this.N) {
                if (drawable.isStateful()) {
                    drawable.setState(this.A0);
                }
                drawable.setTintList(this.P);
                return;
            }
            if (drawable.isStateful()) {
                drawable.setState(getState());
            }
            Drawable drawable2 = this.I;
            if (drawable == drawable2 && this.L) {
                drawable2.setTintList(this.J);
            }
        }
    }

    public void a(CharSequence charSequence) {
        if (charSequence == null) {
            charSequence = "";
        }
        if (!TextUtils.equals(this.G, charSequence)) {
            this.G = charSequence;
            this.m0.d = true;
            invalidateSelf();
            o();
        }
    }

    public void a(TextAppearance textAppearance) {
        this.m0.a(textAppearance, this.f0);
    }

    public void a(boolean z2) {
        if (this.S != z2) {
            this.S = z2;
            float k2 = k();
            if (!z2 && this.t0) {
                this.t0 = false;
            }
            float k3 = k();
            invalidateSelf();
            if (k2 != k3) {
                o();
            }
        }
    }
}
