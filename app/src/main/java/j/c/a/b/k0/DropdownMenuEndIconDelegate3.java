package j.c.a.b.k0;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

/* compiled from: DropdownMenuEndIconDelegate */
public class DropdownMenuEndIconDelegate3 extends AnimatorListenerAdapter {
    public final /* synthetic */ DropdownMenuEndIconDelegate a;

    public DropdownMenuEndIconDelegate3(DropdownMenuEndIconDelegate dropdownMenuEndIconDelegate) {
        this.a = dropdownMenuEndIconDelegate;
    }

    public void onAnimationEnd(Animator animator) {
        DropdownMenuEndIconDelegate dropdownMenuEndIconDelegate = this.a;
        dropdownMenuEndIconDelegate.c.setChecked(dropdownMenuEndIconDelegate.h);
        this.a.f2273n.start();
    }
}
