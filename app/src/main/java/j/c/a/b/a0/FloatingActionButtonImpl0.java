package j.c.a.b.a0;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import j.c.a.b.a0.FloatingActionButtonImpl1;

/* compiled from: FloatingActionButtonImpl */
public class FloatingActionButtonImpl0 extends AnimatorListenerAdapter {
    public final /* synthetic */ boolean a;
    public final /* synthetic */ FloatingActionButtonImpl1.f b;
    public final /* synthetic */ FloatingActionButtonImpl1 c;

    public FloatingActionButtonImpl0(FloatingActionButtonImpl1 floatingActionButtonImpl1, boolean z, FloatingActionButtonImpl1.f fVar) {
        this.c = floatingActionButtonImpl1;
        this.a = z;
        this.b = fVar;
    }

    public void onAnimationEnd(Animator animator) {
        FloatingActionButtonImpl1 floatingActionButtonImpl1 = this.c;
        floatingActionButtonImpl1.f2158p = 0;
        floatingActionButtonImpl1.f2153k = null;
        FloatingActionButtonImpl1.f fVar = this.b;
        if (fVar != null) {
            FloatingActionButton floatingActionButton = (FloatingActionButton) fVar;
            floatingActionButton.a.b(floatingActionButton.b);
        }
    }

    public void onAnimationStart(Animator animator) {
        this.c.f2162t.a(0, this.a);
        FloatingActionButtonImpl1 floatingActionButtonImpl1 = this.c;
        floatingActionButtonImpl1.f2158p = 2;
        floatingActionButtonImpl1.f2153k = animator;
    }
}
