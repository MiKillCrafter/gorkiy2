package j.c.a.b.b0;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import androidx.appcompat.widget.LinearLayoutCompat;
import j.c.a.b.l;

public class ForegroundLinearLayout extends LinearLayoutCompat {

    /* renamed from: q  reason: collision with root package name */
    public Drawable f2181q;

    /* renamed from: r  reason: collision with root package name */
    public final Rect f2182r;

    /* renamed from: s  reason: collision with root package name */
    public final Rect f2183s;

    /* renamed from: t  reason: collision with root package name */
    public int f2184t;
    public boolean u;
    public boolean v;

    public ForegroundLinearLayout(Context context) {
        this(context, null);
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        Drawable drawable = this.f2181q;
        if (drawable != null) {
            if (this.v) {
                this.v = false;
                Rect rect = this.f2182r;
                Rect rect2 = this.f2183s;
                int right = getRight() - getLeft();
                int bottom = getBottom() - getTop();
                if (this.u) {
                    rect.set(0, 0, right, bottom);
                } else {
                    rect.set(getPaddingLeft(), getPaddingTop(), right - getPaddingRight(), bottom - getPaddingBottom());
                }
                Gravity.apply(this.f2184t, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), rect, rect2);
                drawable.setBounds(rect2);
            }
            drawable.draw(canvas);
        }
    }

    @TargetApi(21)
    public void drawableHotspotChanged(float f2, float f3) {
        super.drawableHotspotChanged(f2, f3);
        Drawable drawable = this.f2181q;
        if (drawable != null) {
            drawable.setHotspot(f2, f3);
        }
    }

    public void drawableStateChanged() {
        super.drawableStateChanged();
        Drawable drawable = this.f2181q;
        if (drawable != null && drawable.isStateful()) {
            this.f2181q.setState(getDrawableState());
        }
    }

    public Drawable getForeground() {
        return this.f2181q;
    }

    public int getForegroundGravity() {
        return this.f2184t;
    }

    public void jumpDrawablesToCurrentState() {
        super.jumpDrawablesToCurrentState();
        Drawable drawable = this.f2181q;
        if (drawable != null) {
            drawable.jumpToCurrentState();
        }
    }

    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        super.onLayout(z, i2, i3, i4, i5);
        this.v = z | this.v;
    }

    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        this.v = true;
    }

    public void setForeground(Drawable drawable) {
        Drawable drawable2 = this.f2181q;
        if (drawable2 != drawable) {
            if (drawable2 != null) {
                drawable2.setCallback(null);
                unscheduleDrawable(this.f2181q);
            }
            this.f2181q = drawable;
            if (drawable != null) {
                setWillNotDraw(false);
                drawable.setCallback(this);
                if (drawable.isStateful()) {
                    drawable.setState(getDrawableState());
                }
                if (this.f2184t == 119) {
                    drawable.getPadding(new Rect());
                }
            } else {
                setWillNotDraw(true);
            }
            requestLayout();
            invalidate();
        }
    }

    public void setForegroundGravity(int i2) {
        if (this.f2184t != i2) {
            if ((8388615 & i2) == 0) {
                i2 |= 8388611;
            }
            if ((i2 & 112) == 0) {
                i2 |= 48;
            }
            this.f2184t = i2;
            if (i2 == 119 && this.f2181q != null) {
                this.f2181q.getPadding(new Rect());
            }
            requestLayout();
        }
    }

    public boolean verifyDrawable(Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.f2181q;
    }

    public ForegroundLinearLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ForegroundLinearLayout(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f2182r = new Rect();
        this.f2183s = new Rect();
        this.f2184t = 119;
        this.u = true;
        this.v = false;
        TypedArray b = ThemeEnforcement.b(context, attributeSet, l.ForegroundLinearLayout, i2, 0, new int[0]);
        this.f2184t = b.getInt(l.ForegroundLinearLayout_android_foregroundGravity, this.f2184t);
        Drawable drawable = b.getDrawable(l.ForegroundLinearLayout_android_foreground);
        if (drawable != null) {
            setForeground(drawable);
        }
        this.u = b.getBoolean(l.ForegroundLinearLayout_foregroundInsidePadding, true);
        b.recycle();
    }
}
