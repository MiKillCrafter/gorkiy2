package j.c.a.b.s;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.LayerDrawable;
import android.view.View;
import com.google.android.material.button.MaterialButton;
import j.c.a.a.c.n.c;
import j.c.a.b.b;
import j.c.a.b.g0.MaterialShapeDrawable;
import j.c.a.b.g0.ShapeAppearanceModel;
import j.c.a.b.g0.Shapeable;

public class MaterialButtonHelper {
    public final MaterialButton a;
    public ShapeAppearanceModel b;
    public int c;
    public int d;

    /* renamed from: e  reason: collision with root package name */
    public int f2328e;

    /* renamed from: f  reason: collision with root package name */
    public int f2329f;
    public int g;
    public int h;

    /* renamed from: i  reason: collision with root package name */
    public PorterDuff.Mode f2330i;

    /* renamed from: j  reason: collision with root package name */
    public ColorStateList f2331j;

    /* renamed from: k  reason: collision with root package name */
    public ColorStateList f2332k;

    /* renamed from: l  reason: collision with root package name */
    public ColorStateList f2333l;

    /* renamed from: m  reason: collision with root package name */
    public Drawable f2334m;

    /* renamed from: n  reason: collision with root package name */
    public boolean f2335n = false;

    /* renamed from: o  reason: collision with root package name */
    public boolean f2336o = false;

    /* renamed from: p  reason: collision with root package name */
    public boolean f2337p = false;

    /* renamed from: q  reason: collision with root package name */
    public boolean f2338q;

    /* renamed from: r  reason: collision with root package name */
    public LayerDrawable f2339r;

    public MaterialButtonHelper(MaterialButton materialButton, ShapeAppearanceModel shapeAppearanceModel) {
        this.a = materialButton;
        this.b = shapeAppearanceModel;
    }

    public final MaterialShapeDrawable a(boolean z) {
        LayerDrawable layerDrawable = this.f2339r;
        if (layerDrawable == null || layerDrawable.getNumberOfLayers() <= 0) {
            return null;
        }
        return (MaterialShapeDrawable) ((LayerDrawable) ((InsetDrawable) this.f2339r.getDrawable(0)).getDrawable()).getDrawable(z ^ true ? 1 : 0);
    }

    public MaterialShapeDrawable b() {
        return a(false);
    }

    public final MaterialShapeDrawable c() {
        return a(true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(android.view.View, int):int
     arg types: [com.google.android.material.button.MaterialButton, int]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(android.view.View, int):int */
    public final void d() {
        MaterialShapeDrawable b2 = b();
        MaterialShapeDrawable c2 = c();
        if (b2 != null) {
            b2.a((float) this.h, this.f2332k);
            if (c2 != null) {
                c2.a((float) this.h, this.f2335n ? c.a((View) this.a, b.colorSurface) : 0);
            }
        }
    }

    public Shapeable a() {
        LayerDrawable layerDrawable = this.f2339r;
        if (layerDrawable == null || layerDrawable.getNumberOfLayers() <= 1) {
            return null;
        }
        if (this.f2339r.getNumberOfLayers() > 2) {
            return (Shapeable) this.f2339r.getDrawable(2);
        }
        return (Shapeable) this.f2339r.getDrawable(1);
    }

    public void a(ShapeAppearanceModel shapeAppearanceModel) {
        this.b = shapeAppearanceModel;
        if (b() != null) {
            MaterialShapeDrawable b2 = b();
            b2.b.a = shapeAppearanceModel;
            b2.invalidateSelf();
        }
        if (c() != null) {
            MaterialShapeDrawable c2 = c();
            c2.b.a = shapeAppearanceModel;
            c2.invalidateSelf();
        }
        if (a() != null) {
            a().setShapeAppearanceModel(shapeAppearanceModel);
        }
    }
}
