package j.c.a.b.a0;

import android.animation.Animator;
import android.animation.AnimatorSet;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import java.util.List;

public interface MotionStrategy {
    AnimatorSet a();

    void a(ExtendedFloatingActionButton.c cVar);

    void b();

    void c();

    List<Animator.AnimatorListener> d();

    boolean e();

    void f();

    void onAnimationStart(Animator animator);
}
