package j.c.a.b.k0;

import android.animation.ValueAnimator;
import android.content.res.ColorStateList;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.RippleDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Spinner;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.textfield.TextInputLayout;
import i.b.l.a.AppCompatResources;
import i.h.l.ViewCompat;
import i.h.l.x.AccessibilityNodeInfoCompat;
import j.c.a.b.e;
import j.c.a.b.g0.MaterialShapeDrawable;
import j.c.a.b.g0.ShapeAppearanceModel;
import j.c.a.b.j;
import j.c.a.b.m.AnimationUtils;

public class DropdownMenuEndIconDelegate extends EndIconDelegate {
    public final TextWatcher d = new a();

    /* renamed from: e  reason: collision with root package name */
    public final TextInputLayout.e f2266e = new b(super.a);

    /* renamed from: f  reason: collision with root package name */
    public final TextInputLayout.f f2267f = new c();
    public boolean g = false;
    public boolean h = false;

    /* renamed from: i  reason: collision with root package name */
    public long f2268i = RecyclerView.FOREVER_NS;

    /* renamed from: j  reason: collision with root package name */
    public StateListDrawable f2269j;

    /* renamed from: k  reason: collision with root package name */
    public MaterialShapeDrawable f2270k;

    /* renamed from: l  reason: collision with root package name */
    public AccessibilityManager f2271l;

    /* renamed from: m  reason: collision with root package name */
    public ValueAnimator f2272m;

    /* renamed from: n  reason: collision with root package name */
    public ValueAnimator f2273n;

    public class a implements TextWatcher {

        /* renamed from: j.c.a.b.k0.DropdownMenuEndIconDelegate$a$a  reason: collision with other inner class name */
        public class C0027a implements Runnable {
            public final /* synthetic */ AutoCompleteTextView b;

            public C0027a(AutoCompleteTextView autoCompleteTextView) {
                this.b = autoCompleteTextView;
            }

            public void run() {
                boolean isPopupShowing = this.b.isPopupShowing();
                DropdownMenuEndIconDelegate.a(DropdownMenuEndIconDelegate.this, isPopupShowing);
                DropdownMenuEndIconDelegate.this.g = isPopupShowing;
            }
        }

        public a() {
        }

        public void afterTextChanged(Editable editable) {
            DropdownMenuEndIconDelegate dropdownMenuEndIconDelegate = DropdownMenuEndIconDelegate.this;
            AutoCompleteTextView a = DropdownMenuEndIconDelegate.a(dropdownMenuEndIconDelegate, dropdownMenuEndIconDelegate.a.getEditText());
            a.post(new C0027a(a));
        }

        public void beforeTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
        }

        public void onTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
        }
    }

    public class b extends TextInputLayout.e {
        public b(TextInputLayout textInputLayout) {
            super(textInputLayout);
        }

        public void a(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            boolean z;
            super.a(view, accessibilityNodeInfoCompat);
            accessibilityNodeInfoCompat.a.setClassName(Spinner.class.getName());
            if (Build.VERSION.SDK_INT >= 26) {
                z = accessibilityNodeInfoCompat.a.isShowingHintText();
            } else {
                Bundle b = accessibilityNodeInfoCompat.b();
                z = b != null && (b.getInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.BOOLEAN_PROPERTY_KEY", 0) & 4) == 4;
            }
            if (z) {
                accessibilityNodeInfoCompat.a((CharSequence) null);
            }
        }

        public void c(View view, AccessibilityEvent accessibilityEvent) {
            this.a.onPopulateAccessibilityEvent(view, accessibilityEvent);
            DropdownMenuEndIconDelegate dropdownMenuEndIconDelegate = DropdownMenuEndIconDelegate.this;
            AutoCompleteTextView a = DropdownMenuEndIconDelegate.a(dropdownMenuEndIconDelegate, dropdownMenuEndIconDelegate.a.getEditText());
            if (accessibilityEvent.getEventType() == 1 && DropdownMenuEndIconDelegate.this.f2271l.isTouchExplorationEnabled()) {
                DropdownMenuEndIconDelegate.a(DropdownMenuEndIconDelegate.this, a);
            }
        }
    }

    public class c implements TextInputLayout.f {
        public c() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: j.c.a.a.c.n.c.a(android.view.View, int):int
         arg types: [android.widget.AutoCompleteTextView, int]
         candidates:
          j.c.a.a.c.n.c.a(android.content.Context, int):float
          j.c.a.a.c.n.c.a(byte[], int):long
          j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
          j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
          j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
          j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
          j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
          j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
          j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
          j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
          j.c.a.a.c.n.c.a(android.view.View, float):void
          j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
          j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
          j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
          j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
          j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
          j.c.a.a.c.n.c.a(android.view.View, int):int */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: j.c.a.a.c.n.c.a(int, int, float):int
         arg types: [int, int, int]
         candidates:
          j.c.a.a.c.n.c.a(float, float, float):float
          j.c.a.a.c.n.c.a(android.content.Context, int, int):int
          j.c.a.a.c.n.c.a(android.content.Context, int, java.lang.String):int
          j.c.a.a.c.n.c.a(byte[], int, j.c.a.a.f.e.s2):int
          j.c.a.a.c.n.c.a(byte[], int, int):long
          j.c.a.a.c.n.c.a(android.content.Context, android.content.res.TypedArray, int):android.content.res.ColorStateList
          j.c.a.a.c.n.c.a(android.content.Context, i.b.q.TintTypedArray, int):android.content.res.ColorStateList
          j.c.a.a.c.n.c.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList, android.graphics.PorterDuff$Mode):android.graphics.PorterDuffColorFilter
          j.c.a.a.c.n.c.a(j.c.a.a.i.e, long, java.util.concurrent.TimeUnit):TResult
          j.c.a.a.c.n.c.a(java.lang.String, java.lang.String[], java.lang.String[]):java.lang.String
          j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[], byte[]):java.security.interfaces.ECPublicKey
          j.c.a.a.c.n.c.a(j.c.a.a.f.e.f5, java.lang.StringBuilder, int):void
          j.c.a.a.c.n.c.a(j.c.e.MessageLite, java.lang.StringBuilder, int):void
          j.c.a.a.c.n.c.a(byte[], long, int):void
          j.c.a.a.c.n.c.a(android.content.Context, int, boolean):boolean
          j.c.a.a.c.n.c.a(l.b.ObservableSource, l.b.Observer, l.b.t.Function):boolean
          j.c.a.a.c.n.c.a(int, int, float):int */
        public void a(TextInputLayout textInputLayout) {
            AutoCompleteTextView a2 = DropdownMenuEndIconDelegate.a(DropdownMenuEndIconDelegate.this, textInputLayout.getEditText());
            DropdownMenuEndIconDelegate dropdownMenuEndIconDelegate = DropdownMenuEndIconDelegate.this;
            int boxBackgroundMode = dropdownMenuEndIconDelegate.a.getBoxBackgroundMode();
            if (boxBackgroundMode == 2) {
                a2.setDropDownBackgroundDrawable(dropdownMenuEndIconDelegate.f2270k);
            } else if (boxBackgroundMode == 1) {
                a2.setDropDownBackgroundDrawable(dropdownMenuEndIconDelegate.f2269j);
            }
            DropdownMenuEndIconDelegate dropdownMenuEndIconDelegate2 = DropdownMenuEndIconDelegate.this;
            if (dropdownMenuEndIconDelegate2 != null) {
                if (a2.getKeyListener() == null) {
                    int boxBackgroundMode2 = dropdownMenuEndIconDelegate2.a.getBoxBackgroundMode();
                    MaterialShapeDrawable boxBackground = dropdownMenuEndIconDelegate2.a.getBoxBackground();
                    int a3 = j.c.a.a.c.n.c.a((View) a2, j.c.a.b.b.colorControlHighlight);
                    int[][] iArr = {new int[]{16842919}, new int[0]};
                    if (boxBackgroundMode2 == 2) {
                        int a4 = j.c.a.a.c.n.c.a((View) a2, j.c.a.b.b.colorSurface);
                        MaterialShapeDrawable materialShapeDrawable = new MaterialShapeDrawable(boxBackground.b.a);
                        int a5 = j.c.a.a.c.n.c.a(a3, a4, 0.1f);
                        materialShapeDrawable.a(new ColorStateList(iArr, new int[]{a5, 0}));
                        materialShapeDrawable.setTint(a4);
                        ColorStateList colorStateList = new ColorStateList(iArr, new int[]{a5, a4});
                        MaterialShapeDrawable materialShapeDrawable2 = new MaterialShapeDrawable(boxBackground.b.a);
                        materialShapeDrawable2.setTint(-1);
                        ViewCompat.a(a2, new LayerDrawable(new Drawable[]{new RippleDrawable(colorStateList, materialShapeDrawable, materialShapeDrawable2), boxBackground}));
                    } else if (boxBackgroundMode2 == 1) {
                        int boxBackgroundColor = dropdownMenuEndIconDelegate2.a.getBoxBackgroundColor();
                        ViewCompat.a(a2, new RippleDrawable(new ColorStateList(iArr, new int[]{j.c.a.a.c.n.c.a(a3, boxBackgroundColor, 0.1f), boxBackgroundColor}), boxBackground, boxBackground));
                    }
                }
                DropdownMenuEndIconDelegate dropdownMenuEndIconDelegate3 = DropdownMenuEndIconDelegate.this;
                if (dropdownMenuEndIconDelegate3 != null) {
                    a2.setOnTouchListener(new DropdownMenuEndIconDelegate0(dropdownMenuEndIconDelegate3, a2));
                    a2.setOnFocusChangeListener(new DropdownMenuEndIconDelegate1(dropdownMenuEndIconDelegate3));
                    a2.setOnDismissListener(new DropdownMenuEndIconDelegate2(dropdownMenuEndIconDelegate3));
                    a2.setThreshold(0);
                    a2.removeTextChangedListener(DropdownMenuEndIconDelegate.this.d);
                    a2.addTextChangedListener(DropdownMenuEndIconDelegate.this.d);
                    textInputLayout.setErrorIconDrawable((Drawable) null);
                    textInputLayout.setTextInputAccessibilityDelegate(DropdownMenuEndIconDelegate.this.f2266e);
                    textInputLayout.setEndIconVisible(true);
                    return;
                }
                throw null;
            }
            throw null;
        }
    }

    public class d implements View.OnClickListener {
        public d() {
        }

        public void onClick(View view) {
            DropdownMenuEndIconDelegate.a(DropdownMenuEndIconDelegate.this, (AutoCompleteTextView) DropdownMenuEndIconDelegate.this.a.getEditText());
        }
    }

    public DropdownMenuEndIconDelegate(TextInputLayout textInputLayout) {
        super(textInputLayout);
    }

    public void a() {
        float dimensionPixelOffset = (float) super.b.getResources().getDimensionPixelOffset(j.c.a.b.d.mtrl_shape_corner_size_small_component);
        float dimensionPixelOffset2 = (float) super.b.getResources().getDimensionPixelOffset(j.c.a.b.d.mtrl_exposed_dropdown_menu_popup_elevation);
        int dimensionPixelOffset3 = super.b.getResources().getDimensionPixelOffset(j.c.a.b.d.mtrl_exposed_dropdown_menu_popup_vertical_padding);
        MaterialShapeDrawable a2 = a(dimensionPixelOffset, dimensionPixelOffset, dimensionPixelOffset2, dimensionPixelOffset3);
        MaterialShapeDrawable a3 = a(0.0f, dimensionPixelOffset, dimensionPixelOffset2, dimensionPixelOffset3);
        this.f2270k = a2;
        StateListDrawable stateListDrawable = new StateListDrawable();
        this.f2269j = stateListDrawable;
        stateListDrawable.addState(new int[]{16842922}, a2);
        this.f2269j.addState(new int[0], a3);
        super.a.setEndIconDrawable(AppCompatResources.c(super.b, e.mtrl_dropdown_arrow));
        TextInputLayout textInputLayout = super.a;
        textInputLayout.setEndIconContentDescription(textInputLayout.getResources().getText(j.exposed_dropdown_menu_content_description));
        super.a.setEndIconOnClickListener(new d());
        super.a.a(this.f2267f);
        ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, 1.0f);
        ofFloat.setInterpolator(AnimationUtils.a);
        ofFloat.setDuration((long) 67);
        ofFloat.addUpdateListener(new DropdownMenuEndIconDelegate4(this));
        this.f2273n = ofFloat;
        ValueAnimator ofFloat2 = ValueAnimator.ofFloat(1.0f, 0.0f);
        ofFloat2.setInterpolator(AnimationUtils.a);
        ofFloat2.setDuration((long) 50);
        ofFloat2.addUpdateListener(new DropdownMenuEndIconDelegate4(this));
        this.f2272m = ofFloat2;
        ofFloat2.addListener(new DropdownMenuEndIconDelegate3(this));
        ViewCompat.h(super.c, 2);
        this.f2271l = (AccessibilityManager) super.b.getSystemService("accessibility");
    }

    public boolean a(int i2) {
        return i2 != 0;
    }

    public boolean b() {
        return true;
    }

    public final boolean c() {
        long currentTimeMillis = System.currentTimeMillis() - this.f2268i;
        return currentTimeMillis < 0 || currentTimeMillis > 300;
    }

    public static /* synthetic */ void a(DropdownMenuEndIconDelegate dropdownMenuEndIconDelegate, AutoCompleteTextView autoCompleteTextView) {
        if (dropdownMenuEndIconDelegate == null) {
            throw null;
        } else if (autoCompleteTextView != null) {
            if (dropdownMenuEndIconDelegate.c()) {
                dropdownMenuEndIconDelegate.g = false;
            }
            if (!dropdownMenuEndIconDelegate.g) {
                boolean z = dropdownMenuEndIconDelegate.h;
                boolean z2 = !z;
                if (z != z2) {
                    dropdownMenuEndIconDelegate.h = z2;
                    dropdownMenuEndIconDelegate.f2273n.cancel();
                    dropdownMenuEndIconDelegate.f2272m.start();
                }
                if (dropdownMenuEndIconDelegate.h) {
                    autoCompleteTextView.requestFocus();
                    autoCompleteTextView.showDropDown();
                    return;
                }
                autoCompleteTextView.dismissDropDown();
                return;
            }
            dropdownMenuEndIconDelegate.g = false;
        }
    }

    public static /* synthetic */ AutoCompleteTextView a(DropdownMenuEndIconDelegate dropdownMenuEndIconDelegate, EditText editText) {
        if (dropdownMenuEndIconDelegate == null) {
            throw null;
        } else if (editText instanceof AutoCompleteTextView) {
            return (AutoCompleteTextView) editText;
        } else {
            throw new RuntimeException("EditText needs to be an AutoCompleteTextView if an Exposed Dropdown Menu is being used.");
        }
    }

    public static /* synthetic */ void a(DropdownMenuEndIconDelegate dropdownMenuEndIconDelegate, boolean z) {
        if (dropdownMenuEndIconDelegate.h != z) {
            dropdownMenuEndIconDelegate.h = z;
            dropdownMenuEndIconDelegate.f2273n.cancel();
            dropdownMenuEndIconDelegate.f2272m.start();
        }
    }

    public final MaterialShapeDrawable a(float f2, float f3, float f4, int i2) {
        ShapeAppearanceModel.b bVar = new ShapeAppearanceModel.b();
        bVar.c(f2);
        bVar.d(f2);
        bVar.a(f3);
        bVar.b(f3);
        ShapeAppearanceModel a2 = bVar.a();
        MaterialShapeDrawable a3 = MaterialShapeDrawable.a(super.b, f4);
        a3.b.a = a2;
        a3.invalidateSelf();
        MaterialShapeDrawable.b bVar2 = a3.b;
        if (bVar2.f2230i == null) {
            bVar2.f2230i = new Rect();
        }
        a3.b.f2230i.set(0, i2, 0, i2);
        a3.invalidateSelf();
        return a3;
    }
}
