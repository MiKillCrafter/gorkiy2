package j.c.a.b.x;

import android.view.View;
import i.h.l.AccessibilityDelegateCompat;
import i.h.l.x.AccessibilityNodeInfoCompat;
import j.c.a.b.j;

/* compiled from: MaterialCalendar */
public class MaterialCalendar4 extends AccessibilityDelegateCompat {
    public final /* synthetic */ MaterialCalendar d;

    public MaterialCalendar4(MaterialCalendar materialCalendar) {
        this.d = materialCalendar;
    }

    public void a(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
        String str;
        super.a.onInitializeAccessibilityNodeInfo(view, accessibilityNodeInfoCompat.a);
        if (this.d.h0.getVisibility() == 0) {
            str = this.d.a(j.mtrl_picker_toggle_to_year_selection);
        } else {
            str = this.d.a(j.mtrl_picker_toggle_to_day_selection);
        }
        accessibilityNodeInfoCompat.a((CharSequence) str);
    }
}
