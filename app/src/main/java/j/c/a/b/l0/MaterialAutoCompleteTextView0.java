package j.c.a.b.l0;

import android.view.ViewParent;
import android.view.accessibility.AccessibilityManager;
import android.widget.Filterable;
import android.widget.ListAdapter;
import com.google.android.material.textfield.TextInputLayout;
import i.b.q.AppCompatAutoCompleteTextView;
import i.b.q.ListPopupWindow0;

/* compiled from: MaterialAutoCompleteTextView */
public class MaterialAutoCompleteTextView0 extends AppCompatAutoCompleteTextView {

    /* renamed from: e  reason: collision with root package name */
    public final ListPopupWindow0 f2292e;

    /* renamed from: f  reason: collision with root package name */
    public final AccessibilityManager f2293f;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public MaterialAutoCompleteTextView0(android.content.Context r4, android.util.AttributeSet r5) {
        /*
            r3 = this;
            int r0 = j.c.a.b.b.autoCompleteTextViewStyle
            r1 = 0
            android.content.Context r4 = j.c.a.b.m0.a.MaterialThemeOverlay.a(r4, r5, r0, r1)
            r3.<init>(r4, r5, r0)
            android.content.Context r4 = r3.getContext()
            java.lang.String r5 = "accessibility"
            java.lang.Object r5 = r4.getSystemService(r5)
            android.view.accessibility.AccessibilityManager r5 = (android.view.accessibility.AccessibilityManager) r5
            r3.f2293f = r5
            i.b.q.ListPopupWindow0 r5 = new i.b.q.ListPopupWindow0
            int r0 = i.b.a.listPopupWindowStyle
            r2 = 0
            r5.<init>(r4, r2, r0, r1)
            r3.f2292e = r5
            r4 = 1
            r5.a(r4)
            i.b.q.ListPopupWindow0 r4 = r3.f2292e
            r4.f1005s = r3
            r5 = 2
            android.widget.PopupWindow r4 = r4.C
            r4.setInputMethodMode(r5)
            i.b.q.ListPopupWindow0 r4 = r3.f2292e
            android.widget.ListAdapter r5 = r3.getAdapter()
            r4.a(r5)
            i.b.q.ListPopupWindow0 r4 = r3.f2292e
            j.c.a.b.l0.MaterialAutoCompleteTextView r5 = new j.c.a.b.l0.MaterialAutoCompleteTextView
            r5.<init>(r3)
            r4.f1006t = r5
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.b.l0.MaterialAutoCompleteTextView0.<init>(android.content.Context, android.util.AttributeSet):void");
    }

    public CharSequence getHint() {
        TextInputLayout textInputLayout;
        ViewParent parent = getParent();
        while (true) {
            if (parent == null) {
                textInputLayout = null;
                break;
            } else if (parent instanceof TextInputLayout) {
                textInputLayout = (TextInputLayout) parent;
                break;
            } else {
                parent = parent.getParent();
            }
        }
        if (textInputLayout == null || !textInputLayout.B) {
            return super.getHint();
        }
        return textInputLayout.getHint();
    }

    public <T extends ListAdapter & Filterable> void setAdapter(T t2) {
        super.setAdapter(t2);
        this.f2292e.a(getAdapter());
    }

    public void showDropDown() {
        AccessibilityManager accessibilityManager;
        if (getInputType() != 0 || (accessibilityManager = this.f2293f) == null || !accessibilityManager.isTouchExplorationEnabled()) {
            super.showDropDown();
        } else {
            this.f2292e.a();
        }
    }
}
