package j.c.a.b.h0;

import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import j.c.a.b.m.AnimationUtils;

/* compiled from: BaseTransientBottomBar */
public class BaseTransientBottomBar8 implements Runnable {
    public final /* synthetic */ BaseTransientBottomBar b;

    public BaseTransientBottomBar8(BaseTransientBottomBar baseTransientBottomBar) {
        this.b = baseTransientBottomBar;
    }

    public void run() {
        BaseTransientBottomBar.i iVar = this.b.c;
        if (iVar != null) {
            iVar.setVisibility(0);
            if (this.b.c.getAnimationMode() == 1) {
                BaseTransientBottomBar baseTransientBottomBar = this.b;
                if (baseTransientBottomBar != null) {
                    ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, 1.0f);
                    ofFloat.setInterpolator(AnimationUtils.a);
                    ofFloat.addUpdateListener(new BaseTransientBottomBar1(baseTransientBottomBar));
                    ValueAnimator ofFloat2 = ValueAnimator.ofFloat(0.8f, 1.0f);
                    ofFloat2.setInterpolator(AnimationUtils.d);
                    ofFloat2.addUpdateListener(new BaseTransientBottomBar2(baseTransientBottomBar));
                    AnimatorSet animatorSet = new AnimatorSet();
                    animatorSet.playTogether(ofFloat, ofFloat2);
                    animatorSet.setDuration(150L);
                    animatorSet.addListener(new BaseTransientBottomBar(baseTransientBottomBar));
                    animatorSet.start();
                    return;
                }
                throw null;
            }
            BaseTransientBottomBar baseTransientBottomBar2 = this.b;
            int a = baseTransientBottomBar2.a();
            baseTransientBottomBar2.c.setTranslationY((float) a);
            ValueAnimator valueAnimator = new ValueAnimator();
            valueAnimator.setIntValues(a, 0);
            valueAnimator.setInterpolator(AnimationUtils.b);
            valueAnimator.setDuration(250L);
            valueAnimator.addListener(new BaseTransientBottomBar3(baseTransientBottomBar2));
            valueAnimator.addUpdateListener(new BaseTransientBottomBar4(baseTransientBottomBar2, a));
            valueAnimator.start();
        }
    }
}
